﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class identitasSettingFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(identitasSettingFrm))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.SaveMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.cancelMnu = New System.Windows.Forms.ToolStripButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.rdpasienmanual = New System.Windows.Forms.RadioButton
        Me.rdpasienauto = New System.Windows.Forms.RadioButton
        Me.rdlabmanual = New System.Windows.Forms.RadioButton
        Me.rdlabauto = New System.Windows.Forms.RadioButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SaveMnu, Me.ToolStripSeparator6, Me.cancelMnu})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(232, 40)
        Me.ToolStrip1.TabIndex = 26
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'SaveMnu
        '
        Me.SaveMnu.Image = Global.MEDLIS.My.Resources.Resources.save
        Me.SaveMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SaveMnu.Name = "SaveMnu"
        Me.SaveMnu.Size = New System.Drawing.Size(67, 37)
        Me.SaveMnu.Text = "Save"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 40)
        '
        'cancelMnu
        '
        Me.cancelMnu.AutoSize = False
        Me.cancelMnu.Image = Global.MEDLIS.My.Resources.Resources.cancel
        Me.cancelMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cancelMnu.Name = "cancelMnu"
        Me.cancelMnu.Size = New System.Drawing.Size(85, 39)
        Me.cancelMnu.Text = "Cancel"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdpasienauto)
        Me.GroupBox1.Controls.Add(Me.rdpasienmanual)
        Me.GroupBox1.Location = New System.Drawing.Point(7, 87)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(222, 101)
        Me.GroupBox1.TabIndex = 27
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "ID Pasien"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rdlabauto)
        Me.GroupBox2.Controls.Add(Me.rdlabmanual)
        Me.GroupBox2.Location = New System.Drawing.Point(7, 194)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(222, 104)
        Me.GroupBox2.TabIndex = 28
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Lab Number"
        '
        'rdpasienmanual
        '
        Me.rdpasienmanual.AutoSize = True
        Me.rdpasienmanual.Location = New System.Drawing.Point(21, 28)
        Me.rdpasienmanual.Name = "rdpasienmanual"
        Me.rdpasienmanual.Size = New System.Drawing.Size(141, 17)
        Me.rdpasienmanual.TabIndex = 0
        Me.rdpasienmanual.TabStop = True
        Me.rdpasienmanual.Text = "Manual huruf dan angka"
        Me.rdpasienmanual.UseVisualStyleBackColor = True
        '
        'rdpasienauto
        '
        Me.rdpasienauto.AutoSize = True
        Me.rdpasienauto.Location = New System.Drawing.Point(21, 65)
        Me.rdpasienauto.Name = "rdpasienauto"
        Me.rdpasienauto.Size = New System.Drawing.Size(153, 17)
        Me.rdpasienauto.TabIndex = 29
        Me.rdpasienauto.TabStop = True
        Me.rdpasienauto.Text = "Auto number hanya angka "
        Me.rdpasienauto.UseVisualStyleBackColor = True
        '
        'rdlabmanual
        '
        Me.rdlabmanual.AutoSize = True
        Me.rdlabmanual.Location = New System.Drawing.Point(21, 30)
        Me.rdlabmanual.Name = "rdlabmanual"
        Me.rdlabmanual.Size = New System.Drawing.Size(141, 17)
        Me.rdlabmanual.TabIndex = 0
        Me.rdlabmanual.TabStop = True
        Me.rdlabmanual.Text = "Manual huruf dan angka"
        Me.rdlabmanual.UseVisualStyleBackColor = True
        '
        'rdlabauto
        '
        Me.rdlabauto.AutoSize = True
        Me.rdlabauto.Location = New System.Drawing.Point(21, 63)
        Me.rdlabauto.Name = "rdlabauto"
        Me.rdlabauto.Size = New System.Drawing.Size(150, 17)
        Me.rdlabauto.TabIndex = 1
        Me.rdlabauto.TabStop = True
        Me.rdlabauto.Text = "Auto number hanya angka"
        Me.rdlabauto.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 61)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(16, 13)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "..."
        '
        'identitasSettingFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(232, 301)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "identitasSettingFrm"
        Me.Text = "Setting Nomor "
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents SaveMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cancelMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rdpasienauto As System.Windows.Forms.RadioButton
    Friend WithEvents rdpasienmanual As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rdlabauto As System.Windows.Forms.RadioButton
    Friend WithEvents rdlabmanual As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
