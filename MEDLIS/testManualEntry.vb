﻿Imports System.Data.SqlClient
Public Class testManualEntry
    Dim selectedLabnumber As String
    Dim selectedPatient As Integer
    Dim patientName As String
    Dim patientGender As Integer
    Dim patientaddress As String
    Dim patientPhone As String

    Private Sub btnfindlabnumber_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnfindlabnumber.Click
        LoadAllManualJob()

    End Sub

    
    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

        Dim i As Integer
        Dim read As String
        Dim resultstatus As String
        Dim abnormalflag As String

        Dim refrange As String
        Dim fail_to_fill As Integer
        fail_to_fill = 0
        Dim _val, _status, _abnormalflag, _range As Boolean
        Dim lowercritical, lowernormal, uppernormal, uppercritical As String




        Dim id_row As Integer

        For i = 0 To dgvManual.RowCount - 1
            dgvManual.Item("resultabnormalflag", i).ReadOnly = False
        Next


        For i = 0 To dgvManual.RowCount - 1
            _val = False
            _status = False
            _abnormalflag = False
            _range = False


            'belum selesai gender nya
            If Not IsDBNull(dgvManual.Item("measurementvalue", i).Value) Then
                If Not Trim(dgvManual.Item("measurementvalue", i).Value) = "" Then
                    read = dgvManual.Item("measurementvalue", i).Value
                    dgvManual.Item("measurementvalue", i).ReadOnly = False
                    'dgvManual.Item("resultabnormalflag", i).ReadOnly = False
                    'dgvManual.Item("resultabnormalflag", i).Value = UpdateAbnormalFlag(read, dgvManual.Item("idtesttype", i).Value, patientGender)
                    ''hitung disini ... permintaan pak budi harsono
                    _val = True
                End If
            End If

            If Not IsDBNull(dgvManual.Item("id", i).Value) Then
                If Not Trim(dgvManual.Item("id", i).Value) = "" Then
                    id_row = dgvManual.Item("id", i).Value

                End If

            End If


            'resultstatus = "M"
            'If Not IsDBNull(dgvManual.Item("resulmanualtstatus", i).Value) Then
            '    If Not Trim(dgvManual.Item("resulmanualtstatus", i).Value) = "" Then
            '        resultstatus = dgvManual.Item("resulmanualtstatus", i).Value
            '        _status = True
            '    Else
            '        resultstatus = ""
            '    End If
            'End If

            abnormalflag = ""
            If Not IsDBNull(dgvManual.Item("resultabnormalflag", i).Value) Then
                If Not Trim(dgvManual.Item("resultabnormalflag", i).Value) = "" Then
                    abnormalflag = dgvManual.Item("resultabnormalflag", i).Value
                    _abnormalflag = True
                Else

                    abnormalflag = ""
                End If

            End If

            lowercritical = ""
            If Not IsDBNull(dgvManual.Item("lowercritical", i).Value) Then
                If Not Trim(dgvManual.Item("lowercritical", i).Value) = "" Then
                    lowercritical = dgvManual.Item("lowercritical", i).Value

                Else
                    lowercritical = ""
                End If

            End If

            lowernormal = ""
            If Not IsDBNull(dgvManual.Item("lowernormal", i).Value) Then
                If Not Trim(dgvManual.Item("lowernormal", i).Value) = "" Then
                    lowernormal = dgvManual.Item("lowernormal", i).Value

                Else
                    lowernormal = ""
                End If

            End If

            uppernormal = ""
            If Not IsDBNull(dgvManual.Item("uppernormal", i).Value) Then
                If Not Trim(dgvManual.Item("uppernormal", i).Value) = "" Then
                    uppernormal = dgvManual.Item("uppernormal", i).Value

                Else
                    uppernormal = ""
                End If

            End If

            uppercritical = ""
            If Not IsDBNull(dgvManual.Item("uppercritical", i).Value) Then
                If Not Trim(dgvManual.Item("uppercritical", i).Value) = "" Then
                    uppercritical = dgvManual.Item("uppercritical", i).Value

                Else
                    uppercritical = ""
                End If

            End If



            If _val = True And _abnormalflag = True Then
                quickupdate_per_row(id_row, read, resultstatus, abnormalflag, refrange, lowercritical, lowernormal, uppernormal, uppercritical)
            Else
                fail_to_fill = fail_to_fill + 1
            End If
        Next

        'dgvJobList.DataSource = Nothing
        'If dgvJobList.Columns.Contains("Detail") Then
        '    dgvJobList.Columns.Clear()
        'End If

        'dgvManual.DataSource = Nothing

        ' UpdateManualTest(selectedLabnumber, patientGender)
        UpdateJobStatus()
        LoadAllManualJob()

        'For i = 0 To dgvManual.RowCount - 1
        '    dgvManual.Item("resultabnormalflag", i).ReadOnly = False
        'Next

    End Sub

    


    Private Sub UpdateJobStatus() 'penting update status
        Dim strsql As String
        Dim pormat As String
        pormat = "yyyy-MM-dd HH:mm:ss"

        Dim datenow As Date
        datenow = Now

        strsql = "select job.labnumber from job where job.status='" & NEWSAMPLE & "' and job.labnumber not in (select distinct labnumber from jobdetail where status<>'" & FINISHSAMPLE & "' and active='" & ACTIVESAMPLE & "')"

        Dim ogreupdate As New clsGreConnect
        ogreupdate.buildConn()
        Dim cmdupdate As New SqlClient.SqlCommand(strsql, ogreupdate.grecon)
        Dim rdrupdate As SqlDataReader = cmdupdate.ExecuteReader(CommandBehavior.CloseConnection)
        If rdrupdate.HasRows Then
            Do While rdrupdate.Read
                'update job

                Dim squ As String
                squ = "update job set status='" & FINISHSAMPLE & "',datefinish='" & datenow.ToString(pormat) & "' where labnumber='" & rdrupdate("labnumber") & "'"
                Dim ogrejob As New clsGreConnect
                ogrejob.buildConn()
                ogrejob.ExecuteNonQuery(squ)
                ogrejob.CloseConn()
            Loop
        End If
        ogreupdate.CloseConn()
        MessageBox.Show("Data telah terupdate")

    End Sub



    
    Private Sub quickupdate_per_row(ByVal idrow As Integer, ByVal read_val As String, ByVal resultstatus As String, ByVal result_abnormal As String, ByVal referencerange As String, ByVal lowercritical As String, ByVal lowernormal As String, ByVal uppernormal As String, ByVal uppercritical As String)
        Dim ogre_update As New clsGreConnect
        ogre_update.buildConn()

        Dim strsql As String
        strsql = "update jobdetail set measurementvalue='" & read_val & "',resultstatus='" & resultstatus & "',resultabnormalflag='" & result_abnormal & "',referencerange='" & referencerange & "',status='" & FINISHSAMPLE & "',lowercritical='" & lowercritical & "',lowernormal='" & lowernormal & "',uppernormal='" & uppernormal & "',uppercritical='" & uppercritical & "',machineorhuman='0',idmachine='0',iduser='" & userid & "' where id='" & idrow & "'"
        ogre_update.ExecuteNonQuery(strsql)
        ogre_update.CloseConn()



    End Sub

    Private Sub ManualResultFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Icon = New System.Drawing.Icon("medlis2.ico")
            ReadLicense()

            'If Capability(909, "Manual".Length) <> "Manual" Then
            '    For Each ctrl As Control In Controls
            '        If (ctrl.GetType() Is GetType(Button)) Then
            '            Dim btn As Button = CType(ctrl, Button)
            '            btn.Enabled = False
            '        End If
            '    Next
            '    MessageBox.Show("Anda tidak punya license")
            '    Exit Sub
            'End If

            'ReadAll(918, "Hema".Length)
            'ReadAll(927, "Dirui240".Length)
            'ReadAll(936, "Palio100".Length)


            dgvJobList.DataSource = Nothing
            If dgvJobList.Columns.Contains("Detail") Then
                dgvJobList.Columns.Clear()
            End If

            dgvJobList.AllowUserToAddRows = False
            dgvJobList.RowHeadersVisible = False

            LoadAllManualJob()
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
        
    End Sub

    Private Sub LoadAllManualJob()
        Try
            dgvJobList.DataSource = Nothing
            If dgvJobList.Columns.Contains("Detail") Then
                dgvJobList.Columns.Clear()
            End If


            Dim strsql As String
            If Trim(txtlabnum.Text) = "" Then
                strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.malefemale as malefemale, patient.id as patientid,job.labnumber,patient.address,job.datereceived, patient.phone from job,patient where patient.id=job.idpasien and job.paymentstatus<>'" & NOTPAID & "' and job.labnumber in (select distinct jobdetail.labnumber from jobdetail,testtype where status='" & ALREADYSAMPLING & "' and active='" & ACTIVESAMPLE & "' and jobdetail.idtesttype=testtype.id and testtype.uf2='" & isTestWithManualEntry & "')"
            Else
                Dim src As String
                src = Trim(txtlabnum.Text)
                strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.malefemale as malefemale,patient.id as patientid,job.labnumber,job.datereceived,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.paymentstatus<>'" & NOTPAID & "' and job.labnumber='" & src & "' and job.labnumber in (select distinct jobdetail.labnumber from jobdetail,testtype where status='" & ALREADYSAMPLING & "' and active='" & ACTIVESAMPLE & "' and jobdetail.idtesttype=testtype.id and testtype.uf2='" & isTestWithManualEntry & "')"
            End If

            Dim greobject As New clsGreConnect
            greobject.buildConn()

            Dim dtable As New DataTable
            dtable = greobject.ExecuteQuery(strsql)

            'dgvjoblist = New DataGridView
            dgvJobList.DataSource = dtable
            Dim j As Integer
            For j = 0 To dgvJobList.Columns.Count - 1
                dgvJobList.Columns(j).Visible = False
            Next

            dgvJobList.Columns("labnumber").Visible = True
            dgvJobList.Columns("labnumber").HeaderText = "Lab Number"
            dgvJobList.Columns("labnumber").Width = 120

            dgvJobList.Columns("name").Visible = True
            dgvJobList.Columns("name").HeaderText = "Nama pasien"
            dgvJobList.Columns("name").Width = 120

            dgvJobList.Columns("datereceived").Visible = True
            dgvJobList.Columns("datereceived").HeaderText = "Tanggal"
            dgvJobList.Columns("datereceived").Width = 120


            Dim buttonColumn As New DataGridViewButtonColumn()
            With buttonColumn
                .Name = "Detail"
                .HeaderText = "Detail"
                .Width = 90
                .Text = "Detail"
                .UseColumnTextForButtonValue = True
                .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                .FlatStyle = FlatStyle.Standard
                .CellTemplate.Style.BackColor = Color.Yellow
                .DisplayIndex = 0
            End With
            dgvJobList.Columns.Add(buttonColumn)
            dgvJobList.AllowUserToAddRows = False
            dgvJobList.RowHeadersVisible = False

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
   

    Private Sub ShowTestData()
        Dim strsql As String
        Dim ogre As New clsGreConnect

        dgvManual.RowHeadersVisible = False
        dgvManual.AllowUserToAddRows = False
        ogre.buildConn()

        dgvManual.DataSource = Nothing
        strsql = "select jobdetail.id,jobdetail.idtesttype,jobdetail.resultabnormalflag,jobdetail.universaltest,jobdetail.measurementvalue,jobdetail.lowercritical,jobdetail.lowernormal,jobdetail.uppernormal,jobdetail.uppercritical,testtype.isrelated from jobdetail,testtype where labnumber='" & selectedLabnumber & "' and active='" & ACTIVESAMPLE & "' and status='" & ALREADYSAMPLING & "' and jobdetail.idtesttype=testtype.id and testtype.uf2='" & isTestWithManualEntry & "'"
        Dim dtable As New DataTable
        dtable = ogre.ExecuteQuery(strsql)
        dgvManual.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvManual.Columns.Count - 1
            dgvManual.Columns(j).Visible = False
        Next


        dgvManual.Columns("universaltest").Visible = True
        dgvManual.Columns("universaltest").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("universaltest").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("universaltest").HeaderText = "Test Name"
        dgvManual.Columns("universaltest").Width = 60
        dgvManual.Columns("universaltest").ReadOnly = True

        dgvManual.Columns("measurementvalue").Visible = True
        dgvManual.Columns("measurementvalue").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("measurementvalue").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("measurementvalue").HeaderText = "Nilai"
        dgvManual.Columns("measurementvalue").Width = 120

       
        'Dim dgvcomboResultStatus As New DataGridViewComboBoxColumn()  'bagus
        '' manual(result)
        ''test status tidak dimasukkan karena test status berisi data status terhadap mesin, bukan hasil
        'dgvcomboResultStatus.Visible = False
        'dgvcomboResultStatus.HeaderText = "Result Status"
        'dgvcomboResultStatus.Width = 70
        'dgvcomboResultStatus.Name = "resulmanualtstatus"
        'dgvcomboResultStatus.Items.Add("F")
        'dgvcomboResultStatus.Items.Add("C")
        'dgvManual.Columns.Add(dgvcomboResultStatus)

        'keren sebenernya bagus 
        'Dim dgvcomboAbnormalFlag As New DataGridViewComboBoxColumn()  'bagus

        'dgvcomboAbnormalFlag.HeaderText = "Abnormal Flag"
        'dgvcomboAbnormalFlag.Width = 70
        'dgvcomboAbnormalFlag.Name = "manualabnormalflag"
        'dgvcomboAbnormalFlag.Items.Add("L")
        'dgvcomboAbnormalFlag.Items.Add("H")
        'dgvcomboAbnormalFlag.Items.Add("N")
        'dgvcomboAbnormalFlag.Items.Add("A")
        'dgvManual.Columns.Add(dgvcomboAbnormalFlag)

        

        dgvManual.Columns("resultabnormalflag").Visible = True
        dgvManual.Columns("resultabnormalflag").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("resultabnormalflag").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("resultabnormalflag").HeaderText = "Abnormal Flag"
        dgvManual.Columns("resultabnormalflag").Width = 70
        dgvManual.Columns("resultabnormalflag").ReadOnly = False

        dgvManual.Columns("lowercritical").Visible = False
        dgvManual.Columns("lowercritical").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("lowercritical").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("lowercritical").HeaderText = "Lower Critical"
        dgvManual.Columns("lowercritical").Width = 70


        dgvManual.Columns("lowernormal").Visible = False
        dgvManual.Columns("lowernormal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("lowernormal").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("lowernormal").HeaderText = "Lower Normal"
        dgvManual.Columns("lowernormal").Width = 70


        dgvManual.Columns("uppernormal").Visible = False
        dgvManual.Columns("uppernormal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("uppernormal").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("uppernormal").HeaderText = "Upper Normal"
        dgvManual.Columns("uppernormal").Width = 70


        dgvManual.Columns("uppercritical").Visible = False
        dgvManual.Columns("uppercritical").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("uppercritical").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("uppercritical").HeaderText = "Upper Critical"
        dgvManual.Columns("uppercritical").Width = 70
        ogre.CloseConn()

        'hide a row --------

        'bagus ===untuk nyembunyikan row

        'For Each Row As DataGridViewRow In dgvManual.Rows
        '    Dim Visible As Boolean = True

        '    'Do this to inspect all cells in the row
        '    For i As Integer = 0 To Row.Cells.Count - 1
        '        If Not IsDBNull(Row.Cells("isrelated").Value) Then
        '            If Row.Cells("isrelated").Value = ISRELATED Then
        '                Row.Cells("resultabnormalflag").ReadOnly = True
        '                Row.Cells("measurementvalue").ReadOnly = True
        '                'Visible = False
        '                Exit For
        '            End If
        '        End If
        '    Next
        '    Row.Visible = Visible

        '    'Or you can check specific columns for their values --- alternatif
        '    ' ''If Row.Cells(0).Value Is Nothing OrElse _
        '    ' ''(IsNumeric(Row.Cells(0).Value) AndAlso CInt(Row.Cells(0).Value) < 0) Then
        '    ' ''    Visible = False
        '    ' ''End If

        'Next
        'Dim k As Integer
        'For k = 0 To dgvManual.RowCount - 1
        '    If Not IsDBNull(dgvManual.Item("measurementvalue", i).Value) Then
        '        If Not Trim(dgvManual.Item("measurementvalue", i).Value) = "" Then
        '            read = dgvManual.Item("measurementvalue", i).Value
        '            dgvManual.Item("resultabnormalflag", i).Value = UpdateAbnormalFlag(read, dgvManual.Item("idtesttype", i).Value, patientGender)
        '            'hitung disini ... permintaan pak budi harsono


        '            _val = True
        '        End If
        '    End If

        'Next
        

        ogre.CloseConn()
        ogre = Nothing

    End Sub

    Private Sub dgvJobList_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvJobList.CellContentClick
        If e.RowIndex < 0 OrElse Not e.ColumnIndex = dgvJobList.Columns("Detail").Index Then
            Return

        Else

            If Not dgvJobList.CurrentRow.IsNewRow Then

                selectedLabnumber = dgvJobList.Item("labnumber", dgvJobList.CurrentRow.Index).Value
                selectedPatient = dgvJobList.Item("patientid", dgvJobList.CurrentRow.Index).Value
                patientName = dgvJobList.Item("name", dgvJobList.CurrentRow.Index).Value
                patientaddress = dgvJobList.Item("address", dgvJobList.CurrentRow.Index).Value
                patientPhone = dgvJobList.Item("phone", dgvJobList.CurrentRow.Index).Value
                patientGender = dgvJobList.Item("malefemale", dgvJobList.CurrentRow.Index).Value
                txtaddress.ReadOnly = False
                txttelepon.ReadOnly = False
                txtname.ReadOnly = False
                txtLabnumber.ReadOnly = False
                txtSN.ReadOnly = False

                txtaddress.Text = patientaddress
                txtname.Text = patientName
                txttelepon.Text = patientPhone
                txtLabnumber.Text = selectedLabnumber
                ShowSampleNo()

                txtaddress.ReadOnly = True
                txttelepon.ReadOnly = True
                txtname.ReadOnly = True
                txtLabnumber.ReadOnly = True



                ShowTestData()
                'ShowDetail()
            End If

        End If
    End Sub

    Private Sub ShowSampleNo()
        txtSN.ReadOnly = False
        txtbarcode.ReadOnly = False
        Dim strsql As String
        strsql = "select distinct sampleno,barcode from jobdetail where labnumber='" & selectedLabnumber & "'"
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim cmd As New SqlClient.SqlCommand(strsql, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("sampleno")) Then
                    txtSN.Text = rdr("sampleno")
                End If
                If Not IsDBNull(rdr("barcode")) Then
                    txtbarcode.Text = rdr("barcode")
                End If
            Loop
        End If
        txtSN.ReadOnly = True
        txtbarcode.ReadOnly = True
        rdr.Close()
        cmd.Dispose()
        ogre.CloseConn()
    End Sub

    Private Function UpdateAbnormalFlag(ByVal value As String, ByVal idtest As Integer, ByVal gender As Integer) As String
        Dim strsql As String
        Dim min, max As String
        Dim minvalid As Boolean = False
        Dim maxvalid As Boolean = False

        Dim returnresult As String

        If gender = genderMale Then
            strsql = "select min as bawah,max as atas from testtype where id='" & idtest & "'"
        ElseIf gender = genderFemale Then
            strsql = "select minwoman as bawah,maxwoman as atas from testtype where id='" & idtest & "'"
        ElseIf gender = genderChild Then
            strsql = "select minchild as bawah,maxchild as atas from testtype where id='" & idtest & "'"
        End If

        Dim cn As New clsGreConnect
        cn.buildConn()

        returnresult = ""
        Dim cmd As New SqlClient.SqlCommand(strsql, cn.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

        If rdr.HasRows Then
            Do While rdr.Read

                If Not IsDBNull(rdr("bawah")) Then
                    min = rdr("bawah")
                    minvalid = True
                Else
                    MessageBox.Show("Nilai batas bawah belum diisi")
                End If

                If Not IsDBNull(rdr("atas")) Then
                    max = rdr("atas")
                    maxvalid = True
                Else
                    MessageBox.Show("Nilai batas atas belum diisi")
                End If

                If minvalid = True And maxvalid = True Then
                    Dim nilai As Double
                    Dim bawah As Double
                    Dim atas As Double

                    nilai = 0
                    If value.Length > 0 Then
                        nilai = CDbl(value)
                    End If

                    bawah = 0
                    If min.Length > 0 Then
                        bawah = CDbl(min)
                    End If

                    atas = 0
                    If max.Length > 0 Then
                        atas = CDbl(max)
                    End If

                    Dim result As String
                    result = "A"

                    If nilai < bawah Then
                        result = "L"
                    End If

                    If nilai > atas Then
                        result = "H"
                    End If

                    If nilai < atas And nilai > bawah Then
                        result = "N"
                    End If

                    If nilai = atas Then
                        result = "N"
                    End If

                    If nilai = bawah Then
                        result = "N"
                    End If
                    returnresult = result
                End If

                
            Loop
        End If
        Return returnresult

    End Function
   
End Class