﻿
Imports System.Data.SqlClient

Public Class PalioConnectProperty
    Private clientPalio As TCPControl
    Dim myPort As Array
    Dim isACK As Boolean
    Dim number_of_tick As Integer
    Private Sub PalioConnectProperty_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CekLicense()
        SaveMnu.Enabled = False
        LoadInstrumentName()

        ShowInstrumentProperty()
    End Sub
    Private Sub LoadInstrumentName()
        Dim strins As String
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        strins = "select distinct name from palioinstrument"
        Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                cbInstrument.Items.Add(rdr("name"))
            Loop
            cbInstrument.SelectedIndex = 0
        End If
        rdr.Close()
        cmd = Nothing
        ogre.CloseConn()
    End Sub
    

    Private Sub cbInstrument_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbInstrument.SelectedIndexChanged
        Dim strins As String
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        strins = "select * from palioinstrumentconnect where name='" & Trim(cbInstrument.Text) & "'"
        Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("ipaddress")) Then
                    'txtipaddress.Enabled = True
                    txtipaddress.Text = rdr("ipaddress")
                    'txtipaddress.Enabled = False
                End If
                
            Loop
        End If
       
    End Sub
    Private Sub ShowInstrumentProperty()
        Dim strins As String
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        strins = "select * from palioinstrumentconnect where name='" & Trim(cbInstrument.Text) & "'"
        Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("ipaddress")) Then
                    'cbPort.SelectedIndex = cbPort.FindStringExact(rdr("com"))
                    txtipaddress.Text = rdr("ipaddress")
                End If
               
            Loop
        End If
        txtipaddress.ReadOnly = True
    End Sub
    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click
        Dim strupdate As String
        strupdate = "update palioinstrumentconnect set ipaddress='" & Trim(txtipaddress.Text) & "'" & _
        " where name='" & Trim(cbInstrument.Text) & "'"

        Dim ogre As New clsGreConnect
        ogre.buildConn()
        ogre.ExecuteNonQuery(strupdate)
        ogre.CloseConn()

        EditMnu.Enabled = True
        SaveMnu.Enabled = False
        txtipaddress.ReadOnly = True
    End Sub

    Private Sub EditMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditMnu.Click
        EditMnu.Enabled = False
        SaveMnu.Enabled = True
        txtipaddress.ReadOnly = False
    End Sub

    Private Sub cancelMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelMnu.Click
        EditMnu.Enabled = True
        SaveMnu.Enabled = False
        txtipaddress.ReadOnly = True
    End Sub

    Private Sub btntest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btntest.Click
        Try

            clientPalio = New TCPControl(txtipaddress.Text, 4000)
            If clientPalio.theClient.Connected Then
                lbltest.Text = "connected"

            End If
        Catch ex As Exception
            MessageBox.Show("Mohon periksa koneksi network Anda")
        End Try
    End Sub



    
End Class