﻿Imports System
Imports System.Data
Imports Npgsql
Imports System.Data.SqlClient



Public Class clsGreConnect

    'Public grecon As Npgsql.NpgsqlConnection
    Public grecon As SqlConnection
    Protected greCommand As System.Data.SqlClient.SqlCommand
    Protected greDA As SqlClient.SqlDataAdapter
    Protected greDS As DataSet
    Protected greDT As DataTable


    Public Function TestConnection() As Boolean
        Try
            grecon = New SqlClient.SqlConnection("Data Source=" & Global_serverName & ";Initial Catalog=" & Global_dbName & ";Persist Security Info=True;User ID=" & Global_dbUser & ";Password=" & Global_dbPass & "")
            'directSqlConn.Open()

            grecon.Open()
            If grecon.State = ConnectionState.Open Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error on Test Connection")
        End Try

    End Function
    Public Function buildConn() As Boolean

        'Try
        '    Dim strconn As String
        '    strconn = "Server=127.0.0.1;Port=5432;Database=MEDLIS;User Id=dian;Password=dianjuga"
        '    'strconn = "Server=localhost;Port=5432;Database=MEDLIS;User Id=dian;Password=dianjuga"
        '    grecon = New NpgsqlConnection '(strconn)
        '    grecon.ConnectionString = strconn
        '    grecon.Open()

        '    If grecon.State <> ConnectionState.Open Then
        '        Return False
        '    ElseIf grecon.State = ConnectionState.Open Then
        '        Return True
        '    End If


        'Catch ex As Exception
        '    MsgBox(ex.Message, "warning")
        'End Try
        ' Dim grecon As New SqlClient.SqlConnection
        'grecon.ConnectionString = "Driver={PostgreSQL ANSI};database=MEDLIS;server=127.0.0.1;port=5432;uid=dian;sslmode=disable;readonly=0;protocol=7.4;User ID=dian;password=dianjuga;"
        'grecon = New SqlClient.SqlConnection("Driver={PostgreSQL ANSI};database=" & Global_dbName & ";server=" & Global_serverName & ";port=5432;uid=" & Global_dbUser & ";sslmode=disable;readonly=0;protocol=7.4;User ID=" & Global_dbUser & ";password=" & Global_dbPass & ";")
        grecon = New SqlClient.SqlConnection(strdbconn)
        grecon.Open()
        If grecon.State = ConnectionState.Open Then
            'MsgBox("Connected To PostGres", MsgBoxStyle.MsgBoxSetForeground)
            Return True
        Else
            Return False
        End If
    End Function

    Public Function ExecuteQuery(ByVal Query As String) As DataTable
        If grecon.State <> ConnectionState.Open Then
            MsgBox(" Connection lost", MsgBoxStyle.Critical, " Acces Failed")
            Return Nothing
            Exit Function

        End If
        'greCommand = New Npgsql.NpgsqlCommand(Query, grecon)
        greCommand = New SqlClient.SqlCommand(Query, grecon)
        greDA = New SqlClient.SqlDataAdapter

        greDA.SelectCommand = greCommand
        greDS = New Data.DataSet
        greDA.Fill(greDS)
        greDT = New DataTable

        greDT = greDS.Tables(0)
        Return greDT


        greDS = Nothing
        greDA = Nothing
        greCommand = Nothing


    End Function
    Public Sub CloseConn()
        If Not IsNothing(grecon) Then
            grecon.Close()
            grecon = Nothing
        End If
    End Sub
    Public Sub ExecuteNonQuery(ByVal Query As String, Optional parameters As List(Of SqlClient.SqlParameter) = Nothing)
        If grecon.State <> ConnectionState.Open Then
            MsgBox(" Connection lost", MsgBoxStyle.Critical, " Acces Failed")
            'Return Nothing
            Exit Sub

        End If
        greCommand = New SqlClient.SqlCommand
        greCommand.Connection = grecon
        greCommand.CommandType = CommandType.Text
        greCommand.CommandText = Query

        If parameters IsNot Nothing Then
            For Each parameter In parameters
                greCommand.Parameters.Add(parameter)
            Next
        End If

        greCommand.ExecuteNonQuery()
        greCommand = Nothing
        CloseConn()

    End Sub

    'Public Function OpenConn() As Boolean
    '    CN = New SqlClient.SqlConnection("Data Source=" & Global_serverName & ";Initial Catalog=" & Global_dbName & ";Persist Security Info=True;User ID=" & Global_dbUser & ";Password=" & Global_dbPass & "")
    '    CN.Open()
    '    If CN.State <> ConnectionState.Open Then
    '        Return False
    '    Else
    '        Return True
    '    End If
    'End Function

End Class
