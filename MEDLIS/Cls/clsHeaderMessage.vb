﻿Public Class clsHeaderMessage

    Public Function MsgHeader() As String
        Dim result As String

        'used 1,2,5,10,13,14

        'record type 1
        Dim tu_rectype_1 As String = "1H|" 'used from lis to analyzer

        'delimiter def 2
        Dim wa_dmt_2 As String = "\^&" 'used  from lis to analyzer

        'message control id 3
        Dim ga_msgctrl_3 As String = "|"

        'password 4
        Dim pat_pwd_4 As String = "|"

        'sending system company name 5
        Dim ma_compname_5 As String = "|"  'analyzer

        'sending system address 6
        Dim nam_company_add_6 As String = "|"

        'reserved 7
        Dim ju_reserved_7 As String = "|"

        'sender phone number 8
        Dim pan_phonenumber_8 As String = "|"

        'comunication params 9
        Dim sem_commparm_9 As String = "|"

        'receiver id 10
        Dim sep_receiverid_10 As String = "|" 'host used

        'comment /special instruction 11
        Dim sebelas_comment_11 As String = "|"

        'processing id 12
        Dim dualas_processingid_12 As String = "|"

        'version number 13
        Dim tigalas_version_13 As String = "|"

        'message date and time 14
        Dim empatlas_msgdatetime_14 As String

        'example: 1H|\^&||PASSWORD|DPC CIRRUS||Flanders^New^Jersey^07836||973-927-2828|N81|Your System||P|1|19940407120613<CR><ETX>[51 Checksum] <CR><LF> 
        Dim msgData As String
        ' msgData = tu_rectype_1 & wa_dmt_2 & ga_msgctrl_3 & pat_pwd_4 & ma_compname_5 & nam_company_add_6 & ju_reserved_7 & pan_phonenumber_8 & sem_commparm_9 & sep_receiverid_10 & sebelas_comment_11 & dualas_processingid_12 & tigalas_version_13 & empatlas_msgdatetime_14
        msgData = tu_rectype_1 & wa_dmt_2
        '[STX] MESSAGE [CR][ETX]CHECKSUM[CR][LF]

        'Chr(2)-> [STX]
        'Chr(3)-> [ETX]
        'Chr(13)->[CR]
        'Chr(10)->[LF]

        result = Chr(2) & firstPart(msgData) & secondPart(msgData)

        Return result
    End Function

    Private Function firstPart(ByVal str As String) As String
        Dim result As String

        result = str & Chr(13) & Chr(3)

        Return result
    End Function

    Private Function secondPart(ByVal str As String) As String
        Dim result As String
        result = CountChecksum(str) & Chr(13) & Chr(10)
        Return result
    End Function

    Public Function CountChecksum(ByVal strtocount As String) As String
        Dim result As String

        Dim i As Integer
        Dim n As Integer
        Dim m As Integer
        n = 0
        For i = 1 To Len(strtocount)

            n = n + Asc(Mid(strtocount, i, 1))

        Next
        'Label4.Text = n
        'Label5.Text = n Mod 256
        n = n + 13 + 3
        m = n Mod 256
        result = Conversion.Hex(m)

        Return result
    End Function
End Class
