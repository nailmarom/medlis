﻿Public Class clsPatientMessage
    Public Function MsgPatient(ByVal labnumber As String) As String
        Dim result As String

        'recordtype 1
        Dim tu_rectype_1 As String

        'sequence number 2
        Dim wa_seqnum_2 As String

        'practice assign patient id 3
        Dim ga_practicassign_3 As String

        'laboratory assign patient id 4
        Dim pat_labassignpatientid_4 As String

        'patient id 5
        Dim ma_patientid_5 As String

        'patient name 6
        Dim nam_patientname_6 As String

        'mother maiden name 7
        Dim ju_mathername_7 As String

        'birth date 8
        Dim pan_birthdate_8 As String

        'patient sex 9
        Dim sem_sex_9 As String

        'patient race 10
        Dim sep_race_10 As String

        'patient address 11
        Dim seblas_address_11 As String

        'reserve 12
        Dim dualas_reserve_12 As String

        'patient phone 13
        Dim tigalas_phone_13 As String

        'attending physician id 14
        Dim emplas_attphysic_14 As String

        'special field 1 15
        Dim limalas_spefield1_15 As String

        'special field 2 16
        Dim enamlas_specfield2_16 As String

        'patient height 17
        Dim julas_heigh_17 As String

        'patient weight 18
        Dim dellas_weigh_18 As String

        'patient known or suspected diagnosis 19
        Dim semlas_suspectknown_19 As String

        'patient active medication 20
        Dim duapul_activemedication_20 As String

        'patient diet 21
        Dim duasatu_diet_21 As String

        'practice field 1 22
        Dim duadua_practicefield1_22 As String

        'practive field 2 23
        Dim duatiga_practicefield2_23 As String

        'admision and discharge date 24
        Dim duapat_admisiondate_24 As String

        'admision status 25
        Dim dualima_admisionstatus_25 As String

        'location 26
        Dim duaenam_location_26 As String


        'nature of alternatife 27
        Dim duaju_nature_27 As String

        'alternative code class 28
        Dim duapan_alternativecode_28 As String

        'patient religion 29
        Dim duasem_religion_29 As String

        'marital status 30
        Dim tigapul_maritalstat_30 As String

        'isolation status 31
        Dim tigasat_isolationstat_31 As String

        'language 32
        Dim tigadu_lang_32 As String

        'hospital service 33
        Dim tigatiga_hospiservice_33 As String

        'hostpital institution 34
        Dim tigaempat_hospital_34 As String

        'dosage category 35
        Dim tigalima_dosagecat_35 As String

        result = tu_rectype_1 & wa_seqnum_2 & ga_practicassign_3 & pat_labassignpatientid_4 & ma_patientid_5 & nam_patientname_6 & _
                 ju_mathername_7 & pan_birthdate_8 & sem_sex_9 & sep_race_10 & seblas_address_11 & dualas_reserve_12 & tigalas_phone_13 & _
                 emplas_attphysic_14 & limalas_spefield1_15 & enamlas_specfield2_16 & julas_heigh_17 & dellas_weigh_18 & _
                 semlas_suspectknown_19 & duapul_activemedication_20 & duasatu_diet_21 & duadua_practicefield1_22 & duatiga_practicefield2_23 & _
                 duapat_admisiondate_24 & dualima_admisionstatus_25 & duaenam_location_26 & duaju_nature_27 & _
                 duapan_alternativecode_28 & duasem_religion_29 & tigapul_maritalstat_30 & tigasat_isolationstat_31 & tigadu_lang_32 & _
                 tigatiga_hospiservice_33 & tigaempat_hospital_34 & tigalima_dosagecat_35

        Return result
    End Function
End Class
