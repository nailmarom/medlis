﻿Imports System.IO.File
Imports System.IO
Imports System.Security.Cryptography

Public Class iniReader

    'Public 

    Public serverName As String
    Public dbName As String
    Public dbUser As String
    Public dbPass As String
    Public filePath As String
    Public encrypKey As String
    Public decrypKey As String

    Dim appUserPass As String
    Dim appKeyEnc As String


    Public Sub New()

    End Sub
    Public Sub New(ByVal userPass As String, ByVal keyenc As String)
        appUserPass = userPass
        appKeyEnc = keyenc
    End Sub


    Public Sub ReadIni()
        Dim arrSetting(3) As String
        Dim myReader As iniReader
        Dim decrypKey As String
        decrypKey = "Password"
        myReader = New iniReader
        Dim testDBsetting As New clsGreConnect




        Dim strtoshow As String
        Dim getstr As String
        Dim position As Integer
        strtoshow = ""
        Try
            Dim inipath As String
            'inipath = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "greSetting.ini")
            inipath = System.IO.Path.Combine(System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.MyDocuments, "greSetting.ini"))
            Dim lines() As String = IO.File.ReadAllLines(inipath)
            Dim k As Integer
            k = 0
            For Each line As String In lines

                position = InStr(line, "=")
                For i = position To Len(line) - 1
                    getstr = getstr + line.Substring(i, 1)
                Next

                arrSetting(k) = Decrypt(getstr, decrypKey)
                k = k + 1
                getstr = ""
            Next


            Global_serverName = arrSetting(0)
            Global_dbName = arrSetting(1)
            Global_dbUser = arrSetting(2)
            Global_dbPass = arrSetting(3)


            If testDBsetting.TestConnection() Then
                isIniPresent = True
                isIniValid = True

                strdbconn = "Data Source=" & Global_serverName & ";Initial Catalog=" & Global_dbName & ";Persist Security Info=True;User ID=" & Global_dbUser & ";Password=" & Global_dbPass & ";MultipleActiveResultSets=True"

            Else
                MessageBox.Show("DBSetting.ini is Invalid", "File not found", MessageBoxButtons.OK)
                isIniPresent = True
                isIniValid = False
                'arrSetting(0). = ""

                'MessageBox.Show("Setting Database Salah")

            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "File not found", MessageBoxButtons.OK)
            isIniPresent = False
            isIniValid = False

        End Try
    End Sub


    'Public Sub New(ByVal filePath As String, ByVal decrypKey As String)
    '    'Me.decrypKey = decrypKey
    '    'Me.filePath = filePath 
    'End Sub



    Public Sub New(ByVal servername As String, ByVal dbName As String, ByVal dbUser As String, ByVal dbPass As String, ByVal filepath As String, ByVal encrypKey As String)
        Me.serverName = servername
        Me.dbName = dbName
        Me.dbUser = dbUser
        Me.dbPass = dbPass
        Me.filePath = filepath
        Me.encrypKey = encrypKey
    End Sub
    Public Function CreateEncryptedString() As String
        Dim encryptedString As String
        Dim encServer As String
        Dim encDBName As String
        Dim encDBUser As String
        Dim encDBPass As String

        encServer = Encrypt(serverName, encrypKey)
        encDBName = Encrypt(dbName, encrypKey)
        encDBUser = Encrypt(dbUser, encrypKey)
        encDBPass = Encrypt(dbPass, encrypKey)
        'encryptedString = Encrypt(serverName, encrypKey) & Encrypt(dbName, encrypKey) & Encrypt(dbUser, encrypKey) & Encrypt(dbPass, encrypKey)
        encryptedString = encServer & encDBName & encDBUser & encDBPass
        Return encryptedString
    End Function
    Public Function CreateEncryptedPassword() As String

        Dim encPass As String

        encPass = Encrypt(appUserPass, appKeyEnc)
        Return encPass

    End Function

    Public Function DisplayDecryptedString(ByVal strToDecryp) As String
        Dim decryptedString As String
        decryptedString = Decrypt(strToDecryp, decrypKey)
        Return decryptedString
    End Function



    Public Function ReadIniFile()
        Dim fileContent As String
        Dim fileStream As FileStream
        ' Dim streamWriter As StreamWriter
        Dim streamReader As StreamReader

        fileStream = New FileStream(Path:=filePath, _
            mode:=FileMode.OpenOrCreate, access:=FileAccess.Read)

        'create a stream reader instance
        streamReader = New StreamReader(Stream:=fileStream)

        'set the file pointer to the start of the file
        streamReader.BaseStream.Seek(offset:=0, _
           origin:=SeekOrigin.Begin)

        'loop through the file and write to console until the
        ' end of file reached
        Do While streamReader.Peek > -1
            fileContent = fileContent & vbCrLf & streamReader.ReadLine()
        Loop
        'close the stream reader
        streamReader.Close()
        Return fileContent




    End Function


    '[strText]: a string that has been encrypt with the above method
    '[sDecrKey]: string is the key needed to decrypt

    Public Function Decrypt(ByVal strText As String, ByVal sDecrKey As String) As String
        Dim IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
        Dim inputByteArray(strText.Length) As Byte
        Try
            Dim byKey() As Byte = System.Text.Encoding.UTF8.GetBytes(Left(sDecrKey, 8))
            Dim des As New DESCryptoServiceProvider
            inputByteArray = Convert.FromBase64String(strText)
            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8
            Return encoding.GetString(ms.ToArray())
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function




    Private Shared Function Encrypt(ByVal strText As String, ByVal strEncrKey As String) As String
        Dim IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
        Try
            Dim bykey() As Byte = System.Text.Encoding.UTF8.GetBytes(Left(strEncrKey, 8))
            Dim InputByteArray() As Byte = System.Text.Encoding.UTF8.GetBytes(strText)
            Dim des As New DESCryptoServiceProvider
            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(bykey, IV), CryptoStreamMode.Write)
            cs.Write(InputByteArray, 0, InputByteArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function


    Public Sub SaveTextToFile()

        Dim i As Integer
        Dim aryText(3) As String

        aryText(0) = "ServerName=" & Encrypt(serverName, encrypKey)
        aryText(1) = "DBname=" & Encrypt(dbName, encrypKey)
        aryText(2) = "DBuser=" & Encrypt(dbUser, encrypKey)
        aryText(3) = "Dbpass=" & Encrypt(dbPass, encrypKey)


        Dim objWriter As New System.IO.StreamWriter(filePath)

        For i = 0 To 3
            objWriter.WriteLine(aryText(i))
        Next
        objWriter.Close()
    End Sub




End Class
