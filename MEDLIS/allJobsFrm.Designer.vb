﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AlljobsFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AlljobsFrm))
        Me.dgvJoblist = New System.Windows.Forms.DataGridView
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtname = New System.Windows.Forms.TextBox
        Me.txtaddress = New System.Windows.Forms.TextBox
        Me.txttelepon = New System.Windows.Forms.TextBox
        Me.dgvdetail = New System.Windows.Forms.DataGridView
        Me.lblStatus = New System.Windows.Forms.Label
        Me.txtlabnum = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.NewTestMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.SamplingMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.readMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.FinishMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator
        Me.AllTest = New System.Windows.Forms.ToolStripButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Barcode = New System.Windows.Forms.Label
        Me.txtbarcode = New System.Windows.Forms.TextBox
        Me.txtsn = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        CType(Me.dgvJoblist, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvdetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvJoblist
        '
        Me.dgvJoblist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvJoblist.Location = New System.Drawing.Point(12, 78)
        Me.dgvJoblist.Name = "dgvJoblist"
        Me.dgvJoblist.Size = New System.Drawing.Size(609, 453)
        Me.dgvJoblist.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(67, 49)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 17)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Nama"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(59, 81)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 17)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Alamat"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(49, 108)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 17)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Telepon"
        '
        'txtname
        '
        Me.txtname.Location = New System.Drawing.Point(123, 49)
        Me.txtname.Name = "txtname"
        Me.txtname.Size = New System.Drawing.Size(315, 21)
        Me.txtname.TabIndex = 12
        '
        'txtaddress
        '
        Me.txtaddress.Location = New System.Drawing.Point(123, 79)
        Me.txtaddress.Name = "txtaddress"
        Me.txtaddress.Size = New System.Drawing.Size(315, 21)
        Me.txtaddress.TabIndex = 13
        '
        'txttelepon
        '
        Me.txttelepon.Location = New System.Drawing.Point(123, 109)
        Me.txttelepon.Name = "txttelepon"
        Me.txttelepon.Size = New System.Drawing.Size(165, 21)
        Me.txttelepon.TabIndex = 14
        '
        'dgvdetail
        '
        Me.dgvdetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvdetail.Location = New System.Drawing.Point(630, 268)
        Me.dgvdetail.Name = "dgvdetail"
        Me.dgvdetail.Size = New System.Drawing.Size(463, 263)
        Me.dgvdetail.TabIndex = 15
        '
        'lblStatus
        '
        Me.lblStatus.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(13, 48)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblStatus.Size = New System.Drawing.Size(397, 27)
        Me.lblStatus.TabIndex = 24
        Me.lblStatus.Text = "....."
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtlabnum
        '
        Me.txtlabnum.Location = New System.Drawing.Point(123, 19)
        Me.txtlabnum.Name = "txtlabnum"
        Me.txtlabnum.Size = New System.Drawing.Size(165, 21)
        Me.txtlabnum.TabIndex = 27
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(20, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 17)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "Lab Number"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewTestMnu, Me.ToolStripSeparator5, Me.SamplingMnu, Me.ToolStripSeparator6, Me.readMnu, Me.ToolStripSeparator7, Me.FinishMnu, Me.ToolStripSeparator8, Me.AllTest})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1020, 46)
        Me.ToolStrip1.TabIndex = 33
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'NewTestMnu
        '
        Me.NewTestMnu.Image = Global.MEDLIS.My.Resources.Resources.document_plus
        Me.NewTestMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.NewTestMnu.Name = "NewTestMnu"
        Me.NewTestMnu.Size = New System.Drawing.Size(67, 43)
        Me.NewTestMnu.Text = "Baru"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 46)
        '
        'SamplingMnu
        '
        Me.SamplingMnu.Image = Global.MEDLIS.My.Resources.Resources.heart
        Me.SamplingMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SamplingMnu.Name = "SamplingMnu"
        Me.SamplingMnu.Size = New System.Drawing.Size(93, 43)
        Me.SamplingMnu.Text = "Sampling"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 46)
        '
        'readMnu
        '
        Me.readMnu.Image = Global.MEDLIS.My.Resources.Resources.terminal
        Me.readMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.readMnu.Name = "readMnu"
        Me.readMnu.Size = New System.Drawing.Size(68, 43)
        Me.readMnu.Text = "Baca"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(6, 46)
        '
        'FinishMnu
        '
        Me.FinishMnu.AutoSize = False
        Me.FinishMnu.Image = Global.MEDLIS.My.Resources.Resources.check
        Me.FinishMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.FinishMnu.Name = "FinishMnu"
        Me.FinishMnu.Size = New System.Drawing.Size(82, 39)
        Me.FinishMnu.Text = "Finish"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(6, 46)
        '
        'AllTest
        '
        Me.AllTest.AutoSize = False
        Me.AllTest.Image = Global.MEDLIS.My.Resources.Resources.folder_document_in
        Me.AllTest.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.AllTest.Name = "AllTest"
        Me.AllTest.Size = New System.Drawing.Size(85, 39)
        Me.AllTest.Text = "Semua"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Barcode)
        Me.GroupBox1.Controls.Add(Me.txtbarcode)
        Me.GroupBox1.Controls.Add(Me.txtsn)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtlabnum)
        Me.GroupBox1.Controls.Add(Me.txttelepon)
        Me.GroupBox1.Controls.Add(Me.txtaddress)
        Me.GroupBox1.Controls.Add(Me.txtname)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(629, 70)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(464, 192)
        Me.GroupBox1.TabIndex = 34
        Me.GroupBox1.TabStop = False
        '
        'Barcode
        '
        Me.Barcode.AutoSize = True
        Me.Barcode.Location = New System.Drawing.Point(56, 166)
        Me.Barcode.Name = "Barcode"
        Me.Barcode.Size = New System.Drawing.Size(60, 15)
        Me.Barcode.TabIndex = 35
        Me.Barcode.Text = "Barcode"
        '
        'txtbarcode
        '
        Me.txtbarcode.Location = New System.Drawing.Point(123, 163)
        Me.txtbarcode.Name = "txtbarcode"
        Me.txtbarcode.Size = New System.Drawing.Size(165, 21)
        Me.txtbarcode.TabIndex = 34
        '
        'txtsn
        '
        Me.txtsn.Location = New System.Drawing.Point(123, 136)
        Me.txtsn.Name = "txtsn"
        Me.txtsn.Size = New System.Drawing.Size(165, 21)
        Me.txtsn.TabIndex = 30
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(38, 138)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(78, 15)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "No Sample"
        '
        'AlljobsFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1020, 600)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.dgvdetail)
        Me.Controls.Add(Me.dgvJoblist)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "AlljobsFrm"
        Me.Text = "Job Monitoring"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvJoblist, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvdetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvJoblist As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtname As System.Windows.Forms.TextBox
    Friend WithEvents txtaddress As System.Windows.Forms.TextBox
    Friend WithEvents txttelepon As System.Windows.Forms.TextBox
    Friend WithEvents dgvdetail As System.Windows.Forms.DataGridView
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents txtlabnum As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents NewTestMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SamplingMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents readMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents FinishMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AllTest As System.Windows.Forms.ToolStripButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtsn As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Barcode As System.Windows.Forms.Label
    Friend WithEvents txtbarcode As System.Windows.Forms.TextBox
End Class
