﻿Imports System.Data
Imports System.Data.SqlClient

Public Class licenseFrm
    Dim tblGroupTest As DataTable
    Dim objConn As New clsGreConnect
    Dim selectedLicenseId
    Dim isedit, isnew As Boolean

    Private Sub LicenseUsbParentShow()
        Try
            Dim strsql As String
            Dim usbparent As Integer
            usbparent = getParent(890, 6) '.Replace("""", "")

            Label2.Text = usbparent
            'strsql = "insert into licensemanager(name,val,parent) values('" & txtnama.Text.Trim & "','" & txtvalue.Text.Trim & "','" & usbparent & "') "
            'Dim ogre As New clsGreConnect
            'ogre.buildConn()
            'ogre.ExecuteNonQuery(strsql)
            'ogre.CloseConn()
        Catch ex As Exception
            MessageBox.Show("license error")
        End Try
        
    End Sub




    Private Sub licenseFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
        txtnama.Enabled = False
        txtvalue.Enabled = False
        SaveMnu.Enabled = False

        DeleteMnu.Enabled = False
        'ToolStrip1.ImageScalingSize = New Size(40, 40)
        LicenseUsbParentShow()
        PopulateDg()
    End Sub
    Private Sub PopulateDg()


        dgv.DataSource = Nothing
        If dgv.Columns.Contains("chk") Then
            dgv.Columns.Clear()
        End If

        objConn.buildConn()
        tblGroupTest = New DataTable
        tblGroupTest = objConn.ExecuteQuery("select * from licensemanager")

        dgv.AllowUserToAddRows = False
        dgv.RowHeadersVisible = False


        dgv.DataSource = tblGroupTest


        Dim j As Integer
        For j = 0 To dgv.Columns.Count - 1
            dgv.Columns(j).Visible = False
        Next



        Dim chk As New DataGridViewCheckBoxColumn()
        dgv.Columns.Add(chk)
        chk.HeaderText = "SELECT"
        chk.Width = 60
        chk.Name = "chk"
        dgv.Columns("chk").DisplayIndex = 0
        dgv.Columns("chk").Name = "chk"

        dgv.Columns("name").Visible = True
        dgv.Columns("name").HeaderText = "Nama"
        dgv.Columns("name").Width = 100
        dgv.Columns("name").ReadOnly = True

        dgv.Columns("val").Visible = True
        dgv.Columns("val").HeaderText = "Value"
        dgv.Columns("val").Width = 100
        dgv.Columns("val").ReadOnly = True

        dgv.Columns("parent").Visible = True
        dgv.Columns("parent").HeaderText = "USB License "
        dgv.Columns("parent").Width = 100
        dgv.Columns("parent").ReadOnly = True



        dgv.Columns("id").Visible = False
        'DGPack.Columns("packname").Width = 250
        objConn.CloseConn()


    End Sub
    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click
        Try

            If txtnama.Text = "" Or txtvalue.Text = "" Then
                MessageBox.Show("Silahkan isi data yang diperlukan")
                Exit Sub
            End If

            Dim strsql As String
            Dim usbparent As Integer
            usbparent = getParent(890, 6) '.Replace("""", "")

            strsql = "insert into licensemanager(name,val,parent) values('" & txtnama.Text.Trim & "','" & txtvalue.Text.Trim & "','" & usbparent & "') "
            Dim ogre As New clsGreConnect
            ogre.buildConn()
            ogre.ExecuteNonQuery(strsql)
            ogre.CloseConn()
        Catch ex As Exception
            MessageBox.Show("error 888 license Anda tidak valid ")
        End Try
        AddMnu.Enabled = True
        SaveMnu.Enabled = False
        DeleteMnu.Enabled = True
        txtnama.Clear()
        txtvalue.Clear()
        txtnama.Enabled = False
        txtvalue.Enabled = False
        DeleteMnu.Enabled = False
        PopulateDg()
    End Sub

    Private Sub DeleteMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteMnu.Click
        Dim isavailable As Boolean 'top check yang di centang
        isavailable = False

        Dim strsql As String

        Dim j As Integer
        For j = 0 To dgv.Rows.Count - 1
            'If Not (dgTestType.CurrentRow.IsNewRow) Then
            If dgv.Item("chk", j).Value = True Then
                isavailable = True
            End If
        Next
        If isavailable = False Then
            MessageBox.Show("Pilih salah satu test item untuk diedit")
            Exit Sub
        End If

        Dim i As Integer
        For i = 0 To dgv.Rows.Count - 1
            If dgv.Item("chk", i).Value = True Then
                strsql = "delete from licensemanager " & _
                "where id='" & selectedLicenseId & "'"

                objConn.buildConn()
                objConn.ExecuteNonQuery(strsql)
                objConn.CloseConn()
            End If
        Next
        SaveMnu.Enabled = False

        isEdit = False
        isNew = False
        PopulateDg()
    End Sub

    Private Sub dgv_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellContentClick
        If Not (dgv.CurrentRow.IsNewRow) And Not (e.RowIndex = -1) Then
            If Not IsDBNull(dgv.Item("id", e.RowIndex).Value) Then
                If TypeOf dgv.Rows(e.RowIndex).Cells(e.ColumnIndex) Is DataGridViewCheckBoxCell Then
                    Dim dgvCheckBoxCell As DataGridViewCheckBoxCell = dgv.Rows(e.RowIndex).Cells(e.ColumnIndex)
                    'Commit the data to the datasouce.
                    dgv.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    Dim checked As Boolean = CType(dgvCheckBoxCell.Value, Boolean)
                    selectedLicenseId = dgv.Item("id", e.RowIndex).Value
                    'MessageBox.Show(e.RowIndex & " " & checked & " " & e.ColumnIndex)
                    ShowData(selectedLicenseId)
                    If checked Then
                        Dim i As Integer

                        DeleteMnu.Enabled = True
                        For i = 0 To dgv.Rows.Count - 1
                            If i <> e.RowIndex Then
                                dgv.Item("chk", i).Value = False
                            End If
                        Next

                    End If

                End If
            End If
        End If
    End Sub

    Private Sub ShowData(ByVal selid As Integer)
        Dim ogrecon As New clsGreConnect
        ogrecon.buildConn()
        Dim strsql As String
        strsql = "select * from licensemanager where id='" & selid & "'"

        Dim cmd As New SqlClient.SqlCommand(strsql, ogrecon.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        
        Do While rdr.Read
            txtnama.Text = rdr("name")
            txtvalue.Text = rdr("val")
            '############
        Loop

    End Sub

    Private Sub EditMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim i As Integer
        isnew = False
        isedit = True

        txtnama.Enabled = True
        txtvalue.Enabled = True
        SaveMnu.Enabled = True
        DeleteMnu.Enabled = False

    End Sub

    Private Sub AddMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddMnu.Click
        isnew = True

        SaveMnu.Enabled = True
        PopulateDg()
        txtnama.Enabled = True
        txtvalue.Enabled = True
        txtnama.Focus()
    End Sub

    Private Sub cancelMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelMnu.Click
        AddMnu.Enabled = True
        SaveMnu.Enabled = False
        DeleteMnu.Enabled = False
        txtnama.Clear()
        txtvalue.Clear()
        txtnama.Enabled = False
        txtvalue.Enabled = False
        PopulateDg()
    End Sub

    ReadOnly AllowedKeys As String = _
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

    Private Sub txtnama_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtnama.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)

        End Select
    End Sub
End Class