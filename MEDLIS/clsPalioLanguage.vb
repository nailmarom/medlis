﻿
Imports System.Text
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports System.IO
Public Class clsPalioLanguage

  
    Dim iforascii As Integer

    Dim GreenLamp As Boolean
    Dim GreenLampForResult As Boolean
    Dim GreenLampHearResult As Boolean


    Dim fpath As String
    Public fInstrument As String

    Dim selectedLabNumber As String
    Dim selectedPatient As String
    Dim selsampleno As String
    Dim patientName As String

    Dim patientPhone As String
    Dim patientaddress As String


    Dim regex As Regex

    Dim qMcnToLis As New List(Of String)
    Dim oLisToMcn As New List(Of String)
    Dim orMcnToLis As New List(Of String)

    Dim greobject As clsGreConnect
    Dim dtable As DataTable
    Dim stateChoice As Integer

    'counter==============
    Dim seq As Integer
    Dim seqQuery As Integer
    Dim counterP As Integer = 0
    Dim indexToSend As Integer
    Dim wait10second As Integer
    Dim counterO As Integer = 0



    Dim resultIndexToSend As Integer
    '=====================

    Public queryCreate As New List(Of String)

    Public qforRCreate As New List(Of String)
    Dim resultToManage As List(Of String)


    Dim idinstrument As Integer
    Dim objconn As New clsGreConnect


    Public Sub CreateLanguage()

        Try
            Dim lntomake As String
            counterP = 0
            seq = 0


            Dim ogre As New clsGreConnect
            ogre.buildConn()
            Dim strsql As String
            'strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail,palioinstrument where status='" & ALREADYSAMPLING & "' and active='" & ACTIVESAMPLE & "' and jobdetail.idtesttype=palioinstrument.idtest and palioinstrument.name='" & fInstrument & "')"
            'strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone ,jobdetail.labnumber,idtesttype,testtype.analyzertestname,palioinstrument.testbarcode,jobdetail.sampleno,jobdetail.barcode from jobdetail,palioinstrument,job,patient,testtype where job.idpasien=patient.id and jobdetail.status='" & ALREADYSAMPLING & "' and active='" & ACTIVESAMPLE & "' and jobdetail.idtesttype=palioinstrument.idtest and palioinstrument.name='" & fInstrument & "' and job.labnumber=jobdetail.labnumber and jobdetail.idtesttype=testtype.id and testtype.id=palioinstrument.idtest"
            'strsql = "select distinct labnumber from jobdetail,palioinstrument where status='" & ALREADYSAMPLING & "' and active='" & ACTIVESAMPLE & "' and jobdetail.idtesttype=palioinstrument.idtest and palioinstrument.name='" & fInstrument & "'"

            strsql = "select distinct jobdetail.labnumber from jobdetail,palioinstrument,patient,job where jobdetail.status='" & ALREADYSAMPLING & "' and active='" & ACTIVESAMPLE & "' and jobdetail.idtesttype=palioinstrument.idtest and palioinstrument.name='" & Trim(fInstrument) & "' and jobdetail.labnumber=job.labnumber and job.idpasien=patient.id"


            Dim cmd As New SqlClient.SqlCommand(strsql, ogre.grecon)
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            If rdr.HasRows Then

                queryCreate.Add(Chr(5))
                queryCreate.Add(makingH)

                Do While rdr.Read
                    If Not IsDBNull(rdr("labnumber")) Then
                        lntomake = rdr("labnumber")
                        queryCreate.Add(makingP(lntomake))
                        queryCreate.AddRange(makingO(lntomake))
                    End If
                Loop
                queryCreate.Add(makingL)
                queryCreate.Add(Chr(4)) ' add eot 
            End If


            seq = 0
        Catch ex As Exception
            MessageBox.Show(ex.ToString & " | " & ex.TargetSite.Name, "Terjadi kesalahan " & "  ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
      
        'lblmsg.Text = "message to send created"
    End Sub
    Function makingH() As String
        Try
            Dim str As String
            If seq > 6 Then
                seq = 0
            Else
                seq = seq + 1
            End If
            ' str = Chr(2) & CStr(seq) & "H|\^&|||PALIO100|||||||P|LIS2-A2|20110112100000" & Chr(13) & Chr(3) & "66" & Chr(13) & Chr(10)

            Dim firstSTR As String
            Dim secondSTR As String
            Dim indexofSTX As Integer

            firstSTR = Chr(2) & CStr(seq) & "H|\^&|||PALIO100|||||||P|LIS2-A2|20110112100000" & Chr(13) & Chr(3)
            indexofSTX = firstSTR.IndexOf(Chr(3))
            secondSTR = checkSumCalculator(Mid(firstSTR, 2, indexofSTX)) & Chr(13) & Chr(10)

            str = firstSTR & secondSTR

            Return str
        Catch ex As Exception
            MessageBox.Show(ex.ToString & " | " & ex.TargetSite.Name, "Terjadi kesalahan " & "  ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        
    End Function
    Function makingL() As String
        Try
            Dim resultL As String
            Dim firstSTR As String
            Dim secondSTR As String
            Dim counterL As Integer
            Dim indexofSTX As Integer

            counterL = 1

            If seq > 6 Then
                seq = 0
            Else
                seq = seq + 1
            End If
            firstSTR = Chr(2) & CStr(seq) & "L|" & counterL & "|N" & Chr(13) & Chr(3)
            indexofSTX = firstSTR.IndexOf(Chr(3))
            secondSTR = checkSumCalculator(Mid(firstSTR, 2, indexofSTX)) & Chr(13) & Chr(10)

            resultL = firstSTR & secondSTR
            Return resultL

        Catch ex As Exception
            MessageBox.Show(ex.ToString & " | " & ex.TargetSite.Name, "Terjadi kesalahan " & "  ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Function makingP(ByVal ln As String) As String
        Try
            Dim resultP As String
            Dim firststr As String
            Dim secondstr As String
            Dim indexofSTX As Integer
            Dim birthdate As String
            Dim patientsex As String

            If seq > 6 Then
                seq = 0
            Else
                seq = seq + 1
            End If
            'If counterP > 6 Then
            '    counterP = 1
            'Else

            counterP = counterP + 1
            'End If


            Dim strsqlP As String
            strsqlP = "select distinct job.id as jobid,job.idpasien,patient.patientname as name,patient.malefemale,patient.birthdate as tgllahir,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone ,jobdetail.labnumber,jobdetail.sampleno,jobdetail.barcode from jobdetail,palioinstrument,job,patient,testtype where job.idpasien=patient.id and jobdetail.status='1' and active='1' and jobdetail.idtesttype=palioinstrument.idtest and palioinstrument.name='palio100' and job.labnumber=jobdetail.labnumber and jobdetail.idtesttype=testtype.id and testtype.id=palioinstrument.idtest and JOBDETAIL.labnumber='" & ln & "'"

            Dim ogreP As New clsGreConnect
            ogreP.buildConn()


            Dim cmdP As New SqlClient.SqlCommand(strsqlP, ogreP.grecon)
            Dim rdrP As SqlDataReader = cmdP.ExecuteReader(CommandBehavior.CloseConnection)

            If rdrP.HasRows Then
                Do While rdrP.Read

                    patientsex = "M"
                    ' birthdate = Replace(rdrP("tgllahir"), "-", "")
                    If rdrP("malefemale") = genderMale Then
                        patientsex = "M"
                    ElseIf rdrP("malefemale") = genderFemale Then
                        patientsex = "F"
                    ElseIf rdrP("malefemale") = genderChild Then
                        patientsex = "U"
                    End If
                    birthdate = Replace(rdrP("tgllahir"), "/", "")
                    firststr = Chr(2) & CStr(seq) & "P|" & counterP & "||" & rdrP("barcode") & "||" & rdrP("name") & "||" & birthdate & "|" & patientsex & "|W|" & "|||||||||||||||||||U|S|||||A" & Chr(13) & Chr(3)
                    '0016-20101012
                    indexofSTX = firststr.IndexOf(Chr(3))
                    secondstr = checkSumCalculator(Mid(firststr, 2, indexofSTX)) & Chr(13) & Chr(10)
                    resultP = firststr & secondstr
                Loop
            End If

            Return resultP
        Catch ex As Exception
            MessageBox.Show(ex.ToString & " | " & ex.TargetSite.Name, "Terjadi kesalahan " & "  ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        

    End Function

    Function makingO(ByVal ln As String) As List(Of String)
        ' seq = seq + 1
        Try
            Dim firstStr As String
            Dim secondStr As String

            'Dim counterO As Integer
            Dim strsqlO As String
            Dim singleO As String
            Dim strO As New List(Of String)
            Dim indexOfSTX As Integer

            counterO = 0
            strsqlO = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone ,jobdetail.labnumber,jobdetail.dateorder,idtesttype,testtype.analyzertestname,palioinstrument.testbarcode,jobdetail.sampleno,jobdetail.barcode from jobdetail,palioinstrument,job,patient,testtype where job.idpasien=patient.id and jobdetail.status='1' and active='1' and jobdetail.idtesttype=palioinstrument.idtest and palioinstrument.name='" & fInstrument & "' and job.labnumber=jobdetail.labnumber and jobdetail.idtesttype=testtype.id and testtype.id=palioinstrument.idtest and jobdetail.labnumber='" & ln & "'"

            '=====================
            Dim ogre As New clsGreConnect
            ogre.buildConn()

            Dim cmdO As New SqlClient.SqlCommand(strsqlO, ogre.grecon)
            Dim rdrO As SqlDataReader = cmdO.ExecuteReader(CommandBehavior.CloseConnection)
            If rdrO.HasRows Then

                Do While rdrO.Read

                    If Not IsDBNull(rdrO("labnumber")) Then
                        counterO = counterO + 1



                        firstStr = ""
                        secondStr = ""
                        singleO = ""
                        If seq > 6 Then
                            seq = 0
                        Else
                            seq = seq + 1
                        End If
                        'firstStr = Chr(2) & seq & "O" & "|" & counterO & "||" & rdrO("barcode") & "|^" & rdrO("testbarcode") & "^^1|R|" & rdrO("dateorder") & "|||||N||||||||||||||O||||||" & Chr(13) & Chr(3)
                        'firstStr = Chr(2) & seq & "O" & "|" & counterO & "||" & "0016-20101012" & "|^" & rdrO("testbarcode") & "^^1|R|" & rdrO("dateorder") & "|||||N||||||||||||||O||||||" & Chr(13) & Chr(3)
                        firstStr = Chr(2) & CStr(seq) & "O|" & CStr(counterO) & "||" & rdrO("barcode") & "|^" & rdrO("testbarcode") & "^^1|R|" & rdrO("dateorder") & "|||||N||||||||||||||O||||||" & Chr(13) & Chr(3)
                        '0016-20101012
                        '3O|1||0016-20101012|^b4^^1|R|20110110100000|||||N||||||||||||||O||||||
                        indexOfSTX = firstStr.IndexOf(Chr(3))
                        secondStr = checkSumCalculator(Mid(firstStr, 2, indexOfSTX)) & Chr(13) & Chr(10)
                        singleO = firstStr & secondStr
                        strO.Add(singleO)
                    End If

                Loop

            End If
            '=====================
            Return strO
        Catch ex As Exception
            MessageBox.Show(ex.ToString & " | " & ex.TargetSite.Name, "Terjadi kesalahan " & "  ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        
    End Function
    Private Function checkSumCalculator(ByVal str As String) As String

        Dim array() As Byte = System.Text.Encoding.ASCII.GetBytes(str)
        Dim tempresult As String

        ' Display Bytes.
        Dim rslt As Integer
        rslt = 0
        For Each b As Byte In array
            rslt = rslt + b
        Next
        tempresult = CStr(Conversion.Hex(rslt))
        tempresult = tempresult.Substring(tempresult.Length - 2)
        Return tempresult

    End Function

    Private Function PaliocheckSumCalculator(ByVal str As String) As String

        Dim array() As Byte = System.Text.Encoding.ASCII.GetBytes(str)
        Dim tempresult As String

        ' Display Bytes.
        Dim rslt As Integer
        rslt = 0
        For Each b As Byte In array
            rslt = rslt + b
        Next
        tempresult = CStr(Conversion.Hex(rslt))
        tempresult = tempresult.Substring(tempresult.Length - 2)
        Return tempresult
    End Function
    '===============================================================
    'result code
    Public Sub CreateRequestForResult()
        'create request should be use this:
        ' <stx>2q|1|||||||||||F<stx>1F<CR><LF>
        Dim strQforR As String

        qforRCreate.Add(Chr(5))
        qforRCreate.Add(makingH)
        strQforR = Chr(2) & "2Q|1|||||||||||F" & Chr(2) & "1F" & Chr(13) & Chr(10)
        qforRCreate.Add(strQforR)
        ' WriteLogList(qforRCreate)
        seqQuery = 0

    End Sub

    Public Sub ManageResult(ByVal resultpalio As List(Of String))
        'entry result

        Dim barcode As String
        Dim sampleNo As String

        Dim isSampleNoMode As Boolean
        Dim isBarcodeMode As Boolean
        Dim requestDate As String
        Dim regexResult As String()

        Dim mystrucResult(resultpalio.Count - 1) As StrucResult


        isBarcodeMode = False
        isSampleNoMode = False

        CekLicense()

        Try

            Dim i As Integer
            i = 0
            Do While i < resultpalio.Count
                'bersihin string noise disini
                Dim strbersih As String
                'strbersih = orMcnToLis.Item(i).Replace() 

                regexResult = regex.Split(resultpalio.Item(i), "\|")
                If regexResult.ElementAt(0).Substring(regexResult.ElementAt(0).Length - 1) = "O" Then
                    'extract O message
                    Dim strSampleDetail As String
                    Dim palioSampleNo As String
                    Dim tubeDetail As String()
                    Dim testdetail As String()

                    palioSampleNo = regexResult.ElementAt(3)

                    strSampleDetail = regexResult.ElementAt(4)
                    testdetail = regex.Split(strSampleDetail, "\^")

                    mystrucResult(i).idpaliosampleResult = palioSampleNo
                    mystrucResult(i).idtestResult = testdetail.ElementAt(1)

                    'If testdetail.ElementAt(0) = "" Then
                    '    isSampleNoMode = True
                    '    isBarcodeMode = False
                    '    sampleNo = tubeDetail.ElementAt(1)
                    'Else
                    '    isSampleNoMode = False
                    '    isBarcodeMode = True
                    '    barcode = tubeDetail.ElementAt(0)
                    'End If

                ElseIf regexResult.ElementAt(0).Substring(regexResult.ElementAt(0).Length - 1) = "R" Then
                    'R line

                    ' 3R|1|C^b4^NONE^1|0.217299|mg/dl|0.000000:200.000000|N||F||admin|||Miura One3D
                    '  0  1  2              3     4      5                 6  7    8  910  11 
                    Dim testdetail As String()
                    Dim strtestdetail As String

                    strtestdetail = regexResult.ElementAt(2)
                    testdetail = regex.Split(strtestdetail, "\^")

                    If testdetail.ElementAt(1) = mystrucResult(i - 1).idtestResult Then
                        mystrucResult(i - 1).valueTestResult = regexResult.ElementAt(3)
                    End If

                    Dim testname As String
                    Dim value As String
                    Dim unit As String
                    Dim refrange As String
                    Dim resultAbnormalFlag As String
                    Dim resultStatus As String
                    Dim datecomplete As String

                    'testname = regexResult.ElementAt(2).Replace("^", "")
                    '========== correct already
                    value = mystrucResult(i - 1).valueTestResult
                    resultStatus = regexResult.ElementAt(7)
                    resultAbnormalFlag = regexResult.ElementAt(6)
                    unit = regexResult.ElementAt(4)
                    refrange = regexResult.ElementAt(5)
                    'datecomplete = regexResult.ElementAt(12).Substring(0, 14) 'sudah bersih
                    datecomplete = Format(Now, "yyyyMMddhhmmss")



                    Dim universaltest As String

                    universaltest = getTestIDforPalio(mystrucResult(i - 1).idtestResult)

                    Dim two_range As String()
                    Dim normal_val As String
                    Dim critical_val As String()

                    Dim normal_string As String = ""
                    Dim critical_string As String = ""


                    Dim lower_normal As String = ""
                    Dim upper_normal As String = ""


                    Dim lower_critical As String = ""
                    Dim upper_critical As String = ""

                    Dim strsource As String

                    strsource = refrange
                    two_range = regex.Split(strsource, "\:")
                    If strsource = "" Then
                        lower_normal = ""
                        lower_critical = ""
                        upper_normal = ""
                        upper_critical = ""
                    Else
                        normal_string = two_range.ElementAt(0)
                        critical_string = two_range.ElementAt(1)

                        If normal_string = "" Then
                            lower_normal = ""
                            upper_normal = ""
                            lower_critical = ""
                            upper_critical = ""
                        Else
                            normal_val = ""
                            lower_normal = two_range.ElementAt(0)
                            upper_normal = two_range.ElementAt(1)
                            lower_critical = ""
                            upper_critical = ""
                        End If

                    End If
                    '-------------------- manual abnormalflag because of unstable palio flag
                    If lower_normal.Length > 0 And upper_normal.Length > 0 Then
                        Dim nilai As Double
                        Dim bawah As Double
                        Dim atas As Double

                        nilai = 0
                        If value.Length > 0 Then
                            nilai = CDbl(value)
                        End If
                        bawah = 0
                        If lower_normal.Length > 0 Then
                            bawah = CDbl(lower_normal)
                        End If

                        atas = 0
                        If upper_normal.Length > 0 Then
                            atas = CDbl(upper_normal)
                        End If

                        Dim result As String
                        result = "A"
                        If nilai < bawah Then
                            result = "L"
                        End If

                        If nilai > atas Then
                            result = "H"
                        End If

                        If nilai < atas And nilai > bawah Then
                            result = "N"
                        End If

                        If nilai = atas Then
                            result = "N"
                        End If

                        If nilai = bawah Then
                            result = "N"
                        End If
                        resultAbnormalFlag = result
                        '-----------------------------------
                    End If



                    barcode = mystrucResult(i - 1).idpaliosampleResult
                    isBarcodeMode = True

                    Dim strsql As String
                    If isBarcodeMode Then
                        strsql = "update jobdetail " & _
                                 "set measurementvalue='" & value & "',resultstatus='" & resultStatus & "'" & _
                                 ",resultabnormalflag='" & resultAbnormalFlag & "'" & _
                                 ",datecomplete='" & datecomplete & "',referencerange='" & refrange & "'" & _
                                 ",machineorhuman='1',iduser='0',idmachine='" & idinstrument & "',unit='" & unit & "',status='" & FINISHSAMPLE & "' " & _
                                 ",lowernormal='" & lower_normal & "',uppernormal='" & upper_normal & "',lowercritical='" & lower_critical & "',uppercritical='" & upper_critical & "' " & _
                                 "where barcode='" & barcode & "' and universaltest='" & universaltest & "' "
                        'salah
                    ElseIf isSampleNoMode Then
                        strsql = "update jobdetail " & _
                                 "set measurementvalue='" & value & "',resultstatus='" & resultStatus & "'" & _
                                 ",resultabnormalflag='" & resultAbnormalFlag & "'" & _
                                 ",datecomplete='" & datecomplete & "',referencerange='" & refrange & "'" & _
                                 ",machineorhuman='1',iduser='0',idmachine='" & idinstrument & "',unit='" & unit & "',status='" & FINISHSAMPLE & "' " & _
                                 ",lowernormal='" & lower_normal & "',uppernormal='" & upper_normal & "',lowercritical='" & lower_critical & "',uppercritical='" & upper_critical & "' " & _
                                 " where sampleno='" & sampleNo & "' and universaltest='" & testname & "'"
                    End If

                    objconn.buildConn()
                    objconn.ExecuteNonQuery(strsql)
                    objconn.CloseConn()


                End If

                i = i + 1
            Loop
            UpdateJobStatus()
        Catch ex As Exception
            MessageBox.Show(ex.ToString & " | " & ex.TargetSite.Name, "Terjadi kesalahan " & "  ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try




    End Sub
    Private Function getTestIDforPalio(ByVal paliotestname As String) As String
        Dim qcconn As New clsGreConnect
        Dim qcsql As String

        Dim resultUniversalName As String


        qcsql = "select testtype.analyzertestname,testtype.id,palioinstrument.idtest,palioinstrument.testbarcode from palioinstrument,testtype where palioinstrument.idtest=testtype.id and palioinstrument.testbarcode='" & paliotestname & "'"
        qcconn.buildConn()

        Dim getnameCommand As New SqlClient.SqlCommand(qcsql, qcconn.grecon)
        Dim getnameReader As SqlDataReader = getnameCommand.ExecuteReader(CommandBehavior.CloseConnection)
        If getnameReader.HasRows Then
            Do While getnameReader.Read

                resultUniversalName = getnameReader("analyzertestname")
            Loop
        End If
        getnameReader.Close()
        qcconn.CloseConn()

        Return resultUniversalName

    End Function
    Private Sub UpdateJobStatus() 'penting update status
        Dim strsql As String
        Dim pormat As String
        pormat = "yyyy-MM-dd HH:mm:ss"

        Dim datenow As Date
        datenow = Now

        strsql = "select job.labnumber from job where job.status='" & NEWSAMPLE & "' and job.labnumber not in (select distinct labnumber from jobdetail where status<>'" & FINISHSAMPLE & "' and active='" & ACTIVESAMPLE & "')"

        Dim ogreupdate As New clsGreConnect
        ogreupdate.buildConn()
        Dim cmdupdate As New SqlClient.SqlCommand(strsql, ogreupdate.grecon)
        Dim rdrupdate As SqlDataReader = cmdupdate.ExecuteReader(CommandBehavior.CloseConnection)
        If rdrupdate.HasRows Then
            Do While rdrupdate.Read
                'update job

                Dim squ As String
                squ = "update job set status='" & FINISHSAMPLE & "',datefinish='" & datenow.ToString(pormat) & "' where labnumber='" & rdrupdate("labnumber") & "'"
                Dim ogrejob As New clsGreConnect
                ogrejob.buildConn()
                ogrejob.ExecuteNonQuery(squ)
                ogrejob.CloseConn()
            Loop
        End If
        ogreupdate.CloseConn()
    End Sub



End Class
