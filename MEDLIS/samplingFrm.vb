﻿Imports System.Data
Imports System.Data.SqlClient
Imports GenCode128

Imports Zen.Barcode
Imports System.Drawing
Imports System.IO
Imports System.Windows.Forms
Imports System.Drawing.Text

Public Class samplingFrm
    Dim greobject As clsGreConnect
    Dim dtable As DataTable
    Dim selectedLabNumber As String
    Dim selectedPatient As Integer

    Dim selsampleno As String
    Dim selTestName As String
    Dim randomvalue, upperbound, lowerbound As Integer

    Dim isHemaTestAvailable As Boolean = False
    Dim patientName As String

    Dim xpos As Integer
    Dim ypos As Integer

    Dim patientPhone As String
    Dim patientaddress As String

    Dim findsampleno As String
    Dim findbarcode As String
    Dim alreadyhasidentity As Boolean

    Dim fpath As String
    Dim fprint As String
    Dim fInstrument As String

    Dim jmlprintbarcode As Integer

    Dim bcMode As Boolean
    Dim snMode As Boolean
    Dim originalbarcodeimage As Bitmap

    '=======================
    Dim bar_type As String
    Dim bar_height As Integer
    Dim bar_res As Single
    Dim bar_y As Integer
    Dim bar_label_size As Integer
    Dim bar_skala As Integer
    '=======================

    '=====
    Dim listHema As New List(Of String)



    Private Sub setTextSampleNo()
        ' Dim result As Boolean
        Dim str As String
        'str = "SELECT distinct CAST(coalesce(sampleno, '0') AS integer) as sampleno from jobdetail order by sampleno desc limit 1"
        str = "SELECT distinct top 5 CAST(coalesce(sampleno, '0') AS integer) as sampleno from jobdetail order by sampleno desc"
        Dim ocek As New clsGreConnect
        ocek.buildConn()

        Dim cmd As New SqlClient.SqlCommand(str, ocek.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("sampleno")) Then
                    txtSampleNo.Text = CStr(CInt(rdr("sampleno")) + 1)

                    txtSampleNo.Focus()
                    txtSampleNo.SelectionStart = txtSampleNo.SelectionStart + txtSampleNo.TextLength - 1
                    txtSampleNo.SelectionLength = 1
                End If
            Loop

        End If

        rdr.Close()
        cmd.Dispose()
        cmd = Nothing
        ocek.CloseConn()
        ocek = Nothing

    End Sub
    Private Sub setTextBarcode()

        ' Dim result As Boolean
        Dim str As String
        str = "select barcode from jobdetail where barcode is not null order by barcode desc limit 1"
        Dim ocek As New clsGreConnect
        ocek.buildConn()

        Dim cmd As New SqlClient.SqlCommand(str, ocek.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("barcode")) Then
                    txtBarcode.Text = rdr("barcode")
                    txtBarcode.SelectionStart = txtSampleNo.TextLength - 1
                    txtBarcode.SelectionLength = 1
                End If
            Loop
        End If
        rdr.Close()
        cmd.Dispose()
        cmd = Nothing
        ocek.CloseConn()
        ocek = Nothing

    End Sub

    Private Sub samplingFrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        isSamplingWinOpen = 0
    End Sub
    Private Sub jobDetailFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        isSamplingWinOpen = 1
        Icon = New System.Drawing.Icon("medlis2.ico")
        ReadLicense()
        Get_Bar_Parameter()

        btnPrint.Visible = False
        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If

        'FillCBSpeciment()
        Joblist()
        ContructDgv()

        '-- read mode baca
        fpath = System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData, "modesample.txt")
        If ReadInsProperty() = False Then
            MessageBox.Show("Please check sampling read mode, Please check App Setting")
            WriteInsPropertyDefault()
            bcMode = False
            snMode = True
            lblMode.Text = "Sample No. Mode"
        Else

            If fInstrument = "barcodeMode" Then
                bcMode = True
                snMode = False
                lblMode.Text = "Barcode Mode"
                RdoBarcodeMode.Checked = True
            ElseIf fInstrument = "samplenoMode" Then
                bcMode = False
                snMode = True
                rdoSampleMode.Checked = True
                lblMode.Text = "Sample No. Mode"
            End If

        End If
        '----

        '===============
        fprint = System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData, "printcount.txt")
        If ReadPrintProperty() = False Then
            MessageBox.Show("Silahkan cek setting jumlah print, Please check App Setting")
            'WriteInsPropertyDefault()
            jmlprintbarcode = 1
            lblprint.Text = lblprint.Text & " " & jmlprintbarcode & "x"
        Else

            lblprint.Text = lblprint.Text & " " & jmlprintbarcode & "x"

        End If
        '----

        '===============


        If rdoSampleMode.Checked Then
            ShowLastFiveSample()   'bagus penting
        End If

        '===========ADD HEMA LIST
        listHema.Add("LYM%")
        listHema.Add("MON%")
        listHema.Add("GRA%")
        listHema.Add("MCV")
        listHema.Add("RDW-CV")
        listHema.Add("RDW-SD")
        listHema.Add("MPV")
        listHema.Add("PDW")
        listHema.Add("LYM#")
        listHema.Add("MON#")
        listHema.Add("GRA#")
        listHema.Add("HCT")
        listHema.Add("MCH")
        listHema.Add("MCHC")
        listHema.Add("PCT")

        ' setBarcodeText()
        ' setTextSampleNo()

    End Sub
    Private Sub ShowLastFiveSample()
        Dim strsql As String
        'strsql = "SELECT top 5 distinct CAST(coalesce(sampleno, '0') AS integer) as lastfive from jobdetail order by lastfive desc"
        strsql = "select distinct top 5 CAST(coalesce(sampleno, '0') AS integer) as lastfive from jobdetail order by lastfive desc"
        Dim ogre As New clsGreConnect
        ogre.buildConn()
        lblprevsamp.Text = ""
        Dim cmd As New SqlClient.SqlCommand(strsql, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                lblprevsamp.Text = lblprevsamp.Text & rdr("lastfive") & "  "
            Loop

        End If
        cmd.Dispose()
        ogre.CloseConn()

    End Sub

    Public Sub WriteInsPropertyDefault()
        Dim insName As String

        insName = "Mode=samplenoMode"


        Dim objWriter As New System.IO.StreamWriter(fpath)
        objWriter.WriteLine(insName)
        objWriter.Close()
    End Sub

    Private Function ReadInsProperty() As Boolean

        Dim InsName As String
        Dim result As Boolean
        Dim getstr As String
        Dim position As Integer
        If System.IO.File.Exists(fpath) Then
            Try
                Dim lines() As String = IO.File.ReadAllLines(fpath)
                Dim k As Integer
                k = 0
                For Each line As String In lines

                    position = InStr(line, "=")
                    For i = position To Len(line) - 1
                        getstr = getstr + line.Substring(i, 1)
                    Next
                    InsName = getstr
                    getstr = ""
                Next

                fInstrument = InsName
                result = True
            Catch ex As Exception
                MessageBox.Show("Please specify instrument read mode", "File not found", MessageBoxButtons.OK)
            End Try
        Else
            result = False
        End If
        Return result

    End Function

    Private Function ReadPrintProperty() As Boolean

        Dim InsName As String
        Dim result As Boolean
        Dim getstr As String
        Dim position As Integer
        If System.IO.File.Exists(fpath) Then
            Try
                Dim lines() As String = IO.File.ReadAllLines(fprint)
                Dim k As Integer
                k = 0
                For Each line As String In lines

                    position = InStr(line, "=")
                    For i = position To Len(line) - 1
                        getstr = getstr + line.Substring(i, 1)
                    Next
                    InsName = getstr
                    getstr = ""
                Next

                jmlprintbarcode = CInt(InsName)
                result = True
            Catch ex As Exception
                MessageBox.Show("Tentukan Jumlah print barcode di setting", "File not found", MessageBoxButtons.OK)
            End Try
        Else
            jmlprintbarcode = 1
            result = False
        End If
        Return result

    End Function

    Private Sub ContructDgv()


        If dgvdetail.Columns.Contains("Pilih") Then
            dgvdetail.Columns.Clear()
        End If

        dgvdetail.ColumnCount = 1
        'dgvdetail.Columns(0).Name = "SampleNo"
        'dgvdetail.Columns(0).HeaderText = "Sample Number"
        'dgvdetail.Columns(0).Width = 80

        'dgvdetail.Columns(1).Name = "Barcode"
        'dgvdetail.Columns(1).HeaderText = "Barcode"
        'dgvdetail.Columns(1).Width = 80

        dgvdetail.Columns(0).Name = "TestName"
        dgvdetail.Columns(0).HeaderText = "Nama Test"
        dgvdetail.Columns(0).Width = 90

        Dim checkColumn As New DataGridViewCheckBoxColumn
        With checkColumn
            .Name = "Pilih"
            .HeaderText = "Pilih"
            .Width = 90
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            .FlatStyle = FlatStyle.Standard
            .DisplayIndex = 1
        End With
        dgvdetail.Columns.Add(checkColumn)
        dgvdetail.AllowUserToAddRows = False
        dgvdetail.RowHeadersVisible = False
    End Sub


    Private Sub Joblist()

        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If


        Dim strsql As String
        If Trim(txtlabsearch.Text) = "" Then
            strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.id as patientid,job.labnumber,patient.address, patient.phone,datereceived from job,patient where patient.id=job.idpasien and job.paymentstatus<>'" & NOTPAID & "' and job.labnumber in (select distinct labnumber from jobdetail where status='" & NEWSAMPLE & "' and active='" & ACTIVESAMPLE & "') order by datereceived desc"
        Else
            Dim src As String
            src = Trim(txtlabsearch.Text)
            strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.id as patientid,job.labnumber,patient.address, patient.phone,datereceived from job,patient where patient.id=job.idpasien and job.paymentstatus<>'" & NOTPAID & "' and job.labnumber='" & src & "' and job.labnumber in (select distinct labnumber from jobdetail where status='" & NEWSAMPLE & "' and active='" & ACTIVESAMPLE & "') order by datereceived desc"
        End If

        greobject = New clsGreConnect
        greobject.buildConn()

        dtable = New DataTable
        dtable = greobject.ExecuteQuery(strsql)

        'dgvjoblist = New DataGridView
        dgvJoblist.DataSource = dtable
        Dim j As Integer
        For j = 0 To dgvJoblist.Columns.Count - 1
            dgvJoblist.Columns(j).Visible = False
        Next

        dgvJoblist.Columns("labnumber").Visible = True
        dgvJoblist.Columns("labnumber").HeaderText = "Lab Number"
        dgvJoblist.Columns("labnumber").Width = 160

        dgvJoblist.Columns("name").Visible = True
        dgvJoblist.Columns("name").HeaderText = "Nama pasien"
        dgvJoblist.Columns("name").Width = 180

        dgvJoblist.Columns("address").Visible = True
        dgvJoblist.Columns("address").HeaderText = "Alamat"
        dgvJoblist.Columns("address").Width = 170

        dgvJoblist.Columns("datereceived").Visible = True
        dgvJoblist.Columns("datereceived").HeaderText = "Tanggal"
        dgvJoblist.Columns("datereceived").Width = 130


        Dim buttonColumn As New DataGridViewButtonColumn()
        With buttonColumn
            .Name = "Detail"
            .HeaderText = "Detail"
            .Width = 90
            .Text = "Detail"
            .UseColumnTextForButtonValue = True
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            .FlatStyle = FlatStyle.Standard
            .CellTemplate.Style.BackColor = Color.Yellow
            .DisplayIndex = 0
        End With
        dgvJoblist.Columns.Add(buttonColumn)
        dgvJoblist.AllowUserToAddRows = False
        dgvJoblist.RowHeadersVisible = False
    End Sub

    Private Sub dgvJoblist_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvJoblist.CellContentClick
        If e.RowIndex < 0 OrElse Not e.ColumnIndex = dgvJoblist.Columns("Detail").Index Then
            Return

        Else

            If Not dgvJoblist.CurrentRow.IsNewRow Then
                chkPilihSemua.CheckState = CheckState.Unchecked
                selectedLabNumber = dgvJoblist.Item("labnumber", dgvJoblist.CurrentRow.Index).Value
                selectedPatient = dgvJoblist.Item("patientid", dgvJoblist.CurrentRow.Index).Value
                patientName = dgvJoblist.Item("name", dgvJoblist.CurrentRow.Index).Value
                patientaddress = dgvJoblist.Item("address", dgvJoblist.CurrentRow.Index).Value
                patientPhone = dgvJoblist.Item("phone", dgvJoblist.CurrentRow.Index).Value
                selsampleno = ""
                cmdFinish.Enabled = True
                PopulatedgvDetail()
                ShowDetail()
                '   isHemaSampleAvailable()

                If alreadyhasidentity = False Then
                    setTextSampleNo()
                    setBarcodeText()
                End If
                
                RdoBarcodeMode.Enabled = True
                rdoSampleMode.Enabled = True
            End If

        End If
    End Sub
    Private Sub ShowDetail()
        alreadyhasidentity = False

        txtname.ReadOnly = False
        txtaddress.ReadOnly = False
        txttelepon.ReadOnly = False
        txtLabnumber.ReadOnly = False

        txtname.Text = patientName
        txtaddress.Text = patientaddress
        txttelepon.Text = patientPhone
        txtLabnumber.Text = selectedLabNumber


        txtname.ReadOnly = True
        txtaddress.ReadOnly = True
        txttelepon.ReadOnly = True
        txtLabnumber.ReadOnly = True

        findbarcode = ""
        findsampleno = ""
        txtBarcode.Enabled = True
        txtBarcode.ReadOnly = False
        txtSampleNo.Enabled = True
        txtSampleNo.ReadOnly = False
        txtBarcode.Text = ""
        txtSampleNo.Text = ""


        Dim strsql As String
        Dim grefind As New clsGreConnect
        grefind.buildConn()
        'strsql = "select distinct sampleno,barcode from jobdetail where labnumber='" & selectedLabNumber & "'"
        strsql = "select top 1 * from jobdetail where labnumber='" & selectedLabNumber & "' and (barcode is not null or sampleno is not null)"



        Dim cmd As New SqlClient.SqlCommand(strsql, grefind.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("sampleno")) Then
                    findsampleno = Trim(rdr("sampleno"))
                Else
                    findsampleno = ""
                End If

                If Not IsDBNull(rdr("barcode")) Then
                    findbarcode = Trim(rdr(("barcode")))
                Else
                    findbarcode = ""
                End If
            Loop
        End If

        If findsampleno <> "" Or findbarcode <> "" Then
            alreadyhasidentity = True
        End If

        If findbarcode.Length > 0 Then
            txtBarcode.Text = findbarcode
            txtBarcode.ReadOnly = True
        End If
        If findsampleno.Length > 0 Then
            txtSampleNo.Text = findsampleno
            txtSampleNo.ReadOnly = True
        End If

    End Sub

    Private Sub PopulatedgvDetail()
        Dim objnewcon As New clsGreConnect
        Dim strsql As String

        'If dgvdetail.RowCount > 0 Then
        '    dgvdetail.Rows.Clear()
        'End If


        'strsql = "select distinct sampleno,barcode from jobdetail where labnumber='" & selectedLabNumber & "' and active='" & ACTIVESAMPLE & "' and status='0'"
        'strsql = "select * from jobdetail where labnumber='" & selectedLabNumber & "' and active='" & ACTIVESAMPLE & "' and status='0'"

        'newcon.buildConn()

        'Dim thecommand As New SqlClient.SqlCommand(strsql, newcon.grecon)
        'Dim Reader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)

        'Do While Reader.Read
        '    'Dim rowdata = New String() {Reader("sampleno"), Reader("barcode"), GetUniversalTestName(Reader("sampleno"), selectedLabNumber)}
        '    Dim rowdata = New String() {GetUniversalTestName(selectedLabNumber)}
        '    dgvdetail.Rows.Add(rowdata)
        'Loop

        'Reader.Close()
        'thecommand = Nothing

        'newcon.CloseConn()
        'newcon = Nothing
        '--------------------------
        ' Dim greobject As New clsGreConnect
        objnewcon.buildConn()
        dtable = New DataTable
        strsql = "select jobdetail.id,jobdetail.universaltest,jobdetail.idtesttype,testtype.uf1 from jobdetail,testtype where jobdetail.labnumber='" & selectedLabNumber & "' and jobdetail.active='" & ACTIVESAMPLE & "' and jobdetail.status='" & NEWSAMPLE & "' and jobdetail.idtesttype=testtype.id"
        dtable = objnewcon.ExecuteQuery(strsql)

        'dgvtestname = New DataGridView
        dgvdetail.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvdetail.Columns.Count - 1
            dgvdetail.Columns(j).Visible = False
        Next


        dgvdetail.Columns("uf1").Visible = True
        dgvdetail.Columns("uf1").HeaderText = "Jenis Sample"
        dgvdetail.Columns("uf1").Width = 200

        For Each row As DataGridViewRow In dgvdetail.Rows
            If Not row.IsNewRow Then
                If Not row.Cells("uf1").Value Is DBNull.Value Then

                    If row.Cells("uf1").Value = "1" Then row.Cells("uf1").Value = "Serum"
                    If row.Cells("uf1").Value = "2" Then row.Cells("uf1").Value = "Urine"
                    If row.Cells("uf1").Value = "3" Then row.Cells("uf1").Value = "Plasma"
                    If row.Cells("uf1").Value = "4" Then row.Cells("uf1").Value = "Gastric Juice"
                    If row.Cells("uf1").Value = "5" Then row.Cells("uf1").Value = "Ascites"
                    If row.Cells("uf1").Value = "6" Then row.Cells("uf1").Value = "CSF"
                    If row.Cells("uf1").Value = "7" Then row.Cells("uf1").Value = "Whole blood"
                    If row.Cells("uf1").Value = "8" Then row.Cells("uf1").Value = "Feses"
                    If row.Cells("uf1").Value = "9" Then row.Cells("uf1").Value = "Other"

                End If
            End If
        Next

        If Not dgvdetail.Columns.Contains("chk") Then
            Dim chk As New DataGridViewCheckBoxColumn()

            dgvdetail.Columns.Add(chk)
            chk.HeaderText = "SELECT"
            chk.Width = 80
            chk.Name = "chk"

            dgvdetail.Columns("chk").DisplayIndex = 0
            dgvdetail.Columns("chk").Name = "chk"
        End If

        dgvdetail.Columns("chk").Visible = True
        dgvdetail.Columns("universaltest").Visible = True
        dgvdetail.Columns("universaltest").HeaderText = "Test Name"
        dgvdetail.Columns("universaltest").Width = 200
        dgvdetail.Columns("universaltest").ReadOnly = True

        dgvdetail.Columns("uf1").ReadOnly = True




        'dgvdetail.Columns("universaltest").Visible = True
        'dgvdetail.Columns("universaltest").HeaderText = "Test Name"
        'dgvdetail.Columns("universaltest").Width = 200

        'dgvdetail.Columns("analyzertestname").Visible = True
        'dgvdetail.Columns("analyzertestname").HeaderText = "Analyzer Test Name"
        'dgvdetail.Columns("analyzertestname").Width = 200
        objnewcon.CloseConn()
        chkPilihSemua.CheckState = CheckState.Checked

    End Sub
    'Private Function GetUniversalTestName(ByVal sampleno As String, ByVal labnumber As String) As String
    Private Function GetUniversalTestName(ByVal labnumber As String) As String
        Dim strfinder As String
        Dim strresult As String
        Dim testcon As New clsGreConnect
        testcon.buildConn()
        'strfinder = "select universaltest from jobdetail where sampleno='" & sampleno & "' and labnumber='" & labnumber & "'"
        strfinder = "select universaltest from jobdetail where labnumber='" & labnumber & "'"
        Dim thecommand As New SqlClient.SqlCommand(strfinder, testcon.grecon)
        Dim theReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)
        strresult = ""
        Do While theReader.Read
            strresult = strresult & " " & theReader("universaltest")
        Loop
        thecommand = Nothing
        theReader.Close()
        testcon.CloseConn()
        testcon = Nothing

        Return strresult
    End Function

    Private Sub dgvdetail_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvdetail.CellContentClick
      

        'If Not (dgvdetail.CurrentRow.IsNewRow) Then
        '    If Not IsDBNull(dgvdetail.Item("sampleno", e.RowIndex).Value) Then
        '        If TypeOf dgvdetail.Rows(e.RowIndex).Cells(e.ColumnIndex) Is DataGridViewCheckBoxCell Then
        '            Dim dgvCheckBoxCell As DataGridViewCheckBoxCell = dgvdetail.Rows(e.RowIndex).Cells(e.ColumnIndex)
        '            'Commit the data to the datasouce.
        '            dgvdetail.CommitEdit(DataGridViewDataErrorContexts.Commit)
        '            Dim checked As Boolean = CType(dgvCheckBoxCell.Value, Boolean)
        '            If checked Then

        '                'selTestName = dgvdetail.Item("universaltestname", e.RowIndex).Value
        '                'NormalizedSample(selTestName)
        '                'txtSampleNo.Text = selsampleno
        '                'txtBarcode.Text = dgvdetail.Item("barcode", e.RowIndex).Value
        '            End If


        '        End If
        '    End If

        'End If
    End Sub
    'Private Function GetSpecimen() As String
    '    Dim result As String
    '    If cbSpeciment.Text = "Serum" Then
    '        result = "1"
    '    ElseIf cbSpeciment.Text = "Urine" Then
    '        result = "2"
    '    ElseIf cbSpeciment.Text = "Plasma" Then
    '        result = "3"
    '    ElseIf cbSpeciment.Text = "Gastric Juice" Then
    '        result = "4"
    '    ElseIf cbSpeciment.Text = "Ascites" Then
    '        result = "5"
    '    ElseIf cbSpeciment.Text = "CSF" Then
    '        result = "6"
    '    ElseIf cbSpeciment.Text = "Other" Then
    '        result = "7"
    '    End If
    '    Return result
    'End Function
    'Private Sub FillCBSpeciment()
    '    cbSpeciment.Items.Add("Serum") '1
    '    cbSpeciment.Items.Add("Urine")
    '    cbSpeciment.Items.Add("Whole blood")
    '    cbSpeciment.Items.Add("Feses")
    '    cbSpeciment.Items.Add("Plasma")
    '    cbSpeciment.Items.Add("Gastric Juice")
    '    cbSpeciment.Items.Add("Ascites")
    '    cbSpeciment.Items.Add("CSF")
    '    cbSpeciment.Items.Add("Other")
    '    cbSpeciment.SelectedIndex = 0
    'End Sub
    Private Sub NormalizedSample(ByVal sampleno As String)

        Dim i As Integer
        Dim strsql As String
        Dim objcon As New clsGreConnect
        objcon.buildConn()

        strsql = "select distinct sampleno,barcode from jobdetail where labnumber='" & selectedLabNumber & "' and sampleno='" & sampleno & "'"
        Dim clearCommand As New SqlClient.SqlCommand(strsql, objcon.grecon)
        Dim clearReader As SqlDataReader = clearCommand.ExecuteReader(CommandBehavior.CloseConnection)
        Do While clearReader.Read

            For i = 0 To dgvdetail.Rows.Count - 1
                If Not IsDBNull(clearReader("sampleno")) Then
                    If dgvdetail.Item("sampleno", i).Value <> clearReader("sampleno") Then
                        dgvdetail.Rows(dgvdetail.Item("sampleno", i).RowIndex).ReadOnly = False
                        dgvdetail.Rows(dgvdetail.Item("sampleno", i).RowIndex).DefaultCellStyle.BackColor = Color.White
                        dgvdetail.Rows(dgvdetail.Item("sampleno", i).RowIndex).Cells(3).Value = False
                        'sampai disini
                    End If
                End If
            Next
        Loop
        'bagus
        clearReader.Close()
        clearCommand.Dispose()
        objcon.CloseConn()

    End Sub


    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        RdoBarcodeMode.Enabled = True
        rdoSampleMode.Enabled = True
        Dim i As Integer

        If RdoBarcodeMode.Checked = True Then
            bcMode = True
        ElseIf rdoSampleMode.Checked = True Then
            snMode = True
        End If

        If bcMode = True Then
            ' GenerateBarcode() 'generate barcode
            For i = 0 To jmlprintbarcode - 1
                PrintDocument1.Print()
            Next
            'print barcode
        ElseIf snMode = True Then
            '  GenerateLabel()
            For i = 0 To jmlprintbarcode - 1
                PrintDocument1.Print()
            Next

        End If



    End Sub

    Private Sub GenerateLabel()
        lblnosample.Text = "sn: " & Trim(txtSampleNo.Text) & "  ln: " & selectedLabNumber & vbCrLf & "date: " & Format(Now, "yyyyMMdd")
    End Sub


    Private Sub GenerateBarcode()
        Try

            Dim myimg As Image

            Dim strtoprint As String
            Dim weight As Integer
            Dim fontsize As Integer

            'weight = updownWeight.Value
            'fontsize = upDownFontSize.Value

            strtoprint = Trim(txtBarcode.Text)

            myimg = Code128Rendering.MakeBarcodeImage(strtoprint, weight, True)
            pictBarcode.Image = myimg

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try

    End Sub
    Private Sub generateBarcode2()
        Try
            Dim ft As String
            Dim bh As Integer
            Dim bartype As String
            Dim scala As Integer
            Dim ftsize As Integer


            If txtBarcode.Text.Length <> 0 Then


                Dim strsetting As String
                strsetting = "select * from setting where name='barcode'"
                Dim ogre As New clsGreConnect
                ogre.buildConn()

                Dim cmd As New SqlClient.SqlCommand(strsetting, ogre.grecon)
                Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                If rdr.HasRows Then
                    Do While rdr.Read
                        ' ft = rdr("font")
                        bh = rdr("height")
                        bartype = rdr("value")
                        scala = rdr("scala")
                        ftsize = rdr("ftsize")
                        xpos = rdr("x")
                        ypos = rdr("y")
                    Loop

                End If
                cmd.Dispose()
                ogre.CloseConn()



                Dim jenisfont As New Font(ft, ftsize, Font.Style.Regular)
                'code 39 43 char
                'code 93 48 char
                'code 128 108 char

                Dim maxbarHight As Integer
                Dim x, y As Integer
                x = xpos  'CInt(cbx.Text.Trim)
                y = ypos 'CInt(cby.Text.Trim)


                Dim originalbarcodeimage As Bitmap
                Dim CodeBitmap As Bitmap
                Dim ImagewithText As Bitmap
                Dim gr As Graphics

                maxbarHight = bh

                If bartype = "Code 128" Then
                    Dim barcode128 As Code128BarcodeDraw = BarcodeDrawFactory.Code128WithChecksum

                    originalbarcodeimage = barcode128.Draw(txtBarcode.Text.Trim.ToUpper, maxbarHight, scala)
                    originalbarcodeimage.SetResolution(203.0F, 203.0F)

                    ImagewithText = New Bitmap(originalbarcodeimage.Width, originalbarcodeimage.Height + 225)
                    ImagewithText.SetResolution(203.0F, 203.0F)

                    gr = Graphics.FromImage(ImagewithText)
                    gr.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
                    gr.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
                    gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
                    gr.TextRenderingHint = TextRenderingHint.AntiAlias

                    Dim myRectangle As New RectangleF(0, 0, originalbarcodeimage.Width, 50)
                    gr.DrawString(patientName, jenisfont, Brushes.Black, myRectangle)
                    gr.DrawImageUnscaled(originalbarcodeimage, x, y)
                    'gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, 0, originalbarcodeimage.Width, 100))
                    gr.DrawString(txtBarcode.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, originalbarcodeimage.Height + 40, originalbarcodeimage.Width + 10, 50))
                    'Printer.
                    pictBarcode.Image = ImagewithText

                    'CodeBitmap = New Bitmap(pic.Image)

                    CodeBitmap = New Bitmap(ImagewithText)

                    CodeBitmap.SetResolution(203.0F, 203.0F)
                    CodeBitmap.Save(Application.StartupPath & "\" & "001.png")
                    btnPrint.Enabled = True
                ElseIf bartype = "Code EAN 13" Then
                    Dim barcode128 As CodeEan13BarcodeDraw = BarcodeDrawFactory.CodeEan13WithChecksum

                    originalbarcodeimage = barcode128.Draw(txtBarcode.Text.Trim.ToUpper, maxbarHight, scala)
                    originalbarcodeimage.SetResolution(203.0F, 203.0F)

                    ImagewithText = New Bitmap(originalbarcodeimage.Width, originalbarcodeimage.Height + 225)
                    ImagewithText.SetResolution(203.0F, 203.0F)

                    gr = Graphics.FromImage(ImagewithText)
                    gr.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
                    gr.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
                    gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
                    gr.TextRenderingHint = TextRenderingHint.AntiAlias

                    Dim myRectangle As New RectangleF(0, 0, originalbarcodeimage.Width, 50)
                    gr.DrawString(patientName, jenisfont, Brushes.Black, myRectangle)
                    gr.DrawImageUnscaled(originalbarcodeimage, x, y)
                    'gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, 0, originalbarcodeimage.Width, 100))
                    gr.DrawString(txtBarcode.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, originalbarcodeimage.Height + 40, originalbarcodeimage.Width + 10, 50))
                    'Printer.
                    pictBarcode.Image = ImagewithText

                    'CodeBitmap = New Bitmap(pic.Image)

                    CodeBitmap = New Bitmap(ImagewithText)

                    CodeBitmap.SetResolution(203.0F, 203.0F)
                    CodeBitmap.Save(Application.StartupPath & "\" & "001.png")
                    btnPrint.Enabled = True

                ElseIf bartype = "Code EAN 8" Then
                    Dim barcode128 As CodeEan8BarcodeDraw = BarcodeDrawFactory.CodeEan8WithChecksum

                    originalbarcodeimage = barcode128.Draw(txtBarcode.Text.Trim.ToUpper, maxbarHight, scala)
                    originalbarcodeimage.SetResolution(203.0F, 203.0F)

                    ImagewithText = New Bitmap(originalbarcodeimage.Width, originalbarcodeimage.Height + 225)
                    ImagewithText.SetResolution(203.0F, 203.0F)

                    gr = Graphics.FromImage(ImagewithText)
                    gr.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
                    gr.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
                    gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
                    gr.TextRenderingHint = TextRenderingHint.AntiAlias

                    Dim myRectangle As New RectangleF(0, 0, originalbarcodeimage.Width, 50)
                    gr.DrawString(patientName, jenisfont, Brushes.Black, myRectangle)
                    gr.DrawImageUnscaled(originalbarcodeimage, x, y)
                    'gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, 0, originalbarcodeimage.Width, 100))
                    gr.DrawString(txtBarcode.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, originalbarcodeimage.Height + 40, originalbarcodeimage.Width + 10, 50))
                    'Printer.
                    pictBarcode.Image = ImagewithText

                    'CodeBitmap = New Bitmap(pic.Image)

                    CodeBitmap = New Bitmap(ImagewithText)

                    CodeBitmap.SetResolution(203.0F, 203.0F)
                    CodeBitmap.Save(Application.StartupPath & "\" & "001.png")
                    btnPrint.Enabled = True
                    btnPrint.Enabled = True
                ElseIf bartype = "Code 39" Then
                    Dim barcode128 As Code39BarcodeDraw = BarcodeDrawFactory.Code39WithChecksum

                    originalbarcodeimage = barcode128.Draw(txtBarcode.Text.Trim.ToUpper, maxbarHight, scala)
                    originalbarcodeimage.SetResolution(203.0F, 203.0F)

                    ImagewithText = New Bitmap(originalbarcodeimage.Width, originalbarcodeimage.Height + 225)
                    ImagewithText.SetResolution(203.0F, 203.0F)

                    gr = Graphics.FromImage(ImagewithText)
                    gr.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
                    gr.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
                    gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
                    gr.TextRenderingHint = TextRenderingHint.AntiAlias

                    Dim myRectangle As New RectangleF(0, 0, originalbarcodeimage.Width, 50)
                    gr.DrawString(patientName, jenisfont, Brushes.Black, myRectangle)
                    gr.DrawImageUnscaled(originalbarcodeimage, x, y)
                    'gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, 0, originalbarcodeimage.Width, 100))
                    gr.DrawString(txtBarcode.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, originalbarcodeimage.Height + 40, originalbarcodeimage.Width + 10, 50))
                    'Printer.
                    pictBarcode.Image = ImagewithText

                    'CodeBitmap = New Bitmap(pic.Image)

                    CodeBitmap = New Bitmap(ImagewithText)

                    CodeBitmap.SetResolution(203.0F, 203.0F)
                    CodeBitmap.Save(Application.StartupPath & "\" & "001.png")
                    btnPrint.Enabled = True
                    btnPrint.Enabled = True
                ElseIf bartype = "Code 93" Then
                    Dim barcode128 As Code93BarcodeDraw = BarcodeDrawFactory.Code93WithChecksum

                    originalbarcodeimage = barcode128.Draw(txtBarcode.Text.Trim.ToUpper, maxbarHight, scala)
                    originalbarcodeimage.SetResolution(203.0F, 203.0F)

                    ImagewithText = New Bitmap(originalbarcodeimage.Width, originalbarcodeimage.Height + 225)
                    ImagewithText.SetResolution(203.0F, 203.0F)

                    gr = Graphics.FromImage(ImagewithText)
                    gr.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
                    gr.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
                    gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
                    gr.TextRenderingHint = TextRenderingHint.AntiAlias

                    Dim myRectangle As New RectangleF(0, 0, originalbarcodeimage.Width, 50)
                    gr.DrawString(patientName, jenisfont, Brushes.Black, myRectangle)
                    gr.DrawImageUnscaled(originalbarcodeimage, x, y)
                    'gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, 0, originalbarcodeimage.Width, 100))
                    gr.DrawString(txtBarcode.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, originalbarcodeimage.Height + 40, originalbarcodeimage.Width + 10, 50))
                    'Printer.
                    pictBarcode.Image = ImagewithText

                    'CodeBitmap = New Bitmap(pic.Image)

                    'CodeBitmap = New Bitmap(ImagewithText)

                    'CodeBitmap.SetResolution(203.0F, 203.0F)
                    'CodeBitmap.Save(Application.StartupPath & "\" & "001.png")
                    btnPrint.Enabled = True
                End If





            End If
            'Catch ex As Exception
            '    MsgBox(ex.Message)
            'End Try
            Exit Sub
ErrMgr:
            Select Case Err.Number

                Case Else
                    MsgBox(Err.Number)
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try

    End Sub

    Private Function GetSpecimen(ByVal strspec As String) As String
        Dim result As String
        If strspec = "Serum" Then
            result = "1"
        ElseIf strspec = "Urine" Then
            result = "2"
        ElseIf strspec = "Plasma" Then
            result = "3"
        ElseIf strspec = "Gastric Juice" Then
            result = "4"
        ElseIf strspec = "Ascites" Then
            result = "5"
        ElseIf strspec = "CSF" Then
            result = "6"
        ElseIf strspec = "Whole blood" Then
            result = "7"
        ElseIf strspec = "Feses" Then
            result = "8"
        ElseIf strspec = "Other" Then
            result = "9"
        End If
        Return result
    End Function
    ReadOnly AllowedLNKeys As String = _
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 "
    Private Sub txtlabsearch_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtlabsearch.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedLNKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedLNKeys.Contains(e.KeyChar)

        End Select
    End Sub

    Private Sub txtlabsearch_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtlabsearch.KeyUp
        If e.KeyValue = Keys.Return Then
            dgvJoblist.DataSource = Nothing
            If dgvJoblist.Columns.Contains("Detail") Then
                dgvJoblist.Columns.Clear()
            End If


            Joblist()
            'ContructDgv()
        End If
    End Sub


    Private Function IsSampleNoAvailable(ByVal sn As String) As Boolean
        Dim result As Boolean
        Dim str As String
        str = "select count(sampleno) as num from jobdetail where labnumber<>'" & selectedLabNumber & "' and sampleno='" & sn & "'"
        Dim ocek As New clsGreConnect
        ocek.buildConn()

        Dim cmd As New SqlClient.SqlCommand(str, ocek.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            If rdr("num") > 0 Then
                result = True
            Else
                result = False
            End If
        Loop
        rdr.Close()
        cmd.Dispose()
        cmd = Nothing
        ocek.CloseConn()
        ocek = Nothing

        Return result
    End Function

    Private Function IsBarcodeAvailable(ByVal bc As String) As Boolean
        Dim result As Boolean
        Dim str As String
        str = "select count(barcode) as num from jobdetail where labnumber<>'" & selectedLabNumber & "' and barcode='" & bc & "'"
        Dim ocek As New clsGreConnect
        ocek.buildConn()

        Dim cmd As New SqlClient.SqlCommand(str, ocek.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            If rdr("num") > 0 Then
                result = True
            Else
                result = False
            End If
        Loop

        rdr.Close()
        cmd.Dispose()
        cmd = Nothing

        ocek.CloseConn()
        ocek = Nothing

        Return result
    End Function

    Private Sub cmdFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        Dim i As Integer
        Dim available As Boolean
        Dim sampleno As Integer
        Dim bc As String

        Dim isbcmode As Boolean
        Dim issammode As Boolean

        ReadLicense()
        If isHemaSampleAvailable() = True Then
            If Len(txtBarcode.Text) > 8 Then
                MessageBox.Show("Terdapat sample hematology, instrument Hematology menerima hanya sampai 8 digit angka")
                Exit Sub
            End If
        End If

        If Len(txtBarcode.Text) > 40 Then
            MessageBox.Show("Panjang melebihi barcode yang diijinkan")
            Exit Sub
        End If


        If txtSampleNo.Text = "" And txtBarcode.Text = "" Then
            MessageBox.Show("Silahkan isi nomor sample dan angka barcode")
            Return
        ElseIf bcMode = True And txtBarcode.Text = "" Then
            MessageBox.Show("Silahkan isi angka barcode")
            Return
        ElseIf snMode = True And txtSampleNo.Text = "" Then
            MessageBox.Show("Silahkan isi nomor sample")
            Return
        End If

        If Not Trim(txtSampleNo.Text) = "" Then
            If IsSampleNoAvailable(Trim(txtSampleNo.Text)) Then
                MessageBox.Show("Nomor sample tersebut sudah ada")
                txtSampleNo.Focus()
                Return
            Else
                sampleno = Trim(txtSampleNo.Text)
            End If
        Else
            MessageBox.Show("silahkan isi nomor sample")
            Return
        End If

        Dim specimendesc As String
        'specimendesc = GetSpecimen()


        If Not Trim(txtBarcode.Text) = "" Then
            If IsBarcodeAvailable(Trim(txtBarcode.Text)) Then
                MessageBox.Show("Barcode tersebut sudah ada")
                txtBarcode.Focus()
                Return
            End If
        Else
            MessageBox.Show("silahkan isi barcode")
            Return
        End If

        'perlu error check


        bc = UCase((txtBarcode.Text))


        Dim testnamechecked As String

        available = False
        For i = 0 To dgvdetail.Rows.Count - 1
            If dgvdetail.Rows(i).Cells("chk").Value = True Then
                available = True
                testnamechecked = dgvdetail.Rows(i).Cells("universaltest").Value

                'specimendesc = dgvdetail.Rows(i).Cells("uf1").Value
                If Not IsDBNull(dgvdetail.Rows(i).Cells("uf1").Value) Then
                    specimendesc = GetSpecimen(dgvdetail.Rows(i).Cells("uf1").Value)
                Else
                    specimendesc = "9"
                End If


                UpdatePerRowJobdetail(specimendesc, sampleno, bc, testnamechecked)
            End If
        Next
        If available Then

            If bcMode = True Then
                'generateBarcode2()
                'PrintDocument1.Print() 'print barcode
                ShowbarcodeToForm()

                If txtSampleNo.Text <> "" Then
                    GenerateLabel()
                End If

            ElseIf snMode = True Then
                GenerateLabel()
                If txtBarcode.Text <> "" Then
                    'GenerateBarcode()
                    ShowbarcodeToForm()
                End If

            End If

            MessageBox.Show("Silahkan cetak label")
            btnPrint.Visible = True
            cmdFinish.Enabled = False
            RdoBarcodeMode.Enabled = False
            rdoSampleMode.Enabled = False
            Joblist()
            isHemaTestAvailable = False
        Else
            MessageBox.Show("Pilih sample yang hendak diberi label")
        End If
    End Sub
    Private Function isHemaSampleAvailable() As Boolean
        Dim strsql As String
        Dim result As Boolean = False

        strsql = "select id,universaltest from jobdetail where labnumber='" & selectedLabNumber & "' and active='" & ACTIVESAMPLE & "' and status='" & NEWSAMPLE & "'"
        isHemaTestAvailable = False
        Dim ocek As New clsGreConnect
        ocek.buildConn()

        Dim cmd As New SqlClient.SqlCommand(strsql, ocek.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read

            For Each strfind As String In listHema
                If strfind = Trim(rdr("universaltest")) Then
                    result = True
                    isHemaTestAvailable = True
                    Exit Do
                End If
            Next
        Loop


        rdr.Close()
        cmd.Dispose()
        cmd = Nothing
        ocek.CloseConn()
        ocek = Nothing

        Return result

    End Function


    Private Sub UpdatePerRowJobdetail(ByVal specimendesc As String, ByVal sampleno As String, ByVal bc As String, ByVal testnamechecked As String)
        Dim objupdate As New clsGreConnect
        Dim strupdate As String
        objupdate.buildConn()

        If Trim(txtBarcode.Text) = "" And Trim(txtSampleNo.Text) <> "" Then
            strupdate = "update jobdetail set status='1" & _
                        "',sampleno='" & sampleno & "',specimendesc='" & specimendesc & "' where labnumber='" & selectedLabNumber & "' and active='" & ACTIVESAMPLE & "' and universaltest='" & testnamechecked & "'"
        ElseIf Trim(txtBarcode.Text) <> "" And Trim(txtSampleNo.Text) = "" Then
            strupdate = "update jobdetail set status='1" & _
                        "',barcode='" & bc & "',specimendesc='" & specimendesc & "' where labnumber='" & selectedLabNumber & "' and active='" & ACTIVESAMPLE & "' and universaltest='" & testnamechecked & "'"
        ElseIf Trim(txtBarcode.Text) <> "" And Trim(txtSampleNo.Text) <> "" Then
            strupdate = "update jobdetail set status='1" & _
                        "',barcode='" & bc & "',sampleno='" & sampleno & "',specimendesc='" & specimendesc & "' where labnumber='" & selectedLabNumber & "' and active='" & ACTIVESAMPLE & "' and universaltest='" & testnamechecked & "'"
        End If

        objupdate.ExecuteNonQuery(strupdate)
        objupdate.CloseConn()
    End Sub

    Private Sub txtSampleNo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSampleNo.GotFocus
        If MouseButtons = System.Windows.Forms.MouseButtons.None Then
            If alreadyhasidentity = False Then
                setTextSampleNo()
            End If

        End If
    End Sub

    Private Sub txtSampleNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSampleNo.KeyPress
        If Char.IsDigit(e.KeyChar) = False And Char.IsControl(e.KeyChar) = False Then
            e.Handled = True
            MsgBox("Masukkan Angka") 'bagus
        End If
    End Sub

    Private Sub txtSampleNo_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtSampleNo.MouseClick
        setTextSampleNo()
    End Sub





    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim g As Graphics
        Dim i As Integer
        g = e.Graphics

        Dim fnt As Font
        fnt = New Font("Arial", 11)

        Dim caption As String
        
        If bcMode = True Then
            Dim gr As Graphics = e.Graphics

            'For i = 0 To jmlprintbarcode - 1
            Cetak(gr)
            ' Next


        ElseIf snMode = True Then
            'atur posisi print di kertas
            caption = String.Format(Trim(lblnosample.Text))
            ' For i = 0 To jmlprintbarcode - 1
            g.DrawString(caption, fnt, System.Drawing.Brushes.Black, 5, 5)
            ' Next

        End If
    End Sub

    Public Sub WriteInsProperty()
        Dim insName As String

        If RdoBarcodeMode.Checked = True Then
            insName = "Mode=barcodeMode"
        ElseIf rdoSampleMode.Checked Then
            insName = "Mode=samplenoMode"
        End If

        Dim objWriter As New System.IO.StreamWriter(fpath)
        objWriter.WriteLine(insName)
        objWriter.Close()
    End Sub

    Private Sub RdoBarcodeMode_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RdoBarcodeMode.CheckedChanged
        If RdoBarcodeMode.Checked = True Then
            bcMode = True
            snMode = False
            WriteInsProperty()
        End If
    End Sub

    Private Sub rdoSampleMode_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoSampleMode.CheckedChanged
        If rdoSampleMode.Checked = True Then
            bcMode = False
            snMode = True
            WriteInsProperty()
        End If
    End Sub


    Private Sub chkPilihSemua_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPilihSemua.CheckedChanged
        If chkPilihSemua.Checked = True Then
            For i = 0 To dgvdetail.Rows.Count - 1
                If dgvdetail.Rows(i).Cells("chk").Value = False Then
                    dgvdetail.Rows(i).Cells("chk").Value = True
                End If
            Next
        ElseIf chkPilihSemua.Checked = False Then
            For i = 0 To dgvdetail.Rows.Count - 1
                If dgvdetail.Rows(i).Cells("chk").Value = True Then
                    dgvdetail.Rows(i).Cells("chk").Value = False
                End If
            Next
        End If
    End Sub

    Private Sub txtSampleNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSampleNo.TextChanged

    End Sub
    Private Function generateBarcodeForPrint() As Bitmap
        Try
            Dim ft As String
            Dim bh As Integer
            Dim bartype As String
            Dim scala As Integer
            Dim ftsize As Integer


            If txtBarcode.Text.Length <> 0 Then


                Dim strsetting As String
                strsetting = "select * from setting where name='barcode'"
                Dim ogre As New clsGreConnect
                ogre.buildConn()

                Dim cmd As New SqlClient.SqlCommand(strsetting, ogre.grecon)
                Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                If rdr.HasRows Then
                    Do While rdr.Read
                        ft = rdr("font")
                        bh = rdr("height")
                        bartype = rdr("value")
                        scala = rdr("scala")
                        ftsize = rdr("ftsize")
                        xpos = rdr("x")
                        ypos = rdr("y")
                    Loop

                End If
                cmd.Dispose()
                ogre.CloseConn()



                Dim jenisfont As New Font(ft, ftsize, Font.Style.Regular)
                'code 39 43 char
                'code 93 48 char
                'code 128 108 char

                Dim maxbarHight As Integer
                Dim x, y As Integer
                x = xpos  'CInt(cbx.Text.Trim)
                y = ypos 'CInt(cby.Text.Trim)


                Dim originalbarcodeimage As Bitmap
                Dim CodeBitmap As Bitmap
                Dim ImagewithText As Bitmap
                Dim gr As Graphics

                maxbarHight = bh

                If bartype = "Code 128" Then
                    Dim barcode128 As Code128BarcodeDraw = BarcodeDrawFactory.Code128WithChecksum

                    originalbarcodeimage = barcode128.Draw(txtBarcode.Text.Trim.ToUpper, maxbarHight, scala)
                    originalbarcodeimage.SetResolution(203.0F, 203.0F)

                    ImagewithText = New Bitmap(originalbarcodeimage.Width, originalbarcodeimage.Height + 225)
                    ImagewithText.SetResolution(203.0F, 203.0F)

                    gr = Graphics.FromImage(ImagewithText)
                    gr.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
                    gr.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
                    gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
                    gr.TextRenderingHint = TextRenderingHint.AntiAlias

                    Dim myRectangle As New RectangleF(0, 0, originalbarcodeimage.Width, 50)
                    gr.DrawString(patientName, jenisfont, Brushes.Black, myRectangle)
                    gr.DrawImageUnscaled(originalbarcodeimage, x, y)
                    'gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, 0, originalbarcodeimage.Width, 100))
                    gr.DrawString(txtBarcode.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, originalbarcodeimage.Height + 40, originalbarcodeimage.Width + 10, 50))
                    'Printer.'pictBarcode.Image = ImagewithText
                    Return ImagewithText
                    'CodeBitmap = New Bitmap(pic.Image)

                    'CodeBitmap = New Bitmap(ImagewithText)

                    'CodeBitmap.SetResolution(203.0F, 203.0F)
                    'CodeBitmap.Save(Application.StartupPath & "\" & "001.png")
                    'btnPrint.Enabled = True
                ElseIf bartype = "Code EAN 13" Then
                    Dim barcode128 As CodeEan13BarcodeDraw = BarcodeDrawFactory.CodeEan13WithChecksum

                    originalbarcodeimage = barcode128.Draw(txtBarcode.Text.Trim.ToUpper, maxbarHight, scala)
                    originalbarcodeimage.SetResolution(203.0F, 203.0F)

                    ImagewithText = New Bitmap(originalbarcodeimage.Width, originalbarcodeimage.Height + 225)
                    ImagewithText.SetResolution(203.0F, 203.0F)

                    gr = Graphics.FromImage(ImagewithText)
                    gr.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
                    gr.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
                    gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
                    gr.TextRenderingHint = TextRenderingHint.AntiAlias

                    Dim myRectangle As New RectangleF(0, 0, originalbarcodeimage.Width, 50)
                    gr.DrawString(patientName, jenisfont, Brushes.Black, myRectangle)
                    gr.DrawImageUnscaled(originalbarcodeimage, x, y)
                    'gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, 0, originalbarcodeimage.Width, 100))
                    gr.DrawString(txtBarcode.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, originalbarcodeimage.Height + 40, originalbarcodeimage.Width + 10, 50))
                    'Printer.'pictBarcode.Image = ImagewithText
                    Return ImagewithText
                    'CodeBitmap = New Bitmap(pic.Image)

                    'CodeBitmap = New Bitmap(ImagewithText)

                    'CodeBitmap.SetResolution(203.0F, 203.0F)
                    'CodeBitmap.Save(Application.StartupPath & "\" & "001.png")
                    'btnPrint.Enabled = True
                ElseIf bartype = "Code EAN 8" Then
                    Dim barcode128 As CodeEan8BarcodeDraw = BarcodeDrawFactory.CodeEan8WithChecksum

                    originalbarcodeimage = barcode128.Draw(txtBarcode.Text.Trim.ToUpper, maxbarHight, scala)
                    originalbarcodeimage.SetResolution(203.0F, 203.0F)

                    ImagewithText = New Bitmap(originalbarcodeimage.Width, originalbarcodeimage.Height + 225)
                    ImagewithText.SetResolution(203.0F, 203.0F)

                    gr = Graphics.FromImage(ImagewithText)
                    gr.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
                    gr.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
                    gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
                    gr.TextRenderingHint = TextRenderingHint.AntiAlias

                    Dim myRectangle As New RectangleF(0, 0, originalbarcodeimage.Width, 50)
                    gr.DrawString(patientName, jenisfont, Brushes.Black, myRectangle)
                    gr.DrawImageUnscaled(originalbarcodeimage, x, y)
                    'gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, 0, originalbarcodeimage.Width, 100))
                    gr.DrawString(txtBarcode.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, originalbarcodeimage.Height + 40, originalbarcodeimage.Width + 10, 50))
                    'Printer.'pictBarcode.Image = ImagewithText
                    Return ImagewithText
                    'CodeBitmap = New Bitmap(pic.Image)

                    'CodeBitmap = New Bitmap(ImagewithText)

                    'CodeBitmap.SetResolution(203.0F, 203.0F)
                    'CodeBitmap.Save(Application.StartupPath & "\" & "001.png")
                    'btnPrint.Enabled = True
                ElseIf bartype = "Code 39" Then
                    Dim barcode128 As Code39BarcodeDraw = BarcodeDrawFactory.Code39WithChecksum

                    originalbarcodeimage = barcode128.Draw(txtBarcode.Text.Trim.ToUpper, maxbarHight, scala)
                    originalbarcodeimage.SetResolution(203.0F, 203.0F)

                    ImagewithText = New Bitmap(originalbarcodeimage.Width, originalbarcodeimage.Height + 225)
                    ImagewithText.SetResolution(203.0F, 203.0F)

                    gr = Graphics.FromImage(ImagewithText)
                    gr.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
                    gr.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
                    gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
                    gr.TextRenderingHint = TextRenderingHint.AntiAlias

                    Dim myRectangle As New RectangleF(0, 0, originalbarcodeimage.Width, 50)
                    gr.DrawString(patientName, jenisfont, Brushes.Black, myRectangle)
                    gr.DrawImageUnscaled(originalbarcodeimage, x, y)
                    'gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, 0, originalbarcodeimage.Width, 100))
                    gr.DrawString(txtBarcode.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, originalbarcodeimage.Height + 40, originalbarcodeimage.Width + 10, 50))
                    'Printer.'pictBarcode.Image = ImagewithText
                    Return ImagewithText
                    'CodeBitmap = New Bitmap(pic.Image)

                    'CodeBitmap = New Bitmap(ImagewithText)

                    'CodeBitmap.SetResolution(203.0F, 203.0F)
                    'CodeBitmap.Save(Application.StartupPath & "\" & "001.png")
                    'btnPrint.Enabled = True
                ElseIf bartype = "Code 93" Then
                    Dim barcode128 As Code93BarcodeDraw = BarcodeDrawFactory.Code93WithChecksum

                    originalbarcodeimage = barcode128.Draw(txtBarcode.Text.Trim.ToUpper, maxbarHight, scala)
                    originalbarcodeimage.SetResolution(203.0F, 203.0F)

                    ImagewithText = New Bitmap(originalbarcodeimage.Width, originalbarcodeimage.Height + 225)
                    ImagewithText.SetResolution(203.0F, 203.0F)

                    gr = Graphics.FromImage(ImagewithText)
                    gr.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
                    gr.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
                    gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
                    gr.TextRenderingHint = TextRenderingHint.AntiAlias

                    Dim myRectangle As New RectangleF(0, 0, originalbarcodeimage.Width, 50)
                    gr.DrawString(patientName, jenisfont, Brushes.Black, myRectangle)
                    gr.DrawImageUnscaled(originalbarcodeimage, x, y)
                    'gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, 0, originalbarcodeimage.Width, 100))
                    gr.DrawString(txtBarcode.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, originalbarcodeimage.Height + 40, originalbarcodeimage.Width + 10, 50))
                    'Printer.
                    'pictBarcode.Image = ImagewithText
                    Return ImagewithText
                    'CodeBitmap = New Bitmap(pic.Image)

                    'CodeBitmap = New Bitmap(ImagewithText)

                    'CodeBitmap.SetResolution(203.0F, 203.0F)
                    'CodeBitmap.Save(Application.StartupPath & "\" & "001.png")
                    'btnPrint.Enabled = True
                End If





            End If
            'Catch ex As Exception
            '    MsgBox(ex.Message)
            'End Try
            Exit Function
ErrMgr:
            Select Case Err.Number

                Case Else
                    MsgBox(Err.Number)
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try

    End Function
    Private Sub Get_Bar_Parameter()
        Try

            Dim strsetting As String
            strsetting = "select * from setting where name='barcode'"
            Dim ogre As New clsGreConnect
            ogre.buildConn()

            Dim cmd As New SqlClient.SqlCommand(strsetting, ogre.grecon)
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If rdr.HasRows Then
                Do While rdr.Read
                    bar_res = rdr("res")
                    bar_height = rdr("height")
                    bar_type = rdr("value")
                    bar_skala = rdr("scala")
                    bar_label_size = rdr("ftsize")
                    bar_y = rdr("y")
                Loop
            Else
                bar_height = 60
                bar_label_size = 9
                bar_skala = 3
                bar_res = 203
                bar_y = 0
                bar_type = "code 128"
            End If
            cmd.Dispose()
            ogre.CloseConn()

        Catch ex As Exception
            MessageBox.Show("Terjadi kesalahan, silahkan konfigure barcode")
        End Try

    End Sub
    Private Sub ShowbarcodeToForm()
        Dim gr As Graphics = pictBarcode.CreateGraphics 'cetak di picturebox
        gr.Clear(Me.BackColor)
        Cetak(gr)
    End Sub




    Private Sub Cetak(ByVal gr As Graphics)

        Dim res As Single

        Dim label As String
        label = patientName

        res = bar_res

        If bar_type = "code 128" Then
            Dim barcode128 As Code128BarcodeDraw = BarcodeDrawFactory.Code128WithChecksum
            originalbarcodeimage = barcode128.Draw(txtBarcode.Text.Trim, bar_height, bar_skala)

        ElseIf bar_type = "code 39" Then
            Dim barcode128 As Code39BarcodeDraw = BarcodeDrawFactory.Code39WithChecksum
            originalbarcodeimage = barcode128.Draw(txtBarcode.Text.Trim, bar_height, bar_skala)
        ElseIf bar_type = "code 93" Then
            Dim barcode128 As Code93BarcodeDraw = BarcodeDrawFactory.Code93WithChecksum
            originalbarcodeimage = barcode128.Draw(txtBarcode.Text.Trim, bar_height, bar_skala)
        End If

        originalbarcodeimage.SetResolution(res, res)

        Dim bch As Integer
        bch = originalbarcodeimage.Height


        Dim stringFont As New Font("Arial", bar_label_size)

        ' Measure string.
        Dim stringSize As New SizeF
        stringSize = gr.MeasureString(label, stringFont)


        Dim myTopRectangle As New RectangleF(0, bar_y, stringSize.Width, stringSize.Height)
        gr.DrawString(label, stringFont, Brushes.Black, myTopRectangle)

        Dim bcRectangle As New Rectangle(0, myTopRectangle.Bottom, originalbarcodeimage.Width, originalbarcodeimage.Height)
        gr.DrawImage(originalbarcodeimage, bcRectangle)

        Dim code As String
        code = txtBarcode.Text.Trim
        Dim stringBottomSize As New SizeF
        stringBottomSize = gr.MeasureString(code, stringFont)

        Dim bottomrectangle As New RectangleF(0, bcRectangle.Bottom, stringBottomSize.Width, stringBottomSize.Height)
        gr.DrawString(code, stringFont, Brushes.Black, bottomrectangle)
    End Sub
    ReadOnly AllowedKeys As String = _
    "abcdefghijklmnopqrstuvwxyz0123456789"
    ReadOnly AllowedHema As String = _
    "0123456789"


    Private Sub txtBarcode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBarcode.KeyPress
        If isHemaTestAvailable = False Then
            txtBarcode.MaxLength = 9
            Select Case e.KeyChar

                Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                    ' Call method here...

                Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                    e.Handled = False ' Delete the character

                Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                    ' Paste clipboard content only if contains allowed keys
                    e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

                Case Else ' Other key is pressed
                    e.Handled = Not AllowedKeys.Contains(e.KeyChar)

            End Select

        Else
            txtBarcode.MaxLength = 8
            Select Case e.KeyChar

                Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                    ' Call method here...

                Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                    e.Handled = False ' Delete the character

                Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                    ' Paste clipboard content only if contains allowed keys
                    e.Handled = Not Clipboard.GetText().All(Function(c) AllowedHema.Contains(c))

                Case Else ' Other key is pressed
                    e.Handled = Not AllowedHema.Contains(e.KeyChar)

            End Select

        End If
    End Sub


    Private Sub txtlabsearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtlabsearch.TextChanged

    End Sub
    Dim tips As New ToolTip

    Private Sub txtBarcode_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBarcode.MouseEnter
        If isHemaTestAvailable Then
            tips.Show("Hematology analyzer hanya menerima input maksimal 8 angka", txtBarcode, 3000)
        End If
    End Sub
    




    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        txtLabnumber.Text = ""
        txtBarcode.Text = ""
        txtSampleNo.Text = ""
        txtaddress.Text = ""
        txtlabsearch.Text = ""
        txtname.Text = ""
        txttelepon.Text = ""
        'cbSpeciment.SelectedIndex = 0
        Joblist()

    End Sub

    Private Function CreateRandomNumber() As Integer
        Randomize()
        lowerbound = 88
        upperbound = 99999999
        randomvalue = CInt(Math.Floor((upperbound - lowerbound + 1) * Rnd())) + lowerbound
        Return randomvalue
    End Function


    Private Sub setBarcodeText()
        Dim rndval As Integer
        Dim findthesamebarcode As Integer
        findthesamebarcode = 0
        Randomize()

        rndval = CreateRandomNumber()
        Do While IsBarcodeAvailable(CStr(rndval)) = True
            findthesamebarcode = findthesamebarcode + 1
            If findthesamebarcode > 30 Then
                MessageBox.Show("Sebaiknya data di archive")
                Me.Close()
            End If
            rndval = CreateRandomNumber()
        Loop

        txtBarcode.Text = CStr(rndval)
    End Sub

    Private Sub cbSpeciment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub txtBarcode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBarcode.TextChanged

    End Sub
End Class