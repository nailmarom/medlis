﻿Imports Zen.Barcode
Public Class frnBarcodeConfig
    Dim originalbarcodeimage As Bitmap
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Dim gr As Graphics = Me.CreateGraphics  'cetak di form
        Dim gr As Graphics = PictureBox1.CreateGraphics 'cetak di picturebox


        gr.Clear(Me.BackColor)
        Cetak(gr)

    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim gr As Graphics = e.Graphics

        Cetak(gr)

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        PrintDocument1.Print()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txttinggi.Text = 60
        txtsize.Text = 9
        txtskala.Text = 3
        txtresolution.Text = 203
        txtY.Text = 0
        cbbctype.SelectedText = "code 128"
    End Sub

    Private Sub Cetak(ByVal gr As Graphics)

        Dim res As Single

        Dim label As String
        label = "test"

        res = CSng(txtresolution.Text)


        Dim barcode128 As Code128BarcodeDraw = BarcodeDrawFactory.Code128WithChecksum
        originalbarcodeimage = barcode128.Draw("12345678", CInt(txttinggi.Text), CInt(txtskala.Text))
        'originalbarcodeimage.SetResolution(203.0F, 203.0F)
        originalbarcodeimage.SetResolution(res, res)

        Dim bch As Integer
        bch = originalbarcodeimage.Height


        Dim stringFont As New Font("Arial", CInt(txtsize.Text))

        ' Measure string.
        Dim stringSize As New SizeF
        stringSize = gr.MeasureString(label, stringFont)

        ' Create rectangle for source image.
        'Dim srcRect As New Rectangle(50, 50, 150, 150)
        'Dim units As GraphicsUnit = GraphicsUnit.Pixel
        Dim myTopRectangle As New RectangleF(0, CInt(txtY.Text), stringSize.Width, stringSize.Height)
        gr.DrawString(label, stringFont, Brushes.Black, myTopRectangle)

        Dim bcRectangle As New Rectangle(0, myTopRectangle.Bottom, originalbarcodeimage.Width, originalbarcodeimage.Height)
        gr.DrawImage(originalbarcodeimage, bcRectangle)

        Dim code As String
        code = "12345678"
        Dim stringBottomSize As New SizeF
        stringBottomSize = gr.MeasureString(code, stringFont)

        Dim bottomrectangle As New RectangleF(0, bcRectangle.Bottom, stringBottomSize.Width, stringBottomSize.Height)
        gr.DrawString(code, stringFont, Brushes.Black, bottomrectangle)
    End Sub

End Class
