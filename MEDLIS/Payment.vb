﻿Imports GemBox.Spreadsheet
Imports Microsoft.Win32
Imports System.IO
Imports System.Data.SqlClient
Imports DevExpress.XtraReports.UI

Public Class Payment
    Dim selectedLn As String
    Dim selectedPatientId As Integer
    Dim discountpayment As Double
    Dim selectedPaymentName As String
    Dim rptotal As Double
    Dim price As Double


    Dim beforediscount As Double
    Dim discount As Double
    Dim afterdiscount As Double
    Dim strdatereport As Date
    Dim strjenispembayaran As String

    Dim strpaymentnote As String

    Public mycaller As ListNotPaidFrm


    Public Sub New(ByVal ln As String)
        InitializeComponent()
        selectedLn = ln

    End Sub

    Private Sub Payment_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        isPaymentWinOpen = 0
    End Sub


    Private Sub Payment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY")
        isPaymentWinOpen = 1
        ReadLicense()
        dgvtest.AllowUserToAddRows = False
        dgvtest.RowHeadersVisible = False


        dgpack.AllowUserToAddRows = False
        dgpack.RowHeadersVisible = False

        rdolangsung.Checked = True

        ''bagus
        'System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("id-ID")
        'System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo("id-ID") 'bagus penting
        'bagus currency

        LoadLnData()
        LoadPayment()
        cmdcetak.Enabled = False
        txtuangdibayar.Enabled = False
        txtnomorkartu.Enabled = False
        txtkembali.Enabled = False
        cbpayment.SelectedIndex = 0
        txtuangdibayar.Select()
        txtuangdibayar.Focus()

    End Sub
    Private Sub LoadPayment()
        Dim strsql As String
        strsql = "select * from discountpayment"
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim cmd As New SqlClient.SqlCommand(strsql, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

        cbpayment.Items.Add("Uang-Cash")

        Do While rdr.Read
            If Not IsDBNull(rdr("paymentname")) Then
                cbpayment.Items.Add(rdr("paymentname"))
            End If
        Loop

    End Sub
    Private Sub LoadLnData()
        Dim strsql As String
        strsql = "select * from job where job.labnumber='" & selectedLn & "'"

        Dim OGRE As New clsGreConnect
        OGRE.buildConn()

        Dim cmd As New SqlClient.SqlCommand(strsql, OGRE.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            If Not IsDBNull(rdr("idpasien")) Then
                selectedPatientId = rdr("idpasien")
                txtlabnumber.Text = rdr("labnumber")
                txtDocter.Text = rdr("docname")
                Dim strdate As String
                '-- 1980 0316
                'strdate = rdr("datereceived").ToString.Substring(0, 4) & "-" & rdr("datereceived").ToString.Substring(4, 2) & "-" & rdr("datereceived").ToString.Substring(6, 2) 'bagus string to date
                dpreg.Value = rdr("datereceived") 'DateTime.Parse(strdate)
                strdatereport = rdr("datereceived") 'DateTime.Parse(strdate)
            End If
        Loop
        rdr.Close()
        cmd.Dispose()
        OGRE.CloseConn()

        If selectedPatientId <> 0 Then
            LoadPatientData()
        End If

        FillDgv()
        FillDgvPack()
    End Sub
    Private Sub FillDgv()
        'Dim strsql = "select testtype.testname,testtype.keterangan,testtype.price from jobdetail,testtype where jobdetail.labnumber='" & selectedLn & "' and jobdetail.idtesttype=testtype.id"
        Dim strsql = "select testtype.testname,testtype.keterangan,jobdetail.price from jobdetail,testtype where jobdetail.labnumber='" & selectedLn & "' and jobdetail.idtesttype=testtype.id"
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim dtable As New DataTable
        dtable = ogre.ExecuteQuery(strsql)
        dgvtest.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvtest.Columns.Count - 1
            dgvtest.Columns(j).Visible = False
        Next

        dgvtest.Columns("testname").Visible = True
        dgvtest.Columns("testname").HeaderText = "Test Name"
        dgvtest.Columns("testname").Width = 100
        dgvtest.Columns("testname").ReadOnly = True

        dgvtest.Columns("keterangan").Visible = True
        dgvtest.Columns("keterangan").HeaderText = "Keterangan"
        dgvtest.Columns("keterangan").Width = 200
        dgvtest.Columns("keterangan").ReadOnly = True

        dgvtest.Columns("price").Visible = True
        dgvtest.Columns("price").HeaderText = "Harga"
        dgvtest.Columns("price").Width = 150
        dgvtest.Columns("price").DefaultCellStyle.Format = "n0"
        dgvtest.Columns("price").ReadOnly = True

        ogre.CloseConn()
        MakeTotalPrice()

    End Sub

    Private Sub FillDgvPack()

        Dim strsql = "select jobpack.packname,jobpack.pricepack as price from jobpack where jobpack.labnumber='" & selectedLn & "'"
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim dtable As New DataTable
        dtable = ogre.ExecuteQuery(strsql)
        dgpack.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgpack.Columns.Count - 1
            dgpack.Columns(j).Visible = False
        Next

        dgpack.Columns("packname").Visible = True
        dgpack.Columns("packname").HeaderText = "Test Name"
        dgpack.Columns("packname").Width = 100
        dgpack.Columns("packname").ReadOnly = True
        'dgpack.Columns("keterangan").Visible = True
        'dgpack.Columns("keterangan").HeaderText = "Keterangan"
        'dgpack.Columns("keterangan").Width = 200

        dgpack.Columns("price").Visible = True
        dgpack.Columns("price").HeaderText = "Harga"
        dgpack.Columns("price").Width = 150
        dgpack.Columns("price").DefaultCellStyle.Format = "n0"
        dgpack.Columns("price").ReadOnly = True

        ogre.CloseConn()
        MakeTotalPrice()

    End Sub

    Private Sub MakeTotalPrice()

        Dim i As Integer
        i = 0
        price = 0
        For i = 0 To dgvtest.RowCount - 1
            If Not dgvtest.Rows(i).IsNewRow Then

                price = price + dgvtest.Rows(i).Cells("price").Value
            End If
        Next
        Dim k As Integer
        k = 0
        For k = 0 To dgpack.RowCount - 1
            If Not dgpack.Rows(k).IsNewRow Then

                price = price + dgpack.Rows(k).Cells("price").Value
            End If
        Next

        beforediscount = price
        'txtprice.Text = FormatCurrency(price)
        txtprice.Text = FormatNumber(price, 0)

    End Sub


    Private Sub LoadPatientData()
        Dim strsql As String
        strsql = "select * from patient where id='" & selectedPatientId & "'"
        Dim greshow As New clsGreConnect
        greshow.buildConn()

        enableText()
        Dim cmd As New SqlClient.SqlCommand(strsql, greshow.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            txtNamaPasien.Text = rdr("patientname")
            txtAlamat.Text = rdr("address")
            txtPhone.Text = rdr("phone")
            txtIdPasien.Text = rdr("idpatient")
            txtEmail.Text = rdr("email")
            If rdr("malefemale") = genderMale Then
                rdoLaki.Checked = True
                rdoPerempuan.Checked = False
                rdoanak.Checked = False
            ElseIf rdr("malefemale") = genderFemale Then
                rdoPerempuan.Checked = True
                rdoLaki.Checked = False
                rdoanak.Checked = False
            ElseIf rdr("malefemale") = genderChild Then
                rdoPerempuan.Checked = False
                rdoLaki.Checked = False
                rdoanak.Checked = True
            End If

            dpBirth.Value = rdr("birthdate")
        Loop

        disableText()

        rdr.Close()
        cmd.Dispose()
        greshow.CloseConn()
    End Sub
    Private Sub enableText()
        For Each ctrl As Control In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.Enabled = True
            End If
        Next
        dpBirth.Enabled = True
        rdoLaki.Enabled = True
        rdoPerempuan.Enabled = True
    End Sub
    Private Sub disableText()
        For Each ctrl As Control In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.Enabled = False
            End If
        Next

        dpBirth.Enabled = False
        rdoLaki.Enabled = False
        rdoPerempuan.Enabled = False
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub cbpayment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbpayment.SelectedIndexChanged
        Dim strpayment As String
        strpayment = Trim(cbpayment.Text)

        txtdiscount.ReadOnly = False
        txtblmbulat.ReadOnly = False
        txtsetelahbulat.ReadOnly = False

        If cbpayment.Text = "Uang-Cash" Or cbpayment.SelectedIndex = 0 Then
            discountpayment = 0
            txtuangdibayar.Enabled = True
            txtnomorkartu.Enabled = False
            selectedPaymentName = cbpayment.Text
            txtkembali.Enabled = True
            txtnomorkartu.Enabled = False
            txtuangdibayar.Select()
            txtuangdibayar.Focus()
        Else
            txtkembali.Enabled = False
            txtuangdibayar.Enabled = False
            txtnomorkartu.Enabled = True


            strpaymentnote = Trim(txtnomorkartu.Text)
            Dim strsql As String
            strsql = "select * from discountpayment where paymentname='" & strpayment & "'"

            Dim OGRE As New clsGreConnect
            OGRE.buildConn()
            discountpayment = 0
            Dim cmd As New SqlClient.SqlCommand(strsql, OGRE.grecon)
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While rdr.Read
                If Not IsDBNull(rdr("active")) Then
                    If rdr("active") = "1" Then
                        discountpayment = CDbl(rdr("percent"))
                        selectedPaymentName = rdr("paymentname")
                        txtdiscount.Text = rdr("discountname") & " discount " & rdr("percent") & " percent"
                    End If
                End If
            Loop
            rdr.Close()
            cmd.Dispose()
            OGRE.CloseConn()
        End If
        makeTheGrand()
    End Sub
    Private Sub makeTheGrand()
        Dim rpdiskon As Double

        discount = discountpayment
        rpdiskon = price * discountpayment / 100
        txtblmbulat.Text = FormatNumber(price - rpdiskon, 0)

        rptotal = BulatkanUang((price - rpdiskon), 15, 100)
        afterdiscount = rptotal
        txtsetelahbulat.Text = FormatNumber(rptotal, 0) 'bagus
        lblterbilang.Text = Terbilang.TERBILANG(rptotal)


        txtdiscount.ReadOnly = True
        txtblmbulat.ReadOnly = True
        txtsetelahbulat.ReadOnly = True
        txtkembali.ReadOnly = True


    End Sub
    Public Function BulatkanUang(ByVal NilaiUang As Double, Optional ByVal BatasDihapus As Integer = 0, Optional ByVal PecahanTerkecil As Integer = 100) As Double
        Dim d As Double
        d = NilaiUang - (Fix(NilaiUang / PecahanTerkecil) * PecahanTerkecil)
        If (d = 0) Or (d <= BatasDihapus) Then
            BulatkanUang = NilaiUang - d
        Else
            BulatkanUang = NilaiUang + (PecahanTerkecil - d)
        End If
    End Function
    Private Sub cmdpayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdpayment.Click
        ReadLicense()

        txtkembali.ReadOnly = False

        If Trim(txtuangdibayar.Text).Length = 0 Then
            MessageBox.Show("Silahkan isi jumlah pembayaran")
            Exit Sub
        End If

        Dim uangdibayar As Double
        uangdibayar = CDbl(Trim(txtuangdibayar.Text))

        If uangdibayar < rptotal Then
            MessageBox.Show("Pembayaran kurang")
            Return
        End If
        'txtkembali.Text = FormatCurrency(uangdibayar - rptotal)
        txtkembali.Text = FormatNumber(uangdibayar - rptotal, 0)
        txtkembali.ReadOnly = True

        '-------cek function ---
        If cbpayment.SelectedIndex < 0 Then
            MessageBox.Show("Pilih metode pembayaran")
            Return
        End If


        If cbpayment.SelectedIndex = 0 Then
            If txtuangdibayar.Text.Length < 2 Then
                MessageBox.Show("Masukkan nilai yang benar")
            Else
                Dim rpbayar As Double
                rpbayar = Trim(txtuangdibayar.Text)
                If rpbayar < rptotal Then
                    MessageBox.Show("Uang pembayaran kurang")
                    Return
                End If
            End If
        End If

        If cbpayment.SelectedIndex > 0 Then
            If txtnomorkartu.Text.Length < 4 Then
                MessageBox.Show("Masukkan sekurang kurangnya 4 digit nomor kartu")
                Return
            End If
        End If

        '-----------------------

        Dim strsql As String
        If rdolangsung.Checked = True Then
            strsql = "update job set paymentstatus='" & PAID & "',paymentname='" & selectedPaymentName & "',totalprice='" & rptotal & "',discount='" & discountpayment & "',paymenttype='" & DIRECTPAYMENT & "',paymentnote='" & strpaymentnote & "'  where labnumber='" & selectedLn & "'"
        ElseIf rdotagihkan.Checked = True Then
            strsql = "update job set paymentstatus='" & PAIDLATER & "',totalprice='" & rptotal & "',paymenttype='" & UNDIRECT & "' where labnumber='" & selectedLn & "'"
        End If

        Dim ogre As New clsGreConnect
        ogre.buildConn()
        ogre.ExecuteNonQuery(strsql)

        If txtnomorkartu.Text.Length > 4 Then
            strjenispembayaran = selectedPaymentName & "-" & txtnomorkartu.Text.Substring(txtnomorkartu.TextLength - 4)
        Else
            strjenispembayaran = selectedPaymentName
        End If

        If Trim(cbpayment.Text) = "Uang-Cash" Then
            strpaymentnote = Trim(txtuangdibayar.Text)
        Else
            strpaymentnote = Trim(txtnomorkartu.Text)
        End If

        Dim pormat As String
        pormat = "yyyy-MM-dd HH:mm:ss"

        strsql = "insert into nota(labnumber,idpasien,terbilang,beforediscount," &
        "discount,afterdiscount,datenota,jenispembayaran,grandtotal)" &
        "values('" & selectedLn & "','" & selectedPatientId & "','" & lblterbilang.Text & "','" & beforediscount & "','" & discount & "','" & afterdiscount & "','" & Now.ToString(pormat) & "','" & strjenispembayaran & "'," & rptotal.ToString() & ")"
        ogre.buildConn()
        ogre.ExecuteNonQuery(strsql)

        ogre.CloseConn()
        mycaller.LoadNotYetPaid()

        cmdcetak.Enabled = True
        txtuangdibayar.ReadOnly = True
        cmdpayment.Enabled = False
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.Dispose()
    End Sub

    Private Sub cmdcetak_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdcetak.Click

        Dim rpt As New NotaPembayaran
        rpt.Parameters("IdPatient").Value = selectedPatientId
        rpt.Parameters("IdPatient").Visible = False
        rpt.Parameters("LabNumber").Value = selectedLn
        rpt.Parameters("LabNumber").Visible = False

        Dim printTool As New ReportPrintTool(rpt)

        printTool.ShowPreviewDialog()

        'Dim datenow As Date
        'datenow = Now

        'If txtnomorkartu.Text.Length > 4 Then
        '    strjenispembayaran = selectedPaymentName & "-" & txtnomorkartu.Text.Substring(txtnomorkartu.TextLength - 4)
        'Else
        '    strjenispembayaran = selectedPaymentName
        'End If

        'If Trim(cbpayment.Text) = "Uang-Cash" Then
        '    strpaymentnote = Trim(txtuangdibayar.Text)
        'Else
        '    strpaymentnote = Trim(txtnomorkartu.Text)
        'End If

        ''========== insert into nota
        'Dim ogre As New clsGreConnect
        'ogre.buildConn()

        'Dim strsql As String
        'Dim strdate As Date
        'Dim strsimpledate As Date

        'Dim pormat As String
        'pormat = "yyyy-MM-dd HH:mm:ss"

        'Dim print_terbilang As String
        'print_terbilang = lblterbilang.Text

        'strdate = dp.Value
        'strsimpledate = dp.Value

        ''  to_timestamp('2012-10-11 12:13:14.123', 
        ''yyyy-MM-dd HH24:MI:SS.MS')::timestamp;

        ''penting

        'strsql = "insert into nota(labnumber,idpasien,terbilang,beforediscount," &
        '"discount,afterdiscount,datenota,jenispembayaran,grandtotal)" &
        '"values('" & selectedLn & "','" & selectedPatientId & "','" & lblterbilang.Text & "','" & beforediscount & "','" & discount & "','" & afterdiscount & "','" & datenow.ToString(pormat) & "','" & strjenispembayaran & "'," & rptotal.ToString() & ")"
        'ogre.ExecuteNonQuery(strsql)
        'ogre.CloseConn()

        '& strdate.ToString(pormat) 
        '===========
        'create excel here =================

        'Dim excelPath As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "notapatient.xlsx")
        'Dim logoPath As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "logolab.jpg")
        'Dim ef As ExcelFile = ExcelFile.Load(excelPath)
        'Dim ws As ExcelWorksheet = ef.Worksheets(0)
        ''bagus
        ''Dim headerFooter As SheetHeaderFooter = ws.HeadersFooters
        ''headerFooter.DefaultPage.Header.LeftSection.Content = "Nama Lab"
        ''headerFooter.FirstPage.Header.RightSection.AppendPicture(logoPath, 60, 60)




        'Dim grepasien As New clsGreConnect
        'grepasien.buildConn()

        'Dim print_id_pasien As String
        'Dim print_nama As String
        'Dim print_umur As String
        'Dim print_telepon As String
        'Dim print_address As String



        'Dim sqlpasien As String
        'sqlpasien = "SELECT patientname, address, malefemale, phone, idpatient, email, id, birthdate FROM patient where id='" & selectedPatientId & "'"

        'Dim cmdpasien As New  SqlClient.SqlCommand(sqlpasien, grepasien.grecon)
        'Dim rdrpasien As  SqlDataReader = cmdpasien.ExecuteReader(CommandBehavior.CloseConnection)
        'Do While rdrpasien.Read
        '    If Not IsDBNull(rdrpasien("idpatient")) Then
        '        print_id_pasien = rdrpasien("idpatient")
        '    End If

        '    If Not IsDBNull(rdrpasien("patientname")) Then
        '        print_nama = rdrpasien("patientname")
        '    End If

        '    If Not IsDBNull(rdrpasien("phone")) Then
        '        print_telepon = rdrpasien("phone")
        '    End If

        '    If Not IsDBNull(rdrpasien("phone")) Then
        '        print_telepon = rdrpasien("phone")
        '    End If
        '    If Not IsDBNull(rdrpasien("address")) Then
        '        print_address = rdrpasien("address")
        '    End If
        'Loop


        'Dim sqljob As String
        'Dim print_dokter As String
        'Dim print_nolab As String
        'Dim print_datereceived As String

        'Dim print_labnumber As String
        'print_labnumber = selectedLn

        'Dim print_payment_type As String
        'Dim print_payment_status As String
        'Dim print_payment_name As String



        'sqljob = "SELECT labnumber, idpasien, totalprice, age, bloodsamplingdate1, bloodsamplingdate2, urinesamplingdate, fecessamplingdate, docname, docaddress, userid, id, status, datereceived, print, paymentstatus, discount, userfield1, userfield2, paymenttype, paymentname,printage FROM job where  labnumber='" & selectedLn & "'"
        'Dim grejob As New clsGreConnect
        'grejob.buildConn()

        'Dim cmdjob As New  SqlClient.SqlCommand(sqljob, grejob.grecon)
        'Dim rdrjob As  SqlDataReader = cmdjob.ExecuteReader(CommandBehavior.CloseConnection)
        'Do While rdrjob.Read
        '    If Not IsDBNull(rdrjob("datereceived")) Then
        '        print_datereceived = rdrjob("datereceived")
        '    End If

        '    If Not IsDBNull(rdrjob("docname")) Then
        '        print_datereceived = rdrjob("docname")
        '    End If

        '    If Not IsDBNull(rdrjob("paymentstatus")) Then
        '        print_payment_status = rdrjob("paymentstatus")
        '    End If

        '    If Not IsDBNull(rdrjob("paymentname")) Then
        '        print_payment_name = rdrjob("paymentname")
        '    End If


        '    If Not IsDBNull(rdrjob("printage")) Then
        '        print_umur = rdrjob("printage")
        '    End If

        '    If Not IsDBNull(rdrjob("paymenttype")) Then
        '        print_payment_type = rdrjob("paymenttype")
        '    End If

        'Loop




        ''=========================
        'ws.Cells("B5").Value = ": " & print_id_pasien
        'ws.Cells("B6").Value = ": " & print_nama
        'ws.Cells("B7").Value = ": " & print_umur
        'ws.Cells("B8").Value = ": " & print_telepon
        'ws.Cells("B9").Value = ": " & print_dokter

        'ws.Cells("H5").Value = ": " & print_labnumber
        ''ws.Cells("H6").Style.WrapText = True
        'ws.Cells("H6").Value = ": " & print_datereceived
        'ws.Cells("H7").Value = ": " & print_address

        ''ws.Cells("H5").Value = ": " & tgllahir & " / " & age & "thn"
        ''ws.Cells("H6").Value = ": " & selectedLabNumber

        'Dim row As Integer = 12
        'Dim col As Integer = 0

        '' jika ada testpack maka dikeluarkan dahulu yg bukan pack---
        ''select testtype.keterangan,testtype.testname,jobdetail.idtesttype,jobdetail.price from testtype,jobdetail where testtype.id=jobdetail.idtesttype and labnumber='134' and testtype.id not in (select testpack.idtest from testpack,jobpack where jobpack.labnumber='135' and jobpack.idpack=testpack.idpack)
        ''-----------------------------------------------------------
        'Dim sqlanalisa As String
        'Dim strpack As String
        'Dim strinsidepack As String

        'Dim print_testname As String
        'Dim print_price As Double
        'Dim tempackid As Integer


        'If enek_opo_ora_packe(selectedLn) = True Then
        '    'tampilkan dahulu tanpa testpack -----------------
        '    sqlanalisa = "select testtype.keterangan,testtype.testname,jobdetail.idtesttype,jobdetail.price from testtype,jobdetail where testtype.id=jobdetail.idtesttype and labnumber='" & selectedLn & "' and testtype.id not in (select testpack.idtest from testpack,jobpack where jobpack.labnumber='" & selectedLn & "' and jobpack.idpack=testpack.idpack)"
        '    Dim greanalisa As New clsGreConnect
        '    greanalisa.buildConn()
        '    Dim cmdanalisa As New  SqlClient.SqlCommand(sqlanalisa, greanalisa.grecon)
        '    Dim rdranalisa As  SqlDataReader = cmdanalisa.ExecuteReader(CommandBehavior.CloseConnection)
        '    Do While rdranalisa.Read
        '        If Not IsDBNull(rdranalisa("testname")) Then
        '            print_testname = rdranalisa("testname")
        '        End If
        '        If Not IsDBNull(rdranalisa("price")) Then
        '            print_price = rdranalisa("price")
        '        End If
        '        ws.Cells(row, 0).Value = print_testname
        '        ws.Cells(row, 0).Style.WrapText = False
        '        ws.Cells(row, 6).Value = print_price
        '        ' ws.Cells(row, 6).Style.NumberFormat = "#.##0"
        '        row = row + 1
        '        If row = 38 Then
        '            row = 60
        '        ElseIf row = 83 Then
        '            row = 104
        '        End If
        '    Loop
        '    'tampilkan harga pack dan dibawahnya testitemnya-----
        '    '----------------------------------------------------

        '    strpack = "select jobpack.packname,jobpack.pricepack as price,idpack from jobpack where jobpack.labnumber='" & selectedLn & "'"
        '    Dim grepack As New clsGreConnect
        '    grepack.buildConn()
        '    Dim cmdpack As New  SqlClient.SqlCommand(strpack, grepack.grecon)
        '    Dim rdrpack As  SqlDataReader = cmdpack.ExecuteReader(CommandBehavior.CloseConnection)
        '    Do While rdrpack.Read
        '        If Not IsDBNull(rdrpack("packname")) Then
        '            print_testname = rdrpack("packname")
        '        End If
        '        If Not IsDBNull(rdrpack("price")) Then
        '            print_price = rdrpack("price")
        '        End If
        '        ws.Cells(row, 0).Value = print_testname
        '        ws.Cells(row, 0).Style.WrapText = False
        '        ws.Cells(row, 6).Value = print_price
        '        row = row + 1
        '        If row = 38 Then
        '            row = 60
        '        ElseIf row = 83 Then
        '            row = 104
        '        End If

        '        If Not IsDBNull(rdrpack("idpack")) Then
        '            tempackid = rdrpack("idpack")
        '            'cari test item yg ada di pack ini dan cetak
        '            strinsidepack = "select testtype.analyzertestname as testname from testtype,testpack where testpack.idpack='" & tempackid & "' and testpack.idtest=testtype.id"
        '            Dim greinside As New clsGreConnect
        '            greinside.buildConn()
        '            Dim cmdinside As New  SqlClient.SqlCommand(strinsidepack, greinside.grecon)
        '            Dim rdrinside As  SqlDataReader = cmdinside.ExecuteReader(CommandBehavior.CloseConnection)
        '            Do While rdrinside.Read
        '                If Not IsDBNull(rdrinside("testname")) Then
        '                    print_testname = rdrinside("testname")
        '                End If
        '                'If Not IsDBNull(rdrinside("price")) Then
        '                print_price = 0 'rdrinside("price")
        '                'End If
        '                ws.Cells(row, 0).Value = "  " & print_testname
        '                ws.Cells(row, 0).Style.WrapText = False
        '                ws.Cells(row, 6).Value = "" 'print_price
        '                ' ws.Cells(row, 6).Style.NumberFormat = "#.##0"
        '                row = row + 1
        '                If row = 38 Then
        '                    row = 60
        '                ElseIf row = 83 Then
        '                    row = 104
        '                End If


        '            Loop
        '            cmdinside.Dispose()
        '            greinside.CloseConn()
        '        End If

        '    Loop

        'Else
        '    'tampilkan tanpa testpack
        '    sqlanalisa = "select testtype.keterangan,testtype.testname,jobdetail.idtesttype,jobdetail.price from testtype,jobdetail where testtype.id=jobdetail.idtesttype and labnumber='" & selectedLn & "'"
        '    Dim greanalisa As New clsGreConnect
        '    greanalisa.buildConn()
        '    Dim cmdanalisa As New  SqlClient.SqlCommand(sqlanalisa, greanalisa.grecon)
        '    Dim rdranalisa As  SqlDataReader = cmdanalisa.ExecuteReader(CommandBehavior.CloseConnection)
        '    Do While rdranalisa.Read
        '        If Not IsDBNull(rdranalisa("testname")) Then
        '            print_testname = rdranalisa("testname")
        '        End If
        '        If Not IsDBNull(rdranalisa("price")) Then
        '            print_price = rdranalisa("price")
        '        End If
        '        ws.Cells(row, 0).Value = print_testname
        '        ws.Cells(row, 0).Style.WrapText = False
        '        ws.Cells(row, 6).Value = print_price
        '        ' ws.Cells(row, 6).Style.NumberFormat = "#.##0"
        '        row = row + 1
        '        If row = 38 Then
        '            row = 60
        '        ElseIf row = 83 Then
        '            row = 104
        '        End If
        '    Loop
        'End If




        ''sqlanalisa = "select testtype.keterangan,testtype.testname,jobdetail.idtesttype,jobdetail.price from testtype,jobdetail where testtype.id=jobdetail.idtesttype and labnumber='" & selectedLn & "' and testtype.id not in (select testpack.idtest from testpack,jobpack where jobpack.labnumber='135' and jobpack.idpack=testpack.idpack)"


        ''SELECT labnumber, idpasien, totalprice, age, bloodsamplingdate1, bloodsamplingdate2, urinesamplingdate, fecessamplingdate, docname, docaddress, userid, id, status, datereceived, print, paymentstatus, discount, userfield1, userfield2, paymenttype, paymentname FROM job where  labnumber=@parlabnumber
        ''SELECT patientname, address, malefemale, phone, idpatient, email, id, birthdate FROM patient where id=@parpatientid
        ''select testtype.keterangan,testtype.testname,jobdetail.idtesttype,jobdetail.price from testtype,jobdetail where testtype.id=jobdetail.idtesttype and labnumber=@parjobdetaillabnumber
        'If row < 60 Then
        '    ws.Cells("A37").Value = "Terbilang"
        '    ws.Cells("B37").Value = ": " & print_terbilang
        '    ws.Cells("A40").Value = "Pembayaran"
        '    ws.Cells("G44").Value = "Petugas"
        '    ws.Cells("B41").Value = ": " & print_payment_name
        '    ws.Cells("G41").Value = ": " & FormatNumber(rptotal, 0)
        '    ws.Cells("G46").Value = user_complete_name
        '    Dim cr As CellRange = ws.Cells.GetSubrange("A38", "I38")
        '    cr.Style.Borders.SetBorders(MultipleBorders.Bottom, SpreadsheetColor.FromName(ColorName.Black), LineStyle.Thick)

        'ElseIf row > 60 Then
        '    ws.Cells("A84").Value = "Terbilang"
        '    ws.Cells("B84").Value = ": " & print_terbilang
        '    ws.Cells("A87").Value = "Pembayaran"
        '    ws.Cells("G91").Value = "Petugas"
        '    ws.Cells("B88").Value = ": " & print_payment_name
        '    ws.Cells("G88").Value = ": " & rptotal
        '    ' ws.Cells("G88").Style.NumberFormat = "#.##0"
        '    ws.Cells("G93").Value = user_complete_name
        '    Dim cr As CellRange = ws.Cells.GetSubrange("A85", "I85")
        '    cr.Style.Borders.SetBorders(MultipleBorders.Bottom, SpreadsheetColor.FromName(ColorName.Black), LineStyle.Thick)
        'End If


        'ef.Save("notahasil.xlsx")
        'Dim xpsDoc = ef.ConvertToXpsDocument(SaveOptions.XpsDefault)
        'Dim objDocView As New notaDisplayFrm
        'objDocView.StartPosition = FormStartPosition.Manual
        'objDocView.Left = 0
        'objDocView.Top = 0
        'objDocView.ShowDialog()




        '===================================



        'Dim objNotaxls As New NotaExlFrm
        'objNotaxls.ShowDialog()
        'untuk load excel

        'Dim rptNota As New rptNotaPemeriksaanFrm(selectedLn, selectedPatientId, Trim(lblterbilang.Text), beforediscount, discount, afterdiscount, strdatereport, strjenispembayaran)
        'rptNota.MdiParent = MainFrm
        'rptNota.StartPosition = FormStartPosition.Manual
        'rptNota.Top = 0
        'rptNota.Left = 0
        'rptNota.Show()



    End Sub

    Private Function enek_opo_ora_packe(ByVal lne As String) As Boolean
        Dim strcari As String
        Dim enekopoora As Boolean
        enekopoora = False

        Dim akehe As Integer

        strcari = "select count(idpack) as enek from jobpack where labnumber='" & lne & "'"

        Dim greObject As New clsGreConnect
        greObject.buildConn()

        Dim thecommand As New SqlClient.SqlCommand(strcari, greObject.grecon)
        Dim theReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)
        Do While theReader.Read
            akehe = theReader("enek")
        Loop

        theReader.Close()
        thecommand.Dispose()
        greObject.CloseConn()
        greObject = Nothing

        If akehe = 0 Then
            Return False
        ElseIf akehe > 0 Then
            Return True
        End If


    End Function

    Private Sub txtuangdibayar_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtuangdibayar.KeyDown
        If e.KeyValue = Keys.Return Then

            CekPembayaran()
        End If
    End Sub
    Private Sub CekPembayaran()
        txtkembali.ReadOnly = False
        Dim uangdibayar As Double
        uangdibayar = CDbl(Trim(txtuangdibayar.Text))
        If uangdibayar < rptotal Then
            MessageBox.Show("Pembayaran kurang")
            Return
        End If
        txtkembali.Text = FormatNumber(uangdibayar - rptotal, 0)
        txtkembali.ReadOnly = True

    End Sub

    Private Sub txtuangdibayar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtuangdibayar.KeyPress
        If Char.IsDigit(e.KeyChar) = False And Char.IsControl(e.KeyChar) = False Then
            e.Handled = True
            MsgBox("Masukkan Angka") 'bagus
        End If
    End Sub





    Private Sub txtuangdibayar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtuangdibayar.TextChanged

    End Sub

    Private Sub txtblmbulat_TextChanged(sender As Object, e As EventArgs) Handles txtblmbulat.TextChanged

    End Sub
End Class