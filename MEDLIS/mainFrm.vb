﻿Imports DevExpress.XtraReports.UI
Imports Npgsql
Imports System.Data.SqlClient


Public Class MainFrm

    Private Sub MainFrm_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub MainFrm_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        CloseLicense()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        IsMdiContainer = True
        For Each mnu As ToolStripMenuItem In menuStrip1.Items
            mnu.Enabled = False
        Next
        loginMnu.Enabled = True

        'palio hide
        palio100Menu.Visible = False
        diruiHema3000Mnu.Visible = False

        CekLicense()
        ReadLicense()
        ReadVersion()
        Counter_Trial()
    End Sub

    Public Sub enablemenu()
        For Each mnu As ToolStripMenuItem In menuStrip1.Items
            mnu.Enabled = True

            If isValidInstrumentUser Then
                dirui240Menu.Enabled = True
            Else
                dirui240Menu.Enabled = False
            End If

            If isValidRegisterUser Then
                pasienMnu.Enabled = True
            Else
                pasienMnu.Enabled = False
            End If

            If isValidPembayaranUser Then
                pembayaranMnu.Enabled = True
            Else
                pembayaranMnu.Enabled = False
            End If

            If isValidSettingUser Then
                settingMnu.Enabled = True
                masterMnu.Enabled = True
                managementMnu.Enabled = True
            Else
                settingMnu.Enabled = False
                masterMnu.Enabled = False
                managementMnu.Enabled = False
            End If

            If isvalidResultuser Then
                FinishJobToolStripMenuItem.Enabled = True
                TestStatusToolStripMenuItem.Enabled = True
            Else
                FinishJobToolStripMenuItem.Enabled = False
                TestStatusToolStripMenuItem.Enabled = False
            End If


        Next
        loginMnu.Text = "Logout"
        loginMnu.Image = MEDLIS.My.Resources.Resources.lock_open

    End Sub





    Sub MainLoad(ByVal sender As Object, ByVal e As EventArgs)

    End Sub

    Sub LoginMnuClick(ByVal sender As Object, ByVal e As EventArgs) Handles loginMnu.Click
        'active
        If Not isValidUser Then
            Dim frmLogin As New loginFrm
            frmLogin.MdiParent = Me
            frmLogin.StartPosition = FormStartPosition.Manual
            frmLogin.Left = 0
            frmLogin.Top = 0
            frmLogin.Show()
        Else
            isValidUser = False
            isActiveUser = False
            For Each mnu As ToolStripMenuItem In menuStrip1.Items
                mnu.Enabled = False
            Next
            For Each frm As Form In Me.MdiChildren
                frm.Close()
            Next
            loginMnu.Image = MEDLIS.My.Resources.Resources.lock_forbidden_hover
            loginMnu.Enabled = True
            loginMnu.Text = "Login"
            developerMode = 0
        End If


    End Sub



    Sub MnuRegistrationClick(ByVal sender As Object, ByVal e As EventArgs) Handles jobMnu.Click

    End Sub

    Sub RegistrasiBaruToolStripMenuItemClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim Registration As New frmRegistration
        Registration.Show()

        Registration.MdiParent = Me
    End Sub

    Sub CariPasienToolStripMenuItemClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim findPatient As New cariPasienFrm
        findPatient.Show()
        findPatient.MdiParent = Me
    End Sub

    Sub TestTypeToolStripMenuItemClick(ByVal sender As Object, ByVal e As EventArgs) Handles testTypeToolStripMenuItem.Click
        'active
        Dim testType As New testTypeFrm
        testType.StartPosition = FormStartPosition.Manual
        testType.Left = 0
        testType.Top = 0
        testType.MdiParent = Me
        testType.Show()

    End Sub


    Sub TestGroupToolStripMenuItemClick(ByVal sender As Object, ByVal e As EventArgs) Handles testGroupToolStripMenuItem.Click
        'active
        Dim testgroup As New testGroupFrm
        testgroup.StartPosition = FormStartPosition.Manual
        testgroup.Left = 0
        testgroup.Top = 0
        testgroup.MdiParent = Me

        testgroup.Show()


    End Sub




    Private Sub Instrument2ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim frmrs232 As New Form1
        'frmrs232.MdiParent = Me
        'frmrs232.Show()
    End Sub

    Private Sub pasienMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pasienMnu.Click
        'active



    End Sub

    Private Sub TestToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'active
        Dim frmCodetest As New codeTestfrm
        frmCodetest.MdiParent = Me
        frmCodetest.Show()
    End Sub

    Private Sub InstrumentTmrToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'active
        Dim frmSpTimer As New frmConnectSPTimer
        frmSpTimer.StartPosition = FormStartPosition.Manual
        frmSpTimer.Left = 0
        frmSpTimer.Top = 0

        frmSpTimer.MdiParent = Me
        frmSpTimer.Show()


    End Sub



    Private Sub pengambilanSampleMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'active


    End Sub

    Private Sub TestStatusToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TestStatusToolStripMenuItem.Click
        ReadLicense()
        'active
        Dim frmalljob As New AlljobsFrm(False)
        frmalljob.MdiParent = Me
        frmalljob.StartPosition = FormStartPosition.Manual
        frmalljob.Left = 0
        frmalljob.Top = 0
        frmalljob.Show()

    End Sub

    Private Sub SubreportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rpt As New rptSubreport
        rpt.MdiParent = Me
        rpt.Show()

    End Sub

    Private Sub databaseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles databaseToolStripMenuItem.Click
        'active
        Dim dbsetting As New DBSettingFrm
        dbsetting.MdiParent = Me
        dbsetting.Show()

    End Sub

    Private Sub userToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles userToolStripMenuItem.Click
        'active
        Dim ouserfrm As New frmUser
        ouserfrm.StartPosition = FormStartPosition.Manual
        ouserfrm.Left = 0
        ouserfrm.Top = 0
        ouserfrm.MdiParent = Me
        ouserfrm.Show()

    End Sub
    Private Sub FinishJobToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FinishJobToolStripMenuItem.Click
        'active
        'Dim objallfinish As New AllFinishJobsFrm
        Dim objallfinish As New excellAllFinishJobsFrm
        objallfinish.MdiParent = Me
        objallfinish.StartPosition = FormStartPosition.Manual
        objallfinish.WindowState = FormWindowState.Maximized
        objallfinish.Left = 0
        objallfinish.Top = 0
        objallfinish.Show()

    End Sub

    Private Sub ReportToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'active
        Dim oReportSetting As New ReportLabSettingFrm
        oReportSetting.MdiParent = Me
        oReportSetting.StartPosition = FormStartPosition.Manual
        oReportSetting.Left = 0
        oReportSetting.Top = 0
        oReportSetting.Show()
    End Sub

    Private Sub InstrumentTestConfigurationToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InstrumentTestConfigurationToolStripMenuItem.Click
        'active
        Dim frmMachineMapJob As New machineMapJob
        frmMachineMapJob.MdiParent = Me
        frmMachineMapJob.StartPosition = FormStartPosition.Manual
        frmMachineMapJob.Left = 0
        frmMachineMapJob.Top = 0

        frmMachineMapJob.Show()
    End Sub

    Private Sub CodeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'active
        Dim codeo As New codetest2
        codeo.MdiParent = Me
        codeo.Show()
    End Sub

    Private Sub InstrumentConnectionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InstrumentConnectionToolStripMenuItem.Click
        'active
        Dim oInstrConnect As New InstrumentConnectProperty
        oInstrConnect.MdiParent = Me
        oInstrConnect.StartPosition = FormStartPosition.Manual
        oInstrConnect.Left = 0
        oInstrConnect.Top = 0
        oInstrConnect.Show()
    End Sub


    Private Sub aboutMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles aboutMnu.Click
        'active
        Dim oAbout As New AboutMEDLIS
        oAbout.MdiParent = Me
        oAbout.StartPosition = FormStartPosition.CenterScreen
        oAbout.Show()

    End Sub

    Private Sub helpToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'active
    End Sub

    Private Sub DiscountPaymentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim oDiscPayment As New KonfigureDiskonFrm
        oDiscPayment.MdiParent = Me
        oDiscPayment.StartPosition = FormStartPosition.Manual
        oDiscPayment.Left = 0
        oDiscPayment.Top = 0
        oDiscPayment.Show()
    End Sub

    Private Sub PaymentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim olistnotpaid As New ListNotPaidFrm
        olistnotpaid.MdiParent = Me
        olistnotpaid.StartPosition = FormStartPosition.Manual
        olistnotpaid.Left = 0
        olistnotpaid.Top = 0

        olistnotpaid.Show()

        'Dim opayment As New Payment("ln001")
        'opayment.MdiParent = Me
        'opayment.StartPosition = FormStartPosition.Manual
        'opayment.Left = 0
        'opayment.Top = 0
        'opayment.Show()

    End Sub

    Private Sub DaftarPasienToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim findPatient As New cariPasienFrm
        findPatient.MdiParent = Me
        findPatient.StartPosition = FormStartPosition.Manual
        findPatient.Left = 0
        findPatient.Top = 0
        findPatient.Show()
    End Sub


    Private Sub ModePembacaanSampleToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModePembacaanSampleToolStripMenuItem.Click
        Dim oAppSetting As New AppSettingFrm
        oAppSetting.MdiParent = Me
        oAppSetting.StartPosition = FormStartPosition.Manual

        oAppSetting.Left = 0
        oAppSetting.Top = 0
        oAppSetting.Show()
    End Sub

    Private Sub ManualToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ManualToolStripMenuItem.Click
        Dim oManualResult As New ManualResultFrm
        oManualResult.MdiParent = Me
        oManualResult.StartPosition = FormStartPosition.Manual
        oManualResult.Left = 0
        oManualResult.Top = 0
        oManualResult.Show()

    End Sub

    Private Sub TestDateCobaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim ocobadate As New cobaDate
        ocobadate.MdiParent = Me
        ocobadate.Show()
    End Sub

    Private Sub TestToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim otestref As New codeTestReference
        otestref.MdiParent = Me
        otestref.StartPosition = FormStartPosition.Manual
        otestref.Top = 0
        otestref.Left = 0
        otestref.Show()

    End Sub

    Private Sub ManagementReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles managementMnu.Click


    End Sub

    Private Sub AboutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub InstrumentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dirui240Menu.Click
        If is240WindowsOpen = 0 Then
            Dim ov3 As New v3frmConnectSPTimer
            ov3.MdiParent = Me
            ov3.StartPosition = FormStartPosition.Manual
            ov3.Top = 10
            ov3.Left = 0
            ov3.Show()
        Else
            MessageBox.Show("Silahkan tutup dahulu windows BM240")
        End If

    End Sub

    'Private Sub V2ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim ov2 As New v2frmConnectSPTimer
    '    ov2.MdiParent = Me
    '    ov2.StartPosition = FormStartPosition.Manual
    '    ov2.Top = 10
    '    ov2.Left = 0
    '    ov2.Show()

    'End Sub

    Private Sub V3ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim ov3 As New v3frmConnectSPTimer
        ov3.MdiParent = Me
        ov3.StartPosition = FormStartPosition.Manual
        ov3.Top = 10
        ov3.Left = 0
        ov3.Show()
    End Sub

    Private Sub PembayaranToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PembayaranToolStripMenuItem.Click
        Dim oDiscPayment As New KonfigureDiskonFrm
        oDiscPayment.MdiParent = Me
        oDiscPayment.StartPosition = FormStartPosition.Manual
        oDiscPayment.Left = 0
        oDiscPayment.Top = 0
        oDiscPayment.Show()
    End Sub

    Private Sub PembayaranToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pembayaranMnu.Click
        ReadLicense()
        If isPembayaranWinOpen = 0 Then
            Dim olistnotpaid As New ListNotPaidFrm
            olistnotpaid.MdiParent = Me
            olistnotpaid.StartPosition = FormStartPosition.Manual
            olistnotpaid.Left = 0
            olistnotpaid.Top = 0
            olistnotpaid.Show()
        Else
            MessageBox.Show("Halaman list pembayaran sudah terbuka")
        End If

    End Sub

    Private Sub PengambilanSampleToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PengambilanSampleToolStripMenuItem.Click
        If isSamplingWinOpen = 0 Then

            Dim frmSampling As New samplingFrm
            frmSampling.WindowState = FormWindowState.Maximized
            'frmSampling.Left = 0
            'frmSampling.Top = 0
            frmSampling.MdiParent = Me
            frmSampling.Show()
        Else
            MessageBox.Show("Halaman pengambilan sample sudah terbuka, silahkan tutup dahulu.")
        End If

    End Sub

    Private Sub ReportPerTanggalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReportPerTanggalToolStripMenuItem.Click
        Dim oDaftarJobSelesai As New listSuccessJobFrm
        oDaftarJobSelesai.MdiParent = Me
        oDaftarJobSelesai.StartPosition = FormStartPosition.Manual
        oDaftarJobSelesai.Left = 0
        oDaftarJobSelesai.Top = 0

        oDaftarJobSelesai.Show()
    End Sub

    Private Sub ReportPerTestItemToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReportPerTestItemToolStripMenuItem.Click

        Dim oDaftarTestSelesai As New listSuccessTestFrm
        oDaftarTestSelesai.MdiParent = Me
        oDaftarTestSelesai.StartPosition = FormStartPosition.Manual
        oDaftarTestSelesai.Left = 0
        oDaftarTestSelesai.Top = 0

        oDaftarTestSelesai.Show()
    End Sub

    Private Sub PasienPendaftaranToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PasienPendaftaranToolStripMenuItem.Click
        ReadLicense()
        If isPatientWinOpen = 0 Then
            Dim findPatient As New cariPasienFrm
            findPatient.MdiParent = Me
            findPatient.StartPosition = FormStartPosition.Manual
            findPatient.Left = 0
            findPatient.Top = 0
            findPatient.Show()
        Else
            MessageBox.Show("Halaman pendaftaran pasien sudah terbuka")
        End If

    End Sub

    Private Sub RevisiToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RevisiToolStripMenuItem.Click
        Dim oRecheckVal As New reCheckValueFrm
        oRecheckVal.MdiParent = Me
        oRecheckVal.StartPosition = FormStartPosition.Manual
        oRecheckVal.Left = 0
        oRecheckVal.Top = 0
        oRecheckVal.Show()

    End Sub


    Private Sub MainFrm_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Dim iniReader As New iniReader
        iniReader.ReadIni()
        If Not (isIniPresent And isIniValid) Then
            loginMnu.Enabled = False
            DBSettingFrm.Show()
            DBSettingFrm.Activate()
        Else
            loginMnu.Enabled = True
        End If
    End Sub

    Public Sub EnableLoginButton()
        loginMnu.Enabled = True
    End Sub

    Private Sub TandaCetakHasilToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TandaCetakHasilToolStripMenuItem.Click
        Dim octkHasil As New printCheck
        octkHasil.MdiParent = Me
        octkHasil.StartPosition = FormStartPosition.Manual
        octkHasil.Left = 0
        octkHasil.Top = 0
        octkHasil.Show()

    End Sub

    Private Sub JobStuckInstrumentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles JobStuckInstrumentToolStripMenuItem.Click
        Dim ostuck As New stuckJobsFrm
        ostuck.MdiParent = Me
        ostuck.StartPosition = FormStartPosition.Manual

        ostuck.Left = 0
        ostuck.Top = 0
        ostuck.Show()
    End Sub

    Private Sub PalioTestConfigurationToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PalioTestConfigurationToolStripMenuItem.Click
        Dim frmPalioMachineMapJob As New PaliomachineMapJob
        frmPalioMachineMapJob.MdiParent = Me
        frmPalioMachineMapJob.StartPosition = FormStartPosition.Manual
        frmPalioMachineMapJob.Left = 0
        frmPalioMachineMapJob.Top = 0

        frmPalioMachineMapJob.Show()
    End Sub

    Private Sub PalioConnectionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PalioConnectionToolStripMenuItem.Click
        Dim palioConnect As New PalioConnectProperty
        palioConnect.MdiParent = Me
        palioConnect.StartPosition = FormStartPosition.Manual
        palioConnect.Left = 0
        palioConnect.Top = 0
        palioConnect.Show()

    End Sub


    Private Sub menuStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles menuStrip1.ItemClicked

    End Sub


    Private Sub ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles palio100Menu.Click
        If isPalioWindowsOpen = 0 Then
            Dim oPalio As New frmTcpClient
            oPalio.MdiParent = Me
            oPalio.StartPosition = FormStartPosition.Manual
            oPalio.Left = 0
            oPalio.Top = 0
            oPalio.Show()
        Else
            MessageBox.Show("Window Palio sudah terbuka, silahkan tutup dahulu")
        End If



    End Sub

    Private Sub PalioLaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim paliojozz As New palioSocketComm

        paliojozz.StartPosition = FormStartPosition.Manual
        paliojozz.MdiParent = Me
        paliojozz.Left = 0
        paliojozz.Top = 0
        paliojozz.Show()
    End Sub


    Private Sub ToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objFrmExcel As New excellAllFinishJobsFrm
        objFrmExcel.MdiParent = Me
        objFrmExcel.StartPosition = FormStartPosition.Manual
        objFrmExcel.Left = 0
        objFrmExcel.Top = 0

        objFrmExcel.Show()

    End Sub

    Private Sub TestBarcodeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TestBarcodeToolStripMenuItem.Click
        Dim objFrmBarcode As New BarcodeTest2
        objFrmBarcode.MdiParent = Me
        objFrmBarcode.StartPosition = FormStartPosition.Manual
        objFrmBarcode.Left = 0
        objFrmBarcode.Top = 0
        objFrmBarcode.Show()
    End Sub

    Private Sub SettingJumlahBarcodePrintingToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SettingJumlahBarcodePrintingToolStripMenuItem.Click
        Dim objjmlprint As New AppPrintSettingFrm
        objjmlprint.MdiParent = Me
        objjmlprint.StartPosition = FormStartPosition.Manual
        objjmlprint.Left = 0
        objjmlprint.Top = 0
        objjmlprint.Show()
    End Sub


    Private Sub HapusJobToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HapusJobToolStripMenuItem1.Click
        Dim hapusJOb As New DeleteJob
        hapusJOb.MdiParent = Me
        hapusJOb.Show()
    End Sub

    Private Sub PrintBarcodeUlangToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintBarcodeUlangToolStripMenuItem.Click
        Dim frmbc As New ReprintBarcode
        frmbc.MdiParent = Me
        frmbc.Show()
    End Sub







    Private Sub HemaConnectionToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim hemacon As New HemaConnectProperty
        hemacon.MdiParent = Me
        hemacon.Show()
    End Sub



    Private Sub Hema3ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Hema3ToolStripMenuItem.Click
        If isHema300Open = 0 Then
            Dim objHemaShow As New HemaThreadShowFrm
            objHemaShow.MdiParent = Me
            objHemaShow.StartPosition = FormStartPosition.Manual
            objHemaShow.Left = 0
            objHemaShow.Top = 0
            objHemaShow.Show()
        Else
            MessageBox.Show("Mohon tutup dahulu windows Hema3000")
        End If

    End Sub

    Private Sub ToolStripMenuItem2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objKomaFrm As New KomaFrm
        objKomaFrm.MdiParent = Me
        objKomaFrm.StartPosition = FormStartPosition.CenterParent
        objKomaFrm.Show()

    End Sub

    Private Sub DataHasilLengkapToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataHasilLengkapToolStripMenuItem.Click
        Dim objHemaFinishJob As New HemaAllFinishJobsFrm
        objHemaFinishJob.MdiParent = Me
        objHemaFinishJob.StartPosition = FormStartPosition.Manual
        objHemaFinishJob.Left = 0
        objHemaFinishJob.Top = 0
        objHemaFinishJob.Show()

    End Sub

    Private Sub masterMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles masterMnu.Click

    End Sub

    Private Sub SeetingHeaderLaporanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SeetingHeaderLaporanToolStripMenuItem.Click
        Dim objLabReportSetting As New ReportLabSettingFrm
        objLabReportSetting.MdiParent = Me
        objLabReportSetting.StartPosition = FormStartPosition.CenterScreen
        objLabReportSetting.Show()
    End Sub

    Private Sub HemaConneToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HemaConneToolStripMenuItem.Click
        Dim objHemaConnect As New HemaConnectProperty
        objHemaConnect.MdiParent = Me
        objHemaConnect.StartPosition = FormStartPosition.Manual
        objHemaConnect.Top = 0
        objHemaConnect.Left = 0
        objHemaConnect.Show()
    End Sub

    Private Sub LicenseManagerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LicenseManagerToolStripMenuItem.Click
        Dim objLicense As New licenseFrm
        objLicense.MdiParent = Me
        objLicense.Show()
    End Sub

    Private Sub ToolStripMenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem3.Click
        Dim objlevel As New priceLevelFrm
        objlevel.MdiParent = Me
        objlevel.StartPosition = FormStartPosition.Manual
        objlevel.Show()
    End Sub

    Private Sub ToolStripMenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub mnuSettingNumbering_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSettingNumbering.Click
        Dim objSettingNomor As New identitasSettingFrm
        objSettingNomor.MdiParent = Me
        objSettingNomor.StartPosition = FormStartPosition.Manual
        objSettingNomor.Show()
    End Sub

    Private Sub ArchiveDataToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ArchiveDataToolStripMenuItem.Click
        Dim objArchive As New archiveJobFrm
        objArchive.MdiParent = Me
        objArchive.StartPosition = FormStartPosition.CenterScreen
        objArchive.Show()
    End Sub

    Private Sub TestMethodeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TestMethodeToolStripMenuItem.Click
        Dim objtestpack As New testPack
        objtestpack.WindowState = FormWindowState.Maximized
        objtestpack.MdiParent = Me
        objtestpack.Show()

    End Sub

    Private Sub ToolStripMenuItem2_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objTestRange As New testMinMax
        objTestRange.MdiParent = Me
        objTestRange.WindowState = FormWindowState.Maximized
        objTestRange.Show()

    End Sub

    Private Sub CekDBUpdateToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CekDBUpdateToolStripMenuItem.Click
        Dim objDbUpdate As New UpdateDBDataFrm
        objDbUpdate.MdiParent = Me
        objDbUpdate.Show()
    End Sub

    Private Sub settingMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles settingMnu.Click

    End Sub

    Private Sub mindray3600Mnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mindray3600Mnu.Click
        'If isMindRayBC3600 = 0 Then
        '    Dim objMindRay3600Show As New MindRayHema3651
        '    objMindRay3600Show.MdiParent = Me
        '    objMindRay3600Show.StartPosition = FormStartPosition.Manual
        '    objMindRay3600Show.Left = 0
        '    objMindRay3600Show.Top = 0
        '    objMindRay3600Show.Show()
        'Else
        '    MessageBox.Show("Mohon tutup dahulu windows MindRayBC3600")
        'End If

    End Sub


    Private Sub DataHasilLengkapToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataHasilLengkapToolStripMenuItem1.Click
        Dim objHemaFinishJobMindRay As New hemaAllFinishMindRay
        objHemaFinishJobMindRay.MdiParent = Me
        objHemaFinishJobMindRay.StartPosition = FormStartPosition.Manual
        objHemaFinishJobMindRay.Left = 0
        objHemaFinishJobMindRay.Top = 0
        objHemaFinishJobMindRay.Show()
    End Sub

    Private Sub MindRay5100ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MindRay5100ToolStripMenuItem.Click
        If isMindRayBC5100 = 0 Then
            Dim objMindRay5100 As New MindRay5100
            ' objMindRay5100.MdiParent = Me
            objMindRay5100.StartPosition = FormStartPosition.Manual
            objMindRay5100.Left = 0
            objMindRay5100.Top = 0
            objMindRay5100.Show()
        Else
            MessageBox.Show("Mohon tutup dahulu windows MindRay BC5100")
        End If

    End Sub

    Private Sub MindRay3100ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MindRay3100ToolStripMenuItem.Click
        If isMindRayBC3600 = 0 Then
            Dim objMindRay3600Show As New MindRayHema3651
            ' objMindRay3600Show.MdiParent = Me
            objMindRay3600Show.StartPosition = FormStartPosition.Manual
            objMindRay3600Show.Left = 0
            objMindRay3600Show.Top = 0
            objMindRay3600Show.Show()
        Else
            MessageBox.Show("Mohon tutup dahulu windows MindRayBC3600")
        End If
    End Sub

    Private Sub EntryManualTestToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objManualEntry As New testManualEntry
        objManualEntry.MdiParent = Me
        objManualEntry.Show()

    End Sub

    Private Sub ToolStripMenuItem1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem1.Click
        Dim objBM As New MRBloodMessageSetting
        objBM.MdiParent = Me
        objBM.Show()

    End Sub

    Private Sub MRHemaLogToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MRHemaLogToolStripMenuItem.Click
        Dim objlog As New logMRHema
        objlog.MdiParent = Me
        objlog.Show()

    End Sub

    Private Sub diruiHema3000Mnu_Click(sender As Object, e As EventArgs) Handles diruiHema3000Mnu.Click

    End Sub

    Private Sub mnutest_Click(sender As Object, e As EventArgs) Handles mnutest.Click
        Dim rpt As New PatientResult
        'rpt.Parameters("IdPatient").Value = "93"
        'rpt.Parameters("IdPatient").Visible = False
        rpt.Parameters("LabNumber").Value = "82"
        rpt.Parameters("LabNumber").Visible = False

        Dim printTool As New ReportPrintTool(rpt)

        printTool.ShowPreviewDialog()
    End Sub
End Class
