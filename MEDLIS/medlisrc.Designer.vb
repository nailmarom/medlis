﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System

Namespace My.Resources
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option, or rebuild your VS project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute()>  _
    Friend Class medlisrc
        
        Private Shared resourceMan As Global.System.Resources.ResourceManager
        
        Private Shared resourceCulture As Global.System.Globalization.CultureInfo
        
        <Global.System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>  _
        Friend Sub New()
            MyBase.New
        End Sub
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Shared ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("MEDLIS.medlisrc", GetType(medlisrc).Assembly)
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Shared Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property allnewtest() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("allnewtest", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property allread() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("allread", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property allsampling() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("allsampling", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property alltest() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("alltest", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property bg_medlis2() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("bg-medlis2", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property bg_medlis3() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("bg-medlis3", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property bgindolis() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("bgindolis", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property blood11() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("blood11", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property book122() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("book122", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property books13() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("books13", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property check34() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("check34", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property chemistry10() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("chemistry10", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property clipboard45() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("clipboard45", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property clipboard46() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("clipboard46", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property delete30() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("delete30", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property edit26() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("edit26", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property equalizer16() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("equalizer16", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property family21() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("family21", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property floppy13() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("floppy13", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property floppy15() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("floppy15", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property floppy16() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("floppy16", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property folder83() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("folder83", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property folders4() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("folders4", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property folders41() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("folders41", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property gearwheels() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("gearwheels", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property lifeline9() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("lifeline9", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property locked59() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("locked59", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property locked591() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("locked591", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property login_graphic() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("login graphic", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property login_graphic2() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("login graphic2", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property medical38() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("medical38", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property medical50() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("medical50", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property medical54() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("medical54", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property old63() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("old63", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property old631() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("old631", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property person278() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("person278", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property plus32() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("plus32", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property positive3() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("positive3", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property print42() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("print42", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property print80() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("print80", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property screen8() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("screen8", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property search12() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("search12", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property task() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("task", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend Shared ReadOnly Property undo6() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("undo6", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
    End Class
End Namespace
