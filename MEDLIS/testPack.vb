﻿Imports System.Data.SqlClient

Public Class testPack
    Dim selectedgroupid As Integer
    Dim greobject As clsGreConnect
    Dim dtable As DataTable
    Dim selectedInstrumentName As String
    Dim selectedIdpack As Integer
    Dim isNew As Boolean
    Dim isEdit As Boolean

    Dim myPort As Array
    Private Sub PaliomachineMapJob_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FirstLoad()
    End Sub

    Private Sub FirstLoad()

        isNew = False
        isEdit = False
        CekState()

        dgvinstrument.RowHeadersVisible = False
        dgvinstrument.AllowUserToAddRows = False

        dgvtestname.RowHeadersVisible = False
        dgvtestname.AllowUserToAddRows = False
        'dgvtestname.Rows.Clear()
        InstrumentList()
        selectedInstrumentName = ""
        txtinstrument.ReadOnly = True
        txtdesc.ReadOnly = True

    End Sub


    Private Sub CekState()
        If isNew = False And isEdit = False Then
            AddMnu.Enabled = True
            SaveMnu.Enabled = False
            EditMnu.Enabled = True

            Dim k As Integer
            For k = 0 To dgvtestname.Rows.Count - 1
                dgvtestname.Rows(k).ReadOnly = True
            Next


        ElseIf isNew = True And isEdit = False Then
            SaveMnu.Enabled = True
            AddMnu.Enabled = False
            EditMnu.Enabled = False
        ElseIf isNew = False And isEdit = True Then
            SaveMnu.Enabled = True
            AddMnu.Enabled = False
            EditMnu.Enabled = False
        End If


    End Sub



 
    Private Sub PopulateDgTest()

        greobject = New clsGreConnect
        greobject.buildConn()

        dtable = New DataTable
        dtable = greobject.ExecuteQuery("select * from testtype") ' where idpack='" & selectedgroupid & "' order by testname asc")

        dgvtestname.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvtestname.Columns.Count - 1
            dgvtestname.Columns(j).Visible = False
        Next

        If Not dgvtestname.Columns.Contains("chk") Then
            Dim chk As New DataGridViewCheckBoxColumn()

            dgvtestname.Columns.Add(chk)
            chk.HeaderText = "SELECT"
            chk.Width = 120
            chk.Name = "chk"

            dgvtestname.Columns("chk").DisplayIndex = 0
            dgvtestname.Columns("chk").Name = "chk"
        End If

        dgvtestname.Columns("chk").Visible = True

        dgvtestname.Columns("testname").Visible = True
        dgvtestname.Columns("testname").HeaderText = "Test Name"
        dgvtestname.Columns("testname").Width = 200

        dgvtestname.Columns("analyzertestname").Visible = True
        dgvtestname.Columns("analyzertestname").HeaderText = "Analyzer Test Name"
        dgvtestname.Columns("analyzertestname").Width = 200

        greobject.CloseConn()
        greobject = Nothing

    End Sub

    Private Sub InstrumentList()

        'if you ever add column manually, better to clear before reconstructing
        dgvtestname.RowHeadersVisible = False
        dgvtestname.AllowUserToAddRows = False



        txtinstrument.ReadOnly = True
        txtdesc.ReadOnly = True

        dgvinstrument.DataSource = Nothing
        If dgvinstrument.Columns.Contains("Detail") Then
            dgvinstrument.Columns.Clear()
        End If
        If dgvinstrument.Rows.Count > 0 Then
            dgvinstrument.Rows.Clear()
        End If



        Dim strsql As String
        strsql = "select distinct idpack,packname,remark,ispackprice,pricepack,pricepack2,pricepack3,pricepack4,pricepack5,pricepack6 from testpack order by packname asc"

        greobject = New clsGreConnect
        greobject.buildConn()

        dtable = New DataTable
        dtable = greobject.ExecuteQuery(strsql)

        dgvinstrument.DataSource = dtable
        Dim j As Integer
        For j = 0 To dgvinstrument.Columns.Count - 1
            dgvinstrument.Columns(j).Visible = False
        Next

        dgvinstrument.Columns("idpack").Visible = False
        dgvinstrument.Columns("idpack").HeaderText = "Name"
        dgvinstrument.Columns("idpack").Width = 130

        dgvinstrument.Columns("packname").Visible = True
        dgvinstrument.Columns("packname").HeaderText = "Name"
        dgvinstrument.Columns("packname").Width = 130

        dgvinstrument.Columns("remark").Visible = True
        dgvinstrument.Columns("remark").HeaderText = "Desc"
        dgvinstrument.Columns("remark").Width = 210

        Dim buttonColumn As New DataGridViewButtonColumn()
        With buttonColumn
            .Name = "Detail"
            .HeaderText = "Detail"
            .Width = 60
            .Text = "Detail"
            .UseColumnTextForButtonValue = True
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            .FlatStyle = FlatStyle.Standard
            .CellTemplate.Style.BackColor = Color.Yellow
            .DisplayIndex = 0
        End With
        dgvinstrument.Columns.Add(buttonColumn)

        dgvinstrument.AllowUserToAddRows = False
        dgvinstrument.RowHeadersVisible = False
    End Sub


    Private Sub dgvinstrument_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvinstrument.CellContentClick
        Dim idx As Integer

        Dim idpack As Integer
        Dim instrName As String
        Dim instrDesc As String

        idx = dgvinstrument.Columns("Detail").Index
        ' txtlabnum.Text = ""
        If e.RowIndex < 0 OrElse Not e.ColumnIndex = dgvinstrument.Columns("Detail").Index Then

            Return

        Else

            If Not dgvinstrument.CurrentRow.IsNewRow Then

                instrName = dgvinstrument.Item("packname", dgvinstrument.CurrentRow.Index).Value
                instrDesc = dgvinstrument.Item("remark", dgvinstrument.CurrentRow.Index).Value
                idpack = dgvinstrument.Item("idpack", dgvinstrument.CurrentRow.Index).Value

                If Not IsDBNull(dgvinstrument.Item("pricepack", dgvinstrument.CurrentRow.Index).Value) Then
                    txtprice.Text = dgvinstrument.Item("pricepack", dgvinstrument.CurrentRow.Index).Value
                Else
                    txtprice.Text = 0
                End If


                If Not IsDBNull(dgvinstrument.Item("pricepack2", dgvinstrument.CurrentRow.Index).Value) Then
                    hargaLevel2.Text = dgvinstrument.Item("pricepack2", dgvinstrument.CurrentRow.Index).Value
                Else
                    hargaLevel2.Text = 0
                End If
                If Not IsDBNull(dgvinstrument.Item("pricepack3", dgvinstrument.CurrentRow.Index).Value) Then
                    hargaLevel3.Text = dgvinstrument.Item("pricepack3", dgvinstrument.CurrentRow.Index).Value
                Else
                    hargaLevel3.Text = 0
                End If
                If Not IsDBNull(dgvinstrument.Item("pricepack4", dgvinstrument.CurrentRow.Index).Value) Then
                    hargaLevel4.Text = dgvinstrument.Item("pricepack4", dgvinstrument.CurrentRow.Index).Value
                Else
                    hargaLevel4.Text = 0
                End If
                If Not IsDBNull(dgvinstrument.Item("pricepack5", dgvinstrument.CurrentRow.Index).Value) Then
                    hargaLevel5.Text = dgvinstrument.Item("pricepack5", dgvinstrument.CurrentRow.Index).Value
                Else
                    hargaLevel5.Text = 0
                End If
                If Not IsDBNull(dgvinstrument.Item("pricepack6", dgvinstrument.CurrentRow.Index).Value) Then
                    hargaLevel6.Text = dgvinstrument.Item("pricepack6", dgvinstrument.CurrentRow.Index).Value
                Else
                    hargaLevel6.Text = 0
                End If

                If Not IsDBNull(dgvinstrument.Item("ispackprice", dgvinstrument.CurrentRow.Index).Value) Then
                    If dgvinstrument.Item("ispackprice", dgvinstrument.CurrentRow.Index).Value = 1 Then
                        chkfixprice.Checked = True
                    Else
                        chkfixprice.Checked = False
                    End If
                Else
                    chkfixprice.Checked = False
                End If



                selectedIdpack = idpack
                selectedInstrumentName = instrName
                'txtinstrument.ReadOnly = False
                'txtdesc.ReadOnly = False
                disabled_text()



                txtinstrument.Text = instrName
                txtdesc.Text = instrDesc


                txtinstrument.ReadOnly = True
                txtdesc.ReadOnly = True


                ClearDGVtestname()
                ShowInstrumentAssignTest()
                ShowInstrumenCapabilities(idpack)
            End If

        End If
    End Sub
    Private Sub disabled_text()
        Dim ctrl As Control
        For Each ctrl In GroupBox2.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.ReadOnly = True
                'txt.BackColor = Color.LightYellow
            End If
        Next
    End Sub
    Private Sub clear_text()
        Dim ctrl As Control
        For Each ctrl In GroupBox2.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.ReadOnly = False
                txt.Text = ""
                'txt.BackColor = Color.LightYellow
            End If
        Next
    End Sub
    Private Sub enable_text()
        Dim ctrl As Control
        For Each ctrl In GroupBox2.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.Enabled = True
                txt.ReadOnly = False
                'txt.BackColor = Color.LightYellow
            End If
        Next
    End Sub

    Private Sub ClearDGVtestname()
        dgvtestname.DataSource = Nothing
        If dgvtestname.Columns.Contains("chk") Then
            dgvtestname.Columns.Clear()
        End If
        
        If dgvtestname.Rows.Count > 0 Then
            dgvtestname.Rows.Clear()
        End If

    End Sub
    Private Sub ShowInstrumentAssignTest()

        'load all test item and after that choose

        Dim strsqlins As String
        strsqlins = "select testtype.id,testtype.analyzertestname from testtype" ' instrument,testtype where name='" & instrName & "' and instrument.idtest=testtype.id"

        greobject = New clsGreConnect
        greobject.buildConn()

        dtable = New DataTable
        dtable = greobject.ExecuteQuery(strsqlins)

        dgvtestname.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvtestname.Columns.Count - 1
            dgvtestname.Columns(j).Visible = False
        Next

        If Not dgvtestname.Columns.Contains("chk") Then
            Dim chk As New DataGridViewCheckBoxColumn()

            dgvtestname.Columns.Add(chk)
            chk.HeaderText = "SELECT"
            chk.Width = 60
            chk.Name = "chk"

            dgvtestname.Columns("chk").DisplayIndex = 0
            dgvtestname.Columns("chk").Name = "chk"
        End If



        dgvtestname.Columns("chk").Visible = True

        ' dgvtestname.Columns("txt").ReadOnly = False


        dgvtestname.Columns("analyzertestname").Visible = True
        dgvtestname.Columns("analyzertestname").HeaderText = "Test Name"
        dgvtestname.Columns("analyzertestname").Width = 180

        greobject.CloseConn()
        greobject = Nothing

    End Sub

    Private Sub ShowInstrumenCapabilities(ByVal idpack As Integer)
        Dim strchoose As String
        Dim ocon As New clsGreConnect

        strchoose = "select testpack.idtest,testtype.analyzertestname from testpack,testtype where testpack.idpack='" & idpack & "' and testpack.idtest=testtype.id"
        ocon.buildConn()
        Dim cmdchoose As New SqlClient.SqlCommand(strchoose, ocon.grecon)
        Dim rdrchoose As SqlDataReader = cmdchoose.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdrchoose.Read
            For i = 0 To dgvtestname.Rows.Count - 1
                dgvtestname.Rows(i).ReadOnly = False
                If Not IsDBNull(rdrchoose("idtest")) Then
                    If dgvtestname.Item("id", i).Value = rdrchoose("idtest") Then
                        dgvtestname.Rows(i).ReadOnly = False
                        ' dgvtestname.Rows(i).DefaultCellStyle.BackColor = Color.Aquamarine
                        dgvtestname.Rows(i).Cells(2).Value = True   'check berada di nomor 2

                    End If
                End If
                dgvtestname.Rows(i).ReadOnly = True
            Next
        Loop

    End Sub


    'Private Sub PickJobAssign()
    '    Dim i As Integer
    '    Dim strsql As String

    '    Proses.CreateConn()
    '    strsql = "select * from TESTPACK where testpackid='" & testpackid & "'"
    '    Dim packCommand As New SqlCommand(strsql, Proses.directConn)
    '    Dim packReader As SqlDataReader = packCommand.ExecuteReader(CommandBehavior.CloseConnection)
    '    Do While packReader.Read

    '        For i = 0 To DGTestItem.Rows.Count - 1
    '            If Not IsDBNull(packReader("testitemid")) Then
    '                If DGTestItem.Item("testitemid", i).Value = packReader("testitemid") Then
    '                    DGTestItem.Rows(DGTestItem.Item("testitemid", i).RowIndex).ReadOnly = False
    '                    DGTestItem.Rows(DGTestItem.Item("testitemid", i).RowIndex).DefaultCellStyle.BackColor = Color.White
    '                    DGTestItem.Rows(DGTestItem.Item("testitemid", i).RowIndex).Cells(4).Value = False

    '                End If
    '            End If
    '        Next
    '    Loop

    '    packReader.Close()
    '    packCommand.Dispose()
    '    SqlClient.SqlConnection.ClearPool(Proses.directConn)
    'End Sub
    Private Function getNewPackID()
        Dim newpackid As Integer
        Dim strsql As String

        strsql = "select idpack from testpack order by idpack desc limit 1"
        newpackid = 1
        Dim dbcon As New clsGreConnect
        dbcon.BuildConn()
        Dim newpackCommand As New SqlClient.SqlCommand(strsql, dbcon.grecon)
        Dim newpackReader As SqlDataReader = newpackCommand.ExecuteReader(CommandBehavior.CloseConnection)

        If newpackReader.HasRows Then
            Do While newpackReader.Read
                newpackid = newpackReader("idpack") + 1
            Loop
            Return newpackid
        Else
            Return newpackid
        End If


    End Function

    Private Sub AddMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddMnu.Click
        isNew = True
        CekState()
        enable_text()
        txtinstrument.Text = ""
        txtdesc.Text = ""
        txtinstrument.Focus()

        ClearDGVtestname()
        ShowInstrumentAssignTest()

        txtdesc.ReadOnly = False
        txtinstrument.ReadOnly = False

    End Sub


    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click
        CekLicense()

        If Trim(txtinstrument.Text) = "" Then
            MessageBox.Show("Nama instrument tidak boleh kosong")
            Exit Sub
        End If

        If isNew Then
            If IsTheNameAlreadyThere(Trim(txtinstrument.Text)) = True Then
                MessageBox.Show("Nama test profile tersebut sudah ada, silahkan pilih nama yang lain")
                Exit Sub
            End If
            FillPackTable()


            isNew = False
            isEdit = False

            txtdesc.ReadOnly = True
            txtinstrument.ReadOnly = True

            For Each row As DataGridViewRow In dgvtestname.Rows
                If Not row.IsNewRow Then
                    dgvtestname.Rows(row.Index).Cells("chk").ReadOnly = True
                End If
            Next
            InstrumentList()
        ElseIf isEdit Then
            'update instrument 

            UpdateIntrumentTable()
            InstrumentList()
            isEdit = False
            isNew = False
            txtdesc.ReadOnly = True
            txtinstrument.ReadOnly = True
            disabled_text()
            'For Each row As DataGridViewRow In dgvtestname.Rows
            '    If Not row.IsNewRow Then
            '        dgvtestname.Rows(row.Index).Cells("txt").ReadOnly = True
            '    End If
            'Next


            For Each row As DataGridViewRow In dgvtestname.Rows
                If Not row.IsNewRow Then
                    dgvtestname.Rows(row.Index).Cells("chk").ReadOnly = True
                End If
            Next

        End If
        isEdit = False
        isNew = False
        CekState()
    End Sub
   


    Private Function IsTheNameAlreadyThere(ByVal instrumentName As String) As Boolean
        Dim ceksql As String
        ceksql = "select * from testpack where packname='" & instrumentName & "'"

        Dim ocek As New clsGreConnect
        ocek.buildConn()
        Dim cekcmd As New SqlClient.SqlCommand(ceksql, ocek.grecon)
        Dim cekrdr As SqlDataReader = cekcmd.ExecuteReader(CommandBehavior.CloseConnection)

        If cekrdr.HasRows = True Then
            ocek.CloseConn()
            Return True
        Else
            ocek.CloseConn()
            Return False
        End If

    End Function


    Private Sub UpdateIntrumentTable()
        'update consist of delete old value
        'and insert new rows
        Dim strdelete As String
        strdelete = "delete from testpack where packname='" & selectedInstrumentName & "'"

        Dim ogreDel As New clsGreConnect
        ogreDel.buildConn()
        ogreDel.ExecuteNonQuery(strdelete)
        ogreDel.CloseConn()

        FillPackTable()
        'UpdateInstrumentConnection()

    End Sub
    Private Sub UpdateInstrumentConnection()
        Dim ogreupdate As New clsGreConnect
        ogreupdate.buildConn()

        Dim strupdate As String
        strupdate = "update palioinstrumentconnect set name='" & Trim(txtinstrument.Text) & "' where name='" & selectedInstrumentName & "'"
        ogreupdate.ExecuteNonQuery(strupdate)
        ogreupdate.CloseConn()

    End Sub

    Private Sub FillPackTable()
        Dim i As Integer
        Dim selected_test_itemid As Integer
        Dim idpack As Integer
        Dim instrumentName As String
        Dim desc As String

        Dim ispackprice As Integer

        Dim pricepack As String
        Dim pricepack2 As String
        Dim pricepack3 As String
        Dim pricepack4 As String
        Dim pricepack5 As String
        Dim pricepack6 As String



        idpack = getNewPackID()
        instrumentName = Trim(txtinstrument.Text)
        desc = Trim(txtdesc.Text)
        If chkfixprice.Checked = True Then
            ispackprice = 1
            pricepack = txtprice.Text
            pricepack2 = hargaLevel2.Text
            pricepack3 = hargaLevel3.Text
            pricepack4 = hargaLevel4.Text
            pricepack5 = hargaLevel5.Text
            pricepack6 = hargaLevel6.Text
        Else
            ispackprice = 0
            pricepack = "0"
            pricepack2 = "0"
            pricepack3 = "0"
            pricepack4 = "0"
            pricepack5 = "0"
            pricepack6 = "0"
        End If

        For i = 0 To dgvtestname.Rows.Count - 1
            If dgvtestname.Item("chk", i).Value = True Then
                If Not IsDBNull(dgvtestname.Item("id", i).Value) Then
                    selected_test_itemid = dgvtestname.Item("id", i).Value
                    'barcodetest = dgvtestname.Item("txt", i).Value
                    Instrment_job_insert_one_per_row(idpack, instrumentName, desc, selected_test_itemid, ispackprice, pricepack, pricepack2, pricepack3, pricepack4, pricepack5, pricepack6) 'nyimpen capa
                End If
            End If
        Next

    End Sub
    Private Sub Instrment_job_insert_one_per_row(ByVal idpacknya As Integer, ByVal instrumentName As String, ByVal instrumentDesc As String, ByVal idtest As Integer, ByVal ispackprice As Integer, ByVal packprice As String, ByVal packprice2 As String, ByVal packprice3 As String, ByVal packprice4 As String, ByVal packprice5 As String, ByVal packprice6 As String)
        Dim strinsert As String
        Dim greInsert As New clsGreConnect
        greInsert.buildConn()
        Try
            strinsert = "insert into testpack(idpack,packname,remark,idtest,ispackprice,pricepack,pricepack2,pricepack3,pricepack4,pricepack5,pricepack6)values" & _
            "('" & idpacknya & "','" & instrumentName & "','" & instrumentDesc & "','" & idtest & "','" & ispackprice & "','" & packprice & "','" & packprice2 & "','" & packprice3 & "','" & packprice4 & "','" & packprice5 & "','" & packprice6 & "')"
            greInsert.ExecuteNonQuery(strinsert)
            greInsert.CloseConn()

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try

    End Sub




    Private Sub EditMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditMnu.Click
        isNew = False
        isEdit = True
        CekState()
        If Trim(txtinstrument.Text) = "" Then
            MessageBox.Show("Tidak ada data yang dipilih. Silahkan pilih salah satu instrument")
            isNew = False
            isEdit = False
            CekState()
        Else
            For Each row As DataGridViewRow In dgvtestname.Rows
                If Not row.IsNewRow Then
                    dgvtestname.Rows(row.Index).Cells("chk").ReadOnly = False
                End If
            Next
            enable_text()
           
        End If
    End Sub

    Private Sub dgvtestname_CellMouseLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvtestname.CellMouseLeave
        Button1.PerformClick()
    End Sub



    Private Sub dgvtestname_CellMouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvtestname.CellMouseUp
        Button1.PerformClick()
    End Sub

    Private Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

    End Sub

    Private Sub cancelMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelMnu.Click

        isNew = False
        isEdit = False
        CekState()
        txtdesc.Text = ""
        txtinstrument.Text = ""
        txtdesc.ReadOnly = True
        txtinstrument.ReadOnly = True
        clear_text()
        disabled_text()

        FirstLoad()

    End Sub

    Private Sub DeleteMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteMnu.Click
        Dim deletepalioname As String
        deletepalioname = Trim(txtinstrument.Text)
        If deletepalioname <> "" Then
            Dim delstr As String
            Dim ogre As New clsGreConnect
            ogre.buildConn()
            delstr = "delete from testpack where idpack='" & selectedIdpack & "'"
            ogre.ExecuteNonQuery(delstr)
            ogre.CloseConn()


            
            ClearDGVtestname()
            InstrumentList()
            txtinstrument.Text = ""
            txtdesc.Text = ""

        End If
    End Sub
    ReadOnly AllowedKeys As String = _
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 "
    Private Sub txtinstrument_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtinstrument.KeyPress, txtdesc.KeyPress
        Select Case e.KeyChar
            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)
        End Select
    End Sub

    
  
    ReadOnly AllowedKeysGrid As String = _
       "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    Private Sub dgvtestname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvtestname.KeyPress
        Select Case e.KeyChar
            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeysGrid.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeysGrid.Contains(e.KeyChar)
        End Select
    End Sub

    Private Sub chkfixprice_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkfixprice.CheckedChanged
        If chkfixprice.Checked = True Then
            txtprice.Enabled = True
        Else
            txtprice.Enabled = False
        End If
    End Sub
End Class