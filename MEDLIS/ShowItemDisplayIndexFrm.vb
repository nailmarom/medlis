﻿Imports System.Data

Public Class ShowItemDisplayIndexFrm
    Dim objconn As clsGreConnect
    Dim tblData As DataTable
    Dim strsql As String
    Private Sub ShowItemDisplayIndexFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
        LoadDgv()
    End Sub
    Private Sub LoadDgv()
        tblData = New DataTable
        dgvshow.DataSource = Nothing
        strsql = "select * from testtype"
        objconn = New clsGreConnect
        objconn.buildConn()
        tblData = objconn.ExecuteQuery(strsql) ' order by testgroupname asc")
        dgvshow.DataSource = tblData

        'Dim chk As New DataGridViewCheckBoxColumn()

        'dgvshow.Columns.Add(chk)
        'chk.HeaderText = "SELECT"
        'chk.Width = 70
        'chk.Name = "chk"
        'dgvshow.Columns("chk").DisplayIndex = 0
        'dgvshow.Columns("chk").Name = "chk"


        Dim j As Integer
        For j = 0 To dgvshow.Columns.Count - 1
            dgvshow.Columns(j).Visible = False
        Next

        'dgvshow.Columns("chk").DisplayIndex = 0
        'dgvshow.Columns("chk").Name = "chk"
        'dgvshow.Columns("chk").Visible = True

        dgvshow.Columns("analyzertestname").HeaderText = "Nama Test"
        dgvshow.Columns("analyzertestname").Width = 100
        dgvshow.Columns("analyzertestname").Visible = True
        dgvshow.Columns("analyzertestname").DisplayIndex = 1
        dgvshow.Columns("analyzertestname").ReadOnly = True

        dgvshow.Columns("testname").HeaderText = "Nama"
        dgvshow.Columns("testname").Width = 250
        dgvshow.Columns("testname").Visible = True
        dgvshow.Columns("testname").DisplayIndex = 2
        dgvshow.Columns("testname").ReadOnly = True

        dgvshow.Columns("displayindex").HeaderText = "Urutan"
        dgvshow.Columns("displayindex").Width = 50
        dgvshow.Columns("displayindex").Visible = True
        dgvshow.Columns("displayindex").DisplayIndex = 3
        dgvshow.Columns("displayindex").ReadOnly = True

        dgvshow.Columns("id").Visible = False
        objconn.CloseConn()


        dgvshow.RowHeadersVisible = False
        dgvshow.AllowUserToAddRows = False

    End Sub

End Class