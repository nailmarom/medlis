﻿Imports System.Data
Imports System.Data.SqlClient
Public Class KonfigureDiskonFrm
    Dim selectedDiskonId As Integer
    Dim isnew As Boolean
    Dim isedit As Boolean


    Private Sub KonfigureDiskonFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        isedit = False
        isnew = False
        Icon = New System.Drawing.Icon("medlis2.ico")
        AddMnu.Enabled = True
        SaveMnu.Enabled = False
        EditMnu.Enabled = False
        DeleteMnu.Enabled = False

        PopulateDiskonList()
        cancelMnu.Enabled = False

        txtPaymentName.ReadOnly = True
        txtdiscname.ReadOnly = True
        txtpercent.ReadOnly = True
        chkactive.Enabled = False
        dpstart.Enabled = False
        dpend.Enabled = False
        CekLicense()


    End Sub

    Private Sub PopulateDiskonList()
        Dim objconn As New clsGreConnect
        Dim tbldata As DataTable
        Dim strsql As String

        objconn.buildConn()
        tbldata = New DataTable
        dgvdiscount.DataSource = Nothing
        strsql = "select * from discountpayment"

        tbldata = objconn.ExecuteQuery(strsql) ' order by testgroupname asc")
        dgvdiscount.DataSource = tbldata

        Dim chk As New DataGridViewCheckBoxColumn()

        dgvdiscount.Columns.Add(chk)
        chk.HeaderText = "SELECT"
        chk.Width = 60
        chk.Name = "chk"
        dgvdiscount.Columns("chk").DisplayIndex = 0
        dgvdiscount.Columns("chk").Name = "chk"


        Dim j As Integer
        For j = 0 To dgvdiscount.Columns.Count - 1
            dgvdiscount.Columns(j).Visible = False
        Next

        dgvdiscount.Columns("chk").DisplayIndex = 0
        dgvdiscount.Columns("chk").Name = "chk"
        dgvdiscount.Columns("chk").Visible = True

        dgvdiscount.Columns("discountname").HeaderText = "Nama Program Diskon"
        dgvdiscount.Columns("discountname").Width = 230
        dgvdiscount.Columns("discountname").Visible = True
        dgvdiscount.Columns("discountname").DisplayIndex = 1
        dgvdiscount.Columns("discountname").ReadOnly = True

        dgvdiscount.Columns("percent").HeaderText = "Percent"
        dgvdiscount.Columns("percent").Width = 55
        dgvdiscount.Columns("percent").Visible = True
        dgvdiscount.Columns("percent").DisplayIndex = 2
        dgvdiscount.Columns("percent").ReadOnly = True

        dgvdiscount.Columns("startdate").HeaderText = "Start"
        dgvdiscount.Columns("startdate").Width = 100
        dgvdiscount.Columns("startdate").Visible = True
        dgvdiscount.Columns("startdate").DisplayIndex = 3
        dgvdiscount.Columns("startdate").ReadOnly = True

        dgvdiscount.Columns("enddate").HeaderText = "End"
        dgvdiscount.Columns("enddate").Width = 100
        dgvdiscount.Columns("enddate").Visible = True
        dgvdiscount.Columns("enddate").DisplayIndex = 3
        dgvdiscount.Columns("id").Visible = False
        dgvdiscount.Columns("enddate").ReadOnly = True

      

        objconn.CloseConn()
        dgvdiscount.RowHeadersVisible = False
        dgvdiscount.AllowUserToAddRows = False
    End Sub

  

    Private Sub dgvdiscount_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvdiscount.CellContentClick
        If Not (dgvdiscount.CurrentRow.IsNewRow) And Not (e.RowIndex = -1) Then
            If Not IsDBNull(dgvdiscount.Item("id", e.RowIndex).Value) Then
                If TypeOf dgvdiscount.Rows(e.RowIndex).Cells(e.ColumnIndex) Is DataGridViewCheckBoxCell Then
                    Dim dgvCheckBoxCell As DataGridViewCheckBoxCell = dgvdiscount.Rows(e.RowIndex).Cells(e.ColumnIndex)
                    'Commit the data to the datasouce.
                    dgvdiscount.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    Dim checked As Boolean = CType(dgvCheckBoxCell.Value, Boolean)
                    selectedDiskonId = dgvdiscount.Item("id", e.RowIndex).Value
                    ' MessageBox.Show(e.RowIndex & " " & checked & " " & e.ColumnIndex)
                    'ShowDetail(selectedDiskonId)
                    txtPaymentName.ReadOnly = False
                    txtdiscname.ReadOnly = False
                    txtpercent.ReadOnly = False
                    chkactive.Enabled = True
                    dpstart.Enabled = True
                    dpend.Enabled = True

                    txtdiscname.Text = dgvdiscount.Item("discountname", e.RowIndex).Value
                    txtpercent.Text = dgvdiscount.Item("percent", e.RowIndex).Value
                    txtPaymentName.Text = dgvdiscount.Item("paymentname", e.RowIndex).Value

                    If dgvdiscount.Item("active", e.RowIndex).Value = "1" Then
                        chkactive.Checked = True
                    Else
                        chkactive.Checked = False
                    End If
                    dpstart.Value = dgvdiscount.Item("startdate", e.RowIndex).Value
                    dpend.Value = dgvdiscount.Item("enddate", e.RowIndex).Value

                    DeleteMnu.Enabled = True
                    EditMnu.Enabled = True
                    AddMnu.Enabled = False
                    cancelMnu.Enabled = True


                    txtPaymentName.ReadOnly = True
                    txtdiscname.ReadOnly = True
                    txtpercent.ReadOnly = True
                    chkactive.Enabled = False
                    dpstart.Enabled = False
                    dpend.Enabled = False

                    If checked Then
                        Dim i As Integer
                        For i = 0 To dgvdiscount.Rows.Count - 1
                            If i <> e.RowIndex Then
                                dgvdiscount.Item("chk", i).Value = False
                            End If
                        Next

                    End If

                End If
            End If
        End If
    End Sub

    Private Sub ShowDetail(ByVal id As Integer)


    End Sub

    Private Sub AddMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddMnu.Click
        SaveMnu.Enabled = True
        AddMnu.Enabled = False
        EditMnu.Enabled = False
        cancelMnu.Enabled = True
        DeleteMnu.Enabled = False

        txtdiscname.Enabled = True
        txtdiscname.ReadOnly = False
        txtPaymentName.Enabled = True
        txtPaymentName.ReadOnly = False
        txtpercent.Enabled = True
        txtpercent.ReadOnly = False
        chkactive.Enabled = True
        dpstart.Enabled = True
        dpend.Enabled = True



        isnew = True
        isedit = False
    End Sub

    Private Sub EditMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditMnu.Click

        If selectedDiskonId = 0 Then
            MessageBox.Show("Pilih data yang hendak diedit")
            Return
        End If

        SaveMnu.Enabled = True
        AddMnu.Enabled = False
        EditMnu.Enabled = False
        cancelMnu.Enabled = True
        isedit = True
        isnew = False

        txtdiscname.ReadOnly = False
        txtPaymentName.ReadOnly = False
        txtpercent.ReadOnly = False
        chkactive.Enabled = True
        dpstart.Enabled = True
        dpend.Enabled = True


     
    End Sub

    Private Sub DeleteMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteMnu.Click
        AddMnu.Enabled = True

        isedit = False
        isnew = False

        If selectedDiskonId = 0 Then
            MessageBox.Show("Pilih program diskon yang hendak dihapus")
            Return
        End If

        Dim odel As New clsGreConnect
        odel.buildConn()
        odel.ExecuteNonQuery("delete from discountpayment where id='" & selectedDiskonId & "'")
        odel.CloseConn()

        PopulateDiskonList()

        EditMnu.Enabled = False
        DeleteMnu.Enabled = False
        cancelMnu.Enabled = False
        SaveMnu.Enabled = False

        txtdiscname.Text = ""
        txtpercent.Text = ""
        txtPaymentName.Text = ""

        txtdiscname.ReadOnly = True
        txtPaymentName.ReadOnly = True
        txtpercent.ReadOnly = True
        chkactive.Enabled = False
        dpstart.Enabled = False
        dpend.Enabled = False
        selectedDiskonId = 0
    End Sub

    Private Sub cancelMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelMnu.Click
        AddMnu.Enabled = True
        SaveMnu.Enabled = False
        EditMnu.Enabled = False
        cancelMnu.Enabled = False
        DeleteMnu.Enabled = False

        isedit = False
        isnew = False

        selectedDiskonId = 0

        txtdiscname.Text = ""
        txtpercent.Text = ""

        txtdiscname.ReadOnly = True
        txtPaymentName.ReadOnly = True
        txtpercent.ReadOnly = True
        chkactive.Enabled = False
        dpstart.Enabled = False
        dpend.Enabled = False

        PopulateDiskonList()

    End Sub

    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click
        Dim greo As New clsGreConnect
        greo.buildConn()

        Dim discname As String
        Dim percent As String
        Dim active As String
        Dim startdate As String
        Dim enddate As String
        Dim paymentName As String

        If txtdiscname.Text = "" Then
            txtdiscname.Text = "-"

        End If

        If txtpercent.Text = "" Then
            txtpercent.Text = "0"
        End If

        If txtPaymentName.Text = "" Then
            MessageBox.Show("Silahkan isikan nama pembayaran")
            Return
        End If

        discname = Trim(txtdiscname.Text)
        percent = Trim(txtpercent.Text)
        paymentName = txtPaymentName.Text
        If chkactive.Checked = True Then
            active = "1"
        Else
            active = "0"
        End If

        Dim seldate As Date = Date.Parse(dpstart.Text)
        startdate = Format(seldate, "yyyy-MM-dd")


        Dim selenddate As Date = Date.Parse(dpend.Text)
        enddate = Format(selenddate, "yyyy-MM-dd")

        Dim strinsert As String

        If isnew Then
            strinsert = "insert into discountpayment(paymentname,discountname,percent,active,startdate,enddate)values('" & paymentName & "','" & discname & "','" & percent & "','" & active & "','" & startdate & "','" & enddate & "')"
        ElseIf isedit Then
            strinsert = "update discountpayment set paymentname='" & paymentName & "',discountname='" & discname & "',percent='" & percent & "',active='" & active & "',startdate='" & startdate & "',enddate='" & enddate & "' where id='" & selectedDiskonId & "'"
        End If
        greo.ExecuteNonQuery(strinsert)
        greo.CloseConn()

        PopulateDiskonList()
        SaveMnu.Enabled = False
        AddMnu.Enabled = True
        EditMnu.Enabled = False
        cancelMnu.Enabled = False
        DeleteMnu.Enabled = False

        isnew = False
        isedit = False

        txtdiscname.ReadOnly = True
        txtPaymentName.ReadOnly = True
        txtpercent.ReadOnly = True
        chkactive.Enabled = False
        dpstart.Enabled = False
        dpend.Enabled = False


    End Sub

    ReadOnly AllowedKeys As String = _
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 "
    Private Sub txtdiscname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtdiscname.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)

        End Select
    End Sub
    ReadOnly AllowedKeysPercent As String = _
   "0123456789,"
    Private Sub txtpercent_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtpercent.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeysPercent.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeysPercent.Contains(e.KeyChar)

        End Select
    End Sub
End Class