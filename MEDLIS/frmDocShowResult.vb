﻿Imports GemBox.Spreadsheet
Imports Microsoft.Win32
Public Class frmDocShowResult

   
    Private Sub frmDocShowResult_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        Dim excelPath As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "hasil.xlsx")
        'Dim excelPath As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "new.xlsx")
        Dim ef As ExcelFile = ExcelFile.Load(excelPath)

        Dim xpsDocument = ef.ConvertToXpsDocument(SaveOptions.XpsDefault)
        'Me.PrintPreviewControl1.Tag = xpsDocument

        'Me.PrintPreviewControl1.Document = xpsDocument.GetFixedDocumentSequence()


        Dim docview As New System.Windows.Controls.DocumentViewer
        docview.Height = Me.Height
        ElementHost1.Child = docview
        docview.Tag = xpsDocument
        docview.Document = xpsDocument.GetFixedDocumentSequence()
    End Sub
End Class