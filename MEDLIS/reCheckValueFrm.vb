﻿Imports System.Data.SqlClient
Public Class reCheckValueFrm
    Dim selectedLabnumber As String
    Dim selectedPatient As Integer
    Dim patientName As String
    Dim patientaddress As String
    Dim patientPhone As String
    Dim selectedSampleNo As String
    Dim selectedBarcode As String
    Dim isthereAnUpdate As Boolean


    Private Sub btnfindlabnumber_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnfindlabnumber.Click
        LoadAllFinishJob()

    End Sub


   
    Private Sub UpdateJobStatus() 'penting update status
        Dim strsql As String
        Dim pormat As String
        pormat = "yyyy-MM-dd HH:mm:ss"

        Dim datenow As Date
        datenow = Now

        strsql = "select job.labnumber from job where job.status='" & NEWSAMPLE & "' and job.labnumber not in (select distinct labnumber from jobdetail where status<>'" & FINISHSAMPLE & "' and active='" & ACTIVESAMPLE & "')"

        Dim ogreupdate As New clsGreConnect
        ogreupdate.buildConn()
        Dim cmdupdate As New SqlClient.SqlCommand(strsql, ogreupdate.grecon)
        Dim rdrupdate As SqlDataReader = cmdupdate.ExecuteReader(CommandBehavior.CloseConnection)
        If rdrupdate.HasRows Then
            Do While rdrupdate.Read
                'update job

                Dim squ As String
                squ = "update job set status='" & FINISHSAMPLE & "',datefinish='" & datenow.ToString(pormat) & "' where labnumber='" & rdrupdate("labnumber") & "'"
                Dim ogrejob As New clsGreConnect
                ogrejob.buildConn()
                ogrejob.ExecuteNonQuery(squ)
                ogrejob.CloseConn()
            Loop
        End If
        ogreupdate.CloseConn()
        MessageBox.Show("Data telah terupdate")

    End Sub




    Private Sub quickupdate_per_row(ByVal idrow As Integer, ByVal read_val As String, ByVal resultstatus As String, ByVal result_abnormal As String, ByVal referencerange As String, ByVal lowercritical As String, ByVal lowernormal As String, ByVal uppernormal As String, ByVal uppercritical As String)
        Dim ogre_update As New clsGreConnect
        ogre_update.buildConn()

        Dim strsql As String
        strsql = "update jobdetail set measurementvalue='" & read_val & "',resultstatus='" & resultstatus & "',resultabnormalflag='" & result_abnormal & "',referencerange='" & referencerange & "',status='" & FINISHSAMPLE & "',lowercritical='" & lowercritical & "',lowernormal='" & lowernormal & "',uppernormal='" & uppernormal & "',uppercritical='" & uppercritical & "',machineorhuman='0',idmachine='0',iduser='" & userid & "' where id='" & idrow & "'"
        ogre_update.ExecuteNonQuery(strsql)
        ogre_update.CloseConn()
    End Sub

    Private Sub reCheckValueFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
        ReadLicense()
        dgvJobList.DataSource = Nothing
        If dgvJobList.Columns.Contains("Detail") Then
            dgvJobList.Columns.Clear()
        End If

        dgvJobList.AllowUserToAddRows = False
        dgvJobList.RowHeadersVisible = False
        btnRecheck.Enabled = False
        LoadAllFinishJob()
    End Sub

    Private Sub LoadAllFinishJob()

        dgvJobList.DataSource = Nothing
        If dgvJobList.Columns.Contains("Detail") Then
            dgvJobList.Columns.Clear()
        End If


        Dim strsql As String
        If Trim(txtlabnum.Text) = "" Then
            strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.id as patientid,job.labnumber,patient.address,job.datereceived, patient.phone from job,patient where patient.id=job.idpasien and job.paymentstatus<>'" & NOTPAID & "' and job.labnumber in (select distinct labnumber from jobdetail where status='" & FINISHSAMPLE & "' and active='" & ACTIVESAMPLE & "')"
            'strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.id as patientid,job.labnumber,patient.address,job.datereceived, patient.phone from job,patient where patient.id=job.idpasien and job.paymentstatus<>'" & NOTPAID & "' and job.labnumber in (select distinct labnumber from jobdetail where status='" & BEINGANALYZE & "' and active='" & ACTIVESAMPLE & "')"
        Else
            Dim src As String
            src = Trim(txtlabnum.Text)
            strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.id as patientid,job.labnumber,job.datereceived,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.paymentstatus<>'" & NOTPAID & "' and job.labnumber='" & src & "' and job.labnumber in (select distinct labnumber from jobdetail where status='" & BEINGANALYZE & "' and active='" & ACTIVESAMPLE & "')"
        End If

        Dim greobject As New clsGreConnect
        greobject.buildConn()

        Dim dtable As New DataTable
        dtable = greobject.ExecuteQuery(strsql)

        'dgvjoblist = New DataGridView
        dgvJobList.DataSource = dtable
        Dim j As Integer
        For j = 0 To dgvJobList.Columns.Count - 1
            dgvJobList.Columns(j).Visible = False
        Next

        dgvJobList.Columns("labnumber").Visible = True
        dgvJobList.Columns("labnumber").HeaderText = "Lab Number"
        dgvJobList.Columns("labnumber").Width = 120

        dgvJobList.Columns("name").Visible = True
        dgvJobList.Columns("name").HeaderText = "Nama pasien"
        dgvJobList.Columns("name").Width = 120

        dgvJobList.Columns("datereceived").Visible = True
        dgvJobList.Columns("datereceived").HeaderText = "Tanggal"
        dgvJobList.Columns("datereceived").Width = 120


        Dim buttonColumn As New DataGridViewButtonColumn()
        With buttonColumn
            .Name = "Detail"
            .HeaderText = "Detail"
            .Width = 90
            .Text = "Detail"
            .UseColumnTextForButtonValue = True
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            .FlatStyle = FlatStyle.Standard
            .CellTemplate.Style.BackColor = Color.Yellow
            .DisplayIndex = 0
        End With
        dgvJobList.Columns.Add(buttonColumn)
        dgvJobList.AllowUserToAddRows = False
        dgvJobList.RowHeadersVisible = False
    End Sub


    Private Sub ShowTestData()
        Dim strsql As String
        Dim ogre As New clsGreConnect

        dgvManual.DataSource = Nothing
        dgvManual.RowHeadersVisible = False
        dgvManual.AllowUserToAddRows = False

        ogre.buildConn()

        'strsql = "select jobdetail.id,jobdetail.idtesttype,jobdetail.universaltest,jobdetail.measurementvalue,jobdetail.resultstatus,jobdetail.resultabnormalflag,jobdetail.lowercritical,jobdetail.lowernormal,jobdetail.uppernormal,jobdetail.uppercritical from jobdetail where labnumber='" & selectedLabnumber & "' and active='" & ACTIVESAMPLE & "' and status='" & FINISHSAMPLE & "'"
        strsql = "select jobdetail.id,jobdetail.idtesttype,jobdetail.universaltest,jobdetail.measurementvalue,jobdetail.lowercritical,jobdetail.lowernormal,jobdetail.uppernormal,jobdetail.uppercritical,jobdetail.resultstatus,jobdetail.resultabnormalflag from jobdetail where labnumber='" & selectedLabnumber & "' and active='" & ACTIVESAMPLE & "' and status='" & FINISHSAMPLE & "'"
        Dim dtable As New DataTable
        dtable = ogre.ExecuteQuery(strsql)
        dgvManual.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvManual.Columns.Count - 1
            dgvManual.Columns(j).Visible = False
        Next

        If Not dgvManual.Columns.Contains("chk") Then
            Dim chk As New DataGridViewCheckBoxColumn()

            dgvManual.Columns.Add(chk)
            chk.HeaderText = "PILIH"
            chk.Width = 65
            chk.Name = "chk"

            dgvManual.Columns("chk").DisplayIndex = 0
            dgvManual.Columns("chk").Name = "chk"
        End If

        dgvManual.Columns("chk").Visible = True
        dgvManual.Columns("universaltest").Visible = True
        dgvManual.Columns("universaltest").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("universaltest").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("universaltest").HeaderText = "Test Name"
        dgvManual.Columns("universaltest").Width = 120
        dgvManual.Columns("universaltest").ReadOnly = True

        dgvManual.Columns("measurementvalue").Visible = True
        dgvManual.Columns("measurementvalue").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("measurementvalue").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("measurementvalue").HeaderText = "Nilai"
        dgvManual.Columns("measurementvalue").Width = 120
        dgvManual.Columns("measurementvalue").ReadOnly = True

        dgvManual.Columns("resultstatus").Visible = False
        dgvManual.Columns("resultstatus").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("resultstatus").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("resultstatus").HeaderText = "Result Status"
        dgvManual.Columns("resultstatus").Width = 80


        dgvManual.Columns("resultabnormalflag").Visible = True
        dgvManual.Columns("resultabnormalflag").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("resultabnormalflag").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("resultabnormalflag").HeaderText = "Abnormal Flag"
        dgvManual.Columns("resultabnormalflag").Width = 80
        dgvManual.Columns("resultabnormalflag").ReadOnly = True

        dgvManual.Columns("lowercritical").Visible = False
        dgvManual.Columns("lowercritical").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("lowercritical").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("lowercritical").HeaderText = "Lower Critical"
        dgvManual.Columns("lowercritical").Width = 70


        dgvManual.Columns("lowernormal").Visible = False
        dgvManual.Columns("lowernormal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("lowernormal").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("lowernormal").HeaderText = "Lower Normal"
        dgvManual.Columns("lowernormal").Width = 70


        dgvManual.Columns("uppernormal").Visible = False
        dgvManual.Columns("uppernormal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("uppernormal").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("uppernormal").HeaderText = "Upper Normal"
        dgvManual.Columns("uppernormal").Width = 70


        dgvManual.Columns("uppercritical").Visible = False
        dgvManual.Columns("uppercritical").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("uppercritical").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvManual.Columns("uppercritical").HeaderText = "Upper Critical"
        dgvManual.Columns("uppercritical").Width = 70

        'If Not dgvManual.Columns.Contains("chk") Then
        '    Dim chk As New DataGridViewCheckBoxColumn()
        '    dgvManual.Columns.Add(chk)
        '    chk.HeaderText = "SELECT"
        '    chk.Width = 80
        '    chk.Name = "chk"
        '    dgvManual.Columns("chk").DisplayIndex = 0
        '    dgvManual.Columns("chk").Name = "chk"
        'End If


        ogre.CloseConn()


    End Sub

    Private Sub dgvJobList_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvJobList.CellContentClick
        If e.RowIndex < 0 OrElse Not e.ColumnIndex = dgvJobList.Columns("Detail").Index Then
            Return

        Else

            If Not dgvJobList.CurrentRow.IsNewRow Then

                selectedLabnumber = dgvJobList.Item("labnumber", dgvJobList.CurrentRow.Index).Value
                selectedPatient = dgvJobList.Item("patientid", dgvJobList.CurrentRow.Index).Value
                patientName = dgvJobList.Item("name", dgvJobList.CurrentRow.Index).Value
                patientaddress = dgvJobList.Item("address", dgvJobList.CurrentRow.Index).Value
                patientPhone = dgvJobList.Item("phone", dgvJobList.CurrentRow.Index).Value

                txtaddress.ReadOnly = False
                txttelepon.ReadOnly = False
                txtname.ReadOnly = False
                txtLabnumber.ReadOnly = False
                txtSN.ReadOnly = False

                txtaddress.Text = patientaddress
                txtname.Text = patientName
                txttelepon.Text = patientPhone
                txtLabnumber.Text = selectedLabnumber
                ShowSampleNo()

                txtaddress.ReadOnly = True
                txttelepon.ReadOnly = True
                txtname.ReadOnly = True
                txtLabnumber.ReadOnly = True
                isthereAnUpdate = False


                ShowTestData()
                btnRecheck.Enabled = True
                'ShowDetail()
            End If

        End If
    End Sub

    Private Sub ShowSampleNo()
        txtSN.ReadOnly = False
        Dim strsql As String
        strsql = "select distinct sampleno,barcode from jobdetail where labnumber='" & selectedLabnumber & "'"
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim cmd As New SqlClient.SqlCommand(strsql, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("sampleno")) Then
                    selectedSampleNo = rdr("sampleno")
                    txtSN.Text = selectedSampleNo

                End If
                If Not IsDBNull(rdr("barcode")) Then
                    selectedBarcode = rdr("barcode")
                    txtbarcode.Text = selectedBarcode

                End If
            Loop
        End If
        txtSN.ReadOnly = True
        txtbarcode.ReadOnly = True
        rdr.Close()
        cmd.Dispose()
        ogre.CloseConn()
    End Sub

    Private Sub btnRecheck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRecheck.Click
        Dim i As Integer
        Dim idjd As Integer
        For i = 0 To dgvManual.RowCount - 1
            'If Not (dgvManual.CurrentRow.IsNewRow) Then
            If Not IsDBNull(dgvManual.Item("id", i).Value) Then
                If TypeOf dgvManual.Rows(i).Cells("chk") Is DataGridViewCheckBoxCell Then
                    Dim dgvCheckBoxCell As DataGridViewCheckBoxCell = dgvManual.Rows(i).Cells("chk")
                    'Commit the data to the datasouce.
                    Dim checked As Boolean = CType(dgvCheckBoxCell.Value, Boolean) 'bagus
                    If checked Then
                        idjd = dgvManual.Item("id", i).Value
                        RecheckValue(idjd)
                    End If


                End If
            End If
            ' End If
        Next

        If isthereAnUpdate = True Then
            Dim ogrejob As New clsGreConnect
            ogrejob.buildConn()
            ogrejob.ExecuteNonQuery("update job set status='" & NEWSAMPLE & "' where labnumber='" & selectedLabnumber & "'")
            ogrejob.CloseConn()

            MessageBox.Show("Data sudah ter-recheck")
            dgvManual.DataSource = Nothing

            If dgvManual.Columns.Contains("chk") Then
                dgvManual.Columns.Clear()
            End If

            LoadAllFinishJob()
            btnRecheck.Enabled = False
        End If
    End Sub

    Private Sub RecheckValue(ByVal idupdate As Integer)
        Dim ogre As New clsGreConnect
        Try
            ogre.buildConn()

            Dim strupdate As String
            strupdate = "update jobdetail set status='" & ALREADYSAMPLING & "' where id='" & idupdate & "'"
            ogre.ExecuteNonQuery(strupdate)
            ogre.CloseConn()
            isthereAnUpdate = True
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Terjadi kesalahan")
        End Try

    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim i As Integer
        Dim idjd As Integer
        For i = 0 To dgvManual.RowCount - 1
            'If Not (dgvManual.CurrentRow.IsNewRow) Then
            If Not IsDBNull(dgvManual.Item("id", i).Value) Then
                If TypeOf dgvManual.Rows(i).Cells("chk") Is DataGridViewCheckBoxCell Then
                    Dim dgvCheckBoxCell As DataGridViewCheckBoxCell = dgvManual.Rows(i).Cells("chk")
                    'Commit the data to the datasouce.
                    Dim checked As Boolean = CType(dgvCheckBoxCell.Value, Boolean) 'bagus
                    If checked Then
                        idjd = dgvManual.Item("id", i).Value
                        RecheckValue(idjd)
                    End If


                End If
            End If
            ' End If
        Next

        If isthereAnUpdate = True Then
            Dim ogrejob As New clsGreConnect
            ogrejob.buildConn()
            ogrejob.ExecuteNonQuery("update job set status='" & NEWSAMPLE & "' where labnumber='" & selectedLabnumber & "'")
            ogrejob.CloseConn()

            MessageBox.Show("Data sudah ter-recheck")
            dgvManual.DataSource = Nothing

            If dgvManual.Columns.Contains("chk") Then
                dgvManual.Columns.Clear()
            End If

            LoadAllFinishJob()
            btnRecheck.Enabled = False
        End If
    End Sub
End Class