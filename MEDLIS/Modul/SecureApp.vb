﻿Module SecureApp
    Dim SDhandle As UInt16
    Dim p1, p2, p3, p4, retcode As UInt16
    Dim lp1, lp2 As UInt32
    Dim buffer(1024) As Byte
    Dim tempHID(16) As UInt32

    Public Sub CekLicense()

        'DEMO DEFAULT PASSWORD
        p1 = &H66BB
        p2 = &HF730
        p3 = 0 '&H8058 'Advance password. Must set to 0 for end user application
        p4 = 0 '&H5D6A 'Advance password. Must set to 0 for end user application

        'Find Dongle
        retcode = SD.SecureDongle(SD_FIND, SDhandle, lp1, lp2, p1, p2, p3, p4, buffer)
        If (retcode <> 0) Then
            MessageBox.Show("Silahkan pasang usb license yang valid")
            Application.Exit()
            Exit Sub
        Else
            tempHID(0) = lp1
            OpenLicense()
        End If

        
    End Sub

    Private Sub OpenLicense()
        'Open Dongle
        'DEMO DEFAULT PASSWORD
        p1 = &H66BB
        p2 = &HF730
        p3 = 0
        p4 = 0
        lp1 = tempHID(0) 'retrive HID base on dongle selected from ComboBox

        retcode = SD.SecureDongle(SD_OPEN, SDhandle, lp1, lp2, p1, p2, p3, p4, buffer)
        'If return value not equals to 0 then show error code.
        If (retcode <> 0) Then
            MessageBox.Show("License gagal 1")
            Application.Exit()
            Exit Sub
        Else
            'If return value equals to 0
        End If
    End Sub

    Public Sub ReadLicense()
        Try
            Dim str1, str2 As String

            p1 = 88 'OffSet of UDZ (UDZ memory position)
            p2 = 10  'length
            retcode = SD.SecureDongle(SD_READ, SDhandle, lp1, lp2, p1, p2, p3, p4, buffer)
            If (retcode <> 0) Then
                MessageBox.Show("License gagal 2")
                Application.Exit()
                Exit Sub
            Else
                str1 = System.Text.Encoding.UTF8.GetString(buffer, 0, p2)
            End If

            p1 = 868 'OffSet of UDZ (UDZ memory position)
            p2 = 10  'length
            retcode = SD.SecureDongle(SD_READ, SDhandle, lp1, lp2, p1, p2, p3, p4, buffer)
            If (retcode <> 0) Then
                MessageBox.Show("License gagal 3")
                Application.Exit()
                Exit Sub
            Else
                str2 = System.Text.Encoding.UTF8.GetString(buffer, 0, p2)
            End If
            'If str1 <> str2 Then
            '    ' MessageBox.Show()
            '    Application.Exit()
            'End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Terjadi kesalahan pembacaan license")
        End Try
        

    End Sub
    Public Sub ReadVersion()
        Dim str1, str2 As String

        p1 = 818 'OffSet of UDZ (UDZ memory position)
        p2 = 5  'length
        retcode = SD.SecureDongle(SD_READ, SDhandle, lp1, lp2, p1, p2, p3, p4, buffer)
        If (retcode <> 0) Then
            MessageBox.Show("License gagal")
            Application.Exit()
            Exit Sub
        Else
            str1 = System.Text.Encoding.UTF8.GetString(buffer, 0, p2)
        End If

        'VERSI License
        If str1 <> VersionControl Then
            MessageBox.Show(" Versi license Anda dan versi aplikasi berbeda")
            Application.Exit()
        End If

    End Sub
    Public Sub Counter_Trial()
        p1 = 5  'Get from which module?
        Dim counterValue As UInt32
        retcode = SD.SecureDongle(SD_GET_COUNTER_EX, SDhandle, lp1, lp2, p1, p2, p3, p4, buffer)
        If (retcode <> 0) Then
            If (retcode <> 228) Then
                MessageBox.Show("License gagal")
                Application.Exit()
                Exit Sub
            Else
                MessageBox.Show("Masa percobaan habis gagal")
                Application.Exit()
                Exit Sub

            End If
        Else
            CopyMemory1(counterValue, buffer, 4)  'get 4byte(DWORD) from buffer to read the counter value
            'ListBox1.Items.Add("Get Counter Sucess! Value:" & counterValue)
            'ListBox1.SelectedIndex = ListBox1.Items.Count - 1
        End If

    End Sub

    Public Sub CloseLicense()
        retcode = SD.SecureDongle(SD_CLOSE, SDhandle, lp1, lp2, p1, p2, p3, p4, buffer)
    End Sub
    Public Function Capability(ByVal nilaiP1 As UInt16, ByVal nilaiP2 As UInt16) As String
        Dim result_license As String

        p1 = nilaiP1  'OffSet of UDZ (UDZ memory position)
        p2 = nilaiP2  'length
        retcode = SD.SecureDongle(SD_READ, SDhandle, lp1, lp2, p1, p2, p3, p4, buffer)
        If (retcode <> 0) Then
            'ListBox1.Items.Add("Error Code: " & retcode)
            'ListBox1.SelectedIndex = ListBox1.Items.Count - 1
            'Exit Function
            Return "fail"
        Else
            'ListBox1.Items.Add("UDZ Read: " & System.Text.Encoding.UTF8.GetString(buffer, 0, p2))
            'ListBox1.SelectedIndex = ListBox1.Items.Count - 1
            Return System.Text.Encoding.UTF8.GetString(buffer, 0, p2)
        End If
    End Function

    Public Function getParent(ByVal nilaiP1 As UInt16, ByVal nilaiP2 As UInt16) As String
        Dim result_license As String

        p1 = 890  'OffSet of UDZ (UDZ memory position)
        p2 = 10  'length
        retcode = SD.SecureDongle(SD_READ, SDhandle, lp1, lp2, p1, p2, p3, p4, buffer)
        If (retcode <> 0) Then
            'ListBox1.Items.Add("Error Code: " & retcode)
            'ListBox1.SelectedIndex = ListBox1.Items.Count - 1
            'Exit Function
            Return "fail"
        Else
            'ListBox1.Items.Add("UDZ Read: " & System.Text.Encoding.UTF8.GetString(buffer, 0, p2))
            'ListBox1.SelectedIndex = ListBox1.Items.Count - 1
            result_license = System.Text.Encoding.UTF8.GetString(buffer, 0, p2)
            Return result_license
        End If
    End Function


End Module
