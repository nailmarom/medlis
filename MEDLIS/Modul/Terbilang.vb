﻿Module Terbilang
    Public Function TERBILANG(ByVal x As Double) As String
        Dim tampung As Double
        Dim teks As String
        Dim bagian As String
        Dim i As Integer
        Dim tanda As Boolean

        Dim letak(5)
        letak(1) = "RIBU "
        letak(2) = "JUTA "
        letak(3) = "MILYAR "
        letak(4) = "TRILYUN "

        If (x < 0) Then
            TERBILANG = ""
            Exit Function
        End If

        If (x = 0) Then
            TERBILANG = "NOL"
            Exit Function
        End If

        If (x < 2000) Then
            tanda = True
        End If
        teks = ""

        If (x >= 1.0E+15) Then
            TERBILANG = "NILAI TERLALU BESAR"
            Exit Function
        End If

        For i = 4 To 1 Step -1
            tampung = Int(x / (10 ^ (3 * i)))
            If (tampung > 0) Then
                bagian = ratusan(tampung, tanda)
                teks = teks & bagian & letak(i)
            End If
            x = x - tampung * (10 ^ (3 * i))
        Next

        teks = teks & ratusan(x, False)
        TERBILANG = teks & "RUPIAH"
    End Function

    Function ratusan(ByVal y As Double, ByVal flag As Boolean) As String
        Dim tmp As Double
        Dim bilang As String
        Dim bag As String
        Dim j As Integer

        Dim angka(9)
        angka(1) = "SE"
        angka(2) = "DUA "
        angka(3) = "TIGA "
        angka(4) = "EMPAT "
        angka(5) = "LIMA "
        angka(6) = "ENAM "
        angka(7) = "TUJUH "
        angka(8) = "DELAPAN "
        angka(9) = "SEMBILAN "

        Dim posisi(2)
        posisi(1) = "PULUH "
        posisi(2) = "RATUS "

        bilang = ""
        For j = 2 To 1 Step -1
            tmp = Int(y / (10 ^ j))
            If (tmp > 0) Then
                bag = angka(tmp)
                If (j = 1 And tmp = 1) Then
                    y = y - tmp * 10 ^ j
                    If (y >= 1) Then
                        posisi(j) = "BELAS "
                    Else
                        angka(y) = "SE"
                    End If
                    bilang = bilang & angka(y) & posisi(j)
                    ratusan = bilang
                    Exit Function
                Else
                    bilang = bilang & bag & posisi(j)
                End If
            End If
            y = y - tmp * 10 ^ j
        Next

        If (flag = False) Then
            angka(1) = "SATU "
        End If
        bilang = bilang & angka(y)
        ratusan = bilang
    End Function

    Public Function Age(startDate As Date, endDate As Date) As String
        Dim Years = New DateTime(endDate.Subtract(startDate).Ticks).Year - 1
        Dim PastYearDate = startDate.AddYears(Years)
        Dim Months As Integer = 0

        For i As Integer = 1 To 12
            If (PastYearDate.AddMonths(i) = endDate) Then
                Months = i
                Exit For
            ElseIf (PastYearDate.AddMonths(i) >= endDate) Then
                Months = i - 1
                Exit For
            End If
        Next

        Dim ToTalMonths = (Years * 12) + Months

        Dim Days = endDate.Subtract(PastYearDate.AddMonths(Months)).Days

        If Days < 0 Then
            ToTalMonths = ToTalMonths - 1
            Years = ToTalMonths \ 12
            Months = ToTalMonths Mod 12
            Days = 31 + Days

        End If

        Return String.Format("{0} {1} {2}", IIf(Years = 0, "", Years.ToString + " thn"), IIf(Months = 0, "", Months.ToString + " bln"), IIf(Days = 0, "", Days.ToString + " hari")).Trim()
    End Function


End Module
