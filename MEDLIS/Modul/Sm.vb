﻿Module Sm
    ' Interface:
    '(1) Find Dongle
    '    Input:
    '    function = 0
    '    p1 = pass1
    '    p2 = pass2
    '    p3 = pass3
    '    p4 = pass4
    '    Return:
    '    lp1 = SecureDongle HID
    '    return 0 = Success, else is error code
    '
    '(2) Find Next Dongle
    '    Input:
    '    function = 1
    '    p1 = pass1
    '    p2 = pass2
    '    p3 = pass3
    '    p4 = pass4
    '    Return:
    '    lp1 = SecureDongle HID
    '    return 0 = Success, else is error code
    '
    '(3) Open Dongle
    '    Input:
    '    function = 2
    '    p1 = pass1
    '    p2 = pass2
    '    p3 = pass3
    '    p4 = pass4
    '    lp1 = SecureDongle HID
    '    Return:
    '    handle = Opened dongle handle
    '    return 0 = Success, else is error code
    '
    '(4) Close Dongle
    '    Input:
    '    function = 3
    '    handle = dongle handle
    '    Return:
    '    return 0 = Success, else is error code
    '
    '(5) Read Dongle
    '    Input:
    '    function = 4
    '    handle = dongle handle
    '    p1 = pos
    '    p2 = length
    '    buffer = pointer of buffer
    '    Return:
    '    Fill buffer with read contents
    '    return 0 = Success, else is error code
    '
    '(6) Write Dongle
    '    Input:
    '    function = 5
    '    handle = dongle handle
    '    p1 = pos
    '    p2 = length
    '    buffer = pointer of buffer
    '    Return:
    '    return 0 = Success, else is error code
    '
    '(7) Generate Random Number
    '    Input:
    '    function = 6
    '    handle = dongle handle
    '    Return:
    '    p1 = random number
    '    return 0 = Success, else is error code
    '
    '(8) Generate Seed Code
    '    Input:
    '    function = 7
    '    handle = dongle handle
    '    lp2 = seed code
    '    Return:
    '    p1 = seed return code 1
    '    p2 = seed return code 2
    '    p3 = seed return code 3
    '    p4 = seed return code 4
    '    return 0 = Success, else is error code
    '
    '(9) Write User ID [*]
    '    Input:
    '    function = 8
    '    handle = dongle handle
    '    lp1 = User ID
    '    Return:
    '    return 0 = Success, else is error code
    '
    '(10) Read User ID
    '     Input:
    '     function = 9
    '     handle = dongle handle
    '     Return:
    '     lp1 = User ID
    '     return 0 = Success, else is error code
    '
    '(11) Set Module [*]
    '     Input:
    '     function = 10
    '     handle = dongle handle
    '     p1 = module number
    '     p2 = module content
    '     p3 = set whether allow decrease (1 = allow, 0 = no allow)
    '     Return:
    '     return 0 = Success, else is error code
    '
    '(12) Check Module
    '     Input:
    '     function = 11
    '     handle = dongle handle
    '     p1 = module number
    '     Return:
    '     p2 = 1 means the module is valid, 0 means the module is invalid
    '     p3 = 1 means the module can't decrease, 0 means the module can decrease
    '     return 0 = Success, else is error code
    '
    '(13) Write Arithmetic [*]
    '     Input:
    '     function = 12
    '     handle = dongle handle
    '     p1 = pos
    '     buffer = arithmetic instruction string
    '     Return:
    '     return 0 = Success, else is error code
    '
    '(14) Calculate 1 (Hide Unit Init Content = HID high 16bit, HID low 16bit, module content, random number)
    '     Input:
    '     function = 13
    '     handle = dongle handle
    '     lp1 = calculate begin pos
    '     lp2 = module number
    '     p1 = input value 1
    '     p2 = input value 2
    '     p3 = input value 3
    '     p4 = input value 4
    '     Return:
    '     p1 = return code 1
    '     p2 = return code 2
    '     p3 = return code 3
    '     p4 = return code 4
    '     return 0 = Success, else is error code
    '
    '(15) Calculate 2 (Hide Unit Init Content = seed return code 1, seed return code 2, seed return code 3, seed return code 4)
    '     Input:
    '     function = 14
    '     handle = dongle handle
    '     lp1 = calculate begin pos
    '     lp2 = seed code
    '     p1 = input value 1
    '     p2 = input value 2
    '     p3 = input value 3
    '     p4 = input value 4
    '     Return:
    '     p1 = return code 1
    '     p2 = return code 2
    '     p3 = return code 3
    '     p4 = return code 4
    '     return 0 = Success, else is error code
    '
    '(16) Calculate 3 (Hide Unit Init Content = module content, module+1 content, module+2 content, module+3 content)
    '     Input:
    '     function = 15
    '     handle = dongle handle
    '     lp1 = calculate begin pos
    '     lp2 = module begin pos
    '     p1 = input value 1
    '     p2 = input value 2
    '     p3 = input value 3
    '     p4 = input value 4
    '     Return:
    '     p1 = return code 1
    '     p2 = return code 2
    '     p3 = return code 3
    '     p4 = return code 4
    '     return 0 = Success, else is error code
    '
    '(17) Decrease Module Unit
    '     Input:
    '     function = 16
    '     handle = dongle handle
    '     p1 = module number
    '     Return:
    '     return 0 = Success, else is error code
    Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (ByVal Destination As Byte(), ByRef Source As UInt32, ByVal Length As Integer)
    Public Declare Sub CopyMemory1 Lib "kernel32" Alias "RtlMoveMemory" (ByRef Destination As UInt32, ByVal Source As Byte(), ByVal Length As Integer)

    Public SD As New Securedongle.SecuredongleControl

    'Function Code
    Public Const SD_FIND = 1                        'Find Dongle
    Public Const SD_FIND_NEXT = 2                   'Find Next Dongle
    Public Const SD_OPEN = 3                        'Open Dongle
    Public Const SD_CLOSE = 4                       'Close Dongle
    Public Const SD_READ = 5                        'Read Dongle
    Public Const SD_WRITE = 6                       'Write Dongle
    Public Const SD_RANDOM = 7                      'Generate Random Number
    Public Const SD_SEED = 8                        'Generate Seed Code
    Public Const SD_WRITE_USERID = 9                'Write User ID
    Public Const SD_READ_USERID = 10                'Read User ID
    Public Const SD_SET_MODULE = 11                 'Set Module
    Public Const SD_CHECK_MODULE = 12               'Check Module
    Public Const SD_WRITE_ARITHMETIC = 13           'Write Arithmetic
    Public Const SD_CALCULATE1 = 14                 'Calculate 1
    Public Const SD_CALCULATE2 = 15                 'Calculate 2
    Public Const SD_CALCULATE3 = 16                 'Calculate 3
    Public Const SD_DECREASE = 17                   'Decrease Module Unit
    Public Const SD_SET_RSAKEY_N = 29               'Decrease Module Unit
    Public Const SD_SET_RSAKEY_D = 30                   'Decrease Module Unit
    Public Const SD_RSA_ENC = 44                   'Decrease Module Unit
    Public Const SD_RSA_DEC = 45
    Public Const SD_DES_ENC = 42                   'Decrease Module Unit
    Public Const SD_DES_DEC = 43
    Public Const SD_SET_DES_KEY = 41

    Public Const SD_SET_COUNTER_EX = 160                 ' Set Counter, Type change from WORD to DWORD
    Public Const SD_GET_COUNTER_EX = 161

    'Error Code
    Public Const ERR_SUCCESS = 0                    'Success
    Public Const ERR_NO_SECUREDONGLE = 3            'No SecureDongle dongle
    Public Const ERR_INVALID_PASSWORD = 4           'Found SecureDongle dongle, but base password is wrong
    Public Const ERR_INVALID_PASSWORD_OR_ID = 5     'Wrong password or SecureDongle HID
    Public Const ERR_SETID = 6                      'Set SecureDongle HID wrong
    Public Const ERR_INVALID_ADDR_OR_SIZE = 7       'Read/Write address is wrong
    Public Const ERR_UNKNOWN_COMMAND = 8            'No such command
    Public Const ERR_NOTBELEVEL3 = 9                'Inside error
    Public Const ERR_READ = 10                      'Read error
    Public Const ERR_WRITE = 11                     'Write error
    Public Const ERR_RANDOM = 12                    'Random error
    Public Const ERR_SEED = 13                      'Seed Code error
    Public Const ERR_CALCULATE = 14                 'Calculate error
    Public Const ERR_NO_OPEN = 15                   'No open dongle before operate dongle
    Public Const ERR_OPEN_OVERFLOW = 16             'Too more open dongle(>16)
    Public Const ERR_NOMORE = 17                    'No more dongle
    Public Const ERR_NEED_FIND = 18                 'No Find before FindNext
    Public Const ERR_DECREASE = 19                  'Decrease error
    Public Const ERR_AR_BADCOMMAND = 20             'Arithmetic instruction error
    Public Const ERR_AR_UNKNOWN_OPCODE = 21         'Arithmetic operator error
    Public Const ERR_AR_WRONGBEGIN = 22             'Public Const number can't use on first arithmetic instruction
    Public Const ERR_AR_WRONG_END = 23              'Public Const number can't use on last arithmetic instruction
    Public Const ERR_AR_VALUEOVERFLOW = 24          'Public Const number > 63
    Public Const ERR_UNKNOWN = &HFFFF               'Unknown error
    Public Const ERR_RECEIVE_NULL = &H100           'Receive null


End Module


