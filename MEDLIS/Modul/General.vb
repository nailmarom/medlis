Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Module General
    '############
    'keterangan tentang user field
    ' uf1 table testitem untuk speciment desc
    ' uf2 table testitem untuk manual dan tidak manual
    Public isTestWithManualEntry As String = "1"
    Public isTestWithInstrumentEntry As String = "0"

    '############
    Public developerMode As Integer = 0

    Public Const PASIEN_AUTO_NUMBERING As Integer = 0
    Public Const PASIEN_MANUAL_NUMBERING As Integer = 1
    Public Const LAB_AUTO_NUMBERING As Integer = 0
    Public Const LAB_MANUAL_NUMBERING As Integer = 1
    Public labnumbering As Integer
    Public patientnumbering As Integer


    Public isTestManualEntryWindowsOpen As Integer = 0
    Public isPalioWindowsOpen As Integer = 0
    Public is240WindowsOpen As Integer = 0

    Public isHema300Open As Integer = 0
    Public isMindRayBC3600 As Integer = 0
    Public isMindRayBC5100 As Integer = 0

    Public isPembayaranWinOpen As Integer = 0
    Public isRegWinOpen As Integer = 0
    Public isPatientWinOpen As Integer = 0
    Public isPaymentWinOpen As Integer = 0
    Public isSamplingWinOpen As Integer = 0


    Public Const PAIDLATER As Integer = 2
    Public Const PAID As Integer = 1
    Public Const NOTPAID As Integer = 0

    Public Const DIRECTPAYMENT As Integer = 8
    Public Const UNDIRECT As Integer = 7
    Public Const VersionControl As String = "v0001"

    '===========
    Public Const NEWSAMPLE As Integer = 0
    Public Const ALREADYSAMPLING As Integer = 1
    Public Const FINISHSAMPLE As Integer = 2
    Public Const BEINGANALYZE As Integer = 8 'DAFTAR KONSTANTA
    Public Const ALLAVAILABLESAMPLE As Integer = 3

    Public Const PRINTALREADY As Integer = 1
    Public Const PRINTNOT As Integer = 0

    Public Const ACTIVESAMPLE As Integer = 1
    Public Const INACTIVESAMPLE As Integer = 0

    Public Const ISRELATED As Integer = 1
    Public Const NOTRELATED As Integer = 0

    Public Const genderMale As Integer = 1
    Public Const genderFemale As Integer = 0
    Public Const genderChild As Integer = 2

    '====================
    Public strdbconn As String

    Public isIniPresent As Boolean
    Public isIniValid As Boolean

    Public filePath As String
    Public currentInstrument As String

    Public Global_serverName As String
    Public Global_dbName As String
    Public Global_dbUser As String
    Public Global_dbPass As String

    Public isValidRegisterUser As Boolean
    Public isvalidResultuser As Boolean
    Public isValidSettingUser As Boolean
    Public isValidPembayaranUser As Boolean
    Public isValidInstrumentUser As Boolean

    Public isActiveUser As Boolean

    Public mComs(9) As Boolean
    Public userid As String
    Public user_complete_name As String

    Public isValidUser As Boolean

    Public digitResult As Integer

    'Report data
    Public GLabAddress1 As String
    Public GLabAddress2 As String
    Public GLabPhone As String
    Public GLabFax As String
    Public GLabMaster As String
    Public GLabName As String
    Public GLabFooterNote As String

    Public Const Result_type_Number As String = "Angka"
    Public Const Result_type_Text As String = "Text"
    Public Const Result_type_Number_and_Text As String = "Text dan Angka"


    Public Sub LoadGeneralData()
        Try
            Dim ogre As New clsGreConnect
            Dim strsql As String

            ogre.buildConn()
            strsql = "select * from labinfo"
            Dim cmd As New SqlClient.SqlCommand(strsql, ogre.grecon)
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While rdr.Read
                If Not IsDBNull(rdr("labname")) Then
                    GLabName = rdr("labname")
                Else
                    GLabName = ""
                End If

                If Not IsDBNull(rdr("address1")) Then
                    GLabAddress1 = rdr("address1")
                Else
                    GLabAddress1 = ""
                End If

                If Not IsDBNull(rdr("address2")) Then
                    GLabAddress2 = rdr("address2")
                Else
                    GLabAddress2 = ""
                End If

                If Not IsDBNull(rdr("labphone")) Then
                    GLabPhone = rdr("labphone")
                Else
                    GLabPhone = ""
                End If


                If Not IsDBNull(rdr("labfax")) Then
                    GLabFax = rdr("labfax")
                Else
                    GLabFax = ""
                End If

                If Not IsDBNull(rdr("labmaster")) Then
                    GLabMaster = rdr("labmaster")
                Else
                    GLabMaster = ""
                End If

                If Not IsDBNull(rdr("footerdesc")) Then
                    GLabFooterNote = rdr("footerdesc")
                Else
                    GLabFooterNote = ""
                End If


            Loop
            cmd.Dispose()
            rdr.Close()


            Dim strsetting As String
            strsetting = "select * from setting" '-- where name='digit'"
            Dim ogredigit As New clsGreConnect
            ogredigit.buildConn()

            Dim cmddigit As New SqlClient.SqlCommand(strsetting, ogredigit.grecon)
            Dim rdrdigit As SqlDataReader = cmddigit.ExecuteReader(CommandBehavior.CloseConnection)
            If rdrdigit.HasRows Then
                Do While rdrdigit.Read
                    ' digitResult = rdrdigit("value")
                    If rdrdigit("name") = "patientnumbering" Then
                        patientnumbering = rdrdigit("value")
                    End If
                    If rdrdigit("name") = "labnumbering" Then
                        labnumbering = rdrdigit("value")
                    End If

                Loop
            Else
                digitResult = 2

            End If
            rdrdigit.Close()
            cmd.Dispose()

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Terjadi Error")
        End Try
        
    End Sub
    Dim LtFn As LTFunctions.Functions
    Dim oldFn As String
    Dim DecSep As String
    Dim dgt As Integer = 2

    Public Sub FindAndUpdateSpecialTest(ByVal labnumber As String, ByVal gender As Integer)

        Dim resultcal As Decimal
        'find in the jobdetail if there is spesial test
        '--------------
        LtFn = New LTFunctions.Functions
        ' Button1.Enabled = False
        Dim Nfi As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.InstalledUICulture.NumberFormat
        DecSep = Nfi.NumberDecimalSeparator
        Nfi = Nothing


        '--------------

        Dim findspecial As String

        findspecial = "select jobdetail.id,jobdetail.idtesttype,testtype.formula,testtype.x,testtype.y,testtype.z,testtype.isrelated from jobdetail,testtype where labnumber='" & labnumber & "' and jobdetail.idtesttype=testtype.id and testtype.isrelated='" & ISRELATED & "'"

        Dim cnfind As New clsGreConnect
        cnfind.buildConn()

        Dim cmd As New SqlClient.SqlCommand(findspecial, cnfind.grecon)
        Dim rdr As SqlDataReader
        rdr = cmd.ExecuteReader()
        If rdr.HasRows Then
            Do While rdr.Read
                If Val(rdr("isrelated")) = ISRELATED Then
                    Dim strf As String
                    Dim strx As String
                    Dim stry As String
                    Dim strz As String


                    strf = rdr("formula")
                    strx = getXYZ(labnumber, rdr("x"))
                    stry = getXYZ(labnumber, rdr("y"))
                    strz = getXYZ(labnumber, rdr("z"))

                    'CalculateSpecial(rdr("formula"), labnumber, rdr("idtesttype")) ', rdr("x"), rdr("y"), rdr("z"))
                    strf = strf.Replace(".", DecSep).Replace(",", DecSep)
                    strx = strx.Replace(".", DecSep).Replace(",", DecSep)
                    stry = stry.Replace(".", DecSep).Replace(",", DecSep)
                    strz = strz.Replace(".", DecSep).Replace(",", DecSep)
                    With LtFn
                        .Function = strf
                        If strf <> oldFn Then
                            .BuildFunctionTree()
                            oldFn = strf
                        End If
                        .X = GetVal(strx)
                        .Y = GetVal(stry)
                        .Z = GetVal(strz)

                        resultcal = Math.Round(.Result, dgt, MidpointRounding.AwayFromZero)

                        Dim resultabnormalflag As String = UpdateAbnormalFlag(resultcal, rdr("idtesttype"), gender)


                        Dim cnupdate As New clsGreConnect
                        cnupdate.buildConn()
                        Dim strupdate As String
                        strupdate = "update jobdetail set measurementvalue='" & resultcal & "',resultabnormalflag='" & resultabnormalflag & "' where labnumber='" & labnumber & "' and idtesttype='" & rdr("idtesttype") & "'"
                        cnupdate.ExecuteNonQuery(strupdate)
                        cnupdate.CloseConn()

                    End With


                End If
            Loop
        End If



    End Sub
    Private Function UpdateAbnormalFlag(ByVal value As String, ByVal idtest As Integer, ByVal gender As Integer) As String
        Dim strsql As String
        Dim min, max As String
        Dim minvalid As Boolean = False
        Dim maxvalid As Boolean = False

        Dim returnresult As String

        If gender = genderMale Then
            strsql = "select min as bawah,max as atas from testtype where id='" & idtest & "'"
        ElseIf gender = genderFemale Then
            strsql = "select minwoman as bawah,maxwoman as atas from testtype where id='" & idtest & "'"
        ElseIf gender = genderChild Then
            strsql = "select minchild as bawah,maxchild as atas from testtype where id='" & idtest & "'"
        End If

        Dim cn As New clsGreConnect
        cn.buildConn()

        returnresult = ""
        Dim cmd As New SqlClient.SqlCommand(strsql, cn.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

        If rdr.HasRows Then
            Do While rdr.Read

                If Not IsDBNull(rdr("bawah")) Then
                    min = rdr("bawah")
                    minvalid = True
                Else
                    MessageBox.Show("Nilai batas bawah belum diisi")
                End If

                If Not IsDBNull(rdr("atas")) Then
                    max = rdr("atas")
                    maxvalid = True
                Else
                    MessageBox.Show("Nilai batas atas belum diisi")
                End If

                If minvalid = True And maxvalid = True Then
                    Dim nilai As Double
                    Dim bawah As Double
                    Dim atas As Double

                    nilai = 0
                    If value.Length > 0 Then
                        nilai = CDbl(value)
                    End If

                    bawah = 0
                    If min.Length > 0 Then
                        bawah = CDbl(min)
                    End If

                    atas = 0
                    If max.Length > 0 Then
                        atas = CDbl(max)
                    End If

                    Dim result As String
                    result = "A"

                    If nilai < bawah Then
                        result = "L"
                    End If

                    If nilai > atas Then
                        result = "H"
                    End If

                    If nilai < atas And nilai > bawah Then
                        result = "N"
                    End If

                    If nilai = atas Then
                        result = "N"
                    End If

                    If nilai = bawah Then
                        result = "N"
                    End If
                    returnresult = result
                End If


            Loop
        End If
        Return returnresult

    End Function


    Private Function getXYZ(ByVal ln As String, ByVal idtest_to_lookup As Integer) As String
        Dim str As String
        Dim valx As String
        str = "select measurementvalue from jobdetail where labnumber='" & ln & "' and idtesttype='" & idtest_to_lookup & "'"
        Dim cn As New clsGreConnect
        cn.buildConn()

        Dim cmdx As New SqlClient.SqlCommand(str, cn.grecon)
        Dim rdrx As SqlDataReader
        valx = ""
        rdrx = cmdx.ExecuteReader
        If rdrx.HasRows Then
            Do While rdrx.Read
                valx = rdrx("measurementvalue")
            Loop
        End If

        rdrx.Close()
        cmdx.Dispose()
        cn.CloseConn()
        Return valx
    End Function

    
    

    Private Function GetVal(ByVal s As String) As Decimal
        If s.Trim = "" Then Return 0
        Try
            Return CDec(s.Replace(".", DecSep).Replace(",", DecSep))
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Private Function CalculateSpecial(ByVal F As String, ByVal labnumber As String, ByVal idtest As Integer) ', ByVal x As Integer, ByVal y As Integer, ByVal z As Integer) As Decimal
        'if x then
        Dim strsql As String
        strsql = "select measurementvalue from jobdetail where idtesttype="
    End Function

    Public Structure StrucResult
        Public idpaliosampleResult As String
        Public idtestResult As String
        Public valueTestResult As String
    End Structure


    Public Sub WriteLog()
        'Dim filename As String = Application.StartupPath & "\test.log"
        'Dim sw As StreamWriter = filename.Aggregate(filename)
        'sw.WriteLine(Now() & " " & "sample log file entry")
        'sw.Close()

        Dim strFile As String = Application.StartupPath & "\ErrorLog_" & DateTime.Today.ToString("dd-MMM-yyyy") & ".txt"
        Dim sw As StreamWriter
        Try
            If (Not File.Exists(strFile)) Then
                sw = File.CreateText(strFile)
                sw.WriteLine("Start Error Log for today")
            Else
                sw = File.AppendText(strFile)
            End If
            sw.WriteLine("Error Message in  Occured at-- " & DateTime.Now)
            sw.Close()
        Catch ex As IOException
            MsgBox("Error writing to log file.")
        End Try
    End Sub
    Public Sub WriteLogList(ByVal lst As List(Of String))
        'Dim filename As String = Application.StartupPath & "\test.log"
        'Dim sw As StreamWriter = filename.Aggregate(filename)
        'sw.WriteLine(Now() & " " & "sample log file entry")
        'sw.Close()

        Dim strFile As String = Application.StartupPath & "\LogList_" & DateTime.Today.ToString("dd-MMM-yyyy") & ".txt"
        Dim sw As StreamWriter
        Try
            If (Not File.Exists(strFile)) Then
                sw = File.CreateText(strFile)
                sw.WriteLine("Start Error Log for today")
            Else
                sw = File.AppendText(strFile)
            End If
            sw.WriteLine("------- " & DateTime.Now)
            Dim i As Integer
            For i = 0 To lst.Count - 1
                sw.WriteLine(lst.Item(i))
            Next

            sw.Close()
        Catch ex As IOException
            MsgBox("Error writing to log file.")
        End Try
    End Sub
    Public Sub WriteLogListTitle(ByVal lst As List(Of String), ByVal title As String)
        'Dim filename As String = Application.StartupPath & "\test.log"
        'Dim sw As StreamWriter = filename.Aggregate(filename)
        'sw.WriteLine(Now() & " " & "sample log file entry")
        'sw.Close()

        Dim strFile As String = Application.StartupPath & "\LogListTitle_" & DateTime.Today.ToString("dd-MMM-yyyy") & ".txt"
        Dim sw As StreamWriter
        Try
            If (Not File.Exists(strFile)) Then
                sw = File.CreateText(strFile)
                sw.WriteLine("Start Log for today " + title)
            Else
                sw = File.AppendText(strFile)
            End If
            sw.WriteLine("------- " & DateTime.Now)
            Dim i As Integer
            For i = 0 To lst.Count - 1
                sw.WriteLine(lst.Item(i))
            Next

            sw.Close()
        Catch ex As IOException
            MsgBox("Error writing to log file.")
        End Try
    End Sub

    Public Sub WriteLogListResultTitle(ByVal lst As List(Of String), ByVal title As String)
        'Dim filename As String = Application.StartupPath & "\test.log"
        'Dim sw As StreamWriter = filename.Aggregate(filename)
        'sw.WriteLine(Now() & " " & "sample log file entry")
        'sw.Close()

        Dim strFile As String = Application.StartupPath & "\LogListResultTitle_" & DateTime.Today.ToString("dd-MMM-yyyy") & ".txt"
        Dim sw As StreamWriter
        Try
            If (Not File.Exists(strFile)) Then
                sw = File.CreateText(strFile)
                sw.WriteLine("Start Log for today " + title)
            Else
                sw = File.AppendText(strFile)
            End If
            sw.WriteLine("------- " & DateTime.Now)
            Dim i As Integer
            For i = 0 To lst.Count - 1
                sw.WriteLine(lst.Item(i))
            Next

            sw.Close()
        Catch ex As IOException
            MsgBox("Error writing to log file.")
        End Try
    End Sub

    Public Sub WriteLogLineTitle(ByVal baris As String, ByVal title As String)
        'Dim filename As String = Application.StartupPath & "\test.log"
        'Dim sw As StreamWriter = filename.Aggregate(filename)
        'sw.WriteLine(Now() & " " & "sample log file entry")
        'sw.Close()

        Dim strFile As String = Application.StartupPath & "\LogListTitle_" & DateTime.Today.ToString("dd-MMM-yyyy") & ".txt"
        Dim sw As StreamWriter
        Try
            If (Not File.Exists(strFile)) Then
                sw = File.CreateText(strFile)
                sw.WriteLine("Start Log for today " + title)
            Else
                sw = File.AppendText(strFile)
            End If
            sw.WriteLine(baris & " " & DateTime.Now)
            sw.Close()
        Catch ex As IOException
            MsgBox("Error writing to log file.")
        End Try
    End Sub

    Public Function GetLicenseFor(ByVal liname As String, ByVal usbparent As String) As String
        Dim result As Integer
        Dim ogrecon As New clsGreConnect
        ogrecon.buildConn()
        Dim strsql As String
        strsql = "select * from licensemanager where parent='" & usbparent & "' and name='" & liname & "'"

        Dim cmd As New SqlClient.SqlCommand(strsql, ogrecon.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            result = rdr("val")
        Loop
        Return result
    End Function

End Module
