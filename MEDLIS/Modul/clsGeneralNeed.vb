﻿Imports System.Data
Imports System.Data.SqlClient
Public Class clsGeneralNeed
    Dim labnum As String
    Private Sub New(ByVal labnumber As String)
        labnum = labnumber
    End Sub

    Private Function CekIfSampleIsDone() As Boolean
        Dim strsql As String
        Dim result As Boolean
        strsql = "select status from jobdetail where labnumber='" & labnum & "'"

        Dim objcon As New clsGreConnect
        objcon.buildConn()

        Dim cmd As New SqlClient.SqlCommand(strsql, objcon.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        result = True
        Do While rdr.Read
            If rdr("status") = "0" Then '0 not finish
                result = False
            End If
        Loop

        Return result
    End Function

    Public Sub UpdateJobStatus()
        'status table job 0 berarti masih baru registrasi, 1 berarti sudah ambil sampel siap baca, 2 berarti sudah di baca
        If CekIfSampleIsDone() Then
            Dim strsql As String
            strsql = "update job set status='2' where labnumber='" & labnum & "'"

            Dim objupd As New clsGreConnect
            objupd.buildConn()
            objupd.ExecuteNonQuery(strsql)
            objupd.CloseConn()
        End If
    End Sub

End Class
