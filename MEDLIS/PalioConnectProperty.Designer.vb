﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PalioConnectProperty
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PalioConnectProperty))
        Me.cbInstrument = New System.Windows.Forms.ComboBox
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.SaveMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.EditMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.cancelMnu = New System.Windows.Forms.ToolStripButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtipaddress = New System.Windows.Forms.TextBox
        Me.lbltest = New System.Windows.Forms.Label
        Me.btntest = New System.Windows.Forms.Button
        Me.Label6 = New System.Windows.Forms.Label
        Me.tmrreceive = New System.Windows.Forms.Timer(Me.components)
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cbInstrument
        '
        Me.cbInstrument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbInstrument.FormattingEnabled = True
        Me.cbInstrument.Location = New System.Drawing.Point(134, 37)
        Me.cbInstrument.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cbInstrument.Name = "cbInstrument"
        Me.cbInstrument.Size = New System.Drawing.Size(240, 23)
        Me.cbInstrument.TabIndex = 55
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SaveMnu, Me.ToolStripSeparator6, Me.EditMnu, Me.ToolStripSeparator7, Me.cancelMnu})
        Me.ToolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(411, 46)
        Me.ToolStrip1.TabIndex = 56
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'SaveMnu
        '
        Me.SaveMnu.Image = Global.MEDLIS.My.Resources.Resources.save
        Me.SaveMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SaveMnu.Name = "SaveMnu"
        Me.SaveMnu.Size = New System.Drawing.Size(67, 43)
        Me.SaveMnu.Text = "Save"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 46)
        '
        'EditMnu
        '
        Me.EditMnu.Image = Global.MEDLIS.My.Resources.Resources.write_edit
        Me.EditMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.EditMnu.Name = "EditMnu"
        Me.EditMnu.Size = New System.Drawing.Size(63, 43)
        Me.EditMnu.Text = "Edit"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(6, 46)
        '
        'cancelMnu
        '
        Me.cancelMnu.AutoSize = False
        Me.cancelMnu.Image = Global.MEDLIS.My.Resources.Resources.cancel
        Me.cancelMnu.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cancelMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cancelMnu.Name = "cancelMnu"
        Me.cancelMnu.Size = New System.Drawing.Size(85, 39)
        Me.cancelMnu.Text = "Cancel"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtipaddress)
        Me.GroupBox1.Controls.Add(Me.lbltest)
        Me.GroupBox1.Controls.Add(Me.btntest)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.cbInstrument)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 48)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox1.Size = New System.Drawing.Size(394, 194)
        Me.GroupBox1.TabIndex = 57
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Instrument"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(50, 68)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(65, 15)
        Me.Label7.TabIndex = 64
        Me.Label7.Text = "IP Address"
        '
        'txtipaddress
        '
        Me.txtipaddress.Location = New System.Drawing.Point(134, 65)
        Me.txtipaddress.Name = "txtipaddress"
        Me.txtipaddress.Size = New System.Drawing.Size(180, 23)
        Me.txtipaddress.TabIndex = 63
        '
        'lbltest
        '
        Me.lbltest.AutoSize = True
        Me.lbltest.Location = New System.Drawing.Point(133, 122)
        Me.lbltest.Name = "lbltest"
        Me.lbltest.Size = New System.Drawing.Size(25, 15)
        Me.lbltest.TabIndex = 62
        Me.lbltest.Text = "......"
        '
        'btntest
        '
        Me.btntest.Location = New System.Drawing.Point(134, 150)
        Me.btntest.Name = "btntest"
        Me.btntest.Size = New System.Drawing.Size(240, 23)
        Me.btntest.TabIndex = 61
        Me.btntest.Text = "Test Connection"
        Me.btntest.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(14, 41)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(101, 15)
        Me.Label6.TabIndex = 60
        Me.Label6.Text = "Intrument Name"
        '
        'tmrreceive
        '
        Me.tmrreceive.Interval = 1000
        '
        'PalioConnectProperty
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(411, 250)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "PalioConnectProperty"
        Me.Text = "Palio Connect Property"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cbInstrument As System.Windows.Forms.ComboBox
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents SaveMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents EditMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cancelMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lbltest As System.Windows.Forms.Label
    Friend WithEvents btntest As System.Windows.Forms.Button
    Friend WithEvents tmrreceive As System.Windows.Forms.Timer
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtipaddress As System.Windows.Forms.TextBox
End Class
