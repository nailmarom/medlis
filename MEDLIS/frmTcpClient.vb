﻿Imports System.Data.SqlClient
Imports System.Text
Public Class frmTcpClient

    Private clock As Integer
    Private clientPalio As TCPControl
    Private clsCreator As clsPalioLanguage
    Private clsCreator2 As clsPalioLanguage2


    'list for language 
    Private language As List(Of String)
    Private language_result As List(Of String)
    Private resultToManage As List(Of String)
    '=============================================


    Private myList As New List(Of String)


    '--counter untuk lis
    Private counter As Integer
    Private itemcount As Integer
    Private counter_result As Integer
    Private itemcount_result As Integer


    Private cansend As Boolean = True
    Private waitfor8 As Boolean = False
    Private nakcounter As Integer = 10

    Private isTimeToDoResult As Boolean = False

    '=========================
    Dim fpath As String
    Dim fInstrument As String
    Private statechoice As Integer
    Dim greobject As clsGreConnect
    Dim dtable As DataTable
    Dim selectedLabNumber As String

    Private isAskingResult As Boolean = False
    Private isSendingQuery As Boolean = False
    Private isWaitingListening As Boolean = True

    Private Delegate Sub UpdateTextDelegate(ByVal tb As TextBox, ByVal txt As String)

    Private Delegate Sub LedGreenDelegate(ByVal pnl As Panel)
    Private Delegate Sub LedBlueDelegate(ByVal pnl As Panel)
    Private Delegate Sub LedGrayDelegate(ByVal pnl As Panel)

    Private Sub cmdConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConnect.Click
        Try
            clientPalio = New TCPControl(txtipaddress.Text, CInt(txtport.Text))
            If clientPalio.theClient.Connected Then
                cmdConnect.Text = "connected"
                cmdsendlist.Enabled = True
            End If

            AddHandler clientPalio.MessageReceive, AddressOf OnlineReceive
        Catch ex As Exception
            MessageBox.Show("Mohon periksa koneksi network Anda")
        End Try


    End Sub

    Private Sub UpdateText(ByVal tb As TextBox, ByVal txt As String)
        If tb.InvokeRequired Then
            tb.Invoke(New UpdateTextDelegate(AddressOf UpdateText), New Object() {tb, txt})
        Else
            If txt IsNot Nothing Then tb.AppendText(txt & vbCrLf)

        End If
    End Sub
    Private Sub LedGreen(ByVal pnl As Panel)
        If pnl.InvokeRequired Then
            pnl.Invoke(New LedGreenDelegate(AddressOf LedGreen), New Object() {pnl})
        Else
            pnl.BackColor = Color.GreenYellow
        End If
    End Sub

    Private Sub LedBlue(ByVal pnl As Panel)
        If pnl.InvokeRequired Then
            pnl.Invoke(New LedGreenDelegate(AddressOf LedBlue), New Object() {pnl})
        Else
            pnl.BackColor = Color.Blue

        End If
    End Sub
    Private Sub LedGray(ByVal pnl As Panel)
        If pnl.InvokeRequired Then
            pnl.Invoke(New LedGreenDelegate(AddressOf LedGray), New Object() {pnl})
        Else
            pnl.BackColor = Color.Gray
        End If
    End Sub

    Private Sub OnlineReceive(ByVal sender As TCPControl, ByVal data As String)
        UpdateText(txtRcv, data)
        data = Trim(data)
        If isAskingResult = False And isSendingQuery = True And isWaitingListening = False Then
            LedBlue(Panel1)
            If data = Trim(Chr(6)) Then
                cansend = True
            End If
            If data = Chr(21) Then
                waitfor8 = True
            End If


        ElseIf isAskingResult = True And isSendingQuery = False And isWaitingListening = False Then
            'chr 6 is ACK show we can go to next
            If data = Chr(6) Then
                cansend = True
            End If

            'chr 21 NACK, must wait for 8 second
            If data = Chr(21) Then
                waitfor8 = True
            End If
            If data = Chr(4) Then
                cansend = False
            End If

        ElseIf isSendingQuery = False And isAskingResult = False And isWaitingListening = True Then

            If data.Substring(data.Length - 1) = Chr(10) Then

                LedBlue(Panel1)
                resultToManage.Add(Trim(data))
                clientPalio.SendData(Chr(6))
                LedGreen(Panel1)
            ElseIf data = Chr(5) Then
                clientPalio.SendData(Chr(6))
            ElseIf data = Chr(4) Then
                LedGray(Panel1)
                isTimeToDoResult = True


            End If
        End If
    End Sub

    Private Sub sendList(ByVal data As String)
        If clientPalio.theClient.Connected Then
            txtshow.AppendText(data & vbCrLf)
            clientPalio.SendData(data & vbCrLf)
        End If
    End Sub

   

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If cansend = True And counter < itemcount And waitfor8 = False Then
            cansend = False

            '===========color
            clock = clock + 1
            If clock Mod 2 = 0 Then
                Panel1.BackColor = Color.GreenYellow
            Else
                Panel1.BackColor = Color.Gray
            End If
            ' sendList(myList.Item(counter))
            '===============
            sendList(language.Item(counter))
            LedGreen(Panel1)
            If language.Item(counter) = Chr(4) Then
                cmdsendlist.Text = "Kirim"
                isSendingQuery = False
                isWaitingListening = True
                Timer1.Stop()
                Timer1.Enabled = False
                cmdsendlist.Enabled = True
                txtRcv.Clear()
                txtshow.Clear()
            End If

            counter = counter + 1

        ElseIf waitfor8 = True Then
            nakcounter = nakcounter - 1
            If nakcounter = 0 Then
                nakcounter = 10
                cansend = True
                waitfor8 = False
                counter = counter - 1
            End If
        End If
    End Sub

    Private Sub frmTcpClient_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        isPalioWindowsOpen = 0
    End Sub

    Private Sub frmTcpClient_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            isPalioWindowsOpen = 1
            Dim strlival As Integer
            Dim usbparent As Integer
            usbparent = Capability(890, 10)
            Button1.Visible = False

            ReadLicense()
            strlival = GetLicenseFor("palio100", usbparent)

            If CInt(Capability(950, 10)) <> strlival Then
                'If Capability(936, "Palio100".Length) <> "Palio100" Then
                For Each ctrl As Control In Controls
                    If (ctrl.GetType() Is GetType(Button)) Then
                        Dim btn As Button = CType(ctrl, Button)
                        btn.Enabled = False
                    End If
                Next
                MessageBox.Show("Anda tidak punya license")
                Exit Sub
            End If

            'ReadAll(918, "Hema".Length)
            'ReadAll(927, "Dirui240".Length)
            'ReadAll(936, "Palio100".Length)



            btnresult.Left = -100
            cmdsendlist.Enabled = False

            Timer3.Enabled = True
            btnresult.Visible = False

            LoadInstrumentName()
            fpath = System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData, "palioinstrument1.txt")
            If ReadInsProperty() = True Then
                cbpalioAvailable.SelectedIndex = cbpalioAvailable.FindStringExact(fInstrument)
            Else
                SaveFile()
                ReadInsProperty()
                cbpalioAvailable.SelectedIndex = cbpalioAvailable.FindStringExact(fInstrument)
            End If
            '================= load
            statechoice = ALREADYSAMPLING
            Dim strsql As String
            'yg asli
            'strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail,palioinstrument where status='" & ALREADYSAMPLING & "' and active='" & ACTIVESAMPLE & "' and jobdetail.idtesttype=palioinstrument.idtest and palioinstrument.name='" & fInstrument & "')"

            strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail,palioinstrument where status='" & ALREADYSAMPLING & "' and active='" & ACTIVESAMPLE & "' and jobdetail.idtesttype=palioinstrument.idtest and palioinstrument.name='" & fInstrument & "' and jobdetail.barcode<>'')"

            dgvJoblist.DataSource = Nothing
            If dgvJoblist.Columns.Contains("Detail") Then
                dgvJoblist.Columns.Clear()
            End If
            clearDGVdetail()
            Joblist(strsql)
            resultToManage = New List(Of String)

        Catch ex As Exception
            For Each ctrl As Control In Controls
                If (ctrl.GetType() Is GetType(Button)) Then
                    Dim btn As Button = CType(ctrl, Button)
                    btn.Enabled = False
                End If
            Next
            MessageBox.Show("license error")
            'Exit Sub
        End Try
        
    End Sub



    Private Sub cmdsendlist_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdsendlist.Click

        If clientPalio.theClient.Connected Then
            isAskingResult = False
            isSendingQuery = True
            isWaitingListening = False
            clock = 0

            clsCreator = New clsPalioLanguage
            clsCreator.fInstrument = fInstrument
            clsCreator.CreateLanguage()


            language = New List(Of String)
            language = clsCreator.queryCreate
            itemcount = language.Count
            counter = 0
            If language.Count > 0 Then
                cmdsendlist.Text = "proses kirim"
                cmdsendlist.Enabled = False
                Timer1.Enabled = True
            Else
                MessageBox.Show("Tidak ada data yang bisa dikirimkan")
            End If

        End If
       
    End Sub

   
    '==============================================================
    'interface related

    Private Sub LoadInstrumentName()
        Dim strins As String
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        strins = "select distinct name from palioinstrument"
        Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            cbpalioAvailable.Items.Add(rdr("name"))
        Loop
        cbpalioAvailable.SelectedIndex = 0
        rdr.Close()
        cmd = Nothing
        ogre.CloseConn()

    End Sub

    Private Sub Joblist(ByVal strsql As String)
        'Dim strsql As String
        'strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.id as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.status='0'"
        greobject = New clsGreConnect
        greobject.buildConn()

        dtable = New DataTable
        dtable = greobject.ExecuteQuery(strsql)

        'dgvjoblist = New DataGridView

        dgvJoblist.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvJoblist.Columns.Count - 1
            dgvJoblist.Columns(j).Visible = False
        Next

        dgvJoblist.Columns("labnumber").Visible = True
        dgvJoblist.Columns("labnumber").HeaderText = "Lab Number"
        dgvJoblist.Columns("labnumber").Width = 90

        dgvJoblist.Columns("name").Visible = True
        dgvJoblist.Columns("name").HeaderText = "Nama pasien"
        dgvJoblist.Columns("name").Width = 180

        dgvJoblist.Columns("address").Visible = True
        dgvJoblist.Columns("address").HeaderText = "Alamat"
        dgvJoblist.Columns("address").Width = 220

        Dim buttonColumn As New DataGridViewButtonColumn()
        With buttonColumn
            .Name = "Detail"
            .HeaderText = "Detail"
            .Width = 90
            .Text = "Detail"
            .UseColumnTextForButtonValue = True
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            .FlatStyle = FlatStyle.Standard
            .CellTemplate.Style.BackColor = Color.Yellow
            .DisplayIndex = 0
        End With
        dgvJoblist.Columns.Add(buttonColumn)

        dgvJoblist.AllowUserToAddRows = False
        dgvJoblist.RowHeadersVisible = False
    End Sub
    Private Function ReadInsProperty() As Boolean
        Dim InsName As String
        Dim result As Boolean
        Dim getstr As String
        Dim position As Integer
        If System.IO.File.Exists(fpath) Then
            Try
                Dim lines() As String = IO.File.ReadAllLines(fpath)
                Dim k As Integer
                k = 0
                For Each line As String In lines
                    position = InStr(line, "=")
                    For i = position To Len(line) - 1
                        getstr = getstr + line.Substring(i, 1)
                    Next
                    InsName = getstr
                    getstr = ""
                Next
                fInstrument = InsName
                result = True
            Catch ex As Exception
                MessageBox.Show("Seeting file tidak ada", "File not found", MessageBoxButtons.OK)
            End Try
        Else
            result = False
        End If
        Return result
    End Function
    Private Sub clearDGVdetail()
        dgvdetail.DataSource = Nothing
    End Sub

    Private Sub dgvJoblist_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvJoblist.CellContentClick
        Dim idx As Integer
        idx = dgvJoblist.Columns("Detail").Index
        'txtlabnum.Text = ""
        If e.RowIndex < 0 OrElse Not e.ColumnIndex = dgvJoblist.Columns("Detail").Index Then
            Return
        Else
            If Not dgvJoblist.CurrentRow.IsNewRow Then
                selectedLabNumber = dgvJoblist.Item("labnumber", dgvJoblist.CurrentRow.Index).Value
                PopulatedgvDetail()
            End If

        End If
    End Sub
    Private Sub PopulatedgvDetail()
        Dim newcon As New clsGreConnect
        Dim strsql As String

        If stateChoice = ALLAVAILABLESAMPLE Then 'semua

            strsql = "select * from jobdetail where labnumber='" & selectedLabNumber & "' and active='1'" ' and status='0'"
        ElseIf stateChoice = FINISHSAMPLE Then 'finish

            strsql = "select * from jobdetail where labnumber='" & selectedLabNumber & "' and active='1' and status='" & FINISHSAMPLE & "'"
        ElseIf stateChoice = ALREADYSAMPLING Then ' sampLing

            strsql = "select * from jobdetail where labnumber='" & selectedLabNumber & "' and active='1' and status='" & ALREADYSAMPLING & "'"
        ElseIf stateChoice = NEWSAMPLE Then ' baru

            strsql = "select * from jobdetail where labnumber='" & selectedLabNumber & "' and active='1' and status='" & NEWSAMPLE & "'"
        ElseIf stateChoice = BEINGANALYZE Then ' baru

            strsql = "select * from jobdetail where labnumber='" & selectedLabNumber & "' and active='1' and status='" & BEINGANALYZE & "'"
        End If
        newcon.buildConn()

        Dim strStatus As String
        Dim statusample As String


        dgvdetail.AllowUserToAddRows = False
        dgvdetail.RowHeadersVisible = False

        newcon.buildConn()
        dtable = New DataTable

        dtable = newcon.ExecuteQuery(strsql)


        dgvdetail.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvdetail.Columns.Count - 1
            dgvdetail.Columns(j).Visible = False
        Next

        dgvdetail.Columns("universaltest").Visible = True
        dgvdetail.Columns("universaltest").HeaderText = "Test Name"
        dgvdetail.Columns("universaltest").Width = 200

        dgvdetail.Columns("barcode").Visible = True
        dgvdetail.Columns("barcode").HeaderText = "Barcode"
        dgvdetail.Columns("barcode").Width = 100

        newcon.CloseConn()

    End Sub

    Private Sub cbpalioAvailable_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbpalioAvailable.SelectedIndexChanged
        ShowInstrumentProperty()
    End Sub
    Private Sub ShowInstrumentProperty()
        Dim strins As String
        Dim ogre As New clsGreConnect
        ogre.buildConn()


        txtipaddress.ReadOnly = False
        strins = "select * from palioinstrumentconnect where name='" & Trim(cbPalioAvailable.Text) & "'"
        Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("ipaddress")) Then
                    txtIpaddress.Text = rdr("ipaddress")
                End If

            Loop
        End If

        txtipaddress.ReadOnly = True
    End Sub

    Private Sub btnPalioSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPalioSimpan.Click
        SaveFile()
        MessageBox.Show("Perubahan sudah tersimpan")
    End Sub
    Private Sub SaveFile()
        Try
            If cbpalioAvailable.Text <> "" And txtipaddress.Text.Length > 0 Then
                WriteInsProperty()

                '========== update instrument
                Dim strupdate As String
                strupdate = "update palioinstrumentconnect set ipaddress='" & Trim(txtipaddress.Text) & "' where name='" & Trim(cbpalioAvailable.Text) & "'"

                Dim ogre As New clsGreConnect
                ogre.buildConn()
                ogre.ExecuteNonQuery(strupdate)
                ogre.CloseConn()
                '=============================
                'MessageBox.Show("Perubahan sudah tersimpan")
            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Terjadi kesalahan err-p1")
        End Try
        
    End Sub

    Public Sub WriteInsProperty()
        Try
            'Dim i As Integer
            'Dim aryText(4) As String
            Dim insName As String


            'aryText(0) = "Port=" & fportName
            'aryText(1) = "Baud=" & fbaud
            'aryText(2) = "Parity=" & fparity
            'aryText(3) = "Stop=" & fstop
            'aryText(4) = "Data=" & fdata
            'aryText(5) = "Instrument=" & fInstrument
            insName = "Instrument=" & cbpalioAvailable.Text

            Dim objWriter As New System.IO.StreamWriter(fpath)
            objWriter.WriteLine(insName)

            'For i = 0 To 3
            '    objWriter.WriteLine(aryText(i))
            'Next
            objWriter.Close()
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Terjadi kesalahan err-p2")
        End Try
        
    End Sub
    '========================== result request ===============
    Private Sub btnresult_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnresult.Click
        If clientPalio.theClient.Connected Then
            isAskingResult = True
            isSendingQuery = False
            isWaitingListening = False

            clsCreator = New clsPalioLanguage
            clsCreator.fInstrument = fInstrument
            clsCreator.CreateRequestForResult()


            language_result = New List(Of String)
            language_result = clsCreator.qforRCreate
            itemcount_result = language_result.Count
            counter_result = 0



            If Timer1.Enabled = True Then
                MessageBox.Show("proses pengiriman data sedang berjalan, mohon tunggu sampai selesai")
                Exit Sub
            Else
                cansend = True
                Timer2.Enabled = True
            End If

        End If
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        If cansend = True And counter_result < itemcount_result And waitfor8 = False Then
            cansend = False
            sendList(language_result.Item(counter_result))
            counter_result = counter_result + 1
            If counter_result = itemcount_result Then
                isAskingResult = False
                isWaitingListening = True
                Timer2.Stop()
            End If
        ElseIf waitfor8 = True Then
            nakcounter = nakcounter - 1
            If nakcounter = 0 Then
                nakcounter = 10
                cansend = True
                waitfor8 = False
            End If
        End If
    End Sub
    '======================================
    '======================================
    'manage result
    Private Sub Timer3_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer3.Tick
        Try
            If isTimeToDoResult = True Then
                isWaitingListening = False
                isTimeToDoResult = False
                isSendingQuery = False
                isAskingResult = False
                btnresult.Enabled = False

                '========== license
                Dim strlival As Integer
                Dim usbparent As Integer
                usbparent = Capability(890, 10)

                ReadLicense()
                strlival = GetLicenseFor("palio100", usbparent)

                If CInt(Capability(950, 10)) <> strlival Then
                    MessageBox.Show("Anda tidak punya license/license Anda tidak terpasang dengan benar")
                    Exit Sub
                End If
                '===============


                clsCreator = New clsPalioLanguage
                clsCreator.ManageResult(resultToManage)
                resultToManage.Clear()

                '===================
                statechoice = ALREADYSAMPLING
                Dim strsql As String
                strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail,palioinstrument where status='" & ALREADYSAMPLING & "' and active='" & ACTIVESAMPLE & "' and jobdetail.idtesttype=palioinstrument.idtest and palioinstrument.name='" & fInstrument & "')"

                dgvJoblist.DataSource = Nothing
                If dgvJoblist.Columns.Contains("Detail") Then
                    dgvJoblist.Columns.Clear()
                End If
                clearDGVdetail()
                Joblist(strsql)
                '============
                txtRcv.Clear()
                txtshow.Clear()
                isWaitingListening = True
                btnresult.Enabled = True

            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString & " | " & ex.TargetSite.Name, "Terjadi kesalahan " & "  " & Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        clsCreator = New clsPalioLanguage
        clsCreator.fInstrument = fInstrument
        clsCreator.CreateLanguage()

        language = New List(Of String)
        language = clsCreator.queryCreate
        itemcount = language.Count

    End Sub
End Class