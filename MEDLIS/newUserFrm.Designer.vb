﻿'
' Created by SharpDevelop.
' User: User
' Date: 10/14/2014
' Time: 1:30 PM
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Partial Class newUserFrm
	Inherits System.Windows.Forms.Form
	
	''' <summary>
	''' Designer variable used to keep track of non-visual components.
	''' </summary>
	Private components As System.ComponentModel.IContainer
	
	''' <summary>
	''' Disposes resources used by the form.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If components IsNot Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub
	
	''' <summary>
	''' This method is required for Windows Forms designer support.
	''' Do not change the method contents inside the source code editor. The Forms designer might
	''' not be able to load this method if it was changed manually.
	''' </summary>
	Private Sub InitializeComponent()
		Me.textBox1 = New System.Windows.Forms.TextBox()
		Me.textBox2 = New System.Windows.Forms.TextBox()
		Me.textBox3 = New System.Windows.Forms.TextBox()
		Me.textBox4 = New System.Windows.Forms.TextBox()
		Me.textBox5 = New System.Windows.Forms.TextBox()
		Me.textBox6 = New System.Windows.Forms.TextBox()
		Me.label1 = New System.Windows.Forms.Label()
		Me.label2 = New System.Windows.Forms.Label()
		Me.label3 = New System.Windows.Forms.Label()
		Me.label4 = New System.Windows.Forms.Label()
		Me.label5 = New System.Windows.Forms.Label()
		Me.label6 = New System.Windows.Forms.Label()
		Me.chkRegister = New System.Windows.Forms.CheckBox()
		Me.chkSetting = New System.Windows.Forms.CheckBox()
		Me.chkInstrument = New System.Windows.Forms.CheckBox()
		Me.chkSampling = New System.Windows.Forms.CheckBox()
		Me.chkActive = New System.Windows.Forms.CheckBox()
		Me.lblStatus = New System.Windows.Forms.Label()
		Me.dataGrid1 = New System.Windows.Forms.DataGrid()
		CType(Me.dataGrid1,System.ComponentModel.ISupportInitialize).BeginInit
		Me.SuspendLayout
		'
		'textBox1
		'
		Me.textBox1.Location = New System.Drawing.Point(465, 35)
		Me.textBox1.Name = "textBox1"
		Me.textBox1.Size = New System.Drawing.Size(241, 20)
		Me.textBox1.TabIndex = 0
		'
		'textBox2
		'
		Me.textBox2.Location = New System.Drawing.Point(465, 82)
		Me.textBox2.Name = "textBox2"
		Me.textBox2.Size = New System.Drawing.Size(241, 20)
		Me.textBox2.TabIndex = 1
		'
		'textBox3
		'
		Me.textBox3.Location = New System.Drawing.Point(465, 120)
		Me.textBox3.Name = "textBox3"
		Me.textBox3.Size = New System.Drawing.Size(241, 20)
		Me.textBox3.TabIndex = 2
		'
		'textBox4
		'
		Me.textBox4.Location = New System.Drawing.Point(465, 293)
		Me.textBox4.Name = "textBox4"
		Me.textBox4.Size = New System.Drawing.Size(241, 20)
		Me.textBox4.TabIndex = 5
		'
		'textBox5
		'
		Me.textBox5.Location = New System.Drawing.Point(465, 255)
		Me.textBox5.Name = "textBox5"
		Me.textBox5.Size = New System.Drawing.Size(241, 20)
		Me.textBox5.TabIndex = 4
		'
		'textBox6
		'
		Me.textBox6.Location = New System.Drawing.Point(465, 212)
		Me.textBox6.Name = "textBox6"
		Me.textBox6.Size = New System.Drawing.Size(241, 20)
		Me.textBox6.TabIndex = 3
		'
		'label1
		'
		Me.label1.AutoSize = true
		Me.label1.Location = New System.Drawing.Point(380, 42)
		Me.label1.Name = "label1"
		Me.label1.Size = New System.Drawing.Size(55, 13)
		Me.label1.TabIndex = 6
		Me.label1.Text = "Username"
		'
		'label2
		'
		Me.label2.AutoSize = true
		Me.label2.Location = New System.Drawing.Point(382, 82)
		Me.label2.Name = "label2"
		Me.label2.Size = New System.Drawing.Size(53, 13)
		Me.label2.TabIndex = 7
		Me.label2.Text = "Password"
		'
		'label3
		'
		Me.label3.AutoSize = true
		Me.label3.Location = New System.Drawing.Point(400, 219)
		Me.label3.Name = "label3"
		Me.label3.Size = New System.Drawing.Size(35, 13)
		Me.label3.TabIndex = 9
		Me.label3.Text = "Nama"
		'
		'label4
		'
		Me.label4.AutoSize = true
		Me.label4.Location = New System.Drawing.Point(349, 120)
		Me.label4.Name = "label4"
		Me.label4.Size = New System.Drawing.Size(86, 13)
		Me.label4.TabIndex = 8
		Me.label4.Text = "Ulangi Password"
		'
		'label5
		'
		Me.label5.AutoSize = true
		Me.label5.Location = New System.Drawing.Point(389, 262)
		Me.label5.Name = "label5"
		Me.label5.Size = New System.Drawing.Size(46, 13)
		Me.label5.TabIndex = 10
		Me.label5.Text = "Telepon"
		'
		'label6
		'
		Me.label6.AutoSize = true
		Me.label6.Location = New System.Drawing.Point(403, 300)
		Me.label6.Name = "label6"
		Me.label6.Size = New System.Drawing.Size(32, 13)
		Me.label6.TabIndex = 11
		Me.label6.Text = "Email"
		'
		'chkRegister
		'
		Me.chkRegister.AutoSize = true
		Me.chkRegister.Location = New System.Drawing.Point(465, 337)
		Me.chkRegister.Name = "chkRegister"
		Me.chkRegister.Size = New System.Drawing.Size(65, 17)
		Me.chkRegister.TabIndex = 12
		Me.chkRegister.Text = "Register"
		Me.chkRegister.UseVisualStyleBackColor = true
		'
		'chkSetting
		'
		Me.chkSetting.AutoSize = true
		Me.chkSetting.Location = New System.Drawing.Point(465, 413)
		Me.chkSetting.Name = "chkSetting"
		Me.chkSetting.Size = New System.Drawing.Size(91, 17)
		Me.chkSetting.TabIndex = 13
		Me.chkSetting.Text = "Setting Admin"
		Me.chkSetting.UseVisualStyleBackColor = true
		'
		'chkInstrument
		'
		Me.chkInstrument.AutoSize = true
		Me.chkInstrument.Location = New System.Drawing.Point(465, 360)
		Me.chkInstrument.Name = "chkInstrument"
		Me.chkInstrument.Size = New System.Drawing.Size(119, 17)
		Me.chkInstrument.TabIndex = 14
		Me.chkInstrument.Text = "Instrument Operator"
		Me.chkInstrument.UseVisualStyleBackColor = true
		'
		'chkSampling
		'
		Me.chkSampling.Location = New System.Drawing.Point(465, 383)
		Me.chkSampling.Name = "chkSampling"
		Me.chkSampling.Size = New System.Drawing.Size(104, 24)
		Me.chkSampling.TabIndex = 15
		Me.chkSampling.Text = "Sample Take"
		Me.chkSampling.UseVisualStyleBackColor = true
		'
		'chkActive
		'
		Me.chkActive.Location = New System.Drawing.Point(465, 164)
		Me.chkActive.Name = "chkActive"
		Me.chkActive.Size = New System.Drawing.Size(104, 24)
		Me.chkActive.TabIndex = 16
		Me.chkActive.Text = "Active"
		Me.chkActive.UseVisualStyleBackColor = true
		'
		'lblStatus
		'
		Me.lblStatus.AutoSize = true
		Me.lblStatus.Location = New System.Drawing.Point(392, 171)
		Me.lblStatus.Name = "lblStatus"
		Me.lblStatus.Size = New System.Drawing.Size(37, 13)
		Me.lblStatus.TabIndex = 17
		Me.lblStatus.Text = "Status"
		'
		'dataGrid1
		'
		Me.dataGrid1.DataMember = ""
		Me.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText
		Me.dataGrid1.Location = New System.Drawing.Point(12, 58)
		Me.dataGrid1.Name = "dataGrid1"
		Me.dataGrid1.Size = New System.Drawing.Size(321, 217)
		Me.dataGrid1.TabIndex = 18
		'
		'newUserFrm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(742, 509)
		Me.Controls.Add(Me.dataGrid1)
		Me.Controls.Add(Me.lblStatus)
		Me.Controls.Add(Me.chkActive)
		Me.Controls.Add(Me.chkSampling)
		Me.Controls.Add(Me.chkInstrument)
		Me.Controls.Add(Me.chkSetting)
		Me.Controls.Add(Me.chkRegister)
		Me.Controls.Add(Me.label6)
		Me.Controls.Add(Me.label5)
		Me.Controls.Add(Me.label3)
		Me.Controls.Add(Me.label4)
		Me.Controls.Add(Me.label2)
		Me.Controls.Add(Me.label1)
		Me.Controls.Add(Me.textBox4)
		Me.Controls.Add(Me.textBox5)
		Me.Controls.Add(Me.textBox6)
		Me.Controls.Add(Me.textBox3)
		Me.Controls.Add(Me.textBox2)
		Me.Controls.Add(Me.textBox1)
		Me.Name = "newUserFrm"
		Me.Text = "User"
		CType(Me.dataGrid1,System.ComponentModel.ISupportInitialize).EndInit
		Me.ResumeLayout(false)
		Me.PerformLayout
	End Sub
	Private dataGrid1 As System.Windows.Forms.DataGrid
	Private lblStatus As System.Windows.Forms.Label
	Private chkActive As System.Windows.Forms.CheckBox
	Private chkSampling As System.Windows.Forms.CheckBox
	Private chkInstrument As System.Windows.Forms.CheckBox
	Private chkSetting As System.Windows.Forms.CheckBox
	Private chkRegister As System.Windows.Forms.CheckBox
	Private label6 As System.Windows.Forms.Label
	Private label5 As System.Windows.Forms.Label
	Private label4 As System.Windows.Forms.Label
	Private label3 As System.Windows.Forms.Label
	Private label2 As System.Windows.Forms.Label
	Private label1 As System.Windows.Forms.Label
	Private textBox6 As System.Windows.Forms.TextBox
	Private textBox5 As System.Windows.Forms.TextBox
	Private textBox4 As System.Windows.Forms.TextBox
	Private textBox3 As System.Windows.Forms.TextBox
	Private textBox2 As System.Windows.Forms.TextBox
	Private textBox1 As System.Windows.Forms.TextBox
End Class
