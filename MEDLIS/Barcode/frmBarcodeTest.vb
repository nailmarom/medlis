﻿Imports Zen.Barcode
Imports System.Drawing
Imports System.IO
Imports System.Windows.Forms
Imports System.Drawing.Text


Public Class frmBarcodeTest

    Dim ImagewithText As Bitmap
    Private jumlahPrint As Integer = 1
    Private WithEvents DocToPrint As New Printing.PrintDocument

    Private Sub cmdCreate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdCreate.Click
        Try
            If txtkode.Text.Length <> 0 Then

                Dim jenisfont As New Font(cbfont.Text, CInt(cbsize.Text), Font.Style.Regular)
                'code 39 43 char
                'code 93 48 char
                'code 128 108 char

                'Dim gu As GraphicsUnit
                'gu = GraphicsUnit.Inch

                Dim maxbarHight As Integer
                Dim x, y As Integer
                x = CInt(cbx.Text.Trim)
                y = CInt(cby.Text.Trim)


                Dim originalbarcodeimage As Bitmap
                Dim CodeBitmap As Bitmap

                Dim gr As Graphics

                maxbarHight = CInt(cbbarhight.Text.Trim)

                If cbsimbol.Text = "Code 128" Then
                    Dim barcode128 As Code128BarcodeDraw = BarcodeDrawFactory.Code128WithChecksum
                    originalbarcodeimage = barcode128.Draw(txtkode.Text.Trim.ToUpper, maxbarHight, CInt(txtSkala.Text.Trim))
                    originalbarcodeimage.SetResolution(203.0F, 203.0F)

                    ImagewithText = New Bitmap(originalbarcodeimage.Width, originalbarcodeimage.Height + 225)
                    ImagewithText.SetResolution(203.0F, 203.0F)

                    gr = Graphics.FromImage(ImagewithText)
                    gr.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
                    gr.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
                    gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality


                    Dim measureString As String = txtlabel.Text.Trim
                    Dim stringFont As New Font("Arial", 8)

                    ' Measure string.
                    Dim stringSize As New SizeF
                    stringSize = gr.MeasureString(measureString, stringFont)

                    ' Draw rectangle representing size of string.
                    gr.DrawRectangle(New Pen(Color.Red, 1), 0.0F, 0.0F, _
                    stringSize.Width, stringSize.Height)



                    Dim myTopRectangle As New RectangleF(x, 0, originalbarcodeimage.Width, 50)
                    gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, myTopRectangle)

                    gr.DrawImageUnscaled(originalbarcodeimage, x, myTopRectangle.Bottom)
                    'gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, 0, originalbarcodeimage.Width, 100))

                    Dim myBottomRectangle As New RectangleF(x, 0, originalbarcodeimage.Width, 50)
                    gr.DrawString(txtkode.Text.Trim, jenisfont, Brushes.Black, New RectangleF(x, originalbarcodeimage.Height + y + 2, originalbarcodeimage.Width + 10, 50))
                    'Printer.
                    pic.Image = ImagewithText

                    'CodeBitmap = New Bitmap(pic.Image)

                    'CodeBitmap = New Bitmap(ImagewithText)

                    'CodeBitmap.SetResolution(300.0F, 300.0F)
                    'CodeBitmap.Save(Application.StartupPath & "\" & "001.png")
                    cmdCetak.Enabled = True

                ElseIf cbsimbol.Text = "Code EAN 13" Then
                    Dim barcode128 As CodeEan13BarcodeDraw = BarcodeDrawFactory.CodeEan13WithChecksum
                    originalbarcodeimage = barcode128.Draw(txtkode.Text.Trim.ToUpper, maxbarHight, CInt(txtSkala.Text.Trim))
                    ImagewithText = New Bitmap(originalbarcodeimage.Width, originalbarcodeimage.Height + 50)
                    gr = Graphics.FromImage(ImagewithText)
                    gr.DrawImageUnscaled(originalbarcodeimage, x, y)
                    gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, 10, originalbarcodeimage.Width, 50))
                    gr.DrawString(txtkode.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, originalbarcodeimage.Height + 23, originalbarcodeimage.Width + 10, 50))
                    pic.Image = ImagewithText

                    CodeBitmap = New Bitmap(pic.Image)
                    CodeBitmap.Save(Application.StartupPath & "\" & "001.png")
                    cmdCetak.Enabled = True
                ElseIf cbsimbol.Text = "Code EAN 8" Then
                    Dim barcode128 As CodeEan8BarcodeDraw = BarcodeDrawFactory.CodeEan8WithChecksum
                    originalbarcodeimage = barcode128.Draw(txtkode.Text.Trim.ToUpper, maxbarHight, CInt(txtSkala.Text.Trim))
                    ImagewithText = New Bitmap(originalbarcodeimage.Width, originalbarcodeimage.Height + 50)
                    gr = Graphics.FromImage(ImagewithText)
                    gr.DrawImageUnscaled(originalbarcodeimage, x, y)
                    gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, 10, originalbarcodeimage.Width, 50))
                    gr.DrawString(txtkode.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, originalbarcodeimage.Height + 10, originalbarcodeimage.Width + 10, 50))
                    pic.Image = ImagewithText

                    CodeBitmap = New Bitmap(pic.Image)
                    CodeBitmap.Save(Application.StartupPath & "\" & "001.png")
                    cmdCetak.Enabled = True
                ElseIf cbsimbol.Text = "Code 39" Then
                    Dim barcode128 As Code39BarcodeDraw = BarcodeDrawFactory.Code39WithChecksum
                    originalbarcodeimage = barcode128.Draw(txtkode.Text.Trim.ToUpper, maxbarHight, CInt(txtSkala.Text.Trim))
                    originalbarcodeimage.SetResolution(203.0F, 203.0F)

                    ImagewithText = New Bitmap(originalbarcodeimage.Width, originalbarcodeimage.Height + 225)
                    ImagewithText.SetResolution(203.0F, 203.0F)

                    gr = Graphics.FromImage(ImagewithText)
                    'gr.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
                    'gr.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
                    'gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
                    'gr.TextRenderingHint = TextRenderingHint.AntiAlias

                    Dim myRectangle As New RectangleF(x, 0, originalbarcodeimage.Width, 50)
                    gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, myRectangle)
                    gr.DrawImageUnscaled(originalbarcodeimage, x, y)
                    'gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, 0, originalbarcodeimage.Width, 100))
                    gr.DrawString(txtkode.Text.Trim, jenisfont, Brushes.Black, New RectangleF(x, originalbarcodeimage.Height + 30, originalbarcodeimage.Width + 10, 50))
                    'Printer.
                    pic.Image = ImagewithText

                    'CodeBitmap = New Bitmap(pic.Image)

                    CodeBitmap = New Bitmap(ImagewithText)

                    CodeBitmap.SetResolution(203.0F, 203.0F)
                    CodeBitmap.Save(Application.StartupPath & "\" & "001.png")
                    cmdCetak.Enabled = True
                ElseIf cbsimbol.Text = "Code 93" Then
                    Dim barcode128 As Code93BarcodeDraw = BarcodeDrawFactory.Code93WithChecksum

                    originalbarcodeimage = barcode128.Draw(txtkode.Text.Trim.ToUpper, maxbarHight, CInt(txtSkala.Text.Trim))
                    originalbarcodeimage.SetResolution(203.0F, 203.0F)

                    ImagewithText = New Bitmap(originalbarcodeimage.Width, originalbarcodeimage.Height + 225)
                    ImagewithText.SetResolution(203.0F, 203.0F)

                    gr = Graphics.FromImage(ImagewithText)
                    gr.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
                    gr.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
                    gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality


                    Dim myRectangle As New RectangleF(x, 0, originalbarcodeimage.Width, 50)
                    gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, myRectangle)
                    gr.DrawImageUnscaled(originalbarcodeimage, x, y)
                    'gr.DrawString(txtlabel.Text.Trim, jenisfont, Brushes.Black, New RectangleF(0, 0, originalbarcodeimage.Width, 100))
                    gr.DrawString(txtkode.Text.Trim, jenisfont, Brushes.Black, New RectangleF(x, originalbarcodeimage.Height + 30, originalbarcodeimage.Width + 10, 50))
                    'Printer.
                    pic.Image = ImagewithText

                    'CodeBitmap = New Bitmap(pic.Image)

                    'CodeBitmap = New Bitmap(ImagewithText)

                    'CodeBitmap.SetResolution(300.0F, 300.0F)
                    'CodeBitmap.Save(Application.StartupPath & "\" & "001.png")
                    cmdCetak.Enabled = True
                End If





            End If
            'Catch ex As Exception
            '    MsgBox(ex.Message)
            'End Try
            Exit Sub
ErrMgr:
            Select Case Err.Number

                Case Else
                    MsgBox(Err.Number)
            End Select
        Catch ex As Exception
            MessageBox.Show("Beberapa font seperti Aharoni tidak bisa digunakan")
        End Try
        
    End Sub
    Private Sub docToPrint_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) _
       Handles DocToPrint.PrintPage
        '  On Error Resume Next
        Dim i As Integer
        Dim separatorkiri As Integer = Val(cbkirimargin.Text)
        Dim marginAtas As Integer = Val(cbatasmargin.Text)
        '
        jumlahPrint = CInt(Trim(cbjumlah.Text))

        For i = 1 To jumlahPrint
            '            e.Graphics.DrawImage(pic.Image, 10, 10)
            e.Graphics.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
            e.Graphics.TextRenderingHint = TextRenderingHint.AntiAlias

            e.Graphics.DrawImageUnscaled(ImagewithText, separatorkiri, marginAtas)
            separatorkiri = separatorkiri + Val(cbjarak.Text)
        Next

    End Sub
    Private Sub frmBarcodeTest_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Try


            cbsimbol.Items.Add("Code 39")
            cbsimbol.Items.Add("Code 93")
            cbsimbol.Items.Add("Code 128")
            'cbsimbol.Items.Add("Code EAN 13")
            'cbsimbol.Items.Add("Code EAN 8")
            cbsimbol.SelectedIndex = cbsimbol.FindStringExact("Code 93")
            cbbarhight.SelectedIndex = cbbarhight.FindStringExact("100")
            cbx.SelectedIndex = cbx.FindStringExact("0")
            cby.SelectedIndex = cby.FindStringExact("45")
            'cbx.Visible = False
            'cby.Visible = False


            Dim fonts As New InstalledFontCollection

            For Each one As FontFamily In fonts.Families
                cbfont.Items.Add(one.Name)
            Next

            cbfont.Text = "Microsoft Sans Serif" 'DefaultFont.Name
            cbsize.Text = "9"
            cbkirimargin.Text = "9"
            cbatasmargin.Text = "9"
            cbjumlah.Text = "1"
            cbjarak.Text = "130"

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub cmdCetak_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdCetak.Click
        Dim frm As Form = DirectCast(ppd, Form)
        frm.WindowState = FormWindowState.Maximized
        frm.Icon = Me.Icon

        ppd.PrintPreviewControl.Zoom = 1 '0.75
        ppd.Document = DocToPrint
        ppd.Document.DefaultPageSettings.Landscape = CBool(chkorientasi.Checked)
        DirectCast((frmPreviewBarcode.ppd.Controls(1)), ToolStrip).Items.RemoveAt(0)
        ppd.ShowDialog()
    End Sub


    Private Sub cmdSaveBarcode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSaveBarcode.Click
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim strsql As String
        Dim strdelete As String

        Dim name As String = "barcode"
        Dim val As String
        val = cbsimbol.Text

        strdelete = "delete from setting where name='barcode'"
        ogre.ExecuteNonQuery(strdelete)
        ogre.CloseConn()

        Dim ogreINSERT As New clsGreConnect
        ogreINSERT.buildConn()

        strsql = "insert into setting(name,value,font,height,scala,ftsize,x,y)values('" & name & "','" & val & "','" & cbfont.Text & "','" & CInt(cbbarhight.Text) & "','" & CInt(txtSkala.Text) & "','" & CInt(cbsize.Text) & "','" & CInt(cbx.Text) & "','" & CInt(cby.Text) & "')"
        ogreINSERT.ExecuteNonQuery(strsql)
        ogreINSERT.CloseConn()

    End Sub

    Private Sub txtlabel_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtlabel.TextChanged

    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim g As Graphics
        g = e.Graphics

        Dim fnt As Font
        fnt = New Font("Arial", 11)

        Dim caption As String
        'caption = String.Format("Code128 barcode weight={0}", txtWeight.Text)
        'g.DrawString(caption, fnt, System.Drawing.Brushes.Black, 50, 50)

        Dim bmp As Bitmap
        'bmp.SetResolution(600.0F, 600.0F)
        'g.DrawImage(pictBarcode.Image, 5, 5) 'atur posisi print di kertas
        'caption = String.Format(txtBarcode.Text)
        'g.DrawString(caption, fnt, System.Drawing.Brushes.Black, 15, 50)
        'bmp = generateBarcodeForPrint()
        'bmp.SetResolution(203.0F, 203.0F)
        ' ''g.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        ' ''g.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
        ' ''g.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
        g.DrawImage(ImagewithText, CInt(cbx.Text), CInt(cby.Text))
        'g.DrawImage(bmp, xpos, ypos)

     
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
          PrintDocument1.Print()
    End Sub
End Class
