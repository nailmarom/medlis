﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPreviewBarcode
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPreviewBarcode))
        Me.ppd = New System.Windows.Forms.PrintPreviewDialog
        Me.ppc = New System.Windows.Forms.PrintPreviewControl
        Me.pd = New System.Drawing.Printing.PrintDocument
        Me.SuspendLayout()
        '
        'ppd
        '
        Me.ppd.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.ppd.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.ppd.ClientSize = New System.Drawing.Size(400, 300)
        Me.ppd.Enabled = True
        Me.ppd.Icon = CType(resources.GetObject("ppd.Icon"), System.Drawing.Icon)
        Me.ppd.Name = "ppd"
        Me.ppd.Visible = False
        '
        'ppc
        '
        Me.ppc.Location = New System.Drawing.Point(5, 2)
        Me.ppc.Name = "ppc"
        Me.ppc.Size = New System.Drawing.Size(719, 379)
        Me.ppc.TabIndex = 0
        '
        'frmPreviewBarcode
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(726, 387)
        Me.Controls.Add(Me.ppc)
        Me.Name = "frmPreviewBarcode"
        Me.Text = "Preview barcode"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ppd As PrintPreviewDialog

    Friend WithEvents ppc As PrintPreviewControl
    Friend WithEvents pd As System.Drawing.Printing.PrintDocument
End Class
