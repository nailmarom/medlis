﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBarcodeTest
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBarcodeTest))
        Me.txtkode = New System.Windows.Forms.TextBox
        Me.txtlabel = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtSkala = New System.Windows.Forms.TextBox
        Me.labelskala = New System.Windows.Forms.Label
        Me.cbfont = New System.Windows.Forms.ComboBox
        Me.cbkirimargin = New System.Windows.Forms.ComboBox
        Me.cbatasmargin = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.pic = New System.Windows.Forms.PictureBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.cby = New System.Windows.Forms.ComboBox
        Me.cbx = New System.Windows.Forms.ComboBox
        Me.cbbarhight = New System.Windows.Forms.ComboBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.cbsimbol = New System.Windows.Forms.ComboBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.cbjarak = New System.Windows.Forms.ComboBox
        Me.cbjumlah = New System.Windows.Forms.ComboBox
        Me.chkorientasi = New System.Windows.Forms.CheckBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.cbsize = New System.Windows.Forms.ComboBox
        Me.cmdCetak = New System.Windows.Forms.Button
        Me.cmdCreate = New System.Windows.Forms.Button
        Me.ppd = New System.Windows.Forms.PrintPreviewDialog
        Me.cmdSaveBarcode = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.pic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtkode
        '
        Me.txtkode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtkode.Location = New System.Drawing.Point(36, 41)
        Me.txtkode.Name = "txtkode"
        Me.txtkode.Size = New System.Drawing.Size(166, 22)
        Me.txtkode.TabIndex = 0
        Me.txtkode.Text = "12345678"
        '
        'txtlabel
        '
        Me.txtlabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtlabel.Location = New System.Drawing.Point(36, 95)
        Me.txtlabel.Name = "txtlabel"
        Me.txtlabel.Size = New System.Drawing.Size(166, 22)
        Me.txtlabel.TabIndex = 1
        Me.txtlabel.Text = "test1"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(32, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(156, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Kode yang akan di cetak"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(32, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Label Tulisan"
        '
        'txtSkala
        '
        Me.txtSkala.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSkala.Location = New System.Drawing.Point(36, 165)
        Me.txtSkala.Name = "txtSkala"
        Me.txtSkala.Size = New System.Drawing.Size(23, 22)
        Me.txtSkala.TabIndex = 4
        Me.txtSkala.Text = "3"
        '
        'labelskala
        '
        Me.labelskala.AutoSize = True
        Me.labelskala.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelskala.Location = New System.Drawing.Point(32, 142)
        Me.labelskala.Name = "labelskala"
        Me.labelskala.Size = New System.Drawing.Size(43, 16)
        Me.labelskala.TabIndex = 5
        Me.labelskala.Text = "Skala"
        '
        'cbfont
        '
        Me.cbfont.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbfont.FormattingEnabled = True
        Me.cbfont.Location = New System.Drawing.Point(135, 165)
        Me.cbfont.Name = "cbfont"
        Me.cbfont.Size = New System.Drawing.Size(244, 24)
        Me.cbfont.TabIndex = 6
        '
        'cbkirimargin
        '
        Me.cbkirimargin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbkirimargin.FormattingEnabled = True
        Me.cbkirimargin.Items.AddRange(New Object() {"3", "6", "9", "12", "15", "18", "21", "24", "27", "30", "33", "36", "39", "42", "45", "48"})
        Me.cbkirimargin.Location = New System.Drawing.Point(36, 241)
        Me.cbkirimargin.Name = "cbkirimargin"
        Me.cbkirimargin.Size = New System.Drawing.Size(70, 24)
        Me.cbkirimargin.TabIndex = 7
        '
        'cbatasmargin
        '
        Me.cbatasmargin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbatasmargin.FormattingEnabled = True
        Me.cbatasmargin.Items.AddRange(New Object() {"3", "6", "9", "12", "15", "18", "21", "24", "27", "30", "33", "36", "39", "42", "45", "48"})
        Me.cbatasmargin.Location = New System.Drawing.Point(135, 241)
        Me.cbatasmargin.Name = "cbatasmargin"
        Me.cbatasmargin.Size = New System.Drawing.Size(57, 24)
        Me.cbatasmargin.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(32, 218)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 16)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Margin Kiri"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(131, 218)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 16)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Margin atas"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(12, 12)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.pic)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label13)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label12)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label11)
        Me.SplitContainer1.Panel2.Controls.Add(Me.cby)
        Me.SplitContainer1.Panel2.Controls.Add(Me.cbx)
        Me.SplitContainer1.Panel2.Controls.Add(Me.cbbarhight)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label10)
        Me.SplitContainer1.Panel2.Controls.Add(Me.cbsimbol)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label8)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label9)
        Me.SplitContainer1.Panel2.Controls.Add(Me.cbjarak)
        Me.SplitContainer1.Panel2.Controls.Add(Me.cbjumlah)
        Me.SplitContainer1.Panel2.Controls.Add(Me.chkorientasi)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label7)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label6)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label5)
        Me.SplitContainer1.Panel2.Controls.Add(Me.cbsize)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtSkala)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label4)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtkode)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtlabel)
        Me.SplitContainer1.Panel2.Controls.Add(Me.cbatasmargin)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.cbkirimargin)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.cbfont)
        Me.SplitContainer1.Panel2.Controls.Add(Me.labelskala)
        Me.SplitContainer1.Size = New System.Drawing.Size(877, 364)
        Me.SplitContainer1.SplitterDistance = 274
        Me.SplitContainer1.TabIndex = 11
        '
        'pic
        '
        Me.pic.Location = New System.Drawing.Point(3, 3)
        Me.pic.Name = "pic"
        Me.pic.Size = New System.Drawing.Size(268, 358)
        Me.pic.TabIndex = 0
        Me.pic.TabStop = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(442, 82)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(17, 16)
        Me.Label13.TabIndex = 26
        Me.Label13.Text = "Y"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(380, 81)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(16, 16)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "X"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(284, 81)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(46, 16)
        Me.Label11.TabIndex = 24
        Me.Label11.Text = "Tinggi"
        '
        'cby
        '
        Me.cby.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cby.FormattingEnabled = True
        Me.cby.Items.AddRange(New Object() {"20", "25", "30", "35", "40", "45"})
        Me.cby.Location = New System.Drawing.Point(438, 100)
        Me.cby.Name = "cby"
        Me.cby.Size = New System.Drawing.Size(48, 24)
        Me.cby.TabIndex = 23
        '
        'cbx
        '
        Me.cbx.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbx.FormattingEnabled = True
        Me.cbx.Items.AddRange(New Object() {"0", "3", "6", "8", "10", "12", "14", "16", "18", "20", "22", "25"})
        Me.cbx.Location = New System.Drawing.Point(383, 100)
        Me.cbx.Name = "cbx"
        Me.cbx.Size = New System.Drawing.Size(48, 24)
        Me.cbx.TabIndex = 22
        '
        'cbbarhight
        '
        Me.cbbarhight.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbbarhight.FormattingEnabled = True
        Me.cbbarhight.Items.AddRange(New Object() {"40", "50", "60", "70", "80", "90", "100"})
        Me.cbbarhight.Location = New System.Drawing.Point(287, 100)
        Me.cbbarhight.Name = "cbbarhight"
        Me.cbbarhight.Size = New System.Drawing.Size(81, 24)
        Me.cbbarhight.TabIndex = 21
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(283, 16)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(94, 16)
        Me.Label10.TabIndex = 20
        Me.Label10.Text = "Jenis barcode"
        '
        'cbsimbol
        '
        Me.cbsimbol.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbsimbol.FormattingEnabled = True
        Me.cbsimbol.Location = New System.Drawing.Point(286, 39)
        Me.cbsimbol.Name = "cbsimbol"
        Me.cbsimbol.Size = New System.Drawing.Size(231, 24)
        Me.cbsimbol.TabIndex = 19
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(131, 294)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(42, 16)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Jarak"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(32, 294)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(51, 16)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Jumlah"
        '
        'cbjarak
        '
        Me.cbjarak.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbjarak.FormattingEnabled = True
        Me.cbjarak.Items.AddRange(New Object() {"40", "50", "60", "70", "80", "90", "100", "120", "130", "140", "150", "160", "170", "180", "190", "200", "210", "220", "230", "240", "250", "260", "270", "280", "290", "300"})
        Me.cbjarak.Location = New System.Drawing.Point(135, 317)
        Me.cbjarak.Name = "cbjarak"
        Me.cbjarak.Size = New System.Drawing.Size(57, 24)
        Me.cbjarak.TabIndex = 16
        '
        'cbjumlah
        '
        Me.cbjumlah.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbjumlah.FormattingEnabled = True
        Me.cbjumlah.Items.AddRange(New Object() {"3", "6", "9", "12", "15", "18", "21", "24", "27", "30", "33", "36", "39", "42", "45", "48"})
        Me.cbjumlah.Location = New System.Drawing.Point(36, 317)
        Me.cbjumlah.Name = "cbjumlah"
        Me.cbjumlah.Size = New System.Drawing.Size(70, 24)
        Me.cbjumlah.TabIndex = 15
        '
        'chkorientasi
        '
        Me.chkorientasi.AutoSize = True
        Me.chkorientasi.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkorientasi.Location = New System.Drawing.Point(286, 241)
        Me.chkorientasi.Name = "chkorientasi"
        Me.chkorientasi.Size = New System.Drawing.Size(139, 20)
        Me.chkorientasi.TabIndex = 14
        Me.chkorientasi.Text = "Lanscape / Portrait"
        Me.chkorientasi.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(64, 168)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(21, 16)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = ": 1"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(391, 144)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 16)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Ukuran"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(132, 144)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(34, 16)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Font"
        '
        'cbsize
        '
        Me.cbsize.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbsize.FormattingEnabled = True
        Me.cbsize.Items.AddRange(New Object() {"3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14"})
        Me.cbsize.Location = New System.Drawing.Point(394, 165)
        Me.cbsize.Name = "cbsize"
        Me.cbsize.Size = New System.Drawing.Size(92, 24)
        Me.cbsize.TabIndex = 11
        '
        'cmdCetak
        '
        Me.cmdCetak.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdCetak.Location = New System.Drawing.Point(10, 394)
        Me.cmdCetak.Name = "cmdCetak"
        Me.cmdCetak.Size = New System.Drawing.Size(85, 32)
        Me.cmdCetak.TabIndex = 12
        Me.cmdCetak.Text = "Cetak"
        Me.cmdCetak.UseVisualStyleBackColor = True
        '
        'cmdCreate
        '
        Me.cmdCreate.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdCreate.Location = New System.Drawing.Point(333, 394)
        Me.cmdCreate.Name = "cmdCreate"
        Me.cmdCreate.Size = New System.Drawing.Size(85, 32)
        Me.cmdCreate.TabIndex = 13
        Me.cmdCreate.Text = "Create"
        Me.cmdCreate.UseVisualStyleBackColor = True
        '
        'ppd
        '
        Me.ppd.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.ppd.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.ppd.ClientSize = New System.Drawing.Size(400, 300)
        Me.ppd.Enabled = True
        Me.ppd.Icon = CType(resources.GetObject("ppd.Icon"), System.Drawing.Icon)
        Me.ppd.Name = "ppd"
        Me.ppd.Visible = False
        '
        'cmdSaveBarcode
        '
        Me.cmdSaveBarcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSaveBarcode.Location = New System.Drawing.Point(438, 394)
        Me.cmdSaveBarcode.Name = "cmdSaveBarcode"
        Me.cmdSaveBarcode.Size = New System.Drawing.Size(89, 32)
        Me.cmdSaveBarcode.TabIndex = 14
        Me.cmdSaveBarcode.Text = "Simpan"
        Me.cmdSaveBarcode.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(112, 394)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 32)
        Me.Button1.TabIndex = 15
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'PrintDocument1
        '
        '
        'frmBarcodeTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(897, 436)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.cmdSaveBarcode)
        Me.Controls.Add(Me.cmdCreate)
        Me.Controls.Add(Me.cmdCetak)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmBarcodeTest"
        Me.Text = "Test Barcode Printer"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.pic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents txtkode As TextBox
    Friend WithEvents txtlabel As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtSkala As TextBox
    Friend WithEvents labelskala As Label
    Friend WithEvents cbfont As ComboBox
    Friend WithEvents cbkirimargin As ComboBox
    Friend WithEvents cbatasmargin As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents pic As PictureBox
    Friend WithEvents cmdCetak As Button
    Friend WithEvents cmdCreate As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents cbsize As ComboBox
    Friend WithEvents ppd As PrintPreviewDialog
    Friend WithEvents chkorientasi As CheckBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents cbjarak As ComboBox
    Friend WithEvents cbjumlah As ComboBox
    Friend WithEvents cbsimbol As System.Windows.Forms.ComboBox
    Friend WithEvents cmdSaveBarcode As System.Windows.Forms.Button
    Friend WithEvents cbbarhight As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cby As System.Windows.Forms.ComboBox
    Friend WithEvents cbx As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
End Class
