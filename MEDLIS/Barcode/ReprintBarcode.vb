﻿Imports Zen.Barcode
Imports System.Data
Imports System.Data.SqlClient


Imports System.Drawing
Imports System.IO
Imports System.Windows.Forms
Imports System.Drawing.Text

Public Class ReprintBarcode
    '=======================
    Dim bar_type As String
    Dim bar_height As Integer
    Dim bar_res As Single
    Dim bar_y As Integer
    Dim bar_label_size As Integer
    Dim bar_skala As Integer
    '=======================
    Dim originalbarcodeimage As Bitmap
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        'Dim gr As Graphics = Me.CreateGraphics  'cetak di form
        If txtbarcode.Text.Length > 0 And txtlabel.Text.Length > 0 Then
            Dim gr As Graphics = PictureBox1.CreateGraphics 'cetak di picturebox
            gr.Clear(Me.BackColor)
            Cetak(gr)
        Else
            MessageBox.Show("Silahkan isi barcode dan label yang hendak di cetak")
        End If
        

    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim gr As Graphics = e.Graphics

        Cetak(gr)

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If txtbarcode.Text.Length > 0 And txtlabel.Text.Length > 0 Then
            PrintDocument1.Print()
        Else
            MessageBox.Show("Silahkan isi dengan benar")
        End If

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'txttinggi.Text = 60
        'txtsize.Text = 9
        'txtskala.Text = 3
        'txtresolution.Text = 203
        'txtY.Text = 0
        'cbbctype.SelectedText = "code 128"
        Get_Bar_Parameter()
    End Sub

    Private Sub Cetak(ByVal gr As Graphics)

        Dim res As Single

        Dim label As String
        label = txtlabel.Text

        Dim code As String
        code = txtbarcode.Text

        res = CSng(txtresolution.Text)

        If cbbctype.Text = "code 128" Then
            Dim barcode128 As Code128BarcodeDraw = BarcodeDrawFactory.Code128WithChecksum
            originalbarcodeimage = barcode128.Draw(code, CInt(txttinggi.Text), CInt(txtskala.Text))

        ElseIf cbbctype.Text = "code 39" Then
            Dim barcode128 As Code39BarcodeDraw = BarcodeDrawFactory.Code39WithChecksum
            originalbarcodeimage = barcode128.Draw(code, CInt(txttinggi.Text), CInt(txtskala.Text))
        ElseIf cbbctype.Text = "code 39" Then
            Dim barcode128 As Code93BarcodeDraw = BarcodeDrawFactory.Code93WithChecksum
            originalbarcodeimage = barcode128.Draw(code, CInt(txttinggi.Text), CInt(txtskala.Text))
        End If

        originalbarcodeimage.SetResolution(res, res)

        Dim bch As Integer
        bch = originalbarcodeimage.Height


        Dim stringFont As New Font("Arial", CInt(txtsize.Text))

        ' Measure string.
        Dim stringSize As New SizeF
        stringSize = gr.MeasureString(label, stringFont)


        Dim myTopRectangle As New RectangleF(0, CInt(txtY.Text), stringSize.Width, stringSize.Height)
        gr.DrawString(label, stringFont, Brushes.Black, myTopRectangle)

        Dim bcRectangle As New Rectangle(0, myTopRectangle.Bottom, originalbarcodeimage.Width, originalbarcodeimage.Height)
        gr.DrawImage(originalbarcodeimage, bcRectangle)

       
        Dim stringBottomSize As New SizeF
        stringBottomSize = gr.MeasureString(code, stringFont)

        Dim bottomrectangle As New RectangleF(0, bcRectangle.Bottom, stringBottomSize.Width, stringBottomSize.Height)
        gr.DrawString(code, stringFont, Brushes.Black, bottomrectangle)
    End Sub

 
    Private Sub Get_Bar_Parameter()

        Dim strsetting As String
        strsetting = "select * from setting where name='barcode'"
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim cmd As New SqlClient.SqlCommand(strsetting, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("res")) Then ' Or rdr("res") <> "" Then
                    bar_res = rdr("res")
                    txtresolution.Text = bar_res
                Else
                    txtresolution.Text = 203
                End If

                If Not IsDBNull(rdr("height")) Then 'Or rdr("height") <> "" Then
                    bar_height = rdr("height")
                    txttinggi.Text = bar_height
                Else
                    txttinggi.Text = 60
                End If


                If Not IsDBNull(rdr("value")) Then ' Or rdr("value") <> "" Then
                    bar_type = rdr("value")
                    cbbctype.SelectedIndex = cbbctype.FindString(bar_type)
                Else
                    cbbctype.SelectedIndex = cbbctype.FindString("code 128")
                End If

                If Not IsDBNull(rdr("scala")) Then 'Or rdr("scala") <> "" Then
                    bar_skala = rdr("scala")
                    txtskala.Text = bar_skala
                Else
                    txtskala.Text = 3
                End If

                If Not IsDBNull(rdr("y")) Then 'Or rdr("y") <> "" Then
                    bar_y = rdr("y")
                    txtY.Text = bar_y
                Else
                    txtY.Text = 0
                End If

                If Not IsDBNull(rdr("ftsize")) Then 'Or rdr("ftsize") <> "" Then
                    bar_label_size = rdr("ftsize")
                    txtsize.Text = bar_label_size
                Else
                    txtsize.Text = 9
                End If




            Loop
        Else
            txttinggi.Text = 60
            txtsize.Text = 9
            txtskala.Text = 3
            txtresolution.Text = 203
            txtY.Text = 0
            cbbctype.SelectedIndex = cbbctype.FindString("code 128")
        End If
        cmd.Dispose()
        ogre.CloseConn()

    End Sub
    ReadOnly AllowedKeys As String = _
   "0123456789"
    Private Sub txtresolution_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtresolution.KeyPress, txtsize.KeyPress, txttinggi.KeyPress, txtY.KeyPress, txtskala.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)

        End Select
    End Sub
    ReadOnly AllowedKeysbarcode As String = _
   "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    ReadOnly AllowedKeysLabel As String = _
  "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 "
    Private Sub txtbarcode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtbarcode.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeysbarcode.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeysbarcode.Contains(e.KeyChar)

        End Select
    End Sub

    Private Sub txtlabel_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtlabel.KeyPress

        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeysLabel.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeysLabel.Contains(e.KeyChar)

        End Select
    End Sub
End Class
