﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ChartFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series
        Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea
        Dim Legend2 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series
        Dim ChartArea3 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea
        Dim Legend3 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend
        Dim Series3 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ChartFrm))
        Me.wbcchart = New System.Windows.Forms.DataVisualization.Charting.Chart
        Me.rbcchart = New System.Windows.Forms.DataVisualization.Charting.Chart
        Me.pltchart = New System.Windows.Forms.DataVisualization.Charting.Chart
        Me.cmdPrintHema = New System.Windows.Forms.Button
        CType(Me.wbcchart, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rbcchart, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pltchart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'wbcchart
        '
        ChartArea1.Name = "ChartArea1"
        Me.wbcchart.ChartAreas.Add(ChartArea1)
        Legend1.Name = "Legend1"
        Me.wbcchart.Legends.Add(Legend1)
        Me.wbcchart.Location = New System.Drawing.Point(12, 12)
        Me.wbcchart.Name = "wbcchart"
        Series1.ChartArea = "ChartArea1"
        Series1.Legend = "Legend1"
        Series1.Name = "Series1"
        Me.wbcchart.Series.Add(Series1)
        Me.wbcchart.Size = New System.Drawing.Size(433, 177)
        Me.wbcchart.TabIndex = 0
        '
        'rbcchart
        '
        ChartArea2.Name = "ChartArea1"
        Me.rbcchart.ChartAreas.Add(ChartArea2)
        Legend2.Name = "Legend1"
        Me.rbcchart.Legends.Add(Legend2)
        Me.rbcchart.Location = New System.Drawing.Point(12, 203)
        Me.rbcchart.Name = "rbcchart"
        Series2.ChartArea = "ChartArea1"
        Series2.Legend = "Legend1"
        Series2.Name = "Series1"
        Me.rbcchart.Series.Add(Series2)
        Me.rbcchart.Size = New System.Drawing.Size(433, 177)
        Me.rbcchart.TabIndex = 1
        '
        'pltchart
        '
        ChartArea3.Name = "ChartArea1"
        Me.pltchart.ChartAreas.Add(ChartArea3)
        Legend3.Name = "Legend1"
        Me.pltchart.Legends.Add(Legend3)
        Me.pltchart.Location = New System.Drawing.Point(12, 393)
        Me.pltchart.Name = "pltchart"
        Series3.ChartArea = "ChartArea1"
        Series3.Legend = "Legend1"
        Series3.Name = "Series1"
        Me.pltchart.Series.Add(Series3)
        Me.pltchart.Size = New System.Drawing.Size(433, 177)
        Me.pltchart.TabIndex = 2
        '
        'cmdPrintHema
        '
        Me.cmdPrintHema.Location = New System.Drawing.Point(482, 12)
        Me.cmdPrintHema.Name = "cmdPrintHema"
        Me.cmdPrintHema.Size = New System.Drawing.Size(86, 67)
        Me.cmdPrintHema.TabIndex = 3
        Me.cmdPrintHema.Text = "Cetak"
        Me.cmdPrintHema.UseVisualStyleBackColor = True
        '
        'ChartFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(596, 581)
        Me.Controls.Add(Me.cmdPrintHema)
        Me.Controls.Add(Me.pltchart)
        Me.Controls.Add(Me.rbcchart)
        Me.Controls.Add(Me.wbcchart)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ChartFrm"
        Me.Text = "Chart Hematology"
        CType(Me.wbcchart, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rbcchart, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pltchart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents wbcchart As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents rbcchart As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents pltchart As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents cmdPrintHema As System.Windows.Forms.Button
End Class
