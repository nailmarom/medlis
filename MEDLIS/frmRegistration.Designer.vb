﻿'
' Created by SharpDevelop.
' User: User
' Date: 10/13/2014
' Time: 1:06 PM
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Partial Class frmRegistration
	Inherits System.Windows.Forms.Form
	
	''' <summary>
	''' Designer variable used to keep track of non-visual components.
	''' </summary>
	Private components As System.ComponentModel.IContainer
	
	''' <summary>
	''' Disposes resources used by the form.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If components IsNot Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub
	
	''' <summary>
	''' This method is required for Windows Forms designer support.
	''' Do not change the method contents inside the source code editor. The Forms designer might
	''' not be able to load this method if it was changed manually.
	''' </summary>
	Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRegistration))
        Me.label6 = New System.Windows.Forms.Label()
        Me.dtTglLahir = New System.Windows.Forms.DateTimePicker()
        Me.rdoPerempuan = New System.Windows.Forms.RadioButton()
        Me.rdoLaki = New System.Windows.Forms.RadioButton()
        Me.label4 = New System.Windows.Forms.Label()
        Me.label3 = New System.Windows.Forms.Label()
        Me.label5 = New System.Windows.Forms.Label()
        Me.txtIdPasien = New System.Windows.Forms.TextBox()
        Me.txtAlamat = New System.Windows.Forms.TextBox()
        Me.label7 = New System.Windows.Forms.Label()
        Me.txtNama = New System.Windows.Forms.TextBox()
        Me.cbTestGroup = New System.Windows.Forms.ComboBox()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.SaveMnu = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.cancelMnu = New System.Windows.Forms.ToolStripButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDocter = New System.Windows.Forms.TextBox()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtdocteraddress = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dgvtestname = New System.Windows.Forms.DataGridView()
        Me.dgvSelTestName = New System.Windows.Forms.DataGridView()
        Me.txtlabnumber = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtyellow = New System.Windows.Forms.TextBox()
        Me.txtpackprice = New System.Windows.Forms.TextBox()
        Me.cmdcancelall = New System.Windows.Forms.Button()
        Me.txtpackdesc = New System.Windows.Forms.TextBox()
        Me.cmdaddprofile = New System.Windows.Forms.Button()
        Me.dgpack = New System.Windows.Forms.DataGridView()
        Me.rdopack = New System.Windows.Forms.RadioButton()
        Me.rdogroup = New System.Windows.Forms.RadioButton()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cmbpack = New System.Windows.Forms.ComboBox()
        Me.btnsampleAdd = New System.Windows.Forms.Button()
        Me.rdS = New System.Windows.Forms.RadioButton()
        Me.rdR = New System.Windows.Forms.RadioButton()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.rdoanak = New System.Windows.Forms.RadioButton()
        Me.cbtipepasien = New System.Windows.Forms.ComboBox()
        Me.lbltypepasien = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.btnprevlab = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.pnlMain = New System.Windows.Forms.Panel()
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.testname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.analyzertestname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.price = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pricenoformat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chk = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.idtestpack = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.dgvtestname, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSelTestName, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgpack, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'label6
        '
        Me.label6.AutoSize = True
        Me.label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label6.Location = New System.Drawing.Point(314, 20)
        Me.label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(96, 15)
        Me.label6.TabIndex = 28
        Me.label6.Text = "Tanggal Lahir"
        '
        'dtTglLahir
        '
        Me.dtTglLahir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtTglLahir.Location = New System.Drawing.Point(413, 19)
        Me.dtTglLahir.Margin = New System.Windows.Forms.Padding(4)
        Me.dtTglLahir.Name = "dtTglLahir"
        Me.dtTglLahir.Size = New System.Drawing.Size(232, 21)
        Me.dtTglLahir.TabIndex = 27
        '
        'rdoPerempuan
        '
        Me.rdoPerempuan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdoPerempuan.Location = New System.Drawing.Point(514, 110)
        Me.rdoPerempuan.Margin = New System.Windows.Forms.Padding(4)
        Me.rdoPerempuan.Name = "rdoPerempuan"
        Me.rdoPerempuan.Size = New System.Drawing.Size(100, 22)
        Me.rdoPerempuan.TabIndex = 26
        Me.rdoPerempuan.TabStop = True
        Me.rdoPerempuan.Text = "Perempuan"
        Me.rdoPerempuan.UseVisualStyleBackColor = True
        '
        'rdoLaki
        '
        Me.rdoLaki.AutoSize = True
        Me.rdoLaki.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdoLaki.Location = New System.Drawing.Point(418, 111)
        Me.rdoLaki.Margin = New System.Windows.Forms.Padding(4)
        Me.rdoLaki.Name = "rdoLaki"
        Me.rdoLaki.Size = New System.Drawing.Size(83, 19)
        Me.rdoLaki.TabIndex = 25
        Me.rdoLaki.TabStop = True
        Me.rdoLaki.Text = "Laki Laki"
        Me.rdoLaki.UseVisualStyleBackColor = True
        '
        'label4
        '
        Me.label4.AutoSize = True
        Me.label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label4.Location = New System.Drawing.Point(362, 83)
        Me.label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(48, 15)
        Me.label4.TabIndex = 24
        Me.label4.Text = "Phone"
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label3.Location = New System.Drawing.Point(359, 52)
        Me.label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(51, 15)
        Me.label3.TabIndex = 22
        Me.label3.Text = "Alamat"
        '
        'label5
        '
        Me.label5.AutoSize = True
        Me.label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label5.Location = New System.Drawing.Point(45, 58)
        Me.label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(45, 15)
        Me.label5.TabIndex = 21
        Me.label5.Text = "Nama"
        '
        'txtIdPasien
        '
        Me.txtIdPasien.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIdPasien.Location = New System.Drawing.Point(95, 26)
        Me.txtIdPasien.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIdPasien.Name = "txtIdPasien"
        Me.txtIdPasien.Size = New System.Drawing.Size(161, 21)
        Me.txtIdPasien.TabIndex = 20
        '
        'txtAlamat
        '
        Me.txtAlamat.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAlamat.Location = New System.Drawing.Point(413, 49)
        Me.txtAlamat.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAlamat.Name = "txtAlamat"
        Me.txtAlamat.Size = New System.Drawing.Size(323, 21)
        Me.txtAlamat.TabIndex = 19
        '
        'label7
        '
        Me.label7.AutoSize = True
        Me.label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label7.Location = New System.Drawing.Point(16, 28)
        Me.label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(74, 15)
        Me.label7.TabIndex = 18
        Me.label7.Text = "ID PASIEN"
        '
        'txtNama
        '
        Me.txtNama.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNama.Location = New System.Drawing.Point(95, 56)
        Me.txtNama.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNama.Name = "txtNama"
        Me.txtNama.Size = New System.Drawing.Size(232, 21)
        Me.txtNama.TabIndex = 17
        '
        'cbTestGroup
        '
        Me.cbTestGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbTestGroup.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTestGroup.FormattingEnabled = True
        Me.cbTestGroup.Location = New System.Drawing.Point(98, 4)
        Me.cbTestGroup.Margin = New System.Windows.Forms.Padding(4)
        Me.cbTestGroup.Name = "cbTestGroup"
        Me.cbTestGroup.Size = New System.Drawing.Size(283, 23)
        Me.cbTestGroup.TabIndex = 6
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SaveMnu, Me.ToolStripSeparator6, Me.cancelMnu})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Padding = New System.Windows.Forms.Padding(10, 0, 1, 0)
        Me.ToolStrip1.Size = New System.Drawing.Size(1127, 46)
        Me.ToolStrip1.TabIndex = 32
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'SaveMnu
        '
        Me.SaveMnu.Image = Global.MEDLIS.My.Resources.Resources.save
        Me.SaveMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SaveMnu.Name = "SaveMnu"
        Me.SaveMnu.Size = New System.Drawing.Size(67, 43)
        Me.SaveMnu.Text = "Save"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 46)
        '
        'cancelMnu
        '
        Me.cancelMnu.AutoSize = False
        Me.cancelMnu.Image = Global.MEDLIS.My.Resources.Resources.cancel
        Me.cancelMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cancelMnu.Name = "cancelMnu"
        Me.cancelMnu.Size = New System.Drawing.Size(85, 39)
        Me.cancelMnu.Text = "Cancel"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(808, 40)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 15)
        Me.Label1.TabIndex = 33
        Me.Label1.Text = "Dokter"
        '
        'txtDocter
        '
        Me.txtDocter.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDocter.Location = New System.Drawing.Point(862, 36)
        Me.txtDocter.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDocter.Name = "txtDocter"
        Me.txtDocter.Size = New System.Drawing.Size(200, 21)
        Me.txtDocter.TabIndex = 2
        '
        'txtTotal
        '
        Me.txtTotal.BackColor = System.Drawing.Color.PaleGoldenrod
        Me.txtTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.Location = New System.Drawing.Point(349, 382)
        Me.txtTotal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(203, 27)
        Me.txtTotal.TabIndex = 35
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(305, 386)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 15)
        Me.Label2.TabIndex = 36
        Me.Label2.Text = "Total"
        '
        'txtdocteraddress
        '
        Me.txtdocteraddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtdocteraddress.Location = New System.Drawing.Point(862, 70)
        Me.txtdocteraddress.Margin = New System.Windows.Forms.Padding(4)
        Me.txtdocteraddress.Name = "txtdocteraddress"
        Me.txtdocteraddress.Size = New System.Drawing.Size(200, 21)
        Me.txtdocteraddress.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(760, 74)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(97, 15)
        Me.Label8.TabIndex = 38
        Me.Label8.Text = "Alamat Dokter"
        '
        'dgvtestname
        '
        Me.dgvtestname.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvtestname.Location = New System.Drawing.Point(6, 71)
        Me.dgvtestname.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvtestname.Name = "dgvtestname"
        Me.dgvtestname.Size = New System.Drawing.Size(546, 255)
        Me.dgvtestname.TabIndex = 7
        '
        'dgvSelTestName
        '
        Me.dgvSelTestName.AllowUserToAddRows = False
        Me.dgvSelTestName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSelTestName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id, Me.testname, Me.analyzertestname, Me.price, Me.pricenoformat, Me.chk, Me.idtestpack})
        Me.dgvSelTestName.Location = New System.Drawing.Point(560, 7)
        Me.dgvSelTestName.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvSelTestName.Name = "dgvSelTestName"
        Me.dgvSelTestName.Size = New System.Drawing.Size(530, 266)
        Me.dgvSelTestName.TabIndex = 8
        '
        'txtlabnumber
        '
        Me.txtlabnumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtlabnumber.Location = New System.Drawing.Point(862, 4)
        Me.txtlabnumber.Margin = New System.Windows.Forms.Padding(4)
        Me.txtlabnumber.Name = "txtlabnumber"
        Me.txtlabnumber.Size = New System.Drawing.Size(128, 21)
        Me.txtlabnumber.TabIndex = 0
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(771, 8)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(86, 15)
        Me.Label11.TabIndex = 46
        Me.Label11.Text = "Lab Number"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.pnlMain)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 191)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(1127, 490)
        Me.GroupBox1.TabIndex = 49
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Pilih Nama Test"
        '
        'txtyellow
        '
        Me.txtyellow.BackColor = System.Drawing.SystemColors.Info
        Me.txtyellow.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtyellow.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtyellow.ForeColor = System.Drawing.Color.Blue
        Me.txtyellow.Location = New System.Drawing.Point(7, 366)
        Me.txtyellow.Multiline = True
        Me.txtyellow.Name = "txtyellow"
        Me.txtyellow.Size = New System.Drawing.Size(246, 86)
        Me.txtyellow.TabIndex = 47
        '
        'txtpackprice
        '
        Me.txtpackprice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtpackprice.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtpackprice.Location = New System.Drawing.Point(330, 336)
        Me.txtpackprice.Name = "txtpackprice"
        Me.txtpackprice.Size = New System.Drawing.Size(137, 24)
        Me.txtpackprice.TabIndex = 46
        '
        'cmdcancelall
        '
        Me.cmdcancelall.Location = New System.Drawing.Point(560, 423)
        Me.cmdcancelall.Name = "cmdcancelall"
        Me.cmdcancelall.Size = New System.Drawing.Size(104, 34)
        Me.cmdcancelall.TabIndex = 45
        Me.cmdcancelall.Text = "Cancel All"
        Me.cmdcancelall.UseVisualStyleBackColor = True
        '
        'txtpackdesc
        '
        Me.txtpackdesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtpackdesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtpackdesc.Location = New System.Drawing.Point(5, 336)
        Me.txtpackdesc.Name = "txtpackdesc"
        Me.txtpackdesc.Size = New System.Drawing.Size(319, 24)
        Me.txtpackdesc.TabIndex = 44
        '
        'cmdaddprofile
        '
        Me.cmdaddprofile.Location = New System.Drawing.Point(473, 336)
        Me.cmdaddprofile.Name = "cmdaddprofile"
        Me.cmdaddprofile.Size = New System.Drawing.Size(79, 24)
        Me.cmdaddprofile.TabIndex = 43
        Me.cmdaddprofile.Text = "Add All"
        Me.cmdaddprofile.UseVisualStyleBackColor = True
        '
        'dgpack
        '
        Me.dgpack.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgpack.Location = New System.Drawing.Point(559, 280)
        Me.dgpack.Name = "dgpack"
        Me.dgpack.Size = New System.Drawing.Size(526, 136)
        Me.dgpack.TabIndex = 42
        '
        'rdopack
        '
        Me.rdopack.AutoSize = True
        Me.rdopack.Location = New System.Drawing.Point(409, 38)
        Me.rdopack.Name = "rdopack"
        Me.rdopack.Size = New System.Drawing.Size(98, 19)
        Me.rdopack.TabIndex = 41
        Me.rdopack.TabStop = True
        Me.rdopack.Text = "Test Profile"
        Me.rdopack.UseVisualStyleBackColor = True
        '
        'rdogroup
        '
        Me.rdogroup.AutoSize = True
        Me.rdogroup.Location = New System.Drawing.Point(409, 6)
        Me.rdogroup.Name = "rdogroup"
        Me.rdogroup.Size = New System.Drawing.Size(95, 19)
        Me.rdogroup.TabIndex = 40
        Me.rdogroup.TabStop = True
        Me.rdogroup.Text = "Test Group"
        Me.rdogroup.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(10, 37)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 15)
        Me.Label13.TabIndex = 39
        Me.Label13.Text = "Test Profile"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(11, 7)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(77, 15)
        Me.Label12.TabIndex = 38
        Me.Label12.Text = "Test Group"
        '
        'cmbpack
        '
        Me.cmbpack.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbpack.FormattingEnabled = True
        Me.cmbpack.Location = New System.Drawing.Point(98, 34)
        Me.cmbpack.Name = "cmbpack"
        Me.cmbpack.Size = New System.Drawing.Size(283, 23)
        Me.cmbpack.TabIndex = 37
        '
        'btnsampleAdd
        '
        Me.btnsampleAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsampleAdd.Location = New System.Drawing.Point(376, 422)
        Me.btnsampleAdd.Margin = New System.Windows.Forms.Padding(4)
        Me.btnsampleAdd.Name = "btnsampleAdd"
        Me.btnsampleAdd.Size = New System.Drawing.Size(177, 35)
        Me.btnsampleAdd.TabIndex = 9
        Me.btnsampleAdd.Text = "Add Sample"
        Me.btnsampleAdd.UseVisualStyleBackColor = True
        '
        'rdS
        '
        Me.rdS.AutoSize = True
        Me.rdS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdS.Location = New System.Drawing.Point(983, 101)
        Me.rdS.Margin = New System.Windows.Forms.Padding(4)
        Me.rdS.Name = "rdS"
        Me.rdS.Size = New System.Drawing.Size(34, 19)
        Me.rdS.TabIndex = 5
        Me.rdS.TabStop = True
        Me.rdS.Text = "S"
        Me.rdS.UseVisualStyleBackColor = True
        '
        'rdR
        '
        Me.rdR.AutoSize = True
        Me.rdR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdR.Location = New System.Drawing.Point(927, 101)
        Me.rdR.Margin = New System.Windows.Forms.Padding(4)
        Me.rdR.Name = "rdR"
        Me.rdR.Size = New System.Drawing.Size(35, 19)
        Me.rdR.TabIndex = 4
        Me.rdR.TabStop = True
        Me.rdR.Text = "R"
        Me.rdR.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.rdoanak)
        Me.GroupBox3.Controls.Add(Me.cbtipepasien)
        Me.GroupBox3.Controls.Add(Me.lbltypepasien)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.txtPhone)
        Me.GroupBox3.Controls.Add(Me.dtTglLahir)
        Me.GroupBox3.Controls.Add(Me.label6)
        Me.GroupBox3.Controls.Add(Me.rdoPerempuan)
        Me.GroupBox3.Controls.Add(Me.txtAlamat)
        Me.GroupBox3.Controls.Add(Me.label3)
        Me.GroupBox3.Controls.Add(Me.rdoLaki)
        Me.GroupBox3.Controls.Add(Me.label4)
        Me.GroupBox3.Controls.Add(Me.label5)
        Me.GroupBox3.Controls.Add(Me.txtIdPasien)
        Me.GroupBox3.Controls.Add(Me.label7)
        Me.GroupBox3.Controls.Add(Me.txtNama)
        Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Left
        Me.GroupBox3.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Size = New System.Drawing.Size(751, 145)
        Me.GroupBox3.TabIndex = 52
        Me.GroupBox3.TabStop = False
        '
        'rdoanak
        '
        Me.rdoanak.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdoanak.Location = New System.Drawing.Point(621, 110)
        Me.rdoanak.Margin = New System.Windows.Forms.Padding(4)
        Me.rdoanak.Name = "rdoanak"
        Me.rdoanak.Size = New System.Drawing.Size(100, 22)
        Me.rdoanak.TabIndex = 33
        Me.rdoanak.TabStop = True
        Me.rdoanak.Text = "Anak anak"
        Me.rdoanak.UseVisualStyleBackColor = True
        '
        'cbtipepasien
        '
        Me.cbtipepasien.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbtipepasien.FormattingEnabled = True
        Me.cbtipepasien.Location = New System.Drawing.Point(95, 112)
        Me.cbtipepasien.Name = "cbtipepasien"
        Me.cbtipepasien.Size = New System.Drawing.Size(232, 23)
        Me.cbtipepasien.TabIndex = 32
        '
        'lbltypepasien
        '
        Me.lbltypepasien.AutoSize = True
        Me.lbltypepasien.Location = New System.Drawing.Point(95, 89)
        Me.lbltypepasien.Name = "lbltypepasien"
        Me.lbltypepasien.Size = New System.Drawing.Size(19, 15)
        Me.lbltypepasien.TabIndex = 31
        Me.lbltypepasien.Text = "..."
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(6, 89)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(85, 15)
        Me.Label10.TabIndex = 30
        Me.Label10.Text = "Type Pasien"
        '
        'txtPhone
        '
        Me.txtPhone.Location = New System.Drawing.Point(414, 81)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(157, 21)
        Me.txtPhone.TabIndex = 29
        '
        'btnprevlab
        '
        Me.btnprevlab.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnprevlab.Location = New System.Drawing.Point(997, 3)
        Me.btnprevlab.Name = "btnprevlab"
        Me.btnprevlab.Size = New System.Drawing.Size(108, 23)
        Me.btnprevlab.TabIndex = 1
        Me.btnprevlab.Text = "sebelumnya"
        Me.btnprevlab.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(865, 102)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(61, 15)
        Me.Label9.TabIndex = 52
        Me.Label9.Text = "Prioritas"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.GroupBox3)
        Me.Panel1.Controls.Add(Me.btnprevlab)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.txtDocter)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.rdS)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.rdR)
        Me.Panel1.Controls.Add(Me.txtdocteraddress)
        Me.Panel1.Controls.Add(Me.txtlabnumber)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 46)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1127, 145)
        Me.Panel1.TabIndex = 53
        '
        'pnlMain
        '
        Me.pnlMain.AutoScroll = True
        Me.pnlMain.Controls.Add(Me.cmdcancelall)
        Me.pnlMain.Controls.Add(Me.txtyellow)
        Me.pnlMain.Controls.Add(Me.dgpack)
        Me.pnlMain.Controls.Add(Me.cbTestGroup)
        Me.pnlMain.Controls.Add(Me.dgvSelTestName)
        Me.pnlMain.Controls.Add(Me.txtpackprice)
        Me.pnlMain.Controls.Add(Me.txtTotal)
        Me.pnlMain.Controls.Add(Me.Label2)
        Me.pnlMain.Controls.Add(Me.txtpackdesc)
        Me.pnlMain.Controls.Add(Me.dgvtestname)
        Me.pnlMain.Controls.Add(Me.cmdaddprofile)
        Me.pnlMain.Controls.Add(Me.btnsampleAdd)
        Me.pnlMain.Controls.Add(Me.cmbpack)
        Me.pnlMain.Controls.Add(Me.rdopack)
        Me.pnlMain.Controls.Add(Me.Label12)
        Me.pnlMain.Controls.Add(Me.rdogroup)
        Me.pnlMain.Controls.Add(Me.Label13)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(4, 18)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(1119, 468)
        Me.pnlMain.TabIndex = 54
        '
        'id
        '
        Me.id.HeaderText = "id"
        Me.id.Name = "id"
        Me.id.Visible = False
        '
        'testname
        '
        Me.testname.HeaderText = "Test Name"
        Me.testname.Name = "testname"
        Me.testname.ReadOnly = True
        Me.testname.Width = 150
        '
        'analyzertestname
        '
        Me.analyzertestname.HeaderText = "Analyzer Test"
        Me.analyzertestname.Name = "analyzertestname"
        Me.analyzertestname.ReadOnly = True
        Me.analyzertestname.Width = 150
        '
        'price
        '
        Me.price.HeaderText = "Harga"
        Me.price.Name = "price"
        Me.price.ReadOnly = True
        '
        'pricenoformat
        '
        Me.pricenoformat.HeaderText = "harga cetak"
        Me.pricenoformat.Name = "pricenoformat"
        Me.pricenoformat.Visible = False
        '
        'chk
        '
        Me.chk.HeaderText = "Pilih"
        Me.chk.Name = "chk"
        Me.chk.Width = 50
        '
        'idtestpack
        '
        Me.idtestpack.HeaderText = "idtestpack"
        Me.idtestpack.Name = "idtestpack"
        Me.idtestpack.ReadOnly = True
        Me.idtestpack.Visible = False
        '
        'frmRegistration
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(1127, 681)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmRegistration"
        Me.Text = "REGISTRATION"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.dgvtestname, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSelTestName, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgpack, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents cbTestGroup As System.Windows.Forms.ComboBox
    Friend WithEvents txtNama As System.Windows.Forms.TextBox
    Private label7 As System.Windows.Forms.Label
    Friend WithEvents txtAlamat As System.Windows.Forms.TextBox
    Private label5 As System.Windows.Forms.Label
    Private label3 As System.Windows.Forms.Label
    Private label4 As System.Windows.Forms.Label
    Friend WithEvents rdoLaki As System.Windows.Forms.RadioButton
    Friend WithEvents rdoPerempuan As System.Windows.Forms.RadioButton
    Private WithEvents dtTglLahir As System.Windows.Forms.DateTimePicker
    Private label6 As System.Windows.Forms.Label
    Friend WithEvents txtIdPasien As System.Windows.Forms.TextBox
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents SaveMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cancelMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDocter As System.Windows.Forms.TextBox
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtdocteraddress As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents dgvtestname As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSelTestName As System.Windows.Forms.DataGridView
    Friend WithEvents txtlabnumber As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnsampleAdd As System.Windows.Forms.Button
    Friend WithEvents rdS As System.Windows.Forms.RadioButton
    Friend WithEvents rdR As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btnprevlab As System.Windows.Forms.Button
    Friend WithEvents txtPhone As System.Windows.Forms.TextBox
    Friend WithEvents lbltypepasien As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cbtipepasien As System.Windows.Forms.ComboBox
    Friend WithEvents rdoanak As System.Windows.Forms.RadioButton
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cmbpack As System.Windows.Forms.ComboBox
    Friend WithEvents rdopack As System.Windows.Forms.RadioButton
    Friend WithEvents rdogroup As System.Windows.Forms.RadioButton
    Friend WithEvents dgpack As System.Windows.Forms.DataGridView
    Friend WithEvents cmdaddprofile As System.Windows.Forms.Button
    Friend WithEvents txtpackdesc As System.Windows.Forms.TextBox
    Friend WithEvents cmdcancelall As System.Windows.Forms.Button
    Friend WithEvents txtpackprice As System.Windows.Forms.TextBox
    Friend WithEvents txtyellow As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents pnlMain As Panel
    Friend WithEvents id As DataGridViewTextBoxColumn
    Friend WithEvents testname As DataGridViewTextBoxColumn
    Friend WithEvents analyzertestname As DataGridViewTextBoxColumn
    Friend WithEvents price As DataGridViewTextBoxColumn
    Friend WithEvents pricenoformat As DataGridViewTextBoxColumn
    Friend WithEvents chk As DataGridViewCheckBoxColumn
    Friend WithEvents idtestpack As DataGridViewTextBoxColumn
End Class
