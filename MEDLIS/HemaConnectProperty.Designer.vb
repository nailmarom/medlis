﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HemaConnectProperty
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(HemaConnectProperty))
        Me.Label3 = New System.Windows.Forms.Label
        Me.cbDataBit = New System.Windows.Forms.ComboBox
        Me.cbStopBit = New System.Windows.Forms.ComboBox
        Me.cbParity = New System.Windows.Forms.ComboBox
        Me.cbBaudRate = New System.Windows.Forms.ComboBox
        Me.cbPort = New System.Windows.Forms.ComboBox
        Me.cbInstrument = New System.Windows.Forms.ComboBox
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.AddMnu = New System.Windows.Forms.ToolStripButton
        Me.SaveMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.EditMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.cancelMnu = New System.Windows.Forms.ToolStripButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.lbltest = New System.Windows.Forms.Label
        Me.btntest = New System.Windows.Forms.Button
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.tmrreceive = New System.Windows.Forms.Timer(Me.components)
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(148, 67)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 15)
        Me.Label3.TabIndex = 54
        Me.Label3.Text = "Port"
        '
        'cbDataBit
        '
        Me.cbDataBit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbDataBit.FormattingEnabled = True
        Me.cbDataBit.Location = New System.Drawing.Point(221, 173)
        Me.cbDataBit.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cbDataBit.Name = "cbDataBit"
        Me.cbDataBit.Size = New System.Drawing.Size(140, 23)
        Me.cbDataBit.TabIndex = 53
        '
        'cbStopBit
        '
        Me.cbStopBit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbStopBit.FormattingEnabled = True
        Me.cbStopBit.Location = New System.Drawing.Point(221, 145)
        Me.cbStopBit.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cbStopBit.Name = "cbStopBit"
        Me.cbStopBit.Size = New System.Drawing.Size(140, 23)
        Me.cbStopBit.TabIndex = 52
        '
        'cbParity
        '
        Me.cbParity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbParity.FormattingEnabled = True
        Me.cbParity.Location = New System.Drawing.Point(221, 118)
        Me.cbParity.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cbParity.Name = "cbParity"
        Me.cbParity.Size = New System.Drawing.Size(140, 23)
        Me.cbParity.TabIndex = 51
        '
        'cbBaudRate
        '
        Me.cbBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbBaudRate.FormattingEnabled = True
        Me.cbBaudRate.Location = New System.Drawing.Point(221, 90)
        Me.cbBaudRate.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cbBaudRate.Name = "cbBaudRate"
        Me.cbBaudRate.Size = New System.Drawing.Size(140, 23)
        Me.cbBaudRate.TabIndex = 50
        '
        'cbPort
        '
        Me.cbPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbPort.FormattingEnabled = True
        Me.cbPort.Location = New System.Drawing.Point(221, 62)
        Me.cbPort.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cbPort.Name = "cbPort"
        Me.cbPort.Size = New System.Drawing.Size(140, 23)
        Me.cbPort.TabIndex = 49
        '
        'cbInstrument
        '
        Me.cbInstrument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbInstrument.FormattingEnabled = True
        Me.cbInstrument.Location = New System.Drawing.Point(122, 15)
        Me.cbInstrument.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cbInstrument.Name = "cbInstrument"
        Me.cbInstrument.Size = New System.Drawing.Size(240, 23)
        Me.cbInstrument.TabIndex = 55
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddMnu, Me.SaveMnu, Me.ToolStripSeparator6, Me.EditMnu, Me.ToolStripSeparator7, Me.cancelMnu})
        Me.ToolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(411, 46)
        Me.ToolStrip1.TabIndex = 56
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'AddMnu
        '
        Me.AddMnu.Image = Global.MEDLIS.My.Resources.Resources.plus
        Me.AddMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.AddMnu.Name = "AddMnu"
        Me.AddMnu.Size = New System.Drawing.Size(65, 43)
        Me.AddMnu.Text = "Add"
        '
        'SaveMnu
        '
        Me.SaveMnu.Image = Global.MEDLIS.My.Resources.Resources.save
        Me.SaveMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SaveMnu.Name = "SaveMnu"
        Me.SaveMnu.Size = New System.Drawing.Size(67, 43)
        Me.SaveMnu.Text = "Save"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 46)
        '
        'EditMnu
        '
        Me.EditMnu.Image = Global.MEDLIS.My.Resources.Resources.write_edit
        Me.EditMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.EditMnu.Name = "EditMnu"
        Me.EditMnu.Size = New System.Drawing.Size(63, 43)
        Me.EditMnu.Text = "Edit"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(6, 46)
        '
        'cancelMnu
        '
        Me.cancelMnu.AutoSize = False
        Me.cancelMnu.Image = Global.MEDLIS.My.Resources.Resources.cancel
        Me.cancelMnu.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cancelMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cancelMnu.Name = "cancelMnu"
        Me.cancelMnu.Size = New System.Drawing.Size(85, 39)
        Me.cancelMnu.Text = "Cancel"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 48)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox1.Size = New System.Drawing.Size(394, 257)
        Me.GroupBox1.TabIndex = 57
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Instrument"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.cbInstrument)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.cbDataBit)
        Me.Panel1.Controls.Add(Me.cbStopBit)
        Me.Panel1.Controls.Add(Me.cbParity)
        Me.Panel1.Controls.Add(Me.cbBaudRate)
        Me.Panel1.Controls.Add(Me.cbPort)
        Me.Panel1.Location = New System.Drawing.Point(12, 22)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(372, 216)
        Me.Panel1.TabIndex = 63
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(2, 19)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(101, 15)
        Me.Label6.TabIndex = 60
        Me.Label6.Text = "Intrument Name"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(148, 180)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 15)
        Me.Label5.TabIndex = 59
        Me.Label5.Text = "Data Bit"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(148, 150)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 15)
        Me.Label4.TabIndex = 58
        Me.Label4.Text = "Stop Bit"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(148, 126)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 15)
        Me.Label2.TabIndex = 57
        Me.Label2.Text = "Parity"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(148, 96)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 15)
        Me.Label1.TabIndex = 56
        Me.Label1.Text = "Baud Rate"
        '
        'lbltest
        '
        Me.lbltest.AutoSize = True
        Me.lbltest.Location = New System.Drawing.Point(127, 376)
        Me.lbltest.Name = "lbltest"
        Me.lbltest.Size = New System.Drawing.Size(25, 15)
        Me.lbltest.TabIndex = 62
        Me.lbltest.Text = "......"
        Me.lbltest.Visible = False
        '
        'btntest
        '
        Me.btntest.Location = New System.Drawing.Point(130, 394)
        Me.btntest.Name = "btntest"
        Me.btntest.Size = New System.Drawing.Size(161, 23)
        Me.btntest.TabIndex = 61
        Me.btntest.Text = "Test Connection"
        Me.btntest.UseVisualStyleBackColor = True
        Me.btntest.Visible = False
        '
        'tmrreceive
        '
        Me.tmrreceive.Interval = 1000
        '
        'HemaConnectProperty
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(411, 314)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lbltest)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.btntest)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "HemaConnectProperty"
        Me.Text = "Hema connect"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbDataBit As System.Windows.Forms.ComboBox
    Friend WithEvents cbStopBit As System.Windows.Forms.ComboBox
    Friend WithEvents cbParity As System.Windows.Forms.ComboBox
    Friend WithEvents cbBaudRate As System.Windows.Forms.ComboBox
    Friend WithEvents cbPort As System.Windows.Forms.ComboBox
    Friend WithEvents cbInstrument As System.Windows.Forms.ComboBox
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents SaveMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents EditMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cancelMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lbltest As System.Windows.Forms.Label
    Friend WithEvents btntest As System.Windows.Forms.Button
    Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
    Friend WithEvents tmrreceive As System.Windows.Forms.Timer
    Friend WithEvents AddMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
