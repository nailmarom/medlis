﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DBSettingFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DBSettingFrm))
        Me.txtdbName = New System.Windows.Forms.TextBox
        Me.txtdbPass = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtdbUser = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtserverName = New System.Windows.Forms.TextBox
        Me.btnGenerateSettingShow = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnGenerateSettingOK = New System.Windows.Forms.Button
        Me.btnGenerateSettingCancel = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtdbName
        '
        Me.txtdbName.Location = New System.Drawing.Point(105, 58)
        Me.txtdbName.Name = "txtdbName"
        Me.txtdbName.Size = New System.Drawing.Size(282, 23)
        Me.txtdbName.TabIndex = 3
        '
        'txtdbPass
        '
        Me.txtdbPass.Location = New System.Drawing.Point(105, 118)
        Me.txtdbPass.Name = "txtdbPass"
        Me.txtdbPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtdbPass.Size = New System.Drawing.Size(282, 23)
        Me.txtdbPass.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 61)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 15)
        Me.Label6.TabIndex = 17
        Me.Label6.Text = "DBname"
        '
        'txtdbUser
        '
        Me.txtdbUser.Location = New System.Drawing.Point(105, 88)
        Me.txtdbUser.Name = "txtdbUser"
        Me.txtdbUser.Size = New System.Drawing.Size(282, 23)
        Me.txtdbUser.TabIndex = 4
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 88)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(48, 15)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "DBuser"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 118)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(76, 15)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "DBpassword"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(20, 22)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(178, 20)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "Generate DB Setting"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(12, 31)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(81, 15)
        Me.Label10.TabIndex = 24
        Me.Label10.Text = "Server Name"
        '
        'txtserverName
        '
        Me.txtserverName.Location = New System.Drawing.Point(105, 28)
        Me.txtserverName.Name = "txtserverName"
        Me.txtserverName.Size = New System.Drawing.Size(282, 23)
        Me.txtserverName.TabIndex = 2
        '
        'btnGenerateSettingShow
        '
        Me.btnGenerateSettingShow.Location = New System.Drawing.Point(314, 197)
        Me.btnGenerateSettingShow.Name = "btnGenerateSettingShow"
        Me.btnGenerateSettingShow.Size = New System.Drawing.Size(73, 30)
        Me.btnGenerateSettingShow.TabIndex = 7
        Me.btnGenerateSettingShow.Text = "Show"
        Me.btnGenerateSettingShow.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnGenerateSettingShow)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtserverName)
        Me.GroupBox1.Controls.Add(Me.btnGenerateSettingOK)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtdbUser)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtdbPass)
        Me.GroupBox1.Controls.Add(Me.txtdbName)
        Me.GroupBox1.Controls.Add(Me.btnGenerateSettingCancel)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 53)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(399, 233)
        Me.GroupBox1.TabIndex = 27
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Create Your DB Setting"
        '
        'btnGenerateSettingOK
        '
        Me.btnGenerateSettingOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnGenerateSettingOK.Location = New System.Drawing.Point(314, 160)
        Me.btnGenerateSettingOK.Name = "btnGenerateSettingOK"
        Me.btnGenerateSettingOK.Size = New System.Drawing.Size(73, 30)
        Me.btnGenerateSettingOK.TabIndex = 6
        Me.btnGenerateSettingOK.Text = "OK"
        Me.btnGenerateSettingOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnGenerateSettingOK.UseVisualStyleBackColor = True
        '
        'btnGenerateSettingCancel
        '
        Me.btnGenerateSettingCancel.Location = New System.Drawing.Point(235, 160)
        Me.btnGenerateSettingCancel.Name = "btnGenerateSettingCancel"
        Me.btnGenerateSettingCancel.Size = New System.Drawing.Size(73, 30)
        Me.btnGenerateSettingCancel.TabIndex = 8
        Me.btnGenerateSettingCancel.Text = "Close"
        Me.btnGenerateSettingCancel.UseVisualStyleBackColor = True
        '
        'DBSettingFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(418, 292)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label9)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "DBSettingFrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DB Setting"
        Me.TopMost = True
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnGenerateSettingCancel As System.Windows.Forms.Button
    Friend WithEvents txtdbName As System.Windows.Forms.TextBox
    Friend WithEvents txtdbPass As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnGenerateSettingOK As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtserverName As System.Windows.Forms.TextBox
    Friend WithEvents txtdbUser As System.Windows.Forms.TextBox
    Friend WithEvents btnGenerateSettingShow As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox

End Class
