﻿'
' Created by SharpDevelop.
' User: User
' Date: 10/14/2014
' Time: 9:49 AM
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Imports System.Data

Partial Public Class testGroupFrm
    Dim selectedtestGroupId As Integer
    Dim strsql As String
    Dim isNew As Boolean
    Dim isEdit As Boolean

    Dim tblGroupTest As DataTable
    Dim objConn As New clsGreConnect

    ' Dim da As Npgsql.NpgSqlClient.SqlDataAdapter
    Dim binding As New BindingSource
    Dim ds As Data.DataSet

    Public Sub New()
        ' The Me.InitializeComponent call is required for Windows Forms designer support.
        Me.InitializeComponent()

        '
        ' TODO : Add constructor code after InitializeComponents
        ''
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


        'strupdate = "update regsamp set customerid='" & customerID & _
        '   "',RECEIVEDATE='" & Format(Date.Parse(DTPreceiveDate.Text), "yyyy-MM-dd HH:mm:ss") & _
        '   "',SHIP='" & selectedShipmentID & _
        '   "',REFF='" & Trim(txtReference.Text) & _
        '   "',note='" & Trim(txtNote.Text) & _
        '   "',CAP='" & cap & _
        '   "',REASON='" & Trim(txtreason.Text) & _
        '   "',TOTALIDR='" & txttotalidr.Text & _
        '   "',TOTALUSD='" & txttotalusd.Text & _
        '   "',USERID='" & LogOnUserID & _
        '   "',STATUSAPP='" & statusapp & _
        '   "',PAYERID='" & payerid & _
        '   "' where regsampid='" & selectedRegSampID & "'"
        'Proses.ExecuteNonQuery(strupdate)


    End Sub
    Private Sub PopulateDg()

     
        dgTestGroup.DataSource = Nothing
        If dgTestGroup.Columns.Contains("chk") Then
            dgTestGroup.Columns.Clear()
        End If

        objConn.buildConn()
        tblGroupTest = New DataTable
        tblGroupTest = objConn.ExecuteQuery("select * from testgroup order by testgroupname asc")

        dgTestGroup.AllowUserToAddRows = False
        dgTestGroup.RowHeadersVisible = False

        
        dgTestGroup.DataSource = tblGroupTest


        Dim j As Integer
        For j = 0 To dgTestGroup.Columns.Count - 1
            dgTestGroup.Columns(j).Visible = False
        Next



        Dim chk As New DataGridViewCheckBoxColumn()
        dgTestGroup.Columns.Add(chk)
        chk.HeaderText = "SELECT"
        chk.Width = 60
        chk.Name = "chk"
        dgTestGroup.Columns("chk").DisplayIndex = 0
        dgTestGroup.Columns("chk").Name = "chk"

        dgTestGroup.Columns("testgroupname").Visible = True
        dgTestGroup.Columns("testgroupname").HeaderText = "Test Group Name"
        dgTestGroup.Columns("testgroupname").Width = 250

        dgTestGroup.Columns("id").Visible = False
        'DGPack.Columns("packname").Width = 250
        objConn.CloseConn()

        
    End Sub
    Private Sub testGroupFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
        txtTestGroupName.Enabled = False
        SaveMnu.Enabled = False
        EditMnu.Enabled = False
        DeleteMnu.Enabled = False
        ToolStrip1.ImageScalingSize = New Size(40, 40)
        PopulateDg()

    End Sub

    Private Sub mnuSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ToolStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles ToolStrip1.ItemClicked

    End Sub

    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click

        Dim testGroupname As String
        CekLicense()

        If txtTestGroupName.Text = "" Then
            MessageBox.Show("Nama test group tidak boleh kosong")
            Exit Sub
        End If


        If isNew = True Then
            testGroupname = txtTestGroupName.Text

            strsql = "insert into testgroup (" & _
            "testgroupname)" & _
            " VALUES (" & _
             "'" & testGroupname & "')"
            objConn.buildConn()
            objConn.ExecuteNonQuery(strsql)

            objConn.CloseConn()


        ElseIf isEdit = True Then
            testGroupname = txtTestGroupName.Text
            strsql = "update testgroup set testgroupname='" & testGroupname & _
               "' where id='" & selectedtestGroupId & "'"
            objConn.buildConn()
            objConn.ExecuteNonQuery(strsql)

            objConn.CloseConn()
        End If
        isEdit = False
        isNew = False
        SaveMnu.Enabled = False
        EditMnu.Enabled = False
        txtTestGroupName.Enabled = False
        PopulateDg()
    End Sub

    Private Sub AddMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddMnu.Click
        isNew = True
        EditMnu.Enabled = False
        SaveMnu.Enabled = True
        PopulateDg()
        txtTestGroupName.Enabled = True
        txtTestGroupName.Focus()

    End Sub

    Private Sub dgTestGroup_Navigate(ByVal sender As System.Object, ByVal ne As System.Windows.Forms.NavigateEventArgs)

    End Sub

    Private Sub EditMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditMnu.Click
        'selectedtestGroup = dgTestGroup.dgTestGroup.CurrentRowIndex

        Dim i As Integer
        isNew = False
        isEdit = True
        txtTestGroupName.Enabled = True
        SaveMnu.Enabled = True
        DeleteMnu.Enabled = False
        'For Each RW As DataGridViewRow In dgTestGroup.SelectedRows

        '    'Send the first cell value into textbox'
        '    If Not (dgTestGroup.CurrentRow.IsNewRow) Then
        '        txtTestGroupName.Text = RW.Cells(1).Value.ToString
        '    End If
        'Next

        'For Each cel As DataGridViewCell In dgTestGroup.SelectedCells
        '    'If Not (dgTestGroup.CurrentRow.IsNewRow) Then
        '    If cel.ColumnIndex = 1 Then
        '        txtTestGroupName.Text = cel.Value
        '    End If
        '    'End If
        'Next

        'For Each col As DataGridViewColumn In dgTestGroup.SelectedColumns
        '    If col.Index = 1 Then
        '        txtTestGroupName.Text = col.ToString ' .Value
        '    End If
        'Next


        'ob = dgTestGroup.Rows(dgTestGroup.se)
        'If Not (IsDBNull(dgTestGroup.Item("id", i).Value)) Then
        '    selectedtestGroup = dgTestGroup.Item("id", i).Value
        '    txtTestGroupName.Text = dgTestGroup.Item("testgroupname", i).Value
        'End If

        For i = 0 To dgTestGroup.Rows.Count - 1
            If dgTestGroup.Item("chk", i).Value = True Then
                If Not (dgTestGroup.CurrentRow.IsNewRow) Then
                    If Not (IsDBNull(dgTestGroup.Item("id", i).Value)) Then
                        'selectedtestGroup = dgTestGroup.Item("id", i).Value
                        txtTestGroupName.Text = dgTestGroup.Item("testgroupname", i).Value
                        'ChoosedTestItem(selected_test_packid)
                        'idr = idr + PackPriceIDR(selected_test_packid)
                        'usd = usd + PackPriceUSD(selected_test_packid)
                        'MsgBox(dgTestGroup.Item("testgroupname", i).Value)
                    End If
                End If
            End If
        Next



    End Sub

    Private Sub dgTestGroup_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgTestGroup.CellContentClick
        'super mantap
        If Not (dgTestGroup.CurrentRow.IsNewRow) And Not (e.RowIndex = -1) Then
            If Not IsDBNull(dgTestGroup.Item("id", e.RowIndex).Value) Then
                If TypeOf dgTestGroup.Rows(e.RowIndex).Cells(e.ColumnIndex) Is DataGridViewCheckBoxCell Then
                    Dim dgvCheckBoxCell As DataGridViewCheckBoxCell = dgTestGroup.Rows(e.RowIndex).Cells(e.ColumnIndex)
                    'Commit the data to the datasouce.
                    dgTestGroup.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    Dim checked As Boolean = CType(dgvCheckBoxCell.Value, Boolean)
                    selectedtestGroupId = dgTestGroup.Item("id", e.RowIndex).Value
                    'MessageBox.Show(e.RowIndex & " " & checked & " " & e.ColumnIndex)
                    If checked Then
                        Dim i As Integer
                        EditMnu.Enabled = True
                        DeleteMnu.Enabled = True
                        For i = 0 To dgTestGroup.Rows.Count - 1
                            If i <> e.RowIndex Then
                                dgTestGroup.Item("chk", i).Value = False
                            End If
                        Next

                    End If

                End If
            End If
        End If
    End Sub


    Private Sub DeleteMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteMnu.Click

        Dim isavailable As Boolean 'top check yang di centang
        isavailable = False
        Dim j As Integer
        For j = 0 To dgTestGroup.Rows.Count - 1
            'If Not (dgTestType.CurrentRow.IsNewRow) Then
            If dgTestGroup.Item("chk", j).Value = True Then
                isavailable = True
            End If
        Next
        If isavailable = False Then
            MessageBox.Show("Pilih salah satu test item untuk diedit")
            Exit Sub
        End If

        Dim i As Integer
        For i = 0 To dgTestGroup.Rows.Count - 1
            If dgTestGroup.Item("chk", i).Value = True Then
                strsql = "delete from testgroup " & _
                "where id='" & selectedtestGroupId & "'"

                objConn.buildConn()
                objConn.ExecuteNonQuery(strsql)
                objConn.CloseConn()
            End If
        Next
        SaveMnu.Enabled = False
        EditMnu.Enabled = False
        isEdit = False
        isNew = False
        PopulateDg()
    End Sub

    Private Sub cancelMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelMnu.Click
        isEdit = False
        isNew = False
        SaveMnu.Enabled = False
        EditMnu.Enabled = False
        DeleteMnu.Enabled = False
        PopulateDg()
        txtTestGroupName.Enabled = False
      



    End Sub
End Class
