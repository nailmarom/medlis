﻿Imports GemBox.Spreadsheet
Imports Microsoft.Win32
Public Class NotaExlFrm

    Private Sub NotaExlFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
        ElementHost1.Width = Me.Width
        ElementHost1.Height = Me.Height - (Me.Height / 8)

        Dim excelPath As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "notapatient.xlsx")
        Dim ef As ExcelFile = ExcelFile.Load(excelPath)

        Dim xpsDocument = ef.ConvertToXpsDocument(SaveOptions.XpsDefault)
        Dim docview As New System.Windows.Controls.DocumentViewer
        docview.Height = Me.Height
        ElementHost1.Child = docview
        docview.Tag = xpsDocument
        docview.Document = xpsDocument.GetFixedDocumentSequence()
    End Sub
End Class