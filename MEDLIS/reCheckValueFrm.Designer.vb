﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class reCheckValueFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(reCheckValueFrm))
        Me.dgvManual = New System.Windows.Forms.DataGridView
        Me.txtlabnum = New System.Windows.Forms.TextBox
        Me.btnfindlabnumber = New System.Windows.Forms.Button
        Me.dgvJobList = New System.Windows.Forms.DataGridView
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtSN = New System.Windows.Forms.TextBox
        Me.txtLabnumber = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtaddress = New System.Windows.Forms.TextBox
        Me.txtname = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txttelepon = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.btnRecheck = New System.Windows.Forms.Button
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtbarcode = New System.Windows.Forms.TextBox
        CType(Me.dgvManual, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvJobList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvManual
        '
        Me.dgvManual.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvManual.Location = New System.Drawing.Point(523, 225)
        Me.dgvManual.Name = "dgvManual"
        Me.dgvManual.Size = New System.Drawing.Size(569, 291)
        Me.dgvManual.TabIndex = 0
        '
        'txtlabnum
        '
        Me.txtlabnum.Location = New System.Drawing.Point(14, 14)
        Me.txtlabnum.Name = "txtlabnum"
        Me.txtlabnum.Size = New System.Drawing.Size(227, 21)
        Me.txtlabnum.TabIndex = 2
        '
        'btnfindlabnumber
        '
        Me.btnfindlabnumber.Location = New System.Drawing.Point(260, 14)
        Me.btnfindlabnumber.Name = "btnfindlabnumber"
        Me.btnfindlabnumber.Size = New System.Drawing.Size(87, 27)
        Me.btnfindlabnumber.TabIndex = 3
        Me.btnfindlabnumber.Text = "OK"
        Me.btnfindlabnumber.UseVisualStyleBackColor = True
        '
        'dgvJobList
        '
        Me.dgvJobList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvJobList.Location = New System.Drawing.Point(14, 52)
        Me.dgvJobList.Name = "dgvJobList"
        Me.dgvJobList.Size = New System.Drawing.Size(492, 503)
        Me.dgvJobList.TabIndex = 4
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtbarcode)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtSN)
        Me.GroupBox1.Controls.Add(Me.txtLabnumber)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtaddress)
        Me.GroupBox1.Controls.Add(Me.txtname)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txttelepon)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(523, 44)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(569, 175)
        Me.GroupBox1.TabIndex = 21
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 88)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 15)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Sample No"
        '
        'txtSN
        '
        Me.txtSN.Location = New System.Drawing.Point(110, 83)
        Me.txtSN.Name = "txtSN"
        Me.txtSN.Size = New System.Drawing.Size(127, 21)
        Me.txtSN.TabIndex = 17
        '
        'txtLabnumber
        '
        Me.txtLabnumber.Location = New System.Drawing.Point(110, 28)
        Me.txtLabnumber.Name = "txtLabnumber"
        Me.txtLabnumber.Size = New System.Drawing.Size(127, 21)
        Me.txtLabnumber.TabIndex = 16
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(17, 31)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(86, 15)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Lab Number"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtaddress
        '
        Me.txtaddress.Location = New System.Drawing.Point(331, 84)
        Me.txtaddress.Name = "txtaddress"
        Me.txtaddress.Size = New System.Drawing.Size(214, 21)
        Me.txtaddress.TabIndex = 13
        '
        'txtname
        '
        Me.txtname.Location = New System.Drawing.Point(110, 54)
        Me.txtname.Name = "txtname"
        Me.txtname.Size = New System.Drawing.Size(243, 21)
        Me.txtname.TabIndex = 12
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(257, 89)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(51, 15)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Alamat"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(17, 60)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Nama"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txttelepon
        '
        Me.txttelepon.Location = New System.Drawing.Point(331, 111)
        Me.txttelepon.Name = "txttelepon"
        Me.txttelepon.Size = New System.Drawing.Size(214, 21)
        Me.txttelepon.TabIndex = 14
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(257, 117)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 15)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Telepon"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnRecheck
        '
        Me.btnRecheck.Location = New System.Drawing.Point(523, 526)
        Me.btnRecheck.Name = "btnRecheck"
        Me.btnRecheck.Size = New System.Drawing.Size(93, 33)
        Me.btnRecheck.TabIndex = 23
        Me.btnRecheck.Text = "Recheck"
        Me.btnRecheck.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(17, 117)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(60, 15)
        Me.Label5.TabIndex = 20
        Me.Label5.Text = "Barcode"
        '
        'txtbarcode
        '
        Me.txtbarcode.Location = New System.Drawing.Point(110, 112)
        Me.txtbarcode.Name = "txtbarcode"
        Me.txtbarcode.Size = New System.Drawing.Size(127, 21)
        Me.txtbarcode.TabIndex = 19
        '
        'reCheckValueFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1103, 567)
        Me.Controls.Add(Me.btnRecheck)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvJobList)
        Me.Controls.Add(Me.btnfindlabnumber)
        Me.Controls.Add(Me.txtlabnum)
        Me.Controls.Add(Me.dgvManual)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "reCheckValueFrm"
        Me.Text = "Revisi Baca Ulang"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvManual, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvJobList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvManual As System.Windows.Forms.DataGridView
    Friend WithEvents txtlabnum As System.Windows.Forms.TextBox
    Friend WithEvents btnfindlabnumber As System.Windows.Forms.Button
    Friend WithEvents dgvJobList As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtLabnumber As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtaddress As System.Windows.Forms.TextBox
    Friend WithEvents txtname As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txttelepon As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSN As System.Windows.Forms.TextBox
    Friend WithEvents btnRecheck As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtbarcode As System.Windows.Forms.TextBox
End Class
