﻿Imports System.Data
Imports System.Data.SqlClient

Public Class identitasSettingFrm

    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click
        If rdlabauto.Checked = False And rdlabmanual.Checked = False And rdpasienauto.Checked = False And rdpasienmanual.Checked = False Then
            MessageBox.Show("Silahkan pilih setting")
            Exit Sub
        End If
        If rdlabauto.Checked = False And rdlabmanual.Checked = False Then
            MessageBox.Show("Silahkan pilih setting nomor lab")
            Exit Sub
        End If
        If rdpasienauto.Checked = False And rdpasienmanual.Checked = False Then
            MessageBox.Show("Silahkan pilih setting nomor pasien")
            Exit Sub
        End If

        patientNumbering()
        LabNumbering()
        Me.Close()
    End Sub
    Private Sub patientNumbering()
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim strsql As String
        Dim strdelete As String

        Dim name As String = "patientnumbering"

        Dim val As String
        If rdpasienmanual.Checked Then
            val = "1"
        ElseIf rdpasienauto.Checked Then
            val = "0"
        End If

        strdelete = "delete from setting where name='patientnumbering'"
        ogre.ExecuteNonQuery(strdelete)
        ogre.CloseConn()

        Dim ogreINSERT As New clsGreConnect
        ogreINSERT.buildConn()
        digitResult = val
        strsql = "insert into setting(name,value)values('" & name & "','" & val & "')"
        ogreINSERT.ExecuteNonQuery(strsql)
        ogreINSERT.CloseConn()
        '  Label2.Text = " Berhasil disimpan"
        ' Label2.ForeColor = Color.Blue
    End Sub
    Private Sub LabNumbering()
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim strsql As String
        Dim strdelete As String

        Dim name As String = "labnumbering"

        Dim val As String
        If rdlabmanual.Checked Then
            val = "1"
        ElseIf rdlabauto.Checked Then
            val = "0"
        End If

        strdelete = "delete from setting where name='labnumbering'"
        ogre.ExecuteNonQuery(strdelete)
        ogre.CloseConn()

        Dim ogreINSERT As New clsGreConnect
        ogreINSERT.buildConn()
        digitResult = val
        strsql = "insert into setting(name,value)values('" & name & "','" & val & "')"
        ogreINSERT.ExecuteNonQuery(strsql)
        ogreINSERT.CloseConn()
        '  Label2.Text = " Berhasil disimpan"
        ' Label2.ForeColor = Color.Blue
    End Sub

    Private Sub identitasSettingFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadPatientNumbering()
        LoadLabNumbering()
    End Sub
    Private Sub LoadPatientNumbering()
        Dim strsetting As String
        strsetting = "select * from setting where name='patientnumbering'"
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim cmd As New SqlClient.SqlCommand(strsetting, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If CInt(rdr("value")) = 1 Then
                    rdpasienmanual.Checked = True
                    rdpasienauto.Checked = False
                ElseIf CInt(rdr("value")) = 0 Then
                    rdpasienauto.Checked = True
                    rdpasienmanual.Checked = False
                End If
            Loop
        Else
            'ComboBox1.SelectedIndex = 1
            Label1.Text = "Silahkan pilih setting.."
        End If
        cmd.Dispose()
        ogre.CloseConn()
    End Sub
    Private Sub LoadLabNumbering()
        Dim strsetting As String
        strsetting = "select * from setting where name='labnumbering'"
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim cmd As New SqlClient.SqlCommand(strsetting, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If CInt(rdr("value")) = 1 Then
                    rdlabmanual.Checked = True
                    rdlabauto.Checked = False
                ElseIf CInt(rdr("value")) = 0 Then
                    rdlabauto.Checked = True
                    rdlabmanual.Checked = False
                End If
            Loop
        Else
            'ComboBox1.SelectedIndex = 1
            Label1.Text = "Silahkan pilih setting.."
        End If
        cmd.Dispose()
        ogre.CloseConn()
    End Sub

    Private Sub ToolStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles ToolStrip1.ItemClicked
        Me.Close()
    End Sub
End Class