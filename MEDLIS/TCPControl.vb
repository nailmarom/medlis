﻿Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports System.Threading
Imports System.Text.RegularExpressions
Imports System.Text

Public Class TCPControl
    Public theClient As TcpClient
    Public SendDataStream As StreamWriter

    Private palioStream As NetworkStream
    Private ReceiveDataStream As StreamReader

    Private CommThread As Thread

    Public isListening As Boolean = True
    Public Event MessageReceive(ByVal sender As TCPControl, ByVal Data As String)

    Public Sub New(ByVal Host As String, ByVal port As Integer)
        Try
            theClient = New TcpClient(Host, port)
            SendDataStream = New StreamWriter(theClient.GetStream)
            If theClient.Client.Connected Then
                CommThread = New Thread(New ThreadStart(AddressOf Listening))
                CommThread.Start()

            End If

        Catch ex As Exception
            MessageBox.Show("Tidak tersambung, silahkan cek IP Address dan koneksi")
        End Try

    End Sub

    Private Sub Listening()

        Do Until isListening = False
            Try
                'ReceiveDataStream = New StreamReader(theClient.GetStream)
                palioStream = theClient.GetStream '.GetStream)
            Catch ex As Exception
                isListening = False
                MessageBox.Show("Palio tidak tersambung")
            End Try

            Try
                Dim inStream(10024) As Byte
                Dim myCompleteMessage As StringBuilder = New StringBuilder()
                Dim numberOfBytesRead As Integer = 0
                Do While palioStream.DataAvailable
                    If palioStream.DataAvailable Then
                        numberOfBytesRead = palioStream.Read(inStream, 0, inStream.Length)
                        If numberOfBytesRead > 0 Then
                            myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(inStream, 0, numberOfBytesRead))
                        End If
                    End If
                Loop
                If myCompleteMessage.ToString.Length > 0 Then
                    RaiseEvent MessageReceive(Me, myCompleteMessage.ToString)
                    ' RaiseEvent MessageReceive(Me, ReceiveDataStream.ReadToEnd)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
        Loop
        ' Thread.Sleep(100)


    End Sub

    Public Sub SendData(ByVal data As String)
        'SendDataStream.Write(data & vbCrLf)
        SendDataStream.Write(data) 'asli
        SendDataStream.Flush()


    End Sub


End Class
