﻿Public Class rptSubreport

 
    Private Sub rptSubreport_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        ShowReport()
    End Sub

    Private Sub ShowReport()
        Try
            Dim rdlView As New fyiReporting.RdlViewer.RdlViewer
            rdlView.SourceFile = New Uri("D:\Medical\VBMEDLIS\MEDLIS\MEDLIS\Reporting\test-sub-report.rdl")
            ' rdlView.Parameters += string.Format("&TesParam1={0}&TestParam2={1}", "Value 1", "Value 2");
            'rdlView.Parameters += String.Format("&TesParam1={0}&TestParam2={1}", "Value 1", "Value 2")
            '  rdlView.Parameters = String.Format("&Parstatus={0}&Parlabnum={1}&Parcon={2}", status, labnum, strcn)
            rdlView.Rebuild()

            rdlView.Dock = DockStyle.Fill
            Me.Controls.Add(rdlView)

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub

End Class