﻿Imports System.Data
Imports Microsoft.Win32
Imports System.Data.SqlClient
Imports GemBox.Spreadsheet
Imports GemBox.Spreadsheet.ConditionalFormatting


Public Class HemaAllFinishJobsFrm
    'Dim greobject As clsGreConnect
    'Dim dtable As DataTable
    'Dim selectedLabNumber As String
    'Dim selectedPatient As String
    'Dim selsampleno As String
    'Dim patientName As String

    'Dim patientPhone As String
    'Dim patientaddress As String
    'Dim selectedDateBirth As Date
    'Dim alreadyclick As Boolean = False
    'Dim stateChoice As Integer '0 new, 1 sampling, 2 finish, 8 LAGI DI BACA, 3 semua
    'Dim selectedHemaBarcode As String

    'Private Sub jobDetailFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '    'bug that means you have to set the desired icon again otherwise it reverts to default when child form is maximised
    '    Icon = New System.Drawing.Icon("medlis2.ico")


    '    CekLicense()
    '    SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY")
    '    ClearLN()
    '    ShowDataDetail(30)

    'End Sub
    'Private Sub ClearLN()
    '    selectedLabNumber = ""
    'End Sub

    'Private Sub ShowButton()
    '    mnuCetak.Enabled = True
    'End Sub
    'Private Sub ShowData()
    '    Dim strsql As String
    '    strsql = "select job.id as jobid,job.idpasien,job.labnumber,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail where status='0' and active='1')"
    '    Joblist(strsql)
    '    ' ContructDgv()
    '    HideButton()
    'End Sub

    'Private Sub HideButton()
    '    mnuCetak.Enabled = False
    'End Sub

    'Private Sub Joblist(ByVal strsql As String)
    '    'Dim strsql As String
    '    'strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.id as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.status='0'"
    '    greobject = New clsGreConnect
    '    greobject.buildConn()

    '    dtable = New DataTable

    '    dtable = greobject.ExecuteQuery(strsql)

    '    'dgvjoblist = New DataGridView

    '    dgvJoblist.DataSource = dtable

    '    Dim j As Integer
    '    For j = 0 To dgvJoblist.Columns.Count - 1
    '        dgvJoblist.Columns(j).Visible = False
    '    Next

    '    dgvJoblist.Columns("labnumber").Visible = True
    '    dgvJoblist.Columns("labnumber").HeaderText = "Lab Number"
    '    dgvJoblist.Columns("labnumber").Width = 80
    '    dgvJoblist.Columns("labnumber").ReadOnly = True

    '    dgvJoblist.Columns("patientname").Visible = True
    '    dgvJoblist.Columns("patientname").HeaderText = "Nama pasien"
    '    dgvJoblist.Columns("patientname").Width = 160
    '    dgvJoblist.Columns("patientname").ReadOnly = True


    '    dgvJoblist.Columns("barcode").Visible = True
    '    dgvJoblist.Columns("barcode").HeaderText = "Barcode"
    '    dgvJoblist.Columns("barcode").Width = 80
    '    dgvJoblist.Columns("barcode").ReadOnly = True




    '    Dim buttonColumn As New DataGridViewButtonColumn()
    '    With buttonColumn
    '        .Name = "Detail"
    '        .HeaderText = "Detail"
    '        .Width = 90
    '        .Text = "Detail"
    '        .UseColumnTextForButtonValue = True
    '        .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
    '        .FlatStyle = FlatStyle.Standard
    '        .CellTemplate.Style.BackColor = Color.Yellow
    '        .DisplayIndex = 0
    '    End With
    '    dgvJoblist.Columns.Add(buttonColumn)

    '    dgvJoblist.AllowUserToAddRows = False
    '    dgvJoblist.RowHeadersVisible = False
    'End Sub

    'Private Sub dgvJoblist_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvJoblist.CellContentClick
    '    Dim idx As Integer
    '    idx = dgvJoblist.Columns("Detail").Index

    '    'top
    '    If e.RowIndex < 0 OrElse Not e.ColumnIndex = dgvJoblist.Columns("Detail").Index Then

    '        Return

    '    Else

    '        If Not dgvJoblist.CurrentRow.IsNewRow Then
    '            selectedHemaBarcode = dgvJoblist.Item("barcode", dgvJoblist.CurrentRow.Index).Value
    '            selectedLabNumber = dgvJoblist.Item("labnumber", dgvJoblist.CurrentRow.Index).Value
    '            PopulatedgvDetail()
    '            alreadyclick = True
    '        End If

    '    End If
    'End Sub


    'Private Sub PopulatedgvDetail()
    '    '
    '    Dim objGreConnect As New clsGreConnect
    '    objGreConnect.buildConn()

    '    Dim dt As New DataTable
    '    dgvdetail.DataSource = Nothing



    '    Dim strsql As String

    '    strsql = "select hemadata.itemname,hemadata.result,hemadata.abnormalflag,hemadata.unit,hemadata.refrange,testtype.displayindex from hemadata,testtype where hemadata.itemname=testtype.analyzertestname and hemadata.barcode='" & selectedHemaBarcode & "' order by testtype.displayindex asc"
    '    'strsql = "select * from hemadata where barcode='" & selectedHemaBarcode & "'"

    '    Dim tblData As New DataTable

    '    tblData = objGreConnect.ExecuteQuery(strsql) ' order by testgroupname asc")
    '    dgvdetail.DataSource = tblData

    '    For j = 0 To dgvdetail.Columns.Count - 1
    '        dgvdetail.Columns(j).Visible = False
    '    Next

    '    dgvdetail.Columns("itemname").Visible = True
    '    dgvdetail.Columns("itemname").HeaderText = "Test Name"
    '    dgvdetail.Columns("itemname").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
    '    dgvdetail.Columns("itemname").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
    '    dgvdetail.Columns("itemname").Width = 100
    '    dgvdetail.Columns("itemname").ReadOnly = True

    '    dgvdetail.Columns("result").Visible = True
    '    dgvdetail.Columns("result").HeaderText = "Result"
    '    dgvdetail.Columns("result").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
    '    dgvdetail.Columns("result").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
    '    dgvdetail.Columns("result").Width = 120
    '    dgvdetail.Columns("result").ReadOnly = True

    '    dgvdetail.Columns("abnormalflag").Visible = True
    '    dgvdetail.Columns("abnormalflag").HeaderText = "Abnormal Flag"
    '    dgvdetail.Columns("abnormalflag").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
    '    dgvdetail.Columns("abnormalflag").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
    '    dgvdetail.Columns("abnormalflag").Width = 120
    '    dgvdetail.Columns("abnormalflag").ReadOnly = True

    '    dgvdetail.Columns("unit").Visible = True
    '    dgvdetail.Columns("unit").HeaderText = "Unit"
    '    dgvdetail.Columns("unit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
    '    dgvdetail.Columns("unit").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
    '    dgvdetail.Columns("unit").Width = 90
    '    dgvdetail.Columns("unit").ReadOnly = True


    '    dgvdetail.Columns("refrange").Visible = True
    '    dgvdetail.Columns("refrange").HeaderText = "Ref Range"
    '    dgvdetail.Columns("refrange").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
    '    dgvdetail.Columns("refrange").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
    '    dgvdetail.Columns("refrange").Width = 100
    '    dgvdetail.Columns("refrange").ReadOnly = True

    '    dgvdetail.AllowUserToAddRows = False
    '    dgvdetail.RowHeadersVisible = False

    '    objGreConnect.CloseConn()

    'End Sub
    'Private Function GetUniversalTestName(ByVal sampleno As String, ByVal labnumber As String) As String
    '    Dim strfinder As String
    '    Dim strresult As String
    '    Dim testcon As New clsGreConnect
    '    testcon.buildConn()
    '    strfinder = "select universaltest from jobdetail where sampleno='" & sampleno & "' and labnumber='" & labnumber & "'"
    '    Dim thecommand As New SqlClient.SqlCommand(strfinder, testcon.grecon)
    '    Dim theReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)
    '    strresult = ""
    '    Do While theReader.Read
    '        strresult = strresult & " " & theReader("universaltest")
    '    Loop
    '    thecommand = Nothing
    '    theReader.Close()
    '    testcon.CloseConn()
    '    testcon = Nothing

    '    Return strresult
    'End Function





    'Private Sub clearDGVdetail()
    '    'If dgvdetail.Rows.Count > 0 Then
    '    '    dgvdetail.Rows.Clear()
    '    'End If
    'End Sub







    'Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    '    Dim ln As String

    '    'strcn = "Driver={PostgreSQL ANSI};database=MEDLIS;server=127.0.0.1;port=5432;uid=dian;sslmode=disable;readonly=0;protocol=7.4;User ID=dian;password=dianjuga;"

    '    Dim rptreportfrm As New resultwithparamandconparamFrm(selectedLabNumber, stateChoice, strdbconn)
    '    rptreportfrm.MdiParent = MainFrm
    '    rptreportfrm.Show()
    'End Sub

    'Private Sub btnLabReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim rptlabreport As New rptLabReportFrm(selectedLabNumber, selectedPatient)
    '    rptlabreport.MdiParent = MainFrm
    '    rptlabreport.Show()

    'End Sub


    'Private Sub ShowDataDetail(ByVal intshow As Integer)
    '    stateChoice = FINISHSAMPLE
    '    ShowButton()
    '    Dim strsql As String

    '    strsql = "select distinct h.barcode,p.patientname,j.labnumber from hemadata h,jobdetail j,patient p,job b where h.barcode=j.barcode and j.labnumber=b.labnumber and b.idpasien=p.id"
    '    dgvJoblist.DataSource = Nothing
    '    If dgvJoblist.Columns.Contains("Detail") Then
    '        dgvJoblist.Columns.Clear()
    '    End If
    '    clearDGVdetail()
    '    Joblist(strsql)
    '    lblStatus.Text = "Selesai/Sudah ada hasil"
    'End Sub

    'Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCetak.Click
    '    ' printresult
    '    If selectedLabNumber.Length <> 0 Then

    '        Dim excelPath As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "patient-result.xlsx")
    '        'Dim excelPath As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "new.xlsx")
    '        Dim ef As ExcelFile = ExcelFile.Load(excelPath)
    '        Dim ws As ExcelWorksheet = ef.Worksheets(0)
    '        'Dim ef As ExcelFile = New ExcelFile
    '        'Dim ws As ExcelWorksheet = ef.Worksheets.Add("abc")
    '        Dim datereceived As Date
    '        Dim idpasien As Integer
    '        Dim docname As String
    '        Dim docaddress As String
    '        Dim age As String
    '        Dim strsql As String
    '        strsql = "select idpasien,docname,docaddress,printage,datereceived from job where labnumber='" & selectedLabNumber & "'"

    '        Dim greshow As New clsGreConnect
    '        greshow.buildConn()

    '        Dim cmd As New  SqlClient.SqlCommand(strsql, greshow.grecon)
    '        Dim rdr As  SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
    '        Do While rdr.Read
    '            idpasien = rdr("idpasien")
    '            If Not IsDBNull(rdr("docname")) Then
    '                docname = rdr("docname")
    '            Else
    '                docname = ""
    '            End If
    '            If Not IsDBNull(rdr("docaddress")) Then
    '                docaddress = rdr("docaddress")
    '            Else
    '                docaddress = ""
    '            End If
    '            If Not IsDBNull(rdr("printage")) Then
    '                age = rdr("printage")
    '            Else
    '                age = " "
    '            End If

    '            If Not IsDBNull(rdr("datereceived")) Then
    '                datereceived = rdr("datereceived")
    '            Else

    '            End If
    '        Loop

    '        rdr.Close()
    '        cmd.Dispose()
    '        greshow.CloseConn()

    '        Dim datetoprint As String
    '        datetoprint = CStr(datereceived.Day) & "/" & CStr(datereceived.Month) & "/" & CStr(datereceived.Year)


    '        '===========================================
    '        strsql = "select * from patient where id='" & idpasien & "'"
    '        Dim grepatient As New clsGreConnect
    '        grepatient.buildConn()

    '        Dim alamatpasien As String
    '        Dim namapasien As String
    '        Dim sex As String
    '        Dim tgllahir As String
    '        Dim phone As String
    '        Dim printIdPasien As String

    '        Dim cmdpatient As New  SqlClient.SqlCommand(strsql, grepatient.grecon)
    '        Dim rdrpatient As  SqlDataReader = cmdpatient.ExecuteReader(CommandBehavior.CloseConnection)
    '        Do While rdrpatient.Read
    '            If Not IsDBNull(rdrpatient("patientname")) Then
    '                namapasien = rdrpatient("patientname")
    '            Else
    '                namapasien = ""
    '            End If
    '            If Not IsDBNull(rdrpatient("address")) Then
    '                alamatpasien = rdrpatient("address")
    '            Else
    '                alamatpasien = ""
    '            End If

    '            If Not IsDBNull(rdrpatient("malefemale")) Then
    '                If rdrpatient("malefemale") = 0 Then
    '                    sex = "Wanita"
    '                Else
    '                    sex = "Laki Laki"
    '                End If
    '            Else
    '                sex = "Unknown"
    '            End If

    '            If Not IsDBNull(rdrpatient("birthdate")) Then
    '                tgllahir = rdrpatient("birthdate")
    '            Else
    '                tgllahir = ""
    '            End If
    '            If Not IsDBNull(rdrpatient("phone")) Then
    '                phone = rdrpatient("phone")
    '            Else
    '                phone = "00000"
    '            End If
    '            If Not IsDBNull(rdrpatient("idpatient")) Then
    '                printIdPasien = rdrpatient("idpatient")
    '            Else
    '                printIdPasien = ""
    '            End If

    '        Loop

    '        rdrpatient.Close()
    '        cmdpatient.Dispose()
    '        grepatient.CloseConn()

    '        '=========================================== analisa
    '        'strsql = "select jobdetail.labnumber,testtype.testname,jobdetail.resultabnormalflag,jobdetail.measurementvalue,testtype.keterangan,testtype.nilairujukan,testtype.satuan,testtype.displayindex from jobdetail,testtype where jobdetail.idtesttype=testtype.id  and  jobdetail.labnumber='" & selectedLabNumber & "' order by displayindex asc"
    '        strsql = "select jobdetail.labnumber,testtype.testname,testtype.analyzertestname,jobdetail.resultabnormalflag,jobdetail.measurementvalue,testtype.keterangan,testtype.nilairujukan,testtype.satuan,testtype.displayindex from jobdetail,testtype where jobdetail.idtesttype=testtype.id  and  jobdetail.labnumber='" & selectedLabNumber & "' order by displayindex asc"

    '        Dim greanalisa As New clsGreConnect
    '        greanalisa.buildConn()

    '        Dim testname As String
    '        Dim resultabnormalflag As String
    '        Dim hasil As String
    '        Dim ket As String
    '        Dim rujukan As String
    '        Dim satuan As String
    '        Dim intRow As Integer = 12

    '        Dim rctestname As String
    '        Dim rchasil As String
    '        Dim rcrujukan As String
    '        Dim rcsatuan As String
    '        Dim rcket As String


    '        Dim row As Integer = 12
    '        Dim col As Integer = 0

    '        Dim cmdanalisa As New  SqlClient.SqlCommand(strsql, greanalisa.grecon)
    '        Dim rdranalisa As  SqlDataReader = cmdanalisa.ExecuteReader(CommandBehavior.CloseConnection)
    '        Do While rdranalisa.Read
    '            testname = rdranalisa("analyzertestname")

    '            'rctestname = "A" & CStr(intRow)
    '            'ws.Cells(rctestname).Value = testname
    '            ws.Cells(row, 0).Value = testname
    '            ws.Cells(row, 0).Style.WrapText = False

    '            If Not IsDBNull(rdranalisa("measurementvalue")) Then
    '                hasil = rdranalisa("measurementvalue")
    '            Else
    '                hasil = "-"
    '            End If


    '            'rchasil = "C" & CStr(intRow)
    '            'ws.Cells(rchasil).Value = hasil
    '            If Not IsDBNull(rdranalisa("resultabnormalflag")) Then
    '                resultabnormalflag = rdranalisa("resultabnormalflag")
    '            Else
    '                resultabnormalflag = "-"
    '            End If

    '            If Trim(resultabnormalflag) = "N" Then
    '                ws.Cells(row, 2).Value = hasil
    '            Else
    '                ws.Cells(row, 2).Value = hasil & " *"
    '            End If

    '            If Not IsDBNull(rdranalisa("nilairujukan")) Then
    '                rujukan = rdranalisa("nilairujukan")
    '            Else
    '                rujukan = ""
    '            End If

    '            'rcrujukan = "E" & CStr(intRow)
    '            'ws.Cells(rcrujukan).Value = rujukan
    '            ws.Cells(row, 4).Value = rujukan
    '            If Not IsDBNull(rdranalisa("satuan")) Then
    '                satuan = rdranalisa("satuan")
    '            Else
    '                satuan = ""
    '            End If

    '            'rcsatuan = "G" & CStr(intRow)
    '            'ws.Cells(rcsatuan).Value = satuan
    '            ws.Cells(row, 6).Value = satuan
    '            If Not IsDBNull(rdranalisa("keterangan")) Then
    '                ket = rdranalisa("keterangan")
    '            Else
    '                ket = ""
    '            End If

    '            rcket = "H" & CStr(intRow)
    '            'ws.Cells(rcket).Style.WrapText = True
    '            'ws.Cells(rcket).Value = ket
    '            ws.Cells(row, 7).Value = ket
    '            'ws.Cells(
    '            ws.Cells(row, 7).Style.WrapText = True

    '            'intRow = intRow + 1
    '            'if int row > 38,83,128 then..  loncat ke 57,101
    '            row = row + 1

    '            If row = 36 Then '39 awalnya
    '                row = 57
    '            ElseIf row = 81 Then
    '                row = 101
    '            End If

    '        Loop

    '        rdranalisa.Close()
    '        cmdanalisa.Dispose()
    '        greanalisa.CloseConn()


    '        'ws.HeadersFooters.FirstPage.Header.CenterSection.Content = "Laboratorium Klinik"
    '        'ws.HeadersFooters.FirstPage.Header.RightSection.Content = "Laboratorium Klinik"



    '        ws.Cells("B2").Value = ": " & docname
    '        ws.Cells("B3").Value = ": " & docaddress
    '        ws.Cells("B4").Value = ": " & selectedLabNumber
    '        ws.Cells("B5").Value = ": " & printIdPasien
    '        ws.Cells("B6").Value = ": " & namapasien
    '        ws.Cells("B7").Value = ": " & alamatpasien
    '        ws.Cells("B7").Style.WrapText = True

    '        ws.Cells("H3").Value = ": " & sex
    '        ws.Cells("H4").Value = ": " & tgllahir
    '        ws.Cells("H5").Value = ": " & age
    '        ws.Cells("H6").Value = ": " & phone
    '        ws.Cells("H7").Value = ": " & datetoprint

    '        If row > 36 And row < 81 Then

    '            Dim cr As CellRange
    '            cr = ws.Cells.GetSubrange("A2", "I10")
    '            cr.CopyTo("A47")

    '            Dim crbottom As CellRange
    '            crbottom = ws.Cells.GetSubrange("A39,I45")
    '            crbottom.CopyTo("A84")
    '        ElseIf row > 81 Then
    '            Dim cr As CellRange
    '            cr = ws.Cells.GetSubrange("A2", "I10")
    '            cr.CopyTo("A47")
    '            cr.CopyTo("A92")

    '            Dim crbottom As CellRange
    '            crbottom = ws.Cells.GetSubrange("A39,I45")
    '            crbottom.CopyTo("A129")

    '        End If

    '        Dim linepos As Integer
    '        linepos = row + 5
    '        Dim crl As CellRange = ws.Cells.GetSubrange("A" & CStr(linepos), "H" & CStr(linepos))

    '        crl.Style.Borders.SetBorders(MultipleBorders.Bottom, SpreadsheetColor.FromName(ColorName.Black), LineStyle.Thin)
    '        ws.Cells("E" & CStr(linepos + 2)).Value = "DiCetak oleh: " & user_complete_name
    '        ws.Cells("E" & CStr(linepos + 3)).Value = "Tanggal: " & Now()
    '        ws.Cells("E" & CStr(linepos + 4)).Value = "Disetujui oleh: "



    '        'If row < 57 Then
    '        '    'Dim cr As CellRange = ws.Cells.GetSubrange("A43", "H43")
    '        '    'cr.Style.Borders.SetBorders(MultipleBorders.Bottom, SpreadsheetColor.FromName(ColorName.Black), LineStyle.Thin)
    '        '    'ws.Cells("E45").Value = "DiCetak oleh: " & user_complete_name
    '        '    'ws.Cells("E46").Value = "Tanggal: " & Now()
    '        '    'ws.Cells("E47").Value = "Disetujui oleh: "
    '        '    Dim linepos As Integer
    '        '    linepos = row + 5
    '        '    Dim cr As CellRange = ws.Cells.GetSubrange("A" & CStr(linepos), "H" & CStr(linepos))

    '        '    cr.Style.Borders.SetBorders(MultipleBorders.Bottom, SpreadsheetColor.FromName(ColorName.Black), LineStyle.Thin)
    '        '    ws.Cells("E" & CStr(linepos + 2)).Value = "DiCetak oleh: " & user_complete_name
    '        '    ws.Cells("E" & CStr(linepos + 3)).Value = "Tanggal: " & Now()
    '        '    ws.Cells("E" & CStr(linepos + 4)).Value = "Disetujui oleh: "

    '        'ElseIf row > 57 Then

    '        '    Dim cr As CellRange = ws.Cells.GetSubrange("A90", "H90")
    '        '    cr.Style.Borders.SetBorders(MultipleBorders.Bottom, SpreadsheetColor.FromName(ColorName.Black), LineStyle.Thin)
    '        '    ws.Cells("E92").Value = "DiCetak oleh: " & user_complete_name
    '        '    ws.Cells("E93").Value = "Tanggal: " & Now()
    '        '    ws.Cells("E94").Value = "Disetujui oleh: "

    '        'ElseIf row > 81 Then
    '        '    Dim cr As CellRange = ws.Cells.GetSubrange("A137", "H137")
    '        '    cr.Style.Borders.SetBorders(MultipleBorders.Bottom, SpreadsheetColor.FromName(ColorName.Black), LineStyle.Thin)
    '        '    ws.Cells("E139").Value = "DiCetak oleh: " & user_complete_name
    '        '    ws.Cells("E140").Value = "Tanggal: " & Now()
    '        '    ws.Cells("E141").Value = "Disetujui oleh: "
    '        'End If



    '        'Dim excResult As String
    '        'Dim excResult As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "patient-result.xlsx")


    '        ef.Save("hasil.xlsx")
    '        Dim xpsDoc = ef.ConvertToXpsDocument(SaveOptions.XpsDefault)
    '        Dim objDocView As New frmDocShowResult
    '        objDocView.StartPosition = FormStartPosition.Manual
    '        objDocView.Left = 0
    '        objDocView.Top = 0
    '        objDocView.ShowDialog()




    '    Else
    '        MessageBox.Show("Silahkan pilih lab number yang hendak di cetak")
    '    End If




    'End Sub

    'Private Sub tsprintnot_RightToLeftChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    'End Sub

    'Private Sub tsprintnot_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If rd30.Checked = True Then
    '        ShowDataDetail(30)
    '    ElseIf rd80.Checked = True Then
    '        ShowDataDetail(80)
    '    Else
    '        ShowDataDetail(10000)
    '    End If
    'End Sub

    'Private Sub tsprintnot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    'End Sub

    'Private Sub AllFinishJobsFrm_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
    '    'ShowDataDetail(30)
    '    rd30.Checked = True
    'End Sub

    'Private Sub rd30_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rd30.CheckedChanged
    '    If rd30.Checked = True Then
    '        ShowDataDetail(30)
    '    End If
    'End Sub

    'Private Sub rd80_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rd80.CheckedChanged
    '    If rd80.Checked = True Then
    '        ShowDataDetail(80)
    '    End If
    'End Sub

    'Private Sub rdsemua_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdsemua.CheckedChanged
    '    If rd80.Checked = True Then
    '        ShowDataDetail(10000)
    '    End If
    'End Sub

    'Private Sub cmdchart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdchart.Click
    '    'If selectedHemaBarcode <> "" Then
    '    If alreadyclick = True Then
    '        Dim objchart As New ChartFrm(selectedHemaBarcode, selectedLabNumber)

    '        objchart.StartPosition = FormStartPosition.Manual
    '        objchart.Left = 0
    '        objchart.Top = 0
    '        objchart.ShowDialog()
    '    End If

    '    'End If
    'End Sub
End Class