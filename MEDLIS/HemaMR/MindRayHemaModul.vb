﻿Module MindRayHemaModul

    Public Const C_MR_MSH As String = "MSH"
    Public Const C_MR_PID As String = "PID"
    Public Const C_MR_OBX As String = "OBX"
    Public Const C_MR_OBR As String = "OBR"
    Public Const C_MR_OBX_NM As String = "NM"
    Public Const C_MR_OBX_ED As String = "ED"
    Public Const C_MR_OBX_IS As String = "IS"

    Public Const C_MR_WBC As String = "WBC"

    Public Const C_MR_NEU_kres As String = "NEU#"
    Public Const C_MR_NEU_persen As String = "NEU%"
    Public Const C_MR_BAS_persen As String = "BAS%"
    Public Const C_MR_BAS_kres As String = "BAS#"
    Public Const C_MR_EOS_kres As String = "EOS#"
    Public Const C_MR_EOS_persen As String = "EOS%"

    'bas
    'neu
    'eos

    Public MR_bloodmessage As New List(Of String)
    '===
    Public iswbcgraf_available As Boolean
    Public ispltgraf_available As Boolean
    Public isrbcgraf_available As Boolean
    Public isscattergraf_available As Boolean


    '===
    Public Const C_MR_LYM_kres As String = "LYM#"
    Public Const C_MR_LYM_persen As String = "LYM%"
    Public Const C_MR_RBC As String = "RBC"
    Public Const C_MR_HGB As String = "HGB"

    Public Const C_MR_MCV As String = "MCV"
    Public Const C_MR_MCH As String = "MCH"
    Public Const C_MR_MCHC As String = "MCHC"
    Public Const C_MR_RDW_CV As String = "RDW-CV"
    Public Const C_MR_RDW_SD As String = "RDW-SD"

    Public Const C_MR_HCT As String = "HCT"
    Public Const C_MR_PLT As String = "PLT"
    Public Const C_MR_MPV As String = "MPV"
    Public Const C_MR_PDW As String = "PDW"
    Public Const C_MR_PCT As String = "PCT"
    Public Const C_MR_MID_kres As String = "MID#"
    Public Const C_MR_MID_persen As String = "MID%"
    Public Const C_MR_GRAN_kres As String = "GRAN#"
    Public Const C_MR_GRAN_persen As String = "GRAN%"
    Public Const C_MR_PLCC As String = "PLCC"
    Public Const C_MR_PLCR As String = "PLCR"

    Public Const C_WBC_Histogram_Meta_Length As String = "WBC Histogram. Meta Length"
    Public Const C_WBC_Histogram_Total As String = "WBC Histogram. Total"
    Public Const C_WBC_Lym_left_line As String = "WBC Lym left line."
    Public Const C_WBC_Lym_Mid_line As String = "WBC Lym Mid line."
    Public Const C_WBC_Mid_Gran_line As String = "WBC Mid Gran line."
    Public Const C_WBC_Histogram_Binary As String = "WBC Histogram. Binary"
    Public Const C_WBC_Histogram_BMP As String = "WBC Histogram. BMP"

    Public Const C_WBC_DIFFscatter_BMP As String = "WBC DIFF Scattergram. BMP"

    Public Const C_RBC_Histogram_Left_Line As String = "RBC Histogram. Left Line"
    Public Const C_RBC_Histogram_Right_Line As String = "RBC Histogram. Right Line"
    Public Const C_RBC_Histogram_Binary_Meta_Length As String = "RBC Histogram. Binary Meta Length"
    Public Const C_RBC_Histogram_Total As String = "RBC Histogram. Total"
    Public Const C_RBC_Histogram_Binary As String = "RBC Histogram. Binary"
    Public Const C_RBC_Histogram_BMP As String = "RBC Histogram. BMP"

    Public Const C_PLT_Histogram_Left_Line As String = "PLT Histogram. Left Line"
    Public Const C_PLT_Histogram_Right_Line As String = "PLT Histogram. Right Line"
    Public Const C_PLT_Histogram_Binary_Meta_Length As String = "PLT Histogram. Binary Meta Length"
    Public Const C_PLT_Histogram_Total As String = "PLT Histogram. Total"
    Public Const C_PLT_Histogram_Binary As String = "PLT Histogram. Binary"
    Public Const C_PLT_Histogram_BMP As String = "PLT Histogram. BMP"

End Module
Public Class MsgData
    Public TestName As String
    Public TestValue As String
    Public TestUnit As String
    Public TestRange As String
    Public TestsFlag As String

End Class




