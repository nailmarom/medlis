﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class hemaAllFinishMindRay
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(hemaAllFinishMindRay))
        Me.dgvJoblist = New System.Windows.Forms.DataGridView
        Me.dgvdetail = New System.Windows.Forms.DataGridView
        Me.lblStatus = New System.Windows.Forms.Label
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.mnuCetak = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.rd30 = New System.Windows.Forms.RadioButton
        Me.rd80 = New System.Windows.Forms.RadioButton
        Me.rdsemua = New System.Windows.Forms.RadioButton
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.cmdchart = New System.Windows.Forms.Button
        Me.dgvmanual = New System.Windows.Forms.DataGridView
        Me.Label1 = New System.Windows.Forms.Label
        Me.pbplt = New System.Windows.Forms.PictureBox
        Me.pbrbc = New System.Windows.Forms.PictureBox
        Me.pbwbc = New System.Windows.Forms.PictureBox
        Me.pbscatter = New System.Windows.Forms.PictureBox
        CType(Me.dgvJoblist, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvdetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvmanual, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbplt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbrbc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbwbc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbscatter, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvJoblist
        '
        Me.dgvJoblist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvJoblist.Location = New System.Drawing.Point(9, 99)
        Me.dgvJoblist.Name = "dgvJoblist"
        Me.dgvJoblist.Size = New System.Drawing.Size(386, 657)
        Me.dgvJoblist.TabIndex = 3
        '
        'dgvdetail
        '
        Me.dgvdetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvdetail.Location = New System.Drawing.Point(401, 99)
        Me.dgvdetail.Name = "dgvdetail"
        Me.dgvdetail.Size = New System.Drawing.Size(527, 657)
        Me.dgvdetail.TabIndex = 15
        '
        'lblStatus
        '
        Me.lblStatus.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(12, 55)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblStatus.Size = New System.Drawing.Size(218, 27)
        Me.lblStatus.TabIndex = 24
        Me.lblStatus.Text = "....."
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCetak, Me.ToolStripSeparator1})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1366, 46)
        Me.ToolStrip1.TabIndex = 33
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'mnuCetak
        '
        Me.mnuCetak.Image = Global.MEDLIS.My.Resources.Resources.print_go_hover
        Me.mnuCetak.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuCetak.Name = "mnuCetak"
        Me.mnuCetak.Size = New System.Drawing.Size(73, 43)
        Me.mnuCetak.Text = "Cetak"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 46)
        '
        'rd30
        '
        Me.rd30.AutoSize = True
        Me.rd30.Location = New System.Drawing.Point(15, 21)
        Me.rd30.Name = "rd30"
        Me.rd30.Size = New System.Drawing.Size(41, 19)
        Me.rd30.TabIndex = 36
        Me.rd30.TabStop = True
        Me.rd30.Text = "30"
        Me.rd30.UseVisualStyleBackColor = True
        '
        'rd80
        '
        Me.rd80.AutoSize = True
        Me.rd80.Location = New System.Drawing.Point(77, 21)
        Me.rd80.Name = "rd80"
        Me.rd80.Size = New System.Drawing.Size(41, 19)
        Me.rd80.TabIndex = 37
        Me.rd80.TabStop = True
        Me.rd80.Text = "80"
        Me.rd80.UseVisualStyleBackColor = True
        '
        'rdsemua
        '
        Me.rdsemua.AutoSize = True
        Me.rdsemua.Location = New System.Drawing.Point(134, 21)
        Me.rdsemua.Name = "rdsemua"
        Me.rdsemua.Size = New System.Drawing.Size(68, 19)
        Me.rdsemua.TabIndex = 38
        Me.rdsemua.TabStop = True
        Me.rdsemua.Text = "semua"
        Me.rdsemua.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rdsemua)
        Me.GroupBox2.Controls.Add(Me.rd80)
        Me.GroupBox2.Controls.Add(Me.rd30)
        Me.GroupBox2.Location = New System.Drawing.Point(236, 46)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(250, 47)
        Me.GroupBox2.TabIndex = 39
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Lab number yang ingin ditampilkan"
        '
        'cmdchart
        '
        Me.cmdchart.Location = New System.Drawing.Point(1228, 483)
        Me.cmdchart.Name = "cmdchart"
        Me.cmdchart.Size = New System.Drawing.Size(126, 41)
        Me.cmdchart.TabIndex = 40
        Me.cmdchart.Text = "Grafik Hematology"
        Me.cmdchart.UseVisualStyleBackColor = True
        '
        'dgvmanual
        '
        Me.dgvmanual.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvmanual.Location = New System.Drawing.Point(939, 536)
        Me.dgvmanual.Name = "dgvmanual"
        Me.dgvmanual.Size = New System.Drawing.Size(425, 165)
        Me.dgvmanual.TabIndex = 44
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Location = New System.Drawing.Point(939, 483)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(281, 17)
        Me.Label1.TabIndex = 45
        Me.Label1.Text = "Test Yang Belum Selesai dari Lab Number"
        '
        'pbplt
        '
        Me.pbplt.Location = New System.Drawing.Point(939, 295)
        Me.pbplt.Name = "pbplt"
        Me.pbplt.Size = New System.Drawing.Size(218, 171)
        Me.pbplt.TabIndex = 43
        Me.pbplt.TabStop = False
        '
        'pbrbc
        '
        Me.pbrbc.Location = New System.Drawing.Point(1181, 99)
        Me.pbrbc.Name = "pbrbc"
        Me.pbrbc.Size = New System.Drawing.Size(218, 171)
        Me.pbrbc.TabIndex = 42
        Me.pbrbc.TabStop = False
        '
        'pbwbc
        '
        Me.pbwbc.Location = New System.Drawing.Point(939, 99)
        Me.pbwbc.Name = "pbwbc"
        Me.pbwbc.Size = New System.Drawing.Size(218, 171)
        Me.pbwbc.TabIndex = 41
        Me.pbwbc.TabStop = False
        '
        'pbscatter
        '
        Me.pbscatter.Location = New System.Drawing.Point(1181, 295)
        Me.pbscatter.Name = "pbscatter"
        Me.pbscatter.Size = New System.Drawing.Size(209, 171)
        Me.pbscatter.TabIndex = 46
        Me.pbscatter.TabStop = False
        '
        'hemaAllFinishMindRay
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1366, 746)
        Me.Controls.Add(Me.pbscatter)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvmanual)
        Me.Controls.Add(Me.pbplt)
        Me.Controls.Add(Me.pbrbc)
        Me.Controls.Add(Me.pbwbc)
        Me.Controls.Add(Me.cmdchart)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.dgvdetail)
        Me.Controls.Add(Me.dgvJoblist)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "hemaAllFinishMindRay"
        Me.Text = "Hema Data Mind Ray"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvJoblist, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvdetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvmanual, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbplt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbrbc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbwbc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbscatter, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvJoblist As System.Windows.Forms.DataGridView
    Friend WithEvents dgvdetail As System.Windows.Forms.DataGridView
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents mnuCetak As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents rd30 As System.Windows.Forms.RadioButton
    Friend WithEvents rd80 As System.Windows.Forms.RadioButton
    Friend WithEvents rdsemua As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdchart As System.Windows.Forms.Button
    Friend WithEvents pbwbc As System.Windows.Forms.PictureBox
    Friend WithEvents pbrbc As System.Windows.Forms.PictureBox
    Friend WithEvents pbplt As System.Windows.Forms.PictureBox
    Friend WithEvents dgvmanual As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pbscatter As System.Windows.Forms.PictureBox
End Class
