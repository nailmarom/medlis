﻿Imports System.Text
Imports System.Text.RegularExpressions

Public Class MindRayBC300Processor
    Private barcode As String

    Public Property Hema_mr_barcode_idtest() As String
        Get
            Return barcode
        End Get
        Set(ByVal value As String)
            barcode = value
        End Set
    End Property
    Private mr_pid As String
    'Public Property Hema_mr_PID() As String
    '    Get
    '        Return mr_pid
    '    End Get
    '    Set(ByVal value As String)
    '        mr_pid = value
    '    End Set
    'End Property
    'OBR|1||12345678|00001^Automated Count^99MRC|||20180301150603|||||||||||||||||HM||||||||Service
    Private mr_date As String
    Public Property Hema_mr_date() As String
        Get
            Return mr_date
        End Get
        Set(ByVal value As String)
            mr_date = value
        End Set
    End Property
    Private mr_wbc As String
    Public Property Hema_mr_WBC() As String
        Get
            Return mr_wbc
        End Get
        Set(ByVal value As String)
            mr_wbc = value
        End Set
    End Property

    Private mr_WBC_value As String
    Public Property Hema_mr_WBC_value() As String
        Get
            Return mr_WBC_value
        End Get
        Set(ByVal value As String)
            mr_WBC_value = value
        End Set
    End Property
    Private mr_WBC_unit As String
    Public Property Hema_mr_WBC_unit() As String
        Get
            Return mr_WBC_unit
        End Get
        Set(ByVal value As String)
            mr_WBC_unit = value
        End Set
    End Property
    Private mr_WBC_refrange As String
    Public Property Hema_mr_WBC_refrange() As String
        Get
            Return mr_WBC_refrange
        End Get
        Set(ByVal value As String)
            mr_WBC_refrange = value
        End Set
    End Property
    Private mr_WBC_abnormal_flag As String
    Public Property Hema_mr_WBC_abnormal_flag() As String
        Get
            Return mr_WBC_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_WBC_abnormal_flag = value
        End Set
    End Property


    Private mr_neu_kres As String
    Public Property Hema_mr_neu_kres() As String
        Get
            Return mr_neu_kres
        End Get
        Set(ByVal value As String)
            mr_neu_kres = value
        End Set
    End Property

    Private mr_neu_kres_value As String
    Public Property Hema_mr_neu_kres_value() As String
        Get
            Return mr_neu_kres_value
        End Get
        Set(ByVal value As String)
            mr_neu_kres_value = value
        End Set
    End Property

    Private mr_neu_kres_unit As String
    Public Property Hema_mr_neu_kres_unit() As String
        Get
            Return mr_neu_kres_unit
        End Get
        Set(ByVal value As String)
            mr_neu_kres_unit = value
        End Set
    End Property

    Private mr_neu_kres_abnormal_flag As String
    Public Property Hema_mr_neu_kres_abnormal_flag() As String
        Get
            Return mr_neu_kres_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_neu_kres_abnormal_flag = value
        End Set
    End Property


    Private mr_neu_kres_refrange As String
    Public Property Hema_mr_neu_kres_refrange() As String
        Get
            Return mr_neu_kres_refrange
        End Get
        Set(ByVal value As String)
            mr_neu_kres_refrange = value
        End Set
    End Property
    Private mr_neu_persen As String
    Public Property Hema_mr_neu_persen() As String
        Get
            Return mr_neu_persen
        End Get
        Set(ByVal value As String)
            mr_neu_persen = value
        End Set
    End Property

    Private mr_neu_persen_value As String
    Public Property Hema_mr_neu_persen_value() As String
        Get
            Return mr_neu_persen_value
        End Get
        Set(ByVal value As String)
            mr_neu_persen_value = value
        End Set
    End Property

    Private mr_neu_persen_unit As String
    Public Property Hema_mr_neu_persen_unit() As String
        Get
            Return mr_neu_persen_unit
        End Get
        Set(ByVal value As String)
            mr_neu_persen_unit = value
        End Set
    End Property

    Private mr_neu_persen_abnormal_flag As String
    Public Property Hema_mr_neu_persen_abnormal_flag() As String
        Get
            Return mr_neu_persen_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_neu_persen_abnormal_flag = value
        End Set
    End Property


    Private mr_neu_persen_refrange As String
    Public Property Hema_mr_neu_persen_refrange() As String
        Get
            Return mr_neu_persen_refrange
        End Get
        Set(ByVal value As String)
            mr_neu_persen_refrange = value
        End Set
    End Property

    '====

    Private mr_bas_persen As String
    Public Property Hema_mr_bas_persen() As String
        Get
            Return mr_bas_persen
        End Get
        Set(ByVal value As String)
            mr_bas_persen = value
        End Set
    End Property

    Private mr_bas_persen_value As String
    Public Property Hema_mr_bas_persen_value() As String
        Get
            Return mr_bas_persen_value
        End Get
        Set(ByVal value As String)
            mr_bas_persen_value = value
        End Set
    End Property

    Private mr_bas_persen_unit As String
    Public Property Hema_mr_bas_persen_unit() As String
        Get
            Return mr_bas_persen_unit
        End Get
        Set(ByVal value As String)
            mr_bas_persen_unit = value
        End Set
    End Property

    Private mr_bas_persen_abnormal_flag As String
    Public Property Hema_mr_bas_persen_abnormal_flag() As String
        Get
            Return mr_bas_persen_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_bas_persen_abnormal_flag = value
        End Set
    End Property


    Private mr_bas_persen_refrange As String
    Public Property Hema_mr_bas_persen_refrange() As String
        Get
            Return mr_bas_persen_refrange
        End Get
        Set(ByVal value As String)
            mr_bas_persen_refrange = value
        End Set
    End Property


    '&&&&&&&&&&&

    Private mr_bas_kres As String
    Public Property Hema_mr_bas_kres() As String
        Get
            Return mr_bas_kres
        End Get
        Set(ByVal value As String)
            mr_bas_kres = value
        End Set
    End Property

    Private mr_bas_kres_value As String
    Public Property Hema_mr_bas_kres_value() As String
        Get
            Return mr_bas_kres_value
        End Get
        Set(ByVal value As String)
            mr_bas_kres_value = value
        End Set
    End Property

    Private mr_bas_kres_unit As String
    Public Property Hema_mr_bas_kres_unit() As String
        Get
            Return mr_bas_kres_unit
        End Get
        Set(ByVal value As String)
            mr_bas_kres_unit = value
        End Set
    End Property

    Private mr_bas_kres_abnormal_flag As String
    Public Property Hema_mr_bas_kres_abnormal_flag() As String
        Get
            Return mr_bas_kres_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_bas_kres_abnormal_flag = value
        End Set
    End Property


    Private mr_bas_kres_refrange As String
    Public Property Hema_mr_bas_kres_refrange() As String
        Get
            Return mr_bas_kres_refrange
        End Get
        Set(ByVal value As String)
            mr_bas_kres_refrange = value
        End Set
    End Property
    '66666666666666666666666

    Private mr_eos_kres As String
    Public Property Hema_mr_eos_kres() As String
        Get
            Return mr_eos_kres
        End Get
        Set(ByVal value As String)
            mr_eos_kres = value
        End Set
    End Property

    Private mr_eos_kres_value As String
    Public Property Hema_mr_eos_kres_value() As String
        Get
            Return mr_eos_kres_value
        End Get
        Set(ByVal value As String)
            mr_eos_kres_value = value
        End Set
    End Property

    Private mr_eos_kres_unit As String
    Public Property Hema_mr_eos_kres_unit() As String
        Get
            Return mr_eos_kres_unit
        End Get
        Set(ByVal value As String)
            mr_eos_kres_unit = value
        End Set
    End Property

    Private mr_eos_kres_abnormal_flag As String
    Public Property Hema_mr_eos_kres_abnormal_flag() As String
        Get
            Return mr_eos_kres_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_eos_kres_abnormal_flag = value
        End Set
    End Property


    Private mr_eos_kres_refrange As String
    Public Property Hema_mr_eos_kres_refrange() As String
        Get
            Return mr_eos_kres_refrange
        End Get
        Set(ByVal value As String)
            mr_eos_kres_refrange = value
        End Set
    End Property

    'KKKKKKKKKK

    Private mr_eos_persen As String
    Public Property Hema_mr_eos_persen() As String
        Get
            Return mr_eos_persen
        End Get
        Set(ByVal value As String)
            mr_eos_persen = value
        End Set
    End Property

    Private mr_eos_persen_value As String
    Public Property Hema_mr_eos_persen_value() As String
        Get
            Return mr_eos_persen_value
        End Get
        Set(ByVal value As String)
            mr_eos_persen_value = value
        End Set
    End Property

    Private mr_eos_persen_unit As String
    Public Property Hema_mr_eos_persen_unit() As String
        Get
            Return mr_eos_persen_unit
        End Get
        Set(ByVal value As String)
            mr_eos_persen_unit = value
        End Set
    End Property

    Private mr_eos_persen_abnormal_flag As String
    Public Property Hema_mr_eos_persen_abnormal_flag() As String
        Get
            Return mr_eos_persen_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_eos_persen_abnormal_flag = value
        End Set
    End Property


    Private mr_eos_persen_refrange As String
    Public Property Hema_mr_eos_persen_refrange() As String
        Get
            Return mr_eos_persen_refrange
        End Get
        Set(ByVal value As String)
            mr_eos_persen_refrange = value
        End Set
    End Property








    '===

    Private mr_HGB As String
    Public Property Hema_mr_HGB() As String
        Get
            Return mr_HGB
        End Get
        Set(ByVal value As String)
            mr_HGB = value
        End Set
    End Property
    Private mr_HGB_value As String
    Public Property Hema_mr_HGB_value() As String
        Get
            Return mr_HGB_value
        End Get
        Set(ByVal value As String)
            mr_HGB_value = value
        End Set
    End Property
    Private mr_HGB_unit As String
    Public Property Hema_mr_HGB_unit() As String
        Get
            Return mr_HGB_unit
        End Get
        Set(ByVal value As String)
            mr_HGB_unit = value
        End Set
    End Property
    Private HGB_refrange As String
    Public Property Hema_mr_HGB_refrange() As String
        Get
            Return HGB_refrange
        End Get
        Set(ByVal value As String)
            HGB_refrange = value
        End Set
    End Property
    Private HGB_abnormal_flag As String
    Public Property Hema_mr_HGB_abnormal_flag() As String
        Get
            Return HGB_abnormal_flag
        End Get
        Set(ByVal value As String)
            HGB_abnormal_flag = value
        End Set
    End Property
    Private LYM_kres As String
    Public Property Hema_mr_LYM_kres() As String
        Get
            Return LYM_kres
        End Get
        Set(ByVal value As String)
            LYM_kres = value
        End Set
    End Property
    Private LYM_kres_value As String
    Public Property Hema_mr_LYM_kres_value() As String
        Get
            Return LYM_kres_value
        End Get
        Set(ByVal value As String)
            LYM_kres_value = value
        End Set
    End Property
    Private LYM_kres_unit As String
    Public Property Hema_mr_LYM_kres_unit() As String
        Get
            Return LYM_kres_unit
        End Get
        Set(ByVal value As String)
            LYM_kres_unit = value
        End Set
    End Property
    Private LYM_kres_refrange As String
    Public Property Hema_mr_LYM_kres_refrange() As String
        Get
            Return LYM_kres_refrange
        End Get
        Set(ByVal value As String)
            LYM_kres_refrange = value
        End Set
    End Property
    Private LYM_kres_abnormal_flag As String
    Public Property Hema_mr_LYM_kres_abnormal_flag() As String
        Get
            Return LYM_kres_abnormal_flag
        End Get
        Set(ByVal value As String)
            LYM_kres_abnormal_flag = value
        End Set
    End Property

    Private mr_LYM_persen As String
    Public Property Hema_mr_LYM_persen() As String
        Get
            Return mr_LYM_persen
        End Get
        Set(ByVal value As String)
            mr_LYM_persen = value
        End Set
    End Property
    Private mr_LYM_persen_value As String
    Public Property Hema_mr_LYM_persen_value() As String
        Get
            Return mr_LYM_persen_value
        End Get
        Set(ByVal value As String)
            mr_LYM_persen_value = value
        End Set
    End Property
    Private mr_LYM_persen_unit As String
    Public Property Hema_mr_LYM_persen_unit() As String
        Get
            Return mr_LYM_persen_unit
        End Get
        Set(ByVal value As String)
            mr_LYM_persen_unit = value
        End Set
    End Property
    Private mr_LYM_persen_refrange As String
    Public Property Hema_mr_LYM_persen_refrange() As String
        Get
            Return mr_LYM_persen_refrange
        End Get
        Set(ByVal value As String)
            mr_LYM_persen_refrange = value
        End Set
    End Property
    Private LYM_persen_abnormal_flag As String
    Public Property Hema_mr_LYM_persen_abnormal_flag() As String
        Get
            Return LYM_persen_abnormal_flag
        End Get
        Set(ByVal value As String)
            LYM_persen_abnormal_flag = value
        End Set
    End Property
    Private mr_RBC As String
    Public Property Hema_mr_RBC() As String
        Get
            Return mr_RBC
        End Get
        Set(ByVal value As String)
            mr_RBC = value
        End Set
    End Property
    Private mr_RBC_value As String
    Public Property Hema_mr_RBC_value() As String
        Get
            Return mr_RBC_value
        End Get
        Set(ByVal value As String)
            mr_RBC_value = value
        End Set
    End Property
    Private mr_RBC_unit As String
    Public Property Hema_mr_RBC_unit() As String
        Get
            Return mr_RBC_unit
        End Get
        Set(ByVal value As String)
            mr_RBC_unit = value
        End Set
    End Property
    Private mr_RBC_refrange As String
    Public Property Hema_mr_RBC_refrange() As String
        Get
            Return mr_RBC_refrange
        End Get
        Set(ByVal value As String)
            mr_RBC_refrange = value
        End Set
    End Property
    Private mr_RBC_abnormal_flag As String
    Public Property Hema_mr_RBC_abnormal_flag() As String
        Get
            Return mr_RBC_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_RBC_abnormal_flag = value
        End Set
    End Property

    Private mr_MCV As String
    Public Property Hema_mr_MCV() As String
        Get
            Return mr_MCV
        End Get
        Set(ByVal value As String)
            mr_MCV = value
        End Set
    End Property
    Private MCV_value As String
    Public Property Hema_mr_MCV_value() As String
        Get
            Return MCV_value
        End Get
        Set(ByVal value As String)
            MCV_value = value
        End Set
    End Property
    Private mr_MCV_unit As String
    Public Property Hema_mr_MCV_unit() As String
        Get
            Return mr_MCV_unit
        End Get
        Set(ByVal value As String)
            mr_MCV_unit = value
        End Set
    End Property
    Private mr_MCV_refrange As String
    Public Property Hema_mr_MCV_refrange() As String
        Get
            Return mr_MCV_refrange
        End Get
        Set(ByVal value As String)
            mr_MCV_refrange = value
        End Set
    End Property
    Private mr_MCV_abnormal_flag As String
    Public Property Hema_mr_MCV_abnormal_flag() As String
        Get
            Return mr_MCV_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_MCV_abnormal_flag = value
        End Set
    End Property
    Private mr_MCH As String
    Public Property Hema_mr_MCH() As String
        Get
            Return mr_MCH
        End Get
        Set(ByVal value As String)
            mr_MCH = value
        End Set
    End Property
    Private mr_MCH_value As String
    Public Property Hema_mr_MCH_value() As String
        Get
            Return mr_MCH_value
        End Get
        Set(ByVal value As String)
            mr_MCH_value = value
        End Set
    End Property
    Private mr_MCH_unit As String
    Public Property Hema_mr_MCH_unit() As String
        Get
            Return mr_MCH_unit
        End Get
        Set(ByVal value As String)
            mr_MCH_unit = value
        End Set
    End Property
    Private mr_MCH_refrange As String
    Public Property Hema_mr_MCH_refrange() As String
        Get
            Return mr_MCH_refrange
        End Get
        Set(ByVal value As String)
            mr_MCH_refrange = value
        End Set
    End Property

    Private mr_MCH_abnormal_flag As String
    Public Property Hema_mr_MCH_abnormal_flag() As String
        Get
            Return mr_MCH_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_MCH_abnormal_flag = value
        End Set
    End Property


    Private mr_MCHC As String
    Public Property Hema_mr_MCHC() As String
        Get
            Return mr_MCHC
        End Get
        Set(ByVal value As String)
            mr_MCHC = value
        End Set
    End Property
    Private MCHC_value As String
    Public Property Hema_mr_MCHC_value() As String
        Get
            Return MCHC_value
        End Get
        Set(ByVal value As String)
            MCHC_value = value
        End Set
    End Property


    Private mr_MCHC_abnormal_flag As String
    Public Property Hema_mr_MCHC_abnormal_flag() As String
        Get
            Return mr_MCHC_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_MCHC_abnormal_flag = value
        End Set
    End Property

    Private mr_MCHC_unit As String
    Public Property Hema_mr_MCHC_unit() As String
        Get
            Return mr_MCHC_unit
        End Get
        Set(ByVal value As String)
            mr_MCHC_unit = value
        End Set
    End Property

    Private mr_MCHC_refrange As String
    Public Property Hema_mr_MCHC_refrange() As String
        Get
            Return mr_MCHC_refrange
        End Get
        Set(ByVal value As String)
            mr_MCHC_refrange = value
        End Set
    End Property
    Private mr_RDW_CV As String
    Public Property Hema_mr_RDW_CV() As String
        Get
            Return mr_RDW_CV
        End Get
        Set(ByVal value As String)
            mr_RDW_CV = value
        End Set
    End Property
    Private mr_RDW_CV_value As String
    Public Property Hema_mr_RDW_CV_value() As String
        Get
            Return mr_RDW_CV_value
        End Get
        Set(ByVal value As String)
            mr_RDW_CV_value = value
        End Set
    End Property
    Private mr_RDW_CV_unit As String
    Public Property Hema_mr_RDW_CV_unit() As String
        Get
            Return mr_RDW_CV_unit
        End Get
        Set(ByVal value As String)
            mr_RDW_CV_unit = value
        End Set
    End Property
    Private mr_RDW_CV_refrange As String
    Public Property Hema_mr_RDW_CV_refrange() As String
        Get
            Return mr_RDW_CV_refrange
        End Get
        Set(ByVal value As String)
            mr_RDW_CV_refrange = value
        End Set
    End Property
    Private RDW_CV_abnormal_flag As String
    Public Property Hema_mr_RDW_CV_abnormal_flag() As String
        Get
            Return RDW_CV_abnormal_flag
        End Get
        Set(ByVal value As String)
            RDW_CV_abnormal_flag = value
        End Set
    End Property

    Private mr_RDW_SD As String
    Public Property Hema_mr_RDW_SD() As String
        Get
            Return mr_RDW_SD
        End Get
        Set(ByVal value As String)
            mr_RDW_SD = value
        End Set
    End Property
    Private mr_RDW_SD_value As String
    Public Property Hema_mr_RDW_SD_value() As String
        Get
            Return mr_RDW_SD_value
        End Get
        Set(ByVal value As String)
            mr_RDW_SD_value = value
        End Set
    End Property
    Private RDW_SD_unit As String
    Public Property Hema_mr_RDW_SD_unit() As String
        Get
            Return RDW_SD_unit
        End Get
        Set(ByVal value As String)
            RDW_SD_unit = value
        End Set
    End Property
    Private mr_RDW_SD_refrange As String
    Public Property Hema_mr_RDW_SD_refrange() As String
        Get
            Return mr_RDW_SD_refrange
        End Get
        Set(ByVal value As String)
            mr_RDW_SD_refrange = value
        End Set
    End Property
    Private mr_RDW_SD_abnormal_flag As String
    Public Property Hema_mr_RDW_SD_abnormal_flag() As String
        Get
            Return mr_RDW_SD_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_RDW_SD_abnormal_flag = value
        End Set
    End Property
    Private mr_HCT As String
    Public Property Hema_mr_HCT() As String
        Get
            Return mr_HCT
        End Get
        Set(ByVal value As String)
            mr_HCT = value
        End Set
    End Property
    Private mr_HCT_value As String
    Public Property Hema_mr_HCT_value() As String
        Get
            Return mr_HCT_value
        End Get
        Set(ByVal value As String)
            mr_HCT_value = value
        End Set
    End Property
    Private mr_HCT_unit As String
    Public Property Hema_mr_HCT_unit() As String
        Get
            Return mr_HCT_unit
        End Get
        Set(ByVal value As String)
            mr_HCT_unit = value
        End Set
    End Property
    Private mr_HCT_refrange As String
    Public Property Hema_mr_HCT_refrange() As String
        Get
            Return mr_HCT_refrange
        End Get
        Set(ByVal value As String)
            mr_HCT_refrange = value
        End Set
    End Property
    Private mr_HCT_abnormal_flag As String
    Public Property Hema_mr_HCT_abnormal_flag() As String
        Get
            Return mr_HCT_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_HCT_abnormal_flag = value
        End Set
    End Property
    Private mr_PLT As String
    Public Property Hema_mr_PLT() As String
        Get
            Return mr_PLT
        End Get
        Set(ByVal value As String)
            mr_PLT = value
        End Set
    End Property

    Private mr_PLT_value As String
    Public Property Hema_mr_PLT_value() As String
        Get
            Return mr_PLT_value
        End Get
        Set(ByVal value As String)
            mr_PLT_value = value
        End Set
    End Property

    Private mr_PLT_unit As String
    Public Property Hema_mr_PLT_unit() As String
        Get
            Return mr_PLT_unit
        End Get
        Set(ByVal value As String)
            mr_PLT_unit = value
        End Set
    End Property

    Private mr_PLT_refrange As String
    Public Property Hema_mr_PLT_refrange() As String
        Get
            Return mr_PLT_refrange
        End Get
        Set(ByVal value As String)
            mr_PLT_refrange = value
        End Set
    End Property

    Private mr_PLT_abnormal_flag As String
    Public Property Hema_mr_PLT_abnormal_flag() As String
        Get
            Return mr_PLT_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_PLT_abnormal_flag = value
        End Set
    End Property

    Private mr_MPV As String
    Public Property Hema_mr_MPV() As String
        Get
            Return mr_MPV
        End Get
        Set(ByVal value As String)
            mr_MPV = value
        End Set
    End Property

    Private mr_MPV_value As String
    Public Property Hema_mr_MPV_value() As String
        Get
            Return mr_MPV_value
        End Get
        Set(ByVal value As String)
            mr_MPV_value = value
        End Set
    End Property

    Private mr_MPV_unit As String
    Public Property Hema_mr_MPV_unit() As String
        Get
            Return mr_MPV_unit
        End Get
        Set(ByVal value As String)
            mr_MPV_unit = value
        End Set
    End Property

    Private mr_mr_MPV_refrange As String
    Public Property Hema_mr_MPV_refrange() As String
        Get
            Return mr_mr_MPV_refrange
        End Get
        Set(ByVal value As String)
            mr_mr_MPV_refrange = value
        End Set
    End Property


    Private mr_MPV_abnormal_flag As String
    Public Property Hema_mr_MPV_abnormal_flag() As String
        Get
            Return mr_MPV_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_MPV_abnormal_flag = value
        End Set
    End Property

    Private mr_PDW As String
    Public Property Hema_mr_PDW() As String
        Get
            Return mr_PDW
        End Get
        Set(ByVal value As String)
            mr_PDW = value
        End Set
    End Property
    Private mr_PDW_value As String
    Public Property Hema_mr_PDW_value() As String
        Get
            Return mr_PDW_value
        End Get
        Set(ByVal value As String)
            mr_PDW_value = value
        End Set
    End Property
    Private mr_PDW_unit As String
    Public Property Hema_mr_PDW_unit() As String
        Get
            Return mr_PDW_unit
        End Get
        Set(ByVal value As String)
            mr_PDW_unit = value
        End Set
    End Property
    Private mr_PDW_refrange As String
    Public Property Hema_mr_PDW_refrange() As String
        Get
            Return mr_PDW_refrange
        End Get
        Set(ByVal value As String)
            mr_PDW_refrange = value
        End Set
    End Property
    Private mr_PDW_abnormal_flag As String
    Public Property Hema_mr_PDW_abnormal_flag() As String
        Get
            Return mr_PDW_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_PDW_abnormal_flag = value
        End Set
    End Property
    Private mr_PCT As String
    Public Property Hema_mr_PCT() As String
        Get
            Return mr_PCT
        End Get
        Set(ByVal value As String)
            mr_PCT = value
        End Set
    End Property
    Private mr_PCT_value As String
    Public Property Hema_mr_PCT_value() As String
        Get
            Return mr_PCT_value
        End Get
        Set(ByVal value As String)
            mr_PCT_value = value
        End Set
    End Property
    Private mr_PCT_unit As String
    Public Property Hema_mr_PCT_unit() As String
        Get
            Return mr_PCT_unit
        End Get
        Set(ByVal value As String)
            mr_PCT_unit = value
        End Set
    End Property
    Private mr_PCT_refrange As String
    Public Property Hema_mr_PCT_refrange() As String
        Get
            Return mr_PCT_refrange
        End Get
        Set(ByVal value As String)
            mr_PCT_refrange = value
        End Set
    End Property
    Private PCT_abnormal_flag As String
    Public Property Hema_mr_PCT_abnormal_flag() As String
        Get
            Return PCT_abnormal_flag
        End Get
        Set(ByVal value As String)
            PCT_abnormal_flag = value
        End Set
    End Property
    Private mr_MID_kres As String
    Public Property Hema_mr_MID_kres() As String
        Get
            Return mr_MID_kres

        End Get
        Set(ByVal value As String)

            mr_MID_kres = value
        End Set
    End Property
    Private mr_MID_kres_value As String
    Public Property Hema_mr_MID_kres_value() As String
        Get
            Return mr_MID_kres_value
        End Get
        Set(ByVal value As String)
            mr_MID_kres_value = value
        End Set
    End Property

    Private MID_kres_unit As String
    Public Property Hema_mr_MID_kres_unit() As String
        Get
            Return MID_kres_unit
        End Get
        Set(ByVal value As String)
            MID_kres_unit = value
        End Set
    End Property

    Private mr_MID_kres_refrange As String
    Public Property Hema_mr_MID_kres_refrange() As String
        Get
            Return mr_MID_kres_refrange
        End Get
        Set(ByVal value As String)
            mr_MID_kres_refrange = value
        End Set
    End Property

    Private mr_MID_kres_abnormal_flag As String
    Public Property Hema_mr_MID_kres_abnormal_flag() As String
        Get
            Return mr_MID_kres_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_MID_kres_abnormal_flag = value
        End Set
    End Property

    Private mr_MID_persen As String
    Public Property Hema_mr_MID_persen() As String
        Get
            Return mr_MID_persen
        End Get
        Set(ByVal value As String)
            mr_MID_persen = value
        End Set
    End Property

    Private mr_MID_persen_value As String
    Public Property Hema_mr_MID_persen_value() As String
        Get
            Return mr_MID_persen_value
        End Get
        Set(ByVal value As String)
            mr_MID_persen_value = value
        End Set
    End Property

    Private mr_MID_persen_unit As String
    Public Property Hema_mr_MID_persen_unit() As String
        Get
            Return mr_MID_persen_unit
        End Get
        Set(ByVal value As String)
            mr_MID_persen_unit = value
        End Set
    End Property
    Private mr_MID_persen_refrange As String
    Public Property Hema_mr_MID_persen_refrange() As String
        Get
            Return mr_MID_persen_refrange
        End Get
        Set(ByVal value As String)
            mr_MID_persen_refrange = value
        End Set
    End Property

    Private mr_MID_persen_abnormal_flag As String
    Public Property Hema_mr_MID_persen_abnormal_flag() As String
        Get
            Return mr_MID_persen_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_MID_persen_abnormal_flag = value
        End Set
    End Property
    Private mr_GRAN_kres As String
    Public Property Hema_mr_GRAN_kres() As String
        Get
            Return mr_GRAN_kres
        End Get
        Set(ByVal value As String)
            mr_GRAN_kres = value
        End Set
    End Property
    Private mr_GRAN_kres_value As String
    Public Property Hema_mr_GRAN_kres_value() As String
        Get
            Return mr_GRAN_kres_value
        End Get
        Set(ByVal value As String)
            mr_GRAN_kres_value = value
        End Set
    End Property
    Private mr_GRAN_kres_unit As String
    Public Property Hema_mr_GRAN_kres_unit() As String
        Get
            Return mr_GRAN_kres_unit
        End Get
        Set(ByVal value As String)
            mr_GRAN_kres_unit = value
        End Set
    End Property
    Private mr_GRAN_kres_refrange As String
    Public Property Hema_mr_GRAN_kres_refrange() As String
        Get
            Return mr_GRAN_kres_refrange
        End Get
        Set(ByVal value As String)
            mr_GRAN_kres_refrange = value
        End Set
    End Property
    Private mr_GRAN_kres_abnormal_flag As String
    Public Property Hema_mr_GRAN_kres_abnormal_flag() As String
        Get
            Return mr_GRAN_kres_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_GRAN_kres_abnormal_flag = value
        End Set
    End Property
    Private mr_GRAN_persen As String
    Public Property Hema_mr_GRAN_persen() As String
        Get
            Return mr_GRAN_persen
        End Get
        Set(ByVal value As String)
            mr_GRAN_persen = value
        End Set
    End Property
    Private mr_GRAN_persen_value As String
    Public Property Hema_mr_GRAN_persen_value() As String
        Get
            Return mr_GRAN_persen_value
        End Get
        Set(ByVal value As String)
            mr_GRAN_persen_value = value
        End Set
    End Property
    Private mr_GRAN_persen_unit As String
    Public Property Hema_mr_GRAN_persen_unit() As String
        Get
            Return mr_GRAN_persen_unit
        End Get
        Set(ByVal value As String)
            mr_GRAN_persen_unit = value
        End Set
    End Property
    Private mr_GRAN_persen_refrange As String
    Public Property Hema_mr_GRAN_persen_refrange() As String
        Get
            Return mr_GRAN_persen_refrange
        End Get
        Set(ByVal value As String)
            mr_GRAN_persen_refrange = value
        End Set
    End Property
    Private mr_GRAN_persen_abnormal_flag As String
    Public Property Hema_mr_GRAN_persen_abnormal_flag() As String
        Get
            Return mr_GRAN_persen_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_GRAN_persen_abnormal_flag = value
        End Set
    End Property
    Private mr_PLCC As String
    Public Property Hema_mr_PLCC() As String
        Get
            Return mr_PLCC
        End Get
        Set(ByVal value As String)
            mr_PLCC = value
        End Set
    End Property
    Private mr_PLCC_value As String
    Public Property Hema_mr_PLCC_value() As String
        Get
            Return mr_PLCC_value
        End Get
        Set(ByVal value As String)
            mr_PLCC_value = value
        End Set
    End Property
    Private mr_PLCC_unit As String
    Public Property Hema_mr_PLCC_unit() As String
        Get
            Return mr_PLCC_unit
        End Get
        Set(ByVal value As String)
            mr_PLCC_unit = value
        End Set
    End Property
    Private mr_PLCC_refrange As String
    Public Property Hema_mr_PLCC_refrange() As String
        Get
            Return mr_PLCC_refrange
        End Get
        Set(ByVal value As String)
            mr_PLCC_refrange = value
        End Set
    End Property
    Private mr_PLCC_abnormal_flag As String
    Public Property Hema_mr_PLCC_abnormal_flag() As String
        Get
            Return mr_PLCC_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_PLCC_abnormal_flag = value
        End Set
    End Property
    Private mr_PLCR As String
    Public Property Hema_mr_PLCR() As String
        Get
            Return mr_PLCR
        End Get
        Set(ByVal value As String)
            mr_PLCR = value
        End Set
    End Property

    Private mr_PLCR_value As String
    Public Property Hema_mr_PLCR_value() As String
        Get
            Return mr_PLCR_value
        End Get
        Set(ByVal value As String)
            mr_PLCR_value = value
        End Set
    End Property
    Private mr_PLCR_unit
    Public Property Hema_mr_PLCR_unit() As String
        Get
            Return mr_PLCR_unit
        End Get
        Set(ByVal value As String)
            mr_PLCR_unit = value
        End Set
    End Property
    Private mr_PLCR_refrange
    Public Property Hema_mr_PLCR_refrange() As String
        Get
            Return mr_PLCR_refrange
        End Get
        Set(ByVal value As String)
            mr_PLCR_refrange = value
        End Set
    End Property
    Private mr_PLCR_abnormal_flag
    Public Property Hema_mr_PLCR_abnormal_flag() As String
        Get
            Return mr_PLCR_abnormal_flag
        End Get
        Set(ByVal value As String)
            mr_PLCR_abnormal_flag = value
        End Set
    End Property



    Private mr_WBC_Histogram_Meta_Length As String
    Public Property Hema_mr_WBC_Histogram_Meta_Length() As String
        Get
            Return mr_WBC_Histogram_Meta_Length
        End Get
        Set(ByVal value As String)
            mr_WBC_Histogram_Meta_Length = value
        End Set
    End Property
    Private mr_WBC_Histogram_Meta_Length_value As String
    Public Property Hema_mr_WBC_Histogram_Meta_Length_value() As String
        Get
            Return mr_WBC_Histogram_Meta_Length_value
        End Get
        Set(ByVal value As String)
            mr_WBC_Histogram_Meta_Length_value = value
        End Set
    End Property
    Private mr_WBC_Histogram_Total As String
    Public Property Hema_mr_WBC_Histogram_Total() As String
        Get
            Return mr_WBC_Histogram_Total
        End Get
        Set(ByVal value As String)
            mr_WBC_Histogram_Total = value
        End Set
    End Property
    Private mr_WBC_Histogram_Total_value As String
    Public Property Hema_mr_WBC_Histogram_Total_value() As String
        Get
            Return mr_WBC_Histogram_Total_value
        End Get
        Set(ByVal value As String)
            mr_WBC_Histogram_Total_value = value
        End Set
    End Property
    Private mr_WBC_Lym_left_line As String
    Public Property Hema_mr_WBC_Lym_left_line() As String
        Get
            Return mr_WBC_Lym_left_line
        End Get
        Set(ByVal value As String)
            mr_WBC_Lym_left_line = value
        End Set
    End Property
    Private mr_WBC_Lym_left_line_value As String
    Public Property Hema_mr_WBC_Lym_left_line_value() As String
        Get
            Return mr_WBC_Lym_left_line_value
        End Get
        Set(ByVal value As String)
            mr_WBC_Lym_left_line_value = value
        End Set
    End Property
    Private mr_WBC_Lym_Mid_line As String
    Public Property Hema_mr_WBC_Lym_Mid_line() As String
        Get
            Return mr_WBC_Lym_Mid_line
        End Get
        Set(ByVal value As String)
            mr_WBC_Lym_Mid_line = value
        End Set
    End Property
    Private mr_WBC_Lym_Mid_line_value As String
    Public Property Hema_mr_WBC_Lym_Mid_line_value() As String
        Get
            Return mr_WBC_Lym_Mid_line_value
        End Get
        Set(ByVal value As String)
            mr_WBC_Lym_Mid_line_value = value
        End Set
    End Property
    Private mr_WBC_Mid_Gran_line As String
    Public Property Hema_mr_WBC_Mid_Gran_line() As String
        Get
            Return mr_WBC_Mid_Gran_line
        End Get
        Set(ByVal value As String)
            mr_WBC_Mid_Gran_line = value
        End Set
    End Property

    Private mr_WBC_Mid_Gran_line_value As String
    Public Property Hema_mr_WBC_Mid_Gran_line_value() As String
        Get
            Return mr_WBC_Mid_Gran_line_value
        End Get
        Set(ByVal value As String)
            mr_WBC_Mid_Gran_line_value = value
        End Set
    End Property

    Private mr_WBC_Histogram_Binary As String
    Public Property Hema_mr_WBC_Histogram_Binary() As String
        Get
            Return mr_WBC_Histogram_Binary
        End Get
        Set(ByVal value As String)
            mr_WBC_Histogram_Binary = value
        End Set
    End Property

    Private mr_WBC_Histogram_Binary_value As String
    Public Property Hema_mr_WBC_Histogram_Binary_value() As String
        Get
            Return mr_WBC_Histogram_Binary_value
        End Get
        Set(ByVal value As String)
            mr_WBC_Histogram_Binary_value = value
        End Set
    End Property
    Private mr_RBC_Histogram_Right_Line As String
    Public Property Hema_mr_RBC_Histogram_Right_Line() As String
        Get
            Return mr_RBC_Histogram_Right_Line
        End Get
        Set(ByVal value As String)
            mr_RBC_Histogram_Right_Line = value
        End Set
    End Property

    Private mr_RBC_Histogram_Right_Line_value As String
    Public Property Hema_mr_RBC_Histogram_Right_Line_value() As String
        Get
            Return mr_RBC_Histogram_Right_Line_value
        End Get
        Set(ByVal value As String)
            mr_RBC_Histogram_Right_Line_value = value
        End Set
    End Property

    Private mr_RBC_Histogram_Left_Line As String
    Public Property Hema_mr_RBC_Histogram_Left_Line() As String
        Get
            Return mr_RBC_Histogram_Left_Line
        End Get
        Set(ByVal value As String)
            mr_RBC_Histogram_Left_Line = value
        End Set
    End Property

    Private mr_RBC_Histogram_Left_Line_value As String
    Public Property Hema_mr_RBC_Histogram_Left_Line_value() As String
        Get
            Return mr_RBC_Histogram_Left_Line_value
        End Get
        Set(ByVal value As String)
            mr_RBC_Histogram_Left_Line_value = value
        End Set
    End Property
    Private mr_RBC_Histogram_Binary_Meta_Length As String
    Public Property Hema_mr_RBC_Histogram_Binary_Meta_Length() As String
        Get
            Return mr_RBC_Histogram_Binary_Meta_Length
        End Get
        Set(ByVal value As String)
            mr_RBC_Histogram_Binary_Meta_Length = value
        End Set
    End Property
    Private mr_RBC_Histogram_Binary_Meta_Length_value As String
    Public Property Hema_mr_RBC_Histogram_Binary_Meta_Length_value() As String
        Get
            Return mr_RBC_Histogram_Binary_Meta_Length_value
        End Get
        Set(ByVal value As String)
            mr_RBC_Histogram_Binary_Meta_Length_value = value
        End Set
    End Property

    Private mr_RBC_Histogram_Total As String
    Public Property Hema_mr_RBC_Histogram_Total() As String
        Get
            Return mr_RBC_Histogram_Total
        End Get
        Set(ByVal value As String)
            mr_RBC_Histogram_Total = value
        End Set
    End Property

    Private mr_RBC_Histogram_Total_value As String
    Public Property Hema_mr_RBC_Histogram_Total_value() As String
        Get
            Return mr_RBC_Histogram_Total_value
        End Get
        Set(ByVal value As String)
            mr_RBC_Histogram_Total_value = value
        End Set
    End Property

    Private mr_RBC_Histogram_Binary As String
    Public Property Hema_mr_RBC_Histogram_Binary() As String
        Get
            Return mr_RBC_Histogram_Binary
        End Get
        Set(ByVal value As String)
            mr_RBC_Histogram_Binary = value
        End Set
    End Property



    Private mr_RBC_Histogram_Binary_value As String
    Public Property Hema_mr_RBC_Histogram_Binary_value() As String
        Get
            Return mr_RBC_Histogram_Binary_value
        End Get
        Set(ByVal value As String)
            mr_RBC_Histogram_Binary_value = value
        End Set
    End Property


    Private mr_PLT_Histogram_Left_Line As String
    Public Property Hema_mr_PLT_Histogram_Left_Line() As String
        Get
            Return mr_PLT_Histogram_Left_Line
        End Get
        Set(ByVal value As String)
            mr_PLT_Histogram_Left_Line = value
        End Set
    End Property

    Private mr_PLT_Histogram_Left_Line_value As String
    Public Property Hema_mr_PLT_Histogram_Left_Line_value() As String
        Get
            Return mr_PLT_Histogram_Left_Line_value
        End Get
        Set(ByVal value As String)
            mr_PLT_Histogram_Left_Line_value = value
        End Set
    End Property

    Private mr_PLT_Histogram_Right_Line As String
    Public Property Hema_mr_PLT_Histogram_Right_Line() As String
        Get
            Return mr_PLT_Histogram_Right_Line
        End Get
        Set(ByVal value As String)
            mr_PLT_Histogram_Right_Line = value
        End Set
    End Property

    Private mr_PLT_Histogram_Right_Line_value As String
    Public Property Hema_mr_PLT_Histogram_Right_Line_value() As String
        Get
            Return mr_PLT_Histogram_Right_Line_value
        End Get
        Set(ByVal value As String)
            mr_PLT_Histogram_Right_Line_value = value
        End Set
    End Property

    Private mr_PLT_Histogram_Binary_Meta_Length As String
    Public Property Hema_mr_PLT_Histogram_Binary_Meta_Length() As String
        Get
            Return mr_PLT_Histogram_Binary_Meta_Length
        End Get
        Set(ByVal value As String)
            mr_PLT_Histogram_Binary_Meta_Length = value
        End Set
    End Property

    Private mr_PLT_Histogram_Binary_Meta_Length_value As String
    Public Property Hema_mr_PLT_Histogram_Binary_Meta_Length_value() As String
        Get
            Return mr_PLT_Histogram_Binary_Meta_Length_value
        End Get
        Set(ByVal value As String)
            mr_PLT_Histogram_Binary_Meta_Length_value = value
        End Set
    End Property

    Private mr_PLT_Histogram_Total As String
    Public Property Hema_mr_PLT_Histogram_Total() As String
        Get
            Return mr_PLT_Histogram_Total
        End Get
        Set(ByVal value As String)
            mr_PLT_Histogram_Total = value
        End Set
    End Property

    Private mr_PLT_Histogram_Total_value As String
    Public Property Hema_mr_PLT_Histogram_Total_value() As String
        Get
            Return mr_PLT_Histogram_Total_value
        End Get
        Set(ByVal value As String)
            mr_PLT_Histogram_Total_value = value
        End Set
    End Property

    Private mr_PLT_Histogram_Binary As String
    Public Property Hema_mr_PLT_Histogram_Binary() As String
        Get
            Return mr_PLT_Histogram_Binary
        End Get
        Set(ByVal value As String)
            mr_PLT_Histogram_Binary = value
        End Set
    End Property
    Private mr_PLT_Histogram_Binary_value As String
    Public Property Hema_mr_PLT_Histogram_Binary_value() As String
        Get
            Return mr_PLT_Histogram_Binary_value

        End Get
        Set(ByVal value As String)
            mr_PLT_Histogram_Binary_value = value
        End Set
    End Property

    Private mr_wbc_histogram_bmp As String

    Public Property Hema_mr_wbc_histogram_bmp() As String
        Get
            Return mr_wbc_histogram_bmp
        End Get
        Set(ByVal value As String)
            mr_wbc_histogram_bmp = value
        End Set
    End Property
    Private mr_wbc_histogram_bmp_value As String
    Public Property Hema_mr_wbc_histogram_bmp_value() As String
        Get
            Return mr_wbc_histogram_bmp_value
        End Get
        Set(ByVal value As String)
            mr_wbc_histogram_bmp_value = value
        End Set
    End Property

    Private mr_rbc_histogram_bmp As String
    Public Property Hema_mr_rbc_histogram_bmp() As String
        Get
            Return mr_rbc_histogram_bmp
        End Get
        Set(ByVal value As String)
            mr_rbc_histogram_bmp = value
        End Set
    End Property

    Private mr_rbc_histogram_bmp_value As String
    Public Property Hema_mr_rbc_histogram_bmp_value() As String
        Get
            Return mr_rbc_histogram_bmp_value
        End Get
        Set(ByVal value As String)
            mr_rbc_histogram_bmp_value = value
        End Set
    End Property

    Private mr_plt_histogram_bmp As String
    Public Property Hema_mr_plt_histogram_bmp() As String
        Get
            Return mr_plt_histogram_bmp
        End Get
        Set(ByVal value As String)
            mr_plt_histogram_bmp = value
        End Set
    End Property
    Private mr_plt_histogram_bmp_value As String
    Public Property Hema_mr_plt_histogram_bmp_value() As String
        Get
            Return mr_plt_histogram_bmp_value
        End Get
        Set(ByVal value As String)
            mr_plt_histogram_bmp_value = value
        End Set
    End Property


    Public Function IdentifySingleMessage(ByVal strsingle As String) As String
        Try
            Dim result As String
            result = ""
            Dim regex_obx As String()
            Dim regeksingle As RegularExpressions.Regex

            Dim regexResult As String()
            regexResult = Regex.Split(strsingle, "\|")

            If regexResult.ElementAt(0) = C_MR_MSH Then ' msh message
                Return C_MR_MSH
            ElseIf regexResult.ElementAt(0).Trim = C_MR_OBX Then
                Return C_MR_OBX
            ElseIf regexResult.ElementAt(0).Trim = C_MR_OBR Then
                'OBR|1||12345678|00001^Automated Count^99MRC|||20180301150603|||||||||||||||||HM||||||||Service
                Return C_MR_OBR
            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try


    End Function
    Public Function Identify_OBR_get_barcode_idtest(ByVal strobx As String) As String
        Dim result As String
        Dim regexResult As String()
        regexResult = Regex.Split(strobx, "\|")
        'OBR|1||12345678|00001^Automated Count^99MRC|||20180301150603|||||||||||||||||HM||||||||Service
        If Trim(regexResult.ElementAt(3)) <> "" Then
            result = UCase(regexResult.ElementAt(3).Trim)
        Else
            result = ""
        End If
        Return result
    End Function
    Public Function Identify_OBX(ByVal strobx As String) As MsgData
        Try
            Dim result_test As String = ""
            Dim result_value As String = ""

            Dim result_unit As String = ""
            Dim result_refrange As String = ""
            Dim result_flag As String = ""

            Dim regex_obx_nm As String()
            Dim regex_obx_ed As String()


            Dim regexResult As String()
            regexResult = Regex.Split(strobx, "\|")

            If Trim(regexResult.ElementAt(2)) = C_MR_OBX_NM Then
                regex_obx_nm = Regex.Split(regexResult.ElementAt(3), "\^")
                If regex_obx_nm.Count > 1 Then
                    If Trim(regex_obx_nm.ElementAt(1)) = C_MR_WBC Then
                        result_test = C_MR_WBC
                        result_value = regexResult.ElementAt(5)
                        result_unit = regexResult.ElementAt(6)
                        result_refrange = regexResult.ElementAt(7)
                        result_flag = regexResult.ElementAt(8)

                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_HGB Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                        '===========

                        '========
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_LYM_kres Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If

                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_LYM_persen Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                        '==========
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_BAS_persen Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If

                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_BAS_kres Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_NEU_persen Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If

                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_NEU_kres Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If

                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_EOS_kres Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If

                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_EOS_persen Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If

                        '==========
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_RBC Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_MCV Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If

                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_MCH Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_MCHC Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If

                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_RDW_CV Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_RDW_SD Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_HCT Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_PLT Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_MPV Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_PDW Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_PCT Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_MID_kres Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_MID_persen Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_GRAN_kres Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_GRAN_persen Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_PLCC Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_MR_PLCR Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_WBC_Histogram_Meta_Length Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        result_value = regexResult.ElementAt(5)
                        result_unit = ""
                        result_refrange = ""
                        result_flag = ""
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_WBC_Histogram_Total Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_WBC_Lym_left_line Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_WBC_Lym_Mid_line Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_WBC_Mid_Gran_line Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_WBC_Histogram_Binary Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_RBC_Histogram_Left_Line Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_RBC_Histogram_Right_Line Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_RBC_Histogram_Binary_Meta_Length Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        Else
                            result_value = ""
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_RBC_Histogram_Total Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If

                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_PLT_Histogram_Left_Line Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_PLT_Histogram_Right_Line Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_PLT_Histogram_Binary_Meta_Length Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_PLT_Histogram_Total Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    ElseIf Trim(regex_obx_nm.ElementAt(1)) = C_PLT_Histogram_Binary Then
                        result_test = Trim(regex_obx_nm.ElementAt(1))
                        If regexResult.Length > 5 Then
                            result_value = regexResult.ElementAt(5)
                        End If

                        If regexResult.Length > 6 Then
                            result_unit = regexResult.ElementAt(6)
                        Else
                            result_unit = ""
                        End If
                        If regexResult.Length > 7 Then
                            result_refrange = regexResult.ElementAt(7)
                        Else
                            result_refrange = ""
                        End If
                        If regexResult.Length > 8 Then
                            result_flag = regexResult.ElementAt(8)
                        Else
                            result_flag = ""
                        End If
                    End If
                End If

            ElseIf Trim(regexResult.ElementAt(2)) = C_MR_OBX_ED Then
                '=create grafik, not work as there is no bmp
                'regex_obx_ed = Regex.Split(regexResult.ElementAt(3), "\^")
                'If Trim(regex_obx_ed.ElementAt(1)) = C_WBC_Histogram_BMP Then
                '    result_test = Trim(regex_obx_ed.ElementAt(1))
                '    Dim regex_obx_bmp_wbc As String()
                '    regex_obx_bmp_wbc = Regex.Split(regexResult.ElementAt(5), "\^")
                '    result_value = regex_obx_bmp_wbc.ElementAt(4)

                'End If
                'If Trim(regex_obx_ed.ElementAt(1)) = C_RBC_Histogram_BMP Then

                '    result_test = Trim(regex_obx_ed.ElementAt(1))
                '    Dim regex_obx_bmp_rbc As String()

                '    regex_obx_bmp_rbc = Regex.Split(regexResult.ElementAt(5), "\^")
                '    result_value = regex_obx_bmp_rbc.ElementAt(4)

                'End If
                'If Trim(regex_obx_ed.ElementAt(1)) = C_PLT_Histogram_BMP Then
                '    result_test = Trim(regex_obx_ed.ElementAt(1))
                '    Dim regex_obx_bmp_plt As String()
                '    regex_obx_bmp_plt = Regex.Split(regexResult.ElementAt(5), "\^")
                '    result_value = regex_obx_bmp_plt.ElementAt(4)

                'End If



            End If
            Return New MsgData() With {.TestName = result_test, .TestValue = result_value, .TestUnit = result_unit, .TestRange = result_refrange, .TestsFlag = result_flag}

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "MindRayBC3600Processor/Identify_OBX")
        End Try
        
    End Function
    Public Sub Fill_and_do(ByVal data As List(Of String))
        Dim i As Integer
        For i = 0 To data.Count - 1

            If Trim(IdentifySingleMessage(data(i))) = C_MR_OBX Then
                Dim obj_msgdata As New MsgData
                obj_msgdata = Identify_OBX(data(i))

                If Trim(obj_msgdata.TestName) = C_MR_WBC Then
                    Hema_mr_WBC = obj_msgdata.TestName
                    Hema_mr_WBC_value = obj_msgdata.TestValue
                    Hema_mr_WBC_unit = obj_msgdata.TestUnit
                    Hema_mr_WBC_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_WBC_refrange = obj_msgdata.TestRange
                End If

                '==============
                If Trim(obj_msgdata.TestName) = C_MR_LYM_kres Then
                    Hema_mr_LYM_kres = obj_msgdata.TestName
                    Hema_mr_LYM_kres_value = obj_msgdata.TestValue
                    Hema_mr_LYM_kres_unit = obj_msgdata.TestUnit
                    Hema_mr_LYM_kres_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_LYM_kres_refrange = obj_msgdata.TestRange
                End If
                If Trim(obj_msgdata.TestName) = C_MR_LYM_persen Then
                    Hema_mr_LYM_persen = obj_msgdata.TestName
                    Hema_mr_LYM_persen_value = obj_msgdata.TestValue
                    Hema_mr_LYM_persen_unit = obj_msgdata.TestUnit
                    Hema_mr_LYM_persen_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_LYM_persen_refrange = obj_msgdata.TestRange
                End If
                If Trim(obj_msgdata.TestName) = C_MR_RBC Then
                    Hema_mr_RBC = obj_msgdata.TestName
                    Hema_mr_RBC_value = obj_msgdata.TestValue
                    Hema_mr_RBC_unit = obj_msgdata.TestUnit
                    Hema_mr_RBC_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_RBC_refrange = obj_msgdata.TestRange
                End If

                '==============

                If Trim(obj_msgdata.TestName) = C_MR_BAS_kres Then
                    Hema_mr_bas_kres = obj_msgdata.TestName
                    Hema_mr_bas_kres_value = obj_msgdata.TestValue
                    Hema_mr_bas_kres_unit = obj_msgdata.TestUnit
                    Hema_mr_bas_kres_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_bas_kres_refrange = obj_msgdata.TestRange
                End If


                If Trim(obj_msgdata.TestName) = C_MR_BAS_persen Then
                    Hema_mr_bas_persen = obj_msgdata.TestName
                    Hema_mr_bas_persen_value = obj_msgdata.TestValue
                    Hema_mr_bas_persen_unit = obj_msgdata.TestUnit
                    Hema_mr_bas_persen_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_bas_persen_refrange = obj_msgdata.TestRange
                End If

                If Trim(obj_msgdata.TestName) = C_MR_NEU_persen Then
                    Hema_mr_neu_persen = obj_msgdata.TestName
                    Hema_mr_neu_persen_value = obj_msgdata.TestValue
                    Hema_mr_neu_persen_unit = obj_msgdata.TestUnit
                    Hema_mr_neu_persen_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_neu_persen_refrange = obj_msgdata.TestRange
                End If

                If Trim(obj_msgdata.TestName) = C_MR_NEU_kres Then
                    Hema_mr_neu_kres = obj_msgdata.TestName
                    Hema_mr_neu_kres_value = obj_msgdata.TestValue
                    Hema_mr_neu_kres_unit = obj_msgdata.TestUnit
                    Hema_mr_neu_kres_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_neu_kres_refrange = obj_msgdata.TestRange
                End If


                If Trim(obj_msgdata.TestName) = C_MR_EOS_kres Then
                    Hema_mr_eos_kres = obj_msgdata.TestName
                    Hema_mr_eos_kres_value = obj_msgdata.TestValue
                    Hema_mr_eos_kres_unit = obj_msgdata.TestUnit
                    Hema_mr_eos_kres_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_eos_kres_refrange = obj_msgdata.TestRange
                End If

                If Trim(obj_msgdata.TestName) = C_MR_EOS_persen Then
                    Hema_mr_eos_persen = obj_msgdata.TestName
                    Hema_mr_eos_persen_value = obj_msgdata.TestValue
                    Hema_mr_eos_persen_unit = obj_msgdata.TestUnit
                    Hema_mr_eos_persen_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_eos_persen_refrange = obj_msgdata.TestRange
                End If

                If Trim(obj_msgdata.TestName) = C_MR_HGB Then
                    Hema_mr_HGB = obj_msgdata.TestName
                    Hema_mr_HGB_value = obj_msgdata.TestValue
                    Hema_mr_HGB_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_HGB_refrange = obj_msgdata.TestRange
                    Hema_mr_HGB_unit = obj_msgdata.TestUnit

                End If

                If Trim(obj_msgdata.TestName) = C_MR_MCV Then
                    Hema_mr_MCV = obj_msgdata.TestName
                    Hema_mr_MCV_value = obj_msgdata.TestValue
                    Hema_mr_MCV_unit = obj_msgdata.TestUnit
                    Hema_mr_MCV_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_MCV_refrange = obj_msgdata.TestRange
                End If

                If Trim(obj_msgdata.TestName) = C_MR_MCH Then
                    Hema_mr_MCH = obj_msgdata.TestName
                    Hema_mr_MCH_value = obj_msgdata.TestValue
                    Hema_mr_MCH_unit = obj_msgdata.TestUnit
                    Hema_mr_MCH_refrange = obj_msgdata.TestRange
                    Hema_mr_MCH_abnormal_flag = obj_msgdata.TestsFlag
                End If

                If Trim(obj_msgdata.TestName) = C_MR_MCHC Then
                    Hema_mr_MCHC = obj_msgdata.TestName
                    Hema_mr_MCHC_value = obj_msgdata.TestValue
                    Hema_mr_MCHC_unit = obj_msgdata.TestUnit
                    Hema_mr_MCHC_refrange = obj_msgdata.TestRange
                    Hema_mr_MCHC_abnormal_flag = obj_msgdata.TestsFlag
                End If

                If Trim(obj_msgdata.TestName) = C_MR_RDW_CV Then
                    Hema_mr_RDW_CV = obj_msgdata.TestName
                    Hema_mr_RDW_CV_value = obj_msgdata.TestValue
                    Hema_mr_RDW_CV_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_RDW_CV_refrange = obj_msgdata.TestRange
                    Hema_mr_RDW_CV_unit = obj_msgdata.TestUnit
                End If

                If Trim(obj_msgdata.TestName) = C_MR_RDW_SD Then
                    Hema_mr_RDW_SD = obj_msgdata.TestName
                    Hema_mr_RDW_SD_value = obj_msgdata.TestValue
                    Hema_mr_RDW_SD_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_RDW_SD_unit = obj_msgdata.TestUnit
                    Hema_mr_RDW_SD_refrange = obj_msgdata.TestRange
                End If

                If Trim(obj_msgdata.TestName) = C_MR_HCT Then
                    Hema_mr_HCT = obj_msgdata.TestName
                    Hema_mr_HCT_value = obj_msgdata.TestValue
                    Hema_mr_HCT_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_HCT_unit = obj_msgdata.TestUnit
                    Hema_mr_HCT_refrange = obj_msgdata.TestRange
                End If
                If Trim(obj_msgdata.TestName) = C_MR_PLT Then
                    Hema_mr_PLT = obj_msgdata.TestName
                    Hema_mr_PLT_value = obj_msgdata.TestValue
                    Hema_mr_PLT_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_PLT_unit = obj_msgdata.TestUnit
                    Hema_mr_PLT_refrange = obj_msgdata.TestRange
                End If
                If Trim(obj_msgdata.TestName) = C_MR_MPV Then
                    Hema_mr_MPV = obj_msgdata.TestName
                    Hema_mr_MPV_value = obj_msgdata.TestValue
                    Hema_mr_MPV_unit = obj_msgdata.TestUnit
                    Hema_mr_MPV_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_MPV_refrange = obj_msgdata.TestRange
                End If
                If Trim(obj_msgdata.TestName) = C_MR_PDW Then
                    Hema_mr_PDW = obj_msgdata.TestName
                    Hema_mr_PDW_value = obj_msgdata.TestValue
                    Hema_mr_PDW_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_PDW_refrange = obj_msgdata.TestRange
                    Hema_mr_PDW_unit = obj_msgdata.TestUnit
                End If
                If Trim(obj_msgdata.TestName) = C_MR_PCT Then
                    Hema_mr_PCT = obj_msgdata.TestName
                    Hema_mr_PCT_value = obj_msgdata.TestValue
                    Hema_mr_PCT_unit = obj_msgdata.TestUnit
                    Hema_mr_PCT_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_PCT_refrange = obj_msgdata.TestRange
                End If

                If Trim(obj_msgdata.TestName) = C_MR_MID_kres Then
                    Hema_mr_MID_kres = obj_msgdata.TestName
                    Hema_mr_MID_kres_value = obj_msgdata.TestValue
                    Hema_mr_MID_kres_unit = obj_msgdata.TestUnit
                    Hema_mr_MID_kres_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_MID_kres_refrange = obj_msgdata.TestRange
                End If

                If Trim(obj_msgdata.TestName) = C_MR_MID_persen Then
                    Hema_mr_MID_persen = obj_msgdata.TestName
                    Hema_mr_MID_persen_value = obj_msgdata.TestValue
                    Hema_mr_MID_persen_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_MID_persen_unit = obj_msgdata.TestUnit
                    Hema_mr_MID_persen_refrange = obj_msgdata.TestRange
                End If

                If Trim(obj_msgdata.TestName) = C_MR_GRAN_kres Then
                    Hema_mr_GRAN_kres = obj_msgdata.TestName
                    Hema_mr_GRAN_kres_value = obj_msgdata.TestValue
                    Hema_mr_GRAN_kres_unit = obj_msgdata.TestUnit
                    Hema_mr_GRAN_kres_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_GRAN_kres_refrange = obj_msgdata.TestRange
                End If

                If Trim(obj_msgdata.TestName) = C_MR_GRAN_persen Then
                    Hema_mr_GRAN_persen = obj_msgdata.TestName
                    Hema_mr_GRAN_persen_value = obj_msgdata.TestValue
                    Hema_mr_GRAN_persen_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_GRAN_persen_refrange = obj_msgdata.TestRange
                    Hema_mr_GRAN_persen_unit = obj_msgdata.TestUnit
                End If

                If Trim(obj_msgdata.TestName) = C_MR_PLCC Then
                    Hema_mr_PLCC = obj_msgdata.TestName
                    Hema_mr_PLCC_value = obj_msgdata.TestValue
                    Hema_mr_PLCC_refrange = obj_msgdata.TestRange
                    Hema_mr_PLCC_unit = obj_msgdata.TestUnit
                    Hema_mr_PLCC_abnormal_flag = obj_msgdata.TestsFlag
                End If

                If Trim(obj_msgdata.TestName) = C_MR_PLCR Then
                    Hema_mr_PLCR = obj_msgdata.TestName
                    Hema_mr_PLCR_value = obj_msgdata.TestValue
                    Hema_mr_PLCR_unit = obj_msgdata.TestUnit
                    Hema_mr_PLCR_abnormal_flag = obj_msgdata.TestsFlag
                    Hema_mr_PLCR_refrange = obj_msgdata.TestRange
                End If

                If Trim(obj_msgdata.TestName) = C_WBC_Histogram_Meta_Length Then
                    Hema_mr_WBC_Histogram_Meta_Length = obj_msgdata.TestName
                    Hema_mr_WBC_Histogram_Meta_Length_value = obj_msgdata.TestValue

                End If

                If Trim(obj_msgdata.TestName) = C_WBC_Histogram_Total Then
                    Hema_mr_WBC_Histogram_Total = obj_msgdata.TestName
                    Hema_mr_WBC_Histogram_Total_value = obj_msgdata.TestValue

                End If

                If Trim(obj_msgdata.TestName) = C_WBC_Lym_left_line Then
                    Hema_mr_WBC_Lym_left_line = obj_msgdata.TestName
                    Hema_mr_WBC_Lym_left_line_value = obj_msgdata.TestValue
                End If

                If Trim(obj_msgdata.TestName) = C_WBC_Lym_Mid_line Then
                    Hema_mr_WBC_Lym_Mid_line = obj_msgdata.TestName
                    Hema_mr_WBC_Lym_Mid_line = obj_msgdata.TestValue
                End If

                If Trim(obj_msgdata.TestName) = C_WBC_Mid_Gran_line Then
                    Hema_mr_WBC_Mid_Gran_line = obj_msgdata.TestName
                    Hema_mr_WBC_Mid_Gran_line_value = obj_msgdata.TestValue
                End If

                If Trim(obj_msgdata.TestName) = C_WBC_Histogram_Binary Then
                    Hema_mr_WBC_Histogram_Binary = obj_msgdata.TestName
                    Hema_mr_WBC_Histogram_Binary_value = obj_msgdata.TestValue
                End If

                If Trim(obj_msgdata.TestName) = C_RBC_Histogram_Left_Line Then
                    Hema_mr_RBC_Histogram_Left_Line = obj_msgdata.TestName
                    Hema_mr_RBC_Histogram_Left_Line_value = obj_msgdata.TestValue
                End If

                If Trim(obj_msgdata.TestName) = C_RBC_Histogram_Right_Line Then
                    Hema_mr_RBC_Histogram_Right_Line = obj_msgdata.TestName
                    Hema_mr_RBC_Histogram_Right_Line_value = obj_msgdata.TestValue
                End If

                If Trim(obj_msgdata.TestName) = C_RBC_Histogram_Binary_Meta_Length Then
                    Hema_mr_RBC_Histogram_Binary = obj_msgdata.TestName
                    Hema_mr_RBC_Histogram_Binary_Meta_Length_value = obj_msgdata.TestValue
                End If

                If Trim(obj_msgdata.TestName) = C_RBC_Histogram_Total Then
                    Hema_mr_RBC_Histogram_Total = obj_msgdata.TestName
                    Hema_mr_RBC_Histogram_Total_value = obj_msgdata.TestValue
                End If

                If Trim(obj_msgdata.TestName) = C_RBC_Histogram_Binary Then
                    Hema_mr_RBC_Histogram_Binary = obj_msgdata.TestName
                    Hema_mr_RBC_Histogram_Binary_value = obj_msgdata.TestValue
                End If

                If Trim(obj_msgdata.TestName) = C_PLT_Histogram_Left_Line Then
                    Hema_mr_PLT_Histogram_Left_Line = obj_msgdata.TestName
                    Hema_mr_PLT_Histogram_Left_Line_value = obj_msgdata.TestValue
                End If

                If Trim(obj_msgdata.TestName) = C_PLT_Histogram_Right_Line Then
                    Hema_mr_PLT_Histogram_Right_Line = obj_msgdata.TestName
                    Hema_mr_PLT_Histogram_Right_Line_value = obj_msgdata.TestValue
                End If

                If Trim(obj_msgdata.TestName) = C_PLT_Histogram_Binary_Meta_Length Then
                    Hema_mr_PLT_Histogram_Binary_Meta_Length = obj_msgdata.TestName
                    Hema_mr_PLT_Histogram_Binary_Meta_Length_value = obj_msgdata.TestValue
                End If

                If Trim(obj_msgdata.TestName) = C_PLT_Histogram_Total Then
                    Hema_mr_PLT_Histogram_Total = obj_msgdata.TestName
                    Hema_mr_PLT_Histogram_Total_value = obj_msgdata.TestValue
                End If

                If Trim(obj_msgdata.TestName) = C_PLT_Histogram_Binary Then
                    Hema_mr_PLT_Histogram_Binary = obj_msgdata.TestName
                    Hema_mr_PLT_Histogram_Binary_value = obj_msgdata.TestValue
                End If

                If Trim(obj_msgdata.TestName) = C_WBC_Histogram_BMP Then
                    Hema_mr_wbc_histogram_bmp = obj_msgdata.TestName
                    Hema_mr_wbc_histogram_bmp_value = obj_msgdata.TestValue
                End If
                If Trim(obj_msgdata.TestName) = C_RBC_Histogram_BMP Then
                    Hema_mr_rbc_histogram_bmp = obj_msgdata.TestName
                    Hema_mr_rbc_histogram_bmp_value = obj_msgdata.TestValue
                End If

                If Trim(obj_msgdata.TestName) = C_PLT_Histogram_BMP Then
                    Hema_mr_plt_histogram_bmp = obj_msgdata.TestName
                    Hema_mr_plt_histogram_bmp_value = obj_msgdata.TestValue
                End If



            ElseIf Trim(IdentifySingleMessage(data(i))) = C_MR_OBR Then
                Hema_mr_barcode_idtest = Identify_OBR_get_barcode_idtest(data(i))
                'ElseIf Trim(IdentifySingleMessage(data(i))) = C_MR_OBR Then


            End If
        Next
        ''

    End Sub

    Public Function findBloodMessage(ByVal data As List(Of String)) As String
        Dim j As Integer
        Dim result As String
        result = ""
        MR_bloodmessage = New List(Of String)
        MR_bloodmessage.Add("Neutrophilia")

        Dim i As Integer
        For i = 0 To data.Count - 1
            If data.Item(i).Contains(C_MR_OBX) Then
                If data.Item(i).Contains(C_MR_OBX_IS) Then
                    For j = 0 To MR_bloodmessage.Count - 1
                        If data.Item(i).Contains(MR_bloodmessage.Item(j)) Then
                            result = MR_bloodmessage.Item(j)
                        End If
                    Next
                End If
            End If
        Next
        Return result
    End Function


End Class
