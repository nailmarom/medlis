﻿Public Class MRBloodMessageSetting

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GetBloodDefaultMessage()
    End Sub

    Private Sub GetBloodDefaultMessage()
        If My.Settings.bloodmessage Is Nothing Then
            My.Settings.bloodmessage = New System.Collections.Specialized.StringCollection()
        End If

        Dim content As String = My.Resources.BloodMessageTxt
        Dim strperline() As String
        ' TextBox1.Text = content
        If My.Settings.bloodmessage.Count < 1 Then
            strperline = content.Split(vbCrLf)
            For Each item As String In strperline
                My.Settings.bloodmessage.Add(item)
            Next
            For Each blg1 As String In My.Settings.bloodmessage
                TextBox1.Text = TextBox1.Text & blg1 & vbCrLf
            Next
        Else
            For Each blg As String In My.Settings.bloodmessage
                TextBox1.Text = TextBox1.Text & blg & vbCrLf
            Next
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim strperline() As String
        strperline = TextBox1.Text.Split(vbCrLf)
        My.Settings.bloodmessage.Clear()

        For Each item As String In strperline
            If Trim(item) <> "" Then
                My.Settings.bloodmessage.Add(Trim(item))
            End If
        Next


        If developerMode = 1 Then
            ListBox1.Visible = True

            For Each item As String In My.Settings.bloodmessage
                ListBox1.Items.Add(item)
            Next
        End If

    End Sub

    Private Sub MRBloodMessageSetting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ListBox1.Visible = False
        Icon = New System.Drawing.Icon("medlis2.ico")
      
    End Sub
End Class