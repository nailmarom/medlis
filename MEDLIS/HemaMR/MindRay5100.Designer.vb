﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MindRay5100
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MindRay5100))
        Me.txtipaddress = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmdConnect = New System.Windows.Forms.Button
        Me.txtport = New System.Windows.Forms.TextBox
        Me.Button3 = New System.Windows.Forms.Button
        Me.tmrresult = New System.Windows.Forms.Timer(Me.components)
        Me.Label2 = New System.Windows.Forms.Label
        Me.wbctext = New System.Windows.Forms.TextBox
        Me.lymkrestext = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.lympersentext = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.rbctext = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.hgbtext = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.mcvtext = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.mchtext = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.mchctext = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.rdwcvtext = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.rdwsdtext = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.hcttext = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.plttext = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.mpvtext = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.pdwtext = New System.Windows.Forms.TextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.pcttext = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.midkrestext = New System.Windows.Forms.TextBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.midpersentext = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.grankrestext = New System.Windows.Forms.TextBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.grandpersentext = New System.Windows.Forms.TextBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.plcctext = New System.Windows.Forms.TextBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.plcrtext = New System.Windows.Forms.TextBox
        Me.Label22 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.eospersenflagtx = New System.Windows.Forms.TextBox
        Me.eoskresflagtx = New System.Windows.Forms.TextBox
        Me.eospersenunittx = New System.Windows.Forms.TextBox
        Me.eoskresunittx = New System.Windows.Forms.TextBox
        Me.eospersentx = New System.Windows.Forms.TextBox
        Me.Label28 = New System.Windows.Forms.Label
        Me.eoskrestx = New System.Windows.Forms.TextBox
        Me.Label29 = New System.Windows.Forms.Label
        Me.neupersenflagtx = New System.Windows.Forms.TextBox
        Me.neupersenunittx = New System.Windows.Forms.TextBox
        Me.neupersentx = New System.Windows.Forms.TextBox
        Me.Label27 = New System.Windows.Forms.Label
        Me.baspersenflagtx = New System.Windows.Forms.TextBox
        Me.baskresflagtx = New System.Windows.Forms.TextBox
        Me.neukresflagtx = New System.Windows.Forms.TextBox
        Me.baspersenunittx = New System.Windows.Forms.TextBox
        Me.baskresunittx = New System.Windows.Forms.TextBox
        Me.neukresunit = New System.Windows.Forms.TextBox
        Me.neukrestx = New System.Windows.Forms.TextBox
        Me.Label24 = New System.Windows.Forms.Label
        Me.baspersentx = New System.Windows.Forms.TextBox
        Me.Label25 = New System.Windows.Forms.Label
        Me.baskrestx = New System.Windows.Forms.TextBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.rdwcvunittx = New System.Windows.Forms.TextBox
        Me.rbcunittx = New System.Windows.Forms.TextBox
        Me.hgbunittx = New System.Windows.Forms.TextBox
        Me.mcvunittx = New System.Windows.Forms.TextBox
        Me.mchunittx = New System.Windows.Forms.TextBox
        Me.mchcunittx = New System.Windows.Forms.TextBox
        Me.rdwsdunittx = New System.Windows.Forms.TextBox
        Me.hctunittx = New System.Windows.Forms.TextBox
        Me.rdwsdflagtx = New System.Windows.Forms.TextBox
        Me.pltflagtx = New System.Windows.Forms.TextBox
        Me.mpvflagtx = New System.Windows.Forms.TextBox
        Me.plccflagtx = New System.Windows.Forms.TextBox
        Me.pctflagtx = New System.Windows.Forms.TextBox
        Me.plcrflagtx = New System.Windows.Forms.TextBox
        Me.pdwflagtx = New System.Windows.Forms.TextBox
        Me.pltunittx = New System.Windows.Forms.TextBox
        Me.mpvunittx = New System.Windows.Forms.TextBox
        Me.plccunittx = New System.Windows.Forms.TextBox
        Me.pctunittx = New System.Windows.Forms.TextBox
        Me.plcrunittx = New System.Windows.Forms.TextBox
        Me.pdwunittx = New System.Windows.Forms.TextBox
        Me.rbcflagtx = New System.Windows.Forms.TextBox
        Me.hgbflagtx = New System.Windows.Forms.TextBox
        Me.rdwcvflagtx = New System.Windows.Forms.TextBox
        Me.mchflagtx = New System.Windows.Forms.TextBox
        Me.mcvflagtx = New System.Windows.Forms.TextBox
        Me.mchcflagtx = New System.Windows.Forms.TextBox
        Me.hctflagtx = New System.Windows.Forms.TextBox
        Me.wbcunittx = New System.Windows.Forms.TextBox
        Me.lymkresunitunittx = New System.Windows.Forms.TextBox
        Me.grandpersenunittx = New System.Windows.Forms.TextBox
        Me.lympersenunittx = New System.Windows.Forms.TextBox
        Me.grandkresunittx = New System.Windows.Forms.TextBox
        Me.midpersenunittx = New System.Windows.Forms.TextBox
        Me.midkresunittx = New System.Windows.Forms.TextBox
        Me.wbcflagtx = New System.Windows.Forms.TextBox
        Me.lymkresflagtx = New System.Windows.Forms.TextBox
        Me.grandpersenflagtx = New System.Windows.Forms.TextBox
        Me.lympersenflagtx = New System.Windows.Forms.TextBox
        Me.grandkresflagtx = New System.Windows.Forms.TextBox
        Me.midpersenflagtx = New System.Windows.Forms.TextBox
        Me.midkresflagtx = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Button4 = New System.Windows.Forms.Button
        Me.Label23 = New System.Windows.Forms.Label
        Me.txtbarcode = New System.Windows.Forms.TextBox
        Me.lblwbcg = New System.Windows.Forms.Label
        Me.lblrbcg = New System.Windows.Forms.Label
        Me.lblpltg = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.plt_pb = New System.Windows.Forms.PictureBox
        Me.rbc_pb = New System.Windows.Forms.PictureBox
        Me.wbc_pb = New System.Windows.Forms.PictureBox
        Me.txtrcv = New System.Windows.Forms.TextBox
        Me.txtintip = New System.Windows.Forms.TextBox
        Me.txtbloodmessage = New System.Windows.Forms.TextBox
        Me.scatter_pb = New System.Windows.Forms.PictureBox
        Me.Label30 = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.plt_pb, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rbc_pb, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.wbc_pb, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.scatter_pb, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtipaddress
        '
        Me.txtipaddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.txtipaddress.Location = New System.Drawing.Point(15, 32)
        Me.txtipaddress.Name = "txtipaddress"
        Me.txtipaddress.Size = New System.Drawing.Size(122, 24)
        Me.txtipaddress.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.Label1.Location = New System.Drawing.Point(12, 59)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 18)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Status: "
        '
        'cmdConnect
        '
        Me.cmdConnect.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.cmdConnect.Location = New System.Drawing.Point(291, 29)
        Me.cmdConnect.Name = "cmdConnect"
        Me.cmdConnect.Size = New System.Drawing.Size(82, 29)
        Me.cmdConnect.TabIndex = 4
        Me.cmdConnect.Text = "Connect"
        Me.cmdConnect.UseVisualStyleBackColor = True
        '
        'txtport
        '
        Me.txtport.Location = New System.Drawing.Point(211, 30)
        Me.txtport.Name = "txtport"
        Me.txtport.Size = New System.Drawing.Size(67, 26)
        Me.txtport.TabIndex = 6
        Me.txtport.Text = "5100"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(379, 29)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(81, 29)
        Me.Button3.TabIndex = 9
        Me.Button3.Text = "DC"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'tmrresult
        '
        Me.tmrresult.Interval = 1500
        '
        'Label2
        '
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(15, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 24)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "WBC"
        '
        'wbctext
        '
        Me.wbctext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.wbctext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wbctext.Location = New System.Drawing.Point(157, 16)
        Me.wbctext.Name = "wbctext"
        Me.wbctext.Size = New System.Drawing.Size(84, 24)
        Me.wbctext.TabIndex = 11
        '
        'lymkrestext
        '
        Me.lymkrestext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lymkrestext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lymkrestext.Location = New System.Drawing.Point(157, 46)
        Me.lymkrestext.Name = "lymkrestext"
        Me.lymkrestext.Size = New System.Drawing.Size(84, 24)
        Me.lymkrestext.TabIndex = 13
        '
        'Label3
        '
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(15, 46)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 24)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "LYM#"
        '
        'lympersentext
        '
        Me.lympersentext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lympersentext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lympersentext.Location = New System.Drawing.Point(157, 136)
        Me.lympersentext.Name = "lympersentext"
        Me.lympersentext.Size = New System.Drawing.Size(84, 24)
        Me.lympersentext.TabIndex = 15
        '
        'Label4
        '
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(15, 136)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 24)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "LYM%"
        '
        'rbctext
        '
        Me.rbctext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rbctext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbctext.Location = New System.Drawing.Point(518, 14)
        Me.rbctext.Name = "rbctext"
        Me.rbctext.Size = New System.Drawing.Size(85, 24)
        Me.rbctext.TabIndex = 17
        '
        'Label5
        '
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(392, 14)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(71, 24)
        Me.Label5.TabIndex = 16
        Me.Label5.Text = "RBC"
        '
        'hgbtext
        '
        Me.hgbtext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.hgbtext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.hgbtext.Location = New System.Drawing.Point(518, 44)
        Me.hgbtext.Name = "hgbtext"
        Me.hgbtext.Size = New System.Drawing.Size(85, 24)
        Me.hgbtext.TabIndex = 19
        '
        'Label6
        '
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(392, 44)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(71, 24)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "HGB"
        '
        'mcvtext
        '
        Me.mcvtext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mcvtext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mcvtext.Location = New System.Drawing.Point(518, 104)
        Me.mcvtext.Name = "mcvtext"
        Me.mcvtext.Size = New System.Drawing.Size(85, 24)
        Me.mcvtext.TabIndex = 21
        '
        'Label7
        '
        Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(392, 104)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(71, 24)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "MCV"
        '
        'mchtext
        '
        Me.mchtext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mchtext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mchtext.Location = New System.Drawing.Point(518, 134)
        Me.mchtext.Name = "mchtext"
        Me.mchtext.Size = New System.Drawing.Size(85, 24)
        Me.mchtext.TabIndex = 23
        '
        'Label8
        '
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(392, 134)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 24)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "MCH"
        '
        'mchctext
        '
        Me.mchctext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mchctext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mchctext.Location = New System.Drawing.Point(518, 164)
        Me.mchctext.Name = "mchctext"
        Me.mchctext.Size = New System.Drawing.Size(85, 24)
        Me.mchctext.TabIndex = 25
        '
        'Label9
        '
        Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(392, 164)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(71, 24)
        Me.Label9.TabIndex = 24
        Me.Label9.Text = "MCHC"
        '
        'rdwcvtext
        '
        Me.rdwcvtext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rdwcvtext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdwcvtext.Location = New System.Drawing.Point(518, 194)
        Me.rdwcvtext.Name = "rdwcvtext"
        Me.rdwcvtext.Size = New System.Drawing.Size(85, 24)
        Me.rdwcvtext.TabIndex = 27
        '
        'Label10
        '
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(392, 194)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(71, 24)
        Me.Label10.TabIndex = 26
        Me.Label10.Text = "RDW-CV"
        '
        'rdwsdtext
        '
        Me.rdwsdtext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rdwsdtext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdwsdtext.Location = New System.Drawing.Point(518, 221)
        Me.rdwsdtext.Name = "rdwsdtext"
        Me.rdwsdtext.Size = New System.Drawing.Size(85, 24)
        Me.rdwsdtext.TabIndex = 29
        '
        'Label11
        '
        Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(392, 224)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(71, 24)
        Me.Label11.TabIndex = 28
        Me.Label11.Text = "RDW-SD"
        '
        'hcttext
        '
        Me.hcttext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.hcttext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.hcttext.Location = New System.Drawing.Point(518, 74)
        Me.hcttext.Name = "hcttext"
        Me.hcttext.Size = New System.Drawing.Size(85, 24)
        Me.hcttext.TabIndex = 31
        '
        'Label12
        '
        Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(392, 74)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(71, 24)
        Me.Label12.TabIndex = 30
        Me.Label12.Text = "HCT"
        '
        'plttext
        '
        Me.plttext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.plttext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.plttext.Location = New System.Drawing.Point(844, 14)
        Me.plttext.Name = "plttext"
        Me.plttext.Size = New System.Drawing.Size(84, 24)
        Me.plttext.TabIndex = 33
        '
        'Label13
        '
        Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(728, 14)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(59, 24)
        Me.Label13.TabIndex = 32
        Me.Label13.Text = "PLT"
        '
        'mpvtext
        '
        Me.mpvtext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mpvtext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mpvtext.Location = New System.Drawing.Point(844, 44)
        Me.mpvtext.Name = "mpvtext"
        Me.mpvtext.Size = New System.Drawing.Size(84, 24)
        Me.mpvtext.TabIndex = 35
        Me.mpvtext.Text = " "
        '
        'Label14
        '
        Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(728, 44)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(59, 24)
        Me.Label14.TabIndex = 34
        Me.Label14.Text = "MPV"
        '
        'pdwtext
        '
        Me.pdwtext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pdwtext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pdwtext.Location = New System.Drawing.Point(844, 74)
        Me.pdwtext.Name = "pdwtext"
        Me.pdwtext.Size = New System.Drawing.Size(84, 24)
        Me.pdwtext.TabIndex = 37
        '
        'Label15
        '
        Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(728, 74)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(59, 24)
        Me.Label15.TabIndex = 36
        Me.Label15.Text = "PDW"
        '
        'pcttext
        '
        Me.pcttext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pcttext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pcttext.Location = New System.Drawing.Point(844, 104)
        Me.pcttext.Name = "pcttext"
        Me.pcttext.Size = New System.Drawing.Size(84, 24)
        Me.pcttext.TabIndex = 39
        Me.pcttext.Text = " "
        '
        'Label16
        '
        Me.Label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(728, 104)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(59, 24)
        Me.Label16.TabIndex = 38
        Me.Label16.Text = "PCT"
        '
        'midkrestext
        '
        Me.midkrestext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.midkrestext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.midkrestext.Location = New System.Drawing.Point(157, 76)
        Me.midkrestext.Name = "midkrestext"
        Me.midkrestext.Size = New System.Drawing.Size(84, 24)
        Me.midkrestext.TabIndex = 41
        '
        'Label17
        '
        Me.Label17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(15, 76)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(85, 24)
        Me.Label17.TabIndex = 40
        Me.Label17.Text = "MID#"
        '
        'midpersentext
        '
        Me.midpersentext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.midpersentext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.midpersentext.Location = New System.Drawing.Point(157, 166)
        Me.midpersentext.Name = "midpersentext"
        Me.midpersentext.Size = New System.Drawing.Size(84, 24)
        Me.midpersentext.TabIndex = 43
        '
        'Label18
        '
        Me.Label18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(15, 166)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(85, 24)
        Me.Label18.TabIndex = 42
        Me.Label18.Text = "MID%"
        '
        'grankrestext
        '
        Me.grankrestext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.grankrestext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grankrestext.Location = New System.Drawing.Point(157, 106)
        Me.grankrestext.Name = "grankrestext"
        Me.grankrestext.Size = New System.Drawing.Size(84, 24)
        Me.grankrestext.TabIndex = 45
        '
        'Label19
        '
        Me.Label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(15, 106)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(85, 24)
        Me.Label19.TabIndex = 44
        Me.Label19.Text = "GRAND#"
        '
        'grandpersentext
        '
        Me.grandpersentext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.grandpersentext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grandpersentext.Location = New System.Drawing.Point(157, 196)
        Me.grandpersentext.Name = "grandpersentext"
        Me.grandpersentext.Size = New System.Drawing.Size(84, 24)
        Me.grandpersentext.TabIndex = 47
        '
        'Label20
        '
        Me.Label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(15, 196)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(85, 24)
        Me.Label20.TabIndex = 46
        Me.Label20.Text = "GRAND%"
        '
        'plcctext
        '
        Me.plcctext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.plcctext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.plcctext.Location = New System.Drawing.Point(844, 134)
        Me.plcctext.Name = "plcctext"
        Me.plcctext.Size = New System.Drawing.Size(84, 24)
        Me.plcctext.TabIndex = 49
        '
        'Label21
        '
        Me.Label21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(728, 134)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(59, 24)
        Me.Label21.TabIndex = 48
        Me.Label21.Text = "PLCC"
        '
        'plcrtext
        '
        Me.plcrtext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.plcrtext.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.plcrtext.Location = New System.Drawing.Point(844, 164)
        Me.plcrtext.Name = "plcrtext"
        Me.plcrtext.Size = New System.Drawing.Size(84, 24)
        Me.plcrtext.TabIndex = 51
        '
        'Label22
        '
        Me.Label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(728, 164)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(59, 24)
        Me.Label22.TabIndex = 50
        Me.Label22.Text = "PLCR"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.eospersenflagtx)
        Me.Panel1.Controls.Add(Me.eoskresflagtx)
        Me.Panel1.Controls.Add(Me.eospersenunittx)
        Me.Panel1.Controls.Add(Me.eoskresunittx)
        Me.Panel1.Controls.Add(Me.eospersentx)
        Me.Panel1.Controls.Add(Me.Label28)
        Me.Panel1.Controls.Add(Me.eoskrestx)
        Me.Panel1.Controls.Add(Me.Label29)
        Me.Panel1.Controls.Add(Me.neupersenflagtx)
        Me.Panel1.Controls.Add(Me.neupersenunittx)
        Me.Panel1.Controls.Add(Me.neupersentx)
        Me.Panel1.Controls.Add(Me.Label27)
        Me.Panel1.Controls.Add(Me.baspersenflagtx)
        Me.Panel1.Controls.Add(Me.baskresflagtx)
        Me.Panel1.Controls.Add(Me.neukresflagtx)
        Me.Panel1.Controls.Add(Me.baspersenunittx)
        Me.Panel1.Controls.Add(Me.baskresunittx)
        Me.Panel1.Controls.Add(Me.neukresunit)
        Me.Panel1.Controls.Add(Me.neukrestx)
        Me.Panel1.Controls.Add(Me.Label24)
        Me.Panel1.Controls.Add(Me.baspersentx)
        Me.Panel1.Controls.Add(Me.Label25)
        Me.Panel1.Controls.Add(Me.baskrestx)
        Me.Panel1.Controls.Add(Me.Label26)
        Me.Panel1.Controls.Add(Me.rdwcvunittx)
        Me.Panel1.Controls.Add(Me.rbcunittx)
        Me.Panel1.Controls.Add(Me.hgbunittx)
        Me.Panel1.Controls.Add(Me.mcvunittx)
        Me.Panel1.Controls.Add(Me.mchunittx)
        Me.Panel1.Controls.Add(Me.mchcunittx)
        Me.Panel1.Controls.Add(Me.rdwsdunittx)
        Me.Panel1.Controls.Add(Me.hctunittx)
        Me.Panel1.Controls.Add(Me.rdwsdflagtx)
        Me.Panel1.Controls.Add(Me.pltflagtx)
        Me.Panel1.Controls.Add(Me.mpvflagtx)
        Me.Panel1.Controls.Add(Me.plccflagtx)
        Me.Panel1.Controls.Add(Me.pctflagtx)
        Me.Panel1.Controls.Add(Me.plcrflagtx)
        Me.Panel1.Controls.Add(Me.pdwflagtx)
        Me.Panel1.Controls.Add(Me.pltunittx)
        Me.Panel1.Controls.Add(Me.mpvunittx)
        Me.Panel1.Controls.Add(Me.plccunittx)
        Me.Panel1.Controls.Add(Me.pctunittx)
        Me.Panel1.Controls.Add(Me.plcrunittx)
        Me.Panel1.Controls.Add(Me.pdwunittx)
        Me.Panel1.Controls.Add(Me.rbcflagtx)
        Me.Panel1.Controls.Add(Me.hgbflagtx)
        Me.Panel1.Controls.Add(Me.rdwcvflagtx)
        Me.Panel1.Controls.Add(Me.mchflagtx)
        Me.Panel1.Controls.Add(Me.mcvflagtx)
        Me.Panel1.Controls.Add(Me.mchcflagtx)
        Me.Panel1.Controls.Add(Me.hctflagtx)
        Me.Panel1.Controls.Add(Me.wbcunittx)
        Me.Panel1.Controls.Add(Me.lymkresunitunittx)
        Me.Panel1.Controls.Add(Me.grandpersenunittx)
        Me.Panel1.Controls.Add(Me.lympersenunittx)
        Me.Panel1.Controls.Add(Me.grandkresunittx)
        Me.Panel1.Controls.Add(Me.midpersenunittx)
        Me.Panel1.Controls.Add(Me.midkresunittx)
        Me.Panel1.Controls.Add(Me.wbcflagtx)
        Me.Panel1.Controls.Add(Me.lymkresflagtx)
        Me.Panel1.Controls.Add(Me.grandpersenflagtx)
        Me.Panel1.Controls.Add(Me.lympersenflagtx)
        Me.Panel1.Controls.Add(Me.grandkresflagtx)
        Me.Panel1.Controls.Add(Me.midpersenflagtx)
        Me.Panel1.Controls.Add(Me.midkresflagtx)
        Me.Panel1.Controls.Add(Me.rdwcvtext)
        Me.Panel1.Controls.Add(Me.plcrtext)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label22)
        Me.Panel1.Controls.Add(Me.wbctext)
        Me.Panel1.Controls.Add(Me.plcctext)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label21)
        Me.Panel1.Controls.Add(Me.lymkrestext)
        Me.Panel1.Controls.Add(Me.grandpersentext)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label20)
        Me.Panel1.Controls.Add(Me.lympersentext)
        Me.Panel1.Controls.Add(Me.grankrestext)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Controls.Add(Me.rbctext)
        Me.Panel1.Controls.Add(Me.midpersentext)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.hgbtext)
        Me.Panel1.Controls.Add(Me.midkrestext)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.mcvtext)
        Me.Panel1.Controls.Add(Me.pcttext)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.mchtext)
        Me.Panel1.Controls.Add(Me.pdwtext)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.mpvtext)
        Me.Panel1.Controls.Add(Me.mchctext)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.plttext)
        Me.Panel1.Controls.Add(Me.rdwsdtext)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.hcttext)
        Me.Panel1.Location = New System.Drawing.Point(17, 162)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1040, 314)
        Me.Panel1.TabIndex = 52
        '
        'eospersenflagtx
        '
        Me.eospersenflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.eospersenflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eospersenflagtx.Location = New System.Drawing.Point(106, 257)
        Me.eospersenflagtx.Name = "eospersenflagtx"
        Me.eospersenflagtx.Size = New System.Drawing.Size(45, 24)
        Me.eospersenflagtx.TabIndex = 117
        '
        'eoskresflagtx
        '
        Me.eoskresflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.eoskresflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eoskresflagtx.Location = New System.Drawing.Point(106, 227)
        Me.eoskresflagtx.Name = "eoskresflagtx"
        Me.eoskresflagtx.Size = New System.Drawing.Size(45, 24)
        Me.eoskresflagtx.TabIndex = 118
        '
        'eospersenunittx
        '
        Me.eospersenunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.eospersenunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eospersenunittx.Location = New System.Drawing.Point(247, 257)
        Me.eospersenunittx.Name = "eospersenunittx"
        Me.eospersenunittx.Size = New System.Drawing.Size(84, 24)
        Me.eospersenunittx.TabIndex = 115
        '
        'eoskresunittx
        '
        Me.eoskresunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.eoskresunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eoskresunittx.Location = New System.Drawing.Point(247, 227)
        Me.eoskresunittx.Name = "eoskresunittx"
        Me.eoskresunittx.Size = New System.Drawing.Size(84, 24)
        Me.eoskresunittx.TabIndex = 116
        '
        'eospersentx
        '
        Me.eospersentx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.eospersentx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eospersentx.Location = New System.Drawing.Point(157, 257)
        Me.eospersentx.Name = "eospersentx"
        Me.eospersentx.Size = New System.Drawing.Size(84, 24)
        Me.eospersentx.TabIndex = 114
        '
        'Label28
        '
        Me.Label28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(15, 257)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(82, 24)
        Me.Label28.TabIndex = 113
        Me.Label28.Text = "EOS%"
        '
        'eoskrestx
        '
        Me.eoskrestx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.eoskrestx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eoskrestx.Location = New System.Drawing.Point(157, 227)
        Me.eoskrestx.Name = "eoskrestx"
        Me.eoskrestx.Size = New System.Drawing.Size(84, 24)
        Me.eoskrestx.TabIndex = 112
        Me.eoskrestx.Text = " "
        '
        'Label29
        '
        Me.Label29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(15, 227)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(82, 24)
        Me.Label29.TabIndex = 111
        Me.Label29.Text = "EOS#"
        '
        'neupersenflagtx
        '
        Me.neupersenflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.neupersenflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.neupersenflagtx.Location = New System.Drawing.Point(793, 286)
        Me.neupersenflagtx.Name = "neupersenflagtx"
        Me.neupersenflagtx.Size = New System.Drawing.Size(45, 24)
        Me.neupersenflagtx.TabIndex = 110
        '
        'neupersenunittx
        '
        Me.neupersenunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.neupersenunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.neupersenunittx.Location = New System.Drawing.Point(934, 286)
        Me.neupersenunittx.Name = "neupersenunittx"
        Me.neupersenunittx.Size = New System.Drawing.Size(84, 24)
        Me.neupersenunittx.TabIndex = 109
        '
        'neupersentx
        '
        Me.neupersentx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.neupersentx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.neupersentx.Location = New System.Drawing.Point(844, 286)
        Me.neupersentx.Name = "neupersentx"
        Me.neupersentx.Size = New System.Drawing.Size(84, 24)
        Me.neupersentx.TabIndex = 108
        '
        'Label27
        '
        Me.Label27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(728, 286)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(59, 24)
        Me.Label27.TabIndex = 107
        Me.Label27.Text = "NEU%"
        '
        'baspersenflagtx
        '
        Me.baspersenflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.baspersenflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.baspersenflagtx.Location = New System.Drawing.Point(793, 226)
        Me.baspersenflagtx.Name = "baspersenflagtx"
        Me.baspersenflagtx.Size = New System.Drawing.Size(45, 24)
        Me.baspersenflagtx.TabIndex = 104
        '
        'baskresflagtx
        '
        Me.baskresflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.baskresflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.baskresflagtx.Location = New System.Drawing.Point(793, 196)
        Me.baskresflagtx.Name = "baskresflagtx"
        Me.baskresflagtx.Size = New System.Drawing.Size(45, 24)
        Me.baskresflagtx.TabIndex = 106
        '
        'neukresflagtx
        '
        Me.neukresflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.neukresflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.neukresflagtx.Location = New System.Drawing.Point(793, 256)
        Me.neukresflagtx.Name = "neukresflagtx"
        Me.neukresflagtx.Size = New System.Drawing.Size(45, 24)
        Me.neukresflagtx.TabIndex = 105
        '
        'baspersenunittx
        '
        Me.baspersenunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.baspersenunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.baspersenunittx.Location = New System.Drawing.Point(934, 226)
        Me.baspersenunittx.Name = "baspersenunittx"
        Me.baspersenunittx.Size = New System.Drawing.Size(84, 24)
        Me.baspersenunittx.TabIndex = 101
        '
        'baskresunittx
        '
        Me.baskresunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.baskresunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.baskresunittx.Location = New System.Drawing.Point(934, 196)
        Me.baskresunittx.Name = "baskresunittx"
        Me.baskresunittx.Size = New System.Drawing.Size(84, 24)
        Me.baskresunittx.TabIndex = 103
        '
        'neukresunit
        '
        Me.neukresunit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.neukresunit.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.neukresunit.Location = New System.Drawing.Point(934, 256)
        Me.neukresunit.Name = "neukresunit"
        Me.neukresunit.Size = New System.Drawing.Size(84, 24)
        Me.neukresunit.TabIndex = 102
        '
        'neukrestx
        '
        Me.neukrestx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.neukrestx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.neukrestx.Location = New System.Drawing.Point(844, 256)
        Me.neukrestx.Name = "neukrestx"
        Me.neukrestx.Size = New System.Drawing.Size(84, 24)
        Me.neukrestx.TabIndex = 100
        '
        'Label24
        '
        Me.Label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(728, 256)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(59, 24)
        Me.Label24.TabIndex = 99
        Me.Label24.Text = "NEU#"
        '
        'baspersentx
        '
        Me.baspersentx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.baspersentx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.baspersentx.Location = New System.Drawing.Point(844, 226)
        Me.baspersentx.Name = "baspersentx"
        Me.baspersentx.Size = New System.Drawing.Size(84, 24)
        Me.baspersentx.TabIndex = 98
        '
        'Label25
        '
        Me.Label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(728, 226)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(59, 24)
        Me.Label25.TabIndex = 97
        Me.Label25.Text = "BAS%"
        '
        'baskrestx
        '
        Me.baskrestx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.baskrestx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.baskrestx.Location = New System.Drawing.Point(844, 196)
        Me.baskrestx.Name = "baskrestx"
        Me.baskrestx.Size = New System.Drawing.Size(84, 24)
        Me.baskrestx.TabIndex = 96
        Me.baskrestx.Text = " "
        '
        'Label26
        '
        Me.Label26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(728, 196)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(59, 24)
        Me.Label26.TabIndex = 95
        Me.Label26.Text = "BAS#"
        '
        'rdwcvunittx
        '
        Me.rdwcvunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rdwcvunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdwcvunittx.Location = New System.Drawing.Point(609, 194)
        Me.rdwcvunittx.Name = "rdwcvunittx"
        Me.rdwcvunittx.Size = New System.Drawing.Size(82, 24)
        Me.rdwcvunittx.TabIndex = 92
        '
        'rbcunittx
        '
        Me.rbcunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rbcunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbcunittx.Location = New System.Drawing.Point(609, 14)
        Me.rbcunittx.Name = "rbcunittx"
        Me.rbcunittx.Size = New System.Drawing.Size(82, 24)
        Me.rbcunittx.TabIndex = 87
        '
        'hgbunittx
        '
        Me.hgbunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.hgbunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.hgbunittx.Location = New System.Drawing.Point(609, 44)
        Me.hgbunittx.Name = "hgbunittx"
        Me.hgbunittx.Size = New System.Drawing.Size(82, 24)
        Me.hgbunittx.TabIndex = 88
        '
        'mcvunittx
        '
        Me.mcvunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mcvunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mcvunittx.Location = New System.Drawing.Point(609, 104)
        Me.mcvunittx.Name = "mcvunittx"
        Me.mcvunittx.Size = New System.Drawing.Size(82, 24)
        Me.mcvunittx.TabIndex = 89
        '
        'mchunittx
        '
        Me.mchunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mchunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mchunittx.Location = New System.Drawing.Point(609, 134)
        Me.mchunittx.Name = "mchunittx"
        Me.mchunittx.Size = New System.Drawing.Size(82, 24)
        Me.mchunittx.TabIndex = 90
        '
        'mchcunittx
        '
        Me.mchcunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mchcunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mchcunittx.Location = New System.Drawing.Point(609, 164)
        Me.mchcunittx.Name = "mchcunittx"
        Me.mchcunittx.Size = New System.Drawing.Size(82, 24)
        Me.mchcunittx.TabIndex = 91
        '
        'rdwsdunittx
        '
        Me.rdwsdunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rdwsdunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdwsdunittx.Location = New System.Drawing.Point(609, 221)
        Me.rdwsdunittx.Name = "rdwsdunittx"
        Me.rdwsdunittx.Size = New System.Drawing.Size(82, 24)
        Me.rdwsdunittx.TabIndex = 93
        '
        'hctunittx
        '
        Me.hctunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.hctunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.hctunittx.Location = New System.Drawing.Point(609, 74)
        Me.hctunittx.Name = "hctunittx"
        Me.hctunittx.Size = New System.Drawing.Size(82, 24)
        Me.hctunittx.TabIndex = 94
        '
        'rdwsdflagtx
        '
        Me.rdwsdflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rdwsdflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdwsdflagtx.Location = New System.Drawing.Point(469, 223)
        Me.rdwsdflagtx.Name = "rdwsdflagtx"
        Me.rdwsdflagtx.Size = New System.Drawing.Size(45, 24)
        Me.rdwsdflagtx.TabIndex = 86
        '
        'pltflagtx
        '
        Me.pltflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pltflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pltflagtx.Location = New System.Drawing.Point(793, 14)
        Me.pltflagtx.Name = "pltflagtx"
        Me.pltflagtx.Size = New System.Drawing.Size(45, 24)
        Me.pltflagtx.TabIndex = 80
        '
        'mpvflagtx
        '
        Me.mpvflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mpvflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mpvflagtx.Location = New System.Drawing.Point(793, 44)
        Me.mpvflagtx.Name = "mpvflagtx"
        Me.mpvflagtx.Size = New System.Drawing.Size(45, 24)
        Me.mpvflagtx.TabIndex = 81
        '
        'plccflagtx
        '
        Me.plccflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.plccflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.plccflagtx.Location = New System.Drawing.Point(793, 134)
        Me.plccflagtx.Name = "plccflagtx"
        Me.plccflagtx.Size = New System.Drawing.Size(45, 24)
        Me.plccflagtx.TabIndex = 82
        '
        'pctflagtx
        '
        Me.pctflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pctflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pctflagtx.Location = New System.Drawing.Point(793, 104)
        Me.pctflagtx.Name = "pctflagtx"
        Me.pctflagtx.Size = New System.Drawing.Size(45, 24)
        Me.pctflagtx.TabIndex = 85
        '
        'plcrflagtx
        '
        Me.plcrflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.plcrflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.plcrflagtx.Location = New System.Drawing.Point(793, 164)
        Me.plcrflagtx.Name = "plcrflagtx"
        Me.plcrflagtx.Size = New System.Drawing.Size(45, 24)
        Me.plcrflagtx.TabIndex = 84
        '
        'pdwflagtx
        '
        Me.pdwflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pdwflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pdwflagtx.Location = New System.Drawing.Point(793, 74)
        Me.pdwflagtx.Name = "pdwflagtx"
        Me.pdwflagtx.Size = New System.Drawing.Size(45, 24)
        Me.pdwflagtx.TabIndex = 83
        '
        'pltunittx
        '
        Me.pltunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pltunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pltunittx.Location = New System.Drawing.Point(934, 14)
        Me.pltunittx.Name = "pltunittx"
        Me.pltunittx.Size = New System.Drawing.Size(84, 24)
        Me.pltunittx.TabIndex = 73
        '
        'mpvunittx
        '
        Me.mpvunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mpvunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mpvunittx.Location = New System.Drawing.Point(934, 44)
        Me.mpvunittx.Name = "mpvunittx"
        Me.mpvunittx.Size = New System.Drawing.Size(84, 24)
        Me.mpvunittx.TabIndex = 74
        '
        'plccunittx
        '
        Me.plccunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.plccunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.plccunittx.Location = New System.Drawing.Point(934, 134)
        Me.plccunittx.Name = "plccunittx"
        Me.plccunittx.Size = New System.Drawing.Size(84, 24)
        Me.plccunittx.TabIndex = 75
        '
        'pctunittx
        '
        Me.pctunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pctunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pctunittx.Location = New System.Drawing.Point(934, 104)
        Me.pctunittx.Name = "pctunittx"
        Me.pctunittx.Size = New System.Drawing.Size(84, 24)
        Me.pctunittx.TabIndex = 78
        '
        'plcrunittx
        '
        Me.plcrunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.plcrunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.plcrunittx.Location = New System.Drawing.Point(934, 164)
        Me.plcrunittx.Name = "plcrunittx"
        Me.plcrunittx.Size = New System.Drawing.Size(84, 24)
        Me.plcrunittx.TabIndex = 77
        '
        'pdwunittx
        '
        Me.pdwunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pdwunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pdwunittx.Location = New System.Drawing.Point(934, 74)
        Me.pdwunittx.Name = "pdwunittx"
        Me.pdwunittx.Size = New System.Drawing.Size(84, 24)
        Me.pdwunittx.TabIndex = 76
        '
        'rbcflagtx
        '
        Me.rbcflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rbcflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbcflagtx.Location = New System.Drawing.Point(469, 14)
        Me.rbcflagtx.Name = "rbcflagtx"
        Me.rbcflagtx.Size = New System.Drawing.Size(45, 24)
        Me.rbcflagtx.TabIndex = 66
        '
        'hgbflagtx
        '
        Me.hgbflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.hgbflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.hgbflagtx.Location = New System.Drawing.Point(469, 44)
        Me.hgbflagtx.Name = "hgbflagtx"
        Me.hgbflagtx.Size = New System.Drawing.Size(45, 24)
        Me.hgbflagtx.TabIndex = 67
        '
        'rdwcvflagtx
        '
        Me.rdwcvflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rdwcvflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdwcvflagtx.Location = New System.Drawing.Point(469, 194)
        Me.rdwcvflagtx.Name = "rdwcvflagtx"
        Me.rdwcvflagtx.Size = New System.Drawing.Size(45, 24)
        Me.rdwcvflagtx.TabIndex = 72
        '
        'mchflagtx
        '
        Me.mchflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mchflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mchflagtx.Location = New System.Drawing.Point(469, 134)
        Me.mchflagtx.Name = "mchflagtx"
        Me.mchflagtx.Size = New System.Drawing.Size(45, 24)
        Me.mchflagtx.TabIndex = 68
        '
        'mcvflagtx
        '
        Me.mcvflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mcvflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mcvflagtx.Location = New System.Drawing.Point(469, 104)
        Me.mcvflagtx.Name = "mcvflagtx"
        Me.mcvflagtx.Size = New System.Drawing.Size(45, 24)
        Me.mcvflagtx.TabIndex = 71
        '
        'mchcflagtx
        '
        Me.mchcflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mchcflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mchcflagtx.Location = New System.Drawing.Point(469, 164)
        Me.mchcflagtx.Name = "mchcflagtx"
        Me.mchcflagtx.Size = New System.Drawing.Size(45, 24)
        Me.mchcflagtx.TabIndex = 70
        '
        'hctflagtx
        '
        Me.hctflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.hctflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.hctflagtx.Location = New System.Drawing.Point(469, 74)
        Me.hctflagtx.Name = "hctflagtx"
        Me.hctflagtx.Size = New System.Drawing.Size(45, 24)
        Me.hctflagtx.TabIndex = 69
        '
        'wbcunittx
        '
        Me.wbcunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.wbcunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wbcunittx.Location = New System.Drawing.Point(247, 16)
        Me.wbcunittx.Name = "wbcunittx"
        Me.wbcunittx.Size = New System.Drawing.Size(84, 24)
        Me.wbcunittx.TabIndex = 59
        '
        'lymkresunitunittx
        '
        Me.lymkresunitunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lymkresunitunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lymkresunitunittx.Location = New System.Drawing.Point(247, 46)
        Me.lymkresunitunittx.Name = "lymkresunitunittx"
        Me.lymkresunitunittx.Size = New System.Drawing.Size(84, 24)
        Me.lymkresunitunittx.TabIndex = 60
        '
        'grandpersenunittx
        '
        Me.grandpersenunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.grandpersenunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grandpersenunittx.Location = New System.Drawing.Point(247, 196)
        Me.grandpersenunittx.Name = "grandpersenunittx"
        Me.grandpersenunittx.Size = New System.Drawing.Size(84, 24)
        Me.grandpersenunittx.TabIndex = 65
        '
        'lympersenunittx
        '
        Me.lympersenunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lympersenunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lympersenunittx.Location = New System.Drawing.Point(247, 136)
        Me.lympersenunittx.Name = "lympersenunittx"
        Me.lympersenunittx.Size = New System.Drawing.Size(84, 24)
        Me.lympersenunittx.TabIndex = 61
        '
        'grandkresunittx
        '
        Me.grandkresunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.grandkresunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grandkresunittx.Location = New System.Drawing.Point(247, 106)
        Me.grandkresunittx.Name = "grandkresunittx"
        Me.grandkresunittx.Size = New System.Drawing.Size(84, 24)
        Me.grandkresunittx.TabIndex = 64
        '
        'midpersenunittx
        '
        Me.midpersenunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.midpersenunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.midpersenunittx.Location = New System.Drawing.Point(247, 166)
        Me.midpersenunittx.Name = "midpersenunittx"
        Me.midpersenunittx.Size = New System.Drawing.Size(84, 24)
        Me.midpersenunittx.TabIndex = 63
        '
        'midkresunittx
        '
        Me.midkresunittx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.midkresunittx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.midkresunittx.Location = New System.Drawing.Point(247, 76)
        Me.midkresunittx.Name = "midkresunittx"
        Me.midkresunittx.Size = New System.Drawing.Size(84, 24)
        Me.midkresunittx.TabIndex = 62
        '
        'wbcflagtx
        '
        Me.wbcflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.wbcflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wbcflagtx.Location = New System.Drawing.Point(106, 16)
        Me.wbcflagtx.Name = "wbcflagtx"
        Me.wbcflagtx.Size = New System.Drawing.Size(45, 24)
        Me.wbcflagtx.TabIndex = 52
        '
        'lymkresflagtx
        '
        Me.lymkresflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lymkresflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lymkresflagtx.Location = New System.Drawing.Point(106, 46)
        Me.lymkresflagtx.Name = "lymkresflagtx"
        Me.lymkresflagtx.Size = New System.Drawing.Size(45, 24)
        Me.lymkresflagtx.TabIndex = 53
        '
        'grandpersenflagtx
        '
        Me.grandpersenflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.grandpersenflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grandpersenflagtx.Location = New System.Drawing.Point(106, 196)
        Me.grandpersenflagtx.Name = "grandpersenflagtx"
        Me.grandpersenflagtx.Size = New System.Drawing.Size(45, 24)
        Me.grandpersenflagtx.TabIndex = 58
        '
        'lympersenflagtx
        '
        Me.lympersenflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lympersenflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lympersenflagtx.Location = New System.Drawing.Point(106, 136)
        Me.lympersenflagtx.Name = "lympersenflagtx"
        Me.lympersenflagtx.Size = New System.Drawing.Size(45, 24)
        Me.lympersenflagtx.TabIndex = 54
        '
        'grandkresflagtx
        '
        Me.grandkresflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.grandkresflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grandkresflagtx.Location = New System.Drawing.Point(106, 106)
        Me.grandkresflagtx.Name = "grandkresflagtx"
        Me.grandkresflagtx.Size = New System.Drawing.Size(45, 24)
        Me.grandkresflagtx.TabIndex = 57
        '
        'midpersenflagtx
        '
        Me.midpersenflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.midpersenflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.midpersenflagtx.Location = New System.Drawing.Point(106, 166)
        Me.midpersenflagtx.Name = "midpersenflagtx"
        Me.midpersenflagtx.Size = New System.Drawing.Size(45, 24)
        Me.midpersenflagtx.TabIndex = 56
        '
        'midkresflagtx
        '
        Me.midkresflagtx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.midkresflagtx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.midkresflagtx.Location = New System.Drawing.Point(106, 76)
        Me.midkresflagtx.Name = "midkresflagtx"
        Me.midkresflagtx.Size = New System.Drawing.Size(45, 24)
        Me.midkresflagtx.TabIndex = 55
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button4)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.txtport)
        Me.GroupBox1.Controls.Add(Me.cmdConnect)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtipaddress)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(15, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(478, 83)
        Me.GroupBox1.TabIndex = 53
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Connection"
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(137, 32)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(62, 24)
        Me.Button4.TabIndex = 11
        Me.Button4.Text = "save"
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(13, 104)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(169, 20)
        Me.Label23.TabIndex = 54
        Me.Label23.Text = "Barcode ID Pasien: "
        '
        'txtbarcode
        '
        Me.txtbarcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtbarcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbarcode.Location = New System.Drawing.Point(188, 101)
        Me.txtbarcode.Name = "txtbarcode"
        Me.txtbarcode.Size = New System.Drawing.Size(158, 26)
        Me.txtbarcode.TabIndex = 55
        '
        'lblwbcg
        '
        Me.lblwbcg.AutoSize = True
        Me.lblwbcg.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblwbcg.Location = New System.Drawing.Point(12, 479)
        Me.lblwbcg.Name = "lblwbcg"
        Me.lblwbcg.Size = New System.Drawing.Size(49, 20)
        Me.lblwbcg.TabIndex = 59
        Me.lblwbcg.Text = "WBC"
        '
        'lblrbcg
        '
        Me.lblrbcg.AutoSize = True
        Me.lblrbcg.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblrbcg.Location = New System.Drawing.Point(222, 478)
        Me.lblrbcg.Name = "lblrbcg"
        Me.lblrbcg.Size = New System.Drawing.Size(46, 20)
        Me.lblrbcg.TabIndex = 60
        Me.lblrbcg.Text = "RBC"
        '
        'lblpltg
        '
        Me.lblpltg.AutoSize = True
        Me.lblpltg.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpltg.Location = New System.Drawing.Point(438, 478)
        Me.lblpltg.Name = "lblpltg"
        Me.lblpltg.Size = New System.Drawing.Size(40, 20)
        Me.lblpltg.TabIndex = 61
        Me.lblpltg.Text = "PLT"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(1004, 500)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 62
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(1004, 538)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 63
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(1077, 586)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(116, 105)
        Me.PictureBox1.TabIndex = 64
        Me.PictureBox1.TabStop = False
        '
        'plt_pb
        '
        Me.plt_pb.Location = New System.Drawing.Point(442, 504)
        Me.plt_pb.Name = "plt_pb"
        Me.plt_pb.Size = New System.Drawing.Size(203, 190)
        Me.plt_pb.TabIndex = 58
        Me.plt_pb.TabStop = False
        '
        'rbc_pb
        '
        Me.rbc_pb.Location = New System.Drawing.Point(226, 504)
        Me.rbc_pb.Name = "rbc_pb"
        Me.rbc_pb.Size = New System.Drawing.Size(203, 190)
        Me.rbc_pb.TabIndex = 57
        Me.rbc_pb.TabStop = False
        '
        'wbc_pb
        '
        Me.wbc_pb.Location = New System.Drawing.Point(15, 504)
        Me.wbc_pb.Name = "wbc_pb"
        Me.wbc_pb.Size = New System.Drawing.Size(199, 190)
        Me.wbc_pb.TabIndex = 56
        Me.wbc_pb.TabStop = False
        '
        'txtrcv
        '
        Me.txtrcv.Location = New System.Drawing.Point(506, 21)
        Me.txtrcv.MaxLength = 0
        Me.txtrcv.Multiline = True
        Me.txtrcv.Name = "txtrcv"
        Me.txtrcv.Size = New System.Drawing.Size(551, 103)
        Me.txtrcv.TabIndex = 65
        '
        'txtintip
        '
        Me.txtintip.Location = New System.Drawing.Point(375, 104)
        Me.txtintip.Name = "txtintip"
        Me.txtintip.Size = New System.Drawing.Size(100, 20)
        Me.txtintip.TabIndex = 66
        '
        'txtbloodmessage
        '
        Me.txtbloodmessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtbloodmessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbloodmessage.Location = New System.Drawing.Point(809, 130)
        Me.txtbloodmessage.Name = "txtbloodmessage"
        Me.txtbloodmessage.Size = New System.Drawing.Size(247, 26)
        Me.txtbloodmessage.TabIndex = 67
        '
        'scatter_pb
        '
        Me.scatter_pb.Location = New System.Drawing.Point(660, 504)
        Me.scatter_pb.Name = "scatter_pb"
        Me.scatter_pb.Size = New System.Drawing.Size(215, 190)
        Me.scatter_pb.TabIndex = 68
        Me.scatter_pb.TabStop = False
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(659, 479)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(135, 20)
        Me.Label30.TabIndex = 69
        Me.Label30.Text = "WBC SCATTER"
        '
        'MindRay5100
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1372, 703)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.scatter_pb)
        Me.Controls.Add(Me.txtbloodmessage)
        Me.Controls.Add(Me.txtintip)
        Me.Controls.Add(Me.txtrcv)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lblpltg)
        Me.Controls.Add(Me.lblrbcg)
        Me.Controls.Add(Me.lblwbcg)
        Me.Controls.Add(Me.plt_pb)
        Me.Controls.Add(Me.rbc_pb)
        Me.Controls.Add(Me.wbc_pb)
        Me.Controls.Add(Me.txtbarcode)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MindRay5100"
        Me.Text = " MindRay BC 5100/5150"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.plt_pb, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rbc_pb, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.wbc_pb, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.scatter_pb, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtipaddress As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmdConnect As System.Windows.Forms.Button
    Friend WithEvents txtport As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents tmrresult As System.Windows.Forms.Timer
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents wbctext As System.Windows.Forms.TextBox
    Friend WithEvents lymkrestext As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lympersentext As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents rbctext As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents hgbtext As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents mcvtext As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents mchtext As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents mchctext As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents rdwcvtext As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents rdwsdtext As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents hcttext As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents plttext As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents mpvtext As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents pdwtext As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents pcttext As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents midkrestext As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents midpersentext As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents grankrestext As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents grandpersentext As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents plcctext As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents plcrtext As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtbarcode As System.Windows.Forms.TextBox
    Friend WithEvents wbc_pb As System.Windows.Forms.PictureBox
    Friend WithEvents rbc_pb As System.Windows.Forms.PictureBox
    Friend WithEvents plt_pb As System.Windows.Forms.PictureBox
    Friend WithEvents lblwbcg As System.Windows.Forms.Label
    Friend WithEvents lblrbcg As System.Windows.Forms.Label
    Friend WithEvents lblpltg As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents rbcflagtx As System.Windows.Forms.TextBox
    Friend WithEvents hgbflagtx As System.Windows.Forms.TextBox
    Friend WithEvents rdwcvflagtx As System.Windows.Forms.TextBox
    Friend WithEvents mchflagtx As System.Windows.Forms.TextBox
    Friend WithEvents mcvflagtx As System.Windows.Forms.TextBox
    Friend WithEvents mchcflagtx As System.Windows.Forms.TextBox
    Friend WithEvents hctflagtx As System.Windows.Forms.TextBox
    Friend WithEvents wbcunittx As System.Windows.Forms.TextBox
    Friend WithEvents lymkresunitunittx As System.Windows.Forms.TextBox
    Friend WithEvents grandpersenunittx As System.Windows.Forms.TextBox
    Friend WithEvents lympersenunittx As System.Windows.Forms.TextBox
    Friend WithEvents grandkresunittx As System.Windows.Forms.TextBox
    Friend WithEvents midpersenunittx As System.Windows.Forms.TextBox
    Friend WithEvents midkresunittx As System.Windows.Forms.TextBox
    Friend WithEvents wbcflagtx As System.Windows.Forms.TextBox
    Friend WithEvents lymkresflagtx As System.Windows.Forms.TextBox
    Friend WithEvents grandpersenflagtx As System.Windows.Forms.TextBox
    Friend WithEvents lympersenflagtx As System.Windows.Forms.TextBox
    Friend WithEvents grandkresflagtx As System.Windows.Forms.TextBox
    Friend WithEvents midpersenflagtx As System.Windows.Forms.TextBox
    Friend WithEvents midkresflagtx As System.Windows.Forms.TextBox
    Friend WithEvents rdwcvunittx As System.Windows.Forms.TextBox
    Friend WithEvents rbcunittx As System.Windows.Forms.TextBox
    Friend WithEvents hgbunittx As System.Windows.Forms.TextBox
    Friend WithEvents mcvunittx As System.Windows.Forms.TextBox
    Friend WithEvents mchunittx As System.Windows.Forms.TextBox
    Friend WithEvents mchcunittx As System.Windows.Forms.TextBox
    Friend WithEvents rdwsdunittx As System.Windows.Forms.TextBox
    Friend WithEvents hctunittx As System.Windows.Forms.TextBox
    Friend WithEvents rdwsdflagtx As System.Windows.Forms.TextBox
    Friend WithEvents pltflagtx As System.Windows.Forms.TextBox
    Friend WithEvents mpvflagtx As System.Windows.Forms.TextBox
    Friend WithEvents plccflagtx As System.Windows.Forms.TextBox
    Friend WithEvents pctflagtx As System.Windows.Forms.TextBox
    Friend WithEvents plcrflagtx As System.Windows.Forms.TextBox
    Friend WithEvents pdwflagtx As System.Windows.Forms.TextBox
    Friend WithEvents pltunittx As System.Windows.Forms.TextBox
    Friend WithEvents mpvunittx As System.Windows.Forms.TextBox
    Friend WithEvents plccunittx As System.Windows.Forms.TextBox
    Friend WithEvents pctunittx As System.Windows.Forms.TextBox
    Friend WithEvents plcrunittx As System.Windows.Forms.TextBox
    Friend WithEvents pdwunittx As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents baspersenflagtx As System.Windows.Forms.TextBox
    Friend WithEvents baskresflagtx As System.Windows.Forms.TextBox
    Friend WithEvents neukresflagtx As System.Windows.Forms.TextBox
    Friend WithEvents baspersenunittx As System.Windows.Forms.TextBox
    Friend WithEvents baskresunittx As System.Windows.Forms.TextBox
    Friend WithEvents neukresunit As System.Windows.Forms.TextBox
    Friend WithEvents neukrestx As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents baspersentx As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents baskrestx As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents eospersenflagtx As System.Windows.Forms.TextBox
    Friend WithEvents eoskresflagtx As System.Windows.Forms.TextBox
    Friend WithEvents eospersenunittx As System.Windows.Forms.TextBox
    Friend WithEvents eoskresunittx As System.Windows.Forms.TextBox
    Friend WithEvents eospersentx As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents eoskrestx As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents neupersenflagtx As System.Windows.Forms.TextBox
    Friend WithEvents neupersenunittx As System.Windows.Forms.TextBox
    Friend WithEvents neupersentx As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents txtrcv As System.Windows.Forms.TextBox
    Friend WithEvents txtintip As System.Windows.Forms.TextBox
    Friend WithEvents txtbloodmessage As System.Windows.Forms.TextBox
    Friend WithEvents scatter_pb As System.Windows.Forms.PictureBox
    Friend WithEvents Label30 As System.Windows.Forms.Label

End Class
