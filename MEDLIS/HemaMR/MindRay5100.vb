﻿Imports System.Net.Sockets
Imports System.Text
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Reflection

Public Class MindRay5100
    Dim fpath As String
    Dim fInstrument As String
    Dim dataholder As New List(Of String)

    Dim logfilename As String

    Dim countertimer As Integer = 0
    Dim datacounter As New List(Of String)

    Private Delegate Sub UpdateTextDelegate(ByVal tb As TextBox, ByVal txt As String)

    Private Delegate Sub UpdateInfoDelegate(ByVal tb As TextBox, ByVal txt As String)

    Private Delegate Sub IntipDelegate(ByVal tb As TextBox, ByVal txt As String)


    Private clientPalio As TCPControl
    Dim clientSocket As New System.Net.Sockets.TcpClient()
    Dim serverStream As NetworkStream
    Dim iforascii As Integer
    Public mr_candoresult As Boolean

    Dim objx As MindRayBC3651

    Dim isclose As Boolean = False



    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConnect.Click
        Dim port As Integer
        port = 3600
        Try
            clientPalio = New TCPControl(txtipaddress.Text, CInt(Trim(txtport.Text)))
            If clientPalio.theClient.Connected Then
                'cmdConnect.Text = "connected"
                'cmdsendlist.Enabled = True
                Button2.Text = "connected"
                isclose = False
                Label1.Text = "Client Socket Program - Server Connected ..."
            End If

            AddHandler clientPalio.MessageReceive, AddressOf OnlineReceive
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try

    End Sub
    Dim mulainih As Boolean = False
    Dim selesai_saat_hasil As Boolean = False
    Private Sub OnlineReceive(ByVal sender As TCPControl, ByVal data As String)
        If isclose = False Then


            UpdateText(txtrcv, data)

            data = Trim(data)
            If data.Length > 1 Then
                countertimer = countertimer + 1
                ' dataholder.Add(data)
            Else
                'If dataholder.Count > 3 And data.Length < 2 Then
                '    mr_candoresult = True
                'End If
                If countertimer > 3 And data.Length < 2 Then
                    mr_candoresult = True
                End If

            End If
            ' IntipData(txtintip, CStr(countertimer))
            If data = Chr(16) Then
                clientPalio.SendData(Chr(6))

            End If






            If data = Chr(15) Then
                clientPalio.SendData(Chr(6))
                mr_candoresult = True
                'diberik flag kalo sudah terjadi misal now_you_can_read = true
            End If
        End If
    End Sub
    Private Sub UpdateText(ByVal tb As TextBox, ByVal txt As String)
        If tb.InvokeRequired Then
            tb.Invoke(New UpdateTextDelegate(AddressOf UpdateText), New Object() {tb, txt})
        Else
            If txt IsNot Nothing Then tb.AppendText(txt & vbCrLf)

        End If
    End Sub

    Private Sub IntipData(ByVal tb As TextBox, ByVal txt As String)
        If tb.InvokeRequired Then
            tb.Invoke(New intipDelegate(AddressOf IntipData), New Object() {tb, txt})
        Else
            If txt IsNot Nothing Then tb.Text = txt ' .AppendText(txt) ' & vbCrLf)

        End If
    End Sub

    Private Sub UpdateInfo(ByVal tb As RichTextBox, ByVal txt As String)
        If tb.InvokeRequired Then
            tb.Invoke(New UpdateTextDelegate(AddressOf UpdateText), New Object() {tb, txt})
        Else
            If txt IsNot Nothing Then tb.AppendText(txt & vbCrLf)

        End If
    End Sub
    'how to write to file ============
    'Private Sub cmdprint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdprint.Click
    '    Dim FileName As String = "darilist" & ".txt" '"Events_Data.txt"
    '    FileName = FileName.Replace(".txt", "_" & System.DateTime.Now.ToString("MM-dd-ss") & ".txt")
    '    fpath = System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.MyDocuments, FileName)

    '    Dim objWriter As New System.IO.StreamWriter(fpath)
    '    Dim i As Integer
    '    For i = 0 To ListBox1.Items.Count - 1
    '        objWriter.WriteLine(ListBox1.Items(i))

    '    Next
    '    objWriter.Close()

    'End Sub
    '================================
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Label1.Text = "disconnected"
        isclose = True
        clientPalio.isListening = False
        clientPalio.theClient.Close()
        clientPalio = Nothing
        Me.Close()

    End Sub

    Private Sub MindRay5100_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        isMindRayBC5100 = 0
    End Sub

    '=======write to file from txtrcv
    'Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
    '    Dim FileName As String = "darirtb" & ".txt" '"Events_Data.txt"
    '    FileName = FileName.Replace(".txt", "_" & System.DateTime.Now.ToString("MM-dd-ss") & ".txt")
    '    fpath = System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.MyDocuments, FileName)

    '    Dim objWriter As New System.IO.StreamWriter(fpath)

    '    objWriter.WriteLine(txtrcv.Text)


    '    objWriter.Close()
    'End Sub
    '======================================
    Private Sub WritelogMr(ByVal strper() As String)
        Dim FileName As String = "HemaLog" & ".txt" '"Events_Data.txt"
        FileName = FileName.Replace(".txt", "_" & System.DateTime.Now.ToString("MM-dd-hh-mm-ss") & ".txt")
        logfilename = System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.MyDocuments, FileName)

        Dim objWriter As New System.IO.StreamWriter(logfilename)
        Dim i As Integer
        For i = 0 To strper.Length - 1
            objWriter.WriteLine(strper(i))

        Next
        objWriter.Close()
    End Sub




    Private Sub MindRayBC36_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load



        Icon = New System.Drawing.Icon("medlis2.ico")
        txtrcv.Visible = False
        txtintip.Visible = False

        Try
            fpath = System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData, "ipadds5100.txt")
            If ReadInsProperty() = False Then
                MessageBox.Show("Silahkan diisi Ipaddress untuk MR5100")
            Else


                If fInstrument.Length = 0 Then
                    txtipaddress.Text = ""
                Else
                    txtipaddress.Text = fInstrument
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "MindRay5100/MindRayBC36_Load/1")
        End Try




        If developerMode = 0 Then
            Button1.Visible = False
            Button2.Visible = False
            txtrcv.Visible = False
            txtintip.Visible = False
            txtport.Visible = False
            txtport.ReadOnly = True
        ElseIf developerMode = 1 Then
            Button1.Visible = True
            Button2.Visible = True
            txtrcv.Visible = True
            txtintip.Visible = True
            txtport.Visible = True
            txtport.ReadOnly = False
        End If
        DisableText()
        isMindRayBC5100 = 1
        'developer mode
        '===============================
        Try
            Dim strlival As Integer
            Dim usbparent As Integer
            usbparent = Capability(890, 10)
            ReadLicense()
            strlival = GetLicenseFor("mr5100", usbparent)

            If CInt(Capability(521, 10)) <> strlival Then

                For Each ctrl As Control In Controls
                    If (ctrl.GetType() Is GetType(Button)) Then
                        Dim btn As Button = CType(ctrl, Button)
                        btn.Enabled = False
                    End If
                Next

                For Each ctrl As Control In GroupBox1.Controls
                    If (ctrl.GetType() Is GetType(Button)) Then
                        Dim btn As Button = CType(ctrl, Button)
                        btn.Enabled = False
                    End If
                Next

                MessageBox.Show("Anda tidak punya license")
                Exit Sub
            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "MindRay5100/MindRayBC36_Load/2")
        End Try
       

        '===============================
        'blood message
       

        '-------------------------------
        tmrresult.Enabled = True
        mr_candoresult = False
        ' txtrcv.Left = -1000
    End Sub

    Private Sub GetBloodDefaultMessage()
        If My.Settings.bloodmessage Is Nothing Then
            My.Settings.bloodmessage = New System.Collections.Specialized.StringCollection()
        End If

        
        ' TextBox1.Text = content
        If My.Settings.bloodmessage.Count < 1 Then
            Dim content As String = My.Resources.BloodMessageTxt
            Dim strperline() As String
            strperline = content.Split(vbCrLf)
            For Each item As String In strperline
                My.Settings.bloodmessage.Add(item)
            Next
            'For Each blg1 As String In My.Settings.bloodmessage
            '    TextBox1.Text = TextBox1.Text & blg1 & vbCrLf
            'Next
            'Else
            '    'For Each blg As String In My.Settings.bloodmessage
            '    '    TextBox1.Text = TextBox1.Text & blg & vbCrLf
            '    'Next
        End If
    End Sub

    Public Sub WriteInsProperty()
        Dim ipads As String

        If Trim(txtipaddress.Text).Length = 0 Then
            MessageBox.Show("ipaddress tidak boleh kosong")
        Else
            ipads = Trim(txtipaddress.Text)
        End If

        Dim objWriter As New System.IO.StreamWriter(fpath)
        objWriter.WriteLine(ipads)
        objWriter.Close()
    End Sub

    Private Sub tmrresult_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrresult.Tick
        Try
            If mr_candoresult = True Then
                mr_candoresult = False
                countertimer = 0
                Dim strperline() As String
                strperline = txtrcv.Text.Split(vbCrLf)

                If My.Settings.mrhemalog = 1 Then
                    WritelogMr(strperline)
                End If

                Dim i As Integer
                For i = 0 To strperline.Count - 1
                    'If i = 32 Then
                    '    MessageBox.Show(i)
                    'End If
                    If strperline(i).Length > 2 Then

                        If strperline(i).Contains("PV") Or strperline(i).Contains("MSH") Or strperline(i).Contains("OBX") Or strperline(i).Contains("OBR") Or strperline(i).Contains("PID") Then
                            If strperline(i).Contains("OBX") AndAlso strperline(i).Contains("ED") Then
                                dataholder.Add(strperline(i) & strperline(i + 1))
                            Else
                                dataholder.Add(strperline(i))
                            End If
                        End If
                    End If

                Next
                '   txtrcv.Clear()
                objx = New MindRayBC3651
                objx.Fill_and_do(dataholder)
                txtbloodmessage.Text = objx.findBloodMessage(dataholder)
                'DirectUpdate()
                UpdateDataBaseHeMaMR() 'objx.Hema_mr_barcode_idtest)
                dataholder.Clear()

                'Label2.Text = objx.Hema_mr_PCT
                'TextBox3.Text = objx.Hema_mr_PCT_value
                'Do_result 
                EnableText()
                SHowToText()
                DisableText()


            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "MindRay5100/tmrresult_Tick")
        End Try
        
    End Sub
    Private Sub EnableText()
        Dim ctrl As Control
        For Each ctrl In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.ReadOnly = False
            End If
        Next
        txtbarcode.ReadOnly = False
        txtbloodmessage.ReadOnly = False
    End Sub

    Private Sub DisableText()
        Dim ctrl As Control
        For Each ctrl In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.ReadOnly = True

            End If
        Next
        txtbarcode.ReadOnly = True
        txtbloodmessage.ReadOnly = True
    End Sub

    Private Sub SHowToText()
        '  On Error Resume Next

        txtbarcode.Text = objx.Hema_mr_barcode_idtest
        wbctext.Text = objx.Hema_mr_WBC_value '& " " & objx.Hema_mr_WBC & " " & objx.Hema_mr_WBC_refrange & " " & objx.Hema_mr_WBC_abnormal_flag & " " & objx.Hema_mr_WBC_unit
        wbcflagtx.Text = objx.Hema_mr_WBC_abnormal_flag
        wbcunittx.Text = objx.Hema_mr_WBC_unit

        lymkrestext.Text = objx.Hema_mr_LYM_kres_value '& " " & objx.Hema_mr_LYM_kres & " " & objx.Hema_mr_LYM_kres_refrange & " " & objx.Hema_mr_LYM_kres_abnormal_flag & " " & objx.Hema_mr_LYM_kres_unit
        lymkresflagtx.Text = objx.Hema_mr_LYM_kres_abnormal_flag
        lymkresunitunittx.Text = objx.Hema_mr_LYM_kres_unit

        lympersentext.Text = objx.Hema_mr_LYM_persen_value '& " " & objx.Hema_mr_LYM_persen & " " & objx.Hema_mr_LYM_persen_refrange & " " & objx.Hema_mr_LYM_persen_abnormal_flag & " " & objx.Hema_mr_LYM_persen_unit
        lympersenflagtx.Text = objx.Hema_mr_LYM_persen_abnormal_flag
        lympersenunittx.Text = objx.Hema_mr_LYM_persen_unit


        rbctext.Text = objx.Hema_mr_RBC_value '& " " & objx.Hema_mr_RBC & " " & objx.Hema_mr_RBC_refrange & " " & objx.Hema_mr_RBC_abnormal_flag & " " & objx.Hema_mr_RBC_unit
        rbcflagtx.Text = objx.Hema_mr_RBC_abnormal_flag
        rbcunittx.Text = objx.Hema_mr_RBC_unit

        hgbtext.Text = objx.Hema_mr_HGB_value '& " " & objx.Hema_mr_HGB & " " & objx.Hema_mr_HGB_refrange & " " & objx.Hema_mr_HGB_abnormal_flag & " " & objx.Hema_mr_HGB_unit
        hgbflagtx.Text = objx.Hema_mr_HGB_abnormal_flag
        hgbunittx.Text = objx.Hema_mr_HGB_unit


        mcvtext.Text = objx.Hema_mr_MCV_value '& " " & objx.Hema_mr_MCV & " " & objx.Hema_mr_MCV_refrange & " " & objx.Hema_mr_MCV_abnormal_flag & " " & objx.Hema_mr_MCV_unit
        mcvflagtx.Text = objx.Hema_mr_MCV_abnormal_flag
        mcvunittx.Text = objx.Hema_mr_MCV_unit

        mchtext.Text = objx.Hema_mr_MCH_value '& " " & objx.Hema_mr_MCH & " " & objx.Hema_mr_MCH_refrange & " " & objx.Hema_mr_MCH_abnormal_flag & " " & objx.Hema_mr_MCH_unit
        mchflagtx.Text = objx.Hema_mr_MCH_abnormal_flag
        mchunittx.Text = objx.Hema_mr_MCH_unit

        mchctext.Text = objx.Hema_mr_MCHC_value '& " " & objx.Hema_mr_MCHC & " " & objx.Hema_mr_MCHC_refrange & " " & objx.Hema_mr_MCHC_abnormal_flag & " " & objx.Hema_mr_MCHC_unit
        mchcflagtx.Text = objx.Hema_mr_MCHC_abnormal_flag
        mchcunittx.Text = objx.Hema_mr_MCHC_unit

        rdwcvtext.Text = objx.Hema_mr_RDW_CV_value '& " " & objx.Hema_mr_RDW_CV & " " & objx.Hema_mr_RDW_CV_refrange & " " & objx.Hema_mr_RDW_CV_abnormal_flag & " " & objx.Hema_mr_RDW_CV_unit
        rdwcvflagtx.Text = objx.Hema_mr_RDW_CV_abnormal_flag
        rdwcvunittx.Text = objx.Hema_mr_RDW_CV_unit

        rdwsdtext.Text = objx.Hema_mr_RDW_SD_value '& " " & objx.Hema_mr_RDW_SD & " " & objx.Hema_mr_RDW_SD_refrange & " " & objx.Hema_mr_RDW_SD_abnormal_flag & " " & objx.Hema_mr_RDW_SD_unit
        rdwsdflagtx.Text = objx.Hema_mr_RDW_SD_abnormal_flag
        rdwsdunittx.Text = objx.Hema_mr_RDW_SD_unit

        hcttext.Text = objx.Hema_mr_HCT_value '& " " & objx.Hema_mr_HCT & " " & objx.Hema_mr_HCT_refrange & " " & objx.Hema_mr_HCT_abnormal_flag & " " & objx.Hema_mr_HCT_unit
        hctflagtx.Text = objx.Hema_mr_HCT_abnormal_flag
        hctunittx.Text = objx.Hema_mr_HCT_unit


        plttext.Text = objx.Hema_mr_PLT_value '& " " & objx.Hema_mr_PLT & " " & objx.Hema_mr_PLT_refrange & " " & objx.Hema_mr_PLT_abnormal_flag & " " & objx.Hema_mr_PLT_unit
        pltflagtx.Text = objx.Hema_mr_PLT_abnormal_flag
        pltunittx.Text = objx.Hema_mr_PLT_unit

        mpvtext.Text = objx.Hema_mr_MPV_value '& " " & objx.Hema_mr_MPV & " " & objx.Hema_mr_MPV_refrange & " " & objx.Hema_mr_MPV_abnormal_flag & " " & objx.Hema_mr_MPV_unit
        mpvflagtx.Text = objx.Hema_mr_MPV_abnormal_flag
        mpvunittx.Text = objx.Hema_mr_MPV_unit

        pdwtext.Text = objx.Hema_mr_PDW_value '& " " & objx.Hema_mr_PDW & " " & objx.Hema_mr_PDW_refrange & " " & objx.Hema_mr_PDW_abnormal_flag & " " & objx.Hema_mr_PDW_unit
        pdwflagtx.Text = objx.Hema_mr_PDW_abnormal_flag
        pdwunittx.Text = objx.Hema_mr_PDW_unit

        pcttext.Text = objx.Hema_mr_PCT_value '& " " & objx.Hema_mr_PCT & " " & objx.Hema_mr_PCT_refrange & " " & objx.Hema_mr_PCT_abnormal_flag & " " & objx.Hema_mr_PCT_unit
        pctflagtx.Text = objx.Hema_mr_PCT_abnormal_flag
        pctunittx.Text = objx.Hema_mr_PCT_unit

        midkrestext.Text = objx.Hema_mr_MID_kres_value '& " " & objx.Hema_mr_MID_kres & " " & objx.Hema_mr_MID_kres_refrange & " " & objx.Hema_mr_MID_kres_abnormal_flag & " " & objx.Hema_mr_MID_kres_unit
        midkresflagtx.Text = objx.Hema_mr_MID_kres_abnormal_flag
        midkresunittx.Text = objx.Hema_mr_MID_kres_unit

        midpersentext.Text = objx.Hema_mr_MID_persen_value '& " " & objx.Hema_mr_MID_persen & " " & objx.Hema_mr_MID_persen_refrange & " " & objx.Hema_mr_MID_persen_abnormal_flag & " " & objx.Hema_mr_MID_persen_unit
        midpersenflagtx.Text = objx.Hema_mr_MID_persen_abnormal_flag
        midpersenunittx.Text = objx.Hema_mr_MID_persen_unit

        grankrestext.Text = objx.Hema_mr_GRAN_kres_value '& " " & objx.Hema_mr_GRAN_kres & " " & objx.Hema_mr_GRAN_kres_refrange & " " & objx.Hema_mr_GRAN_kres_abnormal_flag & " " & objx.Hema_mr_GRAN_kres_unit
        grandkresflagtx.Text = objx.Hema_mr_GRAN_kres_abnormal_flag
        grandkresunittx.Text = objx.Hema_mr_GRAN_kres_unit

        grandpersentext.Text = objx.Hema_mr_GRAN_persen_value '& " " & objx.Hema_mr_GRAN_persen & " " & objx.Hema_mr_GRAN_persen_refrange & " " & objx.Hema_mr_GRAN_persen_abnormal_flag & " " & objx.Hema_mr_GRAN_persen_unit
        grandpersenflagtx.Text = objx.Hema_mr_GRAN_persen_abnormal_flag
        grandpersenunittx.Text = objx.Hema_mr_GRAN_persen_unit

        plcctext.Text = objx.Hema_mr_PLCC_value '& " " & objx.Hema_mr_PLCC & " " & objx.Hema_mr_PLCC_refrange & " " & objx.Hema_mr_PLCC_abnormal_flag & " " & objx.Hema_mr_PLCC_unit
        plccflagtx.Text = objx.Hema_mr_PLCC_abnormal_flag
        plccunittx.Text = objx.Hema_mr_PLCC_unit

        plcrtext.Text = objx.Hema_mr_PLCR_value '& " " & objx.Hema_mr_PLCR & " " & objx.Hema_mr_PLCR_refrange & " " & objx.Hema_mr_PLCR_abnormal_flag & " " & objx.Hema_mr_PLCR_unit
        plcrflagtx.Text = objx.Hema_mr_PLCR_abnormal_flag
        plcrunittx.Text = objx.Hema_mr_PLCR_unit

        baskrestx.Text = objx.Hema_mr_bas_kres_value
        baskresflagtx.Text = objx.Hema_mr_bas_kres_abnormal_flag
        baskresunittx.Text = objx.Hema_mr_bas_kres_unit

        baspersentx.Text = objx.Hema_mr_bas_persen_value
        baspersenflagtx.Text = objx.Hema_mr_bas_persen_abnormal_flag
        baspersenunittx.Text = objx.Hema_mr_bas_persen_unit

        neukrestx.Text = objx.Hema_mr_neu_kres_value
        neukresflagtx.Text = objx.Hema_mr_neu_kres_abnormal_flag
        neukresunit.Text = objx.Hema_mr_neu_kres_unit

        neupersentx.Text = objx.Hema_mr_neu_persen_value
        neupersenflagtx.Text = objx.Hema_mr_neu_persen_abnormal_flag
        neupersenunittx.Text = objx.Hema_mr_neu_persen_unit

        eospersentx.Text = objx.Hema_mr_eos_persen_value
        eospersenflagtx.Text = objx.Hema_mr_eos_persen_abnormal_flag
        eospersenunittx.Text = objx.Hema_mr_eos_persen_unit

        eoskrestx.Text = objx.Hema_mr_eos_kres_value
        eoskresflagtx.Text = objx.Hema_mr_eos_kres_abnormal_flag
        eoskresunittx.Text = objx.Hema_mr_eos_kres_unit


        On Error Resume Next


        Dim strwbcBase64 As String = Trim(objx.Hema_mr_wbc_histogram_bmp_value)
        strwbcBase64 = strwbcBase64.Replace(" ", "+")
        Dim wbcimageBytes As Byte() = Convert.FromBase64String(strwbcBase64)
        Dim wbcstrm As New MemoryStream(wbcimageBytes)
        wbc_pb.Image = Image.FromStream(wbcstrm)


        Dim strrbcBase64 As String = Trim(objx.Hema_mr_rbc_histogram_bmp_value)
        strrbcBase64 = strrbcBase64.Replace(" ", "+")
        Dim rbcimageBytes As Byte() = Convert.FromBase64String(strrbcBase64)
        Dim rbcstrm As New MemoryStream(rbcimageBytes)
        rbc_pb.Image = Image.FromStream(rbcstrm)

        Dim strpltBase64 As String = Trim(objx.Hema_mr_plt_histogram_bmp_value)
        strpltBase64 = strpltBase64.Replace(" ", "+")
        Dim pltimageBytes As Byte() = Convert.FromBase64String(strpltBase64)
        Dim pltstrm As New MemoryStream(pltimageBytes)
        plt_pb.Image = Image.FromStream(pltstrm)

        '=scatter gram
        Dim strscatterBase64 As String = Trim(objx.Hema_mr_wbc_scatterdiff_bmp_value) ' _histogram_bmp_value)
        strscatterBase64 = strscatterBase64.Replace(" ", "+")
        Dim wbcscatterimageBytes As Byte() = Convert.FromBase64String(strscatterBase64)
        Dim wbcscatterstrm As New MemoryStream(wbcscatterimageBytes)
        scatter_pb.Image = Image.FromStream(wbcscatterstrm)


        Save_Chart(objx.Hema_mr_barcode_idtest, wbcimageBytes, rbcimageBytes, pltimageBytes, wbcscatterimageBytes)


        ' Catch ex As Exception

        'MessageBox.Show(ex.ToString) '("Data tidak bisa dijadikan gambar")
        'End Try


    End Sub
    Private Sub Save_Chart(ByVal patientbarcode As String, ByVal wbcchart As Byte(), ByVal rbcchart As Byte(), ByVal pltchart As Byte(), ByVal wbcscatter As Byte())
        Dim hemaogre As New clsGreConnect
        hemaogre.buildConn()



        'id serial NOT NULL,
        'barcode text,
        'itemname text,
        'result bytea,
        'datefinish text,
        'uf1 text,
        'uf2 text,
        'uf3 text,
        Dim datecomplete As String
        datecomplete = Format(Now, "yyyyMMddhhmmss")

        '   strsql = "insert into hemachart(barcode,itemname,result,datefinish)
        'Dim strinsert As String
        'strinsert = "insert into hemachartmr(barcode,wbcchart,rbcchart,pltchart,uf1,datefinish)" & _
        '"values(:barcode,:wbcchart,:rbcchart,:pltchart,:uf1,:datefinish)"

        ' strinsert = "insert into hemachartmr(barcode,wbcchart,rbcchart,pltchart,datefinish)values('" & patientbarcode & "','" & wbcchart & "','" & rbcchart & "','" & pltchart & "','" & datecomplete & "')"



        'Step 2 : Now to add the picture to the DB
        'Declare a byte array photo and read the image in to it
        ' Dim photo() As Byte = getphoto(ofd1.FileName) ' getPhoto is a User Defined Fn and is defined at the end
        'ofd1 is the name of File Open Box in the form
        'Now create an SqlClient.SqlCommand obj
        'conn is the connection
        Dim C1 As SqlClient.SqlCommand = New SqlClient.SqlCommand("insert into hemachartmr(barcode,wbcchart,rbcchart,pltchart,wbcscatter,datefinish)" & _
        "values(?,?,?,?,?,?)", hemaogre.grecon)

        'Pass the parameters
        C1.Parameters.Add("@barcode", SqlDbType.VarChar, 25).Value = patientbarcode
        C1.Parameters.Add("@wbcchart", SqlDbType.Binary, wbcchart.Length).Value = wbcchart
        C1.Parameters.Add("@rbcchart", SqlDbType.Binary, rbcchart.Length).Value = rbcchart
        C1.Parameters.Add("@pltchart", SqlDbType.Binary, pltchart.Length).Value = pltchart
        C1.Parameters.Add("@wbcscatter", SqlDbType.Binary, wbcscatter.Length).Value = wbcscatter
        C1.Parameters.Add("@datefinish", SqlDbType.VarChar, 20).Value = datecomplete

        C1.ExecuteNonQuery()
        C1.Dispose()

        'cmdinsert.Parameters.AddWithValue("@barcode", barcode)
        'cmdinsert.Parameters.AddWithValue("@wbcchart", wbcchart)
        'cmdinsert.Parameters.AddWithValue("@rbcchart", rbcchart)
        'cmdinsert.Parameters.AddWithValue("@pltchart", pltchart)
        'cmdinsert.Parameters.AddWithValue("@datefinish", datecomplete)


        'cmdinsert.ExecuteNonQuery()
        'cmdinsert.Dispose()
        'hemaogre.ExecuteNonQuery(strinsert)
        hemaogre.CloseConn()

    End Sub


    Private Sub DirectUpdate()


        'If fieldName = C_MR_WBC Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_WBC, objx.Hema_mr_WBC_value, objx.Hema_mr_WBC_unit, objx.Hema_mr_WBC_abnormal_flag, objx.Hema_mr_WBC_refrange)
        'End If
        'If fieldName = C_MR_LYM_kres Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_LYM_kres, objx.Hema_mr_LYM_kres_value, objx.Hema_mr_LYM_kres_unit, objx.Hema_mr_LYM_kres_abnormal_flag, objx.Hema_mr_LYM_kres_refrange)
        'End If

        'If fieldName = C_MR_LYM_persen Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_LYM_persen, objx.Hema_mr_LYM_persen_value, objx.Hema_mr_LYM_persen_unit, objx.Hema_mr_LYM_persen_abnormal_flag, objx.Hema_mr_LYM_persen_refrange)
        'End If 'sampai disini sholat dulu

        'If fieldName = C_MR_RBC Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_RBC, objx.Hema_mr_RBC_value, objx.Hema_mr_RBC_unit, objx.Hema_mr_RBC_abnormal_flag, objx.Hema_mr_RBC_refrange)
        'End If
        'If fieldName = C_MR_HGB Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_HGB, objx.Hema_mr_HGB_value, objx.Hema_mr_HGB_unit, objx.Hema_mr_HGB_abnormal_flag, objx.Hema_mr_HGB_refrange)
        'End If

        'If fieldName = C_MR_MCV Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_MCV, objx.Hema_mr_MCV_value, objx.Hema_mr_MCV_unit, objx.Hema_mr_MCV_abnormal_flag, objx.Hema_mr_MCV_refrange)
        'End If
        'If fieldName = C_MR_MCH Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_MCH, objx.Hema_mr_MCH_value, objx.Hema_mr_MCH_unit, objx.Hema_mr_MCH_abnormal_flag, objx.Hema_mr_MCH_refrange)
        'End If

        'If fieldName = C_MR_MCHC Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_MCHC, objx.Hema_mr_MCHC_value, objx.Hema_mr_MCHC_unit, objx.Hema_mr_MCHC_abnormal_flag, objx.Hema_mr_MCHC_refrange)
        'End If
        'If fieldName = C_MR_RDW_CV Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_RDW_CV, objx.Hema_mr_RDW_CV_value, objx.Hema_mr_RDW_CV_unit, objx.Hema_mr_RDW_CV_abnormal_flag, objx.Hema_mr_RDW_CV_refrange)
        'End If
        'If fieldName = C_MR_RDW_SD Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_RDW_SD, objx.Hema_mr_RDW_SD_value, objx.Hema_mr_RDW_SD_unit, objx.Hema_mr_RDW_SD_abnormal_flag, objx.Hema_mr_RDW_SD_refrange)
        'End If
        'If fieldName = C_MR_PLT Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_PLT, objx.Hema_mr_PLT_value, objx.Hema_mr_PLT_unit, objx.Hema_mr_PLT_abnormal_flag, objx.Hema_mr_PLT_refrange)
        'End If

        'If fieldName = C_MR_HCT Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_HCT, objx.Hema_mr_HCT_value, objx.Hema_mr_HCT_unit, objx.Hema_mr_HCT_abnormal_flag, objx.Hema_mr_HCT_refrange)
        'End If
        'If fieldName = C_MR_MPV Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_MPV, objx.Hema_mr_MPV_value, objx.Hema_mr_MPV_unit, objx.Hema_mr_MPV_abnormal_flag, objx.Hema_mr_MPV_refrange)
        'End If
        'If fieldName = C_MR_PDW Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_PDW, objx.Hema_mr_PDW_value, objx.Hema_mr_PDW_unit, objx.Hema_mr_PDW_abnormal_flag, objx.Hema_mr_PDW_refrange)
        'End If
        'If fieldName = C_MR_PCT Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_PCT, objx.Hema_mr_PCT_value, objx.Hema_mr_PCT_unit, objx.Hema_mr_PCT_abnormal_flag, objx.Hema_mr_PCT_refrange)
        'End If
        'If fieldName = C_MR_MID_kres Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_MID_kres, objx.Hema_mr_MID_kres_value, objx.Hema_mr_MID_kres_unit, objx.Hema_mr_MID_kres_abnormal_flag, objx.Hema_mr_MID_kres_refrange)
        'End If
        'If fieldName = C_MR_MID_persen Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_MID_persen, objx.Hema_mr_MID_persen_value, objx.Hema_mr_MID_persen_unit, objx.Hema_mr_MID_persen_abnormal_flag, objx.Hema_mr_MID_persen_refrange)
        'End If
        'If fieldName = C_MR_GRAN_kres Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_GRAN_kres, objx.Hema_mr_GRAN_kres_value, objx.Hema_mr_GRAN_kres_unit, objx.Hema_mr_GRAN_kres_abnormal_flag, objx.Hema_mr_GRAN_kres_refrange)
        'End If
        'If fieldName = C_MR_GRAN_persen Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_GRAN_persen, objx.Hema_mr_GRAN_persen_value, objx.Hema_mr_GRAN_persen_unit, objx.Hema_mr_GRAN_persen_abnormal_flag, objx.Hema_mr_GRAN_persen_refrange)
        'End If
        'If fieldName = C_MR_PLCC Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_PLCC, objx.Hema_mr_PLCC_value, objx.Hema_mr_PLCC_unit, objx.Hema_mr_PLCC_abnormal_flag, objx.Hema_mr_PLCC_refrange)
        'End If
        'If fieldName = C_MR_PLCR Then
        executeUpdate(objx.Hema_mr_barcode_idtest, C_MR_PLCR, objx.Hema_mr_PLCR_value, objx.Hema_mr_PLCR_unit, objx.Hema_mr_PLCR_abnormal_flag, objx.Hema_mr_PLCR_refrange)
        'End If

        'benerin(disini)
    End Sub

    Private Sub UpdateDataBaseHeMaMR()

        Try
            '==== delete before insert
            Dim sqldel As String
            Dim delbarcode As String

            delbarcode = objx.Hema_mr_barcode_idtest
            sqldel = "delete from hemadatamr where barcode='" & delbarcode & "'"
            Dim delcn As New clsGreConnect
            delcn.buildConn()
            delcn.ExecuteNonQuery(sqldel)
            delcn.CloseConn()

            '========================

            ' Get the type of this instance 
            Dim t As Type = objx.GetType
            Dim fieldName As String
            Dim propertyValue As Object

            ' Use each property of the business object passed in 
            For Each pi As PropertyInfo In t.GetProperties(BindingFlags.Instance Or BindingFlags.Public Or BindingFlags.NonPublic)
                ' Get the name and value of the property 
                fieldName = pi.Name

                ' Get the value of the property 
                propertyValue = pi.GetValue(objx, Nothing)
                'Console.WriteLine(fieldName & ": " & If(propertyValue Is Nothing, "Nothing", propertyValue.ToString))
                'fielname nama property-nya
                'propertyvalue adalah nilainya

                If propertyValue = C_MR_WBC Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_WBC_value, objx.Hema_mr_WBC_unit, objx.Hema_mr_WBC_abnormal_flag, objx.Hema_mr_WBC_refrange)
                End If
                If propertyValue = C_MR_LYM_kres Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_LYM_kres_value, objx.Hema_mr_LYM_kres_unit, objx.Hema_mr_LYM_kres_abnormal_flag, objx.Hema_mr_LYM_kres_refrange)
                End If

                If propertyValue = C_MR_LYM_persen Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_LYM_persen_value, objx.Hema_mr_LYM_persen_unit, objx.Hema_mr_LYM_persen_abnormal_flag, objx.Hema_mr_LYM_persen_refrange)
                End If 'sampai disini sholat dulu

                If propertyValue = C_MR_RBC Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_RBC_value, objx.Hema_mr_RBC_unit, objx.Hema_mr_RBC_abnormal_flag, objx.Hema_mr_RBC_refrange)
                End If
                If propertyValue = C_MR_HGB Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_HGB_value, objx.Hema_mr_HGB_unit, objx.Hema_mr_HGB_abnormal_flag, objx.Hema_mr_HGB_refrange)
                End If

                If propertyValue = C_MR_MCV Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_MCV_value, objx.Hema_mr_MCV_unit, objx.Hema_mr_MCV_abnormal_flag, objx.Hema_mr_MCV_refrange)
                End If
                If propertyValue = C_MR_MCH Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_MCH_value, objx.Hema_mr_MCH_unit, objx.Hema_mr_MCH_abnormal_flag, objx.Hema_mr_MCH_refrange)
                End If

                If propertyValue = C_MR_MCHC Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_MCHC_value, objx.Hema_mr_MCHC_unit, objx.Hema_mr_MCHC_abnormal_flag, objx.Hema_mr_MCHC_refrange)
                End If
                If propertyValue = C_MR_RDW_CV Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_RDW_CV_value, objx.Hema_mr_RDW_CV_unit, objx.Hema_mr_RDW_CV_abnormal_flag, objx.Hema_mr_RDW_CV_refrange)
                End If
                If propertyValue = C_MR_RDW_SD Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_RDW_SD_value, objx.Hema_mr_RDW_SD_unit, objx.Hema_mr_RDW_SD_abnormal_flag, objx.Hema_mr_RDW_SD_refrange)
                End If
                If propertyValue = C_MR_PLT Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_PLT_value, objx.Hema_mr_PLT_unit, objx.Hema_mr_PLT_abnormal_flag, objx.Hema_mr_PLT_refrange)
                End If

                If propertyValue = C_MR_HCT Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_HCT_value, objx.Hema_mr_HCT_unit, objx.Hema_mr_HCT_abnormal_flag, objx.Hema_mr_HCT_refrange)
                End If
                If propertyValue = C_MR_MPV Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_MPV_value, objx.Hema_mr_MPV_unit, objx.Hema_mr_MPV_abnormal_flag, objx.Hema_mr_MPV_refrange)
                End If
                If propertyValue = C_MR_PDW Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_PDW_value, objx.Hema_mr_PDW_unit, objx.Hema_mr_PDW_abnormal_flag, objx.Hema_mr_PDW_refrange)
                End If
                If propertyValue = C_MR_PCT Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_PCT_value, objx.Hema_mr_PCT_unit, objx.Hema_mr_PCT_abnormal_flag, objx.Hema_mr_PCT_refrange)
                End If
                If propertyValue = C_MR_MID_kres Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_MID_kres_value, objx.Hema_mr_MID_kres_unit, objx.Hema_mr_MID_kres_abnormal_flag, objx.Hema_mr_MID_kres_refrange)
                End If
                If propertyValue = C_MR_MID_persen Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_MID_persen_value, objx.Hema_mr_MID_persen_unit, objx.Hema_mr_MID_persen_abnormal_flag, objx.Hema_mr_MID_persen_refrange)
                End If
                If propertyValue = C_MR_GRAN_kres Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_GRAN_kres_value, objx.Hema_mr_GRAN_kres_unit, objx.Hema_mr_GRAN_kres_abnormal_flag, objx.Hema_mr_GRAN_kres_refrange)
                End If
                If propertyValue = C_MR_GRAN_persen Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_GRAN_persen_value, objx.Hema_mr_GRAN_persen_unit, objx.Hema_mr_GRAN_persen_abnormal_flag, objx.Hema_mr_GRAN_persen_refrange)
                End If
                If propertyValue = C_MR_PLCC Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_PLCC_value, objx.Hema_mr_PLCC_unit, objx.Hema_mr_PLCC_abnormal_flag, objx.Hema_mr_PLCC_refrange)
                End If
                If propertyValue = C_MR_PLCR Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_PLCR_value, objx.Hema_mr_PLCR_unit, objx.Hema_mr_PLCR_abnormal_flag, objx.Hema_mr_PLCR_refrange)
                End If

                If propertyValue = C_MR_BAS_kres Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_bas_kres_value, objx.Hema_mr_bas_kres_unit, objx.Hema_mr_bas_kres_abnormal_flag, objx.Hema_mr_bas_kres_refrange)
                End If

                If propertyValue = C_MR_BAS_persen Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_bas_persen_value, objx.Hema_mr_bas_persen_unit, objx.Hema_mr_bas_persen_abnormal_flag, objx.Hema_mr_bas_persen_refrange)
                End If


                If propertyValue = C_MR_NEU_persen Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_neu_persen_value, objx.Hema_mr_neu_persen_unit, objx.Hema_mr_neu_persen_abnormal_flag, objx.Hema_mr_neu_persen_refrange)
                End If

                If propertyValue = C_MR_NEU_kres Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_neu_kres_value, objx.Hema_mr_neu_kres_unit, objx.Hema_mr_neu_kres_abnormal_flag, objx.Hema_mr_neu_kres_refrange)
                End If

                If propertyValue = C_MR_EOS_kres Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_eos_kres_value, objx.Hema_mr_eos_kres_unit, objx.Hema_mr_eos_kres_abnormal_flag, objx.Hema_mr_eos_kres_refrange)
                End If

                If propertyValue = C_MR_EOS_persen Then
                    executeUpdate(objx.Hema_mr_barcode_idtest, propertyValue, objx.Hema_mr_eos_persen_value, objx.Hema_mr_eos_persen_unit, objx.Hema_mr_eos_persen_abnormal_flag, objx.Hema_mr_eos_persen_refrange)
                End If

                'objx = Nothing

            Next
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "MindRay5100/UpdateDataBaseHeMaMR")
        End Try
        


    End Sub
    Private Sub executeUpdate(ByVal barcode As String, ByVal testname As String, ByVal measurevalue As String, ByVal unit As String, ByVal abflag As String, ByVal refrange As String)
        '        Try
        Try
            Dim strsql As String

            Dim resultstatus As String
            Dim datecomplete As String
            Dim machineorhuman As String
            Dim iduser As Integer

            Dim status As Integer
            Dim idmesin As Integer = 1
            Dim lowernormal As String = ""
            Dim uppernormal As String = ""
            Dim lowercritical As String = ""
            Dim uppercritical As String = ""

            Dim regexe As RegularExpressions.Regex
            Dim strbatas As String()
            strbatas = Regex.Split(refrange, "\-")
            machineorhuman = "1"
            resultstatus = "F"
            status = FINISHSAMPLE
            lowernormal = strbatas.ElementAt(0)
            uppernormal = strbatas.ElementAt(1)
            datecomplete = Format(Now, "yyyyMMddhhmmss")

            Dim dbcon As New clsGreConnect
            dbcon.buildConn()
            'refrange = "L N"
            barcode = UCase(barcode)
            strsql = "update jobdetail " & _
                        "set measurementvalue='" & measurevalue & "',resultstatus='" & resultstatus & "'" & _
                        ",resultabnormalflag='" & abflag & "'" & _
                        ",datecomplete='" & datecomplete & "',referencerange='" & refrange & "'" & _
                        ",machineorhuman='" & machineorhuman & "' ,iduser='" & iduser & "',idmachine= '" & idmesin & "',unit='" & unit & "',status='" & status & "'" & _
                        ",lowernormal='" & lowernormal & "' ,uppernormal='" & uppernormal & "',lowercritical='" & lowercritical & "',uppercritical='" & uppercritical & "'" & _
                        " where barcode='" & barcode & "' and universaltest='" & testname & "'"



            'Dim cmd As New SqlClient.SqlCommand(strsql, dbcon.grecon)
            'Debug.Write(strsql)
            'cmd.Parameters.AddWithValue("@barcode", barcode)
            'cmd.Parameters.AddWithValue("@resultstatus", resultstatus)
            'cmd.Parameters.AddWithValue("@abflag", abflag)
            'cmd.Parameters.AddWithValue("@datecomplete", datecomplete)
            'cmd.Parameters.AddWithValue("@refrange", refrange)
            'cmd.Parameters.AddWithValue("@machineorhuman", machineorhuman)
            'cmd.Parameters.AddWithValue("@iduser", iduser)
            'cmd.Parameters.AddWithValue("@measurevalue", measurevalue)
            'cmd.Parameters.AddWithValue("@idmachine", idmesin)
            'cmd.Parameters.AddWithValue("@unit", unit)
            'cmd.Parameters.AddWithValue("@status", status)
            'cmd.Parameters.AddWithValue("@lowernormal", lowernormal)
            'cmd.Parameters.AddWithValue("@uppernormal", uppernormal)
            'cmd.Parameters.AddWithValue("@lowercritical", lowercritical)
            'cmd.Parameters.AddWithValue("@uppercritical", uppercritical)
            'cmd.Parameters.AddWithValue("@testname", testname)
            'cmd.ExecuteNonQuery()
            'cmd.Dispose()
            dbcon.ExecuteNonQuery(strsql)
            dbcon.CloseConn()


            '=============================================================
            'Dim ogre As New clsGreConnect
            'ogre.buildConn()
            'ogre.ExecuteNonQuery(strsql)
            'ogre.CloseConn()
            '===============================
            'insert into hemadata table
            'barcode
            'ItemName
            'Result
            'Unit
            'Reference
            'datefinish ---> get from datecomplete

            Dim hemaogre As New clsGreConnect
            hemaogre.buildConn()
            Dim strinsert As String
            strinsert = "insert into hemadatamr(barcode,itemname,result,unit,refrange,datefinish,abnormalflag)" & _
            "values('" & barcode & "','" & testname & "','" & measurevalue & "','" & unit & "','" & refrange & "','" & datecomplete & "','" & abflag & "')"


            'Dim cmdinsert As New SqlClient.SqlCommand(strinsert, hemaogre.grecon)
            'cmdinsert.Parameters.AddWithValue("@barcode", barcode)
            'cmdinsert.Parameters.AddWithValue("@hematest", testname)
            'cmdinsert.Parameters.AddWithValue("@abflag", abflag)
            'cmdinsert.Parameters.AddWithValue("@datecomplete", datecomplete)
            'cmdinsert.Parameters.AddWithValue("@refrange", refrange)
            'cmdinsert.Parameters.AddWithValue("@hematestvalue", measurevalue)
            'cmdinsert.Parameters.AddWithValue("@unit", unit)
            '' cmdinsert.Parameters.AddWithValue("@idmachine", idmesin)

            'cmdinsert.ExecuteNonQuery()
            'cmdinsert.Dispose()
            hemaogre.ExecuteNonQuery(strinsert)
            'Catch ex As Exception
            '    MessageBox.Show(ex.ToString)
            'End Try
            hemaogre.CloseConn()
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "MindRay5100/executeUpdate")
        End Try
        
        '=============================================================
    End Sub

    Private Sub Do_result()
        Try
            Dim objMR As New MindRayBC3651
            Dim i As Integer
            Dim idtest_barcode As String

            For i = 0 To dataholder.Count - 1
                If Trim(objMR.IdentifySingleMessage(dataholder(i))) = C_MR_OBR Then
                    idtest_barcode = objMR.Identify_OBR_get_barcode_idtest(dataholder(i))

                ElseIf Trim(objMR.IdentifySingleMessage(dataholder(i))) = C_MR_OBX Then
                    Dim obj_msgdata As New MsgData
                    obj_msgdata = objMR.Identify_OBX(dataholder(i))

                    If Trim(obj_msgdata.TestName) = C_MR_WBC Then

                        '=====here display to user
                        'TextBox2.Text = obj_msgdata.TestValue
                        'Label1.Text = obj_msgdata.TestName
                        '=========================
                    End If


                End If
                ' objMR = Nothing
            Next
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "MindRay5100/Do_result")
        End Try

    End Sub


    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        mr_candoresult = True
    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Declare a dataset
        Dim ds As New DataSet
        'declare a byte array
        Dim b() As Byte
        'populate dataset using a user defined fn getDataSet - the code is given at the end
        getDataSet("select wbcchart from hemachartmr where barcode='18'", ds)
        'Now get the pic details back to a binary array
        b = ds.Tables(0).Rows(0).Item("wbcchart")
        'Create a temp Picture in HDD using a user defined fn WritePhoto
        writephoto(b)
    End Sub
    Function getphoto(ByVal filePath As String) As Byte()
        Dim fs As FileStream = New FileStream(filePath, FileMode.Open, FileAccess.Read)
        Dim br As BinaryReader = New BinaryReader(fs)
        Dim photo() As Byte = br.ReadBytes(fs.Length)
        br.Close()
        fs.Close()
        Return photo
    End Function

    Function getDataSet(ByVal myquery As String, ByRef ds1 As DataSet) As Integer
        'Function for populating a dataset
        Dim ogrecon As New clsGreConnect
        ogrecon.buildConn()
        Dim rowcount As Integer
        Dim da_getdataset As New SqlClient.SqlDataAdapter (myquery, ogrecon.grecon)
        Try
            ds1.Reset()
            rowcount = da_getdataset.Fill(ds1)
            Return rowcount
        Catch ex As Exception
            MessageBox.Show(myquery & "," & ex.Message, "Error while populating dataset")
            Return 0
        Finally
            ds1.Dispose()
            da_getdataset.Dispose()
        End Try
    End Function

    Function writephoto(ByVal photobyte() As Byte)

        Dim fs1 As FileStream = New FileStream(My.Computer.FileSystem.SpecialDirectories.MyDocuments + "ambildrdb.png", FileMode.OpenOrCreate, FileAccess.ReadWrite)
        fs1.Write(photobyte, 0, photobyte.Length)
        ' Set that image to a Picture Box
        PictureBox1.Image = Image.FromStream(fs1)
        fs1.Close()

    End Function


    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles eospersentx.TextChanged

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        WriteInsProperty()
    End Sub
    Private Function ReadInsProperty() As Boolean

        Dim InsName As String
        Dim result As Boolean
        Dim getstr As String
        Dim position As Integer
        If System.IO.File.Exists(fpath) Then
            Try
                Dim lines() As String = IO.File.ReadAllLines(fpath)
                Dim k As Integer
                k = 0
                For Each line As String In lines

                    position = InStr(line, "=")
                    For i = position To Len(line) - 1
                        getstr = getstr + line.Substring(i, 1)
                    Next
                    InsName = getstr
                    getstr = ""
                Next

                fInstrument = InsName
                result = True
            Catch ex As Exception
                MessageBox.Show("Please specify instrument read mode", "File not found", MessageBoxButtons.OK)
            End Try
        Else
            result = False
        End If
        Return result

    End Function

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub

    Private Sub txtrcv_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class
