﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HemaAllFinishJobsFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(HemaAllFinishJobsFrm))
        Me.dgvJoblist = New System.Windows.Forms.DataGridView
        Me.dgvdetail = New System.Windows.Forms.DataGridView
        Me.lblStatus = New System.Windows.Forms.Label
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.mnuCetak = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.rd30 = New System.Windows.Forms.RadioButton
        Me.rd80 = New System.Windows.Forms.RadioButton
        Me.rdsemua = New System.Windows.Forms.RadioButton
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.cmdchart = New System.Windows.Forms.Button
        CType(Me.dgvJoblist, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvdetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvJoblist
        '
        Me.dgvJoblist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvJoblist.Location = New System.Drawing.Point(6, 116)
        Me.dgvJoblist.Name = "dgvJoblist"
        Me.dgvJoblist.Size = New System.Drawing.Size(380, 520)
        Me.dgvJoblist.TabIndex = 3
        '
        'dgvdetail
        '
        Me.dgvdetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvdetail.Location = New System.Drawing.Point(407, 116)
        Me.dgvdetail.Name = "dgvdetail"
        Me.dgvdetail.Size = New System.Drawing.Size(468, 486)
        Me.dgvdetail.TabIndex = 15
        '
        'lblStatus
        '
        Me.lblStatus.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(12, 55)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblStatus.Size = New System.Drawing.Size(218, 27)
        Me.lblStatus.TabIndex = 24
        Me.lblStatus.Text = "....."
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCetak, Me.ToolStripSeparator1})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(887, 46)
        Me.ToolStrip1.TabIndex = 33
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'mnuCetak
        '
        Me.mnuCetak.Image = Global.MEDLIS.My.Resources.Resources.print_go_hover
        Me.mnuCetak.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuCetak.Name = "mnuCetak"
        Me.mnuCetak.Size = New System.Drawing.Size(73, 43)
        Me.mnuCetak.Text = "Cetak"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 46)
        '
        'rd30
        '
        Me.rd30.AutoSize = True
        Me.rd30.Location = New System.Drawing.Point(15, 21)
        Me.rd30.Name = "rd30"
        Me.rd30.Size = New System.Drawing.Size(41, 19)
        Me.rd30.TabIndex = 36
        Me.rd30.TabStop = True
        Me.rd30.Text = "30"
        Me.rd30.UseVisualStyleBackColor = True
        '
        'rd80
        '
        Me.rd80.AutoSize = True
        Me.rd80.Location = New System.Drawing.Point(77, 21)
        Me.rd80.Name = "rd80"
        Me.rd80.Size = New System.Drawing.Size(41, 19)
        Me.rd80.TabIndex = 37
        Me.rd80.TabStop = True
        Me.rd80.Text = "80"
        Me.rd80.UseVisualStyleBackColor = True
        '
        'rdsemua
        '
        Me.rdsemua.AutoSize = True
        Me.rdsemua.Location = New System.Drawing.Point(134, 21)
        Me.rdsemua.Name = "rdsemua"
        Me.rdsemua.Size = New System.Drawing.Size(68, 19)
        Me.rdsemua.TabIndex = 38
        Me.rdsemua.TabStop = True
        Me.rdsemua.Text = "semua"
        Me.rdsemua.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rdsemua)
        Me.GroupBox2.Controls.Add(Me.rd80)
        Me.GroupBox2.Controls.Add(Me.rd30)
        Me.GroupBox2.Location = New System.Drawing.Point(236, 46)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(250, 55)
        Me.GroupBox2.TabIndex = 39
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Lab number yang ingin ditampilkan"
        '
        'cmdchart
        '
        Me.cmdchart.Location = New System.Drawing.Point(407, 601)
        Me.cmdchart.Name = "cmdchart"
        Me.cmdchart.Size = New System.Drawing.Size(468, 36)
        Me.cmdchart.TabIndex = 40
        Me.cmdchart.Text = "Grafik Hematology"
        Me.cmdchart.UseVisualStyleBackColor = True
        '
        'HemaAllFinishJobsFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(887, 648)
        Me.Controls.Add(Me.cmdchart)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.dgvdetail)
        Me.Controls.Add(Me.dgvJoblist)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "HemaAllFinishJobsFrm"
        Me.Text = "Hema Data"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvJoblist, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvdetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvJoblist As System.Windows.Forms.DataGridView
    Friend WithEvents dgvdetail As System.Windows.Forms.DataGridView
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents mnuCetak As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents rd30 As System.Windows.Forms.RadioButton
    Friend WithEvents rd80 As System.Windows.Forms.RadioButton
    Friend WithEvents rdsemua As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdchart As System.Windows.Forms.Button
End Class
