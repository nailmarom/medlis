﻿Public Class AppPrintSettingFrm
    Dim fpath As String
    Dim fInstrument As String

    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click
        WriteInsProperty()
    End Sub

    Public Sub WriteInsProperty()
        Dim insName As String

        If txtprint.Text.Length = 0 Then
            insName = "1"
        Else
            insName = Trim(txtprint.Text)
        End If

        Dim objWriter As New System.IO.StreamWriter(fpath)
        objWriter.WriteLine(insName)
        objWriter.Close()
    End Sub

    Private Sub AppSettingFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
        Try
            fpath = System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData, "printcount.txt")
            If ReadInsProperty() = False Then
                MessageBox.Show("Silahkan tentukan jumlah print setting")
            Else


                If fInstrument.Length = 0 Then
                    txtprint.Text = ""
                Else
                    txtprint.Text = fInstrument
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try

    End Sub

    Private Function ReadInsProperty() As Boolean

        Dim InsName As String
        Dim result As Boolean
        Dim getstr As String
        Dim position As Integer
        If System.IO.File.Exists(fpath) Then
            Try
                Dim lines() As String = IO.File.ReadAllLines(fpath)
                Dim k As Integer
                k = 0
                For Each line As String In lines

                    position = InStr(line, "=")
                    For i = position To Len(line) - 1
                        getstr = getstr + line.Substring(i, 1)
                    Next
                    InsName = getstr
                    getstr = ""
                Next

                fInstrument = InsName
                result = True
            Catch ex As Exception
                MessageBox.Show("Please specify instrument read mode", "File not found", MessageBoxButtons.OK)
            End Try
        Else
            result = False
        End If
        Return result

    End Function

    Private Sub cancelMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelMnu.Click
        Me.Close()
    End Sub

    ReadOnly AllowedKeys As String = _
       "123456789"
    Private Sub txtprint_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtprint.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)

        End Select
    End Sub
End Class