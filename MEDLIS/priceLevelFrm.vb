﻿Imports System.Data
Imports System.Data.SqlClient

Public Class priceLevelFrm

    Private Sub priceLevelFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
        LoadDisplayName()
    End Sub
    Private Sub LoadDisplayName()
        Dim strsql As String
        Dim greObject As New clsGreConnect
        strsql = "select * from patientpricelevel"

        greObject.buildConn()

        Dim thecommand As New SqlClient.SqlCommand(strsql, greObject.grecon)
        Dim theReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)

        Do While theReader.Read
            If Trim(theReader("defaultname")) = "price-level1" Then
                If Not IsDBNull(theReader("display")) Then
                    disLevel1.Text = theReader("display")
                End If

            End If
            If Trim(theReader("defaultname")) = "price-level2" Then
                If Not IsDBNull(theReader("display")) Then
                    disLevel2.Text = theReader("display")
                End If

            End If
            If Trim(theReader("defaultname")) = "price-level3" Then
                If Not IsDBNull(theReader("display")) Then
                    disLevel3.Text = theReader("display")
                End If

            End If
            If Trim(theReader("defaultname")) = "price-level4" Then
                If Not IsDBNull(theReader("display")) Then
                    disLevel4.Text = theReader("display")
                End If

            End If
            If Trim(theReader("defaultname")) = "price-level5" Then
                If Not IsDBNull(theReader("display")) Then
                    disLevel5.Text = theReader("display")
                End If

            End If
            If Trim(theReader("defaultname")) = "price-level6" Then
                If Not IsDBNull(theReader("display")) Then
                    disLevel6.Text = theReader("display")
                End If

            End If


        Loop

        thecommand.Dispose()
        greObject.CloseConn()

        Dim ctrl As Control
        For Each ctrl In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.ReadOnly = True
            End If

        Next
    End Sub
    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click
        Dim strsql As String

        If disLevel1.Text = "" Then disLevel1.Text = "-"
        strsql = "update patientpricelevel set display='" & Trim(disLevel1.Text) & "' where defaultname='price-level1'"
        UpdateTablePriceLevel(strsql)

        If disLevel2.Text = "" Then disLevel2.Text = "-"
        strsql = "update patientpricelevel set display='" & Trim(disLevel2.Text) & "' where defaultname='price-level2'"
        UpdateTablePriceLevel(strsql)

        If disLevel3.Text = "" Then disLevel3.Text = "-"
        strsql = "update patientpricelevel set display='" & Trim(disLevel3.Text) & "' where defaultname='price-level3'"
        UpdateTablePriceLevel(strsql)

        If disLevel4.Text = "" Then disLevel4.Text = "-"
        strsql = "update patientpricelevel set display='" & Trim(disLevel4.Text) & "' where defaultname='price-level4'"
        UpdateTablePriceLevel(strsql)

        If disLevel5.Text = "" Then disLevel5.Text = "-"
        strsql = "update patientpricelevel set display='" & Trim(disLevel5.Text) & "' where defaultname='price-level5'"
        UpdateTablePriceLevel(strsql)

        If disLevel6.Text = "" Then disLevel6.Text = "-"
        strsql = "update patientpricelevel set display='" & Trim(disLevel6.Text) & "' where defaultname='price-level6'"
        UpdateTablePriceLevel(strsql)

        Dim ctrl As Control
        For Each ctrl In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.ReadOnly = True
            End If
        Next
    End Sub
    Private Sub UpdateTablePriceLevel(ByVal str As String)
        Dim strsql As String
        Dim ogreINSERT As New clsGreConnect
        ogreINSERT.buildConn()
        ogreINSERT.ExecuteNonQuery(str)
        ogreINSERT.CloseConn()
    End Sub

    Private Sub EditMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditMnu.Click
        Dim ctrl As Control
        For Each ctrl In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.ReadOnly = False
            End If
        Next
    End Sub

    Private Sub cancelMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelMnu.Click
        LoadDisplayName()
    End Sub
End Class