﻿Imports System.Text.RegularExpressions
Public Class codeTestReference

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim two_range As String()

        Dim normal_string As String
        Dim critical_string As String

        Dim normal_val As String()
        Dim lower_normal As String
        Dim upper_normal As String

        Dim critical_val As String()
        Dim lower_critical As String
        Dim upper_critical As String

        Dim strsource As String

        strsource = Trim(TextBox1.Text)
        two_range = Regex.Split(strsource, "\\")
        If strsource = "" Then
            lower_normal = ""
            lower_critical = ""
            upper_normal = ""
            upper_critical = ""
        Else
            normal_string = two_range.ElementAt(0)
            critical_string = two_range.ElementAt(1)

            If normal_string = "" Then
                lower_normal = ""
                upper_normal = ""
            Else
                normal_val = Regex.Split(normal_string, "\^")
                lower_normal = normal_val.ElementAt(0)
                upper_normal = normal_val.ElementAt(1)
            End If

            If critical_string = "" Then
                lower_critical = ""
                upper_critical = ""
            Else
                critical_val = Regex.Split(critical_string, "\^")
                lower_critical = critical_val.ElementAt(0)
                upper_critical = critical_val.ElementAt(1)
            End If
        End If


        Label1.Text = normal_string

        Label2.Text = lower_normal
        Label3.Text = upper_normal

        Label4.Text = critical_string
        Label5.Text = lower_critical
        Label6.Text = upper_critical

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Dim val1 As Double

        If IsNumeric((TextBox1.Text)) Then
            Label8.Text = (CDbl(Trim(TextBox1.Text)) * 2).ToString
        Else
            Label8.Text = "Bukan angka"
        End If


    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim testvar As String
        testvar = Trim(TextBox2.Text)

        Dim str As String
        str = "update jobdetail set lowernormal='" & testvar & "' where id='133'"
        ogre.ExecuteNonQuery(str)
        ogre.CloseConn()
    End Sub
End Class