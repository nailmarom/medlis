﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class listSuccessTestFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(listSuccessTestFrm))
        Me.dpstart = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dpend = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmdShow = New System.Windows.Forms.Button()
        Me.cbTestItem = New System.Windows.Forms.ComboBox()
        Me.chksemua = New System.Windows.Forms.CheckBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dpstart
        '
        Me.dpstart.Location = New System.Drawing.Point(24, 43)
        Me.dpstart.Name = "dpstart"
        Me.dpstart.Size = New System.Drawing.Size(233, 21)
        Me.dpstart.TabIndex = 35
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(21, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(30, 15)
        Me.Label1.TabIndex = 36
        Me.Label1.Text = "Dari"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(316, 25)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 15)
        Me.Label2.TabIndex = 37
        Me.Label2.Text = "Sampai"
        '
        'dpend
        '
        Me.dpend.Location = New System.Drawing.Point(319, 43)
        Me.dpend.Name = "dpend"
        Me.dpend.Size = New System.Drawing.Size(233, 21)
        Me.dpend.TabIndex = 38
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.dpend)
        Me.GroupBox1.Controls.Add(Me.dpstart)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(580, 81)
        Me.GroupBox1.TabIndex = 39
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Rentang Tanggal"
        '
        'cmdShow
        '
        Me.cmdShow.Location = New System.Drawing.Point(505, 104)
        Me.cmdShow.Name = "cmdShow"
        Me.cmdShow.Size = New System.Drawing.Size(87, 24)
        Me.cmdShow.TabIndex = 39
        Me.cmdShow.Text = "OK"
        Me.cmdShow.UseVisualStyleBackColor = True
        '
        'cbTestItem
        '
        Me.cbTestItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbTestItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTestItem.FormattingEnabled = True
        Me.cbTestItem.Location = New System.Drawing.Point(197, 104)
        Me.cbTestItem.Margin = New System.Windows.Forms.Padding(4)
        Me.cbTestItem.Name = "cbTestItem"
        Me.cbTestItem.Size = New System.Drawing.Size(290, 23)
        Me.cbTestItem.TabIndex = 41
        '
        'chksemua
        '
        Me.chksemua.AutoSize = True
        Me.chksemua.Location = New System.Drawing.Point(12, 104)
        Me.chksemua.Name = "chksemua"
        Me.chksemua.Size = New System.Drawing.Size(121, 19)
        Me.chksemua.TabIndex = 42
        Me.chksemua.Text = "Semua Test Type"
        Me.chksemua.UseVisualStyleBackColor = True
        '
        'listSuccessTestFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(772, 138)
        Me.Controls.Add(Me.chksemua)
        Me.Controls.Add(Me.cmdShow)
        Me.Controls.Add(Me.cbTestItem)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "listSuccessTestFrm"
        Me.Text = "Daftar Job Selesai Per Test Item "
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dpstart As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dpend As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdShow As System.Windows.Forms.Button
    Private WithEvents cbTestItem As System.Windows.Forms.ComboBox
    Friend WithEvents chksemua As System.Windows.Forms.CheckBox
End Class
