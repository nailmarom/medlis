﻿Imports System
Imports System.ComponentModel
Imports System.Threading
Imports System.IO.Ports
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Windows.Forms.DataVisualization.Charting

Imports System.Data.SqlClient

Public Class MindRayBC3600
   
    Dim stoptick As Integer
    Dim srbc As Series
    Dim swbc As Series
    Dim splt As Series

    Dim krungu As Boolean

    Dim idInstrument As Integer

    Dim iforpg As Integer = 0
    Dim trd As New Thread(AddressOf ReceiveData)
    '   Dim threadPg As New Thread(AddressOf PgUpdate)

    Dim greobject As clsGreConnect
    Dim dtable As DataTable

    Dim cmport As New SerialPort
    Dim objconn As New clsGreConnect
    Dim tblData As DataTable
    Dim objTestRelated As New TestRelated
    Dim strsql As String
    Dim isReceiving As Boolean

    Dim myPort As Array
    Friend strFromCom As New List(Of String)
    Dim isAck As Boolean
    Dim curIndexSend As Integer
    Dim lastIndexSend As Integer
    Dim isMessageCreated As Boolean

    Dim isEOT As Boolean
    Dim receiveMode As Boolean

    Dim McnIsQ As Boolean
    Dim McnIsOR As Boolean


    Dim regex As Regex

    Dim qMcnToLis As New List(Of String)
    Dim oLisToMcn As New List(Of String)
    Dim orMcnToLis As New List(Of String)

    '----------
    Dim fInstrument As String
    Dim fportName As String
    Dim fbaud As String
    Dim fparity As String
    Dim fstop As String
    Dim fdata As String
    Dim fpath As String

    '--------- hema --
    Dim idtest_barcode As String

    Dim wbc_chart_val1 As String
    Dim wbc_chart_val2 As String
    Dim wbc_chart_val3 As String
    Dim wbc_chart_val4 As String
    Dim wbc_chart_val5 As String
    Dim wbc_chart_val6 As String
    Dim is_wbc_chart As Boolean = False

    Dim rbc_chart_val1 As String
    Dim rbc_chart_val2 As String
    Dim rbc_chart_val3 As String
    Dim rbc_chart_val4 As String
    Dim rbc_chart_val5 As String
    Dim rbc_chart_val6 As String
    Dim is_rbc_chart As Boolean = False

    Dim plt_chart_val1 As String
    Dim plt_chart_val2 As String
    Dim plt_chart_val3 As String
    Dim plt_chart_val4 As String
    Dim plt_chart_val5 As String
    Dim plt_chart_val6 As String
    Dim is_plt_chart As Boolean = False

    Dim LYMpersen As String
    Dim MONpersen As String
    Dim GRApersen As String
    Dim MCV As String
    Dim RDW_CV As String
    Dim RDW_SD As String
    Dim MPV As String
    Dim PDW As String
    Dim LYM_kres As String
    Dim MON_kres As String
    Dim GRA_kres As String
    Dim HCT As String
    Dim MCH As String
    Dim MCHC As String
    Dim PCT As String

    Private Delegate Sub DgtShowDG(ByVal barcode As String)
    Private Delegate Sub DgUpdatePg(ByVal i As Integer)


    Private Sub UpdatePgBar(ByVal i As Integer)
        If ProgressBar1.InvokeRequired Then
            Me.Invoke(New DgUpdatePg(AddressOf UpdatePgBar), i)
        Else
            ProgressBar1.Value = i
        End If
    End Sub





    Private Sub HemaThreadFrm_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            isReceiving = False
            SerialPort1.Close()
            If trd.IsAlive Then
                trd.Abort()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub HemaThreadShowFrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            isMindRayBC3600 = 0
            SerialPort1.DiscardInBuffer()
            SerialPort1.Close()
            SerialPort1.Dispose()
            If trd.IsAlive Then
                trd.Abort()
            End If


        Catch ex As Exception

        End Try

    End Sub



    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '=======
        Try
            isMindRayBC3600 = 1
            Dim strlival As Integer
            Dim usbparent As Integer

            usbparent = Capability(890, 10)


            ReadLicense()
            'alat4 adalah mindraybc3600
            strlival = GetLicenseFor("alat4", usbparent)

            If CInt(Capability(510, 10)) <> strlival Then
                For Each ctrl As Control In Controls
                    If (ctrl.GetType() Is GetType(Button)) Then
                        Dim btn As Button = CType(ctrl, Button)
                        btn.Enabled = False
                    End If
                Next

                For Each ctrl As Control In GroupBox1.Controls
                    If (ctrl.GetType() Is GetType(Button)) Then
                        Dim btn As Button = CType(ctrl, Button)
                        btn.Enabled = False
                    End If
                Next

                MessageBox.Show("Anda tidak punya license")
                Exit Sub
            End If

            'ReadAll(918, "Hema".Length)
            'ReadAll(927, "Dirui240".Length)
            'ReadAll(936, "Palio100".Length)



            wbcchart.Series.Clear()
            pltchart.Series.Clear()
            rbcchart.Series.Clear()

            myPort = IO.Ports.SerialPort.GetPortNames() 'Get all com ports available
            btnDisconnect.Enabled = False


            LoadInstrumentName()
            fpath = System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData, "bc3600.txt")
            If ReadInsProperty() = False Then
                MessageBox.Show("Silahkan pilih instrument")
                cbInstrument.Enabled = True
                cmdSaveIns.Enabled = True
            Else
                cmdSaveIns.Enabled = False
                cbInstrument.Enabled = True
                cbInstrument.SelectedIndex = cbInstrument.FindStringExact(fInstrument)
                cbInstrument.Enabled = False
            End If


        Catch ex As Exception
            'For Each ctrl As Control In Controls
            '    If (ctrl.GetType() Is GetType(Button)) Then
            '        Dim btn As Button = CType(ctrl, Button)
            '        btn.Enabled = False
            '    End If
            'Next
            MessageBox.Show("error 889")
        End Try

    End Sub

    Private Sub btnDisconnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisconnect.Click
        Try
            'Me.Cursor.Shomw = vb
       
            isReceiving = False
            krungu = False
            Me.Cursor = Cursors.WaitCursor
            stoptick = 0
            tmrstop.Enabled = True

            'SerialPort1.Close()
            'trd.ResetAbort()

            'Close our Serial Port

            btnConnect.Enabled = True
            btnDisconnect.Enabled = False
        Catch ex As Exception
            btnDisconnect.Enabled = False
            btnConnect.Enabled = True
            'MessageBox.Show("Port Close")
        End Try

    End Sub

    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        ReadLicense()
        Try
            If Trim(cbInstrument.Text) = "" Then
                MessageBox.Show("Instrument untuk komputer ini mengalami perubahan, silahkan pilih yang baru")
                Exit Sub
            End If

            '--------------
            If txtport.Text <> "" Then
                SerialPort1.PortName = Trim(txtport.Text)
            End If

            If txtbaud.Text <> "" Then
                SerialPort1.BaudRate = Trim(txtbaud.Text)
            End If

            If txtparity.Text <> "" Then
                Dim par As String
                par = Trim(txtparity.Text)
                If par = "None" Then
                    SerialPort1.Parity = IO.Ports.Parity.None
                ElseIf par = "Even" Then
                    SerialPort1.Parity = IO.Ports.Parity.Even
                ElseIf par = "Mark" Then
                    SerialPort1.Parity = IO.Ports.Parity.Mark
                ElseIf par = "Odd" Then
                    SerialPort1.Parity = IO.Ports.Parity.Odd
                End If
            End If

            If txtstop.Text <> "" Then
                Dim stp As String
                stp = txtstop.Text
                If stp = "None" Then
                    SerialPort1.StopBits = IO.Ports.StopBits.None
                ElseIf stp = "One" Then
                    SerialPort1.StopBits = IO.Ports.StopBits.One
                ElseIf stp = "Two" Then
                    SerialPort1.StopBits = IO.Ports.StopBits.Two
                End If
            End If

            If txtdata.Text <> "" Then
                Dim datab As String
                datab = txtdata.Text
                SerialPort1.DataBits = CInt(datab)

            End If
            ' untuk cari ide instrument
            idInstrument = getIdInstrument(Trim(cbInstrument.Text))
        Catch ex As Exception
            MessageBox.Show("Kesalahan pada koneksi serial", "Periksa Serial Port dan Properti-nya")
        End Try

        Try
            SerialPort1.Open()
            btnConnect.Enabled = False '
            btnDisconnect.Enabled = True '

            krungu = True
            isReceiving = True
            trd.IsBackground = True
            trd.Start()


        Catch ex As Exception
            MessageBox.Show("Kesalahan pada koneksi serial", "Periksa Serial Port dan Properti-nya")
        End Try

    End Sub
    Private Sub ReceiveData()
        Try

            Do While krungu = True

                If isReceiving = True Then

                    Dim newReceivedData As String
                    newReceivedData = SerialPort1.ReadLine
                    If newReceivedData.Count > 0 Then

                        If iforpg > 96 Then iforpg = 0
                        iforpg = iforpg + 3
                        UpdatePgBar(iforpg)

                        strFromCom.Add(newReceivedData)
                        'ShowToRtf(newReceivedData)
                        'ListAdd(newReceivedData)
                        Thread.Sleep(700)

                        'on 8ID with handshake is off there will be around 57

                        If strFromCom.Count = 57 Then
                            isReceiving = False
                            SerialPort1.DiscardInBuffer()

                            'tmrReceive.Stop()
                            StepOneHandleHemaMessage()
                        End If
                    End If
                ElseIf isReceiving = False And krungu = False Then
                    SerialPort1.Close()
                End If
            Loop

        Catch ex As Exception
            'MessageBox.Show("Port Close", "Komunikasi ditutup")
        End Try
    End Sub
    Private Delegate Sub ShowCom(ByVal str As String)
    Private Delegate Sub ShowComToList(ByVal str As String)

    Private Sub ShowToRtf(ByVal str As String)
        'If RichTextBox1.InvokeRequired Then
        '    Me.Invoke(New ShowCom(AddressOf ShowToRtf), str)
        'Else
        '    RichTextBox1.Text = str
        'End If

    End Sub
    Private Sub ListAdd(ByVal str As String)

        'If ListBox1.InvokeRequired Then
        '    Me.Invoke(New ShowComToList(AddressOf ListAdd), str)
        'Else
        '    ListBox1.Items.Add(str)
        'End If


    End Sub
    Private Function getIdInstrument(ByVal strname As String) As Integer
        Dim strins As String
        Dim result As Integer
        Dim ogre As New clsGreConnect
        ogre.buildConn()
        result = 0
        strins = "select id from hemaconnect where name='" & Trim(strname) & "'"
        Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                result = rdr("id")
            Loop
        End If

        rdr.Close()
        cmd.Dispose()
        ogre.CloseConn()
        Return result
    End Function




    Private Sub StepOneHandleHemaMessage()
        '   Try
        ' handle message disini

        Dim itemstr As String
        Dim testtype_element As String
        Dim val_element As String

        For Each itemstr In strFromCom
            'hanya untuk progress bar
            If iforpg > 96 Then iforpg = 0
            iforpg = iforpg + 3
            UpdatePgBar(iforpg)
            '-----------------------
            'itemstr diasumsikan tiap baris yang di terima
            'perlu tahu isi dari keseluruhan baris
            'dan ada berapa baris


            '-----------------------
            Dim strregex As String()
            strregex = regex.Split(itemstr, "\:")
            If strregex.Length > 1 Then
                testtype_element = strregex.ElementAt(0)
                val_element = Mid(strregex.ElementAt(1), 1, strregex.ElementAt(1).IndexOf(Chr(13)))
                StepTwoHandlePerLine(testtype_element.Trim, val_element.Trim)
            ElseIf strregex.Length = 1 Then

            End If

        Next

        strFromCom.Clear()
        UpdateJobStatus()

    End Sub

    Private Sub StepTwoHandlePerLine(ByVal test_element As String, ByVal value_element As String)
        ' Try
        Dim unit As String
        Dim refrange As String

        If test_element = "ID" Then
            idtest_barcode = Trim(value_element)
        ElseIf test_element = "WBC-Chart" Then
            StepThreeHandleChart(test_element, value_element, idtest_barcode)
        ElseIf test_element = "RBC-Chart" Then
            StepThreeHandleChart(test_element, value_element, idtest_barcode)
        ElseIf test_element = "PLT-Chart" Then
            StepThreeHandleChart(test_element, value_element, idtest_barcode)
        ElseIf test_element = "RBC" Then
            unit = Mid(strFromCom.ElementAt(8), 1, 8)
            refrange = Mid(strFromCom.ElementAt(8), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)
        ElseIf test_element = "PLT" Then
            unit = Mid(strFromCom.ElementAt(16), 1, 8)
            refrange = Mid(strFromCom.ElementAt(16), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)
        ElseIf test_element = "HCT" Then
            unit = Mid(strFromCom.ElementAt(10), 1, 8)
            refrange = Mid(strFromCom.ElementAt(10), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)
        ElseIf test_element = "PDW" Then
            unit = ""
            unit = Mid(strFromCom.ElementAt(19), 1, 8)
            refrange = Mid(strFromCom.ElementAt(19), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)
        ElseIf test_element = "RDW-SD" Then
            unit = Mid(strFromCom.ElementAt(14), 1, 8)
            refrange = Mid(strFromCom.ElementAt(14), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)
        ElseIf test_element = "RDW-CV" Then
            unit = Mid(strFromCom.ElementAt(15), 1, 8)
            refrange = Mid(strFromCom.ElementAt(15), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)
        ElseIf test_element = "PCT" Then
            unit = Mid(strFromCom.ElementAt(17), 1, 8)
            refrange = Mid(strFromCom.ElementAt(17), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)

        ElseIf test_element = "MPV" Then
            unit = Mid(strFromCom.ElementAt(18), 1, 8)
            refrange = Mid(strFromCom.ElementAt(18), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)
        ElseIf test_element = "MCV" Then
            unit = Mid(strFromCom.ElementAt(11), 1, 8)
            refrange = Mid(strFromCom.ElementAt(11), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)

        ElseIf test_element = "P-LCR" Then
            unit = Mid(strFromCom.ElementAt(20), 1, 8)
            refrange = Mid(strFromCom.ElementAt(20), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)
        ElseIf test_element = "WBC" Then
            unit = Mid(strFromCom.ElementAt(1), 1, 8)
            refrange = Mid(strFromCom.ElementAt(1), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)
        ElseIf test_element = "LYM#" Then
            unit = Mid(strFromCom.ElementAt(2), 1, 8)
            refrange = Mid(strFromCom.ElementAt(2), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)
        ElseIf test_element = "MXD#" Then
            unit = Mid(strFromCom.ElementAt(3), 1, 8)
            refrange = Mid(strFromCom.ElementAt(3), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)

        ElseIf test_element = "NEU#" Then
            unit = Mid(strFromCom.ElementAt(4), 1, 8)
            refrange = Mid(strFromCom.ElementAt(4), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)

        ElseIf test_element = "LYM%" Then
            unit = Mid(strFromCom.ElementAt(5), 1, 8)
            refrange = Mid(strFromCom.ElementAt(5), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)
        ElseIf test_element = "MXD%" Then
            unit = Mid(strFromCom.ElementAt(6), 1, 8)
            refrange = Mid(strFromCom.ElementAt(6), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)
        ElseIf test_element = "NEU%" Then
            unit = Mid(strFromCom.ElementAt(7), 1, 8)
            refrange = Mid(strFromCom.ElementAt(7), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)
        ElseIf test_element = "HGB" Then
            unit = Mid(strFromCom.ElementAt(9), 1, 8)
            refrange = Mid(strFromCom.ElementAt(9), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)
        ElseIf test_element = "MCH" Then
            unit = Mid(strFromCom.ElementAt(12), 1, 8)
            refrange = Mid(strFromCom.ElementAt(12), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)

        ElseIf test_element = "MCHC" Then
            unit = Mid(strFromCom.ElementAt(13), 1, 8)
            refrange = Mid(strFromCom.ElementAt(13), 9, 13)
            StepThreeHandlePerLine(test_element, value_element, idtest_barcode, unit, refrange)
        End If

        'Catch ex As Exception
        '    MessageBox.Show(ex.ToString, "Terjadi kesalahan no-2A")
        'End Try

    End Sub
    Private Sub StepThreeHandlePerLine(ByVal hema_test As String, ByVal hema_test_val As String, ByVal idbarcode As String, ByVal unit As String, ByVal refrange As String)
        ' Try
        Dim strsql As String

        Dim testname As String
        testname = hema_test

        Dim value As String
        'Dim unit As String
        'Dim refrange As String
        Dim resultAbnormalFlag As String
        Dim resultStatus As String
        Dim datecomplete As String

        Dim normal_string As String
        Dim critical_string As String

        Dim normal_val As String()
        Dim lower_normal As String
        Dim upper_normal As String

        Dim critical_val As String()
        Dim lower_critical As String
        Dim upper_critical As String


        datecomplete = Format(Now, "yyyyMMddhhmmss")
        resultAbnormalFlag = "N"
        resultStatus = "F"
        'butuh di benerin
        Dim arr_refrange As String()
        refrange = refrange.Trim()
        refrange = refrange.Replace(" ", "^")
        arr_refrange = regex.Split(refrange, "\^")
        '===

        Dim str As String
        Dim strclean As New List(Of String)

        str = refrange
        str = str.Trim()
        str = str.Replace(" ", "^")

        Dim rgx As String()
        rgx = RegularExpressions.Regex.Split(str, "\^")

        Dim i As Integer
        For i = 0 To rgx.Count - 1

            If rgx.ElementAt(i).Trim() = "" Then
            Else
                strclean.Add(rgx.ElementAt(i))
            End If
        Next

        'Dim y As Integer
        'y = 0
        'For y = 0 To strclean.Count - 1
        '    ListBox2.Items.Add(strclean(y))
        'Next

        'If strclean.Count > 1 Then
        '    Label1.Text = strclean.ElementAt(0)
        '    Label2.Text = strclean.ElementAt(1)
        'Else
        '    Label1.Text = strclean.ElementAt(0)
        'End If



        '===
        If strclean.Count > 1 Then
            lower_normal = strclean.ElementAt(0).Trim()
            upper_normal = strclean.ElementAt(1).Trim()

            '-------------------- manual abnormalflag because of 
            If lower_normal.Length > 0 And upper_normal.Length > 0 Then
                Dim nilai As Double
                Dim bawah As Double
                Dim atas As Double

                nilai = 0
                If hema_test_val.Length > 0 Then
                    nilai = CDbl(hema_test_val)
                End If
                bawah = 0
                If lower_normal.Length > 0 Then
                    bawah = CDbl(lower_normal)
                End If

                atas = 0
                If upper_normal.Length > 0 Then
                    atas = CDbl(upper_normal)
                End If

                Dim result As String
                result = "A"
                If nilai < bawah Then
                    result = "L"
                End If

                If nilai > atas Then
                    result = "H"
                End If

                If nilai < atas And nilai > bawah Then
                    result = "N"
                End If

                If nilai = atas Then
                    result = "N"
                End If

                If nilai = bawah Then
                    result = "N"
                End If
                resultAbnormalFlag = result
                '-----------------------------------
            End If
        ElseIf strclean.Count = 1 Then
            resultAbnormalFlag = ""
            lower_normal = strclean.ElementAt(0).Trim()
            upper_normal = ""
        End If
        'refrange = ""
        'hitung ref range disini
        'tinggal ambil sisi kiri dan sisikanan dari space


        strsql = "update jobdetail " & _
                 "set measurementvalue='" & hema_test_val & "',resultstatus='" & resultStatus & "'" & _
                 ",resultabnormalflag='" & resultAbnormalFlag & "'" & _
                 ",datecomplete='" & datecomplete & "',referencerange='" & refrange & "'" & _
                 ",machineorhuman='1',iduser='" & userid & "',idmachine='" & idInstrument & "',unit='" & unit & "',status='" & FINISHSAMPLE & "' " & _
                 ",lowernormal='" & lower_normal & "',uppernormal='" & upper_normal & "',lowercritical='" & lower_critical & "',uppercritical='" & upper_critical & "' " & _
                 "where barcode='" & Trim(idbarcode) & "' and universaltest='" & testname & "' "

        Dim ogre As New clsGreConnect
        ogre.buildConn()
        ogre.ExecuteNonQuery(strsql)
        ogre.CloseConn()
        '===============================
        'insert into hemadata table
        'barcode
        'ItemName
        'Result
        'Unit
        'Reference
        'datefinish ---> get from datecomplete

        Dim hemaogre As New clsGreConnect
        hemaogre.buildConn()
        strsql = "insert into hemadata(barcode,itemname,result,unit,refrange,datefinish,abnormalflag) values('" & idbarcode & "','" & hema_test & "','" & hema_test_val & "','" & unit & "','" & refrange & "','" & datecomplete & "','" & resultAbnormalFlag & "')"
        hemaogre.ExecuteNonQuery(strsql)
        hemaogre.CloseConn()

        'Catch ex As Exception
        '    MessageBox.Show(ex.ToString, "Terjadi kesalahan no 3A")
        'End Try



    End Sub
    Private Sub StepThreeHandleChart(ByVal hema_test As String, ByVal hema_test_val As String, ByVal idbarcode As String)

        ' Try
        Dim datecomplete As String
        datecomplete = Format(Now, "yyyyMMddhhmmss")
        Dim hemaogre As New clsGreConnect
        hemaogre.buildConn()
        strsql = "insert into hemachart(barcode,itemname,result,datefinish) values('" & Trim(idbarcode) & "','" & hema_test & "','" & hema_test_val & "','" & datecomplete & "')"
        hemaogre.ExecuteNonQuery(strsql)
        hemaogre.CloseConn()
        'Catch ex As Exception
        '    MessageBox.Show(ex.ToString, "Terjadi Kesalahan 3Ab")
        'End Try


    End Sub

    Private Sub UpdateJobStatus() 'penting update status
        'Try

        'If Capability(918, "Hema".Length) <> "Hema" Then
        '    MessageBox.Show("Anda tidak punya license - update hema")
        '    Exit Sub
        'End If


        Dim strsql As String
        Dim pormat As String
        pormat = "yyyy-MM-dd HH:mm:ss"

        Dim datenow As Date
        datenow = Now

        strsql = "select job.labnumber from job where job.status='" & NEWSAMPLE & "' and job.labnumber not in (select distinct labnumber from jobdetail where status<>'" & FINISHSAMPLE & "' and active='" & ACTIVESAMPLE & "')"

        Dim ogreupdate As New clsGreConnect
        ogreupdate.buildConn()
        Dim cmdupdate As New SqlClient.SqlCommand(strsql, ogreupdate.grecon)
        Dim rdrupdate As SqlDataReader = cmdupdate.ExecuteReader(CommandBehavior.CloseConnection)
        If rdrupdate.HasRows Then
            Do While rdrupdate.Read
                'update job

                Dim squ As String
                squ = "update job set status='" & FINISHSAMPLE & "',datefinish='" & datenow.ToString(pormat) & "' where labnumber='" & rdrupdate("labnumber") & "'"
                Dim ogrejob As New clsGreConnect
                ogrejob.buildConn()
                ogrejob.ExecuteNonQuery(squ)
                ogrejob.CloseConn()
            Loop
        End If

        ogreupdate.CloseConn()
        Show_Result_toDG(idtest_barcode)
        UpdateLabelId(idtest_barcode)
        isReceiving = True
        'starting again to receive message

        'Catch ex As Exception
        '    MessageBox.Show(ex.ToString, "Terjadi Kesalahan Update")
        'End Try


        srbc = New Series
        swbc = New Series
        splt = New Series

        ' pltchart.Series.Clear()
        splt.Name = "PLT"
        pltchart.Name = "PLT"

        'rbcchart.Series.Clear()
        srbc.Name = "RBC"
        rbcchart.Name = "RBC"

        'wbcchart.Series.Clear()
        swbc.Name = "WBC"
        wbcchart.Name = "WBC Graff"

        DrawChartWbc(Trim(idtest_barcode))
        DrawChartRBC(Trim(idtest_barcode))
        DrawChartPLT(Trim(idtest_barcode))


        iforpg = 100
        UpdatePgBar(iforpg)

    End Sub
    Private Delegate Sub Dg_UpdateLabelId(ByVal str As String)
    Private Sub UpdateLabelId(ByVal str As String)
        If lblbarcode.InvokeRequired Then
            Me.Invoke(New Dg_UpdateLabelId(AddressOf UpdateLabelId), str)
        Else
            Dim available_test As Boolean = False
            Dim ogrecek As New clsGreConnect
            Dim strcek As String
            strcek = "select distinct barcode from jobdetail where barcode='" & str & "'"
            ogrecek.buildConn()
            Dim cmdcek As New SqlClient.SqlCommand(strcek, ogrecek.grecon)
            Dim rdrcek As SqlDataReader = cmdcek.ExecuteReader(CommandBehavior.CloseConnection)
            If rdrcek.HasRows Then
                Do While rdrcek.Read
                    If rdrcek("barcode") = str Then
                        available_test = True
                    End If
                Loop
            End If
            rdrcek.Close()
            ogrecek.CloseConn()
            If available_test = True Then
                lblbarcode.Text = "ID: " & str
            Else
                lblbarcode.Text = "ID: " & str & "  " & "barcode tersebut tidak ditemukan"
            End If

        End If
    End Sub
    Private Delegate Sub dgateWBC(ByVal barcode As String)
    Private Sub Show_Result_toDG(ByVal strbarcode As String)
        ' Try

        If dghema.InvokeRequired Then
            Me.Invoke(New DgtShowDG(AddressOf Show_Result_toDG), strbarcode)
        Else
            dghema.DataSource = Nothing
            greobject = New clsGreConnect
            greobject.buildConn()
            dtable = New DataTable

            strsql = "select hemadata.itemname,hemadata.result,hemadata.abnormalflag,hemadata.unit,hemadata.refrange,testtype.displayindex from hemadata,testtype where hemadata.itemname=testtype.analyzertestname and hemadata.barcode='" & strbarcode & "' order by testtype.displayindex asc"
            dtable = greobject.ExecuteQuery(strsql)
            dghema.DataSource = dtable

            Dim j As Integer
            For j = 0 To dghema.Columns.Count - 1
                dghema.Columns(j).Visible = False
            Next

            dghema.Columns("itemname").Visible = True
            dghema.Columns("itemname").HeaderText = "Item Name"
            dghema.Columns("itemname").Width = 100
            dghema.Columns("itemname").ReadOnly = True

            dghema.Columns("result").Visible = True
            dghema.Columns("result").HeaderText = "Result"
            dghema.Columns("result").Width = 100
            dghema.Columns("result").ReadOnly = True

            dghema.Columns("abnormalflag").Visible = True
            dghema.Columns("abnormalflag").HeaderText = "Flag"
            dghema.Columns("abnormalflag").Width = 160
            dghema.Columns("abnormalflag").ReadOnly = True

            dghema.Columns("unit").Visible = True
            dghema.Columns("unit").HeaderText = "Unit"
            dghema.Columns("unit").Width = 80
            dghema.Columns("unit").ReadOnly = True

            dghema.Columns("refrange").Visible = True
            dghema.Columns("refrange").HeaderText = "Reference"
            dghema.Columns("refrange").Width = 160
            dghema.Columns("refrange").ReadOnly = True

            dghema.AllowUserToAddRows = False
            dghema.RowHeadersVisible = False

        End If

    End Sub


    Private Sub DrawChartWbc(ByVal barcode As String)

        'wbcchart.Series.Clear()

        If wbcchart.InvokeRequired Then
            Me.Invoke(New dgateWBC(AddressOf DrawChartWbc), barcode)
        Else

          
            wbcchart.Series.Clear()
            Dim strins As String
            Dim wbcresult As String

            Dim ogre As New clsGreConnect
            ogre.buildConn()
            strins = "select result from hemachart where barcode='" & barcode & "' and itemname='WBC-Chart'"
            Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If rdr.HasRows Then
                Do While rdr.Read
                    wbcresult = rdr("result")
                Loop

                Dim rgx As String() = System.Text.RegularExpressions.Regex.Split(wbcresult, "\,")
                Dim c As Integer
                For c = 0 To rgx.Count - 1
                    Dim dp As New DataPoint
                    If rgx.ElementAt(c) <> "" Then
                        dp.SetValueXY(c, CDbl(rgx.ElementAt(c).Trim()))
                        swbc.Points.Add(dp)
                    End If
                Next
                wbcchart.Series.Add(swbc)
            End If

            rdr.Close()
            cmd.Dispose()
            ogre.CloseConn()


        End If
    End Sub
    Private Delegate Sub dgateRBC(ByVal barcode As String)
    Private Sub DrawChartRBC(ByVal barcode As String)

    

        If rbcchart.InvokeRequired Then
            Me.Invoke(New dgateRBC(AddressOf DrawChartRBC), barcode)
        Else
            rbcchart.Series.Clear()
            Dim strins As String
            Dim rbcresult As String

            Dim ogre As New clsGreConnect
            ogre.buildConn()
            strins = "select result from hemachart where barcode='" & barcode & "' and itemname='RBC-Chart' "
            Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If rdr.HasRows Then
                Do While rdr.Read
                    rbcresult = rdr("result")
                Loop

                Dim rgx As String() = System.Text.RegularExpressions.Regex.Split(rbcresult, "\,")
                Dim c As Integer
                For c = 0 To rgx.Count - 1
                    Dim dp As New DataPoint
                    If rgx.ElementAt(c) <> "" Then
                        dp.SetValueXY(c, CDbl(rgx.ElementAt(c).Trim()))
                        srbc.Points.Add(dp)

                    End If
                Next
                rbcchart.Series.Add(srbc)


            End If

            rdr.Close()
            cmd.Dispose()
            ogre.CloseConn()


        End If
    End Sub
    Private Delegate Sub dgatePLT(ByVal barcode As String)
    Private Sub DrawChartPLT(ByVal barcode As String)



        If pltchart.InvokeRequired Then
            Me.Invoke(New dgatePLT(AddressOf DrawChartPLT), barcode)
        Else
            pltchart.Series.Clear()
            Dim strins As String
            Dim PLTresult As String

            Dim ogre As New clsGreConnect
            ogre.buildConn()
            strins = "select result from hemachart where barcode='" & barcode & "' and itemname='PLT-Chart'"
            Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If rdr.HasRows Then
                Do While rdr.Read
                    PLTresult = rdr("result")
                Loop

                Dim rgx As String() = System.Text.RegularExpressions.Regex.Split(PLTresult, "\,")

                Dim c As Integer
                For c = 0 To rgx.Count - 1
                    Dim dp As New DataPoint
                    If rgx.ElementAt(c) <> "" Then
                        dp.SetValueXY(c, CDbl(rgx.ElementAt(c).Trim()))
                        splt.Points.Add(dp)

                    End If

                Next
                pltchart.Series.Add(splt)

            End If

            rdr.Close()
            cmd.Dispose()
            ogre.CloseConn()


            'Dim wbc = "0 , 0 , 0 , 0 , 0, 20 , 127, 78, 89, 9, 50, 55,"

        End If
    End Sub


    '-----------------------------------------------------------------------
    '------------------------------------------------------
    Private Sub LoadInstrumentName()
        Dim strins As String
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        strins = "select distinct name from hemaconnect"
        Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            cbInstrument.Items.Add(rdr("name"))
        Loop
        cbInstrument.SelectedIndex = 0
        rdr.Close()
        cmd = Nothing
        ogre.CloseConn()

    End Sub

    Private Sub ShowInstrumentProperty()
        Dim strins As String
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        txtport.Enabled = True
        txtbaud.Enabled = True
        txtparity.Enabled = True
        txtstop.Enabled = True
        txtdata.Enabled = True

        strins = "select * from hemaconnect where name='" & Trim(cbInstrument.Text) & "'"
        Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("com")) Then
                    txtport.Text = rdr("com")
                End If
                If Not IsDBNull(rdr("baud")) Then
                    txtbaud.Text = rdr("baud")
                End If
                If Not IsDBNull(rdr("parity")) Then
                    txtparity.Text = rdr("parity")
                End If
                If Not IsDBNull(rdr("stop")) Then
                    txtstop.Text = rdr("stop")
                End If
                If Not IsDBNull(rdr("data")) Then
                    txtdata.Text = rdr("data")
                End If
            Loop
        End If
        txtport.Enabled = False
        txtbaud.Enabled = False
        txtparity.Enabled = False
        txtstop.Enabled = False
        txtdata.Enabled = False
    End Sub
    Private Sub cbInstrument_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbInstrument.SelectedIndexChanged
        ShowInstrumentProperty()
    End Sub

    Public Sub WriteInsProperty()

        'Dim i As Integer
        'Dim aryText(4) As String
        Dim insName As String



        'aryText(0) = "Port=" & fportName
        'aryText(1) = "Baud=" & fbaud
        'aryText(2) = "Parity=" & fparity
        'aryText(3) = "Stop=" & fstop
        'aryText(4) = "Data=" & fdata
        'aryText(5) = "Instrument=" & fInstrument
        insName = "Instrument=" & cbInstrument.Text

        Dim objWriter As New System.IO.StreamWriter(fpath)
        objWriter.WriteLine(insName)

        'For i = 0 To 3
        '    objWriter.WriteLine(aryText(i))
        'Next
        objWriter.Close()
    End Sub

    Private Function ReadInsProperty() As Boolean

        Dim InsName As String

        Dim result As Boolean


        Dim getstr As String
        Dim position As Integer


        If System.IO.File.Exists(fpath) Then
            Try

                Dim lines() As String = IO.File.ReadAllLines(fpath)
                Dim k As Integer
                k = 0
                For Each line As String In lines

                    position = InStr(line, "=")
                    For i = position To Len(line) - 1
                        getstr = getstr + line.Substring(i, 1)
                    Next


                    InsName = getstr
                    getstr = ""
                Next


                'fportName = arrSetting(0)
                'fbaud = arrSetting(1)
                'fparity = arrSetting(2)
                'fstop = arrSetting(3)
                'fdata = arrSetting(4)
                fInstrument = InsName

                result = True
            Catch ex As Exception
                MessageBox.Show("Seeting file tidak ada", "File not found", MessageBoxButtons.OK)


            End Try
        Else
            result = False
        End If

        Return result

    End Function



    Private Sub cmdSaveIns_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSaveIns.Click
        WriteInsProperty()
        cbInstrument.Enabled = False
        cmdSaveIns.Enabled = False
        cmdEdit.Enabled = True
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        cbInstrument.Enabled = True
        cmdSaveIns.Enabled = True
        cmdEdit.Enabled = False

        'Dim strins As String
        'Dim ogre As New clsGreConnect
        'ogre.buildConn()

        'strins = "select distinct name from hemaconnect"
        'Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
        'Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        'If rdr.HasRows Then
        '    Do While rdr.Read
        '        cbInstrument.Items.Add(rdr("name"))
        '    Loop
        '    cbInstrument.SelectedIndex = 0

        'End If

    End Sub




    Private Sub tmrstop_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrstop.Tick
        If stoptick = 0 Then
            pleaseWaitFrm.Show()
        End If
        stoptick = stoptick + 1
        If stoptick = 4 Then
            pleaseWaitFrm.Hide()
            pleaseWaitFrm.Dispose()
            Me.Cursor = Cursors.Arrow
            'SerialPort1.DiscardInBuffer()
            SerialPort1.Close()
            SerialPort1.Dispose()

            trd.Abort()

            'trd.ResetAbort()
            tmrstop.Enabled = False
            btnConnect.Enabled = True
            Me.Close()
            'btnDisconnect.Enabled = False
        End If
    End Sub

    Private Sub tmrReceive_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrReceive.Tick

    End Sub
End Class
