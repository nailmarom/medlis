﻿Imports System.Data.SqlClient
Imports DevExpress.XtraReports.UI

Public Class listSuccessTestFrm
    Dim selectedtestid As Integer
    Dim goodstrsql As String
    Dim greobject As clsGreConnect
    Dim dtable As DataTable
    Dim start_to_pass As String
    Dim end_to_pass As String

    Private Sub cmdShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdShow.Click
        Dim rpt As New JobPerTestItem
        rpt.Parameters("DateStart").Value = dpstart.Value.ToString("yyyy-MM-dd")
        rpt.Parameters("DateEnd").Value = dpend.Value.ToString("yyyy-MM-dd")
        If chksemua.Checked Then
            rpt.Parameters("TestID").Value = 0
        Else
            rpt.Parameters("TestID").Value = cbTestItem.SelectedValue
        End If
        rpt.Parameters("DateStart").Visible = False
        rpt.Parameters("DateEnd").Visible = False
        rpt.Parameters("TestID").Visible = False
        Dim preview As New ReportPrintTool(rpt)
        preview.ShowPreviewDialog()
    End Sub

    Private Sub listSuccessTestFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("id-ID")
        System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo("id-ID") 'bagus penting

        LoadChoiceCombo()
    End Sub
    Private Sub LoadChoiceCombo()
        Dim strsql As String
        Dim greObject As New clsGreConnect
        Dim cbtable As New DataTable

        strsql = "select * from testtype"
        greObject.buildConn()
        cbtable = greObject.ExecuteQuery(strsql)

        cbTestItem.DataSource = cbtable
        cbTestItem.DisplayMember = "testname"
        cbTestItem.ValueMember = "id"

        greObject.CloseConn()
        greObject = Nothing


    End Sub
    Private Sub mnuCetak_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


        Dim pormat As String
        pormat = "yyyy-MM-dd"
        Dim dtstart As Date
        Dim dtend As Date

        dtstart = dpstart.Value
        dtend = dpend.Value

        start_to_pass = dtstart.ToString(pormat)
        end_to_pass = dtend.ToString(pormat)

        selectedtestid = cbTestItem.SelectedValue


        Dim strsql As String
        If chksemua.Checked Then
            strsql = "select row_number() over() as num,testtype.testname,job.datereceived,jobdetail.labnumber,patient.patientname,jobdetail.price from job,testtype,patient,jobdetail where job.paymentstatus='1' and jobdetail.labnumber=job.labnumber and jobdetail.idtesttype=testtype.id and job.idpasien=patient.id and datereceived between '" & dtstart.ToString(pormat) & "' and '" & dtend.ToString(pormat) & "'" ' and testtype.id='" & testtosearch & "'"
        Else
            strsql = "select row_number() over() as num,testtype.testname,job.datereceived,jobdetail.labnumber,patient.patientname,jobdetail.price from job,testtype,patient,jobdetail where job.paymentstatus='1' and jobdetail.labnumber=job.labnumber and jobdetail.idtesttype=testtype.id and job.idpasien=patient.id and datereceived between '" & dtstart.ToString(pormat) & "' and '" & dtend.ToString(pormat) & "' and testtype.id='" & selectedtestid & "'"

        End If

        Dim pormat2 As String
        pormat2 = "dd-MM-yyyy"

        dtstart = dpstart.Value
        dtend = dpend.Value

        start_to_pass = dtstart.ToString(pormat2)
        end_to_pass = dtend.ToString(pormat2)

        Dim orptListSuccessTess As New rptListSuccessTest(start_to_pass, end_to_pass, strsql) 'start_to_pass, end_to_pass, selectedtestid, goodstrsql)
        orptListSuccessTess.MdiParent = MainFrm

        orptListSuccessTess.WindowState = FormWindowState.Maximized
        orptListSuccessTess.Show()

    End Sub


    Private Sub cbTestGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbTestItem.SelectedIndexChanged
        'Dim objTestRelated As New TestRelated
        'selectedgroupid = objTestRelated.GetIdTestGroup(cbTestGroup.Text)
        'PopulateDg()
    End Sub

    Private Sub chksemua_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chksemua.CheckedChanged
        If chksemua.Checked Then cbTestItem.Enabled = False Else cbTestItem.Enabled = True

    End Sub
End Class