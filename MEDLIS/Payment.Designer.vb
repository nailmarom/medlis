﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Payment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Payment))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.rdoanak = New System.Windows.Forms.RadioButton()
        Me.dpreg = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtlabnumber = New System.Windows.Forms.TextBox()
        Me.txtDocter = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.dpBirth = New System.Windows.Forms.DateTimePicker()
        Me.rdoPerempuan = New System.Windows.Forms.RadioButton()
        Me.rdoLaki = New System.Windows.Forms.RadioButton()
        Me.label6 = New System.Windows.Forms.Label()
        Me.label4 = New System.Windows.Forms.Label()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.label3 = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        Me.txtIdPasien = New System.Windows.Forms.TextBox()
        Me.txtAlamat = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtNamaPasien = New System.Windows.Forms.TextBox()
        Me.dgvtest = New System.Windows.Forms.DataGridView()
        Me.cbpayment = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtdiscount = New System.Windows.Forms.TextBox()
        Me.lblterbilang = New System.Windows.Forms.Label()
        Me.cmdpayment = New System.Windows.Forms.Button()
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.rdolangsung = New System.Windows.Forms.RadioButton()
        Me.rdotagihkan = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtkembali = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtnomorkartu = New System.Windows.Forms.TextBox()
        Me.txtuangdibayar = New System.Windows.Forms.TextBox()
        Me.txtprice = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtsetelahbulat = New System.Windows.Forms.TextBox()
        Me.txtblmbulat = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdcetak = New System.Windows.Forms.Button()
        Me.dgpack = New System.Windows.Forms.DataGridView()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvtest, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgpack, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.rdoanak)
        Me.Panel1.Controls.Add(Me.dpreg)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.txtlabnumber)
        Me.Panel1.Controls.Add(Me.txtDocter)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txtEmail)
        Me.Panel1.Controls.Add(Me.dpBirth)
        Me.Panel1.Controls.Add(Me.rdoPerempuan)
        Me.Panel1.Controls.Add(Me.rdoLaki)
        Me.Panel1.Controls.Add(Me.label6)
        Me.Panel1.Controls.Add(Me.label4)
        Me.Panel1.Controls.Add(Me.txtPhone)
        Me.Panel1.Controls.Add(Me.label3)
        Me.Panel1.Controls.Add(Me.label2)
        Me.Panel1.Controls.Add(Me.txtIdPasien)
        Me.Panel1.Controls.Add(Me.txtAlamat)
        Me.Panel1.Controls.Add(Me.label1)
        Me.Panel1.Controls.Add(Me.txtNamaPasien)
        Me.Panel1.Location = New System.Drawing.Point(14, 14)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1085, 148)
        Me.Panel1.TabIndex = 56
        '
        'rdoanak
        '
        Me.rdoanak.AutoSize = True
        Me.rdoanak.Location = New System.Drawing.Point(551, 43)
        Me.rdoanak.Name = "rdoanak"
        Me.rdoanak.Size = New System.Drawing.Size(91, 19)
        Me.rdoanak.TabIndex = 60
        Me.rdoanak.TabStop = True
        Me.rdoanak.Text = "Anak anak"
        Me.rdoanak.UseVisualStyleBackColor = True
        '
        'dpreg
        '
        Me.dpreg.Location = New System.Drawing.Point(773, 100)
        Me.dpreg.Name = "dpreg"
        Me.dpreg.Size = New System.Drawing.Size(213, 21)
        Me.dpreg.TabIndex = 59
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(701, 102)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 15)
        Me.Label8.TabIndex = 58
        Me.Label8.Text = "Tanggal"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(674, 45)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(86, 15)
        Me.Label11.TabIndex = 57
        Me.Label11.Text = "Lab Number"
        '
        'txtlabnumber
        '
        Me.txtlabnumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtlabnumber.Location = New System.Drawing.Point(773, 35)
        Me.txtlabnumber.Name = "txtlabnumber"
        Me.txtlabnumber.Size = New System.Drawing.Size(213, 23)
        Me.txtlabnumber.TabIndex = 54
        '
        'txtDocter
        '
        Me.txtDocter.Location = New System.Drawing.Point(773, 70)
        Me.txtDocter.Name = "txtDocter"
        Me.txtDocter.Size = New System.Drawing.Size(213, 21)
        Me.txtDocter.TabIndex = 56
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(711, 75)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 15)
        Me.Label5.TabIndex = 55
        Me.Label5.Text = "Dokter"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(332, 119)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(44, 15)
        Me.Label7.TabIndex = 53
        Me.Label7.Text = "Email"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(380, 116)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(159, 21)
        Me.txtEmail.TabIndex = 52
        '
        'dpBirth
        '
        Me.dpBirth.Location = New System.Drawing.Point(113, 70)
        Me.dpBirth.Name = "dpBirth"
        Me.dpBirth.Size = New System.Drawing.Size(233, 21)
        Me.dpBirth.TabIndex = 50
        '
        'rdoPerempuan
        '
        Me.rdoPerempuan.AutoSize = True
        Me.rdoPerempuan.Location = New System.Drawing.Point(443, 43)
        Me.rdoPerempuan.Name = "rdoPerempuan"
        Me.rdoPerempuan.Size = New System.Drawing.Size(99, 19)
        Me.rdoPerempuan.TabIndex = 49
        Me.rdoPerempuan.TabStop = True
        Me.rdoPerempuan.Text = "Perempuan"
        Me.rdoPerempuan.UseVisualStyleBackColor = True
        '
        'rdoLaki
        '
        Me.rdoLaki.AutoSize = True
        Me.rdoLaki.Location = New System.Drawing.Point(352, 43)
        Me.rdoLaki.Name = "rdoLaki"
        Me.rdoLaki.Size = New System.Drawing.Size(83, 19)
        Me.rdoLaki.TabIndex = 48
        Me.rdoLaki.TabStop = True
        Me.rdoLaki.Text = "Laki Laki"
        Me.rdoLaki.UseVisualStyleBackColor = True
        '
        'label6
        '
        Me.label6.AutoSize = True
        Me.label6.Location = New System.Drawing.Point(8, 73)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(96, 15)
        Me.label6.TabIndex = 32
        Me.label6.Text = "Tanggal Lahir"
        Me.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'label4
        '
        Me.label4.AutoSize = True
        Me.label4.Location = New System.Drawing.Point(56, 117)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(48, 15)
        Me.label4.TabIndex = 25
        Me.label4.Text = "Phone"
        Me.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPhone
        '
        Me.txtPhone.Location = New System.Drawing.Point(113, 116)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(160, 21)
        Me.txtPhone.TabIndex = 24
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Location = New System.Drawing.Point(53, 96)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(51, 15)
        Me.label3.TabIndex = 22
        Me.label3.Text = "Alamat"
        Me.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(59, 43)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(45, 15)
        Me.label2.TabIndex = 21
        Me.label2.Text = "Nama"
        Me.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtIdPasien
        '
        Me.txtIdPasien.Location = New System.Drawing.Point(113, 11)
        Me.txtIdPasien.Name = "txtIdPasien"
        Me.txtIdPasien.Size = New System.Drawing.Size(160, 21)
        Me.txtIdPasien.TabIndex = 20
        '
        'txtAlamat
        '
        Me.txtAlamat.Location = New System.Drawing.Point(113, 94)
        Me.txtAlamat.Name = "txtAlamat"
        Me.txtAlamat.Size = New System.Drawing.Size(426, 21)
        Me.txtAlamat.TabIndex = 19
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Location = New System.Drawing.Point(30, 15)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(74, 15)
        Me.label1.TabIndex = 18
        Me.label1.Text = "ID PASIEN"
        Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNamaPasien
        '
        Me.txtNamaPasien.Location = New System.Drawing.Point(113, 41)
        Me.txtNamaPasien.Name = "txtNamaPasien"
        Me.txtNamaPasien.Size = New System.Drawing.Size(234, 21)
        Me.txtNamaPasien.TabIndex = 17
        '
        'dgvtest
        '
        Me.dgvtest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvtest.Location = New System.Drawing.Point(14, 168)
        Me.dgvtest.Name = "dgvtest"
        Me.dgvtest.Size = New System.Drawing.Size(584, 324)
        Me.dgvtest.TabIndex = 57
        '
        'cbpayment
        '
        Me.cbpayment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbpayment.FormattingEnabled = True
        Me.cbpayment.Location = New System.Drawing.Point(160, 81)
        Me.cbpayment.Name = "cbpayment"
        Me.cbpayment.Size = New System.Drawing.Size(308, 23)
        Me.cbpayment.TabIndex = 58
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(30, 84)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(125, 15)
        Me.Label9.TabIndex = 59
        Me.Label9.Text = "Jenis Pembayaran"
        '
        'txtdiscount
        '
        Me.txtdiscount.Location = New System.Drawing.Point(160, 114)
        Me.txtdiscount.Name = "txtdiscount"
        Me.txtdiscount.Size = New System.Drawing.Size(308, 21)
        Me.txtdiscount.TabIndex = 60
        '
        'lblterbilang
        '
        Me.lblterbilang.AutoSize = True
        Me.lblterbilang.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblterbilang.Location = New System.Drawing.Point(19, 26)
        Me.lblterbilang.Name = "lblterbilang"
        Me.lblterbilang.Size = New System.Drawing.Size(0, 13)
        Me.lblterbilang.TabIndex = 64
        '
        'cmdpayment
        '
        Me.cmdpayment.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdpayment.Location = New System.Drawing.Point(114, 585)
        Me.cmdpayment.Name = "cmdpayment"
        Me.cmdpayment.Size = New System.Drawing.Size(94, 40)
        Me.cmdpayment.TabIndex = 65
        Me.cmdpayment.Text = "Bayar"
        Me.cmdpayment.UseVisualStyleBackColor = True
        '
        'cmdCancel
        '
        Me.cmdCancel.Location = New System.Drawing.Point(12, 585)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(94, 40)
        Me.cmdCancel.TabIndex = 66
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'rdolangsung
        '
        Me.rdolangsung.AutoSize = True
        Me.rdolangsung.Location = New System.Drawing.Point(163, 56)
        Me.rdolangsung.Name = "rdolangsung"
        Me.rdolangsung.Size = New System.Drawing.Size(123, 19)
        Me.rdolangsung.TabIndex = 67
        Me.rdolangsung.TabStop = True
        Me.rdolangsung.Text = "langsung bayar"
        Me.rdolangsung.UseVisualStyleBackColor = True
        '
        'rdotagihkan
        '
        Me.rdotagihkan.AutoSize = True
        Me.rdotagihkan.Location = New System.Drawing.Point(312, 55)
        Me.rdotagihkan.Name = "rdotagihkan"
        Me.rdotagihkan.Size = New System.Drawing.Size(94, 19)
        Me.rdotagihkan.TabIndex = 68
        Me.rdotagihkan.TabStop = True
        Me.rdotagihkan.Text = "Ditagihkan"
        Me.rdotagihkan.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.txtkembali)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.txtnomorkartu)
        Me.GroupBox1.Controls.Add(Me.txtuangdibayar)
        Me.GroupBox1.Controls.Add(Me.txtprice)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtsetelahbulat)
        Me.GroupBox1.Controls.Add(Me.txtblmbulat)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.rdotagihkan)
        Me.GroupBox1.Controls.Add(Me.rdolangsung)
        Me.GroupBox1.Controls.Add(Me.txtdiscount)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.cbpayment)
        Me.GroupBox1.Location = New System.Drawing.Point(614, 214)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(485, 355)
        Me.GroupBox1.TabIndex = 69
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Pembayaran"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(180, 324)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(98, 15)
        Me.Label17.TabIndex = 81
        Me.Label17.Text = "Uang Kembali"
        '
        'txtkembali
        '
        Me.txtkembali.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtkembali.Location = New System.Drawing.Point(279, 318)
        Me.txtkembali.Name = "txtkembali"
        Me.txtkembali.Size = New System.Drawing.Size(187, 26)
        Me.txtkembali.TabIndex = 80
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(67, 270)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(88, 15)
        Me.Label16.TabIndex = 79
        Me.Label16.Text = "Nomor Kartu"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(38, 233)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(117, 15)
        Me.Label15.TabIndex = 78
        Me.Label15.Text = "Uang Dibayarkan"
        '
        'txtnomorkartu
        '
        Me.txtnomorkartu.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnomorkartu.Location = New System.Drawing.Point(160, 266)
        Me.txtnomorkartu.Name = "txtnomorkartu"
        Me.txtnomorkartu.Size = New System.Drawing.Size(229, 26)
        Me.txtnomorkartu.TabIndex = 77
        '
        'txtuangdibayar
        '
        Me.txtuangdibayar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtuangdibayar.Location = New System.Drawing.Point(160, 228)
        Me.txtuangdibayar.Name = "txtuangdibayar"
        Me.txtuangdibayar.Size = New System.Drawing.Size(229, 26)
        Me.txtuangdibayar.TabIndex = 76
        '
        'txtprice
        '
        Me.txtprice.Location = New System.Drawing.Point(160, 27)
        Me.txtprice.Name = "txtprice"
        Me.txtprice.Size = New System.Drawing.Size(229, 21)
        Me.txtprice.TabIndex = 75
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(73, 179)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(82, 15)
        Me.Label14.TabIndex = 74
        Me.Label14.Text = "Grand Total"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(18, 147)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(137, 15)
        Me.Label12.TabIndex = 73
        Me.Label12.Text = "Sebelum Dibulatkan"
        '
        'txtsetelahbulat
        '
        Me.txtsetelahbulat.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsetelahbulat.Location = New System.Drawing.Point(160, 174)
        Me.txtsetelahbulat.Name = "txtsetelahbulat"
        Me.txtsetelahbulat.Size = New System.Drawing.Size(229, 27)
        Me.txtsetelahbulat.TabIndex = 72
        '
        'txtblmbulat
        '
        Me.txtblmbulat.Location = New System.Drawing.Point(160, 145)
        Me.txtblmbulat.Name = "txtblmbulat"
        Me.txtblmbulat.Size = New System.Drawing.Size(229, 21)
        Me.txtblmbulat.TabIndex = 71
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(18, 31)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(137, 15)
        Me.Label13.TabIndex = 70
        Me.Label13.Text = "Total sebelum bayar"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(92, 117)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(63, 15)
        Me.Label10.TabIndex = 69
        Me.Label10.Text = "Discount"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblterbilang)
        Me.GroupBox2.Location = New System.Drawing.Point(329, 585)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(679, 44)
        Me.GroupBox2.TabIndex = 70
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Terbilang"
        '
        'cmdcetak
        '
        Me.cmdcetak.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdcetak.Location = New System.Drawing.Point(216, 585)
        Me.cmdcetak.Name = "cmdcetak"
        Me.cmdcetak.Size = New System.Drawing.Size(94, 40)
        Me.cmdcetak.TabIndex = 76
        Me.cmdcetak.Text = "Cetak"
        Me.cmdcetak.UseVisualStyleBackColor = True
        '
        'dgpack
        '
        Me.dgpack.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgpack.Location = New System.Drawing.Point(14, 498)
        Me.dgpack.Name = "dgpack"
        Me.dgpack.Size = New System.Drawing.Size(584, 81)
        Me.dgpack.TabIndex = 77
        '
        'Payment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1109, 637)
        Me.Controls.Add(Me.dgpack)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdcetak)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmdpayment)
        Me.Controls.Add(Me.dgvtest)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Payment"
        Me.Text = "Pembayaran"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvtest, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgpack, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Private WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Private WithEvents dpBirth As System.Windows.Forms.DateTimePicker
    Private WithEvents rdoPerempuan As System.Windows.Forms.RadioButton
    Private WithEvents rdoLaki As System.Windows.Forms.RadioButton
    Private WithEvents label6 As System.Windows.Forms.Label
    Private WithEvents label4 As System.Windows.Forms.Label
    Private WithEvents txtPhone As System.Windows.Forms.TextBox
    Private WithEvents label3 As System.Windows.Forms.Label
    Private WithEvents label2 As System.Windows.Forms.Label
    Private WithEvents txtIdPasien As System.Windows.Forms.TextBox
    Private WithEvents txtAlamat As System.Windows.Forms.TextBox
    Private WithEvents label1 As System.Windows.Forms.Label
    Private WithEvents txtNamaPasien As System.Windows.Forms.TextBox
    Private WithEvents dpreg As System.Windows.Forms.DateTimePicker
    Private WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtlabnumber As System.Windows.Forms.TextBox
    Friend WithEvents txtDocter As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dgvtest As System.Windows.Forms.DataGridView
    Friend WithEvents cbpayment As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtdiscount As System.Windows.Forms.TextBox
    Friend WithEvents lblterbilang As System.Windows.Forms.Label
    Friend WithEvents cmdpayment As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents rdolangsung As System.Windows.Forms.RadioButton
    Friend WithEvents rdotagihkan As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtblmbulat As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtsetelahbulat As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtprice As System.Windows.Forms.TextBox
    Friend WithEvents cmdcetak As System.Windows.Forms.Button
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtnomorkartu As System.Windows.Forms.TextBox
    Friend WithEvents txtuangdibayar As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtkembali As System.Windows.Forms.TextBox
    Private WithEvents rdoanak As System.Windows.Forms.RadioButton
    Friend WithEvents dgpack As System.Windows.Forms.DataGridView
End Class
