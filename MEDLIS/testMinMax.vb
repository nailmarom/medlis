﻿'
' Created by SharpDevelop.
' User: User
' Date: 10/14/2014
' Time: 9:19 AM
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'

Imports System.Data.SqlClient

Partial Public Class testMinMax
    Dim objConn As New clsGreConnect
    Private tblGroupTest As DataTable
    Dim strsql As String
    Dim selectedgroupid As Integer
    Dim selectedtestid As Integer
    Dim isNew As Boolean
    Dim isEdit As Boolean
    Dim dgt As Integer
    Dim objTestRelated As New TestRelated
    Dim displayindex As Integer

    Public Sub New()
        ' The Me.InitializeComponent call is required for Windows Forms designer support.
        Me.InitializeComponent()

        '
        ' TODO : Add constructor code after InitializeComponents
        ''
    End Sub

    Private Sub testTypeFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'fill the combo
        ' cbdigit.SelectedItem = cbdigit.SelectedIndex.MinValue
        Dim strsql As String
        Dim greObject As New clsGreConnect
        Icon = New System.Drawing.Icon("medlis2.ico")
        strsql = "select * from testgroup"
        greObject.buildConn()

        Dim thecommand As New SqlClient.SqlCommand(strsql, greObject.grecon)
        Dim testTypeReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)
        cbGroupChoice.Items.Add("SEMUA TEST")
        Do While testTypeReader.Read
            cbGroupChoice.Items.Add(testTypeReader("testgroupname"))
        Loop

        Dim ctrl As Control
        For Each ctrl In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.Enabled = False 'BackColor = Color.LightYellow
            End If
        Next
       
        testTypeReader.Close()
        thecommand.Dispose()
        'NpgsqlConnection.ClearAllPools()

        SaveMnu.Enabled = False


    End Sub
    
    
   
    Private Sub cbGroupChoice_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbGroupChoice.SelectedIndexChanged
        'Dim varIdTestGroup As Integer
        ClearControl()
        ' If Trim(cbGroupChoice.Text) <> "Semua Test" Then
        Dim objTestRelated As New TestRelated
        objConn.buildConn()
        selectedgroupid = objTestRelated.GetIdTestGroup(Trim(cbGroupChoice.Text))
        'tblGroupTest = objConn.ExecuteQuery("Select * from TestType where IdTestGroup='" & varIdTestGroup & "'")
        'dgTestType.DataSource = tblGroupTest
        objConn.CloseConn()
        PopulateDg()
        isEdit = False
        isNew = False
        ' ElseIf Trim(cbGroupChoice.Text) = "Semua Test" Then

        '  End If
    End Sub


   

    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click

        Dim testName As String
        Dim minanak As String
        Dim maxanak As String
        Dim minwanita As String
        Dim maxwanita As String
        Dim minpria As String
        Dim maxpria As String



        If Trim(txtminanak.Text).Length > 0 Then
            minanak = txtminanak.Text
        Else
            minanak = "0"
        End If

        If Trim(txtmaxanak.Text).Length > 0 Then
            maxanak = Trim(txtmaxanak.Text)
        Else
            maxanak = "0"
        End If

        If Trim(txtminwanita.Text).Length > 0 Then
            minwanita = txtminwanita.Text
        Else
            minwanita = "0"
        End If

        If Trim(txtmaxwanita.Text).Length > 0 Then
            maxwanita = txtmaxwanita.Text
        Else
            maxwanita = "0"
        End If

        If Trim(txtmindewasa.Text).Length > 0 Then
            minpria = txtmindewasa.Text
        Else
            minpria = "0"
        End If

        If Trim(txtmaxdewasa.Text).Length > 0 Then
            maxpria = txtmaxdewasa.Text
        Else
            maxpria = "0"
        End If


        



        strsql = "update testtype " & _
        "set minchild='" & minanak & _
        "',maxchild='" & maxanak & _
        "',minwoman='" & minwanita & _
        "',maxwoman='" & maxwanita & _
        "',min='" & minpria & _
        "',max='" & maxpria & _
        "' where id='" & selectedtestid & "'"

        objConn.buildConn()
        objConn.ExecuteNonQuery(strsql)
        objConn.CloseConn()

        'mantep 2
        Dim ctrl As Control
        For Each ctrl In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.Text = ""
                txt.Enabled = False 'BackColor = Color.LightYellow
            End If
        Next

        isEdit = False

        PopulateDg()
        SaveMnu.Enabled = False
        ClearControl()
    End Sub

    Private Function IsNameAvailable(ByVal AnalyzerTestName As String) As Boolean
        Dim result As Boolean
        Dim str As String
        str = "select count(analyzertestname) as num from testtype where analyzertestname='" & AnalyzerTestName & "'"
        Dim ocek As New clsGreConnect
        ocek.buildConn()

        Dim cmd As New SqlClient.SqlCommand(str, ocek.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            If rdr("num") > 0 Then
                result = True
            Else
                result = False
            End If
        Loop

        rdr.Close()
        cmd.Dispose()
        cmd = Nothing

        ocek.CloseConn()
        ocek = Nothing

        Return result
    End Function

    Private Function IsNameAvailableOnUpdate(ByVal AnalyzerTestName As String) As Boolean
        Dim result As Boolean
        Dim str As String
        str = "select count(analyzertestname) as num from testtype where analyzertestname='" & AnalyzerTestName & "' and id<>'" & selectedtestid & "'"
        Dim ocek As New clsGreConnect
        ocek.buildConn()

        Dim cmd As New SqlClient.SqlCommand(str, ocek.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            If rdr("num") > 0 Then
                result = True
            Else
                result = False
            End If
        Loop

        rdr.Close()
        cmd.Dispose()
        cmd = Nothing

        ocek.CloseConn()
        ocek = Nothing

        Return result
    End Function


   

    
    Private Sub ClearControl()
        Dim ctrl As Control
        For Each ctrl In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.Text = ""
                txt.Enabled = False 'BackColor = Color.LightYellow
            End If
        Next

    End Sub


    Private Sub WakeControl()
        Dim ctrl As Control
        For Each ctrl In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                '  txt.Text = ""
                txt.Enabled = True 'BackColor = Color.LightYellow
                txt.ReadOnly = False
            End If
        Next

        For Each ctrl In GroupBox1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                '  txt.Text = ""
                txt.Enabled = True 'BackColor = Color.LightYellow
                txt.ReadOnly = False
            End If
        Next

        For Each ctrl In GroupBox2.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                '  txt.Text = ""
                txt.Enabled = True 'BackColor = Color.LightYellow
                txt.ReadOnly = False
            End If
        Next

        For Each ctrl In GroupBox4.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                '  txt.Text = ""
                txt.Enabled = True 'BackColor = Color.LightYellow
                txt.ReadOnly = False
            End If
        Next

        txtanalyzerTestName.ReadOnly = True
        txtTestName.ReadOnly = True
    End Sub

    Private Sub EditMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditMnu.Click

        ' Try


        Dim isavailable As Boolean 'top check yang di centang
        isavailable = False
        Dim i As Integer
        For i = 0 To dgTestType.Rows.Count - 1
            'If Not (dgTestType.CurrentRow.IsNewRow) Then
            If dgTestType.Item("chk", i).Value = True Then
                isavailable = True
            End If
        Next
        If isavailable = False Then
            MessageBox.Show("Pilih salah satu test item untuk diedit")
            Exit Sub
        End If


        isEdit = True
        isNew = False
        WakeControl()
        SaveMnu.Enabled = True

        For i = 0 To dgTestType.Rows.Count - 1
            If dgTestType.Item("chk", i).Value = True Then
                If Not (dgTestType.CurrentRow.IsNewRow) Then
                    If Not (IsDBNull(dgTestType.Item("id", i).Value)) Then
                        'selectedtestGroup = dgTestGroup.Item("id", i).Value
                        txtTestName.Text = dgTestType.Item("testname", i).Value

                        txtanalyzerTestName.Text = dgTestType.Item("analyzertestname", i).Value
                      
                        
                    End If
                End If


            End If
        Next

        
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub PopulateDg()
        Dim strsql As String
        If cbGroupChoice.Text <> "SEMUA TEST" Then
            strsql = "Select * from testtype where idtestgroup='" & selectedgroupid & "' order by testname asc"
        ElseIf cbGroupChoice.Text = "SEMUA TEST" Then
            strsql = "Select * from testtype" ' where idtestgroup='" & selectedgroupid & "' order by testname asc"
        End If


        objConn.buildConn()

        tblGroupTest = New DataTable
        tblGroupTest = objConn.ExecuteQuery(strsql) '"Select * from testtype where idtestgroup='" & selectedgroupid & "' order by testname asc")

        dgTestType.AllowUserToAddRows = False
        dgTestType.RowHeadersVisible = False

        'dgTestType = New DataGridView
        dgTestType.DataSource = tblGroupTest
        Dim j As Integer
        For j = 0 To dgTestType.Columns.Count - 1
            dgTestType.Columns(j).Visible = False
        Next

        If Not dgTestType.Columns.Contains("chk") Then
            Dim chk As New DataGridViewCheckBoxColumn()

            dgTestType.Columns.Add(chk)
            chk.HeaderText = "SELECT"
            chk.Width = 60
            chk.Name = "chk"

            dgTestType.Columns("chk").DisplayIndex = 0
            dgTestType.Columns("chk").Name = "chk"
        End If

        dgTestType.Columns("chk").Visible = True
        dgTestType.Columns("testname").Visible = True
        dgTestType.Columns("testname").HeaderText = "TEST ITEM"
        dgTestType.Columns("testname").Width = 120
        dgTestType.Columns("testname").ReadOnly = True

        dgTestType.Columns("analyzertestname").Visible = True
        dgTestType.Columns("analyzertestname").HeaderText = "ANALYZER TEST NAME"
        dgTestType.Columns("analyzertestname").Width = 180
        dgTestType.Columns("analyzertestname").ReadOnly = True
        'dgTestType.Columns("id").Visible = False
        'DGPack.Columns("packname").Width = 250




        objConn.CloseConn()


    End Sub

    Private Sub dgTestType_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgTestType.CellContentClick
        'If Not (dgTestType.CurrentRow.IsNewRow) Then
        ClearControl()
        If Not e.RowIndex = -1 Then 'untuk detect bahwa bukan header
            If Not IsDBNull(dgTestType.Item("id", e.RowIndex).Value) Then
                If TypeOf dgTestType.Rows(e.RowIndex).Cells(e.ColumnIndex) Is DataGridViewCheckBoxCell Then
                    Dim dgvCheckBoxCell As DataGridViewCheckBoxCell = dgTestType.Rows(e.RowIndex).Cells(e.ColumnIndex)
                    'Commit the data to the datasouce.
                    dgTestType.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    Dim checked As Boolean = CType(dgvCheckBoxCell.Value, Boolean)
                    selectedtestid = dgTestType.Item("id", e.RowIndex).Value
                    'bagus
                    'MessageBox.Show(e.RowIndex & " " & checked & " " & e.ColumnIndex)

                    If checked Then
                        Dim i As Integer
                        For i = 0 To dgTestType.Rows.Count - 1
                            If i <> e.RowIndex Then
                                dgTestType.Item("chk", i).Value = False
                            End If
                        Next

                    End If





                End If
            End If
            ' End If

            WakeControl()
            'SaveMnu.Enabled = false

            For i = 0 To dgTestType.Rows.Count - 1
                If dgTestType.Item("chk", i).Value = True Then
                    If Not (dgTestType.CurrentRow.IsNewRow) Then
                        If Not (IsDBNull(dgTestType.Item("id", i).Value)) Then
                            'selectedtestGroup = dgTestGroup.Item("id", i).Value
                            If Not IsDBNull(dgTestType.Item("testname", i).Value) Then
                                txtTestName.Text = dgTestType.Item("testname", i).Value

                            End If
                            txtTestName.ReadOnly = True

                            If Not IsDBNull(dgTestType.Item("analyzertestname", i).Value) Then
                                txtanalyzerTestName.Text = dgTestType.Item("analyzertestname", i).Value

                            End If
                            txtanalyzerTestName.ReadOnly = True

                            If Not IsDBNull(dgTestType.Item("min", i).Value) Then
                                txtmindewasa.Text = dgTestType.Item("min", i).Value
                            Else
                                txtmindewasa.Text = ""
                            End If
                            txtmindewasa.ReadOnly = True


                            If Not IsDBNull(dgTestType.Item("max", i).Value) Then
                                txtmaxdewasa.Text = dgTestType.Item("max", i).Value
                            Else
                                txtmaxdewasa.Text = ""
                            End If
                            txtmaxdewasa.ReadOnly = True

                            If Not IsDBNull(dgTestType.Item("minwoman", i).Value) Then
                                txtminwanita.Text = dgTestType.Item("minwoman", i).Value
                            Else
                                txtminwanita.Text = ""
                            End If
                            txtminwanita.ReadOnly = True

                            If Not IsDBNull(dgTestType.Item("maxwoman", i).Value) Then
                                txtmaxwanita.Text = dgTestType.Item("maxwoman", i).Value
                            Else
                                txtmaxwanita.Text = ""
                            End If
                            txtmaxwanita.ReadOnly = True

                            If Not IsDBNull(dgTestType.Item("minchild", i).Value) Then
                                txtminanak.Text = dgTestType.Item("minchild", i).Value
                            Else
                                txtminanak.Text = ""
                            End If
                            txtminanak.ReadOnly = True

                            If Not IsDBNull(dgTestType.Item("maxchild", i).Value) Then
                                txtmaxanak.Text = dgTestType.Item("maxchild", i).Value
                            Else
                                txtmaxanak.Text = ""
                            End If
                            txtmaxanak.ReadOnly = True

                        End If
                    End If
                End If
            Next
            

        End If
    End Sub
    Private Function GetGroupName(ByVal idgroup As Integer) As String
        Dim strsql As String
        Dim greObject As New clsGreConnect
        Dim result As String
        strsql = "select id,testgroupname from testgroup where id='" & idgroup & "'"
        greObject.buildConn()

        Dim thecommand As New SqlClient.SqlCommand(strsql, greObject.grecon)
        Dim testTypeReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)

        Do While testTypeReader.Read
            result = testTypeReader("testgroupname")
        Loop
        Return result
    End Function

    Private Sub DeleteMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim i As Integer
        Dim isavailable As Boolean
        isavailable = False
        For i = 0 To dgTestType.Rows.Count - 1
            If dgTestType.Item("chk", i).Value = True Then
                strsql = "delete from testtype " & _
                "where id='" & selectedtestid & "'"
                isavailable = True
                objConn.buildConn()
                objConn.ExecuteNonQuery(strsql)
                objConn.CloseConn()
            End If
        Next
        If isavailable = False Then
            MessageBox.Show("Tidak ada data yang dipilih")
        End If

        PopulateDg()
    End Sub

    Private Sub cancelMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelMnu.Click
        'Dim ctrl As Control
        'For Each ctrl In Panel1.Controls
        '    If (ctrl.GetType() Is GetType(TextBox)) Then
        '        Dim txt As TextBox = CType(ctrl, TextBox)
        '        txt.Text = ""
        '        txt.Enabled = False 'BackColor = Color.LightYellow
        '    End If
        'Next
        'cbTestGroup.Enabled = False
        SaveMnu.Enabled = False

        isEdit = False
        isNew = False
        ClearControl()
        PopulateDg()
    End Sub

    Private Sub txtHarga_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsDigit(e.KeyChar) = False And Char.IsControl(e.KeyChar) = False Then
            e.Handled = True
            MsgBox("Masukkan Angka") 'bagus
        End If
    End Sub


    Private Sub Label15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim objFrmContoh As New ContohFrm
        'objFrmContoh.StartPosition = FormStartPosition.Manual
        'objFrmContoh.Show()
    End Sub

    Private Sub Label15_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Cursor = Cursors.Hand
    End Sub

    Private Sub Label15_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Cursor = Cursors.Default
    End Sub

    Private tips As New ToolTip()

    Private Sub txtanalyzerTestName_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtanalyzerTestName.MouseClick
        tips.Show("Nama test pendek sesuai instrument: ALB", txtTestName, 3000)
    End Sub
   
    
    Private Sub txtTestName_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtTestName.MouseClick
        tips.Show("Nama test panjang misal: Albumin", txtTestName, 3000)
    End Sub

    Private Sub txtNilaiRujukan_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        tips.Show("Rujukan yang tercetak di laporan", txtTestName, 3000)
    End Sub

   
   
   

   

    Private Sub showurutan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objdisplay As New ShowItemDisplayIndexFrm
        objdisplay.ShowDialog()
    End Sub

    Private Sub showurutan_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Cursor = Cursors.Hand

    End Sub

    Private Sub showurutan_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub txturutan_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub hargaLevel2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cbrelatedX_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cbrelatedY_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class
