﻿Imports System.Net.Sockets
Imports System.Text
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports System.IO

Public Class palioSocketComm

    Dim clientSocket As New System.Net.Sockets.TcpClient()
    Dim serverStream As NetworkStream
    Dim iforascii As Integer

    Dim GreenLamp As Boolean
    Dim GreenLampForResult As Boolean
    Dim GreenLampHearResult As Boolean


    Dim fpath As String
    Dim fInstrument As String

    Dim selectedLabNumber As String
    Dim selectedPatient As String
    Dim selsampleno As String
    Dim patientName As String

    Dim patientPhone As String
    Dim patientaddress As String


    Dim regex As Regex

    Dim qMcnToLis As New List(Of String)
    Dim oLisToMcn As New List(Of String)
    Dim orMcnToLis As New List(Of String)

    Dim greobject As clsGreConnect
    Dim dtable As DataTable
    Dim stateChoice As Integer
    'counter==============
    Dim seq As Integer
    Dim seqQuery As Integer
    Dim counterP As Integer
    Dim indexToSend As Integer
    Dim wait10second As Integer

    Dim resultIndexToSend As Integer
    '=====================

    Dim queryCreate As New List(Of String)
    Dim qforRCreate As New List(Of String)
    Dim resultToManage As List(Of String)




    Dim idinstrument As Integer
    Dim objconn As New clsGreConnect


    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        Dim strIpaddress As String
        GreenLamp = False
        GreenLampForResult = False
        GreenLampHearResult = False

        'msg("Client Started")
        Try
            strIpaddress = Trim(txtIpaddress.Text)
            'clientSocket.Connect("192.0.0..", 4000)
            clientSocket.Connect(strIpaddress, 4000)
            Label1.Text = "Client Socket Program - Server Connected ..."
            GreenLamp = True
            GreenLampForResult = True
            btnConnect.Enabled = False
            cmdQuery.Enabled = True
            btnSend.Enabled = True

        Catch ex As Exception
            btnConnect.Enabled = True
            cmdQuery.Enabled = False
            btnSend.Enabled = False

            GreenLamp = False
            GreenLampForResult = False
            MessageBox.Show("can not connect")
            msg("can not connect")
        End Try

    End Sub

    Sub msg(ByVal mesg As String)
        txtout.Text = txtout.Text + Environment.NewLine + " >> " + mesg
        lblmsg.Text = mesg
        If lblmsg.Text = "stxklmn" Then
            Label1.Text = "Berhasil"
        End If

    End Sub

    '=============================
    'send data
    '=============================

    Private Sub SendAndReceivedData(ByVal str As String)
        Try

            ' Kirim data
            '«STX»1H|\^&|||Miura|||||||P|LIS2-
            Dim strTosend As String
            Dim serverStream As NetworkStream = clientSocket.GetStream()
            'Dim outStream As Byte() = _
            'System.Text.Encoding.ASCII.GetBytes("Message from Client$")

            ' Dim str As String
            'str = Chr(2) & "1H|\^&|||Miura|||||||P|LIS2-A2|20110112100000" & Chr(13) & Chr(3) & "66" & Chr(13) & Chr(10)
            'Label7.Text = str
            Dim inStream(10024) As Byte
            strTosend = str

            Dim outStream As Byte() = _
            System.Text.Encoding.ASCII.GetBytes(strTosend)
            msg("Data to send:" + strTosend)
            'Label2.Text = strTosend

            serverStream.Write(outStream, 0, outStream.Length)
            serverStream.Flush()

            If strTosend = Chr(4) Then
                Label1.Text = "EOT"
                btnDisconnect.PerformClick()
                tmrComm.Stop()
            End If



            Dim abslabel As New Label
            '================original=====================
            'Dim inStream(10024) As Byte
            'serverStream.Read(inStream, 0, CInt(clientSocket.ReceiveBufferSize))
            'Dim returndata As String = _
            'System.Text.Encoding.ASCII.GetString(inStream)
            ''msg("Data from Server : " + returndata)
            '' msg(returndata)
            ''If returndata.Contains("stxklmn") Then
            '===============================================



            '======================== benar
            'Do While serverStream.DataAvailable
            '    serverStream.Read(inStream, 0, CInt(clientSocket.ReceiveBufferSize))
            '    returndata = System.Text.Encoding.ASCII.GetString(inStream, 0, CInt(clientSocket.ReceiveBufferSize))

            'Loop
            'If Not returndata Is Nothing Then
            '    If returndata.ToString.Length > 0 Then
            '        msg("You received the following message : " + returndata.ToString())
            '    Else
            '        msg("salah")
            '    End If

            'End If
            '======================= benar

            '####### modifikasi ###########
            Dim myCompleteMessage As StringBuilder = New StringBuilder()
            Dim numberOfBytesRead As Integer = 0

            If serverStream.CanRead Then
                Dim returndata As String


                ' Incoming message may be larger than the buffer size.
                Do While serverStream.DataAvailable
                    If serverStream.DataAvailable Then
                        numberOfBytesRead = serverStream.Read(inStream, 0, inStream.Length)
                        If numberOfBytesRead > 0 Then
                            myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(inStream, 0, numberOfBytesRead))
                        End If
                    End If
                Loop

                If myCompleteMessage.ToString.Length > 0 Then
                    msg("You received the following message : " + myCompleteMessage.ToString())
                Else
                    msg("salah")
                End If
            End If






            '##############################

            'lblmsg.Text = returndata
            lblmsg.Text = myCompleteMessage.ToString

            If Trim(lblmsg.Text) = Chr(6) Then
                msg("ACK")
                indexToSend = indexToSend + 1
                Label2.Text = Label2.Text & CStr(indexToSend)
                If indexToSend = queryCreate.Count Then
                    GreenLamp = False
                    '       MessageBox.Show("timer stop")
                    tmrComm.Stop()
                Else
                    GreenLamp = True
                End If

            ElseIf Trim(lblmsg.Text) = Chr(21) Then
                '    msg("NAK")
                GreenLamp = False

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try

    End Sub




    Private Sub btnDisconnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisconnect.Click
        '  clientSocket.EndConnect()

    End Sub


    Private Sub tmrComm_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmrComm.Tick
        If indexToSend < queryCreate.Count Then
            If GreenLamp Then
                GreenLamp = False
                SendAndReceivedData(queryCreate(indexToSend))
            Else
                If wait10second < 12 Then
                    wait10second = wait10second + 1
                Else
                    GreenLamp = True
                End If
            End If
        Else
            ' MessageBox.Show("timer stop")
            tmrComm.Stop()
            SetStatusLabelIdle()
        End If
    End Sub
    Private Sub SetStatusLabelIdle()
        statusLabel.BackColor = Color.Red
        statusLabel.Text = "IDLE"
        btnSend.Enabled = True
        cmdQuery.Enabled = True
    End Sub
    Private Sub setStatusLabelSendingData()
        cmdQuery.Enabled = False
        statusLabel.BackColor = Color.Green
        statusLabel.Text = "Sending Data"
    End Sub
    Private Sub setStatusLabelReceivingData()
        btnSend.Enabled = False
        statusLabel.BackColor = Color.Blue
        statusLabel.Text = "Receiving Data"
    End Sub


 
    Private Sub Joblist(ByVal strsql As String)
        'Dim strsql As String
        'strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.id as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.status='0'"
        greobject = New clsGreConnect
        greobject.buildConn()

        dtable = New DataTable

        dtable = greobject.ExecuteQuery(strsql)

        'dgvjoblist = New DataGridView

        dgvJoblist.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvJoblist.Columns.Count - 1
            dgvJoblist.Columns(j).Visible = False
        Next

        dgvJoblist.Columns("labnumber").Visible = True
        dgvJoblist.Columns("labnumber").HeaderText = "Lab Number"
        dgvJoblist.Columns("labnumber").Width = 90

        dgvJoblist.Columns("name").Visible = True
        dgvJoblist.Columns("name").HeaderText = "Nama pasien"
        dgvJoblist.Columns("name").Width = 180

        dgvJoblist.Columns("address").Visible = True
        dgvJoblist.Columns("address").HeaderText = "Alamat"
        dgvJoblist.Columns("address").Width = 220

        Dim buttonColumn As New DataGridViewButtonColumn()
        With buttonColumn
            .Name = "Detail"
            .HeaderText = "Detail"
            .Width = 90
            .Text = "Detail"
            .UseColumnTextForButtonValue = True
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            .FlatStyle = FlatStyle.Standard
            .CellTemplate.Style.BackColor = Color.Yellow
            .DisplayIndex = 0
        End With
        dgvJoblist.Columns.Add(buttonColumn)

        dgvJoblist.AllowUserToAddRows = False
        dgvJoblist.RowHeadersVisible = False
    End Sub
    Private Sub ConstructOMsg()
        Dim ENQstr As String
        Dim Hstr As String
        Dim Pstr As String

        Dim Lstr As String
        Dim EOTstr As String


        'construct message
        ENQstr = Chr(5)
        EOTstr = Chr(4)
        Hstr = Chr(2) & "0H|\^" & Chr(13) & Chr(3) & "E5" & Chr(13) & Chr(10)
        Pstr = Chr(2) & "1P|1|||||||||||||^" & Chr(13) & Chr(3) & "E9" & Chr(13) & Chr(10)


        Dim numQ As Integer
        Dim numO As Integer
        Dim i As Integer
        Dim j As Integer

        Dim k As Integer

        Dim FNforL As Integer
        Dim indexOfETX As Integer

        '  Dim labnumber As String
        Dim LstrFirst As String
        Dim LstrSecond As String

        Dim oList As New List(Of String)
        Dim breakOlist As New List(Of List(Of String))

        Dim qMcnToLisAfterSort As New List(Of String)

        'numQ = qMcnToLis.Count
        qMcnToLisAfterSort = qMcnToLis.Distinct.ToList
        numQ = qMcnToLisAfterSort.Count

        oLisToMcn.Add(ENQstr)

        For i = 0 To numQ - 1

            breakOlist = getO(qMcnToLisAfterSort.Item(i))

            For k = 0 To breakOlist.Count - 1
                oLisToMcn.Add(Hstr)
                oLisToMcn.Add(Pstr)

                'oList = getO(qMcnToLisAfterSort.Item(i)) 'bikin pesan
                oList = New List(Of String)
                oList = breakOlist.Item(k)
                'oLisToMcn.AddRange(getO(qMcnToLis.Item(i)))
                oLisToMcn.AddRange(oList)
                numO = oList.Count

                'j = 0
                'labnumber = 
                'For j = 0 To getTestForSample(labnumber) - 1
                '    oLisToMcn.Add(Ostr)
                'Next
                FNforL = 3 + numO
                LstrFirst = Chr(2) & CStr(FNforL) & "L|1|" & "N" & Chr(13) & Chr(3)
                indexOfETX = LstrFirst.IndexOf(Chr(3))
                LstrSecond = checkSumCalculator(Mid(LstrFirst, 2, indexOfETX)) & Chr(13) & Chr(10)
                Lstr = LstrFirst & LstrSecond
                oLisToMcn.Add(Lstr)
            Next
        Next
        oLisToMcn.Add(EOTstr)

        '  curMsgState = 0
        '  serialPort1.Write(Chr(5)) 'ENQ    mengirim enq untuk pertama kali
        'If isENQ = False Then
        '    serialPort1.Write(Chr(5)) 'ENQ 
        '    isENQ = True
        'End If
    End Sub
    Private Function getO(ByVal strQ As String) As List(Of List(Of String))
        'connect to database, find testTypeName i.e 3 name
        'construct message
        ' there are 3 i.e AST, TP, ALB, count 3
        '

        Dim result As New List(Of String)
        result.Clear()

        Dim bunchresult As New List(Of List(Of String))

        Dim i As Integer
        Dim numRec As Integer
        Dim str As String

        Dim FN As Integer
        Dim seq As Integer
        Dim firstStr As String
        Dim secondStr As String
        Dim indexOfSTX As Integer


        Dim objGDM As ClassgetDetailQMessage
        objGDM = New ClassgetDetailQMessage(strQ, fInstrument)
        objGDM.DetectSample()

        numRec = objGDM.testTypeForSingleLabNumber.Count
        'numRec = 3 'misal
        '----------------#########
        Dim quotient, reminder As Integer
        If numRec > 3 Then
            quotient = Math.DivRem(numRec, 3, reminder)
        End If
        '--#######################
        FN = 3
        seq = 1
        For i = 0 To numRec - 1
            'FN = i + 3
            'seq = i + 1

            str = ""
            firstStr = ""
            secondStr = ""
            firstStr = Chr(2) & CStr(FN) & "O|" & seq & "|" & objGDM.strSample & "||" & "^^^" & objGDM.testTypeForSingleLabNumber.Item(i) & "|" & objGDM.priority(i) & "|" & objGDM.requestDate(i) & "|||||||||" & objGDM.specimenDesc(i) & "||||||||||" & objGDM.reportType(i) & Chr(13) & Chr(3) '& checkNum & Chr(13) & Chr(10)
            indexOfSTX = firstStr.IndexOf(Chr(3))
            secondStr = checkSumCalculator(Mid(firstStr, 2, indexOfSTX)) & Chr(13) & Chr(10)
            str = firstStr & secondStr
            result.Add(str)
            FN = FN + 1
            seq = seq + 1
            If numRec > 2 Then
                If FN > 5 Then
                    If FN Mod 3 = 0 Then
                        bunchresult.Add(result)
                        result = New List(Of String)
                        FN = 3
                        seq = 1
                    End If
                End If
            End If
        Next
        bunchresult.Add(result)

        objGDM = Nothing
        'Return result
        Return bunchresult

    End Function


    Private Sub ClearALL()

        'McnIsQ = False
        'McnIsOR = False

        qMcnToLis = New List(Of String)
        oLisToMcn = New List(Of String)
        orMcnToLis = New List(Of String)

        ' SerialPort1.DiscardOutBuffer() 'clear buffer
    End Sub

    Private Class ClassgetDetailQMessage
        Dim strMsg As String

        Public strSample As String
        Public isBarcodeMode As Boolean
        Public isSampleIdMode As Boolean

        Public testTypeForSingleLabNumber As New List(Of String)
        Public priority As New List(Of String)
        Public requestDate As New List(Of String)
        Public specimenDesc As New List(Of String)
        Public reportType As New List(Of String)

        Public sampleNoBeingRead As List(Of KeyValuePair(Of String, String))
        Public barcodeBeingRead As List(Of KeyValuePair(Of String, String))

        Dim theInstrumentname As String
        ' Dim da As Npgsql.NpgSqlClient.SqlDataAdapter
        Dim id As String

        Public Sub New(ByVal str As String, ByVal iname As String)
            strMsg = str
            theInstrumentname = iname
        End Sub

        Public Sub DetectSample()
            Dim i As Integer
            Dim msg As String
            Dim regex As Regex
            Dim str As String

            isBarcodeMode = False
            isSampleIdMode = False

            ' strMsg = Chr(2) & "2Q|1|^130^5^45^N||ALL|||||||O" & Chr(13) & Chr(3) & "B4" & Chr(13) & Chr(10)

            i = strMsg.IndexOf(Chr(13))
            msg = Mid(strMsg, 2, i - 1)


            Dim result As String()
            result = regex.Split(msg, "\|")
            Dim typeofreport As String
            ' Label2.Text = result.Count
            typeofreport = result.ElementAt(12)

            Dim dictQueryMessage As New Dictionary(Of String, String)
            Dim dictSampleDetail As New Dictionary(Of String, String)


            Dim strSampleDetail As String
            Dim tubeDetail As String()

            strSampleDetail = result.ElementAt(2)
            strSample = strSampleDetail

            tubeDetail = regex.Split(strSampleDetail, "\^")

            If Mid(strSampleDetail, 1, 1) = "^" Then 'sample No mode
                dictSampleDetail.Add("sampleNumber", tubeDetail.ElementAt(1))
                dictSampleDetail.Add("diskNumber", tubeDetail.ElementAt(2))
                dictSampleDetail.Add("positionNumber", tubeDetail.ElementAt(3))
                dictSampleDetail.Add("diluent", tubeDetail.ElementAt(4))
                isSampleIdMode = True
            Else
                dictSampleDetail.Add("sampleIDbarcode", tubeDetail.ElementAt(0))
                dictSampleDetail.Add("diskNumber", tubeDetail.ElementAt(2))
                dictSampleDetail.Add("positionNumber", tubeDetail.ElementAt(3))
                dictSampleDetail.Add("diluent", tubeDetail.ElementAt(4))
                isBarcodeMode = True

            End If

            'typeofreport = 
            'dictQueryMessage.Add("rectype", Mid(result.ElementAt(0), 2, 1))
            'dictQueryMessage.Add("seqNumber", result.ElementAt(1))
            'dictQueryMessage.Add("sampIdentity", result.ElementAt(2))
            'dictQueryMessage.Add("universalTestId", result.ElementAt(4))
            'dictQueryMessage.Add("ReqInfoStatusCode", result.ElementAt(11))


            Dim greObject As New clsGreConnect
            Dim strsql As String
            Dim strbarcode As String
            Dim strsampleno As String

            If isBarcodeMode Then
                strbarcode = dictSampleDetail.Item("sampleIDbarcode")
                '--orinya - strsql = "select jobdetail.id,jobdetail.idtesttype,jobdetail.universaltest,testtype.testname as testname,jobdetail.priority,jobdetail.dateorder,jobdetail.specimendesc from jobdetail,testtype where jobdetail.idtesttype=testtype.id and jobdetail.barcode='" & strbarcode & "' and jobdetail.status='" & ALREADYSAMPLING & "'"
                'and jobdetail.idtesttype in (select idtest from instrument where instrument name='" & currentInstrument & "') 
                strsql = "select jobdetail.id,jobdetail.idtesttype,jobdetail.universaltest,jobdetail.priority,jobdetail.dateorder,jobdetail.specimendesc from jobdetail,instrument where jobdetail.barcode='" & strbarcode & "' and jobdetail.status='" & ALREADYSAMPLING & "' and jobdetail.idtesttype=instrument.idtest and instrument.name='" & theInstrumentname & "'"
            ElseIf isSampleIdMode Then
                strsampleno = dictSampleDetail.Item("sampleNumber")
                ' -orinya - strsql = "select jobdetail.id,jobdetail.idtesttype,jobdetail.universaltest,testtype.testname as testname,jobdetail.priority,jobdetail.dateorder,jobdetail.specimendesc from jobdetail,testtype where jobdetail.idtesttype=testtype.id and jobdetail.sampleno='" & strsampleno & "' and jobdetail.status='" & ALREADYSAMPLING & "'"
                'and jobdetail.idtesttype in (select idtest from instrument where instrument name='" & currentInstrument & "') 
                strsql = "select jobdetail.id,jobdetail.idtesttype,jobdetail.universaltest,jobdetail.priority,jobdetail.dateorder,jobdetail.specimendesc from jobdetail,instrument where jobdetail.sampleno='" & strsampleno & "' and jobdetail.status='" & ALREADYSAMPLING & "' and jobdetail.idtesttype=instrument.idtest and instrument.name='" & theInstrumentname & "'"
            End If




            greObject.buildConn()

            Dim thecommand As New SqlClient.SqlCommand(strsql, greObject.grecon)
            Dim labNumberReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)
            Dim idintjobdetail As Integer
            '--for certain sampleno
            '--there should be status for each testname

            Do While labNumberReader.Read
                testTypeForSingleLabNumber.Add(labNumberReader("universaltest"))
                priority.Add(labNumberReader("priority"))
                requestDate.Add(labNumberReader("dateorder"))
                specimenDesc.Add(labNumberReader("specimendesc"))

                idintjobdetail = labNumberReader("id")

                reportType.Add(typeofreport)

                If isBarcodeMode = True Then

                    ' barcodeBeingRead.Add(New KeyValuePair(Of String, String)(strbarcode, labNumberReader("universaltest")))
                    TagJobDetailBeingAnalyze(idintjobdetail)
                ElseIf isSampleIdMode = True Then
                    'sampleNoBeingRead.Add(New KeyValuePair(Of String, String)(strsampleno, labNumberReader("universaltest")))
                    TagJobDetailBeingAnalyze(idintjobdetail)
                End If

            Loop
            greObject.CloseConn()
            '  cbTestGroup.Enabled = False
            'Chr(2)-> [STX]
            'Chr(3)-> [ETX]
            'Chr(13)->[CR]
            'Chr(10)->[LF]
            'chr(4)

            '-----------db operation to find test type
            '------------
        End Sub
        Private Sub TagJobDetailBeingAnalyze(ByVal idjobdetail As Integer)
            Dim strtag As String
            Dim updateconn As New clsGreConnect
            updateconn.buildConn()
            strtag = "update jobdetail set status='" & BEINGANALYZE & "' where id='" & idjobdetail & "'" 'status 8 = sedang analisa 
            updateconn.ExecuteNonQuery(strtag)
            updateconn.CloseConn()
        End Sub

    End Class

    Private Sub palioSocketComm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadInstrumentName()
        fpath = System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData, "palioinstrument1.txt")
        If ReadInsProperty() = True Then
            cbPalioAvailable.SelectedIndex = cbPalioAvailable.FindStringExact(fInstrument)
        End If
        '================= load
        stateChoice = ALREADYSAMPLING
        Dim strsql As String
        strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail,palioinstrument where status='" & ALREADYSAMPLING & "' and active='" & ACTIVESAMPLE & "' and jobdetail.idtesttype=palioinstrument.idtest and palioinstrument.name='" & fInstrument & "')"

        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If
        clearDGVdetail()
        Joblist(strsql)

        SetStatusLabelIdle()
    End Sub
    Private Sub clearDGVdetail()
        dgvdetail.DataSource = Nothing
    End Sub
    Private Sub LoadInstrumentName()
        Dim strins As String
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        strins = "select distinct name from palioinstrument"
        Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            cbPalioAvailable.Items.Add(rdr("name"))
        Loop
        cbPalioAvailable.SelectedIndex = 0
        rdr.Close()
        cmd = Nothing
        ogre.CloseConn()

    End Sub

    Private Sub ShowInstrumentProperty()
        Dim strins As String
        Dim ogre As New clsGreConnect
        ogre.buildConn()



        strins = "select * from palioinstrumentconnect where name='" & Trim(cbPalioAvailable.Text) & "'"
        Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("ipaddress")) Then
                    txtIpaddress.Text = rdr("ipaddress")
                End If

            Loop
        End If

    End Sub

    Public Sub WriteInsProperty()

        'Dim i As Integer
        'Dim aryText(4) As String
        Dim insName As String


        'aryText(0) = "Port=" & fportName
        'aryText(1) = "Baud=" & fbaud
        'aryText(2) = "Parity=" & fparity
        'aryText(3) = "Stop=" & fstop
        'aryText(4) = "Data=" & fdata
        'aryText(5) = "Instrument=" & fInstrument
        insName = "Instrument=" & cbPalioAvailable.Text

        Dim objWriter As New System.IO.StreamWriter(fpath)
        objWriter.WriteLine(insName)

        'For i = 0 To 3
        '    objWriter.WriteLine(aryText(i))
        'Next
        objWriter.Close()
    End Sub

    Private Function ReadInsProperty() As Boolean

        Dim InsName As String

        Dim result As Boolean


        Dim getstr As String
        Dim position As Integer


        If System.IO.File.Exists(fpath) Then
            Try

                Dim lines() As String = IO.File.ReadAllLines(fpath)
                Dim k As Integer
                k = 0
                For Each line As String In lines

                    position = InStr(line, "=")
                    For i = position To Len(line) - 1
                        getstr = getstr + line.Substring(i, 1)
                    Next


                    InsName = getstr
                    getstr = ""
                Next


                'fportName = arrSetting(0)
                'fbaud = arrSetting(1)
                'fparity = arrSetting(2)
                'fstop = arrSetting(3)
                'fdata = arrSetting(4)
                fInstrument = InsName
                result = True
            Catch ex As Exception
                MessageBox.Show("Seeting file tidak ada", "File not found", MessageBoxButtons.OK)


            End Try
        Else
            result = False
        End If

        Return result

    End Function


    Private Sub btnPalioSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPalioSimpan.Click
        WriteInsProperty()

        '========== update instrument
        Dim strupdate As String
        strupdate = "update palioinstrumentconnect set ipaddress='" & Trim(txtIpaddress.Text) & "' where name='" & Trim(cbPalioAvailable.Text) & "'"

        Dim ogre As New clsGreConnect
        ogre.buildConn()
        ogre.ExecuteNonQuery(strupdate)
        ogre.CloseConn()
        '=============================
        MessageBox.Show("Perubahan sudah tersimpan")
    End Sub

    Private Sub dgvJoblist_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvJoblist.CellContentClick
        Dim idx As Integer
        idx = dgvJoblist.Columns("Detail").Index
        'txtlabnum.Text = ""
        If e.RowIndex < 0 OrElse Not e.ColumnIndex = dgvJoblist.Columns("Detail").Index Then

            Return

        Else

            If Not dgvJoblist.CurrentRow.IsNewRow Then

                selectedLabNumber = dgvJoblist.Item("labnumber", dgvJoblist.CurrentRow.Index).Value
                ' txtlabnum.Text = selectedLabNumber
                selectedPatient = dgvJoblist.Item("patientid", dgvJoblist.CurrentRow.Index).Value
                patientName = dgvJoblist.Item("name", dgvJoblist.CurrentRow.Index).Value
                patientaddress = dgvJoblist.Item("address", dgvJoblist.CurrentRow.Index).Value
                patientPhone = dgvJoblist.Item("phone", dgvJoblist.CurrentRow.Index).Value
                selsampleno = ""
                PopulatedgvDetail()
                '  ShowDetail()
            End If

        End If
    End Sub
    Private Sub PopulatedgvDetail()
        Dim newcon As New clsGreConnect
        Dim strsql As String

        'If dgvdetail.RowCount > 0 Then
        '    dgvdetail.Rows.Clear()
        'End If

        'dgvdetail.Rows.Clear()
        If stateChoice = ALLAVAILABLESAMPLE Then 'semua
            ' strsql = "select distinct sampleno,barcode,status from jobdetail where labnumber='" & selectedLabNumber & "' and active='1'" ' and status='0'"
            strsql = "select * from jobdetail where labnumber='" & selectedLabNumber & "' and active='1'" ' and status='0'"
        ElseIf stateChoice = FINISHSAMPLE Then 'finish
            'strsql = "select distinct sampleno,barcode,status from jobdetail where labnumber='" & selectedLabNumber & "' and active='1' and status='" & FINISHSAMPLE & "'"
            strsql = "select * from jobdetail where labnumber='" & selectedLabNumber & "' and active='1' and status='" & FINISHSAMPLE & "'"
        ElseIf stateChoice = ALREADYSAMPLING Then ' sampLing
            'strsql = "select distinct sampleno,barcode,status from jobdetail where labnumber='" & selectedLabNumber & "' and active='1' and status='" & ALREADYSAMPLING & "'"
            strsql = "select * from jobdetail where labnumber='" & selectedLabNumber & "' and active='1' and status='" & ALREADYSAMPLING & "'"
        ElseIf stateChoice = NEWSAMPLE Then ' baru
            'strsql = "select distinct sampleno,barcode,status from jobdetail where labnumber='" & selectedLabNumber & "' and active='1' and status='" & NEWSAMPLE & "'"
            strsql = "select * from jobdetail where labnumber='" & selectedLabNumber & "' and active='1' and status='" & NEWSAMPLE & "'"
        ElseIf stateChoice = BEINGANALYZE Then ' baru
            'strsql = "select distinct sampleno,barcode,status from jobdetail where labnumber='" & selectedLabNumber & "' and active='1' and status='" & BEINGANALYZE & "'"
            strsql = "select * from jobdetail where labnumber='" & selectedLabNumber & "' and active='1' and status='" & BEINGANALYZE & "'"
        End If
        newcon.buildConn()

        Dim strStatus As String
        Dim statusample As String


        dgvdetail.AllowUserToAddRows = False
        dgvdetail.RowHeadersVisible = False

        newcon.buildConn()
        dtable = New DataTable
        ' strsql = "select id,universaltest from jobdetail where labnumber='" & selectedLabNumber & "' and active='" & ACTIVESAMPLE & "' and status='0'"
        dtable = newcon.ExecuteQuery(strsql)

        'dgvtestname = New DataGridView
        dgvdetail.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvdetail.Columns.Count - 1
            dgvdetail.Columns(j).Visible = False
        Next
        'disabled on 09-01-2015 as not needed to add check box
        'If Not dgvdetail.Columns.Contains("chk") Then
        '    Dim chk As New DataGridViewCheckBoxColumn()

        '    dgvdetail.Columns.Add(chk)
        '    chk.HeaderText = "SELECT"
        '    chk.Width = 120
        '    chk.Name = "chk"

        '    dgvdetail.Columns("chk").DisplayIndex = 0
        '    dgvdetail.Columns("chk").Name = "chk"
        'End If

        'dgvdetail.Columns("chk").Visible = True
        dgvdetail.Columns("universaltest").Visible = True
        dgvdetail.Columns("universaltest").HeaderText = "Test Name"
        dgvdetail.Columns("universaltest").Width = 200
        newcon.CloseConn()

    End Sub

    Private Sub cbPalioAvailable_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPalioAvailable.SelectedIndexChanged
        ShowInstrumentProperty()
    End Sub



    Private Sub CreateLanguage()

        Dim counterP As Integer
        Dim counterO As Integer

        Dim lntomake As String
        counterP = 0
        seq = 0


        Dim ogre As New clsGreConnect
        ogre.buildConn()
        Dim strsql As String
        'strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail,palioinstrument where status='" & ALREADYSAMPLING & "' and active='" & ACTIVESAMPLE & "' and jobdetail.idtesttype=palioinstrument.idtest and palioinstrument.name='" & fInstrument & "')"
        'strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone ,jobdetail.labnumber,idtesttype,testtype.analyzertestname,palioinstrument.testbarcode,jobdetail.sampleno,jobdetail.barcode from jobdetail,palioinstrument,job,patient,testtype where job.idpasien=patient.id and jobdetail.status='" & ALREADYSAMPLING & "' and active='" & ACTIVESAMPLE & "' and jobdetail.idtesttype=palioinstrument.idtest and palioinstrument.name='" & fInstrument & "' and job.labnumber=jobdetail.labnumber and jobdetail.idtesttype=testtype.id and testtype.id=palioinstrument.idtest"
        strsql = "select distinct labnumber from jobdetail,palioinstrument where status='" & ALREADYSAMPLING & "' and active='" & ACTIVESAMPLE & "' and jobdetail.idtesttype=palioinstrument.idtest and palioinstrument.name='" & fInstrument & "'"



        Dim cmd As New SqlClient.SqlCommand(strsql, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

        If rdr.HasRows Then

            queryCreate.Add(Chr(5))
            queryCreate.Add(makingH)

            Do While rdr.Read
                If Not IsDBNull(rdr("labnumber")) Then
                    lntomake = rdr("labnumber")
                    queryCreate.Add(makingP(lntomake))
                    queryCreate.AddRange(makingO(lntomake))
                End If
            Loop
            queryCreate.Add(makingL)
            queryCreate.Add(Chr(4)) ' add eot 
        End If


        seq = 0
        lblmsg.Text = "message to send created"
    End Sub

    Function makingH() As String


        Dim str As String
        seq = seq + 1
        ' str = Chr(2) & CStr(seq) & "H|\^&|||PALIO100|||||||P|LIS2-A2|20110112100000" & Chr(13) & Chr(3) & "66" & Chr(13) & Chr(10)

        Dim firstSTR As String
        Dim secondSTR As String
        Dim indexofSTX As Integer

        firstSTR = Chr(2) & CStr(seq) & "H|\^&|||PALIO100|||||||P|LIS2-A2|20110112100000" & Chr(13) & Chr(3)
        indexofSTX = firstSTR.IndexOf(Chr(3))
        secondSTR = checkSumCalculator(Mid(firstSTR, 2, indexofSTX)) & Chr(13) & Chr(10)

        str = firstSTR & secondSTR

        Return str
    End Function
    Function makingL() As String
        Dim resultL As String
        Dim firstSTR As String
        Dim secondSTR As String
        Dim counterL As Integer
        Dim indexofSTX As Integer

        counterL = 1

        seq = seq + 1
        firstSTR = Chr(2) & CStr(seq) & "L|" & counterL & "|N" & Chr(13) & Chr(3)
        indexofSTX = firstSTR.IndexOf(Chr(3))
        secondSTR = checkSumCalculator(Mid(firstSTR, 2, indexofSTX)) & Chr(13) & Chr(10)

        resultL = firstSTR & secondSTR
        Return resultL
    End Function

    Function makingP(ByVal ln As String) As String
        Dim resultP As String
        Dim firststr As String
        Dim secondstr As String
        Dim indexofSTX As Integer
        Dim birthdate As String

        seq = seq + 1
        counterP = counterP + 1
        Dim strsqlP As String
        strsqlP = "select distinct job.id as jobid,job.idpasien,patient.patientname as name,patient.birthdate as tgllahir,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone ,jobdetail.labnumber,jobdetail.sampleno,jobdetail.barcode from jobdetail,palioinstrument,job,patient,testtype where job.idpasien=patient.id and jobdetail.status='1' and active='1' and jobdetail.idtesttype=palioinstrument.idtest and palioinstrument.name='palio100' and job.labnumber=jobdetail.labnumber and jobdetail.idtesttype=testtype.id and testtype.id=palioinstrument.idtest and JOBDETAIL.labnumber='" & ln & "'"

        Dim ogreP As New clsGreConnect
        ogreP.buildConn()

        Dim cmdP As New SqlClient.SqlCommand(strsqlP, ogreP.grecon)
        Dim rdrP As SqlDataReader = cmdP.ExecuteReader(CommandBehavior.CloseConnection)

        If rdrP.HasRows Then
            Do While rdrP.Read
                '
                'firststr = Chr(2) & CStr(seq) & "P|" & counterP & "||" & rdrP("") & "||" & rdrP("name") & "|||||||||||||||||||U|S|||||A" & Chr(3)
                'secondstr =  
                'firststr = Chr(2) & CStr(seq) & "P|" & counterP & "||" & rdrP("barcode") & "||" & rdrP("name") & "||" & "19800907|M|W|" & "|||||||||||||||||||U|S|||||A" & Chr(13) & Chr(3)
                birthdate = Replace(rdrP("tgllahir"), "-", "")
                firststr = Chr(2) & CStr(seq) & "P|" & counterP & "||" & rdrP("barcode") & "||" & rdrP("name") & "||" & birthdate & "|M|W|" & "|||||||||||||||||||U|S|||||A" & Chr(13) & Chr(3)
                '0016-20101012
                indexofSTX = firststr.IndexOf(Chr(3))
                secondstr = checkSumCalculator(Mid(firststr, 2, indexofSTX)) & Chr(13) & Chr(10)
                resultP = firststr & secondstr
            Loop
        End If

        Return resultP

    End Function

    Function makingO(ByVal ln As String) As List(Of String)
        ' seq = seq + 1

        Dim firstStr As String
        Dim secondStr As String

        Dim counterO As Integer
        Dim strsqlO As String
        Dim singleO As String
        Dim strO As New List(Of String)
        Dim indexOfSTX As Integer

        counterO = 0
        strsqlO = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone ,jobdetail.labnumber,jobdetail.dateorder,idtesttype,testtype.analyzertestname,palioinstrument.testbarcode,jobdetail.sampleno,jobdetail.barcode from jobdetail,palioinstrument,job,patient,testtype where job.idpasien=patient.id and jobdetail.status='1' and active='1' and jobdetail.idtesttype=palioinstrument.idtest and palioinstrument.name='" & fInstrument & "' and job.labnumber=jobdetail.labnumber and jobdetail.idtesttype=testtype.id and testtype.id=palioinstrument.idtest and jobdetail.labnumber='" & ln & "'"

        '=====================
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim cmdO As New SqlClient.SqlCommand(strsqlO, ogre.grecon)
        Dim rdrO As SqlDataReader = cmdO.ExecuteReader(CommandBehavior.CloseConnection)
        If rdrO.HasRows Then

            Do While rdrO.Read

                If Not IsDBNull(rdrO("labnumber")) Then
                    counterO = counterO + 1
                    seq = seq + 1
                    'firstStr = Chr(2) & seq & "O" & "|" & counterO & "||" & rdrO("barcode") & "|^" & rdrO("testbarcode") & "^^1|R|" & rdrO("dateorder") & "|||||N||||||||||||||O||||||" & Chr(13) & Chr(3)
                    'firstStr = Chr(2) & seq & "O" & "|" & counterO & "||" & "0016-20101012" & "|^" & rdrO("testbarcode") & "^^1|R|" & rdrO("dateorder") & "|||||N||||||||||||||O||||||" & Chr(13) & Chr(3)
                    firstStr = Chr(2) & "3O|1||" & rdrO("barcode") & "|^" & rdrO("testbarcode") & "^^1|R|" & rdrO("dateorder") & "|||||N||||||||||||||O||||||" & Chr(13) & Chr(3)
                    '0016-20101012
                    '3O|1||0016-20101012|^b4^^1|R|20110110100000|||||N||||||||||||||O||||||
                    indexOfSTX = firstStr.IndexOf(Chr(3))
                    secondStr = checkSumCalculator(Mid(firstStr, 2, indexOfSTX)) & Chr(13) & Chr(10)
                    singleO = firstStr & secondStr
                    strO.Add(singleO)
                End If

            Loop

        End If
        '=====================
        Return strO
    End Function
    Private Function checkSumCalculator(ByVal str As String) As String

        Dim array() As Byte = System.Text.Encoding.ASCII.GetBytes(str)
        Dim tempresult As String

        ' Display Bytes.
        Dim rslt As Integer
        rslt = 0
        For Each b As Byte In array
            rslt = rslt + b
        Next
        tempresult = CStr(Conversion.Hex(rslt))
        tempresult = tempresult.Substring(tempresult.Length - 2)
        Return tempresult

    End Function

    Private Function PaliocheckSumCalculator(ByVal str As String) As String

        Dim array() As Byte = System.Text.Encoding.ASCII.GetBytes(str)
        Dim tempresult As String

        ' Display Bytes.
        Dim rslt As Integer
        rslt = 0
        For Each b As Byte In array
            rslt = rslt + b
        Next
        tempresult = CStr(Conversion.Hex(rslt))
        tempresult = tempresult.Substring(tempresult.Length - 2)
        Return tempresult
    End Function


    Private Sub btnSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSend.Click

        indexToSend = 0
        wait10second = 0
        CreateLanguage()
        setStatusLabelSendingData()
        tmrComm.Start()

    End Sub

    Private Sub cmdQuery_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdQuery.Click
        resultIndexToSend = 0
        CreateRequestForResult()
        resultToManage = New List(Of String)
        GreenLampHearResult = True
        wait10second = 0

        setStatusLabelReceivingData()
        tmrResult.Start()


    End Sub

    Private Sub CreateRequestForResult()
        'create request should be use this:
        ' <stx>2q|1|||||||||||F<stx>1F<CR><LF>
        Dim strQforR As String

        qforRCreate.Add(Chr(5))
        qforRCreate.Add(makingH)
        strQforR = Chr(2) & "2Q|1|||||||||||F" & Chr(2) & "1F" & Chr(13) & Chr(10)
        qforRCreate.Add(strQforR)
        ' WriteLogList(qforRCreate)
        seqQuery = 0

    End Sub

    Private Sub tmrResult_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrResult.Tick
        'timerkirim
        If resultIndexToSend < qforRCreate.Count Then
            If GreenLampForResult Then
                GreenLampForResult = False
                ResultSendAndReceivedData(qforRCreate(resultIndexToSend))
            Else
                If wait10second < 12 Then
                    wait10second = wait10second + 1
                Else
                    GreenLampForResult = True
                End If
            End If
        Else
            'If GreenLampHearResult Then
            '    GreenLampHearResult = False
            '    AnswerSendAndReceivedData()
            'Else
            '    If wait10second < 12 Then
            '        wait10second = wait10second + 1
            '    Else
            '        GreenLampHearResult = True
            '    End If
            'End If
            AnswerSendAndReceivedData()
        End If

    End Sub

    Private Sub AnswerSendAndReceivedData()
        '«STX»1H|\^&|||Miura|||||||P|LIS2-
        '========== 
        'result message from palio captured here ================
        '==========

        'KIRIM2
        Dim strTosend As String
        Dim serverStream As NetworkStream = clientSocket.GetStream()
        'Dim outStream As Byte() = _
        'System.Text.Encoding.ASCII.GetBytes("Message from Client$")

        ' Dim str As String
        'str = Chr(2) & "1H|\^&|||Miura|||||||P|LIS2-A2|20110112100000" & Chr(13) & Chr(3) & "66" & Chr(13) & Chr(10)
        'Label7.Text = str

        ' strTosend = str

        'Dim outStream As Byte() = _
        'System.Text.Encoding.ASCII.GetBytes(strTosend)
        'serverStream.Write(outStream, 0, outStream.Length)
        'serverStream.Flush()

        'Dim abslabel As New Label

        Dim inStream(10024) As Byte
        'serverStream.Read(inStream, 0, CInt(clientSocket.ReceiveBufferSize))
        'Dim returndata As String = _
        'System.Text.Encoding.ASCII.GetString(inStream)

        Dim myCompleteMessage As StringBuilder = New StringBuilder()
        Dim numberOfBytesRead As Integer = 0

        If serverStream.CanRead Then
            Dim returndata As String
            ' Incoming message may be larger than the buffer size.
            Do While serverStream.DataAvailable
                If serverStream.DataAvailable Then
                    numberOfBytesRead = serverStream.Read(inStream, 0, inStream.Length)
                    If numberOfBytesRead > 0 Then
                        myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(inStream, 0, numberOfBytesRead))
                    End If
                End If
            Loop

            'If myCompleteMessage.ToString.Length > 0 Then
            '    msg("You received the following message : " + myCompleteMessage.ToString())
            'Else
            '    msg("salah")
            'End If
            lblmsg.Text = myCompleteMessage.ToString
            If Trim(lblmsg.Text) <> "" Then


                If Trim(lblmsg.Text) = Chr(6) Then
                    '   msg("ACK")
                    '  resultIndexToSend = resultIndexToSend + 1
                    ' GreenLampForResult = True
                    'If indexToSend = qforRCreate.Count Then
                    'GreenLampForResult = False
                    'tmrResult.Stop()
                    'End If

                ElseIf Trim(lblmsg.Text) = Chr(21) Then
                    msg("NAK phase 2")
                    GreenLampHearResult = False

                ElseIf lblmsg.Text.Substring(lblmsg.Text.Length - 1) = Chr(10) Then

                    resultToManage.Add(Trim(lblmsg.Text))

                    Dim outStreamForAnswer As Byte() = System.Text.Encoding.ASCII.GetBytes(Chr(6))
                    serverStream.Write(outStreamForAnswer, 0, outStreamForAnswer.Length)
                    serverStream.Flush()
                    GreenLampHearResult = True

                ElseIf Trim(lblmsg.Text) = Chr(4) Then
                    tmrResult.Stop()
                    'process the message
                    'WriteLogListTitle(resultToManage, "list hasil dari mesin")
                    ManageResult(resultToManage)
                    SetStatusLabelIdle()
                End If
            End If
        End If

        'msg("Data from Server : " + myCompleteMessage.ToString)
        ' msg(returndata)
        'If returndata.Contains("stxklmn") Then

        'lblmsg.Text = returndata

    End Sub

    Private Sub ResultSendAndReceivedData(ByVal str As String)
        '«STX»1H|\^&|||Miura|||||||P|LIS2-
        'KIRIM1

        Dim strTosend As String
        Dim serverStream As NetworkStream = clientSocket.GetStream()
        'Dim outStream As Byte() = _
        'System.Text.Encoding.ASCII.GetBytes("Message from Client$")


        Dim inStream(10024) As Byte
        strTosend = str

        Dim outStream As Byte() = _
        System.Text.Encoding.ASCII.GetBytes(strTosend)
        serverStream.Write(outStream, 0, outStream.Length)
        serverStream.Flush()
        WriteLogLineTitle(strTosend, "ke-miura")

        ' Dim str As String
        'str = Chr(2) & "1H|\^&|||Miura|||||||P|LIS2-A2|20110112100000" & Chr(13) & Chr(3) & "66" & Chr(13) & Chr(10)
        'Label7.Text = str
        ' Dim inStream(10024) As Byte
        Dim myCompleteMessage As StringBuilder = New StringBuilder()
        Dim numberOfBytesRead As Integer = 0

        If serverStream.CanRead Then
            Dim returndata As String
            ' Incoming message may be larger than the buffer size.
            Do While serverStream.DataAvailable
                If serverStream.DataAvailable Then
                    numberOfBytesRead = serverStream.Read(inStream, 0, inStream.Length)
                    If numberOfBytesRead > 0 Then
                        myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(inStream, 0, numberOfBytesRead))
                    End If
                End If
            Loop

            If myCompleteMessage.ToString.Length > 0 Then
                msg("You received the following message : " + myCompleteMessage.ToString())
            Else
                msg("phase 1 blank data")
                WriteLogLineTitle("data kosong", "dari miura")
            End If

            lblmsg.Text = myCompleteMessage.ToString
            If Trim(lblmsg.Text) <> "" Then
                WriteLogLineTitle(lblmsg.Text, "dari-miura")
                If Trim(lblmsg.Text) = Chr(6) Then
                    msg("ACK")
                    resultIndexToSend = resultIndexToSend + 1
                    GreenLampForResult = True
                    If indexToSend = qforRCreate.Count Then
                        GreenLampForResult = False
                        'tmrResult.Stop()
                    End If

                ElseIf Trim(lblmsg.Text) = Chr(21) Then
                    msg("NAK Phase 1")
                    GreenLampForResult = False

                ElseIf lblmsg.Text.Substring(lblmsg.Text.Length - 1) = Chr(10) Then
                    'tidak pernah tereksekusi
                    Dim outStreamForAnswer As Byte() = System.Text.Encoding.ASCII.GetBytes(Chr(6))
                    serverStream.Write(outStreamForAnswer, 0, outStreamForAnswer.Length)
                    serverStream.Flush()
                ElseIf Trim(lblmsg.Text) = Chr(4) Then
                    tmrResult.Stop()
                    'process the message
                End If
            End If


        End If




    End Sub

    Private Sub ManageResult(ByVal resultpalio As List(Of String))
        'sampai disini

        Dim barcode As String
        Dim sampleNo As String

        Dim isSampleNoMode As Boolean
        Dim isBarcodeMode As Boolean
        Dim requestDate As String
        Dim regexResult As String()


        Dim mystrucResult(resultpalio.Count - 1) As StrucResult

        ' WriteLogListResultTitle(resultpalio, " dari palio")


        isBarcodeMode = False
        isSampleNoMode = False

        CekLicense()





        Dim i As Integer
        i = 0
        Do While i < resultpalio.Count
            'bersihin string noise disini
            Dim strbersih As String
            'strbersih = orMcnToLis.Item(i).Replace() 

            regexResult = regex.Split(resultpalio.Item(i), "\|")
            If regexResult.ElementAt(0).Substring(regexResult.ElementAt(0).Length - 1) = "O" Then
                'extract O message
                Dim strSampleDetail As String
                Dim palioSampleNo As String
                Dim tubeDetail As String()
                Dim testdetail As String()

                palioSampleNo = regexResult.ElementAt(3)

                strSampleDetail = regexResult.ElementAt(4)
                testdetail = regex.Split(strSampleDetail, "\^")

                mystrucResult(i).idpaliosampleResult = palioSampleNo
                mystrucResult(i).idtestResult = testdetail.ElementAt(1)

                'If testdetail.ElementAt(0) = "" Then
                '    isSampleNoMode = True
                '    isBarcodeMode = False
                '    sampleNo = tubeDetail.ElementAt(1)
                'Else
                '    isSampleNoMode = False
                '    isBarcodeMode = True
                '    barcode = tubeDetail.ElementAt(0)
                'End If

            ElseIf regexResult.ElementAt(0).Substring(regexResult.ElementAt(0).Length - 1) = "R" Then
                'R line

                ' 3R|1|C^b4^NONE^1|0.217299|mg/dl|0.000000:200.000000|N||F||admin|||Miura One3D
                '  0  1  2              3     4      5                 6  7    8  910  11 
                Dim testdetail As String()
                Dim strtestdetail As String

                strtestdetail = regexResult.ElementAt(2)
                testdetail = regex.Split(strtestdetail, "\^")

                If testdetail.ElementAt(1) = mystrucResult(i - 1).idtestResult Then
                    mystrucResult(i - 1).valueTestResult = regexResult.ElementAt(3)
                End If

                Dim testname As String
                Dim value As String
                Dim unit As String
                Dim refrange As String
                Dim resultAbnormalFlag As String
                Dim resultStatus As String
                Dim datecomplete As String

                'testname = regexResult.ElementAt(2).Replace("^", "")
                '========== correct already
                value = mystrucResult(i - 1).valueTestResult
                resultStatus = regexResult.ElementAt(7)
                resultAbnormalFlag = regexResult.ElementAt(6)
                unit = regexResult.ElementAt(4)
                refrange = regexResult.ElementAt(5)
                'datecomplete = regexResult.ElementAt(12).Substring(0, 14) 'sudah bersih
                datecomplete = ""

                Dim universaltest As String

                universaltest = getTestIDforPalio(mystrucResult(i - 1).idtestResult)

                Dim two_range As String()
                Dim normal_val As String
                Dim critical_val As String()

                Dim normal_string As String = ""
                Dim critical_string As String = ""


                Dim lower_normal As String = ""
                Dim upper_normal As String = ""


                Dim lower_critical As String = ""
                Dim upper_critical As String = ""

                Dim strsource As String

                strsource = refrange
                two_range = regex.Split(strsource, "\:")
                If strsource = "" Then
                    lower_normal = ""
                    lower_critical = ""
                    upper_normal = ""
                    upper_critical = ""
                Else
                    normal_string = two_range.ElementAt(0)
                    critical_string = two_range.ElementAt(1)

                    If normal_string = "" Then
                        lower_normal = ""
                        upper_normal = ""
                        lower_critical = ""
                        upper_critical = ""
                    Else
                        normal_val = ""
                        lower_normal = two_range.ElementAt(0)
                        upper_normal = two_range.ElementAt(1)
                        lower_critical = ""
                        upper_critical = ""
                    End If

                End If

                barcode = mystrucResult(i - 1).idpaliosampleResult
                isBarcodeMode = True
                Dim strsql As String
                If isBarcodeMode Then
                    strsql = "update jobdetail " & _
                             "set measurementvalue='" & value & "',resultstatus='" & resultStatus & "'" & _
                             ",resultabnormalflag='" & resultAbnormalFlag & "'" & _
                             ",datecomplete='" & datecomplete & "',referencerange='" & refrange & "'" & _
                             ",machineorhuman='1',iduser='0',idmachine='" & idinstrument & "',unit='" & unit & "',status='" & FINISHSAMPLE & "' " & _
                             ",lowernormal='" & lower_normal & "',uppernormal='" & upper_normal & "',lowercritical='" & lower_critical & "',uppercritical='" & upper_critical & "' " & _
                             "where barcode='" & barcode & "' and universaltest='" & universaltest & "' "
                    'salah
                ElseIf isSampleNoMode Then
                    strsql = "update jobdetail " & _
                             "set measurementvalue='" & value & "',resultstatus='" & resultStatus & "'" & _
                             ",resultabnormalflag='" & resultAbnormalFlag & "'" & _
                             ",datecomplete='" & datecomplete & "',referencerange='" & refrange & "'" & _
                             ",machineorhuman='1',iduser='0',idmachine='" & idinstrument & "',unit='" & unit & "',status='" & FINISHSAMPLE & "' " & _
                             ",lowernormal='" & lower_normal & "',uppernormal='" & upper_normal & "',lowercritical='" & lower_critical & "',uppercritical='" & upper_critical & "' " & _
                             " where sampleno='" & sampleNo & "' and universaltest='" & testname & "'"
                End If

                objconn.buildConn()
                objconn.ExecuteNonQuery(strsql)
                objconn.CloseConn()


            End If

            i = i + 1
        Loop
        UpdateJobStatus()

    End Sub

    Private Function getTestIDforPalio(ByVal paliotestname As String) As String
        Dim qcconn As New clsGreConnect
        Dim qcsql As String

        Dim resultUniversalName As String


        qcsql = "select testtype.analyzertestname,testtype.id,palioinstrument.idtest,palioinstrument.testbarcode from palioinstrument,testtype where palioinstrument.idtest=testtype.id and palioinstrument.testbarcode='" & paliotestname & "'"
        qcconn.buildConn()

        Dim getnameCommand As New SqlClient.SqlCommand(qcsql, qcconn.grecon)
        Dim getnameReader As SqlDataReader = getnameCommand.ExecuteReader(CommandBehavior.CloseConnection)
        If getnameReader.HasRows Then
            Do While getnameReader.Read

                resultUniversalName = getnameReader("analyzertestname")
            Loop
        End If
        getnameReader.Close()
        qcconn.CloseConn()

        Return resultUniversalName

    End Function




    Private Sub UpdateJobStatus() 'penting update status
        Dim strsql As String
        Dim pormat As String
        pormat = "yyyy-MM-dd HH:mm:ss"

        Dim datenow As Date
        datenow = Now

        strsql = "select job.labnumber from job where job.status='" & NEWSAMPLE & "' and job.labnumber not in (select distinct labnumber from jobdetail where status<>'" & FINISHSAMPLE & "' and active='" & ACTIVESAMPLE & "')"

        Dim ogreupdate As New clsGreConnect
        ogreupdate.buildConn()
        Dim cmdupdate As New SqlClient.SqlCommand(strsql, ogreupdate.grecon)
        Dim rdrupdate As SqlDataReader = cmdupdate.ExecuteReader(CommandBehavior.CloseConnection)
        If rdrupdate.HasRows Then
            Do While rdrupdate.Read
                'update job

                Dim squ As String
                squ = "update job set status='" & FINISHSAMPLE & "',datefinish='" & datenow.ToString(pormat) & "' where labnumber='" & rdrupdate("labnumber") & "'"
                Dim ogrejob As New clsGreConnect
                ogrejob.buildConn()
                ogrejob.ExecuteNonQuery(squ)
                ogrejob.CloseConn()
            Loop
        End If
        ogreupdate.CloseConn()
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try

            Dim strTosend As String
            Dim serverStream As NetworkStream = clientSocket.GetStream()

            Dim inStream(10024) As Byte
            strTosend = Chr(5)

            Dim outStream As Byte() = _
            System.Text.Encoding.ASCII.GetBytes(strTosend)
            Label3.Text = "data to send " & strTosend
            
            serverStream.Write(outStream, 0, outStream.Length)
            serverStream.Flush()

            If strTosend = Chr(4) Then
                Label3.Text = "EOT"
                btnDisconnect.PerformClick()
                tmrComm.Stop()
            End If



            Dim abslabel As New Label
            
            Dim myCompleteMessage As StringBuilder = New StringBuilder()
            Dim numberOfBytesRead As Integer = 0

            If serverStream.CanRead Then
                Dim returndata As String


                ' Incoming message may be larger than the buffer size.
                Do While serverStream.DataAvailable
                    If serverStream.DataAvailable Then
                        numberOfBytesRead = serverStream.Read(inStream, 0, inStream.Length)
                        If numberOfBytesRead > 0 Then
                            myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(inStream, 0, numberOfBytesRead))
                        End If
                    End If
                Loop

                If myCompleteMessage.ToString.Length > 0 Then
                    Label3.Text = myCompleteMessage.ToString & "  berhasil"
                Else
                    Label3.Text = "zero lenght"
                End If
            End If






            '##############################

            'lblmsg.Text = returndata
            lblmsg.Text = myCompleteMessage.ToString

            If Trim(lblmsg.Text) = Chr(6) Then
                msg("ACK")
                indexToSend = indexToSend + 1
                Label2.Text = Label2.Text & CStr(indexToSend)
                If indexToSend = queryCreate.Count Then
                    GreenLamp = False
                    '       MessageBox.Show("timer stop")
                    tmrComm.Stop()
                Else
                    GreenLamp = True
                End If

            ElseIf Trim(lblmsg.Text) = Chr(21) Then
                '    msg("NAK")
                GreenLamp = False

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try

    End Sub
End Class