﻿Public Class developerModeFrm

  
   
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim currendate As Integer = CInt(Date.Today.Day.ToString)
            Dim caldate As Integer = (currendate * 2) - 1
            If Trim(TextBox1.Text) = "hidupkan developer mode" Then
                If TextBox2.Text = caldate Then
                    developerMode = 1
                    Label1.Text = "on"
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Ada kesalahan")
        End Try
        
    End Sub

    Private Sub developerModeFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If developerMode = 0 Then
            Label1.Text = "off"
        ElseIf developerMode = 1 Then
            Label1.Text = "on"

        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        developerMode = 0
        Label1.Text = "off"
        TextBox1.Text = ""
        TextBox2.Text = ""
    End Sub
End Class