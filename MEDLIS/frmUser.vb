﻿Imports System.Data
Imports System.Data.SqlClient

Public Class frmUser
    Dim isadd As Boolean
    Dim isedit As Boolean
    Dim seliduser As Integer

    Private Sub frmUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DeleteMnu.Enabled = False
        cancelMnu.Enabled = False
        SaveMnu.Enabled = False
        AddMnu.Enabled = True
        EditMnu.Enabled = False
        isadd = False
        isedit = False

        LoadDgUser()

        For Each ctrl As Control In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.ReadOnly = True 'BackColor = Color.LightYellow
            End If

            If (ctrl.GetType() Is GetType(CheckBox)) Then
                Dim chk As CheckBox = CType(ctrl, CheckBox)
                chk.Enabled = False 'BackColor = Color.LightYellow
            End If

        Next

    End Sub


    Private Sub AddMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddMnu.Click
        isadd = True
        isedit = False

        For Each ctrl As Control In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.Enabled = True
                txt.ReadOnly = False 'BackColor = Color.LightYellow
                txt.BackColor = Color.White
                txt.Text = ""
            End If

            If (ctrl.GetType() Is GetType(CheckBox)) Then
                Dim chk As CheckBox = CType(ctrl, CheckBox)
                chk.Enabled = True 'BackColor = Color.LightYellow
                chk.Checked = False
            End If

        Next

        cancelMnu.Enabled = True
        SaveMnu.Enabled = True
        EditMnu.Enabled = False
        txtnama.Focus()


    End Sub

    Private Sub LoadDgUser()
        Dim strsql As String
        Dim greobject As New clsGreConnect
        Dim dtable As DataTable
        strsql = "select * from medlisuser"
        greobject = New clsGreConnect
        greobject.buildConn()

        dtable = New DataTable
        dtable = greobject.ExecuteQuery(strsql)

        'dguser = New DataGridView
        dguser.DataSource = dtable



        Dim j As Integer
        For j = 0 To dguser.Columns.Count - 1
            dguser.Columns(j).Visible = False
        Next

        dguser.Columns("username").Visible = True
        dguser.Columns("username").HeaderText = "Username"
        dguser.Columns("username").Width = 120

        dguser.Columns("name").Visible = True
        dguser.Columns("name").HeaderText = "Nama "
        dguser.Columns("name").Width = 200

        dguser.Columns("phone").Visible = True
        dguser.Columns("phone").HeaderText = "Phone"
        dguser.Columns("phone").Width = 100


        Dim buttonColumn As New DataGridViewButtonColumn()
        With buttonColumn
            .Name = "Pilih"
            .HeaderText = "Pilih"
            .Width = 90
            .Text = "Pilih"
            .UseColumnTextForButtonValue = True
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            .FlatStyle = FlatStyle.Standard
            .CellTemplate.Style.BackColor = Color.Yellow
            .DisplayIndex = 0
        End With
        dguser.Columns.Add(buttonColumn)
        dguser.AllowUserToAddRows = False
        dguser.RowHeadersVisible = False

    End Sub


    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click
        CekLicense()
        Dim strsql As String
        Dim active As Integer
        Dim setting As Integer
        Dim result As Integer
        Dim register As Integer

        If chkactive.Checked Then
            active = 1
        Else
            active = 0
        End If

        If chksetting.Checked Then
            setting = 1
        Else
            setting = 0
        End If

        If chkresult.Checked Then
            result = 1
        Else
            result = 0
        End If

        Dim instrument As String
        Dim pembayaran As String

        If chkInstrument.Checked Then
            instrument = 1
        Else
            instrument = 0
        End If

        If chkkasir.Checked Then
            pembayaran = 1
        Else
            pembayaran = 0
        End If


        If chkregister.Checked Then
            register = 1
        Else
            register = 0
        End If

        If txtpass.Text <> txtpass2.Text Then
            MessageBox.Show("Password yang Anda masukkan tidak sama")
            Return
        End If

        Dim ogre As New clsGreConnect
        ogre.buildConn()
        '###############

    
        Dim userPass As String
        Dim userName As String
        Dim encrypKey As String
        Dim passAfterEnc As String
        Dim name As String

        userName = txtuser.Text
        userPass = txtpass.Text
        name = Trim(txtnama.Text)
        encrypKey = "Password"

        Dim myWriter As New iniReader(userPass, encrypKey)
        passAfterEnc = myWriter.CreateEncryptedPassword()
      
        myWriter = Nothing

        '###############
        If isedit = True And isadd = False Then
            strsql = "update medlisuser set name='" & name & "',password='" & passAfterEnc & "',phone='" & Trim(txttelp.Text) & "',active='" & active & "',setting='" & setting & "',register='" & register & "',result='" & result & "',pembayaran='" & pembayaran & "',instrument='" & instrument & "' where id='" & seliduser & "'"
            ogre.ExecuteNonQuery(strsql)
        ElseIf isadd = True And isedit = False Then
            strsql = "insert into medlisuser(username,password,name,phone,active,setting,register,result,pembayaran,instrument)values('" & userName & "','" & passAfterEnc & "','" & txtnama.Text & "','" & txttelp.Text & "','" & active & "','" & setting & "','" & register & "','" & result & "','" & pembayaran & "','" & instrument & "')"
            ogre.ExecuteNonQuery(strsql)
        End If

        ogre.CloseConn()


        For Each ctrl As Control In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then 'bagus
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.Text = "" 'BackColor = Color.LightYellow
                txt.Enabled = False
            End If

            If (ctrl.GetType() Is GetType(CheckBox)) Then
                Dim chk As CheckBox = CType(ctrl, CheckBox)
                chk.Checked = False 'BackColor = Color.LightYellow
                chk.Enabled = False
            End If

        Next

        'BAGUS BAGUS
        SaveMnu.Enabled = False
        DeleteMnu.Enabled = False
        EditMnu.Enabled = False
        ClearDg()
        LoadDgUser()


    End Sub
    Private Sub ClearDg()
        dguser.DataSource = Nothing
        If dguser.Columns.Contains("Pilih") Then
            dguser.Columns.Clear()
        End If
        If dguser.Rows.Count > 0 Then
            dguser.Rows.Clear()
        End If
    End Sub
    Private Sub dguser_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dguser.CellContentClick
        If e.RowIndex < 0 OrElse Not e.ColumnIndex = dguser.Columns("Pilih").Index Then
            Return

        Else

            If Not dguser.CurrentRow.IsNewRow Then

                Dim delo As New clsGreConnect

                delo.buildConn()
                seliduser = dguser.Item("id", dguser.CurrentRow.Index).Value
                'Dim delsql As String

                'delsql = "delete from jobdetail where sampleno=" & _
                '            "'" & selectedsampleNo & "' and labnumber='" & currentlabnumber & "'"

                'delo.ExecuteNonQuery(savestrsql)
                EditMnu.Enabled = True
                ShowData(seliduser)
                DeleteMnu.Enabled = True
                'delo.CloseConn()
            End If

        End If
    End Sub

    Private Sub ShowData(ByVal selid As Integer)
        Dim ogrecon As New clsGreConnect
        ogrecon.buildConn()
        Dim strsql As String
        strsql = "select * from medlisuser where id='" & selid & "'"

        Dim cmd As New SqlClient.SqlCommand(strsql, ogrecon.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        For Each ctrl As Control In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.ReadOnly = False 'BackColor = Color.LightYellow
            End If

            If (ctrl.GetType() Is GetType(CheckBox)) Then
                Dim chk As CheckBox = CType(ctrl, CheckBox)
                chk.Enabled = True 'BackColor = Color.LightYellow
            End If

        Next
        Do While rdr.Read
            txtuser.Text = rdr("username")
            '############
            Dim userPass As String
            Dim encrypKey As String
            Dim passAfterDec As String

            userPass = rdr("password")
            encrypKey = "Password"

            Dim myWriter As New iniReader(userPass, encrypKey)
            passAfterDec = myWriter.Decrypt(userPass, encrypKey)

            myWriter = Nothing


            txtuser.Text = rdr("username")
            txtnama.Text = rdr("name")
            txttelp.Text = rdr("phone")
            txtpass.Text = passAfterDec
            txtpass2.Text = passAfterDec

            If rdr("active") = 1 Then
                chkactive.Checked = True
            Else
                chkactive.Checked = False
            End If

            If rdr("result") = 1 Then
                chkresult.Checked = True
            Else
                chkresult.Checked = False
            End If

            If rdr("setting") = 1 Then
                chksetting.Checked = True
            Else
                chksetting.Checked = False
            End If


            If rdr("register") = 1 Then
                chkregister.Checked = True
            Else
                chkregister.Checked = False
            End If

            If Not IsDBNull(rdr("instrument")) Then
                If rdr("instrument") = 1 Then
                    chkInstrument.Checked = True
                Else
                    chkInstrument.Checked = False
                End If
            End If

            If Not IsDBNull(rdr("pembayaran")) Then
                If rdr("pembayaran") = 1 Then
                    chkkasir.Checked = True
                Else
                    chkkasir.Checked = False
                End If
            End If


            For Each ctrl As Control In Panel1.Controls
                If (ctrl.GetType() Is GetType(TextBox)) Then
                    Dim txt As TextBox = CType(ctrl, TextBox)
                    txt.ReadOnly = True 'BackColor = Color.LightYellow
                End If

                If (ctrl.GetType() Is GetType(CheckBox)) Then
                    Dim chk As CheckBox = CType(ctrl, CheckBox)
                    chk.Enabled = False 'BackColor = Color.LightYellow
                End If

            Next

        Loop

    End Sub


    Private Sub EditMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditMnu.Click
        isedit = True
        isadd = False



        For Each ctrl As Control In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.Enabled = True
                txt.ReadOnly = False 'BackColor = Color.LightYellow
            End If

            If (ctrl.GetType() Is GetType(CheckBox)) Then
                Dim chk As CheckBox = CType(ctrl, CheckBox)
                chk.Enabled = True 'BackColor = Color.LightYellow
            End If

        Next
        AddMnu.Enabled = False
        cancelMnu.Enabled = True
        SaveMnu.Enabled = True
    End Sub

    Private Sub cancelMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelMnu.Click
        For Each ctrl As Control In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.Text = ""
                txt.ReadOnly = True 'BackColor = Color.LightYellow
                txt.Enabled = False
            End If

            If (ctrl.GetType() Is GetType(CheckBox)) Then
                Dim chk As CheckBox = CType(ctrl, CheckBox)
                chk.Checked = False
                chk.Enabled = False 'BackColor = Color.LightYellow
            End If

        Next

        AddMnu.Enabled = True
        SaveMnu.Enabled = False
        EditMnu.Enabled = False
        DeleteMnu.Enabled = False
        isadd = False
        isedit = False

    End Sub

    Private Sub DeleteMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteMnu.Click
        Dim ogre As New clsGreConnect
        Dim strsql As String
        ogre.buildConn()
        strsql = "delete from medlisuser where id='" & seliduser & "'"
        ogre.ExecuteNonQuery(strsql)
        ogre.CloseConn()

        DeleteMnu.Enabled = False
        cancelMnu.Enabled = False
        EditMnu.Enabled = False
        SaveMnu.Enabled = False

        For Each ctrl As Control In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.Text = ""
                txt.ReadOnly = True 'BackColor = Color.LightYellow
            End If

            If (ctrl.GetType() Is GetType(CheckBox)) Then
                Dim chk As CheckBox = CType(ctrl, CheckBox)
                chk.Checked = False
                chk.Enabled = False 'BackColor = Color.LightYellow
            End If

        Next

        ClearDg()
        LoadDgUser()
    End Sub

    
    Private Sub txtnama_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtnama.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)

        End Select
    End Sub

    Private Sub txtpass_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtpass.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)

        End Select
    End Sub

    Private Sub txtuser_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtuser.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)

        End Select
    End Sub
    ReadOnly AllowedKeys As String = _
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 "
    Private Sub txttelp_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txttelp.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)

        End Select
    End Sub

    Private Sub txttelp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txttelp.TextChanged

    End Sub
End Class