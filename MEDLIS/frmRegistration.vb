﻿'
' Created by SharpDevelop.
' User: User
' Date: 10/13/2014
' Time: 1:06 PM
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Imports System.Data
Imports System.Data.SqlClient
Partial Public Class frmRegistration
    Dim tipepasient As String
    Dim colprice As String
    Dim tipepasientable As DataTable


    Dim idDbpatient As Integer
    Dim patientcode As String
    Dim strageforprint As String
    Dim gender As String
    Dim strage As String
    Dim strsql As String
    Dim greobject As clsGreConnect
    Private dtable As DataTable
    Dim selectedgroupid As Integer
    Dim choosedTestItemId As List(Of Integer)
    Dim isSave As Boolean
    Dim pricetotal_tosave As Double
    Dim notyetAdd As Boolean

    Dim idpackfixedprice As Integer

    Dim seldatebirth As Date
    Dim currentlabnumber As String


    Public Sub New()
        ' The Me.InitializeComponent call is required for Windows Forms designer support.
        Me.InitializeComponent()

        '
        ' TODO : Add constructor code after InitializeComponents
        ''
    End Sub
    Public Sub New(ByVal idpatient As Integer, ByVal strpatientcode As String)
        Me.InitializeComponent()
        idDbpatient = idpatient
        patientcode = strpatientcode
    End Sub



    Private Sub frmRegistration_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If isSave = False And currentlabnumber <> "" Then
            Dim odel As clsGreConnect
            Dim delsql As String

            odel = New clsGreConnect
            odel.buildConn()
            delsql = "delete from jobdetail where labnumber=" & _
                        "'" & currentlabnumber & "'"
            odel.ExecuteNonQuery(delsql)
            odel.CloseConn()

            odel = New clsGreConnect
            odel.buildConn()
            delsql = "delete from job where labnumber=" & _
                        "'" & currentlabnumber & "'"
            odel.ExecuteNonQuery(delsql)
            odel.CloseConn()
        End If
    End Sub
    Private Sub LoadTestPack()
        Dim sql As String = Nothing

        sql = "select distinct idpack,packname from testpack"

        Dim cncombo As New clsGreConnect
        Dim cmdcombo As SqlClient.SqlCommand
        Dim dacombo As New SqlClient.SqlDataAdapter 
        Dim dscombo As New DataSet

        cncombo.buildConn()

        cmdcombo = New SqlClient.SqlCommand(sql, cncombo.grecon)
        dacombo.SelectCommand = cmdcombo
        dacombo.Fill(dscombo)
        dacombo.Dispose()
        cmdcombo.Dispose()
        cncombo.CloseConn()
        cmbpack.DataSource = Nothing

        cmbpack.ValueMember = "idpack"
        cmbpack.DisplayMember = "packname"
        cmbpack.DataSource = dscombo.Tables(0)
    End Sub

    Private Sub frmRegistration_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        isRegWinOpen = 0
    End Sub

    Private Sub frmRegistration_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
        isRegWinOpen = 1
        isSave = False
        notyetAdd = True
        cmdaddprofile.Visible = False

        SaveMnu.Enabled = False

        'System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("id-ID")
        'System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo("id-ID") 'bagus penting
        tipepasient = ""
        FirstLoad()
        LoadTipePasien()


        rdogroup.Checked = False
        rdopack.Checked = True

        Me.Left = 0
        Me.Top = 0


        If idDbpatient <> 0 Then
            'LoadPatientData()
            greobject = New clsGreConnect
            Dim strsql As String


            Try
                strsql = "select * from patient where id='" & idDbpatient & "'"
                greobject = New clsGreConnect
                greobject.buildConn()
                Dim cmd As New SqlClient.SqlCommand(strsql, greobject.grecon)
                Dim Reader As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                ' Reader = New 'CommandBehavior.CloseConnection))

                Do While Reader.Read
                    txtNama.Text = Reader("patientname")
                    txtNama.ReadOnly = True

                    txtIdPasien.Text = Reader("idpatient")
                    txtIdPasien.ReadOnly = True

                    txtAlamat.Text = Reader("address")
                    txtAlamat.ReadOnly = True

                    txtPhone.Text = Reader("phone")
                    txtPhone.ReadOnly = True
                    If Not IsDBNull(Reader("pricelevel")) Then
                        If Reader("pricelevel") <> "" Then
                            tipepasient = Reader("pricelevel")
                            cbtipepasien.SelectedValue = tipepasient
                            colprice = tipepasient
                        Else
                            cbtipepasien.SelectedValue = 0
                        End If
                        
                    Else
                        cbtipepasien.SelectedValue = 0
                    End If

                    dtTglLahir.Value = Reader("birthdate")

                    If Reader("malefemale") = genderMale Then
                        rdoLaki.Checked = True
                        gender = genderMale
                    ElseIf Reader("malefemale") = genderFemale Then
                        rdoPerempuan.Checked = True
                        gender = genderFemale
                    ElseIf Reader("malefemale") = genderChild Then
                        gender = genderChild
                    End If
                Loop
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
            greobject.CloseConn()
            greobject = Nothing

            LoadChoiceCombo()

        End If

        'If tipepasient <> "" Then
        '    GetPriceToShowBasedOnType(tipepasient)
        'End If

        Try


            txtlabnumber.Select()
            txtlabnumber.Focus()
            '-set txt labnumber

            Dim strf As String
            If labnumbering = LAB_MANUAL_NUMBERING Then
                strf = "select top 1 labnumber as ln from job order by labnumber desc"
            ElseIf labnumbering = LAB_AUTO_NUMBERING Then
                strf = "select cast(labnumber as int) as ln from job order by ln desc"
            End If


            Dim obf As New clsGreConnect
            obf.buildConn()

            Dim prevLab As String
            Dim cmdf As New SqlClient.SqlCommand(strf, obf.grecon)
            Dim rdrf As SqlDataReader = cmdf.ExecuteReader(CommandBehavior.CloseConnection)
            If rdrf.HasRows Then
                Do While rdrf.Read
                    prevLab = Trim(rdrf("ln"))
                    If labnumbering = LAB_MANUAL_NUMBERING Then
                        txtlabnumber.Text = prevLab
                        txtlabnumber.SelectionStart = txtlabnumber.Text.Length - 1
                        txtlabnumber.SelectionLength = 1

                    ElseIf labnumbering = LAB_AUTO_NUMBERING Then
                        If System.Text.RegularExpressions.Regex.IsMatch(prevLab, "^[0-9 ]+$") Then
                            txtlabnumber.Text = CStr(CInt(prevLab) + 1)
                            txtlabnumber.SelectionStart = txtlabnumber.Text.Length - 1
                            txtlabnumber.SelectionLength = 1
                        Else
                            txtlabnumber.Text = prevLab
                            txtlabnumber.SelectionStart = txtlabnumber.Text.Length - 1
                            txtlabnumber.SelectionLength = 1
                        End If
                    End If
                    txtlabnumber.SelectionStart = txtlabnumber.Text.Length - 1
                    txtlabnumber.SelectionLength = 1
                Loop
            Else
                txtlabnumber.Focus()
            End If
            cmdf.Dispose()
            rdrf.Close()
            obf = Nothing

        Catch ex As Exception
            'MessageBox.Show("Saat ini Anda menggunakan system AutoLabNumber. AutoLabNumber tidak bisa digunakan karena sebelumnya sudah menggunakan system ManualLabNumber. Solusi: Hapus Job yang menggunakan yang dahulu menggunakan ManualLabNumber/ lab number huruf untuk menggunakan Autolabnumber, atau solusi kedua gunakan system ManualLabNumber. Restart Aplikasi untuk perubahan")
            MessageBox.Show(ex.ToString)

        End Try
        rdR.Checked = True

        LoadTestPack()
        'remove row header bagus



    End Sub
    Private Sub GetPriceToShowBasedOnType(ByVal tipeInt As String)
        greobject = New clsGreConnect
        Dim stras As String
        Dim colpricename As String
        greobject.buildConn()
        stras = "select * from patientpricelevel where defaultname='" & tipeInt & "'"
        Dim cmdf As New SqlClient.SqlCommand(stras, greobject.grecon)
        Dim rdr As SqlDataReader = cmdf.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read

                colpricename = rdr("defaultname")

            Loop
        End If
        colprice = Trim(colpricename)
    End Sub

    Private Sub LoadTipePasien()
        Dim strtipe As String
        strtipe = "select * from patientpricelevel"
        Dim objconn As New clsGreConnect
        objconn.buildConn()

        ' Dim thecommand As New SqlClient.SqlCommand(strsql, objconn.grecon)
        ' Dim Reader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)
        tipepasientable = objconn.ExecuteQuery(strtipe)
        cbtipepasien.DataSource = tipepasientable
        cbtipepasien.DisplayMember = "display"
        cbtipepasien.ValueMember = "defaultname"

        'Do While Reader.Read
        '    cbtipepasien.Items.Add(Reader("display"))
        'Loop
        cbtipepasien.Enabled = False
    End Sub
    Private Function ClearApostrofe(ByVal str As String) As String
        Dim result As String
        result = str.Replace("'", " ")
        Return result

    End Function

    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click
        'insert into 
        '----------------------------job detail
        'id serial NOT NULL,
        'labnumber text,
        'idtesttype integer,
        'idtestgroup integer,
        'price money,
        'barcode character(22),
        'sampleno character(6),
        'specimendesc character(1),
        'resultstatus character(1),
        'resultabnormalflag character(1),
        'measurementvalue character(12),
        'universaltest character(10),
        'datecomplete date,
        'priority character(1),
        'dateorder date,
        'reporttypes character(1),
        'referencerange character(30),
        'CONSTRAINT pk_jobdetail_id PRIMARY KEY (id)
        '----------------------------job
        'labnumber text,
        'idpasien integer,
        'datereceived date,
        'currentlabnumber = Trim(txtlabnumber.Text)

        'If IsSampleAvailabe() > 0 Then
        '    MessageBox.Show("Silahkan masukkan data sample")
        '    Return
        'End If

        'Dim datereceived, dateorder As String
        'datereceived = Format(Now, "yyyyMMddhhmmss")
        'dateorder = Format(Now, "yyyyMMddhhmmss")

        Dim pormat As String
        pormat = "yyyy-MM-dd HH:mm:ss"

        Dim datenow As Date
        datenow = Now()

        'totalprice text,
        Dim totalprice As String
        totalprice = CStr(pricetotal_tosave) 'txtTotal.Text

        'age text,
        Dim age As Integer
        age = CountPatientAge(idDbpatient)

        ' userid = "001"


        'bloodsamplingdate1 text,
        'bloodsamplingdate2 text,
        'urinesamplingdate text,
        'fecessamplingdate text,
        'docname text,

        Dim docname As String
        Dim docaddress As String
        docname = ClearApostrofe(txtDocter.Text)
        docaddress = ClearApostrofe(txtdocteraddress.Text)

        'docaddress text,
        'userid integer,
        'id serial NOT NULL,
        'barcode character(22)[],
        '----------------------------

        Dim labnumber As String
        labnumber = ClearApostrofe(txtlabnumber.Text)




        Dim price As String
        Dim specimendesc As String
        Dim universaltest As String
        Dim priority As String
        Dim active As String
        Dim strupdate, strinsert As String
        Dim idpasien As String

        Dim objupdte As New clsGreConnect
        objupdte.buildConn()

        Dim objsave As New clsGreConnect
        objsave.buildConn()


        strupdate = "update jobdetail set active='" & ACTIVESAMPLE & _
                    "',status='" & NEWSAMPLE & "' where labnumber='" & labnumber & "'"
        objupdte.ExecuteNonQuery(strupdate)


        strinsert = "insert into job (labnumber,idpasien,datereceived,totalprice,age,docname,docaddress,userid,status,[print],paymentstatus,printage)" &
                    " values (" &
                    "'" & currentlabnumber & "','" & idDbpatient & "','" & datenow.ToString(pormat) & "','" & totalprice & "'" &
                    ",'" & age & "','" & docname & "','" & docaddress & "','" & userid & "','" & NEWSAMPLE & "','" & PRINTNOT & "','" & NOTPAID & "','" & strageforprint & "')"
        objsave.ExecuteNonQuery(strinsert)


        objupdte.CloseConn()

        'SaveMnu.BackColor = Color.LightCoral
        isSave = True
        objsave.CloseConn()
        objsave = Nothing

        Me.Close()

    End Sub
    Private Function CountPatientAge(ByVal idDbpatient As Integer) As Integer
        Dim result As Integer
        Dim strsql As String

        Dim agecon As New clsGreConnect
        Dim bdate As Date



        agecon.buildConn()
        strsql = "select * from patient where id='" & idDbpatient & "'"


        Dim agecmd As New SqlClient.SqlCommand(strsql, agecon.grecon)
        Dim ageReader As SqlDataReader = agecmd.ExecuteReader(CommandBehavior.CloseConnection)

        Do While ageReader.Read
            bdate = ageReader("birthdate")
        Loop

        ageReader.Close()
        agecmd.Dispose()
        agecon.CloseConn()
        agecon = Nothing
        Dim age As Integer

        age = Today.Year - bdate.Year  'hitung umur()
        If (bdate > Today.AddYears(-age)) Then age -= 1
        result = age
        Return result

    End Function
    Private Function GreCountPatientAge(ByVal idDbpatient As Integer) As String
        Dim result As String
        Dim strsql As String

        Dim agecon As New clsGreConnect
        Dim bdate As Date

        Dim pormat2 As String
        'pormat2 = "MM/dd/yyyy"
        pormat2 = "yyyy-MM-dd"

        agecon.buildConn()
        strsql = "select * from patient where id='" & idDbpatient & "'"


        Dim agecmd As New SqlClient.SqlCommand(strsql, agecon.grecon)
        Dim ageReader As SqlDataReader = agecmd.ExecuteReader(CommandBehavior.CloseConnection)

        Do While ageReader.Read
            bdate = ageReader("birthdate")
        Loop

        ageReader.Close()
        agecmd.Dispose()
        agecon.CloseConn()
        agecon = Nothing

        result = Age(bdate, Today)

        Return result

    End Function



    Private Function GetCurrentAge(ByVal dob As Date) As Integer
        Dim age As Integer
        age = Today.Year - dob.Year
        If (dob > Today.AddYears(-age)) Then age -= 1
        Return age
    End Function

    Private Function IsSampleAvailabe() As Integer
        Dim result As Integer
        Dim str As String
        str = "select count(*) as num from jobdetail where labnumber='" & currentlabnumber & "'"

        Dim ocon As New clsGreConnect
        ocon.buildConn()

        Dim numcmd As New SqlClient.SqlCommand(str, ocon.grecon)
        Dim numreader As SqlDataReader = numcmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While numreader.Read
            'If numreader("num") > 0 Then
            '    result = 
            'Else
            '    result = False
            'End If
            result = numreader("num")
        Loop

        Return result

        ocon.CloseConn()
        ocon = Nothing

    End Function

    Private Sub LoadChoiceCombo()
        Dim strsql As String
        Dim greObject As New clsGreConnect

        strsql = "select * from testgroup"

        greObject.buildConn()

        Dim thecommand As New SqlClient.SqlCommand(strsql, greObject.grecon)
        Dim theReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)

        Do While theReader.Read
            cbTestGroup.Items.Add(theReader("testgroupname"))
            'cbTestGroup.Items.Add(testTypeReader("testgroupname"))
        Loop

        theReader.Close()
        thecommand.Dispose()
        greObject.CloseConn()
        greObject = Nothing


    End Sub
    Private Sub LoadPatientData()

    End Sub

    Private Sub cbTestGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbTestGroup.SelectedIndexChanged
        If rdogroup.Checked = True Then
            Dim objTestRelated As New TestRelated
            selectedgroupid = objTestRelated.GetIdTestGroup(cbTestGroup.Text)
            PopulateDg()
        End If
        
    End Sub
    Private Sub PopulateDg()
        greobject = New clsGreConnect
        greobject.buildConn()

        dtable = New DataTable
        dtable = greobject.ExecuteQuery("select * from testtype where idtestgroup='" & selectedgroupid & "' order by testname asc")

        'dgvtestname = New DataGridView
        dgvtestname.AllowUserToAddRows = False
        dgvtestname.RowHeadersVisible = False
        dgvtestname.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvtestname.Columns.Count - 1
            dgvtestname.Columns(j).Visible = False
        Next

        If Not dgvtestname.Columns.Contains("chk") Then
            Dim chk As New DataGridViewCheckBoxColumn()

            dgvtestname.Columns.Add(chk)
            chk.HeaderText = "PILIH"
            chk.Width = 65
            chk.Name = "chk"

            dgvtestname.Columns("chk").DisplayIndex = 0
            dgvtestname.Columns("chk").Name = "chk"
        End If

        dgvtestname.Columns("chk").Visible = True
        dgvtestname.Columns("testname").Visible = True
        dgvtestname.Columns("testname").HeaderText = "Test Name"
        dgvtestname.Columns("testname").Width = 180
        dgvtestname.Columns("testname").ReadOnly = True

        dgvtestname.Columns("analyzertestname").Visible = True
        dgvtestname.Columns("analyzertestname").HeaderText = "Analyzer Test Name"
        dgvtestname.Columns("analyzertestname").Width = 80
        dgvtestname.Columns("analyzertestname").ReadOnly = True
        If colprice = "price-level1" Then
            dgvtestname.Columns("price").Visible = True
            dgvtestname.Columns("price").HeaderText = "Harga"
            dgvtestname.Columns("price").Width = 120
            dgvtestname.Columns("price").ReadOnly = True
            dgvtestname.Columns("price").DefaultCellStyle.Format = "n0"
            dgvtestname.Columns("price").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        ElseIf colprice = "price-level2" Then
            dgvtestname.Columns("price2").Visible = True
            dgvtestname.Columns("price2").HeaderText = "Harga"
            dgvtestname.Columns("price2").Width = 120
            dgvtestname.Columns("price2").ReadOnly = True
            dgvtestname.Columns("price2").DefaultCellStyle.Format = "n0"
            dgvtestname.Columns("price2").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        ElseIf colprice = "price-level3" Then
            dgvtestname.Columns("price3").Visible = True
            dgvtestname.Columns("price3").HeaderText = "Harga"
            dgvtestname.Columns("price3").Width = 120
            dgvtestname.Columns("price3").ReadOnly = True
            dgvtestname.Columns("price3").DefaultCellStyle.Format = "n0"
            dgvtestname.Columns("price3").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        ElseIf colprice = "price-level4" Then
            dgvtestname.Columns("price4").Visible = True
            dgvtestname.Columns("price4").HeaderText = "Harga"
            dgvtestname.Columns("price4").Width = 120
            dgvtestname.Columns("price4").ReadOnly = True
            dgvtestname.Columns("price4").DefaultCellStyle.Format = "n0"
            dgvtestname.Columns("price4").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        ElseIf colprice = "price-level5" Then
            dgvtestname.Columns("price5").Visible = True
            dgvtestname.Columns("price5").HeaderText = "Harga"
            dgvtestname.Columns("price5").Width = 120
            dgvtestname.Columns("price5").ReadOnly = True
            dgvtestname.Columns("price5").DefaultCellStyle.Format = "n0"
            dgvtestname.Columns("price5").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        ElseIf colprice = "price-level6" Then
            dgvtestname.Columns("price6").Visible = True
            dgvtestname.Columns("price6").HeaderText = "Harga"
            dgvtestname.Columns("price6").Width = 120
            dgvtestname.Columns("price6").ReadOnly = True
            dgvtestname.Columns("price6").DefaultCellStyle.Format = "n0"
            dgvtestname.Columns("price6").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        End If     'dgvtestname.Columns("price").DefaultCellStyle.Format = "c" 'format c datagrid

        'dgvtestname.Columns("id").Visible = False
        'DGPack.Columns("packname").Width = 250




        greobject.CloseConn()
        greobject = Nothing

    End Sub
    Private Sub PopulateDgBasedOnPack(ByVal selectedpackid As Integer)
        greobject = New clsGreConnect
        greobject.buildConn()

        dtable = New DataTable
        dtable = greobject.ExecuteQuery("select tt.testname,tt.analyzertestname,tt.price,tt.price2,tt.price3,tt.price4,tt.price5,tt.price6,tt.id from testtype tt,testpack tp where tt.id=tp.idtest and tp.idpack='" & selectedpackid & "'")

        'dgvtestname = New DataGridView
        dgvtestname.AllowUserToAddRows = False
        dgvtestname.RowHeadersVisible = False
        dgvtestname.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvtestname.Columns.Count - 1
            dgvtestname.Columns(j).Visible = False
        Next

        If Not dgvtestname.Columns.Contains("chk") Then
            Dim chk As New DataGridViewCheckBoxColumn()
            dgvtestname.Columns.Add(chk)
            chk.HeaderText = "PILIH"
            chk.Width = 65
            chk.Name = "chk"
            dgvtestname.Columns("chk").DisplayIndex = 0
            dgvtestname.Columns("chk").Name = "chk"
        End If

        dgvtestname.Columns("chk").Visible = True
        dgvtestname.Columns("testname").Visible = True
        dgvtestname.Columns("testname").HeaderText = "Test Name"
        dgvtestname.Columns("testname").Width = 180
        dgvtestname.Columns("testname").ReadOnly = True

        dgvtestname.Columns("analyzertestname").Visible = True
        dgvtestname.Columns("analyzertestname").HeaderText = "Analyzer Test Name"
        dgvtestname.Columns("analyzertestname").Width = 80
        dgvtestname.Columns("analyzertestname").ReadOnly = True

        If colprice = "price-level1" Then
            dgvtestname.Columns("price").Visible = True
            dgvtestname.Columns("price").HeaderText = "Harga"
            dgvtestname.Columns("price").Width = 120
            dgvtestname.Columns("price").DefaultCellStyle.Format = "n0"
            dgvtestname.Columns("price").ReadOnly = True
            dgvtestname.Columns("price").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        ElseIf colprice = "price-level2" Then
            dgvtestname.Columns("price2").Visible = True
            dgvtestname.Columns("price2").HeaderText = "Harga"
            dgvtestname.Columns("price2").DefaultCellStyle.Format = "n0"
            dgvtestname.Columns("price2").Width = 120
            dgvtestname.Columns("price2").ReadOnly = True
            dgvtestname.Columns("price2").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        ElseIf colprice = "price-level3" Then
            dgvtestname.Columns("price3").Visible = True
            dgvtestname.Columns("price3").HeaderText = "Harga"
            dgvtestname.Columns("price3").Width = 120
            dgvtestname.Columns("price3").DefaultCellStyle.Format = "n0"
            dgvtestname.Columns("price3").ReadOnly = True
            dgvtestname.Columns("price3").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        ElseIf colprice = "price-level4" Then
            dgvtestname.Columns("price4").Visible = True
            dgvtestname.Columns("price4").HeaderText = "Harga"
            dgvtestname.Columns("price4").Width = 120
            dgvtestname.Columns("price4").DefaultCellStyle.Format = "n0"
            dgvtestname.Columns("price4").ReadOnly = True
            dgvtestname.Columns("price4").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        ElseIf colprice = "price-level5" Then
            dgvtestname.Columns("price5").Visible = True
            dgvtestname.Columns("price5").HeaderText = "Harga"
            dgvtestname.Columns("price5").DefaultCellStyle.Format = "n0"
            dgvtestname.Columns("price5").Width = 120
            dgvtestname.Columns("price5").ReadOnly = True
            dgvtestname.Columns("price5").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        ElseIf colprice = "price-level6" Then
            dgvtestname.Columns("price6").Visible = True
            dgvtestname.Columns("price6").HeaderText = "Harga"
            dgvtestname.Columns("price6").Width = 120
            dgvtestname.Columns("price6").DefaultCellStyle.Format = "n0"
            dgvtestname.Columns("price6").ReadOnly = True
            dgvtestname.Columns("price6").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End If     'dgvtestname.Columns("price").DefaultCellStyle.Format = "c" 'format c datagrid

        'dgvtestname.Columns("id").Visible = False
        'DGPack.Columns("packname").Width = 250




        greobject.CloseConn()
        greobject = Nothing

    End Sub
    Private Sub dgvtestname_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvtestname.CellContentClick
        If Not (dgvtestname.CurrentRow.IsNewRow) Then
            If Not IsDBNull(dgvtestname.Item("id", e.RowIndex).Value) Then
                If TypeOf dgvtestname.Rows(e.RowIndex).Cells(e.ColumnIndex) Is DataGridViewCheckBoxCell Then
                    Dim dgvCheckBoxCell As DataGridViewCheckBoxCell = dgvtestname.Rows(e.RowIndex).Cells(e.ColumnIndex)
                    'Commit the data to the datasouce.
                    dgvtestname.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    Dim checked As Boolean = CType(dgvCheckBoxCell.Value, Boolean) 'bagus
                    If checked Then
                        Dim selid As Integer
                        selid = dgvtestname.Item("id", e.RowIndex).Value
                        If IsItAlreadyThere(selid) = False Then
                            GenerateSelectedTest(selid)
                        End If
                    End If
                    'choosedTestItemId.Add(dgvtestname.Item("id", e.RowIndex).Value)
                    'bagus
                    'MessageBox.Show(e.RowIndex & " " & checked & " " & e.ColumnIndex)

                    'If checked Then
                    '    Dim i As Integer
                    '    For i = 0 To dgvtestname.Rows.Count - 1
                    '        If i <> e.RowIndex Then
                    '            dgvtestname.Item("chk", i).Value = False
                    '        End If
                    '    Next

                    'End If

                End If
            End If
        End If
    End Sub

    Private Sub GenerateSelectedTest(ByVal idtest As Integer)
        strsql = "select * from testtype where id='" & idtest & "'"
        Dim rowdata As String()
        greobject = New clsGreConnect
        greobject.buildConn()

        Dim thecommand As New SqlClient.SqlCommand(strsql, greobject.grecon)
        Dim theReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)

        Do While theReader.Read
            'If colprice = "price-level1" Then
            '    rowdata = New String() {theReader("id"), theReader("testname"), theReader("analyzertestname"), FormatCurrency(theReader("price")), theReader("price"), True, 0}
            'ElseIf colprice = "price-level2" Then
            '    rowdata = New String() {theReader("id"), theReader("testname"), theReader("analyzertestname"), FormatCurrency(theReader("price2")), theReader("price2"), True, 0}
            'ElseIf colprice = "price-level3" Then
            '    rowdata = New String() {theReader("id"), theReader("testname"), theReader("analyzertestname"), FormatCurrency(theReader("price3")), theReader("price3"), True, 0}
            'ElseIf colprice = "price-level4" Then
            '    rowdata = New String() {theReader("id"), theReader("testname"), theReader("analyzertestname"), FormatCurrency(theReader("price4")), theReader("price4"), True, 0}
            'ElseIf colprice = "price-level5" Then
            '    rowdata = New String() {theReader("id"), theReader("testname"), theReader("analyzertestname"), FormatCurrency(theReader("price5")), theReader("price5"), True, 0}
            'ElseIf colprice = "price-level6" Then
            '    rowdata = New String() {theReader("id"), theReader("testname"), theReader("analyzertestname"), FormatCurrency(theReader("price6")), theReader("price6"), True, 0}
            'End If

            If colprice = "price-level1" Then
                rowdata = New String() {theReader("id"), theReader("testname"), theReader("analyzertestname"), theReader("price"), theReader("price"), True, 0}
            ElseIf colprice = "price-level2" Then
                rowdata = New String() {theReader("id"), theReader("testname"), theReader("analyzertestname"), theReader("price2"), theReader("price2"), True, 0}
            ElseIf colprice = "price-level3" Then
                rowdata = New String() {theReader("id"), theReader("testname"), theReader("analyzertestname"), theReader("price3"), theReader("price3"), True, 0}
            ElseIf colprice = "price-level4" Then
                rowdata = New String() {theReader("id"), theReader("testname"), theReader("analyzertestname"), theReader("price4"), theReader("price4"), True, 0}
            ElseIf colprice = "price-level5" Then
                rowdata = New String() {theReader("id"), theReader("testname"), theReader("analyzertestname"), theReader("price5"), theReader("price5"), True, 0}
            ElseIf colprice = "price-level6" Then
                rowdata = New String() {theReader("id"), theReader("testname"), theReader("analyzertestname"), theReader("price6"), theReader("price6"), True, 0}
            End If



        Loop

        theReader.Close()
        thecommand.Dispose()
        greobject.CloseConn()

        'dgvSelTestName.ColumnCount = 5

        'Dim chk As New DataGridViewCheckBoxColumn
        'chk.HeaderText = "Pilih"
        'chk.Name = "chk"
        '''chk.DisplayIndex = 5

        'dgvSelTestName.Columns(0).Name = "id"
        ''dgvSelTestName.Columns("id").Visible = False


        'dgvSelTestName.Columns(1).Name = "testname"
        'dgvSelTestName.Columns(1).HeaderText = "Test Name"
        'dgvSelTestName.Columns(1).ReadOnly = True
        ''dgvSelTestName.

        'dgvSelTestName.Columns(2).Name = "analyzertestname"
        'dgvSelTestName.Columns(2).HeaderText = "Analyzer Test"
        'dgvSelTestName.Columns(2).ReadOnly = True

        'dgvSelTestName.Columns(3).Name = "price"
        'dgvSelTestName.Columns(3).HeaderText = "Harga"
        '' dgvSelTestName.Columns(3).DefaultCellStyle.Format = "c" 'format c datagrid
        'dgvSelTestName.Columns(3).ReadOnly = True

        'dgvSelTestName.Columns(4).Name = "pricenoformat"
        'dgvSelTestName.Columns(4).HeaderText = "harga cetak"


        dgvSelTestName.Rows.Add(rowdata)
        'dgvSelTestName.Columns.Add(chk)

        'dgvSelTestName.Columns("id").Visible = False
        'dgvSelTestName.Columns("pricenoformat").Visible = False
        MakeTotalPrice()

        greobject.CloseConn()
        greobject = Nothing

    End Sub

    Private Sub GenerateTestFromPack(ByVal packId As Integer)
        Dim strsql As String
        strsql = "select testpack.idtest as id ,testtype.testname,testtype.analyzertestname,testpack.id as idpack from testtype,testpack where testpack.idpack='" & packId & "' and testtype.id=testpack.idtest"

        Dim packrowdata As String()

        Dim lispackrowdata As New List(Of String())


        greobject = New clsGreConnect
        greobject.buildConn()

        Dim thecommand As New SqlClient.SqlCommand(strsql, greobject.grecon)
        Dim packReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)

        Do While packReader.Read
            If Not IsItAlreadyThere(packReader("id")) Then
                packrowdata = New String() {packReader("id"), packReader("testname"), packReader("analyzertestname"), FormatCurrency(0), 0, True, packId}
                DrawToGridSel(packrowdata)
            Else
                MessageBox.Show("Test " & packReader("testname") & " sudah ada", "Peringatan")
            End If

            'lispackrowdata.Add(packrowdata)

        Loop

        packReader.Close()
        thecommand.Dispose()
        greobject.CloseConn()



        'dgvSelTestName.ColumnCount = 5

        'Dim chk As New DataGridViewCheckBoxColumn
        'chk.HeaderText = "Pilih"
        ' ''chk.DisplayIndex = 5

        'dgvSelTestName.Columns(0).Name = "id"
        ''dgvSelTestName.Columns("id").Visible = False


        'dgvSelTestName.Columns(1).Name = "testname"
        'dgvSelTestName.Columns(1).HeaderText = "Test Name"
        'dgvSelTestName.Columns(1).ReadOnly = True
        ''dgvSelTestName.

        'dgvSelTestName.Columns(2).Name = "analyzertestname"
        'dgvSelTestName.Columns(2).HeaderText = "Analyzer Test"
        'dgvSelTestName.Columns(2).ReadOnly = True

        'dgvSelTestName.Columns(3).Name = "price"
        'dgvSelTestName.Columns(3).HeaderText = "Harga"
        '' dgvSelTestName.Columns(3).DefaultCellStyle.Format = "c" 'format c datagrid
        'dgvSelTestName.Columns(3).ReadOnly = True

        'dgvSelTestName.Columns(4).Name = "pricenoformat"
        'dgvSelTestName.Columns(4).HeaderText = "harga cetak"

        'dgvSelTestName.Rows.ad(lispackrowdata)
        ''dgvSelTestName.Rows.Add(packrowdata)
        'dgvSelTestName.Columns.Add(chk)

        'dgvSelTestName.Columns("id").Visible = False
        'dgvSelTestName.Columns("pricenoformat").Visible = False

    End Sub
    Private Sub GeneratePackToDisplay(ByVal packId As Integer)
        Dim strsql As String
        'strsql = "select testpack.idtest as id ,testtype.testname,testtype.analyzertestname from testtype,testpack where testpack.idpack='" & packId & "' and testtype.id=testpack.idtest"
        strsql = "select distinct idpack,ispackprice,packname,remark,pricepack,pricepack2,pricepack3,pricepack4,pricepack5,pricepack6 from testpack where idpack='" & packId & "'"

        Dim packrowdata As String()

        greobject = New clsGreConnect
        greobject.buildConn()

        Dim thecommand As New SqlClient.SqlCommand(strsql, greobject.grecon)
        Dim packReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)

        Do While packReader.Read
            If colprice = "price-level1" Then
                packrowdata = New String() {packReader("idpack"), packReader("packname"), FormatCurrency(packReader("pricepack")), packReader("pricepack")}
            ElseIf colprice = "price-level2" Then
                packrowdata = New String() {packReader("idpack"), packReader("packname"), FormatCurrency(packReader("pricepack2")), packReader("pricepack2")}
            ElseIf colprice = "price-level3" Then
                packrowdata = New String() {packReader("idpack"), packReader("packname"), FormatCurrency(packReader("pricepack3")), packReader("pricepack3")}
            ElseIf colprice = "price-level4" Then
                packrowdata = New String() {packReader("idpack"), packReader("packname"), FormatCurrency(packReader("pricepack4")), packReader("pricepack4")}
            ElseIf colprice = "price-level5" Then
                packrowdata = New String() {packReader("idpack"), packReader("packname"), FormatCurrency(packReader("pricepack5")), packReader("pricepack5")}
            ElseIf colprice = "price-level6" Then
                packrowdata = New String() {packReader("idpack"), packReader("packname"), FormatCurrency(packReader("pricepack6")), packReader("pricepack6")}
            End If

            'packrowdata = New String() {packReader("idpack"), packReader("packname"), FormatCurrency(packReader("pricepack")), packReader("pricepack")}

        Loop

        packReader.Close()
        thecommand.Dispose()
        greobject.CloseConn()


        dgpack.ColumnCount = 4

        Dim chk As New DataGridViewCheckBoxColumn
        chk.HeaderText = "Pilih"
        chk.Name = "chk"
        dgpack.Columns(0).Name = "idpack"
        'dgpack.Columns("id").Visible = False


        dgpack.Columns(1).Name = "packname"
        dgpack.Columns(1).HeaderText = "Packname"
        dgpack.Columns(1).ReadOnly = True
        'dgpack.

        dgpack.Columns(2).Name = "pricepack"
        dgpack.Columns(2).HeaderText = "Price"
        ' dgpack.Columns(3).DefaultCellStyle.Format = "c" 'format c datagrid
        dgpack.Columns(2).ReadOnly = True

        dgpack.Columns(3).Name = "pricenoformat"
        dgpack.Columns(3).HeaderText = "harga cetak"

        dgpack.Rows.Add(packrowdata)
        'dgpack.Rows.Add(packrowdata)
        dgpack.Columns.Add(chk)

        dgpack.Columns("idpack").Visible = False
        dgpack.Columns("pricenoformat").Visible = False




    End Sub

    Private Sub DrawToGridSel(ByVal strrow As String())
        'dgvSelTestName.ColumnCount = 6

        'Dim chk As New DataGridViewCheckBoxColumn
        'chk.HeaderText = "Pilih"
        'chk.Name = "chk"
        '''chk.DisplayIndex = 5

        'dgvSelTestName.Columns(0).Name = "id"
        ''dgvSelTestName.Columns("id").Visible = False


        'dgvSelTestName.Columns(1).Name = "testname"
        'dgvSelTestName.Columns(1).HeaderText = "Test Name"
        'dgvSelTestName.Columns(1).ReadOnly = True
        ''dgvSelTestName.

        'dgvSelTestName.Columns(2).Name = "analyzertestname"
        'dgvSelTestName.Columns(2).HeaderText = "Analyzer Test"
        'dgvSelTestName.Columns(2).ReadOnly = True

        'dgvSelTestName.Columns(3).Name = "price"
        'dgvSelTestName.Columns(3).HeaderText = "Harga"
        '' dgvSelTestName.Columns(3).DefaultCellStyle.Format = "c" 'format c datagrid
        'dgvSelTestName.Columns(3).ReadOnly = True

        'dgvSelTestName.Columns(4).Name = "pricenoformat"
        'dgvSelTestName.Columns(4).HeaderText = "harga cetak"

        'dgvSelTestName.Columns(5).Name = "idtestpack"
        'dgvSelTestName.Columns(5).HeaderText = "Test Pack"

        'dgvSelTestName.Rows.ad(lispackrowdata)
        dgvSelTestName.Rows.Add(strrow)
        'dgvSelTestName.Columns.Add(chk)

        'dgvSelTestName.Columns("id").Visible = False
        'dgvSelTestName.Columns("pricenoformat").Visible = False
        'dgvSelTestName.Columns("idtestpack").Visible = False

    End Sub

    Private Sub MakeTotalPrice()

        Dim i As Integer
        i = 0
        Dim price As Double
        For i = 0 To dgvSelTestName.RowCount - 1
            If Not dgvSelTestName.Rows(i).IsNewRow Then
                dgvSelTestName.Rows(i).Cells(5).Value = True
                price = price + dgvSelTestName.Rows(i).Cells(4).Value

            End If
        Next
        '------------------
        Dim j As Integer
        For j = 0 To dgpack.RowCount - 1
            If Not dgpack.Rows(j).IsNewRow Then
                dgpack.Rows(j).Cells(4).Value = True
                price = price + dgpack.Rows(j).Cells(3).Value
            End If
        Next




        pricetotal_tosave = price
        txtTotal.Text = FormatNumber(price, 0)
        txtTotal.ReadOnly = True
    End Sub

    Private Function IsItAlreadyThere(ByVal idtest As Integer) As Boolean
        Dim i As Integer
        For i = 0 To dgvSelTestName.Rows.Count - 1
            If dgvSelTestName.Item("id", i).Value = idtest Then
                'dgvSelTestName.Rows(DGTestItem.Item("testitemid", i).RowIndex).DefaultCellStyle.BackColor = Color.LightBlue
                'dgvSelTestName.Rows(DGTestItem.Item("testitemid", i).RowIndex).Cells(4).Value = True 'ambil nilai cell checkbox
                'dgvSelTestName.Rows(DGTestItem.Item("testitemid", i).RowIndex).ReadOnly = True
                'bagus
                Return True
            End If

        Next

        Return False
    End Function
    Private Function PackIsItAlreadyThere(ByVal idpack As Integer) As Boolean
        Dim i As Integer
        Dim result As Boolean
        result = False
        For i = 0 To dgpack.Rows.Count - 1
            If dgpack.Item("idpack", i).Value = idpack Then
                'dgvSelTestName.Rows(DGTestItem.Item("testitemid", i).RowIndex).DefaultCellStyle.BackColor = Color.LightBlue
                'dgvSelTestName.Rows(DGTestItem.Item("testitemid", i).RowIndex).Cells(4).Value = True 'ambil nilai cell checkbox
                'dgvSelTestName.Rows(DGTestItem.Item("testitemid", i).RowIndex).ReadOnly = True
                'bagus
                result = True
            End If

        Next

        Return result
    End Function

    Private Sub dgvSelTestName_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSelTestName.CellContentClick
        If Not (dgvSelTestName.CurrentRow.IsNewRow) Then
            'If Not IsDBNull(dgvSelTestName.Item("id", e.RowIndex).Value) Then
            ' dgvSelTestName.ReadOnly = False
            If TypeOf dgvSelTestName.Rows(e.RowIndex).Cells(e.ColumnIndex) Is DataGridViewCheckBoxCell Then

                Dim dgvCheckBoxCell As DataGridViewCheckBoxCell = dgvSelTestName.Rows(e.RowIndex).Cells(e.ColumnIndex)
                'Commit the data to the datasouce.
                dgvSelTestName.CommitEdit(DataGridViewDataErrorContexts.Commit)
                Dim checked As Boolean = CType(dgvCheckBoxCell.Value, Boolean)
                If Not checked Then
                    dgvSelTestName.Rows.RemoveAt(e.RowIndex)
                End If
            Else
                ' dgvSelTestName.ReadOnly = True
            End If
            'End If
        End If

        MakeTotalPrice()
    End Sub

    Private Sub txtbarcode_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        'If MouseButtons = Windows.Forms.MouseButtons.None Then
        '    setTextBarcode()
        'End If            'bagus
    End Sub



    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub txtTotal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTotal.TextChanged

    End Sub

    Private Sub txtsampleno_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub txtsampleno_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If e.KeyChar <> ChrW(Keys.Back) Then 'bagus hanya boleh angka dan backspace
            If Char.IsNumber(e.KeyChar) Then
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtsampleno_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)

    End Sub




    Private Sub btnsampleAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsampleAdd.Click

        '---to prevent blank test---
        If dgvSelTestName.Rows.Count < 1 Then
            MessageBox.Show("Tidak ada data yang disimpan")
            Exit Sub
        End If

        '------


        'currentlabnumber = "LN001"
        '-----------------sample mode in front registration----
        'If txtsampleno.Text = "" And txtbarcode.Text = "" Then
        '    MessageBox.Show("Silahkan isi nomor sample atau angka barcode")
        '    Return
        'End If

        'If Not Trim(txtsampleno.Text) = "" Then
        '    If IsSampleNoAvailable() Then
        '        MessageBox.Show("Nomor sample tersebut sudah ada")
        '        txtsampleno.Focus()
        '        Return
        '    End If
        'End If


        'If Not Trim(txtbarcode.Text) = "" Then
        '    If IsBarcodeAvailable() Then
        '        MessageBox.Show("Barcode tersebut sudah ada")
        '        txtbarcode.Focus()
        '        Return
        '    End If
        'End If


        '-----------------sample mode in front registration -------
        If Trim(txtlabnumber.Text) = "" Then
            MessageBox.Show("Silahkan isi lab number")
            txtlabnumber.Select()
            txtlabnumber.Focus()
            Return
        End If
        If notyetAdd = True Then
            If CekLabNumber() = True Then

                txtlabnumber.Select()
                txtlabnumber.Focus()
                If txtlabnumber.Text.Length > 1 Then
                    txtlabnumber.SelectionStart = txtlabnumber.Text.Length - 1
                    txtlabnumber.SelectionLength = 1
                End If
                Return


            Else
                txtlabnumber.ReadOnly = True
                currentlabnumber = Trim(txtlabnumber.Text)

            End If


        End If
        notyetAdd = False
        txtlabnumber.ReadOnly = True
        saveSampleDetail()
        ' PopulateDgvSampleList()
        dgvSelTestName.Rows.Clear()
        cbTestGroup.SelectedIndex = 0
        SaveMnu.BackColor = Color.Orange
        SaveMnu.Enabled = True
        'dgvtestname.Rows.Clear()
    End Sub

    Private Sub saveSampleDetail()
        Dim i As Integer

        Dim datereceived, dateorder As String

        datereceived = Format(Now, "yyyy-MM-dd HH:mm:ss")
        dateorder = Format(Now, "yyyy-MM-dd HH:mm:ss")

        'age text,
        Dim age As Integer
        Dim strage As String
        age = CountPatientAge(idDbpatient)
        strageforprint = GreCountPatientAge(idDbpatient)


        'userid = "0001"


        Dim labnumber As String

        labnumber = Trim(txtlabnumber.Text)
        'barcode = Trim(txtbarcode.Text)
        'sampleno = Trim(txtsampleno.Text)



        'Dim price As String
        Dim price As Double

        Dim universaltest As String
        Dim priority As String
        Dim active As String

        Dim idpasien As String
        Dim strupdate As String
        Dim idtestType As Integer
        '--
        priority = "R"
        If rdR.Checked = True Then
            priority = "R"
        ElseIf rdS.Checked = True Then
            priority = "S"
        End If
        active = "0"
        For i = 0 To dgvSelTestName.Rows.Count - 1
            If Not dgvSelTestName.Rows(i).IsNewRow Then
                'strsql = "insert into jobdetail(labnumber,idtes) "

                Dim obadd As New clsGreConnect
                obadd.buildConn()

                Dim pricestr As String
                pricestr = dgvSelTestName.Item("pricenoformat", i).Value
                pricestr = pricestr.Replace("Rp. ", "")
                price = CDbl(pricestr)
                idtestType = dgvSelTestName.Item("id", i).Value
                universaltest = dgvSelTestName.Item("analyzertestname", i).Value
                Dim idtestpack As String = dgvSelTestName.Item("idtestpack", i).Value

                '--original--sample mode in front registration
                'strupdate = "insert into jobdetail(labnumber,idtesttype,price,barcode,sampleno,specimendesc,universaltest,priority,dateorder,active,status)" & _
                '"values ('" & labnumber & "','" & idtestType & "','" & price & "','" & barcode & "','" & sampleno & "','" & specimendesc & "','" & universaltest & "','" & priority & "','" & dateorder & "','" & active & "','0')"
                '--original -with speciment desc, labnumer, sampleno

                strupdate = "insert into jobdetail(labnumber,idtesttype,price,universaltest,priority,dateorder,active,status,idtestpack)" &
                   "values ('" & labnumber & "','" & idtestType & "','" & price & "','" & universaltest & "','" & priority & "','" & dateorder & "','" & active & "','" & INACTIVESAMPLE & "'," & idtestpack & ")"
                obadd.ExecuteNonQuery(strupdate)

                'If id_sample_is_already_there(idtestType, labnumber) = False Then
                '    strupdate = "insert into jobdetail(labnumber,idtesttype,price,universaltest,priority,dateorder,active,status)" & _
                '   "values ('" & labnumber & "','" & idtestType & "','" & price & "','" & universaltest & "','" & priority & "','" & dateorder & "','" & active & "','" & INACTIVESAMPLE & "')"
                '    obadd.ExecuteNonQuery(strupdate)

                'Else
                '    MessageBox.Show("Test " & universaltest & " sudah ada", "Peringatan")
                'End If

                obadd.CloseConn()

            End If
        Next

        '============== PACK
        Dim k As Integer
        Dim packname As String
        Dim idpack As Integer
        Dim ispackprice As Integer = 1

        For k = 0 To dgpack.Rows.Count - 1
            If Not dgpack.Rows(k).IsNewRow Then
                'strsql = "insert into jobdetail(labnumber,idtes) "

                Dim objpack As New clsGreConnect
                objpack.buildConn()

                Dim pricestr As String
                pricestr = dgpack.Item("pricenoformat", k).Value
                pricestr = pricestr.Replace("Rp. ", "")
                price = CDbl(pricestr)
                idpack = dgpack.Item("idpack", k).Value
                packname = dgpack.Item("packname", k).Value


                If id_pack_is_already_there(idpack, labnumber) = False Then
                    strupdate = "insert into jobpack(labnumber,idpack,pricepack,packname,barcode,ispackprice)" & _
                   "values ('" & labnumber & "','" & idpack & "','" & price & "','" & packname & "','','" & ispackprice & "')"
                    objpack.ExecuteNonQuery(strupdate)

                Else
                    MessageBox.Show("Paket " & packname & " sudah ada", "Peringatan")
                End If

                objpack.CloseConn()

            End If
        Next



    End Sub
    Private Function id_sample_is_already_there(ByVal idteste As Integer, ByVal lne As String) As Boolean
        Dim strcari As String
        Dim enekopoora As Boolean
        enekopoora = False

        Dim akehe As Integer

        strcari = "select count(id) as enek from jobdetail where labnumber='" & lne & "' and idtesttype='" & idteste & "'"

        Dim greObject As New clsGreConnect
        greObject.buildConn()

        Dim thecommand As New SqlClient.SqlCommand(strcari, greObject.grecon)
        Dim theReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)
        Do While theReader.Read
            akehe = theReader("enek")
        Loop

        theReader.Close()
        thecommand.Dispose()
        greObject.CloseConn()
        greObject = Nothing

        If akehe = 0 Then
            Return False
        ElseIf akehe > 0 Then
            Return True
        End If


    End Function
    Private Function id_pack_is_already_there(ByVal idpack As Integer, ByVal lne As String) As Boolean
        Dim strcari As String
        Dim enekopoora As Boolean
        enekopoora = False

        Dim akehe As Integer

        strcari = "select count(id) as enek from jobpack where labnumber='" & lne & "' and idpack='" & idpack & "'"

        Dim greObject As New clsGreConnect
        greObject.buildConn()

        Dim thecommand As New SqlClient.SqlCommand(strcari, greObject.grecon)
        Dim theReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)
        Do While theReader.Read
            akehe = theReader("enek")
        Loop

        theReader.Close()
        thecommand.Dispose()
        greObject.CloseConn()
        greObject = Nothing

        If akehe = 0 Then
            Return False
        ElseIf akehe > 0 Then
            Return True
        End If


    End Function


    Private Sub FirstLoad()

        dgvSelTestName.AllowUserToAddRows = False
        dgvSelTestName.RowHeadersVisible = False

        dgvtestname.AllowUserToAddRows = False
        dgvtestname.RowHeadersVisible = False

        dgpack.AllowUserToAddRows = False
        dgpack.RowHeadersVisible = False
        '-------------------------------------------
        'dgvSelTestName.ColumnCount = 5

        'Dim chk As New DataGridViewCheckBoxColumn
        'chk.HeaderText = "Pilih"
        ''chk.DisplayIndex = 5

        'dgvSelTestName.Columns(0).Name = "id"
        ''dgvSelTestName.Columns("id").Visible = False


        'dgvSelTestName.Columns(1).Name = "testname"
        'dgvSelTestName.Columns(1).HeaderText = "Test Name"
        'dgvSelTestName.Columns(1).ReadOnly = True
        ''dgvSelTestName.

        'dgvSelTestName.Columns(2).Name = "analyzertestname"
        'dgvSelTestName.Columns(2).HeaderText = "Analyzer Test"
        'dgvSelTestName.Columns(2).ReadOnly = True

        'dgvSelTestName.Columns(3).Name = "price"
        'dgvSelTestName.Columns(3).HeaderText = "Harga"
        '' dgvSelTestName.Columns(3).DefaultCellStyle.Format = "c" 'format c datagrid
        'dgvSelTestName.Columns(3).ReadOnly = True

        'dgvSelTestName.Columns(4).Name = "pricenoformat"
        'dgvSelTestName.Columns(4).HeaderText = "harga cetak"
        '-----------------------------------------------


    End Sub

    Private Function GetUniversalTestName(ByVal labnumber As String) As String
        Dim strfinder As String
        Dim strresult As String
        Dim testcon As New clsGreConnect
        testcon.buildConn()
        'strfinder = "select universaltest from jobdetail where sampleno='" & sampleno & "' and labnumber='" & labnumber & "'"
        strfinder = "select universaltest from jobdetail where labnumber='" & labnumber & "'"
        Dim thecommand As New SqlClient.SqlCommand(strfinder, testcon.grecon)
        Dim theReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)
        strresult = ""
        Do While theReader.Read
            strresult = strresult & " " & theReader("universaltest")
        Loop
        thecommand = Nothing
        theReader.Close()
        testcon.CloseConn()
        testcon = Nothing

        Return strresult
    End Function



    'Private Sub dgvSampleList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
    '    If e.RowIndex < 0 OrElse Not e.ColumnIndex = dgvSampleList.Columns("Delete").Index Then
    '        Return

    '    Else

    '        If Not dgvSampleList.CurrentRow.IsNewRow Then
    '            Dim selectedsampleNo As String
    '            Dim delo As New clsGreConnect
    '            delo.buildConn()
    '            selectedsampleNo = dgvSampleList.Item("SampleNo", dgvSampleList.CurrentRow.Index).Value
    '            Dim savestrsql As String

    '            savestrsql = "delete from jobdetail where sampleno=" & _
    '                        "'" & selectedsampleNo & "' and labnumber='" & currentlabnumber & "'"

    '            delo.ExecuteNonQuery(savestrsql)
    '            PopulateDgvSampleList()
    '            delo.CloseConn()
    '        End If

    '    End If
    'End Sub



    Private Sub txtlabnumber_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtlabnumber.KeyPress
        If labnumbering = LAB_AUTO_NUMBERING Then
            If Char.IsDigit(e.KeyChar) = False And Char.IsControl(e.KeyChar) = False Then
                e.Handled = True
                MsgBox("Masukkan Angka") 'bagus
            End If
        End If
    End Sub

    Private Sub txtlabnumber_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtlabnumber.Leave


        'Dim ada As Boolean
        'ada = False
        'If Trim(txtlabnumber.Text) = "" Then
        '    MessageBox.Show("Silahkan isi labnumber dahulu")

        '    txtlabnumber.Focus()
        'Else
        '    Dim objcon As New clsGreConnect

        '    objcon.buildConn()
        '    Dim cmd As New SqlClient.SqlCommand("select count(labnumber) as num from job where labnumber='" & Trim(txtlabnumber.Text) & "'", objcon.grecon)
        '    Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        '    Do While rdr.Read
        '        If rdr("num") > 0 Then
        '            MessageBox.Show("Lab number tersebut sudah digunakan")
        '            ada = True
        '        Else
        '            ada = False
        '        End If
        '    Loop
        '    cmd.Dispose()
        '    rdr.Close()
        '    objcon.CloseConn()
        '    objcon = Nothing

        'End If
        'If Trim(txtlabnumber.Text) <> "" Then
        '    If ada = True Then
        '        txtlabnumber.Focus()
        '        If txtlabnumber.Text.Length > 1 Then
        '            txtlabnumber.SelectionStart = txtlabnumber.Text.Length - 1
        '            txtlabnumber.SelectionLength = 1
        '        End If
        '        'Return
        '    ElseIf ada = False Then
        '        txtlabnumber.ReadOnly = True
        '        currentlabnumber = Trim(txtlabnumber.Text)
        '    End If
        'End If

    End Sub
    Private Function CekLabNumber() As Boolean
        Dim ada As Boolean
        ada = False
        If Trim(txtlabnumber.Text) = "" Then
            MessageBox.Show("Silahkan isi labnumber dahulu")
            txtlabnumber.Focus()
        Else
            Dim objcon As New clsGreConnect

            objcon.buildConn()
            Dim cmd As New SqlClient.SqlCommand("select count(labnumber) as num from jobdetail where labnumber='" & Trim(txtlabnumber.Text) & "'", objcon.grecon)
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While rdr.Read
                If rdr("num") > 0 Then
                    MessageBox.Show("Lab number tersebut sudah digunakan")
                    ada = True
                Else
                    ada = False
                End If
            Loop
            cmd.Dispose()
            rdr.Close()
            objcon.CloseConn()
            objcon = Nothing

        End If
        Return ada

    End Function

    Private Sub txtbarcode_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        'setTextBarcode() 'bagus
    End Sub

    Private Sub ToolStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles ToolStrip1.ItemClicked

    End Sub

    Private Sub txtsampleno_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub EditMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub



    Private Sub txtDocter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDocter.TextChanged

    End Sub

    Private Sub txtdocteraddress_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtdocteraddress.TextChanged

    End Sub




    Private Sub btnprevlab_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnprevlab.Click
        Dim lookupLabNum As New LabNumberLookupFrm
        lookupLabNum.StartPosition = FormStartPosition.Manual

        lookupLabNum.Top = Me.Top + 200
        lookupLabNum.Left = Me.Width - lookupLabNum.Width
        lookupLabNum.ShowDialog()
    End Sub

    Private Sub cancelMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelMnu.Click
        Me.Close()
    End Sub

    Private Sub dtTglLahir_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtTglLahir.ValueChanged

    End Sub

    Private Sub frmRegistration_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        If gender = "1" Then
            rdoLaki.Checked = True
        Else
            rdoPerempuan.Checked = True
        End If
    End Sub
    ReadOnly AllowedPhoneKeys As String = _
    "0123456789"
    Private Sub txtPhone_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPhone.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedPhoneKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedPhoneKeys.Contains(e.KeyChar)

        End Select
    End Sub
    ReadOnly AllowedKeys As String = _
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 "
    Private Sub txtAlamat_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAlamat.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)

        End Select
    End Sub




    Private Sub cmbpack_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbpack.SelectedIndexChanged
        If rdopack.Checked = True Then
            Dim idpack As Integer
            'Dim objTestRelated As New TestRelated
            'selectedgroupid = objTestRelated.GetIdTestGroup(cbTestGroup.Text)
            idpack = cmbpack.SelectedValue
            PopulateDgBasedOnPack(idpack)

            idpackfixedprice = 0
            'if packed have certain price-use price paack

            Dim cekpackcon As New clsGreConnect

            cekpackcon.buildConn()
            Dim cmd As New SqlClient.SqlCommand("select * from testpack where idpack='" & idpack & "'", cekpackcon.grecon)
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While rdr.Read
                If Not IsDBNull(rdr("ispackprice")) Then
                    If rdr("ispackprice") = 1 Then
                        cmdaddprofile.Visible = True
                        idpackfixedprice = idpack

                        txtpackdesc.Text = "Test Profile " & cmbpack.Text & "  Harga "
                        txtyellow.Text = "Test Profile mempunyai 1 harga yang sama yang mencakupo semua test. Tidak bisa dipilih satu persatu." & _
                        " Konfigurasi bisa dilakukan dari Setting Test Profile."

                        If colprice = "price-level1" Then
                            txtpackprice.Text = FormatNumber(rdr("pricepack"), 0)
                        ElseIf colprice = "price-level2" Then
                            txtpackprice.Text = FormatNumber(rdr("pricepack2"), 0)
                        ElseIf colprice = "price-level3" Then
                            txtpackprice.Text = FormatNumber(rdr("pricepack3"), 0)
                        ElseIf colprice = "price-level4" Then
                            txtpackprice.Text = FormatNumber(rdr("pricepack4"), 0)
                        ElseIf colprice = "price-level5" Then
                            txtpackprice.Text = FormatNumber(rdr("pricepack5"), 0)
                        ElseIf colprice = "price-level6" Then
                            txtpackprice.Text = FormatNumber(rdr("pricepack6"), 0)
                        End If




                        dgvtestname.ReadOnly = True
                    ElseIf idpackfixedprice = 0 Then
                        'idpackfixedprice = 0
                        ' ada = False
                        txtpackdesc.Text = ""
                        txtpackprice.Text = ""
                        txtyellow.Text = ""
                        cmdaddprofile.Visible = False
                        dgvtestname.ReadOnly = False
                    Else
                        cmdaddprofile.Visible = False
                        dgvtestname.ReadOnly = False
                        txtpackdesc.Text = ""
                        txtyellow.Text = ""
                        txtpackprice.Text = ""
                    End If
                Else
                    dgvtestname.ReadOnly = False
                    txtpackdesc.Text = ""
                    txtyellow.Text = ""
                    txtpackprice.Text = ""
                End If
            Loop
            cmd.Dispose()
            rdr.Close()
            cekpackcon.CloseConn()
            cekpackcon = Nothing


        End If



    End Sub

    Private Sub rdopack_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdopack.CheckedChanged
        If rdopack.Checked = True Then
            rdogroup.Checked = False
            cbTestGroup.Enabled = False
            cmbpack.Enabled = True
        End If
    End Sub

    Private Sub rdogroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdogroup.CheckedChanged
        If rdogroup.Checked = True Then
            rdopack.Checked = False
            cmbpack.Enabled = False
            cbTestGroup.Enabled = True
            cmdaddprofile.Visible = False
            dgvtestname.ReadOnly = False
        End If
    End Sub

    Private Sub cmdaddprofile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdaddprofile.Click

        If PackIsItAlreadyThere(idpackfixedprice) = False Then
            GenerateTestFromPack(idpackfixedprice)
            GeneratePackToDisplay(idpackfixedprice)
            MakeTotalPrice()
        End If


    End Sub

    Private Sub cmdcancelall_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdcancelall.Click
        dgvSelTestName.Rows.Clear()
        dgpack.Rows.Clear()
        txtTotal.Clear()
    End Sub

    Private Sub dgpack_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgpack.CellContentClick
        If Not (dgpack.CurrentRow.IsNewRow) Then
            'If Not IsDBNull(dgvSelTestName.Item("id", e.RowIndex).Value) Then
            ' dgvSelTestName.ReadOnly = False
            If TypeOf dgpack.Rows(e.RowIndex).Cells(e.ColumnIndex) Is DataGridViewCheckBoxCell Then

                Dim dgvCheckBoxCell As DataGridViewCheckBoxCell = dgpack.Rows(e.RowIndex).Cells(e.ColumnIndex)
                'Commit the data to the datasouce.
                dgpack.CommitEdit(DataGridViewDataErrorContexts.Commit)
                Dim checked As Boolean = CType(dgvCheckBoxCell.Value, Boolean)
                If Not checked Then
                    Dim selidpack As Integer
                    selidpack = dgpack.Item("idpack", e.RowIndex).Value
                    'clear idtest
                    NormalizedTestItem(selidpack)
                    dgpack.Rows.RemoveAt(e.RowIndex)
                End If
            Else
                ' dgvSelTestName.ReadOnly = True
            End If
            'End If
        End If

        MakeTotalPrice()
    End Sub

    Private Sub NormalizedTestItem(ByVal idmetode As Integer)

        'Dim i As Integer
        'Dim strsql As String
        'Dim dbcon As New clsGreConnect

        'dbcon.BuildConn()
        'strsql = "select idtest from testpack where idpack='" & idmetode & "'"
        'Dim metodeCommand As New SqlClient.SqlCommand(strsql, dbcon.grecon)
        'Dim metodeReader As SqlDataReader = metodeCommand.ExecuteReader(CommandBehavior.CloseConnection)
        'Do While metodeReader.Read
        '    Dim ic As Integer
        '    ic = dgvSelTestName.Rows.Count - 1
        '    'Do While 
        '    ' For i = 0 To ic 'dgvSelTestName.Rows.Count - 1
        '    i = 0
        '    Do While i < ic + 1
        '        If Not IsDBNull(metodeReader("idtest")) Then
        '            If dgvSelTestName.Item("id", i).Value = metodeReader("idtest") Then
        '                'dgvSelTestName.Rows(dgvSelTestName.Item("id", i).RowIndex).ReadOnly = False
        '                'merubah warna--------------
        '                'dgvSelTestName.Rows(dgvSelTestName.Item("id", i).RowIndex).DefaultCellStyle.BackColor = Color.Red
        '                '---------------------------
        '                'dgvSelTestName.Rows(dgvSelTestName.Item("id", i).RowIndex).Cells("chk").Value = False
        '                dgvSelTestName.Rows.RemoveAt(i)
        '                ic = dgvSelTestName.Rows.Count - 1
        '                i = i + 1
        '            End If
        '            i = i + 1
        '        End If
        '    Loop
        '    'Next
        'Loop

        'metodeReader.Close()
        'metodeCommand.Dispose()

        For irow As Integer = dgvSelTestName.Rows.Count - 1 To 0 Step -1
            If dgvSelTestName.Rows(irow).Cells("idtestpack").Value = idmetode Then
                dgvSelTestName.Rows.RemoveAt(irow)
            End If
        Next

    End Sub


End Class
