﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUser
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUser))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.AddMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.SaveMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.EditMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.DeleteMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator
        Me.cancelMnu = New System.Windows.Forms.ToolStripButton
        Me.dguser = New System.Windows.Forms.DataGridView
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.chkInstrument = New System.Windows.Forms.CheckBox
        Me.chkkasir = New System.Windows.Forms.CheckBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtpass2 = New System.Windows.Forms.TextBox
        Me.chksetting = New System.Windows.Forms.CheckBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.chkresult = New System.Windows.Forms.CheckBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.chkregister = New System.Windows.Forms.CheckBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.chkactive = New System.Windows.Forms.CheckBox
        Me.txttelp = New System.Windows.Forms.TextBox
        Me.txtnama = New System.Windows.Forms.TextBox
        Me.txtpass = New System.Windows.Forms.TextBox
        Me.txtuser = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.ToolStrip1.SuspendLayout()
        CType(Me.dguser, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddMnu, Me.ToolStripSeparator5, Me.SaveMnu, Me.ToolStripSeparator6, Me.EditMnu, Me.ToolStripSeparator7, Me.DeleteMnu, Me.ToolStripSeparator8, Me.cancelMnu})
        Me.ToolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1006, 46)
        Me.ToolStrip1.TabIndex = 36
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'AddMnu
        '
        Me.AddMnu.Image = Global.MEDLIS.My.Resources.Resources.plus
        Me.AddMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.AddMnu.Name = "AddMnu"
        Me.AddMnu.Size = New System.Drawing.Size(65, 43)
        Me.AddMnu.Text = "Add"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 46)
        '
        'SaveMnu
        '
        Me.SaveMnu.Image = Global.MEDLIS.My.Resources.Resources.save
        Me.SaveMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SaveMnu.Name = "SaveMnu"
        Me.SaveMnu.Size = New System.Drawing.Size(67, 43)
        Me.SaveMnu.Text = "Save"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 46)
        '
        'EditMnu
        '
        Me.EditMnu.Image = Global.MEDLIS.My.Resources.Resources.write_edit
        Me.EditMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.EditMnu.Name = "EditMnu"
        Me.EditMnu.Size = New System.Drawing.Size(63, 43)
        Me.EditMnu.Text = "Edit"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(6, 46)
        '
        'DeleteMnu
        '
        Me.DeleteMnu.AutoSize = False
        Me.DeleteMnu.Image = Global.MEDLIS.My.Resources.Resources.cut
        Me.DeleteMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.DeleteMnu.Name = "DeleteMnu"
        Me.DeleteMnu.Size = New System.Drawing.Size(82, 39)
        Me.DeleteMnu.Text = "Delete"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(6, 46)
        '
        'cancelMnu
        '
        Me.cancelMnu.AutoSize = False
        Me.cancelMnu.Image = Global.MEDLIS.My.Resources.Resources.cancel
        Me.cancelMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cancelMnu.Name = "cancelMnu"
        Me.cancelMnu.Size = New System.Drawing.Size(85, 39)
        Me.cancelMnu.Text = "Cancel"
        '
        'dguser
        '
        Me.dguser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dguser.Location = New System.Drawing.Point(7, 56)
        Me.dguser.Name = "dguser"
        Me.dguser.Size = New System.Drawing.Size(544, 385)
        Me.dguser.TabIndex = 38
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkInstrument)
        Me.Panel1.Controls.Add(Me.chkkasir)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txtpass2)
        Me.Panel1.Controls.Add(Me.chksetting)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.chkresult)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.chkregister)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.chkactive)
        Me.Panel1.Controls.Add(Me.txttelp)
        Me.Panel1.Controls.Add(Me.txtnama)
        Me.Panel1.Controls.Add(Me.txtpass)
        Me.Panel1.Controls.Add(Me.txtuser)
        Me.Panel1.Location = New System.Drawing.Point(8, 18)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(414, 368)
        Me.Panel1.TabIndex = 15
        '
        'chkInstrument
        '
        Me.chkInstrument.AutoSize = True
        Me.chkInstrument.Location = New System.Drawing.Point(128, 332)
        Me.chkInstrument.Name = "chkInstrument"
        Me.chkInstrument.Size = New System.Drawing.Size(147, 19)
        Me.chkInstrument.TabIndex = 18
        Me.chkInstrument.Text = "Intrument Operator"
        Me.chkInstrument.UseVisualStyleBackColor = True
        '
        'chkkasir
        '
        Me.chkkasir.AutoSize = True
        Me.chkkasir.Location = New System.Drawing.Point(270, 304)
        Me.chkkasir.Name = "chkkasir"
        Me.chkkasir.Size = New System.Drawing.Size(106, 19)
        Me.chkkasir.TabIndex = 17
        Me.chkkasir.Text = "Pembayaran"
        Me.chkkasir.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(45, 107)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 15)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "password"
        '
        'txtpass2
        '
        Me.txtpass2.Location = New System.Drawing.Point(128, 104)
        Me.txtpass2.Name = "txtpass2"
        Me.txtpass2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtpass2.Size = New System.Drawing.Size(237, 21)
        Me.txtpass2.TabIndex = 15
        '
        'chksetting
        '
        Me.chksetting.AutoSize = True
        Me.chksetting.Location = New System.Drawing.Point(128, 276)
        Me.chksetting.Name = "chksetting"
        Me.chksetting.Size = New System.Drawing.Size(71, 19)
        Me.chksetting.TabIndex = 7
        Me.chksetting.Text = "Setting"
        Me.chksetting.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(45, 277)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(50, 15)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Fungsi"
        '
        'chkresult
        '
        Me.chkresult.AutoSize = True
        Me.chkresult.Location = New System.Drawing.Point(128, 304)
        Me.chkresult.Name = "chkresult"
        Me.chkresult.Size = New System.Drawing.Size(101, 19)
        Me.chkresult.TabIndex = 6
        Me.chkresult.Text = "Print Result"
        Me.chkresult.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(45, 238)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(47, 15)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Status"
        '
        'chkregister
        '
        Me.chkregister.AutoSize = True
        Me.chkregister.Location = New System.Drawing.Point(270, 276)
        Me.chkregister.Name = "chkregister"
        Me.chkregister.Size = New System.Drawing.Size(80, 19)
        Me.chkregister.TabIndex = 5
        Me.chkregister.Text = "Register"
        Me.chkregister.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(45, 196)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 15)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Telp"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(45, 148)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 15)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Nama"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(45, 66)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 15)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "password"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(45, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 15)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Username"
        '
        'chkactive
        '
        Me.chkactive.AutoSize = True
        Me.chkactive.Location = New System.Drawing.Point(128, 237)
        Me.chkactive.Name = "chkactive"
        Me.chkactive.Size = New System.Drawing.Size(63, 19)
        Me.chkactive.TabIndex = 4
        Me.chkactive.Text = "Active"
        Me.chkactive.UseVisualStyleBackColor = True
        '
        'txttelp
        '
        Me.txttelp.Location = New System.Drawing.Point(127, 189)
        Me.txttelp.Name = "txttelp"
        Me.txttelp.Size = New System.Drawing.Size(237, 21)
        Me.txttelp.TabIndex = 3
        '
        'txtnama
        '
        Me.txtnama.Location = New System.Drawing.Point(127, 142)
        Me.txtnama.Name = "txtnama"
        Me.txtnama.Size = New System.Drawing.Size(237, 21)
        Me.txtnama.TabIndex = 2
        '
        'txtpass
        '
        Me.txtpass.Location = New System.Drawing.Point(127, 62)
        Me.txtpass.Name = "txtpass"
        Me.txtpass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtpass.Size = New System.Drawing.Size(237, 21)
        Me.txtpass.TabIndex = 1
        '
        'txtuser
        '
        Me.txtuser.Location = New System.Drawing.Point(127, 20)
        Me.txtuser.Name = "txtuser"
        Me.txtuser.Size = New System.Drawing.Size(237, 21)
        Me.txtuser.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Location = New System.Drawing.Point(567, 49)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(433, 392)
        Me.GroupBox1.TabIndex = 39
        Me.GroupBox1.TabStop = False
        '
        'frmUser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1006, 447)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dguser)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmUser"
        Me.Text = "USER MANAGEMENT"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.dguser, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents AddMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SaveMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents EditMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents DeleteMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cancelMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents dguser As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chksetting As System.Windows.Forms.CheckBox
    Friend WithEvents chkregister As System.Windows.Forms.CheckBox
    Friend WithEvents chkactive As System.Windows.Forms.CheckBox
    Friend WithEvents txttelp As System.Windows.Forms.TextBox
    Friend WithEvents txtnama As System.Windows.Forms.TextBox
    Friend WithEvents txtpass As System.Windows.Forms.TextBox
    Friend WithEvents txtuser As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtpass2 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkkasir As System.Windows.Forms.CheckBox
    Friend WithEvents chkInstrument As System.Windows.Forms.CheckBox
    Friend WithEvents chkresult As System.Windows.Forms.CheckBox
End Class
