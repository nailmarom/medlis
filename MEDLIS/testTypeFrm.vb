﻿'
' Created by SharpDevelop.
' User: User
' Date: 10/14/2014
' Time: 9:19 AM
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'

Imports System.Data.SqlClient

Partial Public Class testTypeFrm
    Dim objConn As New clsGreConnect
    Private tblGroupTest As DataTable
    Dim strsql As String
    Dim selectedgroupid As Integer
    Dim selectedtestid As Integer
    Dim isNew As Boolean
    Dim isEdit As Boolean
    Dim dgt As Integer
    Dim objTestRelated As New TestRelated
    Dim displayindex As Integer

    Public Sub New()
        ' The Me.InitializeComponent call is required for Windows Forms designer support.
        Me.InitializeComponent()

        '
        ' TODO : Add constructor code after InitializeComponents
        ''
    End Sub
    Private Sub FillCBSpeciment()
        cbSpeciment.Items.Add("Serum") '1
        cbSpeciment.Items.Add("Urine")
        cbSpeciment.Items.Add("Whole blood")
        cbSpeciment.Items.Add("Feses")
        cbSpeciment.Items.Add("Plasma")
        cbSpeciment.Items.Add("Gastric Juice")
        cbSpeciment.Items.Add("Ascites")
        cbSpeciment.Items.Add("CSF")
        cbSpeciment.Items.Add("Other")
        cbSpeciment.SelectedIndex = 0
    End Sub

    Private Function GetSpecimen() As String
        Dim result As String
        If cbSpeciment.Text = "Serum" Then
            result = "1"
        ElseIf cbSpeciment.Text = "Urine" Then
            result = "2"
        ElseIf cbSpeciment.Text = "Plasma" Then
            result = "3"
        ElseIf cbSpeciment.Text = "Gastric Juice" Then
            result = "4"
        ElseIf cbSpeciment.Text = "Ascites" Then
            result = "5"
        ElseIf cbSpeciment.Text = "CSF" Then
            result = "6"
        ElseIf cbSpeciment.Text = "Whole blood" Then
            result = "7"
        ElseIf cbSpeciment.Text = "Feses" Then
            result = "8"
        ElseIf cbSpeciment.Text = "Other" Then
            result = "9"
        End If
        Return result
    End Function

    Private Sub testTypeFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
        cbresultType.Items.Add(Result_type_Number)
        cbresultType.Items.Add(Result_type_Text)
        '  cbresultType.Items.Add(Result_type_Number_and_Text)





        'fill the combo
        ' cbdigit.SelectedItem = cbdigit.SelectedIndex.MinValue
        FillCBSpeciment()

        Dim strsql As String
        Dim greObject As New clsGreConnect
        Icon = New System.Drawing.Icon("medlis2.ico")
        strsql = "select * from testgroup"
        greObject.buildConn()

        Dim thecommand As New SqlClient.SqlCommand(strsql, greObject.grecon)
        Dim testTypeReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)
        cbGroupChoice.Items.Add("SEMUA TEST")
        Do While testTypeReader.Read
            cbGroupChoice.Items.Add(testTypeReader("testgroupname"))
            cbTestGroup.Items.Add(testTypeReader("testgroupname"))
        Loop

        cbTestGroup.Enabled = False
        cbdigit.Enabled = False

        Dim ctrl As Control
        For Each ctrl In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.Enabled = False 'BackColor = Color.LightYellow
            End If
        Next
        FillCbRelatedX()
        FillCbRelatedY()
        FillCbRelatedZ()
        testTypeReader.Close()
        thecommand.Dispose()
        'NpgsqlConnection.ClearAllPools()
        SaveMnu.Enabled = False

        cbrelatedX.Enabled = False
        cbrelatedY.Enabled = False
        cbrelatedZ.Enabled = False
        txtrelatedformula.Enabled = False
        rdoyamanual.Enabled = False
        rdotidak.Enabled = False

        Dim txtm As Control
        For Each txtm In Panel2.Controls
            If (txtm.GetType() Is GetType(TextBox)) Then
                Dim tx As TextBox = CType(txtm, TextBox)
                tx.Enabled = False
            End If
        Next


    End Sub
    Private Sub FillCbRelatedX()
        Dim cncombo As New clsGreConnect
        Dim cmdcombo As SqlClient.SqlCommand
        Dim sql As String
        Dim dacombo As New SqlClient.SqlDataAdapter
        Dim dscombo As New DataSet
        cncombo.buildConn()

        sql = "select id, analyzertestname from testtype"

        cmdcombo = New SqlClient.SqlCommand(sql, cncombo.grecon)
        dacombo.SelectCommand = cmdcombo
        dacombo.Fill(dscombo)
        dacombo.Dispose()
        cmdcombo.Dispose()
        cncombo.CloseConn()
        cbrelatedX.DataSource = Nothing

        cbrelatedX.ValueMember = "id"
        cbrelatedX.DisplayMember = "analyzertestname"
        cbrelatedX.DataSource = dscombo.Tables(0)
        cncombo.CloseConn()

    End Sub
    Private Sub FillCbRelatedY()
        Dim cncombo As New clsGreConnect
        Dim cmdcombo As SqlClient.SqlCommand
        Dim sql As String
        Dim dacombo As New SqlClient.SqlDataAdapter
        Dim dscombo As New DataSet
        cncombo.buildConn()

        sql = "select id, analyzertestname from testtype"

        cmdcombo = New SqlClient.SqlCommand(sql, cncombo.grecon)
        dacombo.SelectCommand = cmdcombo
        dacombo.Fill(dscombo)
        dacombo.Dispose()
        cmdcombo.Dispose()
        cncombo.CloseConn()
        cbrelatedY.DataSource = Nothing

        cbrelatedY.ValueMember = "id"
        cbrelatedY.DisplayMember = "analyzertestname"
        cbrelatedY.DataSource = dscombo.Tables(0)
        cncombo.CloseConn()

    End Sub
    Private Sub FillCbRelatedZ()
        Dim cncombo As New clsGreConnect
        Dim cmdcombo As SqlClient.SqlCommand
        Dim sql As String
        Dim dacombo As New SqlClient.SqlDataAdapter
        Dim dscombo As New DataSet
        cncombo.buildConn()

        sql = "select id, analyzertestname from testtype"

        cmdcombo = New SqlClient.SqlCommand(sql, cncombo.grecon)
        dacombo.SelectCommand = cmdcombo
        dacombo.Fill(dscombo)
        dacombo.Dispose()
        cmdcombo.Dispose()
        cncombo.CloseConn()
        cbrelatedZ.DataSource = Nothing

        cbrelatedZ.ValueMember = "id"
        cbrelatedZ.DisplayMember = "analyzertestname"
        cbrelatedZ.DataSource = dscombo.Tables(0)
        cncombo.CloseConn()

    End Sub
    Private Sub cbGroupChoice_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbGroupChoice.SelectedIndexChanged
        'Dim varIdTestGroup As Integer
        ClearControl()
        ' If Trim(cbGroupChoice.Text) <> "Semua Test" Then
        Dim objTestRelated As New TestRelated
        objConn.buildConn()
        selectedgroupid = objTestRelated.GetIdTestGroup(Trim(cbGroupChoice.Text))
        'tblGroupTest = objConn.ExecuteQuery("Select * from TestType where IdTestGroup='" & varIdTestGroup & "'")
        'dgTestType.DataSource = tblGroupTest
        objConn.CloseConn()
        PopulateDg()
        isEdit = False
        isNew = False
        ' ElseIf Trim(cbGroupChoice.Text) = "Semua Test" Then

        '  End If
    End Sub

    Private Sub mnuAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)




    End Sub

    Private Sub mnuSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


        'testGroupname = txtTestGroupName.Text

        'strinsert = "insert into TestGroup (" & _
        '"TestGroupName)" & _
        '" VALUES (" & _
        ' "'" & testGroupname & "')"

        'PopulateDg()

        'strupdate = "update regsamp set customerid='" & customerID & _
        '   "',RECEIVEDATE='" & Format(Date.Parse(DTPreceiveDate.Text), "yyyy-MM-dd HH:mm:ss") & _
        '   "',SHIP='" & selectedShipmentID & _
        '   "',REFF='" & Trim(txtReference.Text) & _
        '   "',note='" & Trim(txtNote.Text) & _
        '   "',CAP='" & cap & _
        '   "',REASON='" & Trim(txtreason.Text) & _
        '   "',TOTALIDR='" & txttotalidr.Text & _
        '   "',TOTALUSD='" & txttotalusd.Text & _
        '   "',USERID='" & LogOnUserID & _
        '   "',STATUSAPP='" & statusapp & _
        '   "',PAYERID='" & payerid & _
        '   "' where regsampid='" & selectedRegSampID & "'"
        'Proses.ExecuteNonQuery(strupdate)

    End Sub

    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click

        Dim digitresult As Integer
        Dim satuan As String
        Dim nilairujukan As String
        Dim ket As String
        Dim testName As String
        Dim price, price2, price3, price4, price5, price6 As Double
        Dim IdTestGroup As Integer
        Dim analyzertestname As String
        Dim min, minwoman, minchild As String
        Dim max, maxwoman, maxchild As String


        Dim ismanual As String
        If rdoyamanual.Checked = True Then
            ismanual = isTestWithManualEntry '1
        Else
            ismanual = isTestWithInstrumentEntry '0
        End If





        Dim formula As String

        If Trim(txtmin.Text).Length > 0 Then
            min = txtmin.Text
        Else
            min = "0"
        End If
        If Trim(txtmax.Text).Length > 0 Then
            max = txtmax.Text
        Else
            max = "0"
        End If


        If Trim(txtminwoman.Text).Length > 0 Then
            minwoman = txtminwoman.Text
        Else
            minwoman = "0"
        End If

        If Trim(txtmaxwoman.Text).Length > 0 Then
            maxwoman = txtmaxwoman.Text
        Else
            maxwoman = "0"
        End If


        If Trim(txtminchild.Text).Length > 0 Then
            minchild = txtminchild.Text
        Else
            minchild = "0"
        End If

        Dim specimendesc As String
        specimendesc = GetSpecimen()

        If Trim(txtmaxchild.Text).Length > 0 Then
            maxchild = txtmaxchild.Text
        Else
            maxchild = "0"
        End If




        If Trim(txtrelatedformula.Text).Length > 0 Then
            formula = Trim(txtrelatedformula.Text)
        Else
            formula = ""
        End If
        If txtHarga.Text = "" Then
            price = 0
        Else
            price = CDbl(txtHarga.Text)
        End If
        If hargaLevel2.Text = "" Then
            price2 = 0
        Else
            price2 = CDbl(hargaLevel2.Text)
        End If
        If hargaLevel3.Text = "" Then
            price3 = 0
        Else
            price3 = CDbl(hargaLevel3.Text)
        End If
        If hargaLevel4.Text = "" Then

            price4 = 0
        Else
            price4 = CDbl(hargaLevel4.Text)
        End If
        If hargaLevel5.Text = "" Then
            price5 = 0
        Else
            price5 = CDbl(hargaLevel5.Text)
        End If
        If hargaLevel6.Text = "" Then
            price6 = 0
        Else
            price6 = CDbl(hargaLevel6.Text)
        End If
        'sampai disini untuk digit setting
        digitresult = cbdigit.Items(cbdigit.SelectedIndex)

        If txturutan.Text = "" Then
            MessageBox.Show("Silahkan tentukan urutan tampilan test items")
            Exit Sub
        Else
            displayindex = CInt(Trim(txturutan.Text))
        End If

        'Dim ty As Integer
        Dim isrelated As Integer
        If rdofree.Checked = True Then
            isrelated = 0
        ElseIf rdorelated.Checked = True Then
            isrelated = 1
            If txtrelatedformula.Text = "" Then
                MessageBox.Show("Isikan formula yang sesuai")
                Exit Sub
            End If

        End If


        Dim result_type As String
        Dim text_inrange As String
        Dim text_outrange As String


        If cbresultType.Text = Result_type_Text Then
            result_type = cbresultType.Text.Trim
            text_inrange = txtdalamrange.Text
            text_outrange = txtdiluarrange.Text
        ElseIf cbresultType.Text = Result_type_Number Then
            result_type = cbresultType.Text.Trim
            text_inrange = "-"
            text_outrange = "-"
        End If


        'ty = dgt
        If isNew = True Then

            analyzertestname = txtanalyzerTestName.Text
            'cek apakah nama tersebut sudah ada
            If IsNameAvailable(analyzertestname) = True Then
                MessageBox.Show("Test dengan nama tersebut telah tersedia")
                txtanalyzerTestName.Focus()
                Exit Sub
            End If

            satuan = txtSatuan.Text
            nilairujukan = Trim(txtNilaiRujukan.Text)
            ket = Trim(txtKet.Text)
            testName = Trim(txtTestName.Text)




            'sampai disini tgl 18 nov


            IdTestGroup = objTestRelated.GetIdTestGroup(cbTestGroup.Text)
            strsql = "insert into testtype (" &
                        "testname,idtestgroup,satuan,nilairujukan,keterangan,price,analyzertestname,min,max,formula,price2,price3,price4,price5,price6,digit,displayindex,x,y,z,isrelated,minwoman,maxwoman,minchild,maxchild,uf1,uf2,report_display,result_text_inrange,result_text_outrange)" &
                        " VALUES (" &
                         "'" & testName & "','" & IdTestGroup & "','" & satuan & "','" & nilairujukan & "','" & ket & "','" & price & "','" & analyzertestname & "','" & min & "','" & max & "','" & formula & "','" & price2 & "','" & price3 & "','" & price4 & "','" & price5 & "','" & price6 & "','" & digitresult & "','" & displayindex & "','" & cbrelatedX.SelectedValue & "','" & cbrelatedY.SelectedValue & "','" & cbrelatedZ.SelectedValue & "','" & isrelated & "','" & minwoman & "','" & maxwoman & "','" & minchild & "','" & maxchild & "','" & specimendesc & "','" & ismanual & "','" & result_type & "','" & text_inrange & "','" & text_outrange & "')"
            objConn.buildConn()
            objConn.ExecuteNonQuery(strsql)
            isEdit = False
            isNew = False
        ElseIf isEdit = True Then
            'testGroupname = txtTestGroupName.Text

            satuan = Trim(txtSatuan.Text)
            nilairujukan = Trim(txtNilaiRujukan.Text)
            ket = Trim(txtKet.Text)
            price = Trim(txtHarga.Text)
            selectedgroupid = objTestRelated.GetIdTestGroup(cbTestGroup.Text)
            'testName = Trim(txtTestName.Text)
            'analyzertestname = Trim(txtanalyzerTestName.Text)

            'If IsNameAvailableOnUpdate(analyzertestname) = True Then
            ' MessageBox.Show("Test dengan nama tersebut sudah ada")
            ' txtanalyzerTestName.Focus()
            ' Exit Sub
            'End If

            strsql = "update testtype set " &
            "satuan='" & satuan &
            "',nilairujukan='" & nilairujukan &
            "',keterangan='" & ket &
            "',price='" & price &
            "',idtestgroup='" & selectedgroupid &
            "',min='" & min &
            "',max='" & max &
            "',formula='" & formula &
            "',price2='" & price2 &
            "',price3='" & price3 &
            "',price4='" & price4 &
            "',price5='" & price5 &
            "',price6='" & price6 &
            "',digit='" & digitresult &
            "',x='" & cbrelatedX.SelectedValue &
            "',y='" & cbrelatedY.SelectedValue &
            "',z='" & cbrelatedZ.SelectedValue &
            "',isrelated='" & isrelated &
            "',displayindex='" & displayindex &
            "',minwoman='" & minwoman &
            "',maxwoman='" & maxwoman &
            "',minchild='" & minchild &
            "',maxchild='" & maxchild &
            "',uf1='" & specimendesc &
            "',uf2='" & ismanual &
            "',report_display='" & result_type &
            "',result_text_inrange='" & text_inrange &
            "',result_text_outrange='" & text_outrange &
            "' where id='" & selectedtestid & "'"

            ' "',analyzertestname='" & analyzertestname & _

            objConn.buildConn()
            objConn.ExecuteNonQuery(strsql)
            objConn.CloseConn()

            'mantep 2
            Dim ctrl As Control
            For Each ctrl In Panel1.Controls
                If (ctrl.GetType() Is GetType(TextBox)) Then
                    Dim txt As TextBox = CType(ctrl, TextBox)
                    txt.Text = ""
                    txt.Enabled = False 'BackColor = Color.LightYellow
                End If
            Next
            cbTestGroup.Enabled = False
            cbdigit.Enabled = False
            isEdit = False
            isNew = False
        End If

        PopulateDg()
        SaveMnu.Enabled = False
        ClearControl()
    End Sub

    Private Function IsNameAvailable(ByVal AnalyzerTestName As String) As Boolean
        Dim result As Boolean
        Dim str As String
        str = "select count(analyzertestname) as num from testtype where analyzertestname='" & AnalyzerTestName & "'"
        Dim ocek As New clsGreConnect
        ocek.buildConn()

        Dim cmd As New SqlClient.SqlCommand(str, ocek.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            If rdr("num") > 0 Then
                result = True
            Else
                result = False
            End If
        Loop

        rdr.Close()
        cmd.Dispose()
        cmd = Nothing

        ocek.CloseConn()
        ocek = Nothing

        Return result
    End Function

    Private Function IsNameAvailableOnUpdate(ByVal AnalyzerTestName As String) As Boolean
        Dim result As Boolean
        Dim str As String
        str = "select count(analyzertestname) as num from testtype where analyzertestname='" & AnalyzerTestName & "' and id<>'" & selectedtestid & "'"
        Dim ocek As New clsGreConnect
        ocek.buildConn()

        Dim cmd As New SqlClient.SqlCommand(str, ocek.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            If rdr("num") > 0 Then
                result = True
            Else
                result = False
            End If
        Loop

        rdr.Close()
        cmd.Dispose()
        cmd = Nothing

        ocek.CloseConn()
        ocek = Nothing

        Return result
    End Function


    Private Sub cbTestGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbTestGroup.SelectedIndexChanged
        Dim objTestRelated As New TestRelated
        selectedgroupid = objTestRelated.GetIdTestGroup(cbTestGroup.Text)
    End Sub

    Private Sub AddMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddMnu.Click
        isNew = True

        SaveMnu.Enabled = True
        WakeControl()
        'Dim ctrl As Control
        'For Each ctrl In Panel1.Controls
        '    If (ctrl.GetType() Is GetType(TextBox)) Then
        '        Dim txt As TextBox = CType(ctrl, TextBox)
        '        txt.Enabled = True 'BackColor = Color.LightYellow
        '    End If
        'Next
        'cbTestGroup.Enabled = True
        'txtTestName.Focus()


    End Sub
    Private Sub ClearControl()
        Dim ctrl As Control
        For Each ctrl In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.Text = ""
                txt.Enabled = False 'BackColor = Color.LightYellow
            End If
        Next

        Dim tx As Control
        Dim grp As Control
        For Each grp In Panel2.Controls
            If (grp.GetType() Is GetType(GroupBox)) Then
                For Each tx In grp.Controls
                    If (tx.GetType() Is GetType(TextBox)) Then
                        Dim txt As TextBox = CType(tx, TextBox)
                        txt.Text = ""
                        txt.Enabled = False 'BackColor = Color.LightYellow
                    End If
                Next
            End If
        Next

        rdofree.Enabled = False
        rdorelated.Enabled = False
        rdotidak.Enabled = False
        rdoyamanual.Enabled = False
        cbTestGroup.Enabled = False
        cbrelatedX.Enabled = False
        cbrelatedY.Enabled = False
        cbrelatedZ.Enabled = False

        cbspeciment.Enabled = False
    End Sub


    Private Sub WakeControl()
        Dim ctrl As Control
        For Each ctrl In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                '  txt.Text = ""
                txt.Enabled = True 'BackColor = Color.LightYellow
                txt.ReadOnly = False
            End If
        Next

        Dim ctrlcb As Control
        For Each ctrlcb In GroupBox2.Controls
            If (ctrlcb.GetType() Is GetType(ComboBox)) Then
                Dim cb As ComboBox = CType(ctrlcb, ComboBox)
                cb.Enabled = True
            End If
        Next

        cbrelatedX.Enabled = True
        rdofree.Enabled = True
        rdorelated.Enabled = True
        cbspeciment.Enabled = True

        rdotidak.Enabled = True
        rdoyamanual.Enabled = True

        txtmin.Enabled = True
        txtmin.ReadOnly = False
        txtmax.Enabled = True
        txtmax.ReadOnly = False
        txtminwoman.Enabled = True
        txtminwoman.ReadOnly = False
        txtmaxwoman.Enabled = True
        txtmaxwoman.ReadOnly = False
        txtminchild.Enabled = True
        txtminchild.ReadOnly = False
        txtmaxchild.Enabled = True
        txtmaxchild.ReadOnly = False
        txtrelatedformula.Enabled = True
        txtrelatedformula.ReadOnly = False

        cbTestGroup.Enabled = True
        cbdigit.Enabled = True
        ' cbdigit.Text = "2"
        'txturutan.Text = 8
    End Sub

    Private Sub EditMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditMnu.Click

        ' Try
        AddMnu.Enabled = False

        Dim isavailable As Boolean 'top check yang di centang
        isavailable = False
        Dim i As Integer
        For i = 0 To dgTestType.Rows.Count - 1
            'If Not (dgTestType.CurrentRow.IsNewRow) Then
            If dgTestType.Item("chk", i).Value = True Then
                isavailable = True
            End If
        Next
        If isavailable = False Then
            MessageBox.Show("Pilih salah satu test item untuk diedit")
            Exit Sub
        End If


        isEdit = True
        isNew = False
        WakeControl()
        SaveMnu.Enabled = True

        For i = 0 To dgTestType.Rows.Count - 1
            If dgTestType.Item("chk", i).Value = True Then
                If Not (dgTestType.CurrentRow.IsNewRow) Then
                    If Not (IsDBNull(dgTestType.Item("id", i).Value)) Then
                        'selectedtestGroup = dgTestGroup.Item("id", i).Value
                        txtTestName.Text = dgTestType.Item("testname", i).Value
                        txtTestName.ReadOnly = True

                        txtSatuan.Text = dgTestType.Item("satuan", i).Value
                        txtanalyzerTestName.Text = dgTestType.Item("analyzertestname", i).Value
                        txtanalyzerTestName.ReadOnly = True

                        txtNilaiRujukan.Text = dgTestType.Item("nilairujukan", i).Value
                        txtKet.Text = dgTestType.Item("keterangan", i).Value
                        txtHarga.Text = dgTestType.Item("price", i).Value
                        cbTestGroup.Enabled = True
                        cbTestGroup.Text = ""
                        cbTestGroup.SelectedText = cbGroupChoice.Text

                        cbrelatedX.Enabled = True
                        cbrelatedY.Enabled = True
                        cbrelatedZ.Enabled = True

                        
                    End If
                End If


            End If
        Next

        
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub PopulateDg()
        Dim strsql As String
        If cbGroupChoice.Text <> "SEMUA TEST" Then
            strsql = "Select * from testtype where idtestgroup='" & selectedgroupid & "' order by testname asc"
        ElseIf cbGroupChoice.Text = "SEMUA TEST" Then
            strsql = "Select * from testtype" ' where idtestgroup='" & selectedgroupid & "' order by testname asc"
        End If


        objConn.buildConn()

        tblGroupTest = New DataTable
        tblGroupTest = objConn.ExecuteQuery(strsql) '"Select * from testtype where idtestgroup='" & selectedgroupid & "' order by testname asc")

        dgTestType.AllowUserToAddRows = False
        dgTestType.RowHeadersVisible = False

        'dgTestType = New DataGridView
        dgTestType.DataSource = tblGroupTest
        Dim j As Integer
        For j = 0 To dgTestType.Columns.Count - 1
            dgTestType.Columns(j).Visible = False
        Next

        If Not dgTestType.Columns.Contains("chk") Then
            Dim chk As New DataGridViewCheckBoxColumn()

            dgTestType.Columns.Add(chk)
            chk.HeaderText = "SELECT"
            chk.Width = 60
            chk.Name = "chk"

            dgTestType.Columns("chk").DisplayIndex = 0
            dgTestType.Columns("chk").Name = "chk"
        End If

        dgTestType.Columns("chk").Visible = True
        dgTestType.Columns("testname").Visible = True
        dgTestType.Columns("testname").HeaderText = "TEST ITEM"
        dgTestType.Columns("testname").Width = 180
        dgTestType.Columns("testname").ReadOnly = True

        dgTestType.Columns("analyzertestname").Visible = True
        dgTestType.Columns("analyzertestname").HeaderText = "ANALYZER TEST NAME"
        dgTestType.Columns("analyzertestname").Width = 130
        dgTestType.Columns("analyzertestname").ReadOnly = True
        'dgTestType.Columns("id").Visible = False
        'DGPack.Columns("packname").Width = 250




        objConn.CloseConn()


    End Sub

    Private Sub dgTestType_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgTestType.CellContentClick
        'If Not (dgTestType.CurrentRow.IsNewRow) Then
        ClearControl()
        If Not e.RowIndex = -1 Then 'untuk detect bahwa bukan header
            If Not IsDBNull(dgTestType.Item("id", e.RowIndex).Value) Then
                If TypeOf dgTestType.Rows(e.RowIndex).Cells(e.ColumnIndex) Is DataGridViewCheckBoxCell Then
                    Dim dgvCheckBoxCell As DataGridViewCheckBoxCell = dgTestType.Rows(e.RowIndex).Cells(e.ColumnIndex)
                    'Commit the data to the datasouce.
                    dgTestType.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    Dim checked As Boolean = CType(dgvCheckBoxCell.Value, Boolean)
                    selectedtestid = dgTestType.Item("id", e.RowIndex).Value
                    'bagus
                    'MessageBox.Show(e.RowIndex & " " & checked & " " & e.ColumnIndex)

                    If checked Then
                        Dim i As Integer
                        For i = 0 To dgTestType.Rows.Count - 1
                            If i <> e.RowIndex Then
                                dgTestType.Item("chk", i).Value = False
                            End If
                        Next

                    End If





                End If
            End If
            ' End If

            WakeControl()
            'SaveMnu.Enabled = false

            For i = 0 To dgTestType.Rows.Count - 1
                If dgTestType.Item("chk", i).Value = True Then
                    If Not (dgTestType.CurrentRow.IsNewRow) Then
                        If Not (IsDBNull(dgTestType.Item("id", i).Value)) Then
                            'selectedtestGroup = dgTestGroup.Item("id", i).Value
                            '======== speciment desc
                            If Not IsDBNull(dgTestType.Item("uf1", i).Value) Then
                                'txtTestName.Text = dgTestType.Item("testname", i).Value
                                If dgTestType.Item("uf1", i).Value = "1" Then
                                    cbspeciment.SelectedIndex = cbspeciment.FindStringExact("Serum")
                                End If

                                If dgTestType.Item("uf1", i).Value = "2" Then
                                    cbspeciment.SelectedIndex = cbspeciment.FindStringExact("Urine")
                                End If



                                If dgTestType.Item("uf1", i).Value = "3" Then
                                    cbspeciment.SelectedIndex = cbspeciment.FindStringExact("Plasma")
                                End If

                                If dgTestType.Item("uf1", i).Value = "4" Then
                                    cbspeciment.SelectedIndex = cbspeciment.FindStringExact("Gastric Juice")
                                End If

                                If dgTestType.Item("uf1", i).Value = "5" Then
                                    cbspeciment.SelectedIndex = cbspeciment.FindStringExact("Ascites")
                                End If

                                If dgTestType.Item("uf1", i).Value = "6" Then
                                    cbspeciment.SelectedIndex = cbspeciment.FindStringExact("CSF")
                                End If

                                If dgTestType.Item("uf1", i).Value = "7" Then
                                    cbspeciment.SelectedIndex = cbspeciment.FindStringExact("Whole blood")
                                End If

                                If dgTestType.Item("uf1", i).Value = "8" Then
                                    cbspeciment.SelectedIndex = cbspeciment.FindStringExact("Feses")
                                End If

                                If dgTestType.Item("uf1", i).Value = "9" Then
                                    cbspeciment.SelectedIndex = cbspeciment.FindStringExact("Other")
                                End If
                            End If
                            cbspeciment.Enabled = False
                            '========
                            If Not IsDBNull(dgTestType.Item("uf2", i).Value) Then
                                If dgTestType.Item("uf2", i).Value = "1" Then
                                    'kalo 1 berarti manual
                                    rdoyamanual.Checked = True
                                Else
                                    rdoyamanual.Checked = True

                                End If
                            Else
                                rdotidak.Checked = True
                            End If
                            rdotidak.Enabled = False
                            rdoyamanual.Enabled = False


                            '=========
                            If Not IsDBNull(dgTestType.Item("testname", i).Value) Then
                                txtTestName.Text = dgTestType.Item("testname", i).Value

                            End If
                            txtTestName.ReadOnly = True
                            If Not IsDBNull(dgTestType.Item("satuan", i).Value) Then
                                txtSatuan.Text = dgTestType.Item("satuan", i).Value

                            End If
                            txtSatuan.ReadOnly = True
                            If Not IsDBNull(dgTestType.Item("analyzertestname", i).Value) Then
                                txtanalyzerTestName.Text = dgTestType.Item("analyzertestname", i).Value

                            End If
                            txtanalyzerTestName.ReadOnly = True
                            If Not IsDBNull(dgTestType.Item("nilairujukan", i).Value) Then
                                txtNilaiRujukan.Text = dgTestType.Item("nilairujukan", i).Value

                            End If
                            txtNilaiRujukan.ReadOnly = True
                            If Not IsDBNull(dgTestType.Item("keterangan", i).Value) Then
                                txtKet.Text = dgTestType.Item("keterangan", i).Value

                            End If
                            txtKet.ReadOnly = True
                            'price show
                            If Not IsDBNull(dgTestType.Item("price", i).Value) Then
                                txtHarga.Text = dgTestType.Item("price", i).Value

                            End If
                            txtHarga.ReadOnly = True

                            If Not IsDBNull(dgTestType.Item("price2", i).Value) Then
                                hargaLevel2.Text = dgTestType.Item("price2", i).Value

                            End If
                            hargaLevel2.ReadOnly = True

                            If Not IsDBNull(dgTestType.Item("price3", i).Value) Then
                                hargaLevel3.Text = dgTestType.Item("price3", i).Value

                            End If
                            hargaLevel3.ReadOnly = True

                            If Not IsDBNull(dgTestType.Item("price4", i).Value) Then
                                hargaLevel4.Text = dgTestType.Item("price4", i).Value

                            End If
                            hargaLevel4.ReadOnly = True

                            If Not IsDBNull(dgTestType.Item("price5", i).Value) Then
                                hargaLevel5.Text = dgTestType.Item("price5", i).Value

                            End If
                            hargaLevel5.ReadOnly = True


                            If Not IsDBNull(dgTestType.Item("price6", i).Value) Then
                                hargaLevel6.Text = dgTestType.Item("price6", i).Value

                            End If
                            hargaLevel6.ReadOnly = True
                            '--digit
                            'Dim dg As Integer
                            'dg = dgTestType.Item("digit", i).Value
                            cbdigit.Enabled = True
                            If Not IsDBNull(dgTestType.Item("digit", i).Value) Then
                                cbdigit.Text = dgTestType.Item("digit", i).Value
                            Else
                                cbdigit.Text = "0"
                            End If
                            cbdigit.Enabled = False

                            If Not IsDBNull(dgTestType.Item("displayindex", i).Value) Then
                                txturutan.Text = dgTestType.Item("displayindex", i).Value
                            Else
                                txturutan.Text = 100
                            End If
                            txturutan.Enabled = False




                            If Not IsDBNull(dgTestType.Item("min", i).Value) Then
                                txtmin.Text = dgTestType.Item("min", i).Value

                            End If
                            txtmin.ReadOnly = True
                            If Not IsDBNull(dgTestType.Item("max", i).Value) Then
                                txtmax.Text = dgTestType.Item("max", i).Value
                            End If
                            txtmax.ReadOnly = True

                            If Not IsDBNull(dgTestType.Item("minchild", i).Value) Then
                                txtminchild.Text = dgTestType.Item("minchild", i).Value
                            End If
                            txtminchild.ReadOnly = True

                            If Not IsDBNull(dgTestType.Item("maxchild", i).Value) Then
                                txtmaxchild.Text = dgTestType.Item("maxchild", i).Value
                            End If
                            txtmaxchild.ReadOnly = True

                            If Not IsDBNull(dgTestType.Item("minwoman", i).Value) Then
                                txtminwoman.Text = dgTestType.Item("minwoman", i).Value
                            End If
                            txtminwoman.ReadOnly = True

                            If Not IsDBNull(dgTestType.Item("maxwoman", i).Value) Then
                                txtmaxwoman.Text = dgTestType.Item("maxwoman", i).Value
                            End If
                            txtmaxwoman.ReadOnly = True


                            If Not IsDBNull(dgTestType.Item("formula", i).Value) Then
                                txtrelatedformula.Text = dgTestType.Item("formula", i).Value
                            End If
                            txtrelatedformula.Enabled = False

                            Dim index As Integer
                            cbTestGroup.Enabled = True
                            'cbTestGroup.SelectedText = cbTestGroup.FindString(Trim(GetGroupName(dgTestType.Item("id", i).Value)))
                            index = cbTestGroup.FindString(Trim(GetGroupName(dgTestType.Item("idtestgroup", i).Value)))
                            cbTestGroup.SelectedIndex = index
                            cbTestGroup.Enabled = False

                            If Not IsDBNull(dgTestType.Item("x", i).Value) Then
                                If dgTestType.Item("x", i).Value <> "" Then
                                    cbrelatedX.SelectedValue = dgTestType.Item("x", i).Value
                                End If

                            End If


                            If Not IsDBNull(dgTestType.Item("y", i).Value) Then
                                If dgTestType.Item("y", i).Value <> "" Then
                                    cbrelatedY.SelectedValue = dgTestType.Item("y", i).Value
                                End If
                            End If


                            If Not IsDBNull(dgTestType.Item("z", i).Value) Then
                                If dgTestType.Item("z", i).Value <> "" Then
                                    cbrelatedZ.SelectedValue = dgTestType.Item("z", i).Value
                                End If

                            End If


                            If Not IsDBNull(dgTestType.Item("isrelated", i).Value) Then
                                Dim related As Integer
                                related = CInt(dgTestType.Item("isrelated", i).Value)
                                If related = 1 Then
                                    rdorelated.Checked = True
                                    rdofree.Checked = False
                                ElseIf related = 0 Then
                                    rdofree.Checked = True
                                    rdorelated.Checked = False
                                End If
                            Else
                                rdofree.Checked = True
                            End If
                            rdofree.Enabled = False
                            txtrelatedformula.Enabled = False
                            rdorelated.Enabled = False
                            cbrelatedX.Enabled = False
                            cbrelatedY.Enabled = False
                            cbrelatedZ.Enabled = False

                            If Not IsDBNull(dgTestType.Item("report_display", i).Value) Then
                                If dgTestType.Item("report_display", i).Value = Result_type_Text Then
                                    cbresultType.Text = Result_type_Text
                                    txtdalamrange.Enabled = True
                                    txtdalamrange.Text = IIf(IsDBNull(dgTestType.Item("result_text_inrange", i)), "", dgTestType.Item("result_text_inrange", i).Value)
                                    txtdiluarrange.Enabled = True
                                    txtdiluarrange.Text = IIf(IsDBNull(dgTestType.Item("result_text_outrange", i)), "", dgTestType.Item("result_text_outrange", i).Value)
                                    txtdalamrange.ReadOnly = True
                                    txtdiluarrange.ReadOnly = True
                                ElseIf dgTestType.Item("report_display", i).Value = Result_type_Number Then
                                    cbresultType.Text = Result_type_Number
                                    txtdalamrange.Enabled = True
                                    txtdiluarrange.Enabled = True
                                    txtdalamrange.Text = ""
                                    txtdiluarrange.Text = ""
                                    txtdalamrange.ReadOnly = True
                                    txtdiluarrange.ReadOnly = True

                                End If
                            End If
                        End If
                    End If
                End If
            Next


        End If
    End Sub
    Private Function GetGroupName(ByVal idgroup As Integer) As String
        Dim strsql As String
        Dim greObject As New clsGreConnect
        Dim result As String
        strsql = "select id,testgroupname from testgroup where id='" & idgroup & "'"
        greObject.buildConn()

        Dim thecommand As New SqlClient.SqlCommand(strsql, greObject.grecon)
        Dim testTypeReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)

        Do While testTypeReader.Read
            result = testTypeReader("testgroupname")
        Loop
        Return result
    End Function

    Private Sub DeleteMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteMnu.Click
        Dim i As Integer
        Dim isavailable As Boolean
        isavailable = False
        For i = 0 To dgTestType.Rows.Count - 1
            If dgTestType.Item("chk", i).Value = True Then
                strsql = "delete from testtype " & _
                "where id='" & selectedtestid & "'"
                isavailable = True
                objConn.buildConn()
                objConn.ExecuteNonQuery(strsql)
                objConn.CloseConn()
            End If
        Next
        If isavailable = False Then
            MessageBox.Show("Tidak ada data yang dipilih")
        End If

        PopulateDg()
    End Sub

    Private Sub cancelMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelMnu.Click
        'Dim ctrl As Control
        'For Each ctrl In Panel1.Controls
        '    If (ctrl.GetType() Is GetType(TextBox)) Then
        '        Dim txt As TextBox = CType(ctrl, TextBox)
        '        txt.Text = ""
        '        txt.Enabled = False 'BackColor = Color.LightYellow
        '    End If
        'Next
        'cbTestGroup.Enabled = False
        SaveMnu.Enabled = False
        AddMnu.Enabled = True
        isEdit = False
        isNew = False

        ClearControl()
        PopulateDg()
        txtrelatedformula.Enabled = False
    End Sub

    Private Sub txtHarga_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtHarga.KeyPress, hargaLevel2.KeyPress, hargaLevel3.KeyPress, hargaLevel4.KeyPress, hargaLevel5.KeyPress, hargaLevel6.KeyPress, txturutan.KeyPress
        If Char.IsDigit(e.KeyChar) = False And Char.IsControl(e.KeyChar) = False Then
            e.Handled = True
            MsgBox("Masukkan Angka") 'bagus
        End If
    End Sub


    Private Sub Label15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim objFrmContoh As New ContohFrm
        'objFrmContoh.StartPosition = FormStartPosition.Manual
        'objFrmContoh.Show()
    End Sub

    Private Sub Label15_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Cursor = Cursors.Hand
    End Sub

    Private Sub Label15_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Cursor = Cursors.Default
    End Sub

    Private tips As New ToolTip()

    Private Sub txtanalyzerTestName_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtanalyzerTestName.MouseClick
        tips.Show("Nama test pendek sesuai instrument: ALB", txtTestName, 3000)
    End Sub
   
    
    Private Sub txtTestName_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtTestName.MouseClick
        tips.Show("Nama test panjang misal: Albumin", txtTestName, 3000)
    End Sub

    Private Sub txtNilaiRujukan_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtNilaiRujukan.MouseClick
        tips.Show("Rujukan yang tercetak di laporan", txtTestName, 3000)
    End Sub

   
   
    Private Sub cbdigit_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbdigit.SelectedIndexChanged
        dgt = cbdigit.Items(cbdigit.SelectedIndex)
    End Sub

    Private Sub txtHarga_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtHarga.TextChanged

    End Sub

    Private Sub showurutan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles showurutan.Click
        Dim objdisplay As New ShowItemDisplayIndexFrm
        objdisplay.ShowDialog()
    End Sub

    Private Sub showurutan_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles showurutan.MouseHover
        Me.Cursor = Cursors.Hand

    End Sub

    Private Sub showurutan_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles showurutan.MouseLeave
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub txturutan_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txturutan.TextChanged

    End Sub

    Private Sub hargaLevel2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hargaLevel2.TextChanged

    End Sub

    Private Sub cbrelatedX_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbrelatedX.SelectedIndexChanged

    End Sub

    Private Sub cbrelatedY_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbrelatedY.SelectedIndexChanged

    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub rdofree_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdofree.CheckedChanged
        If rdofree.Checked = True Then
            cbrelatedX.Enabled = False
            cbrelatedY.Enabled = False
            cbrelatedZ.Enabled = False
            txtrelatedformula.Enabled = False
        End If
        

    End Sub

    Private Sub rdorelated_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdorelated.CheckedChanged
        If rdorelated.Checked = True Then
            cbrelatedX.Enabled = True
            cbrelatedY.Enabled = True
            cbrelatedZ.Enabled = True
            txtrelatedformula.Enabled = True
        End If
    End Sub

    Private Sub txtTestName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTestName.TextChanged

    End Sub
    ReadOnly AllowedFormulaKeys As String = "0123456789XYZxyz-+*/.,"
    Private Sub txtrelatedformula_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtrelatedformula.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedFormulaKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedFormulaKeys.Contains(e.KeyChar)

        End Select
    End Sub
    ReadOnly AllowedMaxMin As String = "0123456789.,"
    Private Sub txtmax_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtmax.KeyPress, txtmaxwoman.KeyPress, txtmaxchild.KeyPress, txtmin.KeyPress, txtminchild.KeyPress, txtminwoman.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedMaxMin.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedMaxMin.Contains(e.KeyChar)

        End Select
    End Sub

   
   
    Private Sub lblguide_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblguide.Click
        Dim objguide As New displayGuideTest
        objguide.Show()
    End Sub

    Private Sub cbspeciment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbspeciment.SelectedIndexChanged

    End Sub

    Private Sub rdoyamanual_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoyamanual.CheckedChanged
        If rdoyamanual.Checked = True Then
            rdofree.Enabled = True
            rdofree.Checked = True
            rdorelated.Checked = False

        End If
    End Sub

    Private Sub cbresultType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbresultType.SelectedIndexChanged
        If cbresultType.Text = Result_type_Number Then
            txtdalamrange.Text = ""
            txtdiluarrange.Text = ""
            txtdalamrange.Enabled = False
            txtdiluarrange.Enabled = False
            'test

        End If
    End Sub
End Class
