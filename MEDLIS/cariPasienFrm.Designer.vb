﻿'
' Created by SharpDevelop.
' User: User
' Date: 10/13/2014
' Time: 1:28 PM
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Partial Class cariPasienFrm
	Inherits System.Windows.Forms.Form
	
	''' <summary>
	''' Designer variable used to keep track of non-visual components.
	''' </summary>
	Private components As System.ComponentModel.IContainer
	
	''' <summary>
	''' Disposes resources used by the form.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If components IsNot Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub
	
	''' <summary>
	''' This method is required for Windows Forms designer support.
	''' Do not change the method contents inside the source code editor. The Forms designer might
	''' not be able to load this method if it was changed manually.
	''' </summary>
	Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(cariPasienFrm))
        Me.label6 = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.label4 = New System.Windows.Forms.Label()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.label3 = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        Me.txtIdPasien = New System.Windows.Forms.TextBox()
        Me.txtAlamat = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtNamaPasien = New System.Windows.Forms.TextBox()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.AddMnu = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.SaveMnu = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.EditMnu = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.DeleteMnu = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.cancelMnu = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuRegistration = New System.Windows.Forms.ToolStripButton()
        Me.cbFinder = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.dpBirth = New System.Windows.Forms.DateTimePicker()
        Me.rdoPerempuan = New System.Windows.Forms.RadioButton()
        Me.rdoLaki = New System.Windows.Forms.RadioButton()
        Me.dgPasien = New System.Windows.Forms.DataGridView()
        Me.btnCari = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmdshowage = New System.Windows.Forms.Button()
        Me.lbldisplayumur = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.rdoanak = New System.Windows.Forms.RadioButton()
        Me.cbtipepasien = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dpfind = New System.Windows.Forms.DateTimePicker()
        Me.txtFinder = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtnip = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtjabatan = New System.Windows.Forms.TextBox()
        Me.cbinstansi = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.dgPasien, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'label6
        '
        Me.label6.AutoSize = True
        Me.label6.Location = New System.Drawing.Point(15, 41)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(96, 15)
        Me.label6.TabIndex = 32
        Me.label6.Text = "Tanggal Lahir"
        Me.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(163, 109)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(65, 27)
        Me.btnCancel.TabIndex = 12
        Me.btnCancel.Text = "Clear"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'label4
        '
        Me.label4.AutoSize = True
        Me.label4.Location = New System.Drawing.Point(63, 184)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(48, 15)
        Me.label4.TabIndex = 25
        Me.label4.Text = "Phone"
        Me.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPhone
        '
        Me.txtPhone.Location = New System.Drawing.Point(124, 182)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(160, 21)
        Me.txtPhone.TabIndex = 6
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Location = New System.Drawing.Point(60, 155)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(51, 15)
        Me.label3.TabIndex = 22
        Me.label3.Text = "Alamat"
        Me.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(63, 72)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(45, 15)
        Me.label2.TabIndex = 21
        Me.label2.Text = "Nama"
        Me.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtIdPasien
        '
        Me.txtIdPasien.Location = New System.Drawing.Point(124, 8)
        Me.txtIdPasien.Name = "txtIdPasien"
        Me.txtIdPasien.Size = New System.Drawing.Size(142, 21)
        Me.txtIdPasien.TabIndex = 0
        '
        'txtAlamat
        '
        Me.txtAlamat.Location = New System.Drawing.Point(124, 152)
        Me.txtAlamat.Name = "txtAlamat"
        Me.txtAlamat.Size = New System.Drawing.Size(426, 21)
        Me.txtAlamat.TabIndex = 5
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Location = New System.Drawing.Point(37, 15)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(74, 15)
        Me.label1.TabIndex = 18
        Me.label1.Text = "ID PASIEN"
        Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNamaPasien
        '
        Me.txtNamaPasien.Location = New System.Drawing.Point(124, 69)
        Me.txtNamaPasien.Name = "txtNamaPasien"
        Me.txtNamaPasien.Size = New System.Drawing.Size(311, 21)
        Me.txtNamaPasien.TabIndex = 1
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.BackColor = System.Drawing.SystemColors.Control
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddMnu, Me.ToolStripSeparator1, Me.SaveMnu, Me.ToolStripSeparator2, Me.EditMnu, Me.ToolStripSeparator3, Me.DeleteMnu, Me.ToolStripSeparator4, Me.cancelMnu, Me.ToolStripSeparator5, Me.mnuRegistration})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Padding = New System.Windows.Forms.Padding(10, 0, 2, 0)
        Me.ToolStrip1.Size = New System.Drawing.Size(1117, 46)
        Me.ToolStrip1.TabIndex = 34
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'AddMnu
        '
        Me.AddMnu.Image = Global.MEDLIS.My.Resources.Resources.plus
        Me.AddMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.AddMnu.Name = "AddMnu"
        Me.AddMnu.Size = New System.Drawing.Size(65, 43)
        Me.AddMnu.Text = "Add"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 46)
        '
        'SaveMnu
        '
        Me.SaveMnu.Image = Global.MEDLIS.My.Resources.Resources.save
        Me.SaveMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SaveMnu.Name = "SaveMnu"
        Me.SaveMnu.Size = New System.Drawing.Size(67, 43)
        Me.SaveMnu.Text = "Save"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 46)
        '
        'EditMnu
        '
        Me.EditMnu.Image = Global.MEDLIS.My.Resources.Resources.write_edit
        Me.EditMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.EditMnu.Name = "EditMnu"
        Me.EditMnu.Size = New System.Drawing.Size(63, 43)
        Me.EditMnu.Text = "Edit"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 46)
        '
        'DeleteMnu
        '
        Me.DeleteMnu.AutoSize = False
        Me.DeleteMnu.Image = Global.MEDLIS.My.Resources.Resources.cut
        Me.DeleteMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.DeleteMnu.Name = "DeleteMnu"
        Me.DeleteMnu.Size = New System.Drawing.Size(82, 37)
        Me.DeleteMnu.Text = "Delete"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 46)
        '
        'cancelMnu
        '
        Me.cancelMnu.AutoSize = False
        Me.cancelMnu.Image = Global.MEDLIS.My.Resources.Resources.cancel
        Me.cancelMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cancelMnu.Name = "cancelMnu"
        Me.cancelMnu.Size = New System.Drawing.Size(85, 37)
        Me.cancelMnu.Text = "Cancel"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 46)
        '
        'mnuRegistration
        '
        Me.mnuRegistration.AutoSize = False
        Me.mnuRegistration.Image = Global.MEDLIS.My.Resources.Resources.document_write
        Me.mnuRegistration.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuRegistration.Name = "mnuRegistration"
        Me.mnuRegistration.Size = New System.Drawing.Size(102, 37)
        Me.mnuRegistration.Text = "Registration"
        '
        'cbFinder
        '
        Me.cbFinder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbFinder.FormattingEnabled = True
        Me.cbFinder.Location = New System.Drawing.Point(107, 20)
        Me.cbFinder.Name = "cbFinder"
        Me.cbFinder.Size = New System.Drawing.Size(233, 23)
        Me.cbFinder.TabIndex = 8
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(315, 191)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(44, 15)
        Me.Label7.TabIndex = 53
        Me.Label7.Text = "Email"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(373, 185)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(177, 21)
        Me.txtEmail.TabIndex = 7
        '
        'dpBirth
        '
        Me.dpBirth.Location = New System.Drawing.Point(124, 36)
        Me.dpBirth.Name = "dpBirth"
        Me.dpBirth.Size = New System.Drawing.Size(251, 21)
        Me.dpBirth.TabIndex = 4
        '
        'rdoPerempuan
        '
        Me.rdoPerempuan.AutoSize = True
        Me.rdoPerempuan.Location = New System.Drawing.Point(228, 99)
        Me.rdoPerempuan.Name = "rdoPerempuan"
        Me.rdoPerempuan.Size = New System.Drawing.Size(99, 19)
        Me.rdoPerempuan.TabIndex = 3
        Me.rdoPerempuan.TabStop = True
        Me.rdoPerempuan.Text = "Perempuan"
        Me.rdoPerempuan.UseVisualStyleBackColor = True
        '
        'rdoLaki
        '
        Me.rdoLaki.AutoSize = True
        Me.rdoLaki.Location = New System.Drawing.Point(124, 99)
        Me.rdoLaki.Name = "rdoLaki"
        Me.rdoLaki.Size = New System.Drawing.Size(83, 19)
        Me.rdoLaki.TabIndex = 2
        Me.rdoLaki.TabStop = True
        Me.rdoLaki.Text = "Laki Laki"
        Me.rdoLaki.UseVisualStyleBackColor = True
        '
        'dgPasien
        '
        Me.dgPasien.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgPasien.Location = New System.Drawing.Point(12, 306)
        Me.dgPasien.Name = "dgPasien"
        Me.dgPasien.Size = New System.Drawing.Size(1091, 325)
        Me.dgPasien.TabIndex = 13
        '
        'btnCari
        '
        Me.btnCari.Location = New System.Drawing.Point(234, 109)
        Me.btnCari.Name = "btnCari"
        Me.btnCari.Size = New System.Drawing.Size(106, 27)
        Me.btnCari.TabIndex = 11
        Me.btnCari.Text = "Cari"
        Me.btnCari.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.cbinstansi)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.txtjabatan)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.txtnip)
        Me.Panel1.Controls.Add(Me.cmdshowage)
        Me.Panel1.Controls.Add(Me.lbldisplayumur)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.rdoanak)
        Me.Panel1.Controls.Add(Me.cbtipepasien)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txtEmail)
        Me.Panel1.Controls.Add(Me.dpBirth)
        Me.Panel1.Controls.Add(Me.rdoPerempuan)
        Me.Panel1.Controls.Add(Me.rdoLaki)
        Me.Panel1.Controls.Add(Me.label6)
        Me.Panel1.Controls.Add(Me.label4)
        Me.Panel1.Controls.Add(Me.txtPhone)
        Me.Panel1.Controls.Add(Me.label3)
        Me.Panel1.Controls.Add(Me.label2)
        Me.Panel1.Controls.Add(Me.txtIdPasien)
        Me.Panel1.Controls.Add(Me.txtAlamat)
        Me.Panel1.Controls.Add(Me.label1)
        Me.Panel1.Controls.Add(Me.txtNamaPasien)
        Me.Panel1.Location = New System.Drawing.Point(12, 49)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1091, 251)
        Me.Panel1.TabIndex = 55
        '
        'cmdshowage
        '
        Me.cmdshowage.Location = New System.Drawing.Point(395, 124)
        Me.cmdshowage.Name = "cmdshowage"
        Me.cmdshowage.Size = New System.Drawing.Size(155, 23)
        Me.cmdshowage.TabIndex = 59
        Me.cmdshowage.Text = "Tunjukkan Umur"
        Me.cmdshowage.UseVisualStyleBackColor = True
        '
        'lbldisplayumur
        '
        Me.lbldisplayumur.AutoSize = True
        Me.lbldisplayumur.Location = New System.Drawing.Point(124, 121)
        Me.lbldisplayumur.Name = "lbldisplayumur"
        Me.lbldisplayumur.Size = New System.Drawing.Size(19, 15)
        Me.lbldisplayumur.TabIndex = 58
        Me.lbldisplayumur.Text = ".. "
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(69, 121)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(42, 15)
        Me.Label8.TabIndex = 57
        Me.Label8.Text = "Umur"
        '
        'rdoanak
        '
        Me.rdoanak.AutoSize = True
        Me.rdoanak.Location = New System.Drawing.Point(344, 99)
        Me.rdoanak.Name = "rdoanak"
        Me.rdoanak.Size = New System.Drawing.Size(91, 19)
        Me.rdoanak.TabIndex = 56
        Me.rdoanak.TabStop = True
        Me.rdoanak.Text = "Anak anak"
        Me.rdoanak.UseVisualStyleBackColor = True
        '
        'cbtipepasien
        '
        Me.cbtipepasien.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbtipepasien.FormattingEnabled = True
        Me.cbtipepasien.Location = New System.Drawing.Point(124, 214)
        Me.cbtipepasien.Name = "cbtipepasien"
        Me.cbtipepasien.Size = New System.Drawing.Size(286, 23)
        Me.cbtipepasien.TabIndex = 55
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(26, 217)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(85, 15)
        Me.Label5.TabIndex = 54
        Me.Label5.Text = "Type Pasien"
        '
        'dpfind
        '
        Me.dpfind.Location = New System.Drawing.Point(107, 84)
        Me.dpfind.Name = "dpfind"
        Me.dpfind.Size = New System.Drawing.Size(233, 21)
        Me.dpfind.TabIndex = 10
        '
        'txtFinder
        '
        Me.txtFinder.Location = New System.Drawing.Point(107, 54)
        Me.txtFinder.Name = "txtFinder"
        Me.txtFinder.Size = New System.Drawing.Size(233, 21)
        Me.txtFinder.TabIndex = 9
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(716, 13)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(65, 15)
        Me.Label9.TabIndex = 63
        Me.Label9.Text = "NIP / NIK"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtnip
        '
        Me.txtnip.Location = New System.Drawing.Point(790, 12)
        Me.txtnip.Name = "txtnip"
        Me.txtnip.Size = New System.Drawing.Size(286, 21)
        Me.txtnip.TabIndex = 61
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(723, 44)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(58, 15)
        Me.Label11.TabIndex = 65
        Me.Label11.Text = "Jabatan"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtjabatan
        '
        Me.txtjabatan.Location = New System.Drawing.Point(790, 42)
        Me.txtjabatan.Name = "txtjabatan"
        Me.txtjabatan.Size = New System.Drawing.Size(286, 21)
        Me.txtjabatan.TabIndex = 64
        '
        'cbinstansi
        '
        Me.cbinstansi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbinstansi.FormattingEnabled = True
        Me.cbinstansi.Location = New System.Drawing.Point(790, 69)
        Me.cbinstansi.Name = "cbinstansi"
        Me.cbinstansi.Size = New System.Drawing.Size(286, 23)
        Me.cbinstansi.TabIndex = 67
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(724, 73)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(57, 15)
        Me.Label10.TabIndex = 66
        Me.Label10.Text = "Instansi"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbFinder)
        Me.GroupBox1.Controls.Add(Me.btnCari)
        Me.GroupBox1.Controls.Add(Me.btnCancel)
        Me.GroupBox1.Controls.Add(Me.txtFinder)
        Me.GroupBox1.Controls.Add(Me.dpfind)
        Me.GroupBox1.Location = New System.Drawing.Point(719, 99)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(357, 149)
        Me.GroupBox1.TabIndex = 68
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Cari Data Pasien"
        '
        'cariPasienFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1117, 643)
        Me.Controls.Add(Me.dgPasien)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "cariPasienFrm"
        Me.Text = "Pasien"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.dgPasien, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtNamaPasien As System.Windows.Forms.TextBox
    Private label1 As System.Windows.Forms.Label
    Friend WithEvents txtAlamat As System.Windows.Forms.TextBox
    Friend WithEvents txtIdPasien As System.Windows.Forms.TextBox
    Private label2 As System.Windows.Forms.Label
    Private label3 As System.Windows.Forms.Label
    Friend WithEvents txtPhone As System.Windows.Forms.TextBox
    Private label4 As System.Windows.Forms.Label
    Private WithEvents btnCari As System.Windows.Forms.Button
    Private WithEvents btnCancel As System.Windows.Forms.Button
    Private label6 As System.Windows.Forms.Label
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents AddMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents SaveMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents EditMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents DeleteMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents cancelMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents cbFinder As System.Windows.Forms.ComboBox
    Private WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Private WithEvents dpBirth As System.Windows.Forms.DateTimePicker
    Private WithEvents rdoPerempuan As System.Windows.Forms.RadioButton
    Private WithEvents rdoLaki As System.Windows.Forms.RadioButton
    Friend WithEvents dgPasien As System.Windows.Forms.DataGridView
    Friend WithEvents mnuRegistration As System.Windows.Forms.ToolStripButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtFinder As System.Windows.Forms.TextBox
    Friend WithEvents dpfind As System.Windows.Forms.DateTimePicker
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cbtipepasien As System.Windows.Forms.ComboBox
    Friend WithEvents rdoanak As System.Windows.Forms.RadioButton
    Friend WithEvents lbldisplayumur As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmdshowage As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents cbinstansi As ComboBox
    Friend WithEvents Label10 As Label
    Private WithEvents Label11 As Label
    Friend WithEvents txtjabatan As TextBox
    Private WithEvents Label9 As Label
    Friend WithEvents txtnip As TextBox
End Class
