﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainFrm))
        Me.menuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.loginMnu = New System.Windows.Forms.ToolStripMenuItem()
        Me.pasienMnu = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasienPendaftaranToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PengambilanSampleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.pembayaranMnu = New System.Windows.Forms.ToolStripMenuItem()
        Me.jobMnu = New System.Windows.Forms.ToolStripMenuItem()
        Me.TestStatusToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FinishJobToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ManualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RevisiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TandaCetakHasilToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JobStuckInstrumentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.masterMnu = New System.Windows.Forms.ToolStripMenuItem()
        Me.testTypeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.testGroupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TestMethodeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.userToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PembayaranToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.settingMnu = New System.Windows.Forms.ToolStripMenuItem()
        Me.databaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModePembacaanSampleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSettingNumbering = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.SettingJumlahBarcodePrintingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TestBarcodeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintBarcodeUlangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.InstrumentTestConfigurationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InstrumentConnectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.PalioTestConfigurationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PalioConnectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.HemaConneToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.HapusJobToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SeetingHeaderLaporanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LicenseManagerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArchiveDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CekDBUpdateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MRHemaLogToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.dirui240Menu = New System.Windows.Forms.ToolStripMenuItem()
        Me.palio100Menu = New System.Windows.Forms.ToolStripMenuItem()
        Me.diruiHema3000Mnu = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataHasilLengkapToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Hema3ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mindray3600Mnu = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataHasilLengkapToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MindRay5100ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MindRay3100ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.managementMnu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportPerTestItemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportPerTanggalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.aboutMnu = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnutest = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'menuStrip1
        '
        Me.menuStrip1.AllowItemReorder = True
        Me.menuStrip1.AutoSize = False
        Me.menuStrip1.BackColor = System.Drawing.SystemColors.Control
        Me.menuStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.menuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.loginMnu, Me.pasienMnu, Me.pembayaranMnu, Me.jobMnu, Me.masterMnu, Me.settingMnu, Me.dirui240Menu, Me.palio100Menu, Me.diruiHema3000Mnu, Me.mindray3600Mnu, Me.managementMnu, Me.aboutMnu, Me.mnutest})
        Me.menuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.menuStrip1.Name = "menuStrip1"
        Me.menuStrip1.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
        Me.menuStrip1.Size = New System.Drawing.Size(1284, 41)
        Me.menuStrip1.TabIndex = 3
        Me.menuStrip1.Text = "menuStrip1"
        '
        'loginMnu
        '
        Me.loginMnu.Image = Global.MEDLIS.My.Resources.Resources.lock_forbidden_hover
        Me.loginMnu.Name = "loginMnu"
        Me.loginMnu.Size = New System.Drawing.Size(81, 37)
        Me.loginMnu.Text = "Login"
        '
        'pasienMnu
        '
        Me.pasienMnu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PasienPendaftaranToolStripMenuItem, Me.PengambilanSampleToolStripMenuItem})
        Me.pasienMnu.Image = Global.MEDLIS.My.Resources.Resources.star
        Me.pasienMnu.Name = "pasienMnu"
        Me.pasienMnu.Size = New System.Drawing.Size(85, 37)
        Me.pasienMnu.Text = "Pasien"
        '
        'PasienPendaftaranToolStripMenuItem
        '
        Me.PasienPendaftaranToolStripMenuItem.Name = "PasienPendaftaranToolStripMenuItem"
        Me.PasienPendaftaranToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.PasienPendaftaranToolStripMenuItem.Text = "Pasien Pendaftaran"
        '
        'PengambilanSampleToolStripMenuItem
        '
        Me.PengambilanSampleToolStripMenuItem.Name = "PengambilanSampleToolStripMenuItem"
        Me.PengambilanSampleToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.PengambilanSampleToolStripMenuItem.Text = "Pengambilan Sample"
        '
        'pembayaranMnu
        '
        Me.pembayaranMnu.Image = Global.MEDLIS.My.Resources.Resources.photo_favorite
        Me.pembayaranMnu.Name = "pembayaranMnu"
        Me.pembayaranMnu.Size = New System.Drawing.Size(117, 37)
        Me.pembayaranMnu.Text = "Pembayaran"
        '
        'jobMnu
        '
        Me.jobMnu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TestStatusToolStripMenuItem, Me.FinishJobToolStripMenuItem, Me.ManualToolStripMenuItem, Me.RevisiToolStripMenuItem, Me.TandaCetakHasilToolStripMenuItem, Me.JobStuckInstrumentToolStripMenuItem})
        Me.jobMnu.Image = Global.MEDLIS.My.Resources.Resources.clipboard_full
        Me.jobMnu.Name = "jobMnu"
        Me.jobMnu.Size = New System.Drawing.Size(69, 37)
        Me.jobMnu.Text = "Job"
        '
        'TestStatusToolStripMenuItem
        '
        Me.TestStatusToolStripMenuItem.Name = "TestStatusToolStripMenuItem"
        Me.TestStatusToolStripMenuItem.Size = New System.Drawing.Size(185, 22)
        Me.TestStatusToolStripMenuItem.Text = "Test Status"
        '
        'FinishJobToolStripMenuItem
        '
        Me.FinishJobToolStripMenuItem.Name = "FinishJobToolStripMenuItem"
        Me.FinishJobToolStripMenuItem.Size = New System.Drawing.Size(185, 22)
        Me.FinishJobToolStripMenuItem.Text = "Finish Job"
        '
        'ManualToolStripMenuItem
        '
        Me.ManualToolStripMenuItem.Name = "ManualToolStripMenuItem"
        Me.ManualToolStripMenuItem.Size = New System.Drawing.Size(185, 22)
        Me.ManualToolStripMenuItem.Text = "Entry Result"
        '
        'RevisiToolStripMenuItem
        '
        Me.RevisiToolStripMenuItem.Name = "RevisiToolStripMenuItem"
        Me.RevisiToolStripMenuItem.Size = New System.Drawing.Size(185, 22)
        Me.RevisiToolStripMenuItem.Text = "Revisi"
        '
        'TandaCetakHasilToolStripMenuItem
        '
        Me.TandaCetakHasilToolStripMenuItem.Name = "TandaCetakHasilToolStripMenuItem"
        Me.TandaCetakHasilToolStripMenuItem.Size = New System.Drawing.Size(185, 22)
        Me.TandaCetakHasilToolStripMenuItem.Text = "Tanda Cetak Hasil"
        '
        'JobStuckInstrumentToolStripMenuItem
        '
        Me.JobStuckInstrumentToolStripMenuItem.Name = "JobStuckInstrumentToolStripMenuItem"
        Me.JobStuckInstrumentToolStripMenuItem.Size = New System.Drawing.Size(185, 22)
        Me.JobStuckInstrumentToolStripMenuItem.Text = "Dirui Job Stuck Instru"
        '
        'masterMnu
        '
        Me.masterMnu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.testTypeToolStripMenuItem, Me.testGroupToolStripMenuItem, Me.TestMethodeToolStripMenuItem, Me.userToolStripMenuItem, Me.PembayaranToolStripMenuItem})
        Me.masterMnu.Image = Global.MEDLIS.My.Resources.Resources.folder_document_out
        Me.masterMnu.Name = "masterMnu"
        Me.masterMnu.Size = New System.Drawing.Size(114, 37)
        Me.masterMnu.Text = "Master Data"
        '
        'testTypeToolStripMenuItem
        '
        Me.testTypeToolStripMenuItem.Name = "testTypeToolStripMenuItem"
        Me.testTypeToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.testTypeToolStripMenuItem.Text = "Test Type"
        '
        'testGroupToolStripMenuItem
        '
        Me.testGroupToolStripMenuItem.Name = "testGroupToolStripMenuItem"
        Me.testGroupToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.testGroupToolStripMenuItem.Text = "Test Group"
        '
        'TestMethodeToolStripMenuItem
        '
        Me.TestMethodeToolStripMenuItem.Name = "TestMethodeToolStripMenuItem"
        Me.TestMethodeToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.TestMethodeToolStripMenuItem.Text = "Test Profile"
        '
        'userToolStripMenuItem
        '
        Me.userToolStripMenuItem.Name = "userToolStripMenuItem"
        Me.userToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.userToolStripMenuItem.Text = "User"
        '
        'PembayaranToolStripMenuItem
        '
        Me.PembayaranToolStripMenuItem.Name = "PembayaranToolStripMenuItem"
        Me.PembayaranToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.PembayaranToolStripMenuItem.Text = "Pembayaran"
        '
        'settingMnu
        '
        Me.settingMnu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.databaseToolStripMenuItem, Me.ModePembacaanSampleToolStripMenuItem, Me.mnuSettingNumbering, Me.ToolStripMenuItem3, Me.ToolStripSeparator5, Me.SettingJumlahBarcodePrintingToolStripMenuItem, Me.TestBarcodeToolStripMenuItem, Me.PrintBarcodeUlangToolStripMenuItem, Me.ToolStripSeparator6, Me.InstrumentTestConfigurationToolStripMenuItem, Me.InstrumentConnectionToolStripMenuItem, Me.ToolStripSeparator4, Me.PalioTestConfigurationToolStripMenuItem, Me.PalioConnectionToolStripMenuItem, Me.ToolStripSeparator1, Me.HemaConneToolStripMenuItem, Me.ToolStripSeparator2, Me.HapusJobToolStripMenuItem1, Me.SeetingHeaderLaporanToolStripMenuItem, Me.LicenseManagerToolStripMenuItem, Me.ArchiveDataToolStripMenuItem, Me.CekDBUpdateToolStripMenuItem, Me.MRHemaLogToolStripMenuItem})
        Me.settingMnu.Image = Global.MEDLIS.My.Resources.Resources.document_settings
        Me.settingMnu.Name = "settingMnu"
        Me.settingMnu.Size = New System.Drawing.Size(88, 37)
        Me.settingMnu.Text = "Setting"
        '
        'databaseToolStripMenuItem
        '
        Me.databaseToolStripMenuItem.BackColor = System.Drawing.Color.AliceBlue
        Me.databaseToolStripMenuItem.Name = "databaseToolStripMenuItem"
        Me.databaseToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.databaseToolStripMenuItem.Text = "Database"
        '
        'ModePembacaanSampleToolStripMenuItem
        '
        Me.ModePembacaanSampleToolStripMenuItem.Name = "ModePembacaanSampleToolStripMenuItem"
        Me.ModePembacaanSampleToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.ModePembacaanSampleToolStripMenuItem.Text = "Mode Pembacaan Sample"
        '
        'mnuSettingNumbering
        '
        Me.mnuSettingNumbering.Name = "mnuSettingNumbering"
        Me.mnuSettingNumbering.Size = New System.Drawing.Size(271, 22)
        Me.mnuSettingNumbering.Text = "Setting system nomor"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(271, 22)
        Me.ToolStripMenuItem3.Text = "Setting Tipe Pasien"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(268, 6)
        '
        'SettingJumlahBarcodePrintingToolStripMenuItem
        '
        Me.SettingJumlahBarcodePrintingToolStripMenuItem.Name = "SettingJumlahBarcodePrintingToolStripMenuItem"
        Me.SettingJumlahBarcodePrintingToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.SettingJumlahBarcodePrintingToolStripMenuItem.Text = "Setting Jumlah Barcode Printing"
        '
        'TestBarcodeToolStripMenuItem
        '
        Me.TestBarcodeToolStripMenuItem.Name = "TestBarcodeToolStripMenuItem"
        Me.TestBarcodeToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.TestBarcodeToolStripMenuItem.Text = "Test Barcode"
        '
        'PrintBarcodeUlangToolStripMenuItem
        '
        Me.PrintBarcodeUlangToolStripMenuItem.Name = "PrintBarcodeUlangToolStripMenuItem"
        Me.PrintBarcodeUlangToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.PrintBarcodeUlangToolStripMenuItem.Text = "Print Barcode Ulang"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(268, 6)
        '
        'InstrumentTestConfigurationToolStripMenuItem
        '
        Me.InstrumentTestConfigurationToolStripMenuItem.Name = "InstrumentTestConfigurationToolStripMenuItem"
        Me.InstrumentTestConfigurationToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.InstrumentTestConfigurationToolStripMenuItem.Text = "Instrument BM240 Test Configuration"
        '
        'InstrumentConnectionToolStripMenuItem
        '
        Me.InstrumentConnectionToolStripMenuItem.Name = "InstrumentConnectionToolStripMenuItem"
        Me.InstrumentConnectionToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.InstrumentConnectionToolStripMenuItem.Text = "Instrument BM240 Connection"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(268, 6)
        '
        'PalioTestConfigurationToolStripMenuItem
        '
        Me.PalioTestConfigurationToolStripMenuItem.Name = "PalioTestConfigurationToolStripMenuItem"
        Me.PalioTestConfigurationToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.PalioTestConfigurationToolStripMenuItem.Text = "Palio Test Configuration"
        '
        'PalioConnectionToolStripMenuItem
        '
        Me.PalioConnectionToolStripMenuItem.Name = "PalioConnectionToolStripMenuItem"
        Me.PalioConnectionToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.PalioConnectionToolStripMenuItem.Text = "Palio Connection"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(268, 6)
        '
        'HemaConneToolStripMenuItem
        '
        Me.HemaConneToolStripMenuItem.Name = "HemaConneToolStripMenuItem"
        Me.HemaConneToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.HemaConneToolStripMenuItem.Text = "Hema Connection"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(268, 6)
        '
        'HapusJobToolStripMenuItem1
        '
        Me.HapusJobToolStripMenuItem1.Name = "HapusJobToolStripMenuItem1"
        Me.HapusJobToolStripMenuItem1.Size = New System.Drawing.Size(271, 22)
        Me.HapusJobToolStripMenuItem1.Text = "Hapus Job"
        '
        'SeetingHeaderLaporanToolStripMenuItem
        '
        Me.SeetingHeaderLaporanToolStripMenuItem.Name = "SeetingHeaderLaporanToolStripMenuItem"
        Me.SeetingHeaderLaporanToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.SeetingHeaderLaporanToolStripMenuItem.Text = "Header Laporan"
        '
        'LicenseManagerToolStripMenuItem
        '
        Me.LicenseManagerToolStripMenuItem.Name = "LicenseManagerToolStripMenuItem"
        Me.LicenseManagerToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.LicenseManagerToolStripMenuItem.Text = "License Manager"
        '
        'ArchiveDataToolStripMenuItem
        '
        Me.ArchiveDataToolStripMenuItem.Name = "ArchiveDataToolStripMenuItem"
        Me.ArchiveDataToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.ArchiveDataToolStripMenuItem.Text = "Archive Data"
        '
        'CekDBUpdateToolStripMenuItem
        '
        Me.CekDBUpdateToolStripMenuItem.Name = "CekDBUpdateToolStripMenuItem"
        Me.CekDBUpdateToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.CekDBUpdateToolStripMenuItem.Text = "DB Update Tool"
        '
        'MRHemaLogToolStripMenuItem
        '
        Me.MRHemaLogToolStripMenuItem.Name = "MRHemaLogToolStripMenuItem"
        Me.MRHemaLogToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.MRHemaLogToolStripMenuItem.Text = "MRHema Log"
        '
        'dirui240Menu
        '
        Me.dirui240Menu.Image = Global.MEDLIS.My.Resources.Resources.settings
        Me.dirui240Menu.Name = "dirui240Menu"
        Me.dirui240Menu.Size = New System.Drawing.Size(114, 37)
        Me.dirui240Menu.Text = "DiruiCST240"
        '
        'palio100Menu
        '
        Me.palio100Menu.Image = Global.MEDLIS.My.Resources.Resources.settings
        Me.palio100Menu.Name = "palio100Menu"
        Me.palio100Menu.Size = New System.Drawing.Size(98, 37)
        Me.palio100Menu.Text = "Palio 100"
        '
        'diruiHema3000Mnu
        '
        Me.diruiHema3000Mnu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DataHasilLengkapToolStripMenuItem, Me.Hema3ToolStripMenuItem})
        Me.diruiHema3000Mnu.Image = Global.MEDLIS.My.Resources.Resources.settings
        Me.diruiHema3000Mnu.Name = "diruiHema3000Mnu"
        Me.diruiHema3000Mnu.Size = New System.Drawing.Size(132, 37)
        Me.diruiHema3000Mnu.Text = "DiruiHema3000"
        '
        'DataHasilLengkapToolStripMenuItem
        '
        Me.DataHasilLengkapToolStripMenuItem.Name = "DataHasilLengkapToolStripMenuItem"
        Me.DataHasilLengkapToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.DataHasilLengkapToolStripMenuItem.Text = "Data Hasil Lengkap"
        '
        'Hema3ToolStripMenuItem
        '
        Me.Hema3ToolStripMenuItem.Name = "Hema3ToolStripMenuItem"
        Me.Hema3ToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.Hema3ToolStripMenuItem.Text = "Hema Transfer"
        '
        'mindray3600Mnu
        '
        Me.mindray3600Mnu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DataHasilLengkapToolStripMenuItem1, Me.MindRay5100ToolStripMenuItem, Me.MindRay3100ToolStripMenuItem, Me.ToolStripMenuItem1})
        Me.mindray3600Mnu.Image = Global.MEDLIS.My.Resources.Resources.settings_hover
        Me.mindray3600Mnu.Name = "mindray3600Mnu"
        Me.mindray3600Mnu.Size = New System.Drawing.Size(133, 37)
        Me.mindray3600Mnu.Text = "MindRay Hema"
        '
        'DataHasilLengkapToolStripMenuItem1
        '
        Me.DataHasilLengkapToolStripMenuItem1.Name = "DataHasilLengkapToolStripMenuItem1"
        Me.DataHasilLengkapToolStripMenuItem1.Size = New System.Drawing.Size(241, 22)
        Me.DataHasilLengkapToolStripMenuItem1.Text = "Data Hasil Lengkap"
        '
        'MindRay5100ToolStripMenuItem
        '
        Me.MindRay5100ToolStripMenuItem.Name = "MindRay5100ToolStripMenuItem"
        Me.MindRay5100ToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.MindRay5100ToolStripMenuItem.Text = "MindRay 5100"
        '
        'MindRay3100ToolStripMenuItem
        '
        Me.MindRay3100ToolStripMenuItem.Name = "MindRay3100ToolStripMenuItem"
        Me.MindRay3100ToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.MindRay3100ToolStripMenuItem.Text = "MindRay 3600"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(241, 22)
        Me.ToolStripMenuItem1.Text = "MindRay BloodMessage Setting"
        '
        'managementMnu
        '
        Me.managementMnu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReportPerTestItemToolStripMenuItem, Me.ReportPerTanggalToolStripMenuItem})
        Me.managementMnu.Image = Global.MEDLIS.My.Resources.Resources.book_mark
        Me.managementMnu.Name = "managementMnu"
        Me.managementMnu.Size = New System.Drawing.Size(86, 37)
        Me.managementMnu.Text = "Report"
        '
        'ReportPerTestItemToolStripMenuItem
        '
        Me.ReportPerTestItemToolStripMenuItem.Name = "ReportPerTestItemToolStripMenuItem"
        Me.ReportPerTestItemToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.ReportPerTestItemToolStripMenuItem.Text = "Report Per Test Item"
        '
        'ReportPerTanggalToolStripMenuItem
        '
        Me.ReportPerTanggalToolStripMenuItem.Name = "ReportPerTanggalToolStripMenuItem"
        Me.ReportPerTanggalToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.ReportPerTanggalToolStripMenuItem.Text = "Report Per Tanggal"
        '
        'aboutMnu
        '
        Me.aboutMnu.Name = "aboutMnu"
        Me.aboutMnu.Size = New System.Drawing.Size(52, 37)
        Me.aboutMnu.Text = "About"
        '
        'mnutest
        '
        Me.mnutest.Name = "mnutest"
        Me.mnutest.Size = New System.Drawing.Size(40, 37)
        Me.mnutest.Text = "TEst"
        '
        'MainFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImage = Global.MEDLIS.My.Resources.medlisrc.bgindolis
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1284, 557)
        Me.Controls.Add(Me.menuStrip1)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.menuStrip1
        Me.Name = "MainFrm"
        Me.Text = "INDO-LIS   WWW.APTANATEKINDO.COM"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.menuStrip1.ResumeLayout(False)
        Me.menuStrip1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents aboutMnu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents databaseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents settingMnu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents userToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents testGroupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents testTypeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents masterMnu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents jobMnu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents pasienMnu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents loginMnu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents menuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents TestStatusToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FinishJobToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InstrumentTestConfigurationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InstrumentConnectionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ModePembacaanSampleToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ManualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents managementMnu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dirui240Menu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PembayaranToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pembayaranMnu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PengambilanSampleToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportPerTestItemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportPerTanggalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PasienPendaftaranToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RevisiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TandaCetakHasilToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JobStuckInstrumentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PalioTestConfigurationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PalioConnectionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents palio100Menu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TestBarcodeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SettingJumlahBarcodePrintingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HapusJobToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintBarcodeUlangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents diruiHema3000Mnu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataHasilLengkapToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Hema3ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SeetingHeaderLaporanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HemaConneToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LicenseManagerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSettingNumbering As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ArchiveDataToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TestMethodeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CekDBUpdateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mindray3600Mnu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataHasilLengkapToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MindRay5100ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MindRay3100ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MRHemaLogToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnutest As ToolStripMenuItem
End Class
