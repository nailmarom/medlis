﻿Imports System.Windows.Forms.DataVisualization.Charting
Imports System.Data.SqlClient
Imports GemBox.Spreadsheet

Public Class ChartFrm
    Dim bc As String
    Dim selectedLabNumber As String

    Public Sub New(ByVal str As String, ByVal str_ln As String)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        bc = str
        selectedLabNumber = str_ln
        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub ChartFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Icon = New System.Drawing.Icon("medlis2.ico")
        DrawChartWbc(bc)
        DrawChartRBC(bc)
        DrawChartPLT(bc)

    End Sub
    Private Delegate Sub dgateWBC(ByVal barcode As String)
    Private Sub DrawChartWbc(ByVal barcode As String)


        wbcchart.Series.Clear()
        Dim s As New Series
        s.Name = "WBC"
        wbcchart.Name = "WBC"

        If wbcchart.InvokeRequired Then
            Me.Invoke(New dgateWBC(AddressOf DrawChartWbc), barcode)
        Else

            Dim strins As String
            Dim wbcresult As String

            Dim ogre As New clsGreConnect
            ogre.buildConn()
            strins = "select result from hemachart where barcode='" & barcode & "' and itemname='WBC-Chart'"
            Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If rdr.HasRows Then
                Do While rdr.Read
                    wbcresult = rdr("result")
                Loop

                Dim rgx As String() = System.Text.RegularExpressions.Regex.Split(wbcresult, "\,")
                Dim c As Integer
                For c = 0 To rgx.Count - 1
                    Dim dp As New DataPoint
                    If rgx.ElementAt(c) <> "" Then
                        dp.SetValueXY(c, CDbl(rgx.ElementAt(c).Trim()))
                        s.Points.Add(dp)
                    End If
                Next
                wbcchart.Series.Add(s)
            End If

            rdr.Close()
            cmd.Dispose()
            ogre.CloseConn()


        End If
    End Sub
    Private Delegate Sub dgateRBC(ByVal barcode As String)
    Private Sub DrawChartRBC(ByVal barcode As String)

        rbcchart.Series.Clear()
        Dim s As New Series
        s.Name = "RBC"
        rbcchart.Name = "RBC"

        If rbcchart.InvokeRequired Then
            Me.Invoke(New dgateRBC(AddressOf DrawChartRBC), barcode)
        Else

            Dim strins As String
            Dim rbcresult As String

            Dim ogre As New clsGreConnect
            ogre.buildConn()
            strins = "select result from hemachart where barcode='" & barcode & "' and itemname='RBC-Chart' "
            Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If rdr.HasRows Then
                Do While rdr.Read
                    rbcresult = rdr("result")
                Loop

                Dim rgx As String() = System.Text.RegularExpressions.Regex.Split(rbcresult, "\,")
                Dim c As Integer
                For c = 0 To rgx.Count - 1
                    Dim dp As New DataPoint
                    If rgx.ElementAt(c) <> "" Then
                        dp.SetValueXY(c, CDbl(rgx.ElementAt(c).Trim()))
                        s.Points.Add(dp)

                    End If
                Next
                rbcchart.Series.Add(s)


            End If

            rdr.Close()
            cmd.Dispose()
            ogre.CloseConn()


        End If
    End Sub
    Private Delegate Sub dgatePLT(ByVal barcode As String)
    Private Sub DrawChartPLT(ByVal barcode As String)

        pltchart.Series.Clear()
        Dim s As New Series
        s.Name = "PLT"
        pltchart.Name = "PLT"

        If pltchart.InvokeRequired Then
            Me.Invoke(New dgatePLT(AddressOf DrawChartPLT), barcode)
        Else

            Dim strins As String
            Dim PLTresult As String

            Dim ogre As New clsGreConnect
            ogre.buildConn()
            strins = "select result from hemachart where barcode='" & barcode & "' and itemname='PLT-Chart'"
            Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If rdr.HasRows Then
                Do While rdr.Read
                    PLTresult = rdr("result")
                Loop

                Dim rgx As String() = System.Text.RegularExpressions.Regex.Split(PLTresult, "\,")

                Dim c As Integer
                For c = 0 To rgx.Count - 1
                    Dim dp As New DataPoint
                    If rgx.ElementAt(c) <> "" Then
                        dp.SetValueXY(c, CDbl(rgx.ElementAt(c).Trim()))
                        s.Points.Add(dp)

                    End If

                Next
                pltchart.Series.Add(s)

            End If

            rdr.Close()
            cmd.Dispose()
            ogre.CloseConn()


            'Dim wbc = "0 , 0 , 0 , 0 , 0, 20 , 127, 78, 89, 9, 50, 55,"

        End If
    End Sub

    Private Sub cmdPrintHema_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrintHema.Click

        Me.Cursor = Cursors.WaitCursor

        '================== chart create image di atas ============
        Dim wbcpath As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "wbcchart.png")
        Dim rbcpath As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "rbcchart.png")
        Dim pltpath As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "pltchart.png")

        wbcchart.SaveImage(wbcpath, System.Drawing.Imaging.ImageFormat.Png)
        rbcchart.SaveImage(rbcpath, System.Drawing.Imaging.ImageFormat.Png)
        pltchart.SaveImage(pltpath, System.Drawing.Imaging.ImageFormat.Png)

        SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY")

        Dim excelPath As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "hema-chart.xlsx")
        Dim ef As ExcelFile = ExcelFile.Load(excelPath)
        Dim ws As ExcelWorksheet = ef.Worksheets(0) '.Add("nilai-chart")
        '=header
        ' ws.HeadersFooters.DefaultPage.Header.RightSection.Content = "Laboratorium Allan Mardian " & vbCrLf & "10 5757"
        'ws.HeadersFooters.FirstPage.Header.RightSection.Content = "Laboratorium Allan Mardian " & vbCrLf & "10 5757"
        '================== chart create image di atas ============

        Dim datereceived As Date
        Dim idpasien As Integer
        Dim docname As String
        Dim docaddress As String
        Dim age As String
        Dim strsql As String
        strsql = "select idpasien,docname,docaddress,printage,datereceived from job where labnumber='" & selectedLabNumber & "'"

        Dim greshow As New clsGreConnect
        greshow.buildConn()

        Dim cmd As New  SqlClient.SqlCommand(strsql, greshow.grecon)
        Dim rdr As  SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            idpasien = rdr("idpasien")
            If Not IsDBNull(rdr("docname")) Then
                docname = rdr("docname")
            Else
                docname = ""
            End If
            If Not IsDBNull(rdr("docaddress")) Then
                docaddress = rdr("docaddress")
            Else
                docaddress = ""
            End If
            If Not IsDBNull(rdr("printage")) Then
                age = rdr("printage")
            Else
                age = " "
            End If

            If Not IsDBNull(rdr("datereceived")) Then
                datereceived = rdr("datereceived")
            Else

            End If
        Loop

        rdr.Close()
        cmd.Dispose()
        greshow.CloseConn()

        Dim datetoprint As String
        datetoprint = CStr(datereceived.Day) & "/" & CStr(datereceived.Month) & "/" & CStr(datereceived.Year)


        '===========================================
        strsql = "select * from patient where id='" & idpasien & "'"
        Dim grepatient As New clsGreConnect
        grepatient.buildConn()

        Dim alamatpasien As String
        Dim namapasien As String
        Dim sex As String
        Dim tgllahir As String
        Dim phone As String
        Dim printIdPasien As String

        Dim cmdpatient As New  SqlClient.SqlCommand(strsql, grepatient.grecon)
        Dim rdrpatient As  SqlDataReader = cmdpatient.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdrpatient.Read
            namapasien = rdrpatient("patientname")
            alamatpasien = rdrpatient("address")
            If rdrpatient("malefemale") = 0 Then
                sex = "Wanita"
            Else
                sex = "Laki Laki"
            End If
            tgllahir = rdrpatient("birthdate")
            phone = rdrpatient("phone")
            printIdPasien = rdrpatient("idpatient")
        Loop

        rdrpatient.Close()
        cmdpatient.Dispose()
        grepatient.CloseConn()

        '=========================================== analisa
        'strsql = "select jobdetail.labnumber,testtype.testname,jobdetail.resultabnormalflag,jobdetail.measurementvalue,testtype.keterangan,testtype.nilairujukan,testtype.satuan from jobdetail,testtype where jobdetail.idtesttype=testtype.id  and  jobdetail.labnumber='" & selectedLabNumber & "'"
        '==strsql = "select itemname,result,unit,refrange,abnormalflag from hemadata where barcode='" & bc & "'"
        strsql = "select hemadata.itemname,hemadata.result,hemadata.abnormalflag,hemadata.unit,hemadata.refrange,testtype.displayindex,testtype.digit from hemadata,testtype where hemadata.itemname=testtype.analyzertestname and hemadata.barcode='" & bc & "' order by testtype.displayindex asc"
        Dim greanalisa As New clsGreConnect
        greanalisa.buildConn()

        Dim testname As String
        Dim resultabnormalflag As String
        Dim hasil As String
        Dim ket As String
        Dim rujukan As String
        Dim satuan As String
        Dim intRow As Integer = 12

        Dim rctestname As String
        Dim rchasil As String
        Dim rcrujukan As String
        Dim rcsatuan As String
        Dim rcket As String


        Dim row As Integer = 12
        Dim col As Integer = 0

        Dim cmdanalisa As New  SqlClient.SqlCommand(strsql, greanalisa.grecon)
        Dim rdranalisa As  SqlDataReader = cmdanalisa.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdranalisa.Read
            testname = rdranalisa("itemname")

            'rctestname = "A" & CStr(intRow)
            'ws.Cells(rctestname).Value = testname
            ws.Cells(row, 0).Value = testname
            ws.Cells(row, 0).Style.WrapText = False

            hasil = Math.Round(CDbl(rdranalisa("result")), rdranalisa("digit"), MidpointRounding.AwayFromZero).ToString
            'rchasil = "C" & CStr(intRow)
            'ws.Cells(rchasil).Value = hasil
            resultabnormalflag = rdranalisa("abnormalflag")
            If Trim(resultabnormalflag) = "N" Then
                ws.Cells(row, 2).Value = hasil
            Else
                ws.Cells(row, 2).Value = hasil & " *"
            End If


            rujukan = rdranalisa("refrange")
            'rcrujukan = "E" & CStr(intRow)
            'ws.Cells(rcrujukan).Value = rujukan
            ws.Cells(row, 4).Value = rujukan

            satuan = rdranalisa("unit")
            'rcsatuan = "G" & CStr(intRow)
            'ws.Cells(rcsatuan).Value = satuan
            ws.Cells(row, 6).Value = satuan

            ket = "" 'rdranalisa("keterangan")
            rcket = "H" & CStr(intRow)
            'ws.Cells(rcket).Style.WrapText = True
            'ws.Cells(rcket).Value = ket
            ws.Cells(row, 7).Value = ket
            'ws.Cells(
            ws.Cells(row, 7).Style.WrapText = True

            'intRow = intRow + 1
            'if int row > 38,83,128 then..  loncat ke 57,101
            row = row + 1

            If row = 36 Then '39 awalnya
                row = 57
            ElseIf row = 81 Then
                row = 101
            End If

        Loop

        rdranalisa.Close()
        cmdanalisa.Dispose()
        greanalisa.CloseConn()


        ' ws.HeadersFooters.FirstPage.Header.CenterSection.Content = "Laboratorium Allan Mardian"
        'ws.HeadersFooters.FirstPage.Header.RightSection.Content = "Laboratorium Allan Mardian"



        ws.Cells("B2").Value = ": " & docname
        ws.Cells("B3").Value = ": " & docaddress
        ws.Cells("B4").Value = ": " & selectedLabNumber
        ws.Cells("B5").Value = ": " & printIdPasien
        ws.Cells("B6").Value = ": " & namapasien
        ws.Cells("B7").Value = ": " & alamatpasien
        ws.Cells("B7").Style.WrapText = True

        ws.Cells("H3").Value = ": " & sex
        ws.Cells("H4").Value = ": " & tgllahir
        ws.Cells("H5").Value = ": " & age
        ws.Cells("H6").Value = ": " & selectedLabNumber
        ws.Cells("H7").Value = ": " & datetoprint

        'If row > 36 And row < 81 Then

        '    Dim cr As CellRange
        '    cr = ws.Cells.GetSubrange("A2", "I10")
        '    cr.CopyTo("A47")

        '    Dim crbottom As CellRange
        '    crbottom = ws.Cells.GetSubrange("A39,I45")
        '    crbottom.CopyTo("A84")
        'ElseIf row > 81 Then
        '    Dim cr As CellRange
        '    cr = ws.Cells.GetSubrange("A2", "I10")
        '    cr.CopyTo("A47")
        '    cr.CopyTo("A92")

        '    Dim crbottom As CellRange
        '    crbottom = ws.Cells.GetSubrange("A39,I45")
        '    crbottom.CopyTo("A129")

        'End If

        Dim linepos As Integer
        linepos = row + 5
        Dim crl As CellRange = ws.Cells.GetSubrange("A" & CStr(linepos), "H" & CStr(linepos))

        crl.Style.Borders.SetBorders(MultipleBorders.Bottom, SpreadsheetColor.FromName(ColorName.Black), LineStyle.Thin)
        ws.Cells("E" & CStr(linepos + 2)).Value = "DiCetak oleh: " & user_complete_name
        ws.Cells("E" & CStr(linepos + 3)).Value = "Tanggal: " & Now()
        ws.Cells("E" & CStr(linepos + 4)).Value = "Disetujui oleh: "


        'cetak-chart
        '==========================================
        Dim crchart As CellRange
        crchart = ws.Cells.GetSubrange("A2", "I8")
        crchart.CopyTo("A52")

        ws.Pictures.Add(wbcpath, "B61", "F68")
        ws.Pictures.Add(rbcpath, "B70", "F77")
        ws.Pictures.Add(pltpath, "B79", "F86")

        ef.Save("Hemachart.xlsx")

        Me.Cursor = Cursors.Arrow


        Dim xpsDoc = ef.ConvertToXpsDocument(SaveOptions.XpsDefault)

        Dim objDocView As New ChartPrintFrm
        objDocView.StartPosition = FormStartPosition.Manual
        objDocView.Left = 0
        objDocView.Top = 0
        objDocView.ShowDialog()

    End Sub
End Class