﻿'
' Created by SharpDevelop.
' User: User
' Date: 10/14/2014
' Time: 9:19 AM
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Partial Class testTypeFrm
	Inherits System.Windows.Forms.Form
	
	''' <summary>
	''' Designer variable used to keep track of non-visual components.
	''' </summary>
	Private components As System.ComponentModel.IContainer
	
	''' <summary>
	''' Disposes resources used by the form.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If components IsNot Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub
	
	''' <summary>
	''' This method is required for Windows Forms designer support.
	''' Do not change the method contents inside the source code editor. The Forms designer might
	''' not be able to load this method if it was changed manually.
	''' </summary>
	Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(testTypeFrm))
        Me.txtHarga = New System.Windows.Forms.TextBox()
        Me.label8 = New System.Windows.Forms.Label()
        Me.cbGroupChoice = New System.Windows.Forms.ComboBox()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.AddMnu = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.SaveMnu = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.EditMnu = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.DeleteMnu = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.cancelMnu = New System.Windows.Forms.ToolStripButton()
        Me.dgTestType = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblguide = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cbspeciment = New System.Windows.Forms.ComboBox()
        Me.showurutan = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txturutan = New System.Windows.Forms.TextBox()
        Me.cbdigit = New System.Windows.Forms.ComboBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.hargaLevel6 = New System.Windows.Forms.TextBox()
        Me.hargaLevel5 = New System.Windows.Forms.TextBox()
        Me.hargaLevel4 = New System.Windows.Forms.TextBox()
        Me.hargaLevel3 = New System.Windows.Forms.TextBox()
        Me.hargaLevel2 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtanalyzerTestName = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cbTestGroup = New System.Windows.Forms.ComboBox()
        Me.label5 = New System.Windows.Forms.Label()
        Me.txtKet = New System.Windows.Forms.TextBox()
        Me.label4 = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        Me.label1 = New System.Windows.Forms.Label()
        Me.label3 = New System.Windows.Forms.Label()
        Me.txtNilaiRujukan = New System.Windows.Forms.TextBox()
        Me.txtSatuan = New System.Windows.Forms.TextBox()
        Me.txtTestName = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.cbrelatedZ = New System.Windows.Forms.ComboBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.cbrelatedY = New System.Windows.Forms.ComboBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtrelatedformula = New System.Windows.Forms.TextBox()
        Me.cbrelatedX = New System.Windows.Forms.ComboBox()
        Me.rdorelated = New System.Windows.Forms.RadioButton()
        Me.rdofree = New System.Windows.Forms.RadioButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtmaxchild = New System.Windows.Forms.TextBox()
        Me.txtminchild = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.txtmaxwoman = New System.Windows.Forms.TextBox()
        Me.txtminwoman = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtmin = New System.Windows.Forms.TextBox()
        Me.txtmax = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.rdotidak = New System.Windows.Forms.RadioButton()
        Me.rdoyamanual = New System.Windows.Forms.RadioButton()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtdiluarrange = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cbresultType = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtdalamrange = New System.Windows.Forms.TextBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.dgTestType, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtHarga
        '
        Me.txtHarga.Location = New System.Drawing.Point(172, 200)
        Me.txtHarga.Name = "txtHarga"
        Me.txtHarga.Size = New System.Drawing.Size(238, 21)
        Me.txtHarga.TabIndex = 6
        Me.txtHarga.Text = "0"
        '
        'label8
        '
        Me.label8.AutoSize = True
        Me.label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label8.Location = New System.Drawing.Point(29, 65)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(90, 17)
        Me.label8.TabIndex = 16
        Me.label8.Text = "Test Group"
        '
        'cbGroupChoice
        '
        Me.cbGroupChoice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbGroupChoice.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbGroupChoice.FormattingEnabled = True
        Me.cbGroupChoice.Location = New System.Drawing.Point(143, 64)
        Me.cbGroupChoice.Name = "cbGroupChoice"
        Me.cbGroupChoice.Size = New System.Drawing.Size(321, 21)
        Me.cbGroupChoice.TabIndex = 21
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddMnu, Me.ToolStripSeparator5, Me.SaveMnu, Me.ToolStripSeparator6, Me.EditMnu, Me.ToolStripSeparator7, Me.DeleteMnu, Me.ToolStripSeparator8, Me.cancelMnu})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Padding = New System.Windows.Forms.Padding(10, 0, 1, 0)
        Me.ToolStrip1.Size = New System.Drawing.Size(1283, 40)
        Me.ToolStrip1.TabIndex = 24
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'AddMnu
        '
        Me.AddMnu.Image = Global.MEDLIS.My.Resources.Resources.plus
        Me.AddMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.AddMnu.Name = "AddMnu"
        Me.AddMnu.Size = New System.Drawing.Size(65, 37)
        Me.AddMnu.Text = "Add"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 40)
        '
        'SaveMnu
        '
        Me.SaveMnu.Image = Global.MEDLIS.My.Resources.Resources.save
        Me.SaveMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SaveMnu.Name = "SaveMnu"
        Me.SaveMnu.Size = New System.Drawing.Size(67, 37)
        Me.SaveMnu.Text = "Save"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 40)
        '
        'EditMnu
        '
        Me.EditMnu.Image = Global.MEDLIS.My.Resources.Resources.write_edit
        Me.EditMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.EditMnu.Name = "EditMnu"
        Me.EditMnu.Size = New System.Drawing.Size(63, 37)
        Me.EditMnu.Text = "Edit"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(6, 40)
        '
        'DeleteMnu
        '
        Me.DeleteMnu.AutoSize = False
        Me.DeleteMnu.Image = Global.MEDLIS.My.Resources.Resources.cut
        Me.DeleteMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.DeleteMnu.Name = "DeleteMnu"
        Me.DeleteMnu.Size = New System.Drawing.Size(82, 39)
        Me.DeleteMnu.Text = "Delete"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(6, 40)
        '
        'cancelMnu
        '
        Me.cancelMnu.AutoSize = False
        Me.cancelMnu.Image = Global.MEDLIS.My.Resources.Resources.cancel
        Me.cancelMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cancelMnu.Name = "cancelMnu"
        Me.cancelMnu.Size = New System.Drawing.Size(85, 39)
        Me.cancelMnu.Text = "Cancel"
        '
        'dgTestType
        '
        Me.dgTestType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgTestType.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgTestType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgTestType.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgTestType.Location = New System.Drawing.Point(8, 93)
        Me.dgTestType.Name = "dgTestType"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgTestType.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgTestType.RowsDefaultCellStyle = DataGridViewCellStyle4
        Me.dgTestType.Size = New System.Drawing.Size(456, 262)
        Me.dgTestType.TabIndex = 25
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblguide)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.cbspeciment)
        Me.Panel1.Controls.Add(Me.showurutan)
        Me.Panel1.Controls.Add(Me.Label22)
        Me.Panel1.Controls.Add(Me.txturutan)
        Me.Panel1.Controls.Add(Me.cbdigit)
        Me.Panel1.Controls.Add(Me.Label21)
        Me.Panel1.Controls.Add(Me.Label20)
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.hargaLevel6)
        Me.Panel1.Controls.Add(Me.hargaLevel5)
        Me.Panel1.Controls.Add(Me.hargaLevel4)
        Me.Panel1.Controls.Add(Me.hargaLevel3)
        Me.Panel1.Controls.Add(Me.hargaLevel2)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.txtanalyzerTestName)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.cbTestGroup)
        Me.Panel1.Controls.Add(Me.label5)
        Me.Panel1.Controls.Add(Me.txtKet)
        Me.Panel1.Controls.Add(Me.label4)
        Me.Panel1.Controls.Add(Me.label2)
        Me.Panel1.Controls.Add(Me.label1)
        Me.Panel1.Controls.Add(Me.txtHarga)
        Me.Panel1.Controls.Add(Me.label3)
        Me.Panel1.Controls.Add(Me.txtNilaiRujukan)
        Me.Panel1.Controls.Add(Me.txtSatuan)
        Me.Panel1.Controls.Add(Me.txtTestName)
        Me.Panel1.Location = New System.Drawing.Point(9, 25)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(422, 482)
        Me.Panel1.TabIndex = 26
        '
        'lblguide
        '
        Me.lblguide.AutoSize = True
        Me.lblguide.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblguide.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblguide.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblguide.Location = New System.Drawing.Point(357, 32)
        Me.lblguide.Name = "lblguide"
        Me.lblguide.Size = New System.Drawing.Size(53, 22)
        Me.lblguide.TabIndex = 50
        Me.lblguide.Text = "...  klik"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(70, 440)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(94, 15)
        Me.Label11.TabIndex = 49
        Me.Label11.Text = "Jenis Sample"
        '
        'cbspeciment
        '
        Me.cbspeciment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbspeciment.FormattingEnabled = True
        Me.cbspeciment.Location = New System.Drawing.Point(173, 437)
        Me.cbspeciment.Name = "cbspeciment"
        Me.cbspeciment.Size = New System.Drawing.Size(181, 23)
        Me.cbspeciment.TabIndex = 48
        '
        'showurutan
        '
        Me.showurutan.AutoSize = True
        Me.showurutan.Location = New System.Drawing.Point(256, 413)
        Me.showurutan.Name = "showurutan"
        Me.showurutan.Size = New System.Drawing.Size(98, 15)
        Me.showurutan.TabIndex = 47
        Me.showurutan.Text = "urutan cetak .."
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(25, 413)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(140, 15)
        Me.Label22.TabIndex = 46
        Me.Label22.Text = "Urutan Baris laporan"
        '
        'txturutan
        '
        Me.txturutan.Location = New System.Drawing.Point(173, 410)
        Me.txturutan.Name = "txturutan"
        Me.txturutan.Size = New System.Drawing.Size(77, 21)
        Me.txturutan.TabIndex = 45
        '
        'cbdigit
        '
        Me.cbdigit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbdigit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbdigit.FormattingEnabled = True
        Me.cbdigit.Items.AddRange(New Object() {"0", "1", "2", "3"})
        Me.cbdigit.Location = New System.Drawing.Point(173, 363)
        Me.cbdigit.Name = "cbdigit"
        Me.cbdigit.Size = New System.Drawing.Size(94, 23)
        Me.cbdigit.TabIndex = 44
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(77, 364)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(86, 15)
        Me.Label21.TabIndex = 43
        Me.Label21.Text = "Digit Setting"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(54, 337)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(110, 17)
        Me.Label20.TabIndex = 42
        Me.Label20.Text = "Harga Level 6"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(54, 311)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(110, 17)
        Me.Label19.TabIndex = 41
        Me.Label19.Text = "Harga Level 5"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(54, 284)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(110, 17)
        Me.Label18.TabIndex = 40
        Me.Label18.Text = "Harga Level 4"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(54, 258)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(110, 17)
        Me.Label17.TabIndex = 39
        Me.Label17.Text = "Harga Level 3"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(54, 229)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(110, 17)
        Me.Label16.TabIndex = 38
        Me.Label16.Text = "Harga Level 2"
        '
        'hargaLevel6
        '
        Me.hargaLevel6.Location = New System.Drawing.Point(172, 335)
        Me.hargaLevel6.Name = "hargaLevel6"
        Me.hargaLevel6.Size = New System.Drawing.Size(238, 21)
        Me.hargaLevel6.TabIndex = 37
        Me.hargaLevel6.Text = "0"
        '
        'hargaLevel5
        '
        Me.hargaLevel5.Location = New System.Drawing.Point(172, 308)
        Me.hargaLevel5.Name = "hargaLevel5"
        Me.hargaLevel5.Size = New System.Drawing.Size(238, 21)
        Me.hargaLevel5.TabIndex = 36
        Me.hargaLevel5.Text = "0"
        '
        'hargaLevel4
        '
        Me.hargaLevel4.Location = New System.Drawing.Point(172, 281)
        Me.hargaLevel4.Name = "hargaLevel4"
        Me.hargaLevel4.Size = New System.Drawing.Size(238, 21)
        Me.hargaLevel4.TabIndex = 35
        Me.hargaLevel4.Text = "0"
        '
        'hargaLevel3
        '
        Me.hargaLevel3.Location = New System.Drawing.Point(172, 254)
        Me.hargaLevel3.Name = "hargaLevel3"
        Me.hargaLevel3.Size = New System.Drawing.Size(238, 21)
        Me.hargaLevel3.TabIndex = 34
        Me.hargaLevel3.Text = "0"
        '
        'hargaLevel2
        '
        Me.hargaLevel2.Location = New System.Drawing.Point(172, 227)
        Me.hargaLevel2.Name = "hargaLevel2"
        Me.hargaLevel2.Size = New System.Drawing.Size(238, 21)
        Me.hargaLevel2.TabIndex = 33
        Me.hargaLevel2.Text = "0"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(19, 36)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(145, 17)
        Me.Label6.TabIndex = 22
        Me.Label6.Text = "Nama Test Singkat"
        '
        'txtanalyzerTestName
        '
        Me.txtanalyzerTestName.Location = New System.Drawing.Point(172, 33)
        Me.txtanalyzerTestName.Name = "txtanalyzerTestName"
        Me.txtanalyzerTestName.Size = New System.Drawing.Size(159, 21)
        Me.txtanalyzerTestName.TabIndex = 21
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(74, 172)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(90, 17)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Test Group"
        '
        'cbTestGroup
        '
        Me.cbTestGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbTestGroup.FormattingEnabled = True
        Me.cbTestGroup.Location = New System.Drawing.Point(172, 168)
        Me.cbTestGroup.Name = "cbTestGroup"
        Me.cbTestGroup.Size = New System.Drawing.Size(238, 23)
        Me.cbTestGroup.TabIndex = 19
        '
        'label5
        '
        Me.label5.AutoSize = True
        Me.label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label5.Location = New System.Drawing.Point(54, 203)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(110, 17)
        Me.label5.TabIndex = 13
        Me.label5.Text = "Harga Level 1"
        '
        'txtKet
        '
        Me.txtKet.Location = New System.Drawing.Point(172, 62)
        Me.txtKet.Multiline = True
        Me.txtKet.Name = "txtKet"
        Me.txtKet.Size = New System.Drawing.Size(238, 44)
        Me.txtKet.TabIndex = 5
        '
        'label4
        '
        Me.label4.AutoSize = True
        Me.label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label4.Location = New System.Drawing.Point(72, 62)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(92, 17)
        Me.label4.TabIndex = 12
        Me.label4.Text = "Keterangan"
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label2.Location = New System.Drawing.Point(105, 115)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(59, 17)
        Me.label2.TabIndex = 10
        Me.label2.Text = "Satuan"
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.Location = New System.Drawing.Point(14, 6)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(150, 17)
        Me.label1.TabIndex = 9
        Me.label1.Text = "Nama Test Panjang"
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label3.Location = New System.Drawing.Point(8, 141)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(156, 17)
        Me.label3.TabIndex = 11
        Me.label3.Text = "Keterangan Rujukan"
        '
        'txtNilaiRujukan
        '
        Me.txtNilaiRujukan.Location = New System.Drawing.Point(172, 140)
        Me.txtNilaiRujukan.Multiline = True
        Me.txtNilaiRujukan.Name = "txtNilaiRujukan"
        Me.txtNilaiRujukan.Size = New System.Drawing.Size(238, 20)
        Me.txtNilaiRujukan.TabIndex = 4
        '
        'txtSatuan
        '
        Me.txtSatuan.Location = New System.Drawing.Point(172, 112)
        Me.txtSatuan.Name = "txtSatuan"
        Me.txtSatuan.Size = New System.Drawing.Size(238, 21)
        Me.txtSatuan.TabIndex = 3
        '
        'txtTestName
        '
        Me.txtTestName.Location = New System.Drawing.Point(172, 6)
        Me.txtTestName.Name = "txtTestName"
        Me.txtTestName.Size = New System.Drawing.Size(238, 21)
        Me.txtTestName.TabIndex = 2
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(478, 58)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(437, 514)
        Me.GroupBox1.TabIndex = 27
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Test Detail"
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.cbrelatedZ)
        Me.GroupBox2.Controls.Add(Me.Label25)
        Me.GroupBox2.Controls.Add(Me.cbrelatedY)
        Me.GroupBox2.Controls.Add(Me.Label24)
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Controls.Add(Me.txtrelatedformula)
        Me.GroupBox2.Controls.Add(Me.cbrelatedX)
        Me.GroupBox2.Controls.Add(Me.rdorelated)
        Me.GroupBox2.Controls.Add(Me.rdofree)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(8, 364)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(456, 109)
        Me.GroupBox2.TabIndex = 28
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Is It Related Value"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(263, 49)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(30, 13)
        Me.Label26.TabIndex = 9
        Me.Label26.Text = "Z = "
        '
        'cbrelatedZ
        '
        Me.cbrelatedZ.FormattingEnabled = True
        Me.cbrelatedZ.Location = New System.Drawing.Point(293, 46)
        Me.cbrelatedZ.Name = "cbrelatedZ"
        Me.cbrelatedZ.Size = New System.Drawing.Size(82, 21)
        Me.cbrelatedZ.TabIndex = 8
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(140, 48)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(30, 13)
        Me.Label25.TabIndex = 7
        Me.Label25.Text = "Y = "
        '
        'cbrelatedY
        '
        Me.cbrelatedY.FormattingEnabled = True
        Me.cbrelatedY.Location = New System.Drawing.Point(170, 45)
        Me.cbrelatedY.Name = "cbrelatedY"
        Me.cbrelatedY.Size = New System.Drawing.Size(82, 21)
        Me.cbrelatedY.TabIndex = 6
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(19, 48)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(30, 13)
        Me.Label24.TabIndex = 5
        Me.Label24.Text = "X = "
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(8, 77)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(36, 13)
        Me.Label23.TabIndex = 4
        Me.Label23.Text = "Val ="
        '
        'txtrelatedformula
        '
        Me.txtrelatedformula.Location = New System.Drawing.Point(49, 73)
        Me.txtrelatedformula.Name = "txtrelatedformula"
        Me.txtrelatedformula.Size = New System.Drawing.Size(160, 20)
        Me.txtrelatedformula.TabIndex = 3
        '
        'cbrelatedX
        '
        Me.cbrelatedX.FormattingEnabled = True
        Me.cbrelatedX.Location = New System.Drawing.Point(49, 45)
        Me.cbrelatedX.Name = "cbrelatedX"
        Me.cbrelatedX.Size = New System.Drawing.Size(82, 21)
        Me.cbrelatedX.TabIndex = 2
        '
        'rdorelated
        '
        Me.rdorelated.AutoSize = True
        Me.rdorelated.Location = New System.Drawing.Point(127, 20)
        Me.rdorelated.Name = "rdorelated"
        Me.rdorelated.Size = New System.Drawing.Size(69, 17)
        Me.rdorelated.TabIndex = 1
        Me.rdorelated.TabStop = True
        Me.rdorelated.Text = "Related"
        Me.rdorelated.UseVisualStyleBackColor = True
        '
        'rdofree
        '
        Me.rdofree.AutoSize = True
        Me.rdofree.Location = New System.Drawing.Point(20, 20)
        Me.rdofree.Name = "rdofree"
        Me.rdofree.Size = New System.Drawing.Size(86, 17)
        Me.rdofree.TabIndex = 0
        Me.rdofree.TabStop = True
        Me.rdofree.Text = "Free Value"
        Me.rdofree.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Panel2.Controls.Add(Me.GroupBox5)
        Me.Panel2.Controls.Add(Me.GroupBox4)
        Me.Panel2.Controls.Add(Me.GroupBox3)
        Me.Panel2.Location = New System.Drawing.Point(8, 482)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(456, 90)
        Me.Panel2.TabIndex = 29
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.txtmaxchild)
        Me.GroupBox5.Controls.Add(Me.txtminchild)
        Me.GroupBox5.Controls.Add(Me.Label28)
        Me.GroupBox5.Controls.Add(Me.Label29)
        Me.GroupBox5.Location = New System.Drawing.Point(293, 3)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(160, 81)
        Me.GroupBox5.TabIndex = 2
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Anak Anak"
        '
        'txtmaxchild
        '
        Me.txtmaxchild.Location = New System.Drawing.Point(50, 20)
        Me.txtmaxchild.Name = "txtmaxchild"
        Me.txtmaxchild.Size = New System.Drawing.Size(56, 20)
        Me.txtmaxchild.TabIndex = 24
        '
        'txtminchild
        '
        Me.txtminchild.Location = New System.Drawing.Point(50, 47)
        Me.txtminchild.Name = "txtminchild"
        Me.txtminchild.Size = New System.Drawing.Size(56, 20)
        Me.txtminchild.TabIndex = 23
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(7, 47)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(33, 17)
        Me.Label28.TabIndex = 26
        Me.Label28.Text = "Min"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(6, 23)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(45, 17)
        Me.Label29.TabIndex = 27
        Me.Label29.Text = "Maks"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txtmaxwoman)
        Me.GroupBox4.Controls.Add(Me.txtminwoman)
        Me.GroupBox4.Controls.Add(Me.Label15)
        Me.GroupBox4.Controls.Add(Me.Label27)
        Me.GroupBox4.Location = New System.Drawing.Point(150, 2)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(123, 81)
        Me.GroupBox4.TabIndex = 1
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Wanita"
        '
        'txtmaxwoman
        '
        Me.txtmaxwoman.Location = New System.Drawing.Point(50, 20)
        Me.txtmaxwoman.Name = "txtmaxwoman"
        Me.txtmaxwoman.Size = New System.Drawing.Size(56, 20)
        Me.txtmaxwoman.TabIndex = 24
        '
        'txtminwoman
        '
        Me.txtminwoman.Location = New System.Drawing.Point(50, 47)
        Me.txtminwoman.Name = "txtminwoman"
        Me.txtminwoman.Size = New System.Drawing.Size(56, 20)
        Me.txtminwoman.TabIndex = 23
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(7, 47)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(33, 17)
        Me.Label15.TabIndex = 26
        Me.Label15.Text = "Min"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(6, 23)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(45, 17)
        Me.Label27.TabIndex = 27
        Me.Label27.Text = "Maks"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtmin)
        Me.GroupBox3.Controls.Add(Me.txtmax)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Location = New System.Drawing.Point(5, 1)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(123, 81)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Laki Laki"
        '
        'txtmin
        '
        Me.txtmin.Location = New System.Drawing.Point(50, 46)
        Me.txtmin.Name = "txtmin"
        Me.txtmin.Size = New System.Drawing.Size(56, 20)
        Me.txtmin.TabIndex = 29
        '
        'txtmax
        '
        Me.txtmax.Location = New System.Drawing.Point(50, 21)
        Me.txtmax.Name = "txtmax"
        Me.txtmax.Size = New System.Drawing.Size(56, 20)
        Me.txtmax.TabIndex = 28
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(7, 47)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(33, 17)
        Me.Label7.TabIndex = 26
        Me.Label7.Text = "Min"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(6, 23)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(45, 17)
        Me.Label10.TabIndex = 27
        Me.Label10.Text = "Maks"
        '
        'GroupBox6
        '
        Me.GroupBox6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox6.Controls.Add(Me.rdotidak)
        Me.GroupBox6.Controls.Add(Me.rdoyamanual)
        Me.GroupBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.Location = New System.Drawing.Point(921, 359)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(218, 80)
        Me.GroupBox6.TabIndex = 30
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Manual"
        '
        'rdotidak
        '
        Me.rdotidak.AutoSize = True
        Me.rdotidak.Location = New System.Drawing.Point(89, 34)
        Me.rdotidak.Name = "rdotidak"
        Me.rdotidak.Size = New System.Drawing.Size(62, 17)
        Me.rdotidak.TabIndex = 1
        Me.rdotidak.TabStop = True
        Me.rdotidak.Text = "TIDAK"
        Me.rdotidak.UseVisualStyleBackColor = True
        '
        'rdoyamanual
        '
        Me.rdoyamanual.AutoSize = True
        Me.rdoyamanual.Location = New System.Drawing.Point(26, 34)
        Me.rdoyamanual.Name = "rdoyamanual"
        Me.rdoyamanual.Size = New System.Drawing.Size(41, 17)
        Me.rdoyamanual.TabIndex = 0
        Me.rdoyamanual.TabStop = True
        Me.rdoyamanual.Text = "YA"
        Me.rdoyamanual.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(20, 203)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(179, 17)
        Me.Label12.TabIndex = 44
        Me.Label12.Text = "Tampilan diluar rujukan"
        '
        'txtdiluarrange
        '
        Me.txtdiluarrange.Location = New System.Drawing.Point(20, 229)
        Me.txtdiluarrange.Name = "txtdiluarrange"
        Me.txtdiluarrange.Size = New System.Drawing.Size(203, 21)
        Me.txtdiluarrange.TabIndex = 43
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(20, 30)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(117, 17)
        Me.Label13.TabIndex = 42
        Me.Label13.Text = "Jenis hasil test"
        '
        'cbresultType
        '
        Me.cbresultType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbresultType.FormattingEnabled = True
        Me.cbresultType.Location = New System.Drawing.Point(20, 54)
        Me.cbresultType.Name = "cbresultType"
        Me.cbresultType.Size = New System.Drawing.Size(201, 23)
        Me.cbresultType.TabIndex = 41
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(20, 114)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(181, 17)
        Me.Label14.TabIndex = 40
        Me.Label14.Text = "Tampilan dalam rujukan"
        '
        'txtdalamrange
        '
        Me.txtdalamrange.Location = New System.Drawing.Point(20, 148)
        Me.txtdalamrange.Name = "txtdalamrange"
        Me.txtdalamrange.Size = New System.Drawing.Size(203, 21)
        Me.txtdalamrange.TabIndex = 39
        '
        'GroupBox7
        '
        Me.GroupBox7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox7.Controls.Add(Me.txtdiluarrange)
        Me.GroupBox7.Controls.Add(Me.Label12)
        Me.GroupBox7.Controls.Add(Me.txtdalamrange)
        Me.GroupBox7.Controls.Add(Me.Label14)
        Me.GroupBox7.Controls.Add(Me.Label13)
        Me.GroupBox7.Controls.Add(Me.cbresultType)
        Me.GroupBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox7.Location = New System.Drawing.Point(921, 58)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(292, 287)
        Me.GroupBox7.TabIndex = 45
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Report"
        '
        'testTypeFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1283, 590)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgTestType)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.cbGroupChoice)
        Me.Controls.Add(Me.label8)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "testTypeFrm"
        Me.Text = "TEST TYPE"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.dgTestType, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private label8 As System.Windows.Forms.Label
    Friend WithEvents txtHarga As System.Windows.Forms.TextBox

    Friend WithEvents cbGroupChoice As System.Windows.Forms.ComboBox
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents AddMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SaveMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents EditMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents DeleteMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cancelMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents dgTestType As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Private WithEvents Label6 As System.Windows.Forms.Label
    Private WithEvents txtanalyzerTestName As System.Windows.Forms.TextBox
    Private WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cbTestGroup As System.Windows.Forms.ComboBox
    Private WithEvents label5 As System.Windows.Forms.Label
    Private WithEvents label2 As System.Windows.Forms.Label
    Private WithEvents label1 As System.Windows.Forms.Label
    Private WithEvents label4 As System.Windows.Forms.Label
    Private WithEvents label3 As System.Windows.Forms.Label
    Private WithEvents txtKet As System.Windows.Forms.TextBox
    Private WithEvents txtNilaiRujukan As System.Windows.Forms.TextBox
    Private WithEvents txtSatuan As System.Windows.Forms.TextBox
    Private WithEvents txtTestName As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents hargaLevel6 As System.Windows.Forms.TextBox
    Friend WithEvents hargaLevel5 As System.Windows.Forms.TextBox
    Friend WithEvents hargaLevel4 As System.Windows.Forms.TextBox
    Friend WithEvents hargaLevel3 As System.Windows.Forms.TextBox
    Friend WithEvents hargaLevel2 As System.Windows.Forms.TextBox
    Private WithEvents Label19 As System.Windows.Forms.Label
    Private WithEvents Label18 As System.Windows.Forms.Label
    Private WithEvents Label17 As System.Windows.Forms.Label
    Private WithEvents Label16 As System.Windows.Forms.Label
    Private WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cbdigit As System.Windows.Forms.ComboBox
    Friend WithEvents txturutan As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents showurutan As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rdorelated As System.Windows.Forms.RadioButton
    Friend WithEvents rdofree As System.Windows.Forms.RadioButton
    Friend WithEvents txtrelatedformula As System.Windows.Forms.TextBox
    Friend WithEvents cbrelatedX As System.Windows.Forms.ComboBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents cbrelatedY As System.Windows.Forms.ComboBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents cbrelatedZ As System.Windows.Forms.ComboBox
    Private WithEvents Label10 As System.Windows.Forms.Label
    Private WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents txtmaxchild As System.Windows.Forms.TextBox
    Friend WithEvents txtminchild As System.Windows.Forms.TextBox
    Private WithEvents Label28 As System.Windows.Forms.Label
    Private WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtmaxwoman As System.Windows.Forms.TextBox
    Friend WithEvents txtminwoman As System.Windows.Forms.TextBox
    Private WithEvents Label15 As System.Windows.Forms.Label
    Private WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtmin As System.Windows.Forms.TextBox
    Friend WithEvents txtmax As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cbspeciment As System.Windows.Forms.ComboBox
    Friend WithEvents lblguide As System.Windows.Forms.Label
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents rdotidak As System.Windows.Forms.RadioButton
    Friend WithEvents rdoyamanual As System.Windows.Forms.RadioButton
    Private WithEvents Label12 As Label
    Friend WithEvents txtdiluarrange As TextBox
    Private WithEvents Label13 As Label
    Friend WithEvents cbresultType As ComboBox
    Private WithEvents Label14 As Label
    Friend WithEvents txtdalamrange As TextBox
    Friend WithEvents GroupBox7 As GroupBox
End Class
