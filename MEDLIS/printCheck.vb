﻿Imports System.Data
Imports System.Data.SqlClient

Public Class printCheck
    Dim greobject As clsGreConnect
    Dim dtable As DataTable
    Dim selectedLabNumber As String
    Dim selectedPatient As String
    Dim selsampleno As String
    Dim patientName As String

    Dim patientPhone As String
    Dim patientaddress As String

    Dim stateChoice As Integer '0 new, 1 sampling, 2 finish, 8 LAGI DI BACA, 3 semua


    Private Sub jobDetailFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
        CekLicense()
        'tsprintnot.Items.Add("NOT PRINT")
        'tsprintnot.Items.Add("PRINT")
        'tsprintnot.SelectedIndex = 0

        ShowDataDetail()
    End Sub
    Private Sub ShowButton()
        'mnuCetak.Enabled = True
    End Sub

    Private Sub HideButton()
        btnSudahPrint.Enabled = False
    End Sub

    Private Sub ContructDgv()
        dgvdetail.ColumnCount = 4
        dgvdetail.Columns(0).Name = "SampleNo"
        dgvdetail.Columns(0).HeaderText = "Sample Number"
        dgvdetail.Columns(0).Width = 80
        ' dgvdetail.Columns(0).HeaderText.

        dgvdetail.Columns(1).Name = "Barcode"
        dgvdetail.Columns(1).HeaderText = "Barcode"
        dgvdetail.Columns(1).Width = 80

        dgvdetail.Columns(2).Name = "Status"
        dgvdetail.Columns(2).HeaderText = "Status"
        dgvdetail.Columns(2).Width = 80

        dgvdetail.Columns(3).Name = "TestName"
        dgvdetail.Columns(3).HeaderText = "Nama Test"
        dgvdetail.Columns(3).Width = 120


        dgvdetail.AllowUserToAddRows = False
        dgvdetail.RowHeadersVisible = False
    End Sub


    Private Sub Joblist(ByVal strsql As String)
        'Dim strsql As String
        'strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.id as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.status='0'"
        greobject = New clsGreConnect
        greobject.buildConn()

        dtable = New DataTable

        dtable = greobject.ExecuteQuery(strsql)

        'dgvjoblist = New DataGridView

        dgvJoblist.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvJoblist.Columns.Count - 1
            dgvJoblist.Columns(j).Visible = False
        Next

        dgvJoblist.Columns("labnumber").Visible = True
        dgvJoblist.Columns("labnumber").HeaderText = "Lab Number"
        dgvJoblist.Columns("labnumber").Width = 80

        dgvJoblist.Columns("name").Visible = True
        dgvJoblist.Columns("name").HeaderText = "Nama pasien"
        dgvJoblist.Columns("name").Width = 180

        dgvJoblist.Columns("datefinish").Visible = True
        dgvJoblist.Columns("datefinish").HeaderText = "Tanggal Selesai"
        dgvJoblist.Columns("datefinish").Width = 140
        dgvJoblist.Columns("datefinish").ReadOnly = True

        dgvJoblist.Columns("address").Visible = True
        dgvJoblist.Columns("address").HeaderText = "Alamat"
        dgvJoblist.Columns("address").Width = 230

        Dim buttonColumn As New DataGridViewButtonColumn()
        With buttonColumn
            .Name = "Detail"
            .HeaderText = "Detail"
            .Width = 90
            .Text = "Detail"
            .UseColumnTextForButtonValue = True
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            .FlatStyle = FlatStyle.Standard
            .CellTemplate.Style.BackColor = Color.Yellow
            .DisplayIndex = 0
        End With
        dgvJoblist.Columns.Add(buttonColumn)

        dgvJoblist.AllowUserToAddRows = False
        dgvJoblist.RowHeadersVisible = False
    End Sub

    Private Sub dgvJoblist_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvJoblist.CellContentClick
        Dim idx As Integer
        idx = dgvJoblist.Columns("Detail").Index
        txtlabnum.Text = ""
        If e.RowIndex < 0 OrElse Not e.ColumnIndex = dgvJoblist.Columns("Detail").Index Then

            Return

        Else

            If Not dgvJoblist.CurrentRow.IsNewRow Then

                selectedLabNumber = dgvJoblist.Item("labnumber", dgvJoblist.CurrentRow.Index).Value
                txtlabnum.Text = selectedLabNumber
                selectedPatient = dgvJoblist.Item("patientid", dgvJoblist.CurrentRow.Index).Value
                patientName = dgvJoblist.Item("name", dgvJoblist.CurrentRow.Index).Value
                patientaddress = dgvJoblist.Item("address", dgvJoblist.CurrentRow.Index).Value
                patientPhone = dgvJoblist.Item("phone", dgvJoblist.CurrentRow.Index).Value
                selsampleno = ""
                PopulatedgvDetail()
                ShowDetail()
            End If

        End If
    End Sub
    Private Sub ShowDetail()
        txtname.ReadOnly = False
        txtaddress.ReadOnly = False
        txttelepon.ReadOnly = False
        txtlabnum.ReadOnly = False


        txtname.Text = patientName
        txtaddress.Text = patientaddress
        txttelepon.Text = patientPhone

        txtlabnum.ReadOnly = True
        txtname.ReadOnly = True
        txtaddress.ReadOnly = True
        txttelepon.ReadOnly = True
        txtsn.ReadOnly = False
        txtbarcode.ReadOnly = False

        Dim strsql As String
        strsql = "select distinct sampleno,barcode from jobdetail where labnumber='" & selectedLabNumber & "'"
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim cmd As New SqlClient.SqlCommand(strsql, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("sampleno")) Then
                    txtsn.Text = rdr("sampleno")
                End If
                If Not IsDBNull(rdr("barcode")) Then
                    txtbarcode.Text = rdr("barcode")
                End If
            Loop
        End If
        txtsn.ReadOnly = True
        txtbarcode.ReadOnly = True
        rdr.Close()
        cmd.Dispose()
        ogre.CloseConn()
    End Sub


    Private Sub PopulatedgvDetail()
        '
        Dim objGreConnect As New clsGreConnect
        objGreConnect.buildConn()

        Dim dt As New DataTable
        dgvdetail.DataSource = Nothing



        Dim strsql As String
        'strsql = "select jobdetail.labnumber,jobdetail.sampleno,jobdetail.universaltest,jobdetail.measurementvalue from jobdetail where jobdetail.status='2' and jobdetail.active='1' and labnumber not in (select distinct labnumber from jobdetail where status<>'2' and active='1')"
        'strsql = "select distinct job.labnumber,job.idpasien,patient.patientname,jobdetail.labnumber from job,patient,jobdetail where job.idpasien=patient.id and job.labnumber=jobdetail.labnumber and job.[print]<>'1' and job.labnumber not in (select distinct labnumber from jobdetail where status<>'2' and active='1')"
        strsql = "select distinct sampleno,barcode,status,universaltest,measurementvalue,lowercritical,lowernormal,uppernormal,uppercritical,resultstatus,resultabnormalflag from jobdetail where labnumber='" & selectedLabNumber & "' and active='1' and status='2'"
        Dim tblData As New DataTable

        tblData = objGreConnect.ExecuteQuery(strsql) ' order by testgroupname asc")
        dgvdetail.DataSource = tblData

        For j = 0 To dgvdetail.Columns.Count - 1
            dgvdetail.Columns(j).Visible = False
        Next

        dgvdetail.Columns("universaltest").Visible = True
        dgvdetail.Columns("universaltest").HeaderText = "Test Type"
        dgvdetail.Columns("universaltest").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("universaltest").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("universaltest").Width = 100
        dgvdetail.Columns("universaltest").ReadOnly = True

        dgvdetail.Columns("measurementvalue").Visible = True
        dgvdetail.Columns("measurementvalue").HeaderText = "Nilai Hasil"
        dgvdetail.Columns("measurementvalue").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("measurementvalue").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("measurementvalue").Width = 120
        dgvdetail.Columns("measurementvalue").ReadOnly = True

        dgvdetail.Columns("lowercritical").Visible = False
        dgvdetail.Columns("lowercritical").HeaderText = "Lower Critical"
        dgvdetail.Columns("lowercritical").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("lowercritical").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("lowercritical").Width = 80


        dgvdetail.Columns("lowernormal").Visible = False
        dgvdetail.Columns("lowernormal").HeaderText = "Lower Normal"
        dgvdetail.Columns("lowernormal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("lowernormal").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("lowernormal").Width = 80

        dgvdetail.Columns("uppernormal").Visible = False
        dgvdetail.Columns("uppernormal").HeaderText = "Upper Normal"
        dgvdetail.Columns("uppernormal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("uppernormal").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("uppernormal").Width = 80

        dgvdetail.Columns("uppercritical").Visible = False
        dgvdetail.Columns("uppercritical").HeaderText = "Upper Critical"
        dgvdetail.Columns("uppercritical").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("uppercritical").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("uppercritical").Width = 80

        dgvdetail.Columns("resultstatus").Visible = False
        dgvdetail.Columns("resultstatus").HeaderText = "Result Status"
        dgvdetail.Columns("resultstatus").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("resultstatus").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("resultstatus").Width = 80


        dgvdetail.Columns("resultabnormalflag").Visible = True
        dgvdetail.Columns("resultabnormalflag").HeaderText = "Abnormal Flag"
        dgvdetail.Columns("resultabnormalflag").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("resultabnormalflag").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("resultabnormalflag").Width = 80
        dgvdetail.Columns("resultabnormalflag").ReadOnly = True

        dgvdetail.AllowUserToAddRows = False
        dgvdetail.RowHeadersVisible = False

        objGreConnect.CloseConn()

    End Sub
    Private Function GetUniversalTestName(ByVal sampleno As String, ByVal labnumber As String) As String
        Dim strfinder As String
        Dim strresult As String
        Dim testcon As New clsGreConnect
        testcon.buildConn()
        strfinder = "select universaltest from jobdetail where sampleno='" & sampleno & "' and labnumber='" & labnumber & "'"
        Dim thecommand As New SqlClient.SqlCommand(strfinder, testcon.grecon)
        Dim theReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)
        strresult = ""
        Do While theReader.Read
            strresult = strresult & " " & theReader("universaltest")
        Loop
        thecommand = Nothing
        theReader.Close()
        testcon.CloseConn()
        testcon = Nothing

        Return strresult
    End Function





    Private Sub clearDGVdetail()
        'If dgvdetail.Rows.Count > 0 Then
        '    dgvdetail.Rows.Clear()
        'End If
    End Sub







    Private Sub ShowDataDetail()
        stateChoice = FINISHSAMPLE
        ShowButton()
        Dim strsql As String
        ' strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail where status='" & FINISHSAMPLE & "' and active='" & ACTIVESAMPLE & "')"
        
        strsql = "select distinct job.labnumber,job.datefinish,job.idpasien,patient.phone,patient.patientname as name,patient.address,patient.idpatient as patientid,jobdetail.labnumber from job,patient,jobdetail where job.idpasien=patient.id and job.labnumber=jobdetail.labnumber and job.[print]<>'" & PRINTALREADY & "' and job.labnumber not in (select distinct labnumber from jobdetail where status<>'" & FINISHSAMPLE & "' and active='" & ACTIVESAMPLE & "') order by job.datefinish desc"


        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If

        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If
        clearDGVdetail()
        Joblist(strsql)
        lblStatus.Text = "Selesai/Sudah ada hasil"
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rptlabreport As New rptLabReportFrm(selectedLabNumber, selectedPatient)
        rptlabreport.MdiParent = MainFrm
        rptlabreport.Show()
    End Sub

    Private Sub tsprintnot_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ShowDataDetail()
    End Sub

    Private Sub tsprintnot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnSudahPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSudahPrint.Click
        Try
            Dim strprint As String
            strprint = "update job set print='" & PRINTALREADY & "' where labnumber='" & selectedLabNumber & "'"

            Dim ogre As New clsGreConnect
            ogre.buildConn()
            ogre.ExecuteNonQuery(strprint)
            ogre.CloseConn()

            ShowDataDetail()

        Catch ex As Exception
            MessageBox.Show("Ada kesalahan saat update data")
        End Try
        

    End Sub
End Class