﻿Public Class RptResultFrm

    Private Sub RptResultFrm_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            Dim strcon As String
            strcon = "Driver={PostgreSQL ANSI};database=MEDLIS;server=127.0.0.1;port=5432;uid=dian;sslmode=disable;readonly=0;protocol=7.4;User ID=dian;password=dianjuga;"

            Dim rdlView As New fyiReporting.RdlViewer.RdlViewer
            rdlView.SourceFile = New Uri("D:\Medical\VBMEDLIS\MEDLIS\MEDLIS\Reporting\ResultReport.rdl")

            rdlView.Parameters = String.Format("&constr={0}", strcon)
            rdlView.Rebuild()

            rdlView.Dock = DockStyle.Fill
            Me.Controls.Add(rdlView)

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub

    Private Sub RptResultFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
    End Sub
End Class