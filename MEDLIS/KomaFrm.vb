﻿Imports System.Data
Imports System.Data.SqlClient
Public Class KomaFrm
    ReadOnly AllowedKeys As String = _
   "0123456789"
    Private Sub ComboBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ComboBox1.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)

        End Select
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

        Try
            If ComboBox1.Text <> "" And IsNumeric(ComboBox1.Text) = True Then
                Label2.Text = "Contoh Hasil: " & Math.Round(1.23456, CInt(Trim(ComboBox1.Text)), MidpointRounding.AwayFromZero)
            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Terjadi Error")
        End Try
    End Sub

    Private Sub KomaFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
        LoadDigitSetting()
    End Sub

    Private Sub LoadDigitSetting()
        Dim strsetting As String
        strsetting = "select * from setting where name='digit'"
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim cmd As New SqlClient.SqlCommand(strsetting, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                ComboBox1.SelectedIndex = ComboBox1.FindStringExact(rdr("value"))
            Loop
        Else
            ComboBox1.SelectedIndex = 1
            MessageBox.Show("Silahkan pilih jumlah digit result")
        End If
        cmd.Dispose()
        ogre.CloseConn()
    End Sub

    Private Sub ComboBox1_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedValueChanged

    End Sub

    Private Sub ComboBox1_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.Validated

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSimpan.Click
        Try
            If ComboBox1.Text <> "" And IsNumeric(ComboBox1.Text) = True Then
                If CInt(ComboBox1.Text) > 0 And CInt(ComboBox1.Text) < 6 Then
                    Dim ogre As New clsGreConnect
                    ogre.buildConn()

                    Dim strsql As String
                    Dim strdelete As String

                    Dim name As String = "digit"
                    Dim val As String
                    val = ComboBox1.Text

                    strdelete = "delete from setting where name='digit'"
                    ogre.ExecuteNonQuery(strdelete)
                    ogre.CloseConn()

                    Dim ogreINSERT As New clsGreConnect
                    ogreINSERT.buildConn()
                    digitResult = val
                    strsql = "insert into setting(name,value)values('" & name & "','" & val & "')"
                    ogreINSERT.ExecuteNonQuery(strsql)
                    ogreINSERT.CloseConn()
                    Label2.Text = " Berhasil disimpan"
                    Label2.ForeColor = Color.Blue
                Else
                    Label2.Text = "Silahkan pilih antara 1 - 5"
                    Label2.ForeColor = Color.Red
                End If

            Else
                Label2.Text = "Silahkan pilih antara 1 - 5"
                Label2.ForeColor = Color.Red
            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Terjadi Error")
        End Try

    End Sub
End Class