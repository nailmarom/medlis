﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AllFinishJobsFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AllFinishJobsFrm))
        Me.dgvJoblist = New System.Windows.Forms.DataGridView
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtname = New System.Windows.Forms.TextBox
        Me.txtaddress = New System.Windows.Forms.TextBox
        Me.txttelepon = New System.Windows.Forms.TextBox
        Me.dgvdetail = New System.Windows.Forms.DataGridView
        Me.lblStatus = New System.Windows.Forms.Label
        Me.txtlabnum = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.mnuCetak = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.tsprintnot = New System.Windows.Forms.ToolStripComboBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Barcode = New System.Windows.Forms.Label
        Me.txtbarcode = New System.Windows.Forms.TextBox
        Me.txtsn = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.rd30 = New System.Windows.Forms.RadioButton
        Me.rd80 = New System.Windows.Forms.RadioButton
        Me.rdsemua = New System.Windows.Forms.RadioButton
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        CType(Me.dgvJoblist, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvdetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvJoblist
        '
        Me.dgvJoblist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvJoblist.Location = New System.Drawing.Point(6, 116)
        Me.dgvJoblist.Name = "dgvJoblist"
        Me.dgvJoblist.Size = New System.Drawing.Size(544, 460)
        Me.dgvJoblist.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(73, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Nama"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(67, 78)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(51, 15)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Alamat"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(59, 106)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 15)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Telepon"
        '
        'txtname
        '
        Me.txtname.Location = New System.Drawing.Point(121, 47)
        Me.txtname.Name = "txtname"
        Me.txtname.Size = New System.Drawing.Size(243, 21)
        Me.txtname.TabIndex = 12
        '
        'txtaddress
        '
        Me.txtaddress.Location = New System.Drawing.Point(121, 77)
        Me.txtaddress.Name = "txtaddress"
        Me.txtaddress.Size = New System.Drawing.Size(353, 21)
        Me.txtaddress.TabIndex = 13
        '
        'txttelepon
        '
        Me.txttelepon.Location = New System.Drawing.Point(121, 107)
        Me.txttelepon.Name = "txttelepon"
        Me.txttelepon.Size = New System.Drawing.Size(243, 21)
        Me.txttelepon.TabIndex = 14
        '
        'dgvdetail
        '
        Me.dgvdetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvdetail.Location = New System.Drawing.Point(556, 257)
        Me.dgvdetail.Name = "dgvdetail"
        Me.dgvdetail.Size = New System.Drawing.Size(647, 319)
        Me.dgvdetail.TabIndex = 15
        '
        'lblStatus
        '
        Me.lblStatus.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(6, 83)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblStatus.Size = New System.Drawing.Size(218, 27)
        Me.lblStatus.TabIndex = 24
        Me.lblStatus.Text = "....."
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtlabnum
        '
        Me.txtlabnum.Location = New System.Drawing.Point(121, 17)
        Me.txtlabnum.Name = "txtlabnum"
        Me.txtlabnum.Size = New System.Drawing.Size(243, 21)
        Me.txtlabnum.TabIndex = 27
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(32, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 15)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "Lab Number"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCetak, Me.ToolStripSeparator1, Me.tsprintnot})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1213, 46)
        Me.ToolStrip1.TabIndex = 33
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'mnuCetak
        '
        Me.mnuCetak.Image = Global.MEDLIS.My.Resources.Resources.print_go_hover
        Me.mnuCetak.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuCetak.Name = "mnuCetak"
        Me.mnuCetak.Size = New System.Drawing.Size(73, 43)
        Me.mnuCetak.Text = "Cetak"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 46)
        '
        'tsprintnot
        '
        Me.tsprintnot.Name = "tsprintnot"
        Me.tsprintnot.Size = New System.Drawing.Size(121, 46)
        Me.tsprintnot.Tag = ""
        Me.tsprintnot.ToolTipText = "Urutkan berdasar yang sudah cetak atau belum cetak"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Barcode)
        Me.GroupBox1.Controls.Add(Me.txtbarcode)
        Me.GroupBox1.Controls.Add(Me.txtsn)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtlabnum)
        Me.GroupBox1.Controls.Add(Me.txttelepon)
        Me.GroupBox1.Controls.Add(Me.txtaddress)
        Me.GroupBox1.Controls.Add(Me.txtname)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(556, 55)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(647, 196)
        Me.GroupBox1.TabIndex = 34
        Me.GroupBox1.TabStop = False
        '
        'Barcode
        '
        Me.Barcode.AutoSize = True
        Me.Barcode.Location = New System.Drawing.Point(58, 165)
        Me.Barcode.Name = "Barcode"
        Me.Barcode.Size = New System.Drawing.Size(60, 15)
        Me.Barcode.TabIndex = 33
        Me.Barcode.Text = "Barcode"
        '
        'txtbarcode
        '
        Me.txtbarcode.Location = New System.Drawing.Point(121, 161)
        Me.txtbarcode.Name = "txtbarcode"
        Me.txtbarcode.Size = New System.Drawing.Size(142, 21)
        Me.txtbarcode.TabIndex = 32
        '
        'txtsn
        '
        Me.txtsn.Location = New System.Drawing.Point(121, 134)
        Me.txtsn.Name = "txtsn"
        Me.txtsn.Size = New System.Drawing.Size(142, 21)
        Me.txtsn.TabIndex = 31
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(15, 137)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(103, 15)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "Nomor Sample"
        '
        'rd30
        '
        Me.rd30.AutoSize = True
        Me.rd30.Location = New System.Drawing.Point(15, 21)
        Me.rd30.Name = "rd30"
        Me.rd30.Size = New System.Drawing.Size(41, 19)
        Me.rd30.TabIndex = 36
        Me.rd30.TabStop = True
        Me.rd30.Text = "30"
        Me.rd30.UseVisualStyleBackColor = True
        '
        'rd80
        '
        Me.rd80.AutoSize = True
        Me.rd80.Location = New System.Drawing.Point(77, 21)
        Me.rd80.Name = "rd80"
        Me.rd80.Size = New System.Drawing.Size(41, 19)
        Me.rd80.TabIndex = 37
        Me.rd80.TabStop = True
        Me.rd80.Text = "80"
        Me.rd80.UseVisualStyleBackColor = True
        '
        'rdsemua
        '
        Me.rdsemua.AutoSize = True
        Me.rdsemua.Location = New System.Drawing.Point(134, 21)
        Me.rdsemua.Name = "rdsemua"
        Me.rdsemua.Size = New System.Drawing.Size(68, 19)
        Me.rdsemua.TabIndex = 38
        Me.rdsemua.TabStop = True
        Me.rdsemua.Text = "semua"
        Me.rdsemua.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rdsemua)
        Me.GroupBox2.Controls.Add(Me.rd80)
        Me.GroupBox2.Controls.Add(Me.rd30)
        Me.GroupBox2.Location = New System.Drawing.Point(300, 55)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(250, 55)
        Me.GroupBox2.TabIndex = 39
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Lab number yang ingin ditampilkan"
        '
        'AllFinishJobsFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1213, 585)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.dgvdetail)
        Me.Controls.Add(Me.dgvJoblist)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "AllFinishJobsFrm"
        Me.Text = "Daftar Pekerjaan Yang Sudah Selesai"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvJoblist, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvdetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvJoblist As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtname As System.Windows.Forms.TextBox
    Friend WithEvents txtaddress As System.Windows.Forms.TextBox
    Friend WithEvents txttelepon As System.Windows.Forms.TextBox
    Friend WithEvents dgvdetail As System.Windows.Forms.DataGridView
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents txtlabnum As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents mnuCetak As System.Windows.Forms.ToolStripButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsprintnot As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtsn As System.Windows.Forms.TextBox
    Friend WithEvents rd30 As System.Windows.Forms.RadioButton
    Friend WithEvents rd80 As System.Windows.Forms.RadioButton
    Friend WithEvents rdsemua As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Barcode As System.Windows.Forms.Label
    Friend WithEvents txtbarcode As System.Windows.Forms.TextBox
End Class
