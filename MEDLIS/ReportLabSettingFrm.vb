﻿'
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Public Class ReportLabSettingFrm
    Dim strsql As String
    Dim isedit As Boolean
    Dim rcdpresent As Boolean
    Dim imagechange As Boolean

    Private Sub ReportLabSettingFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CekLicense()
        LoadLabInfo()
        imagechange = False
        isedit = False
        CekState()
    End Sub
    Private Sub enableText()
        For Each ctrl As Control In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then 'bagus
                Dim txt As TextBox = CType(ctrl, TextBox)
                'txt.Text = "" 'BackColor = Color.LightYellow
                txt.Enabled = True
            End If
        Next
    End Sub
    Private Sub disableText()

        For Each ctrl As Control In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then 'bagus
                Dim txt As TextBox = CType(ctrl, TextBox)
                'txt.Text = "" 'BackColor = Color.LightYellow
                txt.Enabled = False
            End If
        Next
    End Sub


    Private Sub LoadLabInfo()
        enableText()
        Dim ogre As New clsGreConnect
        rcdpresent = False
        ogre.buildConn()
        strsql = "select * from labinfo"
        Dim cmd As New SqlClient.SqlCommand(strsql, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            If Not IsDBNull(rdr("labname")) Then
                GLabName = rdr("labname")
            Else
                GLabName = ""
            End If

            If Not IsDBNull(rdr("address1")) Then
                GLabAddress1 = rdr("address1")
            Else
                GLabAddress1 = ""
            End If

            If Not IsDBNull(rdr("address2")) Then
                GLabAddress2 = rdr("address2")
            Else
                GLabAddress2 = ""
            End If

            If Not IsDBNull(rdr("labphone")) Then
                GLabPhone = rdr("labphone")
            Else
                GLabPhone = ""
            End If


            If Not IsDBNull(rdr("labfax")) Then
                GLabFax = rdr("labfax")
            Else
                GLabFax = ""
            End If

            If Not IsDBNull(rdr("labmaster")) Then
                GLabMaster = rdr("labmaster")
            Else
                GLabMaster = ""
            End If

            If Not IsDBNull(rdr("footerdesc")) Then
                GLabFooterNote = rdr("footerdesc")
            Else
                GLabFooterNote = ""
            End If

            If Not IsDBNull(rdr("logo")) Then
                Dim logoByte As Byte() = DirectCast(rdr("logo"), Byte())
                Using ms As MemoryStream = New MemoryStream(logoByte)
                    Try
                        picLogo.Image = New Bitmap(ms)
                    Catch ex As Exception

                    End Try
                End Using
            End If

            txtNamaLab.Text = GLabName
            txtAlamat1.Text = GLabAddress1
            txtAlamat2.Text = GLabAddress2
            txtTelp.Text = GLabPhone
            txtFax.Text = GLabFax
            txtPenanggungJawab.Text = GLabMaster
            txtReportFooter.Text = GLabFooterNote

            rcdpresent = True
        Loop

        'Try - 
        '    pictbox.Image = Image.FromFile(IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.MyPictures, "ReportImageFile.jpg"))
        'Catch exp As IO.FileNotFoundException
        '    lblMessage.Text = "File not found"
        'End Try

        'If (Image.FromFile(IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.MyPictures, "ReportImageFile.jpg"))) IsNot Nothing Then
        '    pictbox.Image = Image.FromFile(IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.MyPictures, "ReportImageFile.jpg"))
        'End If
        cmd.Dispose()
        rdr.Close()
        disableText()
    End Sub

    Private Sub EditMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditMnu.Click

        isedit = True
        CekState()
    End Sub

    Private Sub CekState()
        If isedit = True Then
            SaveMnu.Enabled = True
            enableText()
            ' btnBrowsImage.Enabled = True
            cancelMnu.Enabled = True
            btnBrowse.Enabled = True
        ElseIf isedit = False Then
            SaveMnu.Enabled = False
            disableText()
            cancelMnu.Enabled = False
            btnBrowse.Enabled = False
            'btnBrowsImage.Enabled = False
        End If

    End Sub

    Private Sub cancelMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelMnu.Click
        LoadLabInfo()
        isedit = False
        CekState()
    End Sub

    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click
        'save labinfo
        GLabName = txtNamaLab.Text
        GLabAddress1 = txtAlamat1.Text
        GLabAddress2 = txtAlamat2.Text
        GLabPhone = txtTelp.Text
        GLabFax = txtFax.Text
        GLabMaster = txtPenanggungJawab.Text
        GLabFooterNote = txtReportFooter.Text

        If rcdpresent = True Then
            strsql = "update labinfo set labname='" & GLabName &
            "',address1='" & GLabAddress1 &
            "',address2='" & GLabAddress2 &
            "',labphone='" & GLabPhone &
            "',labfax='" & GLabFax &
            "',labmaster='" & GLabMaster &
            "',footerdesc='" & GLabFooterNote & "'" &
            ",logo=?"
        Else
            strsql = "insert into labinfo(labname,address1,address2,labphone,labfax,labmaster,footerdesc,logo) values('" & GLabName & "','" & GLabAddress1 & "','" & GLabAddress2 & "','" & GLabPhone & "','" & GLabFax & "','" & GLabMaster & "','" & GLabFooterNote & "',?)"
        End If
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim ms As New MemoryStream

        Dim params As New List(Of SqlClient.SqlParameter)
        Dim pLogo As New SqlClient.SqlParameter()
        pLogo.ParameterName = "logo"
        'pLogo.DbType = DbType.Binary
        pLogo.SqlDbType = SqlDbType.Binary

        If picLogo.Image IsNot Nothing Then
            picLogo.Image.Save(ms, Imaging.ImageFormat.Png)
            pLogo.Value = ms.GetBuffer()
        Else
            pLogo.Value = DBNull.Value
        End If

        params.Add(pLogo)

        ogre.ExecuteNonQuery(strsql, params)
        ogre.CloseConn()
        'If imagechange Then
        '    SaveImage()
        'End If

        isedit = False
        CekState()

        '   "',REASON='" & Trim(txtreason.Text) & _
        '   "',TOTALIDR='" & txttotalidr.Text & _
        '   "',TOTALUSD='" & txttotalusd.Text & _
        '   "',USERID='" & LogOnUserID & _

    End Sub
    'Private Sub SaveImage()
    '    If Me.pictbox.Image IsNot Nothing Then
    '        Me.pictbox.Image.Save(IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.MyPictures, "ReportImageFile.jpg")) 'path report image penting
    '        imagechange = False

    '    End If
    'End Sub
    Private Sub btnBrowsImage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Using OpenFileDialog1 As New OpenFileDialog
            With OpenFileDialog1
                .CheckFileExists = True
                .ShowReadOnly = False
                .Filter = "All Files|*.*|Bitmap Files (*)|*.bmp;*.gif;*.jpg"
                .FilterIndex = 2

                If .ShowDialog = DialogResult.OK Then
                    ' pictbox.Image = New Bitmap(Image.FromFile(.FileName)) 'bagus untuk A generic error occurred in GDI+
                    imagechange = True
                    'lblmessage.Text = ""
                End If
            End With
        End Using
    End Sub
    ReadOnly AllowedKeys As String =
  "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789. "


    Private Sub check_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAlamat2.KeyPress, txtAlamat1.KeyPress, txtFax.KeyPress, txtNamaLab.KeyPress, txtPenanggungJawab.KeyPress, txtReportFooter.KeyPress, txtTelp.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)

        End Select
    End Sub

    Private Sub btnBrowse_Click(sender As Object, e As EventArgs) Handles btnBrowse.Click
        Dim result As DialogResult = ofd.ShowDialog()
        If result = DialogResult.OK Then
            Dim filepath As String = ofd.FileName
            picLogo.ImageLocation = filepath
            picLogo.Refresh()
        End If
    End Sub
End Class