﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class KonfigureDiskonFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(KonfigureDiskonFrm))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtPaymentName = New System.Windows.Forms.TextBox
        Me.dpend = New System.Windows.Forms.DateTimePicker
        Me.dpstart = New System.Windows.Forms.DateTimePicker
        Me.chkactive = New System.Windows.Forms.CheckBox
        Me.txtpercent = New System.Windows.Forms.TextBox
        Me.txtdiscname = New System.Windows.Forms.TextBox
        Me.dgvdiscount = New System.Windows.Forms.DataGridView
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.AddMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.SaveMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.EditMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.DeleteMnu = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator
        Me.cancelMnu = New System.Windows.Forms.ToolStripButton
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvdiscount, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtPaymentName)
        Me.GroupBox1.Controls.Add(Me.dpend)
        Me.GroupBox1.Controls.Add(Me.dpstart)
        Me.GroupBox1.Controls.Add(Me.chkactive)
        Me.GroupBox1.Controls.Add(Me.txtpercent)
        Me.GroupBox1.Controls.Add(Me.txtdiscname)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 60)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(280, 299)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Payment Detail"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 141)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 14)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Percent"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(106, 14)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Nama Program"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(132, 14)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Nama Pembayaran"
        '
        'txtPaymentName
        '
        Me.txtPaymentName.Location = New System.Drawing.Point(16, 52)
        Me.txtPaymentName.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPaymentName.Name = "txtPaymentName"
        Me.txtPaymentName.Size = New System.Drawing.Size(245, 22)
        Me.txtPaymentName.TabIndex = 7
        '
        'dpend
        '
        Me.dpend.Location = New System.Drawing.Point(16, 245)
        Me.dpend.Margin = New System.Windows.Forms.Padding(4)
        Me.dpend.Name = "dpend"
        Me.dpend.Size = New System.Drawing.Size(232, 22)
        Me.dpend.TabIndex = 6
        '
        'dpstart
        '
        Me.dpstart.Location = New System.Drawing.Point(16, 217)
        Me.dpstart.Margin = New System.Windows.Forms.Padding(4)
        Me.dpstart.Name = "dpstart"
        Me.dpstart.Size = New System.Drawing.Size(232, 22)
        Me.dpstart.TabIndex = 5
        '
        'chkactive
        '
        Me.chkactive.AutoSize = True
        Me.chkactive.Location = New System.Drawing.Point(16, 191)
        Me.chkactive.Margin = New System.Windows.Forms.Padding(4)
        Me.chkactive.Name = "chkactive"
        Me.chkactive.Size = New System.Drawing.Size(128, 18)
        Me.chkactive.TabIndex = 4
        Me.chkactive.Text = "Discount Active"
        Me.chkactive.UseVisualStyleBackColor = True
        '
        'txtpercent
        '
        Me.txtpercent.Location = New System.Drawing.Point(16, 159)
        Me.txtpercent.Margin = New System.Windows.Forms.Padding(4)
        Me.txtpercent.Name = "txtpercent"
        Me.txtpercent.Size = New System.Drawing.Size(80, 22)
        Me.txtpercent.TabIndex = 3
        '
        'txtdiscname
        '
        Me.txtdiscname.Location = New System.Drawing.Point(16, 102)
        Me.txtdiscname.Margin = New System.Windows.Forms.Padding(4)
        Me.txtdiscname.Name = "txtdiscname"
        Me.txtdiscname.Size = New System.Drawing.Size(245, 22)
        Me.txtdiscname.TabIndex = 2
        '
        'dgvdiscount
        '
        Me.dgvdiscount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvdiscount.Location = New System.Drawing.Point(299, 65)
        Me.dgvdiscount.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvdiscount.Name = "dgvdiscount"
        Me.dgvdiscount.Size = New System.Drawing.Size(594, 293)
        Me.dgvdiscount.TabIndex = 1
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddMnu, Me.ToolStripSeparator5, Me.SaveMnu, Me.ToolStripSeparator6, Me.EditMnu, Me.ToolStripSeparator7, Me.DeleteMnu, Me.ToolStripSeparator8, Me.cancelMnu})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(895, 43)
        Me.ToolStrip1.TabIndex = 24
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'AddMnu
        '
        Me.AddMnu.Image = Global.MEDLIS.My.Resources.Resources.plus
        Me.AddMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.AddMnu.Name = "AddMnu"
        Me.AddMnu.Size = New System.Drawing.Size(68, 40)
        Me.AddMnu.Text = "Add"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 43)
        '
        'SaveMnu
        '
        Me.SaveMnu.Image = Global.MEDLIS.My.Resources.Resources.save
        Me.SaveMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SaveMnu.Name = "SaveMnu"
        Me.SaveMnu.Size = New System.Drawing.Size(76, 40)
        Me.SaveMnu.Text = "Save"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 43)
        '
        'EditMnu
        '
        Me.EditMnu.Image = Global.MEDLIS.My.Resources.Resources.write_edit
        Me.EditMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.EditMnu.Name = "EditMnu"
        Me.EditMnu.Size = New System.Drawing.Size(68, 40)
        Me.EditMnu.Text = "Edit"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(6, 43)
        '
        'DeleteMnu
        '
        Me.DeleteMnu.AutoSize = False
        Me.DeleteMnu.Image = Global.MEDLIS.My.Resources.Resources.cut
        Me.DeleteMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.DeleteMnu.Name = "DeleteMnu"
        Me.DeleteMnu.Size = New System.Drawing.Size(82, 39)
        Me.DeleteMnu.Text = "Delete"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(6, 43)
        '
        'cancelMnu
        '
        Me.cancelMnu.AutoSize = False
        Me.cancelMnu.Image = Global.MEDLIS.My.Resources.Resources.cancel
        Me.cancelMnu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cancelMnu.Name = "cancelMnu"
        Me.cancelMnu.Size = New System.Drawing.Size(85, 39)
        Me.cancelMnu.Text = "Cancel"
        '
        'KonfigureDiskonFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(895, 362)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.dgvdiscount)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "KonfigureDiskonFrm"
        Me.Text = "Konfigurasi Diskon"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvdiscount, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkactive As System.Windows.Forms.CheckBox
    Friend WithEvents txtpercent As System.Windows.Forms.TextBox
    Friend WithEvents txtdiscname As System.Windows.Forms.TextBox
    Friend WithEvents dpend As System.Windows.Forms.DateTimePicker
    Friend WithEvents dpstart As System.Windows.Forms.DateTimePicker
    Friend WithEvents dgvdiscount As System.Windows.Forms.DataGridView
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents AddMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SaveMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents EditMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents DeleteMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cancelMnu As System.Windows.Forms.ToolStripButton
    Friend WithEvents txtPaymentName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
