﻿'
' Created by SharpDevelop.
' User: User
' Date: 10/13/2014
' Time: 1:28 PM
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Imports System.Data.SqlClient

Partial Public Class cariPasienFrm
    Dim isNew As Boolean
    Dim isEdit As Boolean
    Dim objconn As New clsGreConnect
    Dim tblData As DataTable
    Dim objTestRelated As New TestRelated
    Dim strsql As String
    Dim selectedPasienId As Integer
    Dim selectedDateBirth As Date
    Dim tipepasienTable As DataTable


    Public Sub New()
        ' The Me.InitializeComponent call is required for Windows Forms designer support.
        Me.InitializeComponent()

        '
        ' TODO : Add constructor code after InitializeComponents
        ''
    End Sub

    Private Sub cariPasienFrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        isPatientWinOpen = 0
    End Sub

    Private Sub cariPasienFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
        isPatientWinOpen = 1
        Icon = New System.Drawing.Icon("medlis2.ico")
        cbFinder.Items.Add("Nama")
        cbFinder.Items.Add("Tanggal Lahir")
        cbFinder.Items.Add("No Telepon")
        cbFinder.Items.Add("ID Pasien")
        cbFinder.SelectedIndex = 0
        LoadTipePasien()
        PopulateDgv("")
        EditMnu.Enabled = False
        SaveMnu.Enabled = False
        disableText()
    End Sub
    Private Sub LoadTipePasien()
        Dim strtipe As String
        strtipe = "select * from patientpricelevel"

        objconn.buildConn()

        ' Dim thecommand As New SqlClient.SqlCommand(strsql, objconn.grecon)
        ' Dim Reader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)
        tipepasienTable = objconn.ExecuteQuery(strtipe)
        cbtipepasien.DataSource = tipepasienTable
        cbtipepasien.DisplayMember = "display"
        cbtipepasien.ValueMember = "defaultname"

        'Do While Reader.Read
        '    cbtipepasien.Items.Add(Reader("display"))
        'Loop

        cbtipepasien.Enabled = False

        Dim ctrl As Control
        For Each ctrl In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.Enabled = False 'BackColor = Color.LightYellow
            End If
        Next
        'cbtipepasien.SelectedValue = cbtipepasien.Items(cbtipepasien.SelectedIndex)
        'testTypeReader.Close()
        'thecommand.Dispose()
        objconn.CloseConn()
        objconn = Nothing
    End Sub


    Private Sub btnCari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCari.Click
        Dim strcondition As String

        strcondition = ""
    
        'lagi di kerjakan
        If cbFinder.SelectedIndex = 0 Or cbFinder.SelectedIndex = 2 Or cbFinder.SelectedIndex = 3 Then
            If cbFinder.Text = "Nama" Then
                strcondition = "where patientname like '%" & Trim(txtFinder.Text) & "%' "
            
            ElseIf cbFinder.Text = "No Telepon" Then
                strcondition = "where phone like '%" & Trim(txtFinder.Text) & "%' "
            ElseIf cbFinder.Text = "ID Pasien" Then
                strcondition = "where idpatient like '%" & Trim(txtFinder.Text) & "%' "
            End If
            PopulateDgv(strcondition)
        ElseIf cbFinder.SelectedIndex = 1 Then

            Dim birthdate As String
            Dim seldate As Date = Date.Parse(dpfind.Text)
            birthdate = Format(seldate, "yyyy-MM-dd")
            strcondition = "where birthdate='" & birthdate & "%' "
            PopulateDgv(strcondition)
        Else
            strcondition = ""
            PopulateDgv(strcondition)
        End If
    End Sub
    Private Sub PopulateDg()
        'Dim strsql, strcondition As String


        'objconn.buildConn()
        'tblData = New DataTable
        ''lagi di kerjakan
        'If cbFinder.Text = "Nama" Then
        '    strcondition = "patientname like '%" & Trim(txtFinder.Text) & "%' """
        'ElseIf cbFinder.Text = "Tanggal Lahir" Then
        '    strcondition = "patientname like '%" & Trim(txtFinder.Text) & "%' """
        'ElseIf cbFinder.Text = "No Telepon" Then
        '    strcondition = "patientname like '%" & Trim(txtFinder.Text) & "%' """
        'ElseIf cbFinder.Text = "ID Pasien" Then
        '    strcondition = "patientname like '%" & Trim(txtFinder.Text) & "%' """
        'End If
        'strsql = "select * from patient order by patientname asc where " & strcondition

        'tblData = objconn.ExecuteQuery(strsql)

        'dgTestGroup.DataSource = tblGroupTest
        'Dim chk As New DataGridViewCheckBoxColumn()

        'dgTestGroup.Columns.Add(chk)
        'chk.HeaderText = "SELECT"
        'chk.Width = 120
        'chk.Name = "chk"
        'dgTestGroup.Columns("chk").DisplayIndex = 0
        'dgTestGroup.Columns("chk").Name = "chk"

        'dgTestGroup.Columns("testgroupname").HeaderText = "Test Group Name"
        'dgTestGroup.Columns("testgroupname").Width = 250

        'dgTestGroup.Columns("id").Visible = False
        'objconn.CloseConn()


    End Sub
    Private Function ClearApostrofe(ByVal str As String) As String
        Dim result As String
        result = str.Replace("'", " ")

        Return result
    End Function
    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click
        Dim pasienName As String
        Dim address As String
        Dim gender As Integer '1 cowo 0 cewe
        Dim phone As String
        Dim idpatient As String
        Dim email As String
        Dim birthdate As String
        Dim pricelevel As String

        Dim nik As String
        Dim jabatan As String
        Dim instansi As String



        ReadLicense()

        If txtAlamat.Text = "" Or txtNamaPasien.Text = "" Or txtPhone.Text = "" Then
            MessageBox.Show("Mohon isi data dengan lengkap")
            Return
        End If

        If rdoLaki.Checked = False And rdoPerempuan.Checked = False And rdoanak.Checked = False Then
            MessageBox.Show("Mohon isi data dengan lengkap")
            Return
        End If

        nik = txtnip.Text
        jabatan = txtjabatan.Text
        instansi = cbinstansi.Text

        If isNew = True Then

            pasienName = ClearApostrofe(txtNamaPasien.Text)
            address = ClearApostrofe(txtAlamat.Text)
            If rdoLaki.Checked = True Then
                gender = genderMale
            ElseIf rdoPerempuan.Checked = True Then
                gender = genderFemale
            ElseIf rdoanak.Checked = True Then
                gender = genderChild
            End If

            phone = txtPhone.Text
            email = txtEmail.Text
            idpatient = txtIdPasien.Text

            pricelevel = cbtipepasien.SelectedValue
            Dim seldate As Date = Date.Parse(dpBirth.Text)
            birthdate = Format(seldate, "yyyy-MM-dd")   'bagus date

            'IdTestGroup = objTestRelated.GetIdTestGroup(cbTestGroup.Text)
            strsql = "insert into patient (" &
                        "patientname,address,malefemale,phone,idpatient,email,birthdate,pricelevel,nik,jabatan,organization)" &
                        " VALUES (" &
                         "'" & pasienName & "','" & address & "','" & gender & "','" & phone & "','" & idpatient & "','" & email & "','" & birthdate & "','" & pricelevel & "','" & nik & "','" & jabatan & "','" & instansi & "')"
            objconn.buildConn()
            objconn.ExecuteNonQuery(strsql)
            objconn.CloseConn()
        ElseIf isEdit = True Then
            pasienName = ClearApostrofe(txtNamaPasien.Text)
            address = ClearApostrofe(txtAlamat.Text)

            If rdoLaki.Checked = True Then
                gender = genderMale
            ElseIf rdoPerempuan.Checked = True Then
                gender = genderFemale
            ElseIf rdoanak.Checked = True Then
                gender = genderChild
            End If

            pricelevel = cbtipepasien.SelectedValue
            phone = txtPhone.Text
            idpatient = txtIdPasien.Text
            email = txtEmail.Text
            Dim seldate As Date = Date.Parse(dpBirth.Text)
            birthdate = Format(seldate, "yyyy-MM-dd") 'bagus

            strsql = "update patient set patientname='" & pasienName &
            "',address='" & address &
            "',malefemale='" & gender &
            "',phone='" & phone &
            "',idpatient='" & idpatient &
            "',pricelevel='" & pricelevel &
            "',email='" & email &
            "',birthdate='" & birthdate &
            "',nik='" & nik &
            "',jabatan='" & jabatan &
            "',organization='" & instansi &
            "' where id='" & selectedPasienId & "'"

            objconn.buildConn()
            objconn.ExecuteNonQuery(strsql)
            objconn.CloseConn()


        End If
        PopulateDgv("")
        disableText()
        SaveMnu.Enabled = False
        AddMnu.Enabled = True
        EditMnu.Enabled = True
        mnuRegistration.Enabled = True
        isEdit = False
        isNew = False

    End Sub

    Private Sub PopulateDgv(ByVal strsearch As String)

        Dim pormat3 As String
        pormat3 = "dd/MM/yyyy"
        objconn = New clsGreConnect
        objconn.buildConn()
        tblData = New DataTable
        dgPasien.DataSource = Nothing
        strsql = "select id,patientname,address,malefemale,phone,idpatient,email,birthdate from patient " & strsearch

        tblData = objconn.ExecuteQuery(strsql) ' order by testgroupname asc")
        dgPasien.DataSource = tblData

        Dim chk As New DataGridViewCheckBoxColumn()

        dgPasien.Columns.Add(chk)
        chk.HeaderText = "SELECT"
        chk.Width = 70
        chk.Name = "chk"
        dgPasien.Columns("chk").DisplayIndex = 0
        dgPasien.Columns("chk").Name = "chk"


        Dim j As Integer
        For j = 0 To dgPasien.Columns.Count - 1
            dgPasien.Columns(j).Visible = False
        Next

        dgPasien.Columns("chk").DisplayIndex = 0
        dgPasien.Columns("chk").Name = "chk"
        dgPasien.Columns("chk").Visible = True

        dgPasien.Columns("idpatient").HeaderText = "ID Pasien"
        dgPasien.Columns("idpatient").Width = 100
        dgPasien.Columns("idpatient").Visible = True
        dgPasien.Columns("idpatient").DisplayIndex = 1

        dgPasien.Columns("patientname").HeaderText = "Nama"
        dgPasien.Columns("patientname").Width = 250
        dgPasien.Columns("patientname").Visible = True
        dgPasien.Columns("patientname").DisplayIndex = 2

        dgPasien.Columns("address").HeaderText = "Alamat"
        dgPasien.Columns("address").Width = 250
        dgPasien.Columns("address").Visible = True
        dgPasien.Columns("address").DisplayIndex = 3

        dgPasien.Columns("phone").HeaderText = "Phone"
        dgPasien.Columns("phone").Width = 130
        dgPasien.Columns("phone").Visible = True
        dgPasien.Columns("phone").DisplayIndex = 4

        dgPasien.Columns("birthdate").HeaderText = "Tanggal lahir"
        dgPasien.Columns("birthdate").Width = 120
        dgPasien.Columns("birthdate").ToString.Format(pormat3)
        dgPasien.Columns("birthdate").Visible = True
        dgPasien.Columns("birthdate").DisplayIndex = 5

        dgPasien.Columns("id").Visible = False
        objconn.CloseConn()


        dgPasien.RowHeadersVisible = False
        dgPasien.AllowUserToAddRows = False

        EditMnu.Enabled = False
        SaveMnu.Enabled = False

    End Sub

    Private Sub AddMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddMnu.Click
        isNew = True
        isEdit = False
        SaveMnu.Enabled = True
        cbtipepasien.Enabled = True
        enableText()
        ClearText()
        initialPatientNumbering()

    End Sub

    Private Sub initialPatientNumbering()

        Dim strf As String
        strf = "select id,idpatient from patient order by id desc limit 1"
        Dim obf As New clsGreConnect
        obf.buildConn()

        Dim idpatient As String
        Dim cmdf As New SqlClient.SqlCommand(strf, obf.grecon)
        Dim rdrf As SqlDataReader = cmdf.ExecuteReader(CommandBehavior.CloseConnection)
        If rdrf.HasRows Then
            Do While rdrf.Read
                idpatient = Trim(rdrf("idpatient"))
                If patientnumbering = PASIEN_MANUAL_NUMBERING Then
                    txtIdPasien.Text = idpatient
                    txtIdPasien.Focus()

                    txtIdPasien.SelectionStart = txtIdPasien.SelectionStart + txtIdPasien.Text.Length - 1
                    txtIdPasien.SelectionLength = 1

                ElseIf patientnumbering = PASIEN_AUTO_NUMBERING Then
                    If System.Text.RegularExpressions.Regex.IsMatch(idpatient, "^[0-9 ]+$") Then
                        txtIdPasien.Text = CStr(CInt(idpatient) + 1)
                        txtIdPasien.Focus()
                        txtIdPasien.SelectionStart = txtIdPasien.SelectionStart + txtIdPasien.Text.Length - 1
                        txtIdPasien.SelectionLength = 1
                    Else
                        txtIdPasien.Text = idpatient
                        txtIdPasien.Focus()
                        txtIdPasien.SelectionStart = txtIdPasien.SelectionStart + txtIdPasien.Text.Length - 1
                        txtIdPasien.SelectionLength = 1
                    End If

                End If
                'txtIdPasien.SelectionStart = txtIdPasien.Text.Length - 1
                'txtIdPasien.SelectionLength = 1
            Loop
        Else
            txtIdPasien.Focus()
        End If
        cmdf.Dispose()
        rdrf.Close()
        obf = Nothing
    End Sub

    Private Sub ClearText()
        For Each ctrl As Control In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.Text = ""
            End If
        Next
        dpBirth.Enabled = True
        rdoLaki.Enabled = True
        rdoPerempuan.Enabled = True
    End Sub
    Private Sub EditMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditMnu.Click
        isEdit = True
        isNew = False
        AddMnu.Enabled = False
        EditMnu.Enabled = False
        SaveMnu.Enabled = True
        mnuRegistration.Enabled = False
        enableText()
        Dim i As Integer
        For i = 0 To dgPasien.Rows.Count - 1
            If dgPasien.Item("chk", i).Value = True Then
                If Not (dgPasien.CurrentRow.IsNewRow) Then
                    If Not (IsDBNull(dgPasien.Item("id", i).Value)) Then
                        If Not IsDBNull(dgPasien.Item("idpatient", i).Value) Then
                            txtIdPasien.Text = dgPasien.Item("idpatient", i).Value
                        Else
                            txtIdPasien.Text = ""
                        End If
                        If Not IsDBNull(dgPasien.Item("email", i).Value) Then
                            txtEmail.Text = dgPasien.Item("email", i).Value
                        Else
                            txtEmail.Text = ""
                        End If
                        If Not IsDBNull(dgPasien.Item("patientname", i).Value) Then
                            txtNamaPasien.Text = dgPasien.Item("patientname", i).Value
                        Else
                            txtNamaPasien.Text = ""
                        End If
                        If Not IsDBNull(dgPasien.Item("phone", i).Value) Then
                            txtPhone.Text = dgPasien.Item("phone", i).Value
                        Else
                            txtPhone.Text = ""
                        End If

                        txtAlamat.Text = dgPasien.Item("address", i).Value

                        If dgPasien.Item("malefemale", i).Value = genderMale Then
                            rdoLaki.Checked = True
                        ElseIf dgPasien.Item("malefemale", i).Value = genderFemale Then
                            rdoPerempuan.Checked = True
                        ElseIf dgPasien.Item("malefemale", i).Value = genderChild Then
                            rdoanak.Checked = True
                        End If

                        dpBirth.Text = CStr(Format(dgPasien.Item("birthdate", i).Value, "yyyy-MM-dd"))
                        
                    End If
                    End If
                End If
        Next


    End Sub

   
    Private Sub dgPasien_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgPasien.CellContentClick
        If Not (dgPasien.CurrentRow.IsNewRow) And Not (e.RowIndex = -1) Then
            If Not IsDBNull(dgPasien.Item("id", e.RowIndex).Value) Then
                If TypeOf dgPasien.Rows(e.RowIndex).Cells(e.ColumnIndex) Is DataGridViewCheckBoxCell Then
                    Dim dgvCheckBoxCell As DataGridViewCheckBoxCell = dgPasien.Rows(e.RowIndex).Cells(e.ColumnIndex)
                    'Commit the data to the datasouce.
                    dgPasien.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    Dim checked As Boolean = CType(dgvCheckBoxCell.Value, Boolean)
                    selectedPasienId = dgPasien.Item("id", e.RowIndex).Value
                    ' MessageBox.Show(e.RowIndex & " " & checked & " " & e.ColumnIndex)
                    CekLicense()
                    SHowDetail(selectedPasienId)
                    If checked Then

                        EditMnu.Enabled = True

                        Dim i As Integer
                        For i = 0 To dgPasien.Rows.Count - 1
                            If i <> e.RowIndex Then
                                dgPasien.Item("chk", i).Value = False
                            End If
                        Next

                    End If

                End If
            End If
        End If
    End Sub
    Private Sub SHowDetail(ByVal selpatientid As Integer)
        Dim strsql As String
        strsql = "select * from patient where id='" & selpatientid & "'"
        Dim greshow As New clsGreConnect
        greshow.buildConn()

        enableText()
        Dim cmd As New SqlClient.SqlCommand(strsql, greshow.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            txtNamaPasien.Text = rdr("patientname")
            txtAlamat.Text = rdr("address")
            txtPhone.Text = rdr("phone")
            txtIdPasien.Text = rdr("idpatient")
            txtEmail.Text = rdr("email")
            If rdr("malefemale") = genderMale Then
                rdoLaki.Checked = True
                rdoPerempuan.Checked = False
            ElseIf rdr("malefemale") = genderFemale Then
                rdoPerempuan.Checked = True
                rdoLaki.Checked = False
            ElseIf rdr("malefemale") = genderChild Then
                rdoanak.Checked = True
            End If

            txtnip.Text = IIf(IsDBNull(rdr("nik")), "", rdr("nik"))
            txtjabatan.Text = IIf(IsDBNull(rdr("jabatan")), "", rdr("jabatan"))
            cbinstansi.Text = IIf(IsDBNull(rdr("organization")), "", rdr("organization"))

            cbtipepasien.SelectedValue = rdr("pricelevel")

            selectedDateBirth = rdr("birthdate")
            dpBirth.Value = rdr("birthdate") ' penting date picker di isi date, bisa langsung
        Loop

        disableText()

        rdr.Close()
        cmd.Dispose()
        greshow.CloseConn()
        cmdshowage.PerformClick()
    End Sub
    Private Sub enableText()
        For Each ctrl As Control In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.Enabled = True
                txt.ReadOnly = False
            End If
        Next
        cbtipepasien.Enabled = True
        dpBirth.Enabled = True
        rdoLaki.Enabled = True
        rdoPerempuan.Enabled = True
    End Sub

    Private Sub DeleteMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteMnu.Click
        If selectedPasienId <> 0 Then
            Dim i As Integer
            For i = 0 To dgPasien.Rows.Count - 1
                If dgPasien.Item("chk", i).Value = True Then
                    strsql = "delete from patient " & _
                    "where id='" & selectedPasienId & "'"

                    objconn.buildConn()
                    objconn.ExecuteNonQuery(strsql)
                    objconn.CloseConn()
                End If
            Next

            ClearText()
            disableText()
            PopulateDgv("")
        Else
            MessageBox.Show("Silahkan pilih pasien yang hendak di hapus datanya ")
        End If

    End Sub

  

    Private Sub cancelMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelMnu.Click
        isNew = False
        isEdit = False
        selectedPasienId = 0
        EditMnu.Enabled = True
        AddMnu.Enabled = True
        mnuRegistration.Enabled = True
        PopulateDgv("")
        ClearText()
        disableText()
    End Sub

    Private Sub disableText()
        For Each ctrl As Control In Panel1.Controls
            If (ctrl.GetType() Is GetType(TextBox)) Then
                Dim txt As TextBox = CType(ctrl, TextBox)
                txt.ReadOnly = True
            End If
        Next

        txtFinder.Enabled = True
        txtFinder.ReadOnly = False
        dpBirth.Enabled = True
        cbtipepasien.Enabled = False
        rdoLaki.Enabled = False
        rdoPerempuan.Enabled = False
    End Sub

    Private Sub mnuRegistration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRegistration.Click
        If selectedPasienId <> 0 Then
            If isRegWinOpen = 0 Then

                If Trim(cbtipepasien.Text) = "" Then
                    MessageBox.Show("Pilih Tipe pasien")
                    Exit Sub
                End If

                Dim patientcode As String
                patientcode = Trim(txtIdPasien.Text)
                Dim objfrmRegistration As New frmRegistration(selectedPasienId, patientcode)
                objfrmRegistration.MdiParent = MainFrm

                'objfrmRegistration.StartPosition = FormStartPosition.Manual
                'objfrmRegistration.Left = 0
                'objfrmRegistration.Top = 0
                objfrmRegistration.Show()
                Me.Close()
            Else
                MessageBox.Show("Halaman registrasi sudah terbuka, mohon tutup dahulu")
            End If

        Else
            MessageBox.Show("Silahkan pilih pasien yang hendak di registrasi")
        End If
    End Sub

    Private Sub cbFinder_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbFinder.SelectedIndexChanged
        If cbFinder.SelectedIndex = 0 Or cbFinder.SelectedIndex = 2 Or cbFinder.SelectedIndex = 3 Then
            dpfind.Enabled = False
            txtFinder.Enabled = True
        ElseIf cbFinder.SelectedIndex = 1 Then
            dpfind.Enabled = True
            txtFinder.Enabled = False
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        cbFinder.SelectedIndex = 0
        PopulateDgv("")
        SaveMnu.Enabled = False
        disableText()
    End Sub

    Private Sub dpBirth_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles dpBirth.Validated
        'If isNew = False And isEdit = False And selectedPasienId = 0 Then
        '    dpBirth.Value = Now
        'ElseIf isNew = False And isEdit = False And selectedPasienId <> 0 Then
        '    dpBirth.Value = selectedDateBirth
        'End If

        'lbldisplayumur.Text = GreCountPatientAge()
    End Sub

    Private Sub dpBirth_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dpBirth.ValueChanged
        'If isNew = False And isEdit = False And selectedPasienId = 0 Then
        '    dpBirth.Value = Now
        'ElseIf isNew = False And isEdit = False And selectedPasienId <> 0 Then
        '    dpBirth.Value = selectedDateBirth
        'End If

        'lbldisplayumur.Text = GreCountPatientAge()

    End Sub
    Private Function GreCountPatientAge() As String
        Dim result As String

        result = Age(dpBirth.Value, Today)

        Return result

    End Function
    Private Sub txtIdPasien_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtIdPasien.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)

        End Select
    End Sub

    Private Sub txtAlamat_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAlamat.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)

        End Select
    End Sub

    Private Sub txtNamaPasien_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNamaPasien.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)

        End Select
    End Sub

    Private Sub txtEmail_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEmail.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedEmailKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedEmailKeys.Contains(e.KeyChar)

        End Select
    End Sub
    ReadOnly AllowedKeys As String = _
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789. "
    ReadOnly AllowedEmailKeys As String = _
   "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.@"
    ReadOnly AllowedPhoneKeys As String = _
   "0123456789"

    Private Sub txtFinder_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFinder.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedEmailKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedEmailKeys.Contains(e.KeyChar)

        End Select
    End Sub

    Private Sub txtPhone_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPhone.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedPhoneKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedPhoneKeys.Contains(e.KeyChar)

        End Select
    End Sub

    Private Sub Label8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label8.Click

    End Sub

    Private Sub txtPhone_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPhone.TextChanged

    End Sub

    Private Sub cmdshowage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdshowage.Click
        If isNew = False And isEdit = False And selectedPasienId = 0 Then
            dpBirth.Value = Now
        ElseIf isNew = False And isEdit = False And selectedPasienId <> 0 Then
            dpBirth.Value = selectedDateBirth
        End If

        lbldisplayumur.Text = GreCountPatientAge()
    End Sub

    Private Sub txtIdPasien_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtIdPasien.TextChanged

    End Sub

    Private Sub cbtipepasien_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbtipepasien.SelectedIndexChanged

    End Sub
End Class
