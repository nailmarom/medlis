﻿Imports System.Data
Imports System.Data.SqlClient

Public Class stuckJobsFrm
    Dim greobject As clsGreConnect
    Dim dtable As DataTable
    Dim selectedLabNumber As String
    Dim selectedPatient As String
    Dim selsampleno As String
    Dim patientName As String

    Dim patientPhone As String
    Dim patientaddress As String

    Dim stateChoice As Integer '0 new, 1 sampling, 2 finish, 8 LAGI DI BACA, 3 semua


    Private Sub jobDetailFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim strsql As String
        'strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.id as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail where status='0' and active='1')"
        'Joblist(strsql)
        'ContructDgv()
        Icon = New System.Drawing.Icon("medlis2.ico")
        CekLicense()
        HideButton()
        ShowStuckData()
    End Sub
    Private Sub ShowStuckData()
        HideButton()
        stateChoice = BEINGANALYZE
        Dim strsql As String
        strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail where status='" & BEINGANALYZE & "' and active='" & ACTIVESAMPLE & "')"
        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If

        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If
        clearDGVdetail()
        Joblist(strsql)
        lblStatus.Text = "Job berada di instrument"
    End Sub


    Private Sub ShowButton()
        btnLabReport.Visible = True
    End Sub

    Private Sub HideRereadButton()
        btnreread.Visible = False
    End Sub

    Private Sub ShowRereadButton()
        btnreread.Visible = True
    End Sub
    Private Sub HideButton()
        btnLabReport.Visible = False

    End Sub

    Private Sub ContructDgv()
        dgvdetail.ColumnCount = 4
        dgvdetail.Columns(0).Name = "SampleNo"
        dgvdetail.Columns(0).HeaderText = "Sample Number"
        dgvdetail.Columns(0).Width = 80

        dgvdetail.Columns(1).Name = "Barcode"
        dgvdetail.Columns(1).HeaderText = "Barcode"
        dgvdetail.Columns(1).Width = 80

        dgvdetail.Columns(2).Name = "Status"
        dgvdetail.Columns(2).HeaderText = "Status"
        dgvdetail.Columns(2).Width = 80

        dgvdetail.Columns(3).Name = "TestName"
        dgvdetail.Columns(3).HeaderText = "Nama Test"
        dgvdetail.Columns(3).Width = 120

        'Dim checkColumn As New DataGridViewCheckBoxColumn
        'With checkColumn
        '    .Name = "Pilih"
        '    .HeaderText = "Pilih"
        '    .Width = 90
        '    .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        '    .FlatStyle = FlatStyle.Standard
        '    .DisplayIndex = 3
        'End With
        'dgvdetail.Columns.Add(checkColumn)
        dgvdetail.AllowUserToAddRows = False
        dgvdetail.RowHeadersVisible = False
    End Sub


    Private Sub Joblist(ByVal strsql As String)
        'Dim strsql As String
        'strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.id as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.status='0'"
        greobject = New clsGreConnect
        greobject.buildConn()

        dtable = New DataTable

        dtable = greobject.ExecuteQuery(strsql)

        'dgvjoblist = New DataGridView

        dgvJoblist.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvJoblist.Columns.Count - 1
            dgvJoblist.Columns(j).Visible = False
        Next

        dgvJoblist.Columns("labnumber").Visible = True
        dgvJoblist.Columns("labnumber").HeaderText = "Lab Number"
        dgvJoblist.Columns("labnumber").Width = 90

        dgvJoblist.Columns("name").Visible = True
        dgvJoblist.Columns("name").HeaderText = "Nama pasien"
        dgvJoblist.Columns("name").Width = 180

        dgvJoblist.Columns("address").Visible = True
        dgvJoblist.Columns("address").HeaderText = "Alamat"
        dgvJoblist.Columns("address").Width = 220

        Dim buttonColumn As New DataGridViewButtonColumn()
        With buttonColumn
            .Name = "Detail"
            .HeaderText = "Detail"
            .Width = 90
            .Text = "Detail"
            .UseColumnTextForButtonValue = True
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            .FlatStyle = FlatStyle.Standard
            .CellTemplate.Style.BackColor = Color.Yellow
            .DisplayIndex = 0
        End With
        dgvJoblist.Columns.Add(buttonColumn)

        dgvJoblist.AllowUserToAddRows = False
        dgvJoblist.RowHeadersVisible = False
    End Sub

    Private Sub dgvJoblist_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvJoblist.CellContentClick
        Dim idx As Integer
        idx = dgvJoblist.Columns("Detail").Index
        txtlabnum.Text = ""
        If e.RowIndex < 0 OrElse Not e.ColumnIndex = dgvJoblist.Columns("Detail").Index Then

            Return

        Else

            If Not dgvJoblist.CurrentRow.IsNewRow Then

                selectedLabNumber = dgvJoblist.Item("labnumber", dgvJoblist.CurrentRow.Index).Value
                txtlabnum.Text = selectedLabNumber
                selectedPatient = dgvJoblist.Item("patientid", dgvJoblist.CurrentRow.Index).Value
                patientName = dgvJoblist.Item("name", dgvJoblist.CurrentRow.Index).Value
                patientaddress = dgvJoblist.Item("address", dgvJoblist.CurrentRow.Index).Value
                patientPhone = dgvJoblist.Item("phone", dgvJoblist.CurrentRow.Index).Value
                selsampleno = ""
                PopulatedgvDetail()
                ShowDetail()
            End If

        End If
    End Sub
    Private Sub ShowDetail()
        txtname.ReadOnly = False
        txtaddress.ReadOnly = False
        txttelepon.ReadOnly = False
        txtlabnum.ReadOnly = False

        txtname.Text = patientName
        txtaddress.Text = patientaddress
        txttelepon.Text = patientPhone

        txtlabnum.ReadOnly = True
        txtname.ReadOnly = True
        txtaddress.ReadOnly = True
        txttelepon.ReadOnly = True

        txtbarcode.ReadOnly = False

        txtsn.ReadOnly = False
        Dim strsql As String
        strsql = "select distinct sampleno,barcode from jobdetail where labnumber='" & selectedLabNumber & "'"
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim cmd As New SqlClient.SqlCommand(strsql, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("sampleno")) Then
                    txtsn.Text = rdr("sampleno")
                End If
                If Not IsDBNull(rdr("barcode")) Then
                    txtbarcode.Text = rdr("barcode")
                End If

            Loop
        End If
        txtsn.ReadOnly = True
        txtbarcode.ReadOnly = True
        rdr.Close()
        cmd.Dispose()
        ogre.CloseConn()
    End Sub


    Private Sub PopulatedgvDetail()
        Dim newcon As New clsGreConnect
        Dim strsql As String

        'If dgvdetail.RowCount > 0 Then
        '    dgvdetail.Rows.Clear()
        'End If

        'dgvdetail.Rows.Clear()
        If stateChoice = BEINGANALYZE Then ' baru
            'strsql = "select distinct sampleno,barcode,status from jobdetail where labnumber='" & selectedLabNumber & "' and active='1' and status='" & BEINGANALYZE & "'"
            strsql = "select * from jobdetail where labnumber='" & selectedLabNumber & "' and active='1' and status='" & BEINGANALYZE & "'"
        End If
        newcon.buildConn()

        Dim strStatus As String
        Dim statusample As String


        dgvdetail.AllowUserToAddRows = False
        dgvdetail.RowHeadersVisible = False

        newcon.buildConn()
        dtable = New DataTable
        ' strsql = "select id,universaltest from jobdetail where labnumber='" & selectedLabNumber & "' and active='" & ACTIVESAMPLE & "' and status='0'"
        dtable = newcon.ExecuteQuery(strsql)

        'dgvtestname = New DataGridView
        dgvdetail.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvdetail.Columns.Count - 1
            dgvdetail.Columns(j).Visible = False
        Next
        'disabled on 09-01-2015 as not needed to add check box
        'If Not dgvdetail.Columns.Contains("chk") Then
        '    Dim chk As New DataGridViewCheckBoxColumn()

        '    dgvdetail.Columns.Add(chk)
        '    chk.HeaderText = "SELECT"
        '    chk.Width = 120
        '    chk.Name = "chk"

        '    dgvdetail.Columns("chk").DisplayIndex = 0
        '    dgvdetail.Columns("chk").Name = "chk"
        'End If

        'dgvdetail.Columns("chk").Visible = True
        dgvdetail.Columns("universaltest").Visible = True
        dgvdetail.Columns("universaltest").HeaderText = "Test Name"
        dgvdetail.Columns("universaltest").Width = 200
        newcon.CloseConn()

    End Sub
    Private Function GetUniversalTestName(ByVal sampleno As String, ByVal labnumber As String, ByVal sts As String) As String
        Dim strfinder As String
        Dim strresult As String
        Dim testcon As New clsGreConnect
        testcon.buildConn()
        strfinder = "select universaltest from jobdetail where sampleno='" & sampleno & "' and labnumber='" & labnumber & "' and status='" & sts & "'"
        Dim thecommand As New SqlClient.SqlCommand(strfinder, testcon.grecon)
        Dim theReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)
        strresult = ""
        Do While theReader.Read
            strresult = strresult & " " & theReader("universaltest")
        Loop
        thecommand = Nothing
        theReader.Close()
        testcon.CloseConn()
        testcon = Nothing

        Return strresult
    End Function

    Private Sub dgvdetail_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvdetail.CellContentClick


        If Not (dgvdetail.CurrentRow.IsNewRow) Then
            If Not IsDBNull(dgvdetail.Item("sampleno", e.RowIndex).Value) Then
                If TypeOf dgvdetail.Rows(e.RowIndex).Cells(e.ColumnIndex) Is DataGridViewCheckBoxCell Then
                    Dim dgvCheckBoxCell As DataGridViewCheckBoxCell = dgvdetail.Rows(e.RowIndex).Cells(e.ColumnIndex)
                    'Commit the data to the datasouce.
                    dgvdetail.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    Dim checked As Boolean = CType(dgvCheckBoxCell.Value, Boolean)
                    If checked Then

                        selsampleno = dgvdetail.Item("sampleno", e.RowIndex).Value
                        NormalizedSample(selsampleno)

                    End If


                End If
            End If

        End If
    End Sub

    Private Sub NormalizedSample(ByVal sampleno As String)

        Dim i As Integer
        Dim strsql As String
        Dim objcon As New clsGreConnect
        objcon.buildConn()

        strsql = "select distinct sampleno,barcode from jobdetail where labnumber='" & selectedLabNumber & "' and sampleno='" & sampleno & "'"
        Dim clearCommand As New SqlClient.SqlCommand(strsql, objcon.grecon)
        Dim clearReader As SqlDataReader = clearCommand.ExecuteReader(CommandBehavior.CloseConnection)
        Do While clearReader.Read

            For i = 0 To dgvdetail.Rows.Count - 1
                If Not IsDBNull(clearReader("sampleno")) Then
                    If dgvdetail.Item("sampleno", i).Value <> clearReader("sampleno") Then
                        dgvdetail.Rows(dgvdetail.Item("sampleno", i).RowIndex).ReadOnly = False
                        dgvdetail.Rows(dgvdetail.Item("sampleno", i).RowIndex).DefaultCellStyle.BackColor = Color.White
                        dgvdetail.Rows(dgvdetail.Item("sampleno", i).RowIndex).Cells(3).Value = False
                        'sampai disini
                    End If
                End If
            Next
        Loop
        'bagus
        clearReader.Close()
        clearCommand.Dispose()
        objcon.CloseConn()

    End Sub




    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        stateChoice = NEWSAMPLE
        Dim strsql As String
        ' strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.id as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.status='0'"
        strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail where status='" & NEWSAMPLE & "' and active='1')"

        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If
        clearDGVdetail()
        Joblist(strsql)
        lblStatus.Text = "Baru"
    End Sub
    Private Sub clearDGVdetail()
        dgvdetail.DataSource = Nothing
        txtaddress.ReadOnly = False
        txtlabnum.ReadOnly = False
        txtname.ReadOnly = False
        txttelepon.ReadOnly = False
        txtsn.ReadOnly = False
        txtbarcode.ReadOnly = False
        txtaddress.Text = ""
        txtlabnum.Text = ""
        txtname.Text = ""
        txttelepon.Text = ""
        txtsn.Text = ""
        txtbarcode.Text = ""
        txtaddress.ReadOnly = True
        txtlabnum.ReadOnly = True
        txtname.ReadOnly = True
        txttelepon.ReadOnly = True
        txtsn.ReadOnly = True
        txtbarcode.ReadOnly = True
    End Sub




    Private Sub btnSampling_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        stateChoice = ALREADYSAMPLING
        Dim strsql As String
        strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail where status='" & ALREADYSAMPLING & "' and active='" & ACTIVESAMPLE & "')"

        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If
        clearDGVdetail()
        Joblist(strsql)
        lblStatus.Text = "Sudah selesai pengambilan sample"
    End Sub

    Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        stateChoice = FINISHSAMPLE

        Dim strsql As String
        strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail where status='" & FINISHSAMPLE & "' and active='" & ACTIVESAMPLE & "')"
        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If

        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If
        clearDGVdetail()
        Joblist(strsql)
        lblStatus.Text = "Selesai/Sudah ada hasil"

    End Sub

    Private Sub btnAllJobs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        stateChoice = 3
        Dim strsql As String
        ' strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.id as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.status='0'"


        strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail where active='" & ACTIVESAMPLE & "')"

        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If
        clearDGVdetail()
        Joblist(strsql)
        lblStatus.Text = "Semua Test"
    End Sub

    Private Sub btnPrintReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rptresult As New RptResultFrm
        rptresult.MdiParent = MainFrm
        rptresult.Show()
    End Sub

    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim strcn As String
        Dim ln As String

        'strcn = "Driver={PostgreSQL ANSI};database=MEDLIS;server=127.0.0.1;port=5432;uid=dian;sslmode=disable;readonly=0;protocol=7.4;User ID=dian;password=dianjuga;"
        strcn = strdbconn
        Dim rptreportfrm As New resultwithparamandconparamFrm(selectedLabNumber, stateChoice, strcn)
        rptreportfrm.MdiParent = MainFrm
        rptreportfrm.Show()
    End Sub

    Private Sub btnLabReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLabReport.Click
        Dim rptlabreport As New rptLabReportFrm(selectedLabNumber, selectedPatient)
        rptlabreport.MdiParent = MainFrm
        rptlabreport.Show()

    End Sub

    Private Sub AllTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        stateChoice = 3
        HideRereadButton()
        Dim strsql As String
        ' strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.id as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.status='0'"
        HideButton()

        strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail where active='" & ACTIVESAMPLE & "')"

        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If
        clearDGVdetail()
        Joblist(strsql)
        lblStatus.Text = "Semua Test"
    End Sub

    Private Sub NewTestMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        HideButton()
        HideRereadButton()
        stateChoice = NEWSAMPLE
        Dim strsql As String
        ' strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.id as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.status='0'"
        strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail where status='" & NEWSAMPLE & "' and active='" & ACTIVESAMPLE & "')"

        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If
        clearDGVdetail()
        Joblist(strsql)
        lblStatus.Text = "Baru"
    End Sub

    Private Sub SamplingMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        HideButton()
        HideRereadButton()
        stateChoice = ALREADYSAMPLING
        Dim strsql As String
        strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail where status='" & ALREADYSAMPLING & "' and active='" & ACTIVESAMPLE & "')"

        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If
        clearDGVdetail()
        Joblist(strsql)
        lblStatus.Text = "Sudah selesai pengambilan sample"
    End Sub

    Private Sub readMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        HideButton()
        ShowRereadButton()
        stateChoice = BEINGANALYZE
        Dim strsql As String
        strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail where status='" & BEINGANALYZE & "' and active='" & ACTIVESAMPLE & "')"
        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If

        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If
        clearDGVdetail()
        Joblist(strsql)
        lblStatus.Text = "Sedang dikerjakan oleh mesin"
    End Sub

    Private Sub FinishMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        stateChoice = FINISHSAMPLE
        HideRereadButton()
        ShowButton()
        Dim strsql As String
        ' strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail where status='" & FINISHSAMPLE & "' and active='" & ACTIVESAMPLE & "')"
        strsql = "select distinct job.labnumber,job.idpasien,patient.phone,patient.patientname as name,patient.address,patient.idpatient as patientid,jobdetail.labnumber from job,patient,jobdetail where job.idpasien=patient.id and job.labnumber=jobdetail.labnumber and job.[print]<>'1' and job.labnumber not in (select distinct labnumber from jobdetail where status<>'2' and active='1')"
        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If

        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If
        clearDGVdetail()
        Joblist(strsql)
        lblStatus.Text = "Selesai/Sudah ada hasil"
    End Sub

    Private Sub btnFinish_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnNew_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnreread_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnreread.Click
        Try
            If stateChoice = BEINGANALYZE Then
                If selectedLabNumber <> "" Then
                    Dim strsql As String
                    strsql = "update jobdetail set status='" & ALREADYSAMPLING & "' where labnumber='" & selectedLabNumber & "' and status='" & BEINGANALYZE & "'"
                    Dim oupdate As New clsGreConnect
                    oupdate.buildConn()

                    oupdate.ExecuteNonQuery(strsql)
                    oupdate.CloseConn()
                    MessageBox.Show("Data tersebut siap dibaca kembali dari instrment")
                End If
            End If
        Catch ex As Exception

        End Try
        
    End Sub
End Class