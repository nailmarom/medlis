﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class samplingFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(samplingFrm))
        Me.txtlabsearch = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.dgvJoblist = New System.Windows.Forms.DataGridView
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.btnPrint = New System.Windows.Forms.Button
        Me.txtname = New System.Windows.Forms.TextBox
        Me.txtaddress = New System.Windows.Forms.TextBox
        Me.txttelepon = New System.Windows.Forms.TextBox
        Me.dgvdetail = New System.Windows.Forms.DataGridView
        Me.txtSampleNo = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtBarcode = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtLabnumber = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.cmdFinish = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.Label10 = New System.Windows.Forms.Label
        Me.lblMode = New System.Windows.Forms.Label
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.lblnosample = New System.Windows.Forms.Label
        Me.RdoBarcodeMode = New System.Windows.Forms.RadioButton
        Me.rdoSampleMode = New System.Windows.Forms.RadioButton
        Me.chkPilihSemua = New System.Windows.Forms.CheckBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.lblprevsamp = New System.Windows.Forms.Label
        Me.lblprint = New System.Windows.Forms.Label
        Me.pictBarcode = New System.Windows.Forms.PictureBox
        CType(Me.dgvJoblist, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvdetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.pictBarcode, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtlabsearch
        '
        Me.txtlabsearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtlabsearch.Location = New System.Drawing.Point(114, 24)
        Me.txtlabsearch.Name = "txtlabsearch"
        Me.txtlabsearch.Size = New System.Drawing.Size(237, 21)
        Me.txtlabsearch.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(22, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 15)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Lab Number"
        '
        'dgvJoblist
        '
        Me.dgvJoblist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvJoblist.Location = New System.Drawing.Point(12, 61)
        Me.dgvJoblist.Name = "dgvJoblist"
        Me.dgvJoblist.Size = New System.Drawing.Size(704, 343)
        Me.dgvJoblist.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(22, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Nama"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(22, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(51, 15)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Alamat"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(22, 103)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 15)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Telepon"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdCancel
        '
        Me.cmdCancel.Location = New System.Drawing.Point(627, 17)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(87, 37)
        Me.cmdCancel.TabIndex = 10
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(431, 17)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(97, 37)
        Me.btnPrint.TabIndex = 11
        Me.btnPrint.Text = "Print Label"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'txtname
        '
        Me.txtname.Location = New System.Drawing.Point(124, 44)
        Me.txtname.Name = "txtname"
        Me.txtname.Size = New System.Drawing.Size(243, 21)
        Me.txtname.TabIndex = 12
        '
        'txtaddress
        '
        Me.txtaddress.Location = New System.Drawing.Point(124, 71)
        Me.txtaddress.Name = "txtaddress"
        Me.txtaddress.Size = New System.Drawing.Size(243, 21)
        Me.txtaddress.TabIndex = 13
        '
        'txttelepon
        '
        Me.txttelepon.Location = New System.Drawing.Point(124, 98)
        Me.txttelepon.Name = "txttelepon"
        Me.txttelepon.Size = New System.Drawing.Size(243, 21)
        Me.txttelepon.TabIndex = 14
        '
        'dgvdetail
        '
        Me.dgvdetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvdetail.Location = New System.Drawing.Point(734, 148)
        Me.dgvdetail.Name = "dgvdetail"
        Me.dgvdetail.Size = New System.Drawing.Size(432, 286)
        Me.dgvdetail.TabIndex = 15
        '
        'txtSampleNo
        '
        Me.txtSampleNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSampleNo.Location = New System.Drawing.Point(96, 15)
        Me.txtSampleNo.MaxLength = 4
        Me.txtSampleNo.Name = "txtSampleNo"
        Me.txtSampleNo.Size = New System.Drawing.Size(82, 21)
        Me.txtSampleNo.TabIndex = 16
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(17, 19)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(78, 15)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Sample No"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(17, 47)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(60, 15)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Barcode"
        '
        'txtBarcode
        '
        Me.txtBarcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBarcode.Location = New System.Drawing.Point(96, 46)
        Me.txtBarcode.MaxLength = 9
        Me.txtBarcode.Name = "txtBarcode"
        Me.txtBarcode.Size = New System.Drawing.Size(228, 21)
        Me.txtBarcode.TabIndex = 19
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtLabnumber)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtaddress)
        Me.GroupBox1.Controls.Add(Me.txtname)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txttelepon)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(731, 14)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(435, 130)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        '
        'txtLabnumber
        '
        Me.txtLabnumber.Location = New System.Drawing.Point(124, 18)
        Me.txtLabnumber.Name = "txtLabnumber"
        Me.txtLabnumber.Size = New System.Drawing.Size(243, 21)
        Me.txtLabnumber.TabIndex = 16
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(22, 21)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(86, 15)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Lab Number"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdFinish
        '
        Me.cmdFinish.Location = New System.Drawing.Point(534, 17)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(87, 37)
        Me.cmdFinish.TabIndex = 59
        Me.cmdFinish.Text = "Finish"
        Me.cmdFinish.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtBarcode)
        Me.GroupBox2.Controls.Add(Me.txtSampleNo)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(734, 495)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(330, 89)
        Me.GroupBox2.TabIndex = 60
        Me.GroupBox2.TabStop = False
        '
        'PrintDocument1
        '
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(22, 419)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(55, 15)
        Me.Label10.TabIndex = 67
        Me.Label10.Text = "Mode : "
        '
        'lblMode
        '
        Me.lblMode.AutoSize = True
        Me.lblMode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMode.Location = New System.Drawing.Point(79, 421)
        Me.lblMode.Name = "lblMode"
        Me.lblMode.Size = New System.Drawing.Size(134, 13)
        Me.lblMode.TabIndex = 68
        Me.lblMode.Text = "Barcode or Sample No"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.lblnosample)
        Me.GroupBox4.Location = New System.Drawing.Point(25, 541)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(188, 91)
        Me.GroupBox4.TabIndex = 69
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Sample No"
        '
        'lblnosample
        '
        Me.lblnosample.AutoSize = True
        Me.lblnosample.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblnosample.Location = New System.Drawing.Point(12, 28)
        Me.lblnosample.Name = "lblnosample"
        Me.lblnosample.Size = New System.Drawing.Size(19, 15)
        Me.lblnosample.TabIndex = 0
        Me.lblnosample.Text = "...."
        '
        'RdoBarcodeMode
        '
        Me.RdoBarcodeMode.AutoSize = True
        Me.RdoBarcodeMode.Location = New System.Drawing.Point(25, 442)
        Me.RdoBarcodeMode.Name = "RdoBarcodeMode"
        Me.RdoBarcodeMode.Size = New System.Drawing.Size(118, 19)
        Me.RdoBarcodeMode.TabIndex = 3
        Me.RdoBarcodeMode.TabStop = True
        Me.RdoBarcodeMode.Text = "Barcode Mode"
        Me.RdoBarcodeMode.UseVisualStyleBackColor = True
        '
        'rdoSampleMode
        '
        Me.rdoSampleMode.AutoSize = True
        Me.rdoSampleMode.Location = New System.Drawing.Point(25, 473)
        Me.rdoSampleMode.Name = "rdoSampleMode"
        Me.rdoSampleMode.Size = New System.Drawing.Size(129, 19)
        Me.rdoSampleMode.TabIndex = 2
        Me.rdoSampleMode.TabStop = True
        Me.rdoSampleMode.Text = "Sample Number"
        Me.rdoSampleMode.UseVisualStyleBackColor = True
        '
        'chkPilihSemua
        '
        Me.chkPilihSemua.AutoSize = True
        Me.chkPilihSemua.Location = New System.Drawing.Point(735, 442)
        Me.chkPilihSemua.Name = "chkPilihSemua"
        Me.chkPilihSemua.Size = New System.Drawing.Size(104, 19)
        Me.chkPilihSemua.TabIndex = 70
        Me.chkPilihSemua.Text = "Pilih Semua"
        Me.chkPilihSemua.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(735, 477)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(169, 15)
        Me.Label11.TabIndex = 71
        Me.Label11.Text = "No Sample Sebelumnya: "
        '
        'lblprevsamp
        '
        Me.lblprevsamp.AutoSize = True
        Me.lblprevsamp.Location = New System.Drawing.Point(901, 477)
        Me.lblprevsamp.Name = "lblprevsamp"
        Me.lblprevsamp.Size = New System.Drawing.Size(15, 15)
        Me.lblprevsamp.TabIndex = 72
        Me.lblprevsamp.Text = ".."
        '
        'lblprint
        '
        Me.lblprint.AutoSize = True
        Me.lblprint.Location = New System.Drawing.Point(26, 514)
        Me.lblprint.Name = "lblprint"
        Me.lblprint.Size = New System.Drawing.Size(131, 15)
        Me.lblprint.TabIndex = 73
        Me.lblprint.Text = "Jumlah print label :"
        '
        'pictBarcode
        '
        Me.pictBarcode.Location = New System.Drawing.Point(219, 421)
        Me.pictBarcode.Name = "pictBarcode"
        Me.pictBarcode.Size = New System.Drawing.Size(497, 211)
        Me.pictBarcode.TabIndex = 61
        Me.pictBarcode.TabStop = False
        '
        'samplingFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1171, 644)
        Me.Controls.Add(Me.lblprint)
        Me.Controls.Add(Me.pictBarcode)
        Me.Controls.Add(Me.lblprevsamp)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.chkPilihSemua)
        Me.Controls.Add(Me.RdoBarcodeMode)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.rdoSampleMode)
        Me.Controls.Add(Me.lblMode)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.cmdFinish)
        Me.Controls.Add(Me.dgvdetail)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.dgvJoblist)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtlabsearch)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "samplingFrm"
        Me.Text = "Pengambilan Sample"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvJoblist, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvdetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.pictBarcode, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtlabsearch As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvJoblist As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents txtname As System.Windows.Forms.TextBox
    Friend WithEvents txtaddress As System.Windows.Forms.TextBox
    Friend WithEvents txttelepon As System.Windows.Forms.TextBox
    Friend WithEvents dgvdetail As System.Windows.Forms.DataGridView
    Friend WithEvents txtSampleNo As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtBarcode As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdFinish As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtLabnumber As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents pictBarcode As System.Windows.Forms.PictureBox
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblMode As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents lblnosample As System.Windows.Forms.Label
    Friend WithEvents RdoBarcodeMode As System.Windows.Forms.RadioButton
    Friend WithEvents rdoSampleMode As System.Windows.Forms.RadioButton
    Friend WithEvents chkPilihSemua As System.Windows.Forms.CheckBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lblprevsamp As System.Windows.Forms.Label
    Friend WithEvents lblprint As System.Windows.Forms.Label
End Class
