﻿Public Class LabNumberLookupFrm

    Private Sub LabNumberLookupFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
        Dim strsql As String
        strsql = "select distinct labnumber from jobdetail order by labnumber desc limit 5"
        Dim ogre As New clsGreConnect
        ogre.buildConn()
        Dim dtable As New DataTable
        dtable = ogre.ExecuteQuery(strsql)
        dgvlookup.DataSource = dtable
        dgvlookup.Columns("labnumber").HeaderText = "Lab Number"
        ogre.CloseConn()

        dgvlookup.AllowUserToAddRows = False
        dgvlookup.RowHeadersVisible = False
        CekLicense()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class