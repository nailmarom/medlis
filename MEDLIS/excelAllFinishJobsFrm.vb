﻿Imports System.Data
Imports Microsoft.Win32
Imports System.Data.SqlClient
Imports GemBox.Spreadsheet
Imports GemBox.Spreadsheet.ConditionalFormatting
Imports DevExpress.XtraReports.UI


Public Class excellAllFinishJobsFrm
    Dim greobject As clsGreConnect
    Dim dtable As DataTable
    Dim selectedLabNumber As String
    Dim selectedPatient As String
    Dim selsampleno As String
    Dim patientName As String
    Dim digitfromdb As Integer
    Dim patientPhone As String
    Dim patientaddress As String
    Dim selectedDateBirth As Date
    Dim gender As Integer
    Dim index_position_result As Integer 'index position of the grid result

    Dim stateChoice As Integer '0 new, 1 sampling, 2 finish, 8 LAGI DI BACA, 3 semua


    Private Sub jobDetailFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")

        CekLicense()
        SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY")

        tsprintnot.Items.Add("NOT PRINT")
        tsprintnot.Items.Add("PRINT")
        tsprintnot.SelectedIndex = 0
        ClearLN()

        ' ShowDataDetail()
    End Sub
    Private Sub ClearLN()
        selectedLabNumber = ""
    End Sub

    Private Sub ShowButton()
        mnuCetak.Enabled = True
    End Sub
    Private Sub ShowData()
        Dim strsql As String
        strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,patient.malefemale as gender,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail where status='0' and active='1')"
        Joblist(strsql)
        ' ContructDgv()
        HideButton()
    End Sub

    Private Sub HideButton()
        mnuCetak.Enabled = False
    End Sub

    Private Sub ContructDgv()
        dgvdetail.ColumnCount = 4
        dgvdetail.Columns(0).Name = "SampleNo"
        dgvdetail.Columns(0).HeaderText = "Sample Number"
        dgvdetail.Columns(0).Width = 80
        ' dgvdetail.Columns(0).HeaderText.

        dgvdetail.Columns(1).Name = "Barcode"
        dgvdetail.Columns(1).HeaderText = "Barcode"
        dgvdetail.Columns(1).Width = 80

        dgvdetail.Columns(2).Name = "Status"
        dgvdetail.Columns(2).HeaderText = "Status"
        dgvdetail.Columns(2).Width = 80

        dgvdetail.Columns(3).Name = "TestName"
        dgvdetail.Columns(3).HeaderText = "Nama Test"
        dgvdetail.Columns(3).Width = 120


        dgvdetail.AllowUserToAddRows = False
        dgvdetail.RowHeadersVisible = False
    End Sub


    Private Sub Joblist(ByVal strsql As String)
        'Dim strsql As String
        'strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.id as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.status='0'"
        greobject = New clsGreConnect
        greobject.buildConn()

        dtable = New DataTable

        dtable = greobject.ExecuteQuery(strsql)

        'dgvjoblist = New DataGridView

        dgvJoblist.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvJoblist.Columns.Count - 1
            dgvJoblist.Columns(j).Visible = False
        Next

        dgvJoblist.Columns("labnumber").Visible = True
        dgvJoblist.Columns("labnumber").HeaderText = "Lab Number"
        dgvJoblist.Columns("labnumber").Width = 80

        dgvJoblist.Columns("name").Visible = True
        dgvJoblist.Columns("name").HeaderText = "Nama pasien"
        dgvJoblist.Columns("name").Width = 160

        dgvJoblist.Columns("datefinish").Visible = True
        dgvJoblist.Columns("datefinish").HeaderText = "Tanggal Selesai"
        dgvJoblist.Columns("datefinish").Width = 140

        dgvJoblist.Columns("address").Visible = True
        dgvJoblist.Columns("address").HeaderText = "Alamat"
        dgvJoblist.Columns("address").Width = 230



        Dim buttonColumn As New DataGridViewButtonColumn()
        With buttonColumn
            .Name = "Detail"
            .HeaderText = "Detail"
            .Width = 90
            .Text = "Detail"
            .UseColumnTextForButtonValue = True
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            .FlatStyle = FlatStyle.Standard
            .CellTemplate.Style.BackColor = Color.Yellow
            .DisplayIndex = 0
        End With
        dgvJoblist.Columns.Add(buttonColumn)

        dgvJoblist.AllowUserToAddRows = False
        dgvJoblist.RowHeadersVisible = False
    End Sub

    Private Sub dgvJoblist_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvJoblist.CellContentClick
        Dim idx As Integer
        idx = dgvJoblist.Columns("Detail").Index
        txtlabnum.Text = ""
        'top
        If e.RowIndex < 0 OrElse Not e.ColumnIndex = dgvJoblist.Columns("Detail").Index Then

            Return

        Else

            If Not dgvJoblist.CurrentRow.IsNewRow Then

                selectedLabNumber = dgvJoblist.Item("labnumber", dgvJoblist.CurrentRow.Index).Value
                '=======
                gender = dgvJoblist.Item("gender", dgvJoblist.CurrentRow.Index).Value

                FindAndUpdateSpecialTest(selectedLabNumber, gender)

                '=======
                txtlabnum.Text = selectedLabNumber
                selectedPatient = dgvJoblist.Item("patientid", dgvJoblist.CurrentRow.Index).Value
                patientName = dgvJoblist.Item("name", dgvJoblist.CurrentRow.Index).Value
                patientaddress = dgvJoblist.Item("address", dgvJoblist.CurrentRow.Index).Value
                patientPhone = dgvJoblist.Item("phone", dgvJoblist.CurrentRow.Index).Value
                selsampleno = ""
                PopulatedgvDetail()
                'Button1.PerformClick()
                DigitChange()
                ShowDetail()
            End If

        End If
    End Sub
    Private Sub ShowDetail()
        txtname.ReadOnly = False
        txtaddress.ReadOnly = False
        txttelepon.ReadOnly = False
        txtlabnum.ReadOnly = False


        txtname.Text = patientName
        txtaddress.Text = patientaddress
        txttelepon.Text = patientPhone

        txtlabnum.ReadOnly = True
        txtname.ReadOnly = True
        txtaddress.ReadOnly = True
        txttelepon.ReadOnly = True
        txtsn.ReadOnly = False
        txtbarcode.ReadOnly = False

        Dim strsql As String
        strsql = "select distinct sampleno,barcode from jobdetail where labnumber='" & selectedLabNumber & "'"
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim cmd As New SqlClient.SqlCommand(strsql, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("sampleno")) Then
                    txtsn.Text = rdr("sampleno")
                End If
                If Not IsDBNull(rdr("barcode")) Then
                    txtbarcode.Text = rdr("barcode")
                End If
            Loop
        End If
        txtsn.ReadOnly = True
        txtbarcode.ReadOnly = True

        rdr.Close()
        cmd.Dispose()
        ogre.CloseConn()
    End Sub


    Private Sub PopulatedgvDetail()
        '
        Dim objGreConnect As New clsGreConnect
        objGreConnect.buildConn()

        Dim dt As New DataTable
        dgvdetail.DataSource = Nothing


        Dim strsql As String
        'strsql = "select jobdetail.labnumber,jobdetail.sampleno,jobdetail.universaltest,jobdetail.measurementvalue from jobdetail where jobdetail.status='2' and jobdetail.active='1' and labnumber not in (select distinct labnumber from jobdetail where status<>'2' and active='1')"
        'strsql = "select distinct job.labnumber,job.idpasien,patient.patientname,jobdetail.labnumber from job,patient,jobdetail where job.idpasien=patient.id and job.labnumber=jobdetail.labnumber and job.[print]<>'1' and job.labnumber not in (select distinct labnumber from jobdetail where status<>'2' and active='1')"
        strsql = "select distinct sampleno,barcode,status,universaltest,measurementvalue,lowercritical,lowernormal,uppernormal,uppercritical,resultstatus,resultabnormalflag,testtype.displayindex,testtype.digit from jobdetail,testtype where labnumber='" & selectedLabNumber & "' and active='1' and status='2' and jobdetail.idtesttype=testtype.id order by displayindex asc"
        Dim tblData As New DataTable

        tblData = objGreConnect.ExecuteQuery(strsql) ' order by testgroupname asc")
        dgvdetail.DataSource = tblData

        For j = 0 To dgvdetail.Columns.Count - 1
            dgvdetail.Columns(j).Visible = False
        Next

        dgvdetail.Columns("universaltest").Visible = True
        dgvdetail.Columns("universaltest").HeaderText = "Test Type"
        dgvdetail.Columns("universaltest").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("universaltest").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("universaltest").Width = 100
        dgvdetail.Columns("universaltest").ReadOnly = True



        index_position_result = dgvdetail.Columns("measurementvalue").Index
        dgvdetail.Columns("measurementvalue").Visible = True
        dgvdetail.Columns("measurementvalue").HeaderText = "Nilai Hasil"
        dgvdetail.Columns("measurementvalue").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("measurementvalue").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("measurementvalue").Width = 120
        dgvdetail.Columns("measurementvalue").ReadOnly = True
      

        dgvdetail.Columns("lowercritical").Visible = False
        dgvdetail.Columns("lowercritical").HeaderText = "Lower Critical"
        dgvdetail.Columns("lowercritical").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("lowercritical").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("lowercritical").Width = 80


        dgvdetail.Columns("lowernormal").Visible = False
        dgvdetail.Columns("lowernormal").HeaderText = "Lower Normal"
        dgvdetail.Columns("lowernormal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("lowernormal").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("lowernormal").Width = 80

        dgvdetail.Columns("uppernormal").Visible = False
        dgvdetail.Columns("uppernormal").HeaderText = "Upper Normal"
        dgvdetail.Columns("uppernormal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("uppernormal").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("uppernormal").Width = 80

        dgvdetail.Columns("uppercritical").Visible = False
        dgvdetail.Columns("uppercritical").HeaderText = "Upper Critical"
        dgvdetail.Columns("uppercritical").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("uppercritical").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("uppercritical").Width = 80

        dgvdetail.Columns("resultstatus").Visible = False
        dgvdetail.Columns("resultstatus").HeaderText = "Result Status"
        dgvdetail.Columns("resultstatus").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("resultstatus").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("resultstatus").Width = 80


        dgvdetail.Columns("resultabnormalflag").Visible = True
        dgvdetail.Columns("resultabnormalflag").HeaderText = "Abnormal Flag"
        dgvdetail.Columns("resultabnormalflag").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("resultabnormalflag").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvdetail.Columns("resultabnormalflag").Width = 80
        dgvdetail.Columns("resultabnormalflag").ReadOnly = True



        'Dim buttonColumn As New DataGridViewButtonColumn()
        'With buttonColumn
        '    .Name = "Detail"
        '    .HeaderText = "Detail"
        '    .Width = 90
        '    .Text = "Detail"
        '    .UseColumnTextForButtonValue = True
        '    .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        '    .FlatStyle = FlatStyle.Standard
        '    .CellTemplate.Style.BackColor = Color.Yellow
        '    .DisplayIndex = 0
        'End With
        'dgvdetail.Columns.Add(buttonColumn)

        dgvdetail.AllowUserToAddRows = False
        dgvdetail.RowHeadersVisible = False

        objGreConnect.CloseConn()

    End Sub
    Private Function GetUniversalTestName(ByVal sampleno As String, ByVal labnumber As String) As String
        Dim strfinder As String
        Dim strresult As String
        Dim testcon As New clsGreConnect
        testcon.buildConn()
        strfinder = "select universaltest from jobdetail where sampleno='" & sampleno & "' and labnumber='" & labnumber & "'"
        Dim thecommand As New SqlClient.SqlCommand(strfinder, testcon.grecon)
        Dim theReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)
        strresult = ""
        Do While theReader.Read
            strresult = strresult & " " & theReader("universaltest")
        Loop
        thecommand = Nothing
        theReader.Close()
        testcon.CloseConn()
        testcon = Nothing

        Return strresult
    End Function





    Private Sub clearDGVdetail()
        'If dgvdetail.Rows.Count > 0 Then
        '    dgvdetail.Rows.Clear()
        'End If
    End Sub







    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim ln As String

        'strcn = "Driver={PostgreSQL ANSI};database=MEDLIS;server=127.0.0.1;port=5432;uid=dian;sslmode=disable;readonly=0;protocol=7.4;User ID=dian;password=dianjuga;"

        Dim rptreportfrm As New resultwithparamandconparamFrm(selectedLabNumber, stateChoice, strdbconn)
        rptreportfrm.MdiParent = MainFrm
        rptreportfrm.Show()
    End Sub

    Private Sub btnLabReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rptlabreport As New rptLabReportFrm(selectedLabNumber, selectedPatient)
        rptlabreport.MdiParent = MainFrm
        rptlabreport.Show()

    End Sub


    Private Sub ShowDataDetail(ByVal intshow As Integer)
        stateChoice = FINISHSAMPLE
        ShowButton()
        Dim strsql As String
        ' strsql = "select job.id as jobid,job.idpasien,patient.patientname as name,patient.idpatient as patientid,job.labnumber,patient.address, patient.phone from job,patient where patient.id=job.idpasien and job.labnumber in (select distinct labnumber from jobdetail where status='" & FINISHSAMPLE & "' and active='" & ACTIVESAMPLE & "')"
        If tsprintnot.SelectedIndex = 0 Then
            strsql = "select distinct top " & intshow & " job.labnumber,job.idpasien,patient.phone,patient.patientname as name,patient.malefemale as gender,job.datefinish,patient.address,patient.idpatient as patientid,jobdetail.labnumber from job,patient,jobdetail where job.idpasien=patient.id and job.labnumber=jobdetail.labnumber and job.[print]<>'" & PRINTALREADY & "' and job.labnumber not in (select distinct labnumber from jobdetail where status<>'" & FINISHSAMPLE & "' and active='" & ACTIVESAMPLE & "') order by job.datefinish desc"
        ElseIf tsprintnot.SelectedIndex = 1 Then
            strsql = "Select distinct top " & intshow & " job.labnumber,job.idpasien,patient.phone,patient.patientname As name,patient.malefemale As gender,job.datefinish,patient.address,patient.idpatient As patientid,jobdetail.labnumber from job,patient,jobdetail where job.idpasien=patient.id And job.labnumber=jobdetail.labnumber And job.[print]='" & PRINTALREADY & "' and job.labnumber not in (select distinct labnumber from jobdetail where status<>'" & FINISHSAMPLE & "' and active='" & ACTIVESAMPLE & "') order by job.datefinish desc" ' limit " & intshow
        End If

        dgvJoblist.DataSource = Nothing
        If dgvJoblist.Columns.Contains("Detail") Then
            dgvJoblist.Columns.Clear()
        End If
        clearDGVdetail()
        Joblist(strsql)
        lblStatus.Text = "Selesai/Sudah ada hasil"
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCetak.Click
        ' printresult
        Try
            ReadLicense()
            If selectedLabNumber.Length <> 0 Then

                Dim rpt As New PatientResult
                rpt.Parameters("LabNumber").Value = selectedLabNumber
                rpt.Parameters("LabNumber").Visible = False

                Dim preview As New ReportPrintTool(rpt)
                preview.ShowPreviewDialog()

                'Dim excelPath As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "patient-result.xlsx")
                ''Dim excelPath As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "new.xlsx")
                'Dim ef As ExcelFile = ExcelFile.Load(excelPath)
                'Dim ws As ExcelWorksheet = ef.Worksheets(0)
                ''Dim ef As ExcelFile = New ExcelFile
                ''Dim ws As ExcelWorksheet = ef.Worksheets.Add("abc")
                'Dim datereceived As Date
                'Dim idpasien As Integer
                'Dim docname As String
                'Dim docaddress As String
                'Dim age As String
                'Dim strsql As String
                'strsql = "select idpasien,docname,docaddress,printage,datereceived from job where labnumber='" & selectedLabNumber & "'"

                'Dim greshow As New clsGreConnect
                'greshow.buildConn()

                'Dim cmd As New  SqlClient.SqlCommand(strsql, greshow.grecon)
                'Dim rdr As  SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                'Do While rdr.Read
                '    idpasien = rdr("idpasien")
                '    If Not IsDBNull(rdr("docname")) Then
                '        docname = rdr("docname")
                '    Else
                '        docname = ""
                '    End If
                '    If Not IsDBNull(rdr("docaddress")) Then
                '        docaddress = rdr("docaddress")
                '    Else
                '        docaddress = ""
                '    End If
                '    If Not IsDBNull(rdr("printage")) Then
                '        age = rdr("printage")
                '    Else
                '        age = " "
                '    End If

                '    If Not IsDBNull(rdr("datereceived")) Then
                '        datereceived = rdr("datereceived")
                '    Else

                '    End If
                'Loop

                'rdr.Close()
                'cmd.Dispose()
                'greshow.CloseConn()

                'Dim datetoprint As String
                'datetoprint = CStr(datereceived.Day) & "/" & CStr(datereceived.Month) & "/" & CStr(datereceived.Year)


                ''===========================================
                'strsql = "select * from patient where id='" & idpasien & "'"
                'Dim grepatient As New clsGreConnect
                'grepatient.buildConn()

                'Dim alamatpasien As String
                'Dim namapasien As String
                'Dim sex As String
                'Dim tgllahir As String
                'Dim phone As String
                'Dim printIdPasien As String

                'Dim cmdpatient As New  SqlClient.SqlCommand(strsql, grepatient.grecon)
                'Dim rdrpatient As  SqlDataReader = cmdpatient.ExecuteReader(CommandBehavior.CloseConnection)
                'Do While rdrpatient.Read
                '    namapasien = rdrpatient("patientname")
                '    alamatpasien = rdrpatient("address")
                '    If rdrpatient("malefemale") = genderFemale Then
                '        sex = "Wanita"
                '    ElseIf rdrpatient("malefemale") = genderMale Then
                '        sex = "Laki Laki"
                '    ElseIf rdrpatient("malefemale") = genderChild Then
                '        sex = "Anak anak"
                '    End If

                '    tgllahir = rdrpatient("birthdate")
                '    phone = rdrpatient("phone")
                '    printIdPasien = rdrpatient("idpatient")
                'Loop

                'rdrpatient.Close()
                'cmdpatient.Dispose()
                'grepatient.CloseConn()

                ''=========================================== analisa
                'strsql = "select jobdetail.labnumber,testtype.testname,jobdetail.resultabnormalflag,jobdetail.measurementvalue,testtype.keterangan,testtype.nilairujukan,testtype.satuan,testtype.displayindex,testtype.digit from jobdetail,testtype where jobdetail.idtesttype=testtype.id  and  jobdetail.labnumber='" & selectedLabNumber & "' order by testtype.displayindex asc"
                'Dim greanalisa As New clsGreConnect
                'greanalisa.buildConn()

                'Dim testname As String
                'Dim resultabnormalflag As String
                'Dim hasil As String
                'Dim ket As String
                'Dim rujukan As String
                'Dim satuan As String
                'Dim intRow As Integer = 12

                'Dim rctestname As String
                'Dim rchasil As String
                'Dim rcrujukan As String
                'Dim rcsatuan As String
                'Dim rcket As String


                'Dim row As Integer = 12
                'Dim col As Integer = 0

                'Dim cmdanalisa As New  SqlClient.SqlCommand(strsql, greanalisa.grecon)
                'Dim rdranalisa As  SqlDataReader = cmdanalisa.ExecuteReader(CommandBehavior.CloseConnection)
                'Do While rdranalisa.Read
                '    testname = rdranalisa("testname")

                '    'rctestname = "A" & CStr(intRow)
                '    'ws.Cells(rctestname).Value = testname
                '    ws.Cells(row, 0).Value = testname
                '    ws.Cells(row, 0).Style.WrapText = False
                '    If IsNumeric(rdranalisa("measurementvalue")) = True Then
                '        hasil = Math.Round(CDbl(rdranalisa("measurementvalue")), rdranalisa("digit"), MidpointRounding.AwayFromZero).ToString
                '    Else
                '        hasil = rdranalisa("measurementvalue")
                '    End If

                '    'rchasil = "C" & CStr(intRow)
                '    'ws.Cells(rchasil).Value = hasil
                '    resultabnormalflag = rdranalisa("resultabnormalflag")
                '    If Trim(resultabnormalflag) = "N" Then
                '        ws.Cells(row, 2).Value = hasil
                '    Else
                '        ws.Cells(row, 2).Value = hasil & " *"
                '    End If


                '    rujukan = rdranalisa("nilairujukan")
                '    'rcrujukan = "E" & CStr(intRow)
                '    'ws.Cells(rcrujukan).Value = rujukan
                '    ws.Cells(row, 4).Value = rujukan

                '    satuan = rdranalisa("satuan")
                '    'rcsatuan = "G" & CStr(intRow)
                '    'ws.Cells(rcsatuan).Value = satuan
                '    ws.Cells(row, 6).Value = satuan

                '    ket = rdranalisa("keterangan")
                '    rcket = "H" & CStr(intRow)
                '    'ws.Cells(rcket).Style.WrapText = True
                '    'ws.Cells(rcket).Value = ket
                '    ws.Cells(row, 7).Value = ket
                '    'ws.Cells(
                '    ws.Cells(row, 7).Style.WrapText = True

                '    'intRow = intRow + 1
                '    'if int row > 38,83,128 then..  loncat ke 57,101
                '    row = row + 1

                '    If row = 36 Then '39 awalnya
                '        row = 57
                '    ElseIf row = 81 Then
                '        row = 101
                '    End If

                'Loop

                'rdranalisa.Close()
                'cmdanalisa.Dispose()
                'greanalisa.CloseConn()


                ''ws.HeadersFooters.FirstPage.Header.CenterSection.Content = "Laboratorium Klinik"
                ''ws.HeadersFooters.FirstPage.Header.RightSection.Content = "Laboratorium Klinik"



                'ws.Cells("B2").Value = ": " & docname
                'ws.Cells("B3").Value = ": " & docaddress
                'ws.Cells("B4").Value = ": " & selectedLabNumber
                'ws.Cells("B5").Value = ": " & printIdPasien
                'ws.Cells("B6").Value = ": " & namapasien
                'ws.Cells("B7").Value = ": " & alamatpasien
                'ws.Cells("B7").Style.WrapText = True

                'ws.Cells("H3").Value = ": " & sex
                'ws.Cells("H4").Value = ": " & tgllahir
                'ws.Cells("H5").Value = ": " & age
                'ws.Cells("H6").Value = ": " & phone
                'ws.Cells("H7").Value = ": " & datetoprint

                'If row > 36 And row < 81 Then

                '    Dim cr As CellRange
                '    cr = ws.Cells.GetSubrange("A2", "I10")
                '    cr.CopyTo("A47")

                '    Dim crbottom As CellRange
                '    crbottom = ws.Cells.GetSubrange("A39,I45")
                '    crbottom.CopyTo("A84")
                'ElseIf row > 81 Then
                '    Dim cr As CellRange
                '    cr = ws.Cells.GetSubrange("A2", "I10")
                '    cr.CopyTo("A47")
                '    cr.CopyTo("A92")

                '    Dim crbottom As CellRange
                '    crbottom = ws.Cells.GetSubrange("A39,I45")
                '    crbottom.CopyTo("A129")

                'End If

                'Dim linepos As Integer
                'linepos = row + 5
                'Dim crl As CellRange = ws.Cells.GetSubrange("A" & CStr(linepos), "H" & CStr(linepos))

                'crl.Style.Borders.SetBorders(MultipleBorders.Bottom, SpreadsheetColor.FromName(ColorName.Black), LineStyle.Thin)
                'ws.Cells("E" & CStr(linepos + 2)).Value = "DiCetak oleh: " & user_complete_name
                'ws.Cells("E" & CStr(linepos + 3)).Value = "Tanggal: " & Now()
                'ws.Cells("E" & CStr(linepos + 4)).Value = "Disetujui oleh: "



                ''If row < 57 Then
                ''    'Dim cr As CellRange = ws.Cells.GetSubrange("A43", "H43")
                ''    'cr.Style.Borders.SetBorders(MultipleBorders.Bottom, SpreadsheetColor.FromName(ColorName.Black), LineStyle.Thin)
                ''    'ws.Cells("E45").Value = "DiCetak oleh: " & user_complete_name
                ''    'ws.Cells("E46").Value = "Tanggal: " & Now()
                ''    'ws.Cells("E47").Value = "Disetujui oleh: "
                ''    Dim linepos As Integer
                ''    linepos = row + 5
                ''    Dim cr As CellRange = ws.Cells.GetSubrange("A" & CStr(linepos), "H" & CStr(linepos))

                ''    cr.Style.Borders.SetBorders(MultipleBorders.Bottom, SpreadsheetColor.FromName(ColorName.Black), LineStyle.Thin)
                ''    ws.Cells("E" & CStr(linepos + 2)).Value = "DiCetak oleh: " & user_complete_name
                ''    ws.Cells("E" & CStr(linepos + 3)).Value = "Tanggal: " & Now()
                ''    ws.Cells("E" & CStr(linepos + 4)).Value = "Disetujui oleh: "

                ''ElseIf row > 57 Then

                ''    Dim cr As CellRange = ws.Cells.GetSubrange("A90", "H90")
                ''    cr.Style.Borders.SetBorders(MultipleBorders.Bottom, SpreadsheetColor.FromName(ColorName.Black), LineStyle.Thin)
                ''    ws.Cells("E92").Value = "DiCetak oleh: " & user_complete_name
                ''    ws.Cells("E93").Value = "Tanggal: " & Now()
                ''    ws.Cells("E94").Value = "Disetujui oleh: "

                ''ElseIf row > 81 Then
                ''    Dim cr As CellRange = ws.Cells.GetSubrange("A137", "H137")
                ''    cr.Style.Borders.SetBorders(MultipleBorders.Bottom, SpreadsheetColor.FromName(ColorName.Black), LineStyle.Thin)
                ''    ws.Cells("E139").Value = "DiCetak oleh: " & user_complete_name
                ''    ws.Cells("E140").Value = "Tanggal: " & Now()
                ''    ws.Cells("E141").Value = "Disetujui oleh: "
                ''End If



                ''Dim excResult As String
                ''Dim excResult As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "patient-result.xlsx")

                'ef.Save("hasil.xlsx")
                'Dim xpsDoc = ef.ConvertToXpsDocument(SaveOptions.XpsDefault)
                'Dim objDocView As New frmDocShowResult
                'objDocView.StartPosition = FormStartPosition.Manual
                'objDocView.Left = 0
                'objDocView.Top = 0
                'objDocView.ShowDialog()




            Else
                MessageBox.Show("Silahkan pilih lab number yang hendak di cetak")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try




    End Sub

    Private Sub tsprintnot_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsprintnot.SelectedIndexChanged
        If rd30.Checked = True Then
            ShowDataDetail(30)
        ElseIf rd80.Checked = True Then
            ShowDataDetail(80)
        Else
            ShowDataDetail(10000)
        End If
    End Sub

  

    Private Sub AllFinishJobsFrm_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        'ShowDataDetail(30)
        rd30.Checked = True
    End Sub

    Private Sub rd30_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rd30.CheckedChanged
        If rd30.Checked = True Then
            ShowDataDetail(30)
        End If
    End Sub

    Private Sub rd80_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rd80.CheckedChanged
        If rd80.Checked = True Then
            ShowDataDetail(80)
        End If
    End Sub

    Private Sub rdsemua_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdsemua.CheckedChanged
        If rd80.Checked = True Then
            ShowDataDetail(10000)
        End If
    End Sub

    Private Sub dgvdetail_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvdetail.CellFormatting

        If e.ColumnIndex = index_position_result Then
            'penting / bagus / top / contoh
            'Dim d As Double = CDbl(e.Value.ToString)
            'dgvdetail.CurrentCell.
            'dgvdetail.Columns("id
            '---   Dim d As Double = Math.Round(CDbl(e.Value), digitResult, MidpointRounding.AwayFromZero)
            '--- e.Value = d.ToString  '.ToString("N2")
        End If

            

    End Sub


    
    Private Sub DigitChange()
        Dim i As Integer
        Dim selected_test_itemid As Integer
        Dim barcodetest As String

        Dim instrumentName As String
        Dim desc As String

        'desc = Trim(txtdesc.Text)

        For i = 0 To dgvdetail.Rows.Count - 1
            If IsNumeric(dgvdetail.Item("measurementvalue", i)) = True Then
                Dim d As Double = Math.Round(CDbl(dgvdetail.Item("measurementvalue", i).Value), dgvdetail.Item("digit", i).Value, MidpointRounding.AwayFromZero)
                dgvdetail.Item("measurementvalue", i).Value = d

            End If
                        'selected_test_itemid = dgvdetail.Item("id", i).Value
            'barcodetest = dgvdetail.Item("txt", i).Value
            'Instrment_job_insert_one_per_row(instrumentName, desc, selected_test_itemid, barcodetest) 'nyimpen capa
            'End If
            'End If
        Next
    End Sub
    'Private Function digit_to_show() As Integer

    ' End Function

End Class