﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MindRayBC3600
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series
        Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea
        Dim Legend2 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series
        Dim ChartArea3 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea
        Dim Legend3 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend
        Dim Series3 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MindRayBC3600))
        Me.tmrReceive = New System.Windows.Forms.Timer(Me.components)
        Me.tmrSend = New System.Windows.Forms.Timer(Me.components)
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.cbInstrument = New System.Windows.Forms.ComboBox
        Me.txtport = New System.Windows.Forms.TextBox
        Me.txtbaud = New System.Windows.Forms.TextBox
        Me.txtparity = New System.Windows.Forms.TextBox
        Me.txtstop = New System.Windows.Forms.TextBox
        Me.txtdata = New System.Windows.Forms.TextBox
        Me.cmdSaveIns = New System.Windows.Forms.Button
        Me.cmdEdit = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dghema = New System.Windows.Forms.DataGridView
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.lblbarcode = New System.Windows.Forms.Label
        Me.wbcchart = New System.Windows.Forms.DataVisualization.Charting.Chart
        Me.rbcchart = New System.Windows.Forms.DataVisualization.Charting.Chart
        Me.pltchart = New System.Windows.Forms.DataVisualization.Charting.Chart
        Me.tmrstop = New System.Windows.Forms.Timer(Me.components)
        Me.btnDisconnect = New System.Windows.Forms.Button
        Me.btnConnect = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        CType(Me.dghema, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.wbcchart, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rbcchart, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pltchart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tmrReceive
        '
        Me.tmrReceive.Interval = 500
        '
        'tmrSend
        '
        Me.tmrSend.Interval = 500
        '
        'cbInstrument
        '
        Me.cbInstrument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbInstrument.FormattingEnabled = True
        Me.cbInstrument.Location = New System.Drawing.Point(66, 33)
        Me.cbInstrument.Name = "cbInstrument"
        Me.cbInstrument.Size = New System.Drawing.Size(140, 23)
        Me.cbInstrument.TabIndex = 62
        '
        'txtport
        '
        Me.txtport.Location = New System.Drawing.Point(66, 65)
        Me.txtport.Name = "txtport"
        Me.txtport.Size = New System.Drawing.Size(100, 23)
        Me.txtport.TabIndex = 63
        '
        'txtbaud
        '
        Me.txtbaud.Location = New System.Drawing.Point(66, 103)
        Me.txtbaud.Name = "txtbaud"
        Me.txtbaud.Size = New System.Drawing.Size(100, 23)
        Me.txtbaud.TabIndex = 64
        '
        'txtparity
        '
        Me.txtparity.Location = New System.Drawing.Point(66, 141)
        Me.txtparity.Name = "txtparity"
        Me.txtparity.Size = New System.Drawing.Size(100, 23)
        Me.txtparity.TabIndex = 65
        '
        'txtstop
        '
        Me.txtstop.Location = New System.Drawing.Point(66, 179)
        Me.txtstop.Name = "txtstop"
        Me.txtstop.Size = New System.Drawing.Size(100, 23)
        Me.txtstop.TabIndex = 66
        '
        'txtdata
        '
        Me.txtdata.Location = New System.Drawing.Point(66, 217)
        Me.txtdata.Name = "txtdata"
        Me.txtdata.Size = New System.Drawing.Size(100, 23)
        Me.txtdata.TabIndex = 67
        '
        'cmdSaveIns
        '
        Me.cmdSaveIns.Location = New System.Drawing.Point(117, 257)
        Me.cmdSaveIns.Name = "cmdSaveIns"
        Me.cmdSaveIns.Size = New System.Drawing.Size(65, 37)
        Me.cmdSaveIns.TabIndex = 68
        Me.cmdSaveIns.Text = "Pilih"
        Me.cmdSaveIns.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(45, 257)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(65, 37)
        Me.cmdEdit.TabIndex = 69
        Me.cmdEdit.Text = "Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 15)
        Me.Label2.TabIndex = 70
        Me.Label2.Text = "Nama"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(29, 69)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 15)
        Me.Label3.TabIndex = 71
        Me.Label3.Text = "Port"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(25, 106)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 15)
        Me.Label4.TabIndex = 72
        Me.Label4.Text = "Baud"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(21, 143)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(39, 15)
        Me.Label5.TabIndex = 73
        Me.Label5.Text = "Parity"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 181)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(52, 15)
        Me.Label6.TabIndex = 74
        Me.Label6.Text = "Stop Bit"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 220)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(52, 15)
        Me.Label7.TabIndex = 75
        Me.Label7.Text = "Data Bit"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cmdEdit)
        Me.GroupBox1.Controls.Add(Me.cmdSaveIns)
        Me.GroupBox1.Controls.Add(Me.txtdata)
        Me.GroupBox1.Controls.Add(Me.txtstop)
        Me.GroupBox1.Controls.Add(Me.txtparity)
        Me.GroupBox1.Controls.Add(Me.txtbaud)
        Me.GroupBox1.Controls.Add(Me.txtport)
        Me.GroupBox1.Controls.Add(Me.cbInstrument)
        Me.GroupBox1.Location = New System.Drawing.Point(11, 162)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(217, 312)
        Me.GroupBox1.TabIndex = 76
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Instrument"
        '
        'dghema
        '
        Me.dghema.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dghema.Location = New System.Drawing.Point(243, 57)
        Me.dghema.Name = "dghema"
        Me.dghema.Size = New System.Drawing.Size(470, 486)
        Me.dghema.TabIndex = 80
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(11, 114)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(217, 23)
        Me.ProgressBar1.TabIndex = 81
        '
        'lblbarcode
        '
        Me.lblbarcode.AutoSize = True
        Me.lblbarcode.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblbarcode.Location = New System.Drawing.Point(242, 24)
        Me.lblbarcode.Name = "lblbarcode"
        Me.lblbarcode.Size = New System.Drawing.Size(39, 19)
        Me.lblbarcode.TabIndex = 82
        Me.lblbarcode.Text = "ID: .."
        '
        'wbcchart
        '
        ChartArea1.Name = "ChartArea1"
        Me.wbcchart.ChartAreas.Add(ChartArea1)
        Legend1.Name = "Legend1"
        Me.wbcchart.Legends.Add(Legend1)
        Me.wbcchart.Location = New System.Drawing.Point(719, 57)
        Me.wbcchart.Name = "wbcchart"
        Series1.ChartArea = "ChartArea1"
        Series1.Legend = "Legend1"
        Series1.Name = "Series1"
        Me.wbcchart.Series.Add(Series1)
        Me.wbcchart.Size = New System.Drawing.Size(260, 147)
        Me.wbcchart.TabIndex = 86
        '
        'rbcchart
        '
        ChartArea2.Name = "ChartArea1"
        Me.rbcchart.ChartAreas.Add(ChartArea2)
        Legend2.Name = "Legend1"
        Me.rbcchart.Legends.Add(Legend2)
        Me.rbcchart.Location = New System.Drawing.Point(719, 217)
        Me.rbcchart.Name = "rbcchart"
        Series2.ChartArea = "ChartArea1"
        Series2.Legend = "Legend1"
        Series2.Name = "Series1"
        Me.rbcchart.Series.Add(Series2)
        Me.rbcchart.Size = New System.Drawing.Size(260, 147)
        Me.rbcchart.TabIndex = 87
        Me.rbcchart.Text = "Chart2"
        '
        'pltchart
        '
        ChartArea3.Name = "ChartArea1"
        Me.pltchart.ChartAreas.Add(ChartArea3)
        Legend3.Name = "Legend1"
        Me.pltchart.Legends.Add(Legend3)
        Me.pltchart.Location = New System.Drawing.Point(719, 382)
        Me.pltchart.Name = "pltchart"
        Series3.ChartArea = "ChartArea1"
        Series3.Legend = "Legend1"
        Series3.Name = "Series1"
        Me.pltchart.Series.Add(Series3)
        Me.pltchart.Size = New System.Drawing.Size(260, 147)
        Me.pltchart.TabIndex = 88
        Me.pltchart.Text = "Chart3"
        '
        'tmrstop
        '
        Me.tmrstop.Interval = 1000
        '
        'btnDisconnect
        '
        Me.btnDisconnect.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDisconnect.Image = Global.MEDLIS.My.Resources.Resources.button_stop
        Me.btnDisconnect.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDisconnect.Location = New System.Drawing.Point(97, 28)
        Me.btnDisconnect.Name = "btnDisconnect"
        Me.btnDisconnect.Size = New System.Drawing.Size(80, 66)
        Me.btnDisconnect.TabIndex = 1
        Me.btnDisconnect.Text = "Close"
        Me.btnDisconnect.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDisconnect.UseVisualStyleBackColor = True
        '
        'btnConnect
        '
        Me.btnConnect.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConnect.Image = Global.MEDLIS.My.Resources.Resources.button_play
        Me.btnConnect.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnConnect.Location = New System.Drawing.Point(11, 28)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(80, 66)
        Me.btnConnect.TabIndex = 0
        Me.btnConnect.Text = "Connect"
        Me.btnConnect.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnConnect.UseVisualStyleBackColor = True
        '
        'MindRayBC3600
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1014, 555)
        Me.Controls.Add(Me.pltchart)
        Me.Controls.Add(Me.rbcchart)
        Me.Controls.Add(Me.wbcchart)
        Me.Controls.Add(Me.lblbarcode)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.dghema)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnDisconnect)
        Me.Controls.Add(Me.btnConnect)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MindRayBC3600"
        Me.Text = "MindRay-BC3600 - system 8ID - hshake-off"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dghema, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.wbcchart, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rbcchart, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pltchart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tmrReceive As System.Windows.Forms.Timer
    Friend WithEvents tmrSend As System.Windows.Forms.Timer
    Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
    Friend WithEvents btnConnect As System.Windows.Forms.Button
    Friend WithEvents btnDisconnect As System.Windows.Forms.Button
    Friend WithEvents cbInstrument As System.Windows.Forms.ComboBox
    Friend WithEvents txtport As System.Windows.Forms.TextBox
    Friend WithEvents txtbaud As System.Windows.Forms.TextBox
    Friend WithEvents txtparity As System.Windows.Forms.TextBox
    Friend WithEvents txtstop As System.Windows.Forms.TextBox
    Friend WithEvents txtdata As System.Windows.Forms.TextBox
    Friend WithEvents cmdSaveIns As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dghema As System.Windows.Forms.DataGridView
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents lblbarcode As System.Windows.Forms.Label
    Friend WithEvents wbcchart As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents rbcchart As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents pltchart As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents tmrstop As System.Windows.Forms.Timer

End Class
