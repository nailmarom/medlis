﻿Imports System
Imports System.ComponentModel
Imports System.Threading
Imports System.IO.Ports
Imports System.Text
Imports System.Text.RegularExpressions


Imports System.Data.SqlClient

Public Class frmConnectSPTimer

    Dim cmport As New SerialPort
    Dim objconn As New clsGreConnect
    Dim tblData As DataTable
    Dim objTestRelated As New TestRelated
    Dim strsql As String


    Dim myPort As Array
    Friend strFromCom As New List(Of String)
    Dim isAck As Boolean
    Dim curIndexSend As Integer
    Dim lastIndexSend As Integer
    Dim isMessageCreated As Boolean

    Dim isEOT As Boolean
    Dim receiveMode As Boolean

    Dim McnIsQ As Boolean
    Dim McnIsOR As Boolean


    Dim regex As Regex

    Dim qMcnToLis As New List(Of String)
    Dim oLisToMcn As New List(Of String)
    Dim orMcnToLis As New List(Of String)

    '----------
    Dim fInstrument As String
    Dim fportName As String
    Dim fbaud As String
    Dim fparity As String
    Dim fstop As String
    Dim fdata As String
    Dim fpath As String

    '---------

    Private Sub btnStartReceive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tmrReceive.Enabled = True
        'tmrReceive.s
    End Sub

    Private Sub tmrReceive_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrReceive.Tick
        'catch port content
        Dim newReceivedData As String

        newReceivedData = SerialPort1.ReadExisting
        'isAck = False aslinya ada

        If newReceivedData.Count > 0 Then

            '------------------- tampilan --------------------------
            If lblreceive.BackColor = Color.Green Then
                lblreceive.BackColor = Color.GreenYellow
            ElseIf lblreceive.BackColor = Color.GreenYellow Then
                lblreceive.BackColor = Color.Green
            End If
            '------------------- tampilan --------------------------

            'Dim strcari As String
            'If newReceivedData.Length > 1 Then
            '    strcari = newReceivedData.Substring(Text.Length - 1) = Chr(10)
            '    MessageBox.Show(strcari)
            'End If
            If newReceivedData = Chr(5) Then '[ENQ]
                rtbReceived.Text = rtbReceived.Text & "enq" 'newReceivedData
                receiveMode = True
                lastIndexSend = -1
                isEOT = False
                isMessageCreated = False
                SerialPort1.Write(Chr(6)) '[ACK]
                'txtTransmit.Text = txtTransmit.Text & "[ack]" & Chr(10)
                '  'ElseIf newReceivedData.Substring(Text.Length - 1) = Chr(10) Then 'LF
                ' '   SerialPort1.Write(Chr(6)) '[ACK]
                'txtTransmit.Text = txtTransmit.Text & "[ack]" & Chr(10)
                '-------------
                'ElseIf detectComType(newReceivedData) = "H" Then
                '    SerialPort1.Write(Chr(6))
                'ElseIf detectComType(newReceivedData) = "Q" Then
                '    SerialPort1.Write(Chr(6))
                'ElseIf detectComType(newReceivedData) = "L" Then
                '    SerialPort1.Write(Chr(6))
            ElseIf newReceivedData = Chr(4) Then '[EOT] ->TIME TO begin SPEAK TO ROBOT
                'time to speak
                'build message to send
                rtbReceived.Text = rtbReceived.Text & "eot"
                receiveMode = False
                isEOT = True
                SerialPort1.DiscardInBuffer()  'baru clear serial port
            ElseIf newReceivedData.Substring(newReceivedData.Length - 1) = Chr(10) Then 'LF
                '    rtbReceived.Text = rtbReceived.Text & newReceivedData
                rtbReceived.Text = rtbReceived.Text & newReceivedData
                SerialPort1.Write(Chr(6)) '[ACK]
            ElseIf newReceivedData = Chr(6) Then '[ACK]
                rtbReceived.Text = rtbReceived.Text & "ack"
                isAck = True
                curIndexSend = curIndexSend + 1
            End If
        End If

        If receiveMode = True Then
            strFromCom.Add(newReceivedData)
        End If


        'rtbReceived.Text = rtbReceived.Text & newReceivedData

        
        If isEOT = True And isMessageCreated = False Then
            isEOT = False
            HandleMcnMessage()
        End If

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        lblsend.Text = ""
        lblsend.BackColor = Color.Blue
        lblreceive.Text = ""
        lblreceive.BackColor = Color.Green


        myPort = IO.Ports.SerialPort.GetPortNames() 'Get all com ports available
        'cmbBaud.Items.Add(9600) 'Populate the cmbBaud Combo box to common baud rates used 
        'cmbBaud.Items.Add(19200)
        'cmbBaud.Items.Add(38400)
        'cmbBaud.Items.Add(57600)
        'cmbBaud.Items.Add(115200)

        'For i = 0 To UBound(myPort) 
        '    cmbPort.Items.Add(myPort(i))
        'Next
        'cmbPort.Text = cmbPort.Items.Item(0) 'Set cmbPort text to the first COM port detected
        'cmbBaud.Text = cmbBaud.Items.Item(0) 'Set cmbBaud text to the first Baud rate on the list

        btnDisconnect.Enabled = False

        'load instrument data
        'load file, if is not avalaible then LoadInstrument Name manually
        '  fpath = "C:\Windows\System32\greSetting.ini" System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "greSetting.ini")
        LoadInstrumentName()
        fpath = System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData, "instrument1.txt")
        If ReadInsProperty() = False Then
            MessageBox.Show("Silahkan pilih instrument")
            cbInstrument.Enabled = True
            cmdSaveIns.Enabled = True
        Else
            cmdSaveIns.Enabled = False
            cbInstrument.Enabled = True
            cbInstrument.SelectedIndex = cbInstrument.FindStringExact(fInstrument)
            cbInstrument.Enabled = False
        End If


    End Sub

    Private Sub btnDisconnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisconnect.Click
        tmrReceive.Stop()
        tmrSend.Stop()
        SerialPort1.Close() 'Close our Serial Port
        btnConnect.Enabled = True
        btnDisconnect.Enabled = False
    End Sub

    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click

        If Trim(cbInstrument.Text) = "" Then
            MessageBox.Show("Instrument untuk komputer ini mengalami perubahan, silahkan pilih yang baru")
            Exit Sub
        End If

        '--------------
        If txtport.Text <> "" Then
            SerialPort1.PortName = Trim(txtport.Text)
        End If

        If txtbaud.Text <> "" Then
            SerialPort1.BaudRate = Trim(txtbaud.Text)
        End If

        If txtparity.Text <> "" Then
            Dim par As String
            par = Trim(txtparity.Text)
            If par = "None" Then
                SerialPort1.Parity = IO.Ports.Parity.None
            ElseIf par = "Even" Then
                SerialPort1.Parity = IO.Ports.Parity.Even
            ElseIf par = "Mark" Then
                SerialPort1.Parity = IO.Ports.Parity.Mark
            ElseIf par = "Odd" Then
                SerialPort1.Parity = IO.Ports.Parity.Odd
            End If
        End If

        If txtstop.Text <> "" Then
            Dim stp As String
            stp = txtstop.Text
            If stp = "None" Then
                SerialPort1.StopBits = IO.Ports.StopBits.None
            ElseIf stp = "One" Then
                SerialPort1.StopBits = IO.Ports.StopBits.One
            ElseIf stp = "Two" Then
                SerialPort1.StopBits = IO.Ports.StopBits.Two
            End If
        End If

        If txtdata.Text <> "" Then
            Dim datab As String
            datab = txtdata.Text
            SerialPort1.DataBits = CInt(datab)

        End If

        '--------------

        '***************
        'SerialPort1.PortName = Trim(cmbPort.Text) 'Set SerialPort1 to the selected COM port at startup
        'SerialPort1.BaudRate = Trim(cmbBaud.Text) 'Set Baud rate to the selected value on 

        ''Other Serial Port Property
        'SerialPort1.Parity = IO.Ports.Parity.None
        'SerialPort1.StopBits = IO.Ports.StopBits.One
        'SerialPort1.DataBits = 8 'Open our serial port 
        '**************
        ' SerialPort1.

        SerialPort1.Open()
        btnConnect.Enabled = False 'Disable Connect button
        btnDisconnect.Enabled = True 'and Enable Disconnect button
        tmrReceive.Start()

    End Sub



    Private Sub btnSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'SerialPort1.Write(rtbSend.Text & vbCr)
        'The text contained in the txtText will be sent to the serial port as ascii
        'plus the carriage return (Enter Key) the carriage return can be ommitted if the other end does not need it		
    End Sub

    Private Sub tmrSend_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrSend.Tick

        '------------- tampilan -------------------
        If lblsend.BackColor = Color.LightBlue Then
            lblsend.BackColor = Color.Blue
        ElseIf lblsend.BackColor = Color.Blue Then
            lblsend.BackColor = Color.LightBlue
        End If
        '------------- tampilan -------------------

        Dim cansend As Boolean
        If isAck = True Then
            cansend = True
        Else
            cansend = False
        End If
        If cansend = True Then 'tambahan 
            If curIndexSend < oLisToMcn.Count Then
                If lastIndexSend < curIndexSend Then
                    rtbSend.Text = rtbSend.Text & oLisToMcn.Item(curIndexSend)
                    ' SerialPort1.
                    SerialPort1.Write(oLisToMcn.Item(curIndexSend))
                    isAck = False
                    lastIndexSend = curIndexSend
                    If curIndexSend = oLisToMcn.Count - 1 Then
                        tmrSend.Stop()
                        ClearALL()
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub HandleMcnMessage()
        isMessageCreated = True

        Dim itemStr As String
        For Each itemStr In strFromCom
            Dim whatCom As String
            whatCom = detectComType(itemStr)
            If whatCom = "Q" Then
                McnIsQ = True
                McnIsOR = False
                qMcnToLis.Add(itemStr)
            ElseIf whatCom = "O" Then
                McnIsOR = True
                McnIsQ = False
                orMcnToLis.Add(itemStr)
            ElseIf whatCom = "R" Then
                McnIsOR = True
                McnIsQ = False
                orMcnToLis.Add(itemStr)
            ElseIf whatCom = "H" Then
                'detect and get machine name and prameter
                ' 

            End If
        Next
        If McnIsQ = True Then
            If qMcnToLis.Count > 0 Then
                ConstructOMsg() 'bikin pesan

                isAck = True
                curIndexSend = 0
                tmrSend.Start()
            End If
        End If


        If McnIsOR = True Then
            If orMcnToLis.Count > 0 Then
                DistributeResult()
            End If
        End If

        'msgStak = New ArrayList
        'msgStak.Add("satu")
        'msgStak.Add("dua")
        'msgStak.Add("tiga")
        'msgStak.Add("empat")
        'msgStak.Add("lima")
        'msgStak.Add("enam")
        'msgStak.Add("tujuh")

       
    End Sub
    Private Sub DistributeResult()
        'sampai disini

        Dim barcode As String
        Dim sampleNo As String

        Dim isSampleNoMode As Boolean
        Dim isBarcodeMode As Boolean
        Dim requestDate As String
        Dim regexORmessage As String()

        isBarcodeMode = False
        isSampleNoMode = False

        Dim i As Integer
        i = 0
        Do While i < orMcnToLis.Count
            'bersihin string noise disini
            Dim strbersih As String
            'strbersih = orMcnToLis.Item(i).Replace() 
            regexORmessage = regex.Split(orMcnToLis.Item(i), "\|")


            If regexORmessage.ElementAt(0).Substring(regexORmessage.ElementAt(0).Length - 1) = "O" Then
                'extract O message


                'Dim dictQueryMessage As New Dictionary(Of String, String)
                'Dim dictSampleDetail As New Dictionary(Of String, String)
                Dim strSampleDetail As String
                Dim tubeDetail As String()

                strSampleDetail = regexORmessage.ElementAt(2)

                tubeDetail = regex.Split(strSampleDetail, "\^")

                If tubeDetail.ElementAt(0) = "" Then
                    isSampleNoMode = True
                    isBarcodeMode = False
                    sampleNo = tubeDetail.ElementAt(1)
                Else
                    isSampleNoMode = False
                    isBarcodeMode = True
                    barcode = tubeDetail.ElementAt(0)
                End If

                '  If Mid(strSampleDetail, 1, 1) = "^" Then 'sample No mode
                'sampleNo = tubeDetail.ElementAt(1)
                'dictSampleDetail.Add("sampleNo", tubeDetail.ElementAt(0))
                'dictSampleDetail.Add("diskNumber", tubeDetail.ElementAt(1))
                'dictSampleDetail.Add("positionNumber", tubeDetail.ElementAt(2))
                'dictSampleDetail.Add("diluent", tubeDetail.ElementAt(3))

                '  isSampleNoMode = True
                ' Else
                '   barcode = tubeDetail.ElementAt(0)
                'dictSampleDetail.Add("sampleBarcode", tubeDetail.ElementAt(0))
                'dictSampleDetail.Add("diskNumber",' tubeDetail.ElementAt(1))
                'dictSampleDetail.Add("positionNumber", tubeDetail.ElementAt(2))
                'dictSampleDetail.Add("diluent", tubeDetail.ElementAt(3))
                '  isBarcodeMode = True
                ' End If
            ElseIf regexORmessage.ElementAt(0).Substring(regexORmessage.ElementAt(0).Length - 1) = "R" Then
                Dim testname As String
                Dim value As String
                Dim unit As String
                Dim refrange As String
                Dim resultAbnormalFlag As String
                Dim resultStatus As String
                Dim datecomplete As String

                testname = regexORmessage.ElementAt(2).Replace("^", "")
                value = regexORmessage.ElementAt(3)
                unit = regexORmessage.ElementAt(4)
                refrange = regexORmessage.ElementAt(5)
                resultAbnormalFlag = regexORmessage.ElementAt(6)
                resultStatus = regexORmessage.ElementAt(8)

                datecomplete = regexORmessage.ElementAt(12).Substring(0, 14) 'sudah bersih

                'database related
                'Dim seldate As Date = Date.Parse(dpBirth.Text)
                'birthdate = Format(seldate, "yyyy-MM-dd")


                'strsql = "insert into patient (" & _
                '            "measurementvalue,resultstatus,resultabnormalflag,datecomplete,referencerange)" & _
                '            " VALUES (" & _
                '             "'" & value & "','" & resultStatus & "','" & resultAbnormalFlag & "','" & datecomplete & "','" & refrange & "')"

                Dim two_range As String()

                Dim normal_string As String
                Dim critical_string As String

                Dim normal_val As String()
                Dim lower_normal As String
                Dim upper_normal As String

                Dim critical_val As String()
                Dim lower_critical As String
                Dim upper_critical As String

                Dim strsource As String

                strsource = refrange
                two_range = regex.Split(strsource, "\\")
                If strsource = "" Then
                    lower_normal = ""
                    lower_critical = ""
                    upper_normal = ""
                    upper_critical = ""
                Else
                    normal_string = two_range.ElementAt(0)
                    critical_string = two_range.ElementAt(1)

                    If normal_string = "" Then
                        lower_normal = ""
                        upper_normal = ""
                    Else
                        normal_val = regex.Split(normal_string, "\^")
                        lower_normal = normal_val.ElementAt(0)
                        upper_normal = normal_val.ElementAt(1)
                    End If

                    If critical_string = "" Then
                        lower_critical = ""
                        upper_critical = ""
                    Else
                        critical_val = regex.Split(critical_string, "\^")
                        lower_critical = critical_val.ElementAt(0)
                        upper_critical = critical_val.ElementAt(1)
                    End If
                End If



                If isBarcodeMode Then
                    strsql = "update jobdetail " & _
                             "set measurementvalue='" & value & "',resultstatus='" & resultStatus & "'" & _
                             ",resultabnormalflag='" & resultAbnormalFlag & "'" & _
                             ",datecomplete='" & datecomplete & "',referencerange='" & refrange & "'" & _
                             ",unit='" & unit & "',status='" & FINISHSAMPLE & "' " & _
                             ",lowernormal='" & lower_normal & "',uppernormal='" & upper_normal & "',lowercritical='" & lower_critical & "',uppercritical='" & upper_critical & "' " & _
                             "where barcode='" & barcode & "' and universaltest='" & testname & "' "
                    'salah
                ElseIf isSampleNoMode Then
                    strsql = "update jobdetail " & _
                             "set measurementvalue='" & value & "',resultstatus='" & resultStatus & "'" & _
                             ",resultabnormalflag='" & resultAbnormalFlag & "'" & _
                             ",datecomplete='" & datecomplete & "',referencerange='" & refrange & "'" & _
                             ",unit='" & unit & "',status='" & FINISHSAMPLE & "' " & _
                             ",lowernormal='" & lower_normal & "',uppernormal='" & upper_normal & "',lowercritical='" & lower_critical & "',uppercritical='" & upper_critical & "' " & _
                             " where sampleno='" & sampleNo & "' and universaltest='" & testname & "'"
                End If

                objconn.buildConn()
                objconn.ExecuteNonQuery(strsql)
                objconn.CloseConn()


            End If

            i = i + 1
        Loop
        UpdateJobStatus()

    End Sub

    Private Sub UpdateJobStatus() 'penting update status
        Dim strsql As String
        Dim pormat As String
        pormat = "yyyy-MM-dd HH:mm:ss"

        Dim datenow As Date
        datenow = Now

        strsql = "select job.labnumber from job where job.status='" & NEWSAMPLE & "' and job.labnumber not in (select distinct labnumber from jobdetail where status<>'" & FINISHSAMPLE & "' and active='" & ACTIVESAMPLE & "')"

        Dim ogreupdate As New clsGreConnect
        ogreupdate.buildConn()
        Dim cmdupdate As New SqlClient.SqlCommand(strsql, ogreupdate.grecon)
        Dim rdrupdate As SqlDataReader = cmdupdate.ExecuteReader(CommandBehavior.CloseConnection)
        If rdrupdate.HasRows Then
            Do While rdrupdate.Read
                'update job

                Dim squ As String
                squ = "update job set status='" & FINISHSAMPLE & "',datefinish='" & datenow.ToString(pormat) & "' where labnumber='" & rdrupdate("labnumber") & "'"
                Dim ogrejob As New clsGreConnect
                ogrejob.buildConn()
                ogrejob.ExecuteNonQuery(squ)
                ogrejob.CloseConn()
            Loop
        End If
        ogreupdate.CloseConn()
    End Sub

    Private Function detectComType(ByVal strdetect As String) As String
        Dim result As String
        If strdetect.Length > 2 Then
            Dim detectRecType As String()
            detectRecType = regex.Split(strdetect, "\|")
            Dim strfound As String

            strfound = detectRecType.ElementAt(0).Substring(detectRecType.ElementAt(0).Length - 1)
            If detectRecType.ElementAt(0).Substring(detectRecType.ElementAt(0).Length - 1) = "Q" Then
                'txtTransmit.Text = txtTransmit.Text & "Query message" & Chr(10)
                'McnIsQ = True
                'McnIsOR = False
                'qMcnToLis.Add([Text])
                result = "Q"
            ElseIf detectRecType.ElementAt(0).Substring(detectRecType.ElementAt(0).Length - 1) = "O" Then
                'if it is "O" message then 
                'McnIsQ = False
                'McnIsOR = True
                'orMcnToLis.Add([Text])
                result = "O"
            ElseIf detectRecType.ElementAt(0).Substring(detectRecType.ElementAt(0).Length - 1) = "R" Then
                'orMcnToLis.Add([Text])
                result = "R"
            ElseIf detectRecType.ElementAt(0).Substring(detectRecType.ElementAt(0).Length - 1) = "H" Then
                'orMcnToLis.Add([Text])
                result = "H"
            ElseIf detectRecType.ElementAt(0).Substring(detectRecType.ElementAt(0).Length - 1) = "L" Then
                'orMcnToLis.Add([Text])
                result = "L"
            Else
                'orMcnToLis.Add([Text])
                result = "U"
            End If
        End If

        Return result
    End Function

    Private Sub ConstructOMsg()
        Dim ENQstr As String
        Dim Hstr As String
        Dim Pstr As String

        Dim Lstr As String
        Dim EOTstr As String


        'construct message
        ENQstr = Chr(5)
        EOTstr = Chr(4)
        Hstr = Chr(2) & "1H|\^" & Chr(13) & Chr(3) & "E5" & Chr(13) & Chr(10)
        Pstr = Chr(2) & "2P|1|||||||||||||^" & Chr(13) & Chr(3) & "E9" & Chr(13) & Chr(10)


        Dim numQ As Integer
        Dim numO As Integer
        Dim i As Integer
        Dim j As Integer
        Dim FNforL As Integer
        Dim indexOfETX As Integer

        '  Dim labnumber As String
        Dim LstrFirst As String
        Dim LstrSecond As String

        Dim oList As New List(Of String)
        Dim qMcnToLisAfterSort As New List(Of String)

        'numQ = qMcnToLis.Count
        qMcnToLisAfterSort = qMcnToLis.Distinct.ToList
        numQ = qMcnToLisAfterSort.Count

        oLisToMcn.Add(ENQstr)

        For i = 0 To numQ - 1

            oLisToMcn.Add(Hstr)
            oLisToMcn.Add(Pstr)

            oList = getO(qMcnToLisAfterSort.Item(i)) 'bikin pesan
            'oLisToMcn.AddRange(getO(qMcnToLis.Item(i)))
            oLisToMcn.AddRange(oList)
            numO = oList.Count

            'j = 0
            'labnumber = 
            'For j = 0 To getTestForSample(labnumber) - 1
            '    oLisToMcn.Add(Ostr)
            'Next
            FNforL = 3 + numO
            LstrFirst = Chr(2) & CStr(FNforL) & "L|1|" & "N" & Chr(13) & Chr(3)
            indexOfETX = LstrFirst.IndexOf(Chr(3))
            LstrSecond = checkSumCalculator(Mid(LstrFirst, 2, indexOfETX)) & Chr(13) & Chr(10)
            Lstr = LstrFirst & LstrSecond

            oLisToMcn.Add(Lstr)

        Next
        oLisToMcn.Add(EOTstr)

        '  curMsgState = 0
        '  serialPort1.Write(Chr(5)) 'ENQ    mengirim enq untuk pertama kali
        'If isENQ = False Then
        '    serialPort1.Write(Chr(5)) 'ENQ 
        '    isENQ = True
        'End If
    End Sub
    Private Function getO(ByVal strQ As String) As List(Of String)
        'connect to database, find testTypeName i.e 3 name
        'construct message
        ' there are 3 i.e AST, TP, ALB, count 3
        '
        Dim result As New List(Of String)
        result.Clear()

        Dim i As Integer
        Dim numRec As Integer
        Dim str As String


        Dim FN As Integer
        Dim seq As Integer
        'Dim requestdate As String
        'Dim specimentDesc As String
        'Dim checkNum As String
        'Dim reportType As String
        Dim firstStr As String
        Dim secondStr As String
        Dim indexOfSTX As Integer


        Dim objGDM As ClassgetDetailQMessage
        objGDM = New ClassgetDetailQMessage(strQ, fInstrument)
        objGDM.DetectSample()

        numRec = objGDM.testTypeForSingleLabNumber.Count
        'numRec = 3 'misal

        'test

        'test--
        For i = 0 To numRec - 1
            FN = i + 3
            seq = i + 1

            ' requestdate = "20141012093022" 'misal
            'specimentDesc = "1" 'misal

            firstStr = Chr(2) & CStr(FN) & "O|" & seq & "|" & objGDM.strSample & "||" & "^^^" & objGDM.testTypeForSingleLabNumber.Item(i) & "|" & objGDM.priority(i) & "|" & objGDM.requestDate(i) & "|||||||||" & objGDM.specimenDesc(i) & "||||||||||" & objGDM.reportType(i) & Chr(13) & Chr(3) '& checkNum & Chr(13) & Chr(10)
            indexOfSTX = firstStr.IndexOf(Chr(3))
            secondStr = checkSumCalculator(Mid(firstStr, 2, indexOfSTX)) & Chr(13) & Chr(10)
            str = firstStr & secondStr
            result.Add(str)
        Next

        objGDM = Nothing
        Return result

    End Function

    Private Function checkSumCalculator(ByVal str As String) As String
        Dim result As String
        Dim i As Integer
        Dim n As Integer
        Dim m As Integer

        n = 0
        For i = 1 To str.Length
            n = n + Asc(Mid(str, i, 1))
        Next
        m = n Mod 256
        result = Conversion.Hex(m)
        If result.Length = 1 And (IsNumeric(result) = True) Then
            result = "0" & result
        End If
        Return result
    End Function

    Private Sub ClearALL()

        McnIsQ = False
        McnIsOR = False
        'qMcnToLis.Clear()
        'oLisToMcn.Clear()
        'orMcnToLis.Clear()
        qMcnToLis = New List(Of String)
        oLisToMcn = New List(Of String)
        orMcnToLis = New List(Of String)
        ' SerialPort1.DiscardOutBuffer() 'clear buffer
    End Sub
    '-----------------------------------------------------------------------
    '------------------------------------------------------
    Private Sub LoadInstrumentName()
        Dim strins As String
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        strins = "select distinct name from instrument"
        Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            cbInstrument.Items.Add(rdr("name"))
        Loop
        cbInstrument.SelectedIndex = 0

    End Sub

    Private Sub ShowInstrumentProperty()
        Dim strins As String
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        txtport.Enabled = True
        txtbaud.Enabled = True
        txtparity.Enabled = True
        txtstop.Enabled = True
        txtdata.Enabled = True

        strins = "select * from instrumentconnect where name='" & Trim(cbInstrument.Text) & "'"
        Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("com")) Then
                    txtport.Text = rdr("com")
                End If
                If Not IsDBNull(rdr("baud")) Then
                    txtbaud.Text = rdr("baud")
                End If
                If Not IsDBNull(rdr("parity")) Then
                    txtparity.Text = rdr("parity")
                End If
                If Not IsDBNull(rdr("stop")) Then
                    txtstop.Text = rdr("stop")
                End If
                If Not IsDBNull(rdr("data")) Then
                    txtdata.Text = rdr("data")
                End If
            Loop
        End If
        txtport.Enabled = False
        txtbaud.Enabled = False
        txtparity.Enabled = False
        txtstop.Enabled = False
        txtdata.Enabled = False
    End Sub
    Private Sub cbInstrument_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbInstrument.SelectedIndexChanged
        ShowInstrumentProperty()
    End Sub

    Public Sub WriteInsProperty()

        'Dim i As Integer
        'Dim aryText(4) As String
        Dim insName As String



        'aryText(0) = "Port=" & fportName
        'aryText(1) = "Baud=" & fbaud
        'aryText(2) = "Parity=" & fparity
        'aryText(3) = "Stop=" & fstop
        'aryText(4) = "Data=" & fdata
        'aryText(5) = "Instrument=" & fInstrument
        insName = "Instrument=" & cbInstrument.Text

        Dim objWriter As New System.IO.StreamWriter(fpath)
        objWriter.WriteLine(insName)

        'For i = 0 To 3
        '    objWriter.WriteLine(aryText(i))
        'Next
        objWriter.Close()
    End Sub

    Private Function ReadInsProperty() As Boolean

        Dim InsName As String

        Dim result As Boolean


        Dim getstr As String
        Dim position As Integer


        If System.IO.File.Exists(fpath) Then
            Try

                Dim lines() As String = IO.File.ReadAllLines(fpath)
                Dim k As Integer
                k = 0
                For Each line As String In lines

                    position = InStr(line, "=")
                    For i = position To Len(line) - 1
                        getstr = getstr + line.Substring(i, 1)
                    Next


                    InsName = getstr
                    getstr = ""
                Next


                'fportName = arrSetting(0)
                'fbaud = arrSetting(1)
                'fparity = arrSetting(2)
                'fstop = arrSetting(3)
                'fdata = arrSetting(4)
                fInstrument = InsName

                result = True
            Catch ex As Exception
                MessageBox.Show("Seeting file tidak ada", "File not found", MessageBoxButtons.OK)


            End Try
        Else
            result = False
        End If

        Return result

    End Function




    'class to handle Q message
    Private Class ClassgetDetailQMessage
        Dim strMsg As String

        Public strSample As String
        Public isBarcodeMode As Boolean
        Public isSampleIdMode As Boolean

        Public testTypeForSingleLabNumber As New List(Of String)
        Public priority As New List(Of String)
        Public requestDate As New List(Of String)
        Public specimenDesc As New List(Of String)
        Public reportType As New List(Of String)

        Public sampleNoBeingRead As List(Of KeyValuePair(Of String, String))
        Public barcodeBeingRead As List(Of KeyValuePair(Of String, String))

        Dim theInstrumentname As String
        ' Dim da As Npgsql.NpgSqlClient.SqlDataAdapter
        Dim id As String

        Public Sub New(ByVal str As String, ByVal iname As String)
            strMsg = str
            theInstrumentname = iname
        End Sub

        Public Sub DetectSample()
            Dim i As Integer
            Dim msg As String
            Dim regex As Regex
            Dim str As String

            isBarcodeMode = False
            isSampleIdMode = False

            ' strMsg = Chr(2) & "2Q|1|^130^5^45^N||ALL|||||||O" & Chr(13) & Chr(3) & "B4" & Chr(13) & Chr(10)

            i = strMsg.IndexOf(Chr(13))
            msg = Mid(strMsg, 2, i - 1)


            Dim result As String()
            result = regex.Split(msg, "\|")
            Dim typeofreport As String
            ' Label2.Text = result.Count
            typeofreport = result.ElementAt(12)

            Dim dictQueryMessage As New Dictionary(Of String, String)
            Dim dictSampleDetail As New Dictionary(Of String, String)


            Dim strSampleDetail As String
            Dim tubeDetail As String()

            strSampleDetail = result.ElementAt(2)
            strSample = strSampleDetail

            tubeDetail = regex.Split(strSampleDetail, "\^")

            If Mid(strSampleDetail, 1, 1) = "^" Then 'sample No mode
                dictSampleDetail.Add("sampleNumber", tubeDetail.ElementAt(1))
                dictSampleDetail.Add("diskNumber", tubeDetail.ElementAt(2))
                dictSampleDetail.Add("positionNumber", tubeDetail.ElementAt(3))
                dictSampleDetail.Add("diluent", tubeDetail.ElementAt(4))
                isSampleIdMode = True
            Else
                dictSampleDetail.Add("sampleIDbarcode", tubeDetail.ElementAt(0))
                dictSampleDetail.Add("diskNumber", tubeDetail.ElementAt(2))
                dictSampleDetail.Add("positionNumber", tubeDetail.ElementAt(3))
                dictSampleDetail.Add("diluent", tubeDetail.ElementAt(4))
                isBarcodeMode = True

            End If

            'typeofreport = 
            'dictQueryMessage.Add("rectype", Mid(result.ElementAt(0), 2, 1))
            'dictQueryMessage.Add("seqNumber", result.ElementAt(1))
            'dictQueryMessage.Add("sampIdentity", result.ElementAt(2))
            'dictQueryMessage.Add("universalTestId", result.ElementAt(4))
            'dictQueryMessage.Add("ReqInfoStatusCode", result.ElementAt(11))


            Dim greObject As New clsGreConnect
            Dim strsql As String
            Dim strbarcode As String
            Dim strsampleno As String

            If isBarcodeMode Then
                strbarcode = dictSampleDetail.Item("sampleIDbarcode")
                '--orinya - strsql = "select jobdetail.id,jobdetail.idtesttype,jobdetail.universaltest,testtype.testname as testname,jobdetail.priority,jobdetail.dateorder,jobdetail.specimendesc from jobdetail,testtype where jobdetail.idtesttype=testtype.id and jobdetail.barcode='" & strbarcode & "' and jobdetail.status='" & ALREADYSAMPLING & "'"
                'and jobdetail.idtesttype in (select idtest from instrument where instrument name='" & currentInstrument & "') 
                strsql = "select jobdetail.id,jobdetail.idtesttype,jobdetail.universaltest,jobdetail.priority,jobdetail.dateorder,jobdetail.specimendesc from jobdetail,instrument where jobdetail.barcode='" & strbarcode & "' and jobdetail.status='" & ALREADYSAMPLING & "' and jobdetail.idtesttype=instrument.idtest and instrument.name='" & theInstrumentname & "'"
            ElseIf isSampleIdMode Then
                strsampleno = dictSampleDetail.Item("sampleNumber")
                ' -orinya - strsql = "select jobdetail.id,jobdetail.idtesttype,jobdetail.universaltest,testtype.testname as testname,jobdetail.priority,jobdetail.dateorder,jobdetail.specimendesc from jobdetail,testtype where jobdetail.idtesttype=testtype.id and jobdetail.sampleno='" & strsampleno & "' and jobdetail.status='" & ALREADYSAMPLING & "'"
                'and jobdetail.idtesttype in (select idtest from instrument where instrument name='" & currentInstrument & "') 
                strsql = "select jobdetail.id,jobdetail.idtesttype,jobdetail.universaltest,jobdetail.priority,jobdetail.dateorder,jobdetail.specimendesc from jobdetail,instrument where jobdetail.sampleno='" & strsampleno & "' and jobdetail.status='" & ALREADYSAMPLING & "' and jobdetail.idtesttype=instrument.idtest and instrument.name='" & theInstrumentname & "'"
            End If




            greObject.buildConn()

            Dim thecommand As New SqlClient.SqlCommand(strsql, greObject.grecon)
            Dim labNumberReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)
            Dim idintjobdetail As Integer
            '--for certain sampleno
            '--there should be status for each testname

            Do While labNumberReader.Read
                testTypeForSingleLabNumber.Add(labNumberReader("universaltest"))
                priority.Add(labNumberReader("priority"))
                requestDate.Add(labNumberReader("dateorder"))
                specimenDesc.Add(labNumberReader("specimendesc"))

                idintjobdetail = labNumberReader("id")

                reportType.Add(typeofreport)

                If isBarcodeMode = True Then

                    ' barcodeBeingRead.Add(New KeyValuePair(Of String, String)(strbarcode, labNumberReader("universaltest")))
                    TagJobDetailBeingAnalyze(idintjobdetail)
                ElseIf isSampleIdMode = True Then
                    'sampleNoBeingRead.Add(New KeyValuePair(Of String, String)(strsampleno, labNumberReader("universaltest")))
                    TagJobDetailBeingAnalyze(idintjobdetail)
                End If

            Loop
            greObject.CloseConn()
            '  cbTestGroup.Enabled = False
            'Chr(2)-> [STX]
            'Chr(3)-> [ETX]
            'Chr(13)->[CR]
            'Chr(10)->[LF]
            'chr(4)

            '-----------db operation to find test type
            '------------
        End Sub
        Private Sub TagJobDetailBeingAnalyze(ByVal idjobdetail As Integer)
            Dim strtag As String
            Dim updateconn As New clsGreConnect
            updateconn.buildConn()
            strtag = "update jobdetail set status='" & BEINGANALYZE & "' where id='" & idjobdetail & "'" 'status 8 = sedang analisa 
            updateconn.ExecuteNonQuery(strtag)
            updateconn.CloseConn()
        End Sub

    End Class

    
    Private Sub cmdSaveIns_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSaveIns.Click
        WriteInsProperty()
        cbInstrument.Enabled = False
        cmdSaveIns.Enabled = False
        cmdEdit.Enabled = True
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        cbInstrument.Enabled = True
        cmdSaveIns.Enabled = True
        cmdEdit.Enabled = False
    End Sub

    Private Sub lblsend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblsend.Click

    End Sub

    Private Sub tmrCheck_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub txtbaud_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbaud.TextChanged

    End Sub
End Class
