﻿Public Class rptListSuccessTest
    Dim start_to_pass As String
    Dim end_to_pass As String
    Dim goodstrsql As String


    Private Sub rptListSuccessTest_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        ShowReport()
    End Sub
    Private Sub ShowReport()
        Try

            Dim rdlView As New fyiReporting.RdlViewer.RdlViewer
            Dim reportStrip As New fyiReporting.RdlViewer.ViewerToolstrip
            rdlView.ShowParameterPanel = False

            reportStrip.Viewer = rdlView
            'reportStrip.Items(0).Visible = False
            'reportStrip.Items(1).Visible = True
            'reportStrip.Items(2).Visible = False
            reportStrip.Visible = True




            ' ln = labnum
            Dim filepath As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "v3rptDaftarTestjob.rdl")
            rdlView.SourceFile = New Uri(filepath)
            rdlView.Parameters = String.Format("AppLabName={0}&AppAddress1={1}&AppAddress2={2}&AppLabPhone={3}&AppLabFax={4}&AppDateStart={5}&AppDateEnd={6}&cn={7}", GLabName, GLabAddress1, GLabAddress2, GLabPhone, GLabFax, start_to_pass, end_to_pass, strdbconn)
            rdlView.Report.DataSets("Data").SetSource(goodstrsql)


            '  rdlView.HideRunButton()
            rdlView.Rebuild()


            reportStrip.Top = 100
            reportStrip.Items(0).Visible = False
            reportStrip.Items(1).Visible = False
            reportStrip.Items(7).Visible = False

            reportStrip.Dock = DockStyle.Top

            rdlView.Dock = DockStyle.Fill

            Me.Controls.Add(reportStrip)
            Me.Controls.Add(rdlView)

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Public Sub New(ByVal datestart As String, ByVal dateend As String, ByVal sqlgood As String)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        start_to_pass = datestart
        end_to_pass = dateend
        goodstrsql = sqlgood
        ' Add any initialization after the InitializeComponent() call.

    End Sub
  

  
    Private Sub rptListSuccessTest_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class