﻿Imports System.Data.SqlClient
Public Class ListNotPaidFrm

    Private Sub ListNotPaidFrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        isPembayaranWinOpen = 0
    End Sub

    Private Sub ListNotPaidFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        isPembayaranWinOpen = 1
        Icon = New System.Drawing.Icon("medlis2.ico")
        LoadNotYetPaid()

    End Sub

    Public Sub LoadNotYetPaid()
        dgvnotpaid.AllowUserToAddRows = False
        dgvnotpaid.RowHeadersVisible = False
        dgvnotpaid.DataSource = Nothing

        If dgvnotpaid.Columns.Contains("Bayar") Then
            dgvnotpaid.Columns.Clear()
        End If


        Dim ogre As New clsGreConnect
        ogre.buildConn()

        Dim strsql As String
        strsql = "select job.labnumber,patient.patientname,patient.address from job,patient where job.paymentstatus='" & NOTPAID & "' and job.idpasien=patient.id order by job.datereceived desc"

        Dim dtable As New DataTable
        dtable = ogre.ExecuteQuery(strsql)
        dgvnotpaid.DataSource = dtable
        Dim j As Integer
        For j = 0 To dgvnotpaid.Columns.Count - 1
            dgvnotpaid.Columns(j).Visible = False
        Next

        dgvnotpaid.Columns("labnumber").Visible = True
        dgvnotpaid.Columns("labnumber").HeaderText = "Labnumber"
        dgvnotpaid.Columns("labnumber").Width = 100
        dgvnotpaid.Columns("labnumber").ReadOnly = True

        dgvnotpaid.Columns("patientname").Visible = True
        dgvnotpaid.Columns("patientname").HeaderText = "Nama"
        dgvnotpaid.Columns("patientname").Width = 150
        dgvnotpaid.Columns("patientname").ReadOnly = True

        dgvnotpaid.Columns("address").Visible = True
        dgvnotpaid.Columns("address").HeaderText = "Alamat"
        dgvnotpaid.Columns("address").Width = 250
        dgvnotpaid.Columns("patientname").ReadOnly = True


        Dim buttonColumn As New DataGridViewButtonColumn()
        With buttonColumn
            .Name = "Bayar"
            .HeaderText = "Bayar"
            .Width = 90
            .Text = "Bayar"
            .UseColumnTextForButtonValue = True
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            .FlatStyle = FlatStyle.Standard
            .CellTemplate.Style.BackColor = Color.Yellow
            .DisplayIndex = 0
        End With
        dgvnotpaid.Columns.Add(buttonColumn)
        ogre.CloseConn()
    End Sub

    Private Sub dgvnotpaid_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvnotpaid.CellContentClick
        Dim selectedLabNumber As String
        If e.RowIndex < 0 OrElse Not e.ColumnIndex = dgvnotpaid.Columns("Bayar").Index Then

            Return

        Else

            If Not dgvnotpaid.CurrentRow.IsNewRow Then
                If Not IsDBNull(dgvnotpaid.Item("labnumber", dgvnotpaid.CurrentRow.Index).Value) Then
                    selectedLabNumber = dgvnotpaid.Item("labnumber", dgvnotpaid.CurrentRow.Index).Value
                    If isPaymentWinOpen = 0 Then

                        Dim opayment As Payment
                        If opayment Is Nothing Then
                            opayment = New Payment(selectedLabNumber)
                            opayment.mycaller = Me
                            opayment.MdiParent = MainFrm
                            opayment.StartPosition = FormStartPosition.Manual
                            opayment.Left = 0
                            opayment.Top = 0
                            opayment.Show()
                        End If
                    Else
                        MessageBox.Show("Halaman pembayaran telah terbuka")
                    End If

                End If
            End If

        End If
    End Sub
End Class