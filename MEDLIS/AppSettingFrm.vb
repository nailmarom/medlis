﻿Public Class AppSettingFrm
    Dim fpath As String
    Dim fInstrument As String

    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click
        WriteInsProperty()
    End Sub

    Public Sub WriteInsProperty()
        Dim insName As String

        If RdoBarcodeMode.Checked = True Then
            insName = "Mode=barcodeMode"
        ElseIf rdoSampleMode.Checked Then
            insName = "Mode=samplenoMode"
        End If

        Dim objWriter As New System.IO.StreamWriter(fpath)
        objWriter.WriteLine(insName)
        objWriter.Close()
    End Sub
    'tes
    Private Sub AppSettingFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
        fpath = System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData, "modesample.txt")
        If ReadInsProperty() = False Then
            MessageBox.Show("Please choose instrument read mode")
        Else

            If fInstrument = "barcodeMode" Then
                RdoBarcodeMode.Checked = True
                rdoSampleMode.Checked = False
            ElseIf fInstrument = "samplenoMode" Then
                RdoBarcodeMode.Checked = False
                rdoSampleMode.Checked = True

            End If

        End If
    End Sub

    Private Function ReadInsProperty() As Boolean

        Dim InsName As String
        Dim result As Boolean
        Dim getstr As String
        Dim position As Integer
        If System.IO.File.Exists(fpath) Then
            Try
                Dim lines() As String = IO.File.ReadAllLines(fpath)
                Dim k As Integer
                k = 0
                For Each line As String In lines

                    position = InStr(line, "=")
                    For i = position To Len(line) - 1
                        getstr = getstr + line.Substring(i, 1)
                    Next
                    InsName = getstr
                    getstr = ""
                Next

                fInstrument = InsName
                result = True
            Catch ex As Exception
                MessageBox.Show("Please specify instrument read mode", "File not found", MessageBoxButtons.OK)
            End Try
        Else
            result = False
        End If
        Return result

    End Function

    Private Sub cancelMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelMnu.Click
        Me.Close()
    End Sub
End Class