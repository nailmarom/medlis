﻿Imports System.Data.SqlClient

Public Class HemaConnectProperty
    Dim myPort As Array
    Dim isEdit As Boolean
    Dim isNew As Boolean
    Dim isACK As Boolean
    Dim number_of_tick As Integer
    Private Sub HemaConnectProperty_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FirstLoad()
    End Sub
    Private Sub FirstLoad()
        Dim ctrl As Control
        For Each ctrl In Panel1.Controls
            If (ctrl.GetType() Is GetType(ComboBox)) Then
                Dim cb As ComboBox = CType(ctrl, ComboBox)
                cb.Enabled = True
                cb.Items.Clear()
            End If
        Next

        CekLicense()
        SaveMnu.Enabled = False
        LoadInstrumentName()
        LoadStandardPortProperty()
        ShowInstrumentProperty()

    End Sub


    Private Sub LoadInstrumentName()
        Dim strins As String
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        strins = "select distinct name from hemaconnect"
        Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                cbInstrument.Items.Add(rdr("name"))
            Loop
            cbInstrument.SelectedIndex = 0

        End If
    End Sub
    Private Sub LoadStandardPortProperty()

        Dim i As Integer
        myPort = IO.Ports.SerialPort.GetPortNames()
        For i = 0 To UBound(myPort)
            cbPort.Items.Add(myPort(i))
        Next

        cbBaudRate.Items.Add(9600)
        cbBaudRate.Items.Add(19200)
        cbBaudRate.Items.Add(38400)
        cbBaudRate.Items.Add(57600)
        cbBaudRate.Items.Add(115200)

        cbParity.Items.Add("None")
        cbParity.Items.Add("Even")
        cbParity.Items.Add("Mark")
        cbParity.Items.Add("Odd")

        cbStopBit.Items.Add("None")
        cbStopBit.Items.Add("One")
        cbStopBit.Items.Add("Two")

        cbDataBit.Items.Add(5)
        cbDataBit.Items.Add(6)
        cbDataBit.Items.Add(7)
        cbDataBit.Items.Add(8)
        cbDataBit.Items.Add(9)

        cbPort.Enabled = False
        cbBaudRate.Enabled = False
        cbParity.Enabled = False
        cbStopBit.Enabled = False
        cbDataBit.Enabled = False
    End Sub

    Private Sub cbInstrument_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbInstrument.SelectedIndexChanged
        Dim strins As String
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        cbPort.Enabled = True
        cbBaudRate.Enabled = True
        cbParity.Enabled = True
        cbStopBit.Enabled = True
        cbDataBit.Enabled = True

        strins = "select * from hemaconnect where name='" & Trim(cbInstrument.Text) & "'"
        Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("com")) Then
                    cbPort.SelectedIndex = cbPort.FindStringExact(rdr("com"))
                End If
                If Not IsDBNull(rdr("baud")) Then
                    cbBaudRate.SelectedIndex = cbBaudRate.FindStringExact(rdr("baud"))
                End If
                If Not IsDBNull(rdr("parity")) Then
                    cbParity.SelectedIndex = cbParity.FindStringExact(rdr("parity"))
                End If
                If Not IsDBNull(rdr("stop")) Then
                    cbStopBit.SelectedIndex = cbStopBit.FindStringExact(rdr("stop"))
                End If
                If Not IsDBNull(rdr("data")) Then
                    cbDataBit.SelectedIndex = cbDataBit.FindStringExact(rdr("data"))
                End If
            Loop
        End If
        cbPort.Enabled = False
        cbBaudRate.Enabled = False
        cbParity.Enabled = False
        cbStopBit.Enabled = False
        cbDataBit.Enabled = False
    End Sub
    Private Sub ShowInstrumentProperty()
        Dim strins As String
        Dim ogre As New clsGreConnect
        ogre.buildConn()

        cbPort.Enabled = True
        cbBaudRate.Enabled = True
        cbParity.Enabled = True
        cbStopBit.Enabled = True
        cbDataBit.Enabled = True

        strins = "select * from hemaconnect where name='" & Trim(cbInstrument.Text) & "'"
        Dim cmd As New SqlClient.SqlCommand(strins, ogre.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                If Not IsDBNull(rdr("com")) Then
                    cbPort.SelectedIndex = cbPort.FindStringExact(rdr("com"))
                End If
                If Not IsDBNull(rdr("baud")) Then
                    cbBaudRate.SelectedIndex = cbBaudRate.FindStringExact(rdr("baud"))
                End If
                If Not IsDBNull(rdr("parity")) Then
                    cbParity.SelectedIndex = cbParity.FindStringExact(rdr("parity"))
                End If
                If Not IsDBNull(rdr("stop")) Then
                    cbStopBit.SelectedIndex = cbStopBit.FindStringExact(rdr("stop"))
                End If
                If Not IsDBNull(rdr("data")) Then
                    cbDataBit.SelectedIndex = cbDataBit.FindStringExact(rdr("data"))
                End If
            Loop
        End If
        cbPort.Enabled = False
        cbBaudRate.Enabled = False
        cbParity.Enabled = False
        cbStopBit.Enabled = False
        cbDataBit.Enabled = False
    End Sub
    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click
        Dim strsql As String
        If isEdit = True Then
            strsql = "update hemaconnect set com='" & cbPort.Text & "', baud='" & cbBaudRate.Text & "'," & _
            "parity='" & cbParity.Text & "',stop='" & cbStopBit.Text & "',data='" & cbDataBit.Text & "' where name='" & Trim(cbInstrument.Text) & "'"
        ElseIf isNew = True Then
            strsql = "insert into hemaconnect(name,com,baud,parity,stop,data) values('" & Trim(cbInstrument.Text) & "','" & cbPort.Text & "','" & cbBaudRate.Text & "','" & cbParity.Text & "','" & cbStopBit.Text & "','" & cbDataBit.Text & "')"
        End If


        Dim ogre As New clsGreConnect
        ogre.buildConn()
        ogre.ExecuteNonQuery(strsql)
        ogre.CloseConn()

        isNew = False
        isEdit = False
        FirstLoad()

        cbInstrument.DropDownStyle = ComboBoxStyle.DropDownList

        EditMnu.Enabled = True
        SaveMnu.Enabled = False
        AddMnu.Enabled = False
    End Sub

    Private Sub EditMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditMnu.Click
        isEdit = True
        EditMnu.Enabled = False
        SaveMnu.Enabled = True
        AddMnu.Enabled = False
        cbPort.Enabled = True
        cbBaudRate.Enabled = True
        cbParity.Enabled = True
        cbStopBit.Enabled = True
        cbDataBit.Enabled = True
    End Sub

    Private Sub cancelMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelMnu.Click
        EditMnu.Enabled = True
        SaveMnu.Enabled = False
        AddMnu.Enabled = True
    End Sub

    Private Sub btntest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btntest.Click
        If cbPort.Text <> "" Then
            SerialPort1.PortName = Trim(cbPort.Text)
        Else
            MessageBox.Show("Silahkan pilih nama port")
            Return
        End If

        If cbBaudRate.Text <> "" Then
            SerialPort1.BaudRate = Trim(cbBaudRate.Text)
        Else
            MessageBox.Show("Silahkan pilih baudrate")
        End If

        If cbParity.Text <> "" Then
            Dim par As String
            par = Trim(cbParity.Text)
            If par = "None" Then
                SerialPort1.Parity = IO.Ports.Parity.None
            ElseIf par = "Even" Then
                SerialPort1.Parity = IO.Ports.Parity.Even
            ElseIf par = "Mark" Then
                SerialPort1.Parity = IO.Ports.Parity.Mark
            ElseIf par = "Odd" Then
                SerialPort1.Parity = IO.Ports.Parity.Odd
            End If
        Else
            MessageBox.Show("Silahkan pilih parity")
            Return
        End If

        If cbStopBit.Text <> "" Then
            Dim stp As String
            stp = cbStopBit.Text
            If stp = "None" Then
                SerialPort1.StopBits = IO.Ports.StopBits.None
            ElseIf stp = "One" Then
                SerialPort1.StopBits = IO.Ports.StopBits.One
            ElseIf stp = "Two" Then
                SerialPort1.StopBits = IO.Ports.StopBits.Two
            End If
        Else
            MessageBox.Show("Silahkan pilih stopbit")
            Return
        End If

        If cbDataBit.Text <> "" Then
            Dim datab As String
            datab = cbDataBit.Text
            SerialPort1.DataBits = CInt(datab)
        Else
            MessageBox.Show("Silahkan pilih databit")
            Return

        End If

        '--------------

        '***************
        'SerialPort1.PortName = Trim(cmbPort.Text) 'Set SerialPort1 to the selected COM port at startup
        'SerialPort1.BaudRate = Trim(cmbBaud.Text) 'Set Baud rate to the selected value on 

        ''Other Serial Port Property
        'SerialPort1.Parity = IO.Ports.Parity.None
        'SerialPort1.StopBits = IO.Ports.StopBits.One
        'SerialPort1.DataBits = 8 'Open our serial port 
        '**************
        isACK = True
        number_of_tick = 0
        If SerialPort1.IsOpen Then SerialPort1.Close()

        SerialPort1.Open()
        SerialPort1.DiscardInBuffer()
        SerialPort1.DiscardOutBuffer()

        btntest.Enabled = False 'Disable Connect button
        tmrreceive.Start()
        SerialPort1.Write(Chr(5))
    End Sub



    Private Sub tmrreceive_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrreceive.Tick
        Dim newReceivedData As String
        If number_of_tick < 12 Then
            number_of_tick = number_of_tick + 1
            newReceivedData = SerialPort1.ReadExisting
            If newReceivedData = Chr(6) Then
                lbltest.Text = "Connected. The configuration is correct"
                SerialPort1.Write(Chr(4))
                tmrreceive.Stop()
                If SerialPort1.IsOpen Then SerialPort1.Close()
                btntest.Enabled = True
            End If
        Else
            tmrreceive.Stop()
            lbltest.Text = "Not connected"
            MessageBox.Show("Plese reconfigure the connection from PC to Intrument")
            If SerialPort1.IsOpen Then SerialPort1.Close()
            btntest.Enabled = True
        End If
    End Sub

    Private Sub AddMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddMnu.Click
        isNew = True

        Dim ctrl As Control
        For Each ctrl In Panel1.Controls
            If (ctrl.GetType() Is GetType(ComboBox)) Then
                Dim cb As ComboBox = CType(ctrl, ComboBox)
                cb.Enabled = True
                'cb.DropDownStyle = ComboBoxStyle.DropDown
                'cb.Text = "" 'BackColor = Color.LightYellow
            End If
        Next
        cbInstrument.DropDownStyle = ComboBoxStyle.DropDown
        SaveMnu.Enabled = True
        EditMnu.Enabled = False
        AddMnu.Enabled = False

    End Sub

End Class