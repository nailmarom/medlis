﻿Imports System.Data.SqlClient

Public Class UpdateDBDataFrm

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click


        Try
            newdbname = txtdb.Text
            newdbuser = txtuser.Text
            newdbpass = txtpass.Text


            If newdbname = Global_dbName Then
                MessageBox.Show("Anda harus memasukkan database yang benar. DBname, seharusnya berisi nama structure database yang baru yg hendak dicopy", "peringatan", MessageBoxButtons.OK)
                Exit Sub
            End If

            strNewDB = "Driver={PostgreSQL ANSI};database=" & newdbname & ";server=" & Global_serverName & ";port=5432;uid=" & newdbuser & ";sslmode=disable;readonly=0;protocol=7.4;User ID=" & newdbuser & ";password=" & newdbpass & ";"
            Dim newdb As New clsNewGreConnect
            Try
                If newdb.TestConnection = True Then
                    Label4.Text = "Hore. bisa tersambung"
                    cmdvs.Enabled = True
                Else
                    Label4.Text = "cek dbname, password and user"
                End If
            Catch ex As Exception
                MessageBox.Show("Masukkan username database dan password yang benar, dan pastikan Anda memasukkan nama database dari referensi baru", "koneksi gagal")
            End Try



        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Silahkan hub support contact")
        End Try

    End Sub

    Dim iforpg As Integer
    Private Delegate Sub DgUpdatePg(ByVal i As Integer)
    Private Sub UpdatePgBar(ByVal i As Integer)
        If ProgressBar1.InvokeRequired Then
            Me.Invoke(New DgUpdatePg(AddressOf UpdatePgBar), i)
        Else
            ProgressBar1.Value = i
        End If
    End Sub
    Private Sub Upg()
        If iforpg > 299 Then iforpg = 0
        iforpg = iforpg + 2
        UpdatePgBar(iforpg)
    End Sub

    Dim kasil As Boolean

    Private Sub cmdvs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdvs.Click
        Try
            kasil = False
            Button1.Enabled = False
            iforpg = 0
            Stage1()
            ProgressBar1.Value = 300
            kasil = True

        Catch ex As Exception
            kasil = False
            MessageBox.Show(ex.ToString)
        End Try
        If kasil = True Then
            lblprogress.Text = "selesai - berhasil"
            Button1.Enabled = True
        Else
            lblprogress.Text = "selesai - gagal"
            Button1.Enabled = True
        End If
    End Sub
    Private Sub Stage1()
        Me.Cursor = Cursors.WaitCursor
      
        Dim olddb_count As Integer
        Dim newdb_count As Integer
        Dim strsql As String
        Upg()
        strsql = "SELECT count(table_name) as jml FROM information_schema.tables WHERE table_schema='public' AND table_type='BASE TABLE'"
        'old -- first
        Dim greobject As New clsGreConnect
        greobject.buildConn()
        Dim cmd As New SqlClient.SqlCommand(strsql, greobject.grecon)
        Dim Reader As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

        Do While Reader.Read
            Upg()

            olddb_count = Reader("jml")
        Loop

        Dim newgreobject As New clsNewGreConnect
        newgreobject.buildConn()
        Dim newcmd As New SqlClient.SqlCommand(strsql, newgreobject.grecon)
        Dim newReader As SqlDataReader = newcmd.ExecuteReader(CommandBehavior.CloseConnection)

        Do While newReader.Read
            Upg()

            newdb_count = newReader("jml")
        Loop
        lblcount.Text = "Jumlah table db lama :" & olddb_count & Chr(13) & "Dan jumlah table db referensi " & txtdb.Text & " adalah :" & newdb_count
        '--------------------------
        Reader.Close()
        newReader.Close()
        newcmd.Dispose()
        cmd.Dispose()
        greobject.CloseConn()
        newgreobject.CloseConn()

        '=================
        'newtable
        Dim newtb_list As New List(Of String)
        strsql = "SELECT table_name FROM information_schema.tables WHERE table_schema='public' AND table_type='BASE TABLE'"
        Dim seenewgo As New clsNewGreConnect
        seenewgo.buildConn()
        Dim seenewcmd As New SqlClient.SqlCommand(strsql, seenewgo.grecon)
        Dim seenewReader As SqlDataReader = seenewcmd.ExecuteReader(CommandBehavior.CloseConnection)

        Do While seenewReader.Read
            Upg()
            newtb_list.Add(seenewReader("table_name"))
        Loop

        Dim oldtb_list As New List(Of String)
        strsql = "SELECT table_name FROM information_schema.tables WHERE table_schema='public' AND table_type='BASE TABLE'"
        Dim seeoldgo As New clsGreConnect
        seeoldgo.buildConn()
        Dim seeoldcmd As New SqlClient.SqlCommand(strsql, seeoldgo.grecon)
        Dim seeoldReader As SqlDataReader = seeoldcmd.ExecuteReader(CommandBehavior.CloseConnection)

        Do While seeoldReader.Read
            Upg()
            oldtb_list.Add(seeoldReader("table_name"))
        Loop

        Dim i As Integer
        i = 0
        Dim j As Integer
        j = 0

        Dim temp_str_old_table As String
        Dim temp_str_new_table As String

        Dim strcol_old As String
        Dim strcol_new As String

        For i = 0 To oldtb_list.Count - 1
            Upg()


            temp_str_old_table = oldtb_list.Item(i)

            For j = 0 To newtb_list.Count - 1
                Upg()

                If temp_str_old_table = newtb_list.Item(j) Then
                    'kalo nama table sama maka jumlahnya column di cek dan disamakan
                    temp_str_new_table = newtb_list.Item(j)
                    strcol_new = "SELECT * FROM information_schema.columns WHERE table_schema = 'public' AND table_name='" & temp_str_new_table & "'"
                    CheckColumnAndUpdate(strcol_new, temp_str_new_table)



                End If
            Next
        Next

        Dim listdif As New List(Of String)
        listdif = newtb_list.Except(oldtb_list).ToList

        'MessageBox.Show(listdif.Count & " " & listdif.Item(0))
        'if there is more tables, then create
        If listdif.Count > 0 Then
            'repeat this procedure and create new table
            Dim z As Integer
            z = 0
            For z = 0 To listdif.Count - 1
                Upg()
                Dim strsql_mod As String
                strsql_mod = "create table " & listdif.Item(z) & "(id serial primary key)"

                Dim modoldtable_cn As New clsGreConnect
                modoldtable_cn.buildConn()
                modoldtable_cn.ExecuteNonQuery(strsql_mod)
                modoldtable_cn.CloseConn()
            Next

            Stage1()

        End If



        seenewReader.Close()
        seenewcmd.Dispose()
        seenewgo.CloseConn()

        seeoldReader.Close()
        seeoldcmd.Dispose()
        seeoldgo.CloseConn()
        Me.Cursor = Cursors.Default
    End Sub




    Private Sub CheckColumnAndUpdate(ByVal strsql As String, ByVal table_name As String)

        Dim newcollis As New List(Of String)
        Dim oldcollis As New List(Of String)
        Dim colname As String


        Dim seenewgo As New clsNewGreConnect
        seenewgo.buildConn()
        Dim seenewcmd As New SqlClient.SqlCommand(strsql, seenewgo.grecon)
        Dim seenewReader As SqlDataReader = seenewcmd.ExecuteReader(CommandBehavior.CloseConnection)

        Do While seenewReader.Read
            Upg()
            newcollis.Add(seenewReader("column_name"))

        Loop

        seenewReader.Close()
        seenewcmd.Dispose()
        seenewgo.CloseConn()


        Dim seeoldgo As New clsGreConnect
        seeoldgo.buildConn()
        Dim seeoldcmd As New SqlClient.SqlCommand(strsql, seeoldgo.grecon)
        Dim seeoldReader As SqlDataReader = seeoldcmd.ExecuteReader(CommandBehavior.CloseConnection)

        Do While seeoldReader.Read
            Upg()
            oldcollis.Add(seeoldReader("column_name"))
        Loop

        seeoldReader.Close()
        seeoldcmd.Dispose()
        seeoldgo.CloseConn()



        Dim listdif As New List(Of String)
        listdif = newcollis.Except(oldcollis).ToList

        Dim x As Integer
        Dim fn_datatype As String
        Dim fn_colname As String

        x = 0
        For x = 0 To listdif.Count - 1
            Upg()
            Dim strexec As String
            strexec = "SELECT * FROM information_schema.columns WHERE table_schema = 'public' AND table_name ='" & table_name & "' and column_name='" & listdif.Item(x) & "'"

            Dim fn_newconnect As New clsNewGreConnect
            fn_newconnect.buildConn()
            Dim fn_cmd As New SqlClient.SqlCommand(strexec, fn_newconnect.grecon)
            Dim fn_Reader As SqlDataReader = fn_cmd.ExecuteReader(CommandBehavior.CloseConnection)

            Do While fn_Reader.Read
                Upg()
                fn_colname = fn_Reader("column_name")
                fn_datatype = fn_Reader("data_type")

            Loop




            Dim strsql_mod As String
            strsql_mod = "alter table " & table_name & " add column " & fn_colname & " " & fn_datatype


            Dim modoldtable_cn As New clsGreConnect
            modoldtable_cn.buildConn()
            modoldtable_cn.ExecuteNonQuery(strsql_mod)

            fn_Reader.Close()
            fn_cmd.Dispose()
            fn_newconnect.CloseConn()
            modoldtable_cn.CloseConn()


        Next


    End Sub




 
    Private Sub UpdateDBDataFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim perlu As Boolean = False
        Dim strcheck As String
        Dim objgrecheck As New clsGreConnect
        objgrecheck.buildConn()
        strcheck = "select * from INFORMATION_SCHEMA.COLUMNS where table_name='jobdetail' and column_name='resultabnormalflag'"
        Dim cmd As New  SqlClient.SqlCommand(strcheck, objgrecheck.grecon)
        Dim rdr As  SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdr.Read
            If CInt(rdr("character_maximum_length")) <> 10 Then
                perlu = True
            End If
        Loop


        '=====

        If perlu = True Then
            Dim stralter As String
            stralter = "ALTER TABLE jobdetail ALTER COLUMN resultabnormalflag TYPE character(10)"

            Dim objcon As New clsGreConnect
            objcon.buildConn()
            objcon.ExecuteNonQuery(stralter)
            objcon.CloseConn()
            MessageBox.Show("Selesai", "Terimakasih")

        Else
            MessageBox.Show("Tidak ada yg perlu di update")
        End If
        
    End Sub
End Class