﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTcpClient
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTcpClient))
        Me.cmdConnect = New System.Windows.Forms.Button
        Me.txtRcv = New System.Windows.Forms.TextBox
        Me.txtipaddress = New System.Windows.Forms.TextBox
        Me.txtport = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.cmdsendlist = New System.Windows.Forms.Button
        Me.txtshow = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.dgvJoblist = New System.Windows.Forms.DataGridView
        Me.dgvdetail = New System.Windows.Forms.DataGridView
        Me.cbpalioAvailable = New System.Windows.Forms.ComboBox
        Me.btnPalioSimpan = New System.Windows.Forms.Button
        Me.btnresult = New System.Windows.Forms.Button
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        CType(Me.dgvJoblist, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvdetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdConnect
        '
        Me.cmdConnect.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdConnect.Location = New System.Drawing.Point(223, 79)
        Me.cmdConnect.Name = "cmdConnect"
        Me.cmdConnect.Size = New System.Drawing.Size(80, 25)
        Me.cmdConnect.TabIndex = 0
        Me.cmdConnect.Text = "connect!"
        Me.cmdConnect.UseVisualStyleBackColor = True
        '
        'txtRcv
        '
        Me.txtRcv.Location = New System.Drawing.Point(-268, 216)
        Me.txtRcv.Multiline = True
        Me.txtRcv.Name = "txtRcv"
        Me.txtRcv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRcv.Size = New System.Drawing.Size(287, 120)
        Me.txtRcv.TabIndex = 1
        Me.txtRcv.Visible = False
        '
        'txtipaddress
        '
        Me.txtipaddress.Location = New System.Drawing.Point(81, 79)
        Me.txtipaddress.Name = "txtipaddress"
        Me.txtipaddress.Size = New System.Drawing.Size(136, 20)
        Me.txtipaddress.TabIndex = 2
        Me.txtipaddress.Text = "192.168.1.14"
        '
        'txtport
        '
        Me.txtport.Location = New System.Drawing.Point(-86, 136)
        Me.txtport.Name = "txtport"
        Me.txtport.Size = New System.Drawing.Size(105, 20)
        Me.txtport.TabIndex = 3
        Me.txtport.Text = "4000"
        Me.txtport.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 82)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "IP Address"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(-8, 120)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(26, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Port"
        Me.Label2.Visible = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 1100
        '
        'cmdsendlist
        '
        Me.cmdsendlist.Location = New System.Drawing.Point(327, 48)
        Me.cmdsendlist.Name = "cmdsendlist"
        Me.cmdsendlist.Size = New System.Drawing.Size(145, 56)
        Me.cmdsendlist.TabIndex = 8
        Me.cmdsendlist.Text = "Kirim "
        Me.cmdsendlist.UseVisualStyleBackColor = True
        '
        'txtshow
        '
        Me.txtshow.Location = New System.Drawing.Point(-268, 362)
        Me.txtshow.Multiline = True
        Me.txtshow.Name = "txtshow"
        Me.txtshow.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtshow.Size = New System.Drawing.Size(286, 184)
        Me.txtshow.TabIndex = 9
        Me.txtshow.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(-65, 346)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "data terkirim :"
        Me.Label3.Visible = False
        '
        'dgvJoblist
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvJoblist.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvJoblist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvJoblist.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvJoblist.Location = New System.Drawing.Point(12, 110)
        Me.dgvJoblist.Name = "dgvJoblist"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvJoblist.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvJoblist.Size = New System.Drawing.Size(460, 240)
        Me.dgvJoblist.TabIndex = 11
        '
        'dgvdetail
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvdetail.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvdetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvdetail.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvdetail.Location = New System.Drawing.Point(12, 364)
        Me.dgvdetail.Name = "dgvdetail"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvdetail.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvdetail.Size = New System.Drawing.Size(460, 182)
        Me.dgvdetail.TabIndex = 12
        '
        'cbpalioAvailable
        '
        Me.cbpalioAvailable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbpalioAvailable.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbpalioAvailable.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cbpalioAvailable.FormattingEnabled = True
        Me.cbpalioAvailable.Location = New System.Drawing.Point(81, 48)
        Me.cbpalioAvailable.Name = "cbpalioAvailable"
        Me.cbpalioAvailable.Size = New System.Drawing.Size(136, 21)
        Me.cbpalioAvailable.TabIndex = 13
        '
        'btnPalioSimpan
        '
        Me.btnPalioSimpan.Location = New System.Drawing.Point(223, 48)
        Me.btnPalioSimpan.Name = "btnPalioSimpan"
        Me.btnPalioSimpan.Size = New System.Drawing.Size(80, 25)
        Me.btnPalioSimpan.TabIndex = 14
        Me.btnPalioSimpan.Text = "simpan"
        Me.btnPalioSimpan.UseVisualStyleBackColor = True
        '
        'btnresult
        '
        Me.btnresult.Location = New System.Drawing.Point(-62, 162)
        Me.btnresult.Name = "btnresult"
        Me.btnresult.Size = New System.Drawing.Size(80, 25)
        Me.btnresult.TabIndex = 15
        Me.btnresult.Text = "Minta Hasil"
        Me.btnresult.UseVisualStyleBackColor = True
        Me.btnresult.Visible = False
        '
        'Timer2
        '
        Me.Timer2.Interval = 1100
        '
        'Timer3
        '
        Me.Timer3.Interval = 1500
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(6, 6)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(466, 33)
        Me.Panel1.TabIndex = 16
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(-62, 190)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 13)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "data diterima"
        Me.Label4.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 53)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 13)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "Instrument"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(495, 50)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(86, 105)
        Me.Button1.TabIndex = 19
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmTcpClient
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(482, 554)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnresult)
        Me.Controls.Add(Me.btnPalioSimpan)
        Me.Controls.Add(Me.cbpalioAvailable)
        Me.Controls.Add(Me.dgvdetail)
        Me.Controls.Add(Me.dgvJoblist)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtshow)
        Me.Controls.Add(Me.cmdsendlist)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtport)
        Me.Controls.Add(Me.txtipaddress)
        Me.Controls.Add(Me.txtRcv)
        Me.Controls.Add(Me.cmdConnect)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmTcpClient"
        Me.Text = "Palio Miura Data Transfer"
        CType(Me.dgvJoblist, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvdetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdConnect As System.Windows.Forms.Button
    Friend WithEvents txtRcv As System.Windows.Forms.TextBox
    Friend WithEvents txtipaddress As System.Windows.Forms.TextBox
    Friend WithEvents txtport As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents cmdsendlist As System.Windows.Forms.Button
    Friend WithEvents txtshow As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvJoblist As System.Windows.Forms.DataGridView
    Friend WithEvents dgvdetail As System.Windows.Forms.DataGridView
    Friend WithEvents cbpalioAvailable As System.Windows.Forms.ComboBox
    Friend WithEvents btnPalioSimpan As System.Windows.Forms.Button
    Friend WithEvents btnresult As System.Windows.Forms.Button
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents Timer3 As System.Windows.Forms.Timer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
