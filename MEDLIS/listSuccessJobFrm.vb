﻿Imports DevExpress.XtraReports.UI

Public Class listSuccessJobFrm
    Dim start_to_pass As String
    Dim end_to_pass As String

    Private Sub cmdShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdShow.Click

        Dim rpt As New JobComplete
        rpt.Parameters("DateStart").Value = dpstart.Value.ToString("yyyy-MM-dd")
        rpt.Parameters("DateEnd").Value = dpend.Value.ToString("yyyy-MM-dd")
        rpt.Parameters("DateStart").Visible = False
        rpt.Parameters("DateEnd").Visible = False
        Dim preview As New ReportPrintTool(rpt)
        preview.ShowPreviewDialog()

        'Dim objGreConnect As New clsGreConnect
        'objGreConnect.buildConn()


        'Dim pormat As String
        'pormat = "yyyy-MM-dd"

        'Dim dt As New DataTable
        'dgvdetail.DataSource = Nothing

        'Dim dtstart As Date
        'Dim dtend As Date

        'dtstart = dpstart.Value
        'dtend = dpend.Value


        'start_to_pass = dtstart.ToString(pormat)
        'end_to_pass = dtend.ToString(pormat)

        'Dim strsql As String
        ''strsql = "select jobdetail.labnumber,jobdetail.sampleno,jobdetail.universaltest,jobdetail.measurementvalue from jobdetail where jobdetail.status='2' and jobdetail.active='1' and labnumber not in (select distinct labnumber from jobdetail where status<>'2' and active='1')"
        ''strsql = "select distinct job.labnumber,job.idpasien,patient.patientname,jobdetail.labnumber from job,patient,jobdetail where job.idpasien=patient.id and job.labnumber=jobdetail.labnumber and job.[print]<>'1' and job.labnumber not in (select distinct labnumber from jobdetail where status<>'2' and active='1')"
        'strsql = "select row_number() over() as num,labnumber,patientname,datereceived,paymentname,discount,totalprice,paymenttype from job,patient where paymentstatus='1' and idpasien=patient.id and datereceived between '" & dtstart.ToString(pormat) & "' and '" & dtend.ToString(pormat) & "'" 'bagus penting query timestamp ke date
        '' payment_date BETWEEN '2007-02-07'
        ''AND '2007-02-15';
        'Dim tblData As New DataTable

        'tblData = objGreConnect.ExecuteQuery(strsql) ' order by testgroupname asc")
        'dgvdetail.DataSource = tblData

        'For j = 0 To dgvdetail.Columns.Count - 1
        '    dgvdetail.Columns(j).Visible = False
        'Next

        'dgvdetail.Columns("num").Visible = True
        'dgvdetail.Columns("num").HeaderText = "No."
        'dgvdetail.Columns("num").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        'dgvdetail.Columns("num").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        'dgvdetail.Columns("num").Width = 80

        'dgvdetail.Columns("labnumber").Visible = True
        'dgvdetail.Columns("labnumber").HeaderText = "Nomor Lab"
        'dgvdetail.Columns("labnumber").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        'dgvdetail.Columns("labnumber").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        'dgvdetail.Columns("labnumber").Width = 120

        'dgvdetail.Columns("patientname").Visible = True
        'dgvdetail.Columns("patientname").HeaderText = "Nama Pasien"
        'dgvdetail.Columns("patientname").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        'dgvdetail.Columns("patientname").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        'dgvdetail.Columns("patientname").Width = 160


        'dgvdetail.Columns("datereceived").Visible = True
        'dgvdetail.Columns("datereceived").HeaderText = "Tanggal"
        'dgvdetail.Columns("datereceived").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        'dgvdetail.Columns("datereceived").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        'dgvdetail.Columns("datereceived").Width = 80

        'dgvdetail.Columns("totalprice").Visible = True
        'dgvdetail.Columns("totalprice").HeaderText = "Total Harga"
        'dgvdetail.Columns("totalprice").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        'dgvdetail.Columns("totalprice").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        'dgvdetail.Columns("totalprice").DefaultCellStyle.Format = "c"

        'dgvdetail.Columns("totalprice").Width = 80


        'dgvdetail.AllowUserToAddRows = False
        'dgvdetail.RowHeadersVisible = False
        'objGreConnect.CloseConn()

    End Sub

    Private Sub listSuccessJobFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("id-ID")
        System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo("id-ID") 'bagus penting

    End Sub

    Private Sub mnuCetak_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim pormat As String
        pormat = "yyyy-MM-dd"



        Dim dtstart As Date
        Dim dtend As Date

        dtstart = dpstart.Value
        dtend = dpend.Value


        start_to_pass = dtstart.ToString(pormat)
        end_to_pass = dtend.ToString(pormat)

        Dim strsql As String
        strsql = "select row_number() over() as num,labnumber,patientname,datereceived,paymentname,discount,totalprice,paymenttype from job,patient where paymentstatus='1' and idpasien=patient.id and datereceived between '" & dtstart.ToString(pormat) & "' and '" & dtend.ToString(pormat) & "'" 'bagus penting query timestamp ke date

        Dim pormat2 As String
        pormat2 = "dd-MM-yyyy"
        start_to_pass = dtstart.ToString(pormat2)
        end_to_pass = dtend.ToString(pormat2)

        Dim orptListSuccess As New rptListSuccessFrm(start_to_pass, end_to_pass, strsql) 'start_to_pass, end_to_pass)
        orptListSuccess.MdiParent = MainFrm
        orptListSuccess.StartPosition = FormStartPosition.Manual
        orptListSuccess.Top = 0
        orptListSuccess.Left = 0
        orptListSuccess.Show()

    End Sub
End Class