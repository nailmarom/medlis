﻿Imports System.Data.SqlClient

Public Class PaliomachineMapJob
    Dim selectedgroupid As Integer
    Dim greobject As clsGreConnect
    Dim dtable As DataTable
    Dim selectedInstrumentName As String
    Dim isNew As Boolean
    Dim isEdit As Boolean

    Dim myPort As Array
    Private Sub PaliomachineMapJob_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim strsql As String
        isNew = False
        isEdit = False
        CekState()
        ' strsql = "select instrument.id,instrument.name,instrument.desc,instrument.idtestgroup from instrument,testtype where instrument.idtest=testtype.id" 'sampai sini

        
        dgvinstrument.RowHeadersVisible = False
        dgvinstrument.AllowUserToAddRows = False

        
        dgvtestname.RowHeadersVisible = False
        dgvtestname.AllowUserToAddRows = False

        InstrumentList()
        selectedInstrumentName = ""
        txtinstrument.ReadOnly = True
        txtdesc.ReadOnly = True
    End Sub



    Private Sub CekState()
        If isNew = False And isEdit = False Then
            AddMnu.Enabled = True
            SaveMnu.Enabled = False
            EditMnu.Enabled = True
        ElseIf isNew = True And isEdit = False Then
            SaveMnu.Enabled = True
            AddMnu.Enabled = False
            EditMnu.Enabled = False
        ElseIf isNew = False And isEdit = True Then
            SaveMnu.Enabled = True
            AddMnu.Enabled = False
            EditMnu.Enabled = False
        End If


    End Sub


    Private Sub LoadChoiceCombo()
        Dim strsql As String
        Dim greObject As New clsGreConnect

        strsql = "select * from testgroup"

        greObject.buildConn()

        Dim thecommand As New SqlClient.SqlCommand(strsql, greObject.grecon)
        Dim theReader As SqlDataReader = thecommand.ExecuteReader(CommandBehavior.CloseConnection)

        Do While theReader.Read
            'cbtestgroup.Items.Add(theReader("testgroupname"))
            'cbTestGroup.Items.Add(testTypeReader("testgroupname"))
        Loop

        theReader.Close()
        thecommand.Dispose()
        greObject.CloseConn()
        greObject = Nothing


    End Sub

    Private Sub cbtestgroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objTestRelated As New TestRelated
        ' selectedgroupid = objTestRelated.GetIdTestGroup(cbtestgroup.Text)
        PopulateDgTest()
    End Sub
    Private Sub PopulateDgTest()

        greobject = New clsGreConnect
        greobject.buildConn()

        dtable = New DataTable
        dtable = greobject.ExecuteQuery("Select * from testtype where idtestgroup='" & selectedgroupid & "' order by testname asc")

        dgvtestname.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvtestname.Columns.Count - 1
            dgvtestname.Columns(j).Visible = False
        Next

        If Not dgvtestname.Columns.Contains("chk") Then
            Dim chk As New DataGridViewCheckBoxColumn()

            dgvtestname.Columns.Add(chk)
            chk.HeaderText = "SELECT"
            chk.Width = 120
            chk.Name = "chk"

            dgvtestname.Columns("chk").DisplayIndex = 0
            dgvtestname.Columns("chk").Name = "chk"
        End If

        dgvtestname.Columns("chk").Visible = True

        dgvtestname.Columns("testname").Visible = True
        dgvtestname.Columns("testname").HeaderText = "Test Name"
        dgvtestname.Columns("testname").Width = 200

        dgvtestname.Columns("analyzertestname").Visible = True
        dgvtestname.Columns("analyzertestname").HeaderText = "Analyzer Test Name"
        dgvtestname.Columns("analyzertestname").Width = 200

        greobject.CloseConn()
        greobject = Nothing

    End Sub

    Private Sub InstrumentList()

        'if you ever add column manually, better to clear before reconstructing
        dgvinstrument.DataSource = Nothing
        If dgvinstrument.Columns.Contains("Detail") Then
            dgvinstrument.Columns.Clear()
        End If
        If dgvinstrument.Rows.Count > 0 Then
            dgvinstrument.Rows.Clear()
        End If



        Dim strsql As String
        strsql = "select distinct name,description from palioinstrument order by name asc"

        greobject = New clsGreConnect
        greobject.buildConn()

        dtable = New DataTable
        dtable = greobject.ExecuteQuery(strsql)

        dgvinstrument.DataSource = dtable
        Dim j As Integer
        For j = 0 To dgvinstrument.Columns.Count - 1
            dgvinstrument.Columns(j).Visible = False
        Next

        dgvinstrument.Columns("name").Visible = True
        dgvinstrument.Columns("name").HeaderText = "Name"
        dgvinstrument.Columns("name").Width = 130

        dgvinstrument.Columns("description").Visible = True
        dgvinstrument.Columns("description").HeaderText = "Desc"
        dgvinstrument.Columns("description").Width = 210

        Dim buttonColumn As New DataGridViewButtonColumn()
        With buttonColumn
            .Name = "Detail"
            .HeaderText = "Detail"
            .Width = 60
            .Text = "Detail"
            .UseColumnTextForButtonValue = True
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            .FlatStyle = FlatStyle.Standard
            .CellTemplate.Style.BackColor = Color.Yellow
            .DisplayIndex = 0
        End With
        dgvinstrument.Columns.Add(buttonColumn)

        dgvinstrument.AllowUserToAddRows = False
        dgvinstrument.RowHeadersVisible = False
    End Sub


    Private Sub dgvinstrument_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvinstrument.CellContentClick
        Dim idx As Integer

        Dim instrName As String
        Dim instrDesc As String

        idx = dgvinstrument.Columns("Detail").Index
        ' txtlabnum.Text = ""
        If e.RowIndex < 0 OrElse Not e.ColumnIndex = dgvinstrument.Columns("Detail").Index Then

            Return

        Else

            If Not dgvinstrument.CurrentRow.IsNewRow Then

                instrName = dgvinstrument.Item("name", dgvinstrument.CurrentRow.Index).Value
                instrDesc = dgvinstrument.Item("description", dgvinstrument.CurrentRow.Index).Value

                selectedInstrumentName = instrName

                txtinstrument.ReadOnly = False
                txtdesc.ReadOnly = False

                txtinstrument.Text = instrName
                txtdesc.Text = instrDesc


                txtinstrument.ReadOnly = True
                txtdesc.ReadOnly = True


                ClearDGVtestname()
                ShowInstrumentAssignTest() '(instrName)
                ShowInstrumenCapabilities(instrName)
            End If

        End If
    End Sub
    Private Sub ClearDGVtestname()
        dgvtestname.DataSource = Nothing
        If dgvtestname.Columns.Contains("chk") Then
            dgvtestname.Columns.Clear()
        End If
        If dgvtestname.Columns.Contains("txt") Then
            dgvtestname.Columns.Clear()
        End If
        If dgvtestname.Rows.Count > 0 Then
            dgvtestname.Rows.Clear()
        End If

    End Sub
    Private Sub ShowInstrumentAssignTest()

        'load all test item and after that choose

        Dim strsqlins As String
        'strsqlins = "select instrument.idtest,testtype.analyzertestname from instrument,testtype where name='" & instrName & "' and instrument.idtest=testtype.id"
        strsqlins = "select testtype.id,testtype.analyzertestname from testtype" ' instrument,testtype where name='" & instrName & "' and instrument.idtest=testtype.id"

        greobject = New clsGreConnect
        greobject.buildConn()

        dtable = New DataTable
        dtable = greobject.ExecuteQuery(strsqlins)

        dgvtestname.DataSource = dtable

        Dim j As Integer
        For j = 0 To dgvtestname.Columns.Count - 1
            dgvtestname.Columns(j).Visible = False
        Next

        If Not dgvtestname.Columns.Contains("chk") Then
            Dim chk As New DataGridViewCheckBoxColumn()

            dgvtestname.Columns.Add(chk)
            chk.HeaderText = "SELECT"
            chk.Width = 60
            chk.Name = "chk"

            dgvtestname.Columns("chk").DisplayIndex = 0
            dgvtestname.Columns("chk").Name = "chk"
        End If

        If Not dgvtestname.Columns.Contains("barcodetest") Then
            Dim txt As New DataGridViewTextBoxColumn()

            dgvtestname.Columns.Add(txt)
            txt.HeaderText = "Barcode Test Name"
            txt.Width = 120
            txt.Name = "txt"

            dgvtestname.Columns("txt").DisplayIndex = 3
            dgvtestname.Columns("txt").Name = "txt"
        End If



        dgvtestname.Columns("chk").Visible = True
        dgvtestname.Columns("txt").Visible = True
        ' dgvtestname.Columns("txt").ReadOnly = False


        dgvtestname.Columns("analyzertestname").Visible = True
        dgvtestname.Columns("analyzertestname").HeaderText = "Test Name"
        dgvtestname.Columns("analyzertestname").Width = 180

        greobject.CloseConn()
        greobject = Nothing

    End Sub

    Private Sub ShowInstrumenCapabilities(ByVal instrName As String)
        Dim strchoose As String
        Dim ocon As New clsGreConnect

        strchoose = "select palioinstrument.idtest,testtype.analyzertestname,testbarcode from palioinstrument,testtype where name='" & instrName & "' and palioinstrument.idtest=testtype.id"
        ocon.buildConn()
        Dim cmdchoose As New SqlClient.SqlCommand(strchoose, ocon.grecon)
        Dim rdrchoose As SqlDataReader = cmdchoose.ExecuteReader(CommandBehavior.CloseConnection)
        Do While rdrchoose.Read
            For i = 0 To dgvtestname.Rows.Count - 1
                dgvtestname.Rows(i).ReadOnly = False
                If Not IsDBNull(rdrchoose("idtest")) Then
                    If dgvtestname.Item("id", i).Value = rdrchoose("idtest") Then
                        dgvtestname.Rows(i).ReadOnly = False
                        ' dgvtestname.Rows(i).DefaultCellStyle.BackColor = Color.Aquamarine
                        dgvtestname.Rows(i).Cells(2).Value = True   'check berada di nomor 2
                        dgvtestname.Rows(i).Cells(3).Value = rdrchoose("testbarcode")
                    End If
                End If
                dgvtestname.Rows(i).ReadOnly = True
            Next
        Loop

    End Sub


    'Private Sub PickJobAssign()
    '    Dim i As Integer
    '    Dim strsql As String

    '    Proses.CreateConn()
    '    strsql = "select * from TESTPACK where testpackid='" & testpackid & "'"
    '    Dim packCommand As New SqlCommand(strsql, Proses.directConn)
    '    Dim packReader As SqlDataReader = packCommand.ExecuteReader(CommandBehavior.CloseConnection)
    '    Do While packReader.Read

    '        For i = 0 To DGTestItem.Rows.Count - 1
    '            If Not IsDBNull(packReader("testitemid")) Then
    '                If DGTestItem.Item("testitemid", i).Value = packReader("testitemid") Then
    '                    DGTestItem.Rows(DGTestItem.Item("testitemid", i).RowIndex).ReadOnly = False
    '                    DGTestItem.Rows(DGTestItem.Item("testitemid", i).RowIndex).DefaultCellStyle.BackColor = Color.White
    '                    DGTestItem.Rows(DGTestItem.Item("testitemid", i).RowIndex).Cells(4).Value = False

    '                End If
    '            End If
    '        Next
    '    Loop

    '    packReader.Close()
    '    packCommand.Dispose()
    '    SqlClient.SqlConnection.ClearPool(Proses.directConn)
    'End Sub


    Private Sub AddMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddMnu.Click
        isNew = True
        CekState()
        txtinstrument.Text = ""
        txtdesc.Text = ""
        txtinstrument.Focus()

        ClearDGVtestname()
        ShowInstrumentAssignTest()

        txtdesc.ReadOnly = False
        txtinstrument.ReadOnly = False

    End Sub


    Private Sub SaveMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMnu.Click
        CekLicense()

        If Trim(txtinstrument.Text) = "" Then
            MessageBox.Show("Nama instrument tidak boleh kosong")
            Exit Sub
        End If




        If isNew Then
            If IsTheNameAlreadyThere(Trim(txtinstrument.Text)) = True Then
                MessageBox.Show("Nama instrument tersebut sudah ada, silahkan pilih nama yang lain")
                Exit Sub
            End If
            Fill_Instrument_table()
            InstrumentList()
            FillInstrumentConenct()
            isNew = False
            isEdit = False
            txtdesc.ReadOnly = True
            txtinstrument.ReadOnly = True

            For Each row As DataGridViewRow In dgvtestname.Rows
                If Not row.IsNewRow Then
                    dgvtestname.Rows(row.Index).Cells("txt").ReadOnly = True
                End If
            Next


            For Each row As DataGridViewRow In dgvtestname.Rows
                If Not row.IsNewRow Then
                    dgvtestname.Rows(row.Index).Cells("chk").ReadOnly = True
                End If
            Next

        ElseIf isEdit Then
            'update instrument 

            UpdateIntrumentTable()
            InstrumentList()
            isEdit = False
            isNew = False
            txtdesc.ReadOnly = True
            txtinstrument.ReadOnly = True

            For Each row As DataGridViewRow In dgvtestname.Rows
                If Not row.IsNewRow Then
                    dgvtestname.Rows(row.Index).Cells("txt").ReadOnly = True
                End If
            Next


            For Each row As DataGridViewRow In dgvtestname.Rows
                If Not row.IsNewRow Then
                    dgvtestname.Rows(row.Index).Cells("chk").ReadOnly = True
                End If
            Next

        End If
        isEdit = False
        isNew = False
        CekState()
    End Sub
    Private Sub FillInstrumentConenct()
        Dim strinsert As String
        Dim greinsert As New clsGreConnect
        greinsert.buildConn()


        strinsert = "insert into palioinstrumentconnect(name)values" & _
            "('" & Trim(txtinstrument.Text) & "')"
        greinsert.ExecuteNonQuery(strinsert)
        greinsert.CloseConn()


    End Sub


    Private Function IsTheNameAlreadyThere(ByVal instrumentName As String) As Boolean
        Dim ceksql As String
        ceksql = "select * from palioinstrument where name='" & instrumentName & "'"

        Dim ocek As New clsGreConnect
        ocek.buildConn()
        Dim cekcmd As New SqlClient.SqlCommand(ceksql, ocek.grecon)
        Dim cekrdr As SqlDataReader = cekcmd.ExecuteReader(CommandBehavior.CloseConnection)

        If cekrdr.HasRows = True Then
            ocek.CloseConn()
            Return True
        Else
            ocek.CloseConn()
            Return False
        End If

    End Function


    Private Sub UpdateIntrumentTable()
        'update consist of delete old value
        'and insert new rows
        Dim strdelete As String
        strdelete = "delete from palioinstrument where name='" & selectedInstrumentName & "'"

        Dim ogreDel As New clsGreConnect
        ogreDel.buildConn()
        ogreDel.ExecuteNonQuery(strdelete)
        ogreDel.CloseConn()

        Fill_Instrument_table()
        UpdateInstrumentConnection()

    End Sub
    Private Sub UpdateInstrumentConnection()
        Dim ogreupdate As New clsGreConnect
        ogreupdate.buildConn()

        Dim strupdate As String
        strupdate = "update palioinstrumentconnect set name='" & Trim(txtinstrument.Text) & "' where name='" & selectedInstrumentName & "'"
        ogreupdate.ExecuteNonQuery(strupdate)
        ogreupdate.CloseConn()

    End Sub

    Private Sub Fill_Instrument_table()
        Dim i As Integer
        Dim selected_test_itemid As Integer
        Dim barcodetest As String

        Dim instrumentName As String
        Dim desc As String

        instrumentName = Trim(txtinstrument.Text)
        desc = Trim(txtdesc.Text)

        For i = 0 To dgvtestname.Rows.Count - 1
            If dgvtestname.Item("chk", i).Value = True Then
                If Not IsDBNull(dgvtestname.Item("id", i).Value) Then
                    selected_test_itemid = dgvtestname.Item("id", i).Value
                    barcodetest = dgvtestname.Item("txt", i).Value
                    Instrment_job_insert_one_per_row(instrumentName, desc, selected_test_itemid, barcodetest) 'nyimpen capa
                End If
            End If
        Next

    End Sub
    Private Sub Instrment_job_insert_one_per_row(ByVal instrumentName As String, ByVal instrumentDesc As String, ByVal idtest As Integer, ByVal barcodetest As String)
        Dim strinsert As String
        Dim greInsert As New clsGreConnect
        greInsert.buildConn()
        Try
            strinsert = "insert into palioinstrument(name,description,idtest,testbarcode)values" & _
            "('" & instrumentName & "','" & instrumentDesc & "','" & idtest & "','" & barcodetest & "')"
            greInsert.ExecuteNonQuery(strinsert)
            greInsert.CloseConn()

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try

    End Sub




    Private Sub EditMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditMnu.Click
        isNew = False
        isEdit = True
        CekState()
        If Trim(txtinstrument.Text) = "" Then
            MessageBox.Show("Tidak ada data yang dipilih. Silahkan pilih salah satu instrument")
            isNew = False
            isEdit = False
            CekState()
        Else
            For Each row As DataGridViewRow In dgvtestname.Rows
                If Not row.IsNewRow Then
                    dgvtestname.Rows(row.Index).Cells("chk").ReadOnly = False
                End If
            Next
            For Each row As DataGridViewRow In dgvtestname.Rows
                If Not row.IsNewRow Then
                    dgvtestname.Rows(row.Index).Cells("txt").ReadOnly = False
                End If
            Next
            txtdesc.ReadOnly = False
            txtinstrument.ReadOnly = False
        End If
    End Sub

    Private Sub dgvtestname_CellMouseLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvtestname.CellMouseLeave
        Button1.PerformClick()
    End Sub



    Private Sub dgvtestname_CellMouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvtestname.CellMouseUp
        Button1.PerformClick()
    End Sub

    Private Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

    End Sub

    Private Sub cancelMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelMnu.Click
        isNew = False
        isEdit = False
        CekState()
        txtdesc.ReadOnly = True
        txtinstrument.ReadOnly = True
    End Sub

    Private Sub DeleteMnu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteMnu.Click
        Dim deletepalioname As String
        deletepalioname = Trim(txtinstrument.Text)
        If deletepalioname <> "" Then
            Dim delstr As String
            Dim ogre As New clsGreConnect
            ogre.buildConn()
            delstr = "delete from palioinstrument where name='" & deletepalioname & "'"
            ogre.ExecuteNonQuery(delstr)
            ogre.CloseConn()


            Dim ogreCon As New clsGreConnect
            ogreCon.buildConn()
            delstr = "delete from palioinstrumentconnect where name='" & deletepalioname & "'"
            ogreCon.ExecuteNonQuery(delstr)
            ogreCon.CloseConn()

            ClearDGVtestname()
            InstrumentList()
            txtinstrument.Text = ""
            txtdesc.Text = ""

        End If
    End Sub
    ReadOnly AllowedKeys As String = _
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 "
    Private Sub txtinstrument_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtinstrument.KeyPress, txtdesc.KeyPress
        Select Case e.KeyChar
            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)
        End Select
    End Sub

    
  
    ReadOnly AllowedKeysGrid As String = _
       "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    Private Sub dgvtestname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvtestname.KeyPress
        Select Case e.KeyChar
            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeysGrid.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeysGrid.Contains(e.KeyChar)
        End Select
    End Sub
End Class