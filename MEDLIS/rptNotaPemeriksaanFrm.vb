﻿Public Class rptNotaPemeriksaanFrm
    Dim labnum As String
    Dim id_patient As Integer
    Dim cetak_terbilang As String

    Dim rpt_datereceived As Date
    Dim rpt_beforediscount As String
    Dim rpt_discount As String
    Dim rpt_afterdiscount As String
    Dim rpt_strpembayaran As String
    Dim rpt_jenispembayaran As String


    Public Sub New(ByVal ln As String, ByVal patientid As Integer, ByVal printterbilang As String, ByVal beforediscount As Double, ByVal discount As Double, ByVal afterdiscount As Double, ByVal strdatereceived As Date, ByVal str_jenispembayaran As String)
        InitializeComponent()

        labnum = ln
        id_patient = patientid
        cetak_terbilang = printterbilang
        rpt_beforediscount = beforediscount
        rpt_discount = discount
        rpt_afterdiscount = afterdiscount
        rpt_datereceived = strdatereceived
        rpt_jenispembayaran = str_jenispembayaran

    End Sub

    Private Sub rptNotaPemeriksaanFrm_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        ShowReport()
    End Sub

    Private Sub ShowReport()
        Try
            Dim ln As String
            Dim rdlView As New fyiReporting.RdlViewer.RdlViewer
            Dim reportStrip As New fyiReporting.RdlViewer.ViewerToolstrip


            reportStrip.Viewer = rdlView
            reportStrip.Visible = True


            ln = labnum
            'rdlView.SourceFile = New Uri("D:\Medical\VBMEDLIS\MEDLIS\MEDLIS\Reporting\rptNotaPemeriksaan.rdl")
            Dim filepath As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "rptNotaPemeriksaan.rdl")
            rdlView.SourceFile = New Uri(filepath)
            'rdlView.Parameters = String.Format("&Parpatientid={0}&Parlabnumber={1}&AppLabName={2}&AppAddress1={3}&AppAddress2={4}&AppLabPhone={5}&AppLabFax={6}&AppLabMaster={7}&Parln={8}", idpa, labnum, GLabName, GLabAddress1, GLabAddress2, GLabPhone, GLabFax, GLabMaster, ln)
            rdlView.Parameters = String.Format("&parlabnumber={0}&parpatientid={1}&parjobdetaillabnumber={2}&AppLabName={3}&AppAddress1={4}&AppAddress2={5}&AppLabPhone={6}&AppLabFax={7}&AppLabMaster={8}&AppTerbilang={9}&parbeforediscount={10}&pardiscount={11}&parafterdiscount={12}&pardatereceived={13}&paruser={14}&parjenispembayaran={15}&cn={16}", labnum, id_patient, labnum, GLabName, GLabAddress1, GLabAddress2, GLabPhone, GLabFax, GLabMaster, cetak_terbilang, rpt_beforediscount, rpt_discount, rpt_afterdiscount, rpt_datereceived, user_complete_name, rpt_jenispembayaran, strdbconn)
            ' rdlView.HideRunButton()
            rdlView.Rebuild()

            reportStrip.Top = 100
            reportStrip.Items(0).Visible = False
            reportStrip.Items(7).Visible = False

            reportStrip.Dock = DockStyle.Top

            rdlView.Dock = DockStyle.Fill

            Me.Controls.Add(reportStrip)
            Me.Controls.Add(rdlView)

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub

    Private Sub rptNotaPemeriksaanFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
        CekLicense()
    End Sub
End Class