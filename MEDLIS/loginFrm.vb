﻿'
' Created by SharpDevelop.
' User: User
' Date: 10/13/2014
' Time: 12:21 PM
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'

Imports System.Data
Imports System.Data.SqlClient
Public Partial Class loginFrm
	Public Sub New()
		' The Me.InitializeComponent call is required for Windows Forms designer support.
		Me.InitializeComponent()
		
		'
		' TODO : Add constructor code after InitializeComponents
        '
       

        'Dim iniReader As New iniReader
        'iniReader.ReadIni()

	End Sub

    Private Sub loginFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Icon = New System.Drawing.Icon("medlis2.ico")
        isValidUser = False

        isValidRegisterUser = False
        isvalidResultuser = False
        isValidSettingUser = False
        isValidPembayaranUser = False
        isValidInstrumentUser = False

        isActiveUser = False

        'If Not (isIniPresent And isIniValid) Then
        '    Me.Hide()
        '    DBSettingFrm.Show()
        '    DBSettingFrm.Activate()

        'End If
        
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ReadLicense()
        ValidateUser()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub loginFrm_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        If (isIniPresent And isIniValid) Then
            Dim usercount As Integer
            Dim strsql As String
            strsql = "select count(id) as num from medlisuser"
            Dim ogre As New clsGreConnect
            ogre.buildConn()

            Dim cmd As New SqlClient.SqlCommand(strsql, ogre.grecon)
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            Do While rdr.Read
                usercount = rdr("num")
            Loop

            If usercount = 0 Then
                Dim userfrm As New frmUser
                userfrm.MdiParent = MainFrm
                userfrm.Show()
                Me.Hide()
            Else
                txtusername.Focus()
            End If
        End If

        
    End Sub

    Private Sub txtpassword_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtpassword.KeyDown
        If e.KeyCode = Keys.Return Then
            ValidateUser()
        End If

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    
   

    Private Sub ValidateUser()
        Dim greobject As New clsGreConnect
        Dim strsql As String
        Dim struser As String
        Dim pass As String

        struser = txtusername.Text
        pass = txtpassword.Text

        '###############

        Dim userPass As String
        Dim userName As String
        Dim encrypKey As String
        Dim passAfterEnc As String

        userPass = pass
        encrypKey = "Password"

        Dim myWriter As New iniReader(userPass, encrypKey)
        passAfterEnc = myWriter.CreateEncryptedPassword()
        myWriter = Nothing

        '###############




        greobject.buildConn()
        strsql = "select * from medlisuser where username='" & struser & "' and password='" & passAfterEnc & "'"


        Dim cmd As New SqlClient.SqlCommand(strsql, greobject.grecon)
        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If rdr.HasRows Then
            Do While rdr.Read
                userid = rdr("id")
                user_complete_name = rdr("name")
                isValidUser = True

                If rdr("setting") = 1 Then
                    isValidSettingUser = True
                Else
                    isValidSettingUser = False
                End If

                If rdr("result") = 1 Then
                    isvalidResultuser = True
                Else
                    isvalidResultuser = False
                End If

                If rdr("register") = 1 Then
                    isValidRegisterUser = True
                Else
                    isValidRegisterUser = False
                End If

                If rdr("instrument") = 1 Then
                    isValidInstrumentUser = True
                Else
                    isValidInstrumentUser = False
                End If

                If rdr("pembayaran") = 1 Then
                    isValidPembayaranUser = True
                Else
                    isValidPembayaranUser = False
                End If


                If rdr("active") = 1 Then
                    isActiveUser = True
                Else
                    isActiveUser = False
                End If

            Loop

        End If

        If isValidUser = True Then
            'cek user level
            ' level 1 operator fo
            ' level 2 sp
            ' level 3 all

            If isActiveUser = True Then
                LoadGeneralData()
                MainFrm.enablemenu()
                Me.Hide()

            Else
                MessageBox.Show("Username Anda sudah tidak aktif")
            End If
        Else
            MessageBox.Show("Silahkan cek username dan password Anda")
        End If
    End Sub
    ReadOnly AllowedKeys As String = _
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 "
    Private Sub txtusername_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtusername.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)

        End Select
    End Sub

    Private Sub txtusername_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtusername.KeyUp
        If e.KeyValue = Keys.Return Then
            txtpassword.Focus()
        End If
    End Sub

    

    

    Private Sub txtpassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtpassword.KeyPress
        Select Case e.KeyChar

            Case Convert.ToChar(Keys.Enter) ' Enter is pressed
                ' Call method here...

            Case Convert.ToChar(Keys.Back) ' Backspace is pressed
                e.Handled = False ' Delete the character

            Case Convert.ToChar(Keys.Capital Or Keys.RButton) ' CTRL+V is pressed
                ' Paste clipboard content only if contains allowed keys
                e.Handled = Not Clipboard.GetText().All(Function(c) AllowedKeys.Contains(c))

            Case Else ' Other key is pressed
                e.Handled = Not AllowedKeys.Contains(e.KeyChar)

        End Select
    End Sub

    Private Sub txtpassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtpassword.TextChanged

    End Sub
End Class
